﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.Core.Handling;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.Handler
{
    public class MinilandPacketHandler : IPacketHandler
    {
        #region Instantiation

        public MinilandPacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     mjoin packet
        /// </summary>
        /// <param name="mJoinPacket"></param>
        public void JoinMiniland(MJoinPacket mJoinPacket)
        {
            if (Session.Character.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance ||
                Session.Character.IsSeal)
            {
                var sess = ServerManager.Instance.GetSessionByCharacterId(characterId: mJoinPacket.CharacterId);
                if (sess?.Character != null && (ServerManager.Instance.ChannelId != 51 ||
                                                sess.Character.Faction == Session.Character.Faction))
                {
                    if (!Session.Character.IsFriendOfCharacter(characterId: sess.Character.CharacterId) && !sess
                        .Character
                        .BattleEntity.GetOwnedNpcs()
                        .Any(predicate: s => sess.Character.BattleEntity.IsSignpost(vnum: s.NpcVNum))) return;
                    if (sess.Character.MinilandState == MinilandState.Open)
                        ServerManager.Instance.JoinMiniland(session: Session, minilandOwner: sess);
                    else
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "MINILAND_CLOSED_BY_FRIEND")));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(
                    message: Language.Instance.GetMessageFromKey(key: "CANT_DO_THAT"),
                    type: 10));
            }
        }

        /// <summary>
        ///     mg packet
        /// </summary>
        /// <param name="packet"></param>
        public void MinigamePlay(MinigamePacket packet)
        {
            //Minigames desactivated
#if DEBUG

            var client =
                ServerManager.Instance.Sessions.FirstOrDefault(predicate: s =>
                    s.Character?.Miniland == Session.Character.MapInstance);
            var mlobj =
                client?.Character.MinilandObjects.Find(match: s => s.ItemInstance.ItemVNum == packet.MinigameVNum);
            if (mlobj != null)
            {
                const bool full = false;
                var game = (byte)mlobj.ItemInstance.Item.EquipmentSlot;
                switch (packet.Type)
                {
                    //play
                    case 1:
                        if (mlobj.ItemInstance.DurabilityPoint <= 0)
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_DURABILITY_POINT"),
                                type: 0));
                            return;
                        }

                        if (Session.Character.MinilandPoint <= 0)
                            Session.SendPacket(
                                packet:
                                $"qna #mg^1^7^3125^1^1 {Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MINILAND_POINT")}");

                        Session.Character.MapInstance.Broadcast(
                            packet: UserInterfaceHelper.GenerateGuri(type: 2, argument: 1,
                                callerId: Session.Character.CharacterId));
                        Session.Character.CurrentMinigame = (short)(game == 0 ? 5102 :
                            game == 1 ? 5103 :
                            game == 2 ? 5105 :
                            game == 3 ? 5104 :
                            game == 4 ? 5113 : 5112);
                        Session.Character.MinigameLog = new MinigameLogDto
                        {
                            CharacterId = Session.Character.CharacterId,
                            StartTime = DateTime.Now.Ticks,
                            Minigame = game
                        };
                        Session.SendPacket(packet: $"mlo_st {game}");
                        break;

                    //stop
                    case 2:
                        Session.Character.CurrentMinigame = 0;
                        Session.Character.MapInstance.Broadcast(
                            packet: UserInterfaceHelper.GenerateGuri(type: 6, argument: 1,
                                callerId: Session.Character.CharacterId));
                        break;

                    case 3:
                        Session.Character.CurrentMinigame = 0;
                        Session.Character.MapInstance.Broadcast(
                            packet: UserInterfaceHelper.GenerateGuri(type: 6, argument: 1,
                                callerId: Session.Character.CharacterId));
                        if (packet.Point.HasValue && Session.Character.MinigameLog != null)
                        {
                            Session.Character.MinigameLog.EndTime = DateTime.Now.Ticks;
                            Session.Character.MinigameLog.Score = packet.Point.Value;

                            var level = -1;
                            for (short i = 0; i < GetMinilandMaxPoint(game: game).Length; i++)
                                if (packet.Point > GetMinilandMaxPoint(game: game)[i])
                                    level = i;
                                else
                                    break;

                            Session.SendPacket(packet: level != -1
                                ? $"mlo_lv {level}"
                                : $"mg 3 {game} {packet.MinigameVNum} 0 0");
                        }

                        break;

                    // select gift
                    case 4:
                        if (Session.Character.MinilandPoint >= 100 && Session.Character.MinigameLog != null
                                                                   && packet.Point.HasValue && packet.Point > 0)
                            if (GetMinilandMaxPoint(game: game)[packet.Point.Value - 1] <
                                Session.Character.MinigameLog.Score)
                            {
                                var dto = Session.Character.MinigameLog;
                                DaoFactory.MinigameLogDao.InsertOrUpdate(minigameLog: ref dto);
                                Session.Character.MinigameLog = null;
                                var obj = GetMinilandGift(game: packet.MinigameVNum, point: (int)packet.Point);
                                if (obj != null)
                                {
                                    Session.SendPacket(packet: $"mlo_rw {obj.VNum} {obj.Amount}");
                                    Session.SendPacket(packet: Session.Character.GenerateMinilandPoint());
                                    var inv =
                                        Session.Character.Inventory.AddNewToInventory(vnum: obj.VNum,
                                            amount: obj.Amount);
                                    Session.Character.MinilandPoint -= 100;
                                    if (inv.Count == 0)
                                        Session.Character.SendGift(id: Session.Character.CharacterId, vnum: obj.VNum,
                                            amount: obj.Amount,
                                            rare: 0, upgrade: 0, design: 0, isNosmall: false);

                                    if (client != Session)
                                        switch (packet.Point)
                                        {
                                            case 0:
                                                mlobj.Level1BoxAmount++;
                                                break;

                                            case 1:
                                                mlobj.Level2BoxAmount++;
                                                break;

                                            case 2:
                                                mlobj.Level3BoxAmount++;
                                                break;

                                            case 3:
                                                mlobj.Level4BoxAmount++;
                                                break;

                                            case 4:
                                                mlobj.Level5BoxAmount++;
                                                break;
                                        }
                                }
                            }

                        break;

                    case 5:
                        Session.SendPacket(packet: Session.Character.GenerateMloMg(mlobj: mlobj, packet: packet));
                        break;

                    //refill
                    case 6:
                        if (packet.Point == null || packet.Point < 0) return;

                        if (Session.Character.Gold > packet.Point)
                        {
                            Session.Character.Gold -= (int)packet.Point;
                            Session.SendPacket(packet: Session.Character.GenerateGold());
                            mlobj.ItemInstance.DurabilityPoint += (int)(packet.Point / 100);
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "REFILL_MINIGAME"),
                                    arg0: (int)packet.Point / 100)));
                            Session.SendPacket(packet: Session.Character.GenerateMloMg(mlobj: mlobj, packet: packet));
                        }

                        break;

                    //gift
                    case 7:
                        Session.SendPacket(
                            packet:
                            $"mlo_pmg {packet.MinigameVNum} {Session.Character.MinilandPoint} {(mlobj.ItemInstance.DurabilityPoint < 1000 ? 1 : 0)} {(full ? 1 : 0)} {(mlobj.Level1BoxAmount > 0 ? $"392 {mlobj.Level1BoxAmount}" : "0 0")} {(mlobj.Level2BoxAmount > 0 ? $"393 {mlobj.Level2BoxAmount}" : "0 0")} {(mlobj.Level3BoxAmount > 0 ? $"394 {mlobj.Level3BoxAmount}" : "0 0")} {(mlobj.Level4BoxAmount > 0 ? $"395 {mlobj.Level4BoxAmount}" : "0 0")} {(mlobj.Level5BoxAmount > 0 ? $"396 {mlobj.Level5BoxAmount}" : "0 0")} 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0");
                        break;

                    //get gift
                    case 8:
                        var amount = 0;
                        switch (packet.Point)
                        {
                            case 0:
                                amount = mlobj.Level1BoxAmount;
                                break;

                            case 1:
                                amount = mlobj.Level2BoxAmount;
                                break;

                            case 2:
                                amount = mlobj.Level3BoxAmount;
                                break;

                            case 3:
                                amount = mlobj.Level4BoxAmount;
                                break;

                            case 4:
                                amount = mlobj.Level5BoxAmount;
                                break;
                        }

                        var gifts = new List<Gift>();
                        for (var i = 0; i < amount; i++)
                            if (packet.Point != null)
                            {
                                var gift = GetMinilandGift(game: packet.MinigameVNum, point: (int)packet.Point);
                                if (gift != null)
                                {
                                    if (gifts.Any(predicate: o => o.VNum == gift.VNum))
                                        gifts.First(predicate: o => o.Amount == gift.Amount).Amount += gift.Amount;
                                    else
                                        gifts.Add(item: gift);
                                }
                            }

                        var str = "";
                        for (var i = 0; i < 9; i++)
                            if (gifts.Count > i)
                            {
                                var itemVNum = gifts[index: i].VNum;
                                var itemAmount = gifts[index: i].Amount;
                                var inv =
                                    Session.Character.Inventory.AddNewToInventory(vnum: itemVNum, amount: itemAmount);
                                if (inv.Count > 0)
                                    Session.SendPacket(packet: Session.Character.GenerateSay(
                                        message:
                                        $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {ServerManager.GetItem(vnum: itemVNum).Name} x {itemAmount}",
                                        type: 12));
                                else
                                    Session.Character.SendGift(id: Session.Character.CharacterId, vnum: itemVNum,
                                        amount: itemAmount, rare: 0,
                                        upgrade: 0, design: 0, isNosmall: false);

                                str += $" {itemVNum} {itemAmount}";
                            }
                            else
                            {
                                str += " 0 0";
                            }

                        Session.SendPacket(
                            packet:
                            $"mlo_pmg {packet.MinigameVNum} {Session.Character.MinilandPoint} {(mlobj.ItemInstance.DurabilityPoint < 1000 ? 1 : 0)} {(full ? 1 : 0)} {(mlobj.Level1BoxAmount > 0 ? $"392 {mlobj.Level1BoxAmount}" : "0 0")} {(mlobj.Level2BoxAmount > 0 ? $"393 {mlobj.Level2BoxAmount}" : "0 0")} {(mlobj.Level3BoxAmount > 0 ? $"394 {mlobj.Level3BoxAmount}" : "0 0")} {(mlobj.Level4BoxAmount > 0 ? $"395 {mlobj.Level4BoxAmount}" : "0 0")} {(mlobj.Level5BoxAmount > 0 ? $"396 {mlobj.Level5BoxAmount}" : "0 0")}{str}");
                        break;

                    //coupon
                    case 9:
                        var items = Session.Character.Inventory
                            .Where(predicate: s => s.ItemVNum == 1269 || s.ItemVNum == 1271)
                            .OrderBy(keySelector: s => s.Slot).ToList();
                        if (items.Count > 0)
                        {
                            var itemVNum = items[index: 0].ItemVNum;
                            Session.Character.Inventory.RemoveItemAmount(vnum: itemVNum);
                            var point = itemVNum == 1269 ? 300 : 500;
                            mlobj.ItemInstance.DurabilityPoint += point;
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "REFILL_MINIGAME"), arg0: point)));
                            Session.SendPacket(packet: Session.Character.GenerateMloMg(mlobj: mlobj, packet: packet));
                        }

                        break;
                }
            }

#endif
        }

        /// <summary>
        ///     addobj packet
        /// </summary>
        /// <param name="addObjPacket"></param>
        public void MinilandAddObject(AddObjPacket addObjPacket)
        {
            var minilandobject =
                Session.Character.Inventory.LoadBySlotAndType(slot: addObjPacket.Slot, type: InventoryType.Miniland);
            if (minilandobject != null)
            {
                if (Session.Character.MinilandObjects.All(predicate: s => s.ItemInstanceId != minilandobject.Id))
                {
                    if (Session.Character.MinilandState == MinilandState.Lock)
                    {
                        var minilandobj = new MinilandObject
                        {
                            CharacterId = Session.Character.CharacterId,
                            ItemInstance = minilandobject,
                            ItemInstanceId = minilandobject.Id,
                            MapX = addObjPacket.PositionX,
                            MapY = addObjPacket.PositionY,
                            Level1BoxAmount = 0,
                            Level2BoxAmount = 0,
                            Level3BoxAmount = 0,
                            Level4BoxAmount = 0,
                            Level5BoxAmount = 0
                        };

                        if (minilandobject.Item.ItemType == ItemType.House)
                        {
                            switch (minilandobject.Item.ItemSubType)
                            {
                                case 2:
                                    minilandobj.MapX = 31;
                                    minilandobj.MapY = 3;
                                    break;

                                case 0:
                                    minilandobj.MapX = 24;
                                    minilandobj.MapY = 7;
                                    break;

                                case 1:
                                    minilandobj.MapX = 21;
                                    minilandobj.MapY = 4;
                                    break;
                            }

                            var min = Session.Character.MinilandObjects.Find(match: s =>
                                s.ItemInstance.Item.ItemType == ItemType.House && s.ItemInstance.Item.ItemSubType
                                == minilandobject.Item.ItemSubType);
                            if (min != null)
                                MinilandRemoveObject(packet: new RmvobjPacket { Slot = min.ItemInstance.Slot });
                        }

                        Session.Character.MinilandObjects.Add(item: minilandobj);
                        Session.SendPacket(packet: minilandobj.GenerateMinilandEffect(removed: false));
                        Session.SendPacket(packet: Session.Character.GenerateMinilandPoint());
                        Session.SendPacket(packet: minilandobj.GenerateMinilandObject(deleted: false));
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "MINILAND_NEED_LOCK"),
                                type: 0));
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "ALREADY_THIS_MINILANDOBJECT"), type: 0));
                }
            }
        }

        /// <summary>
        ///     mledit packet
        /// </summary>
        /// <param name="mlEditPacket"></param>
        public void MinilandEdit(MLEditPacket mlEditPacket)
        {
            if (mlEditPacket != null && mlEditPacket.Parameters != null)
                switch (mlEditPacket.Type)
                {
                    case 1:
                        Session.Character.MinilandMessage = mlEditPacket.Parameters.Truncate(length: 50);
                        Session.SendPacket(
                            packet: $"mlintro {Session.Character.MinilandMessage.Replace(oldChar: ' ', newChar: '^')}");
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "MINILAND_INFO_CHANGED")));
                        break;

                    case 2:
                        MinilandState state;
                        Enum.TryParse(value: mlEditPacket.Parameters, result: out state);

                        switch (state)
                        {
                            case MinilandState.Private:
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "MINILAND_PRIVATE"),
                                        type: 0));

                                //Need to be review to permit one friend limit on the miniland
                                Session.Character.Miniland.Sessions
                                    .Where(predicate: s => s.Character != Session.Character)
                                    .ToList()
                                    .ForEach(action: s => ServerManager.Instance.ChangeMap(id: s.Character.CharacterId,
                                        mapId: s.Character.MapId, mapX: s.Character.MapX, mapY: s.Character.MapY));
                                break;

                            case MinilandState.Lock:
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "MINILAND_LOCK"),
                                        type: 0));
                                Session.Character.Miniland.Sessions
                                    .Where(predicate: s => s.Character != Session.Character)
                                    .ToList()
                                    .ForEach(action: s => ServerManager.Instance.ChangeMap(id: s.Character.CharacterId,
                                        mapId: s.Character.MapId, mapX: s.Character.MapX, mapY: s.Character.MapY));
                                break;

                            case MinilandState.Open:
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "MINILAND_PUBLIC"),
                                        type: 0));
                                break;
                        }

                        Session.Character.MinilandState = state;
                        break;
                }
        }

        /// <summary>
        ///     rmvobj packet
        /// </summary>
        /// <param name="packet"></param>
        public void MinilandRemoveObject(RmvobjPacket packet)
        {
            var minilandobject =
                Session.Character.Inventory.LoadBySlotAndType(slot: packet.Slot, type: InventoryType.Miniland);
            if (minilandobject != null)
            {
                if (Session.Character.MinilandState == MinilandState.Lock)
                {
                    var minilandObject =
                        Session.Character.MinilandObjects.Find(match: s => s.ItemInstanceId == minilandobject.Id);
                    if (minilandObject != null)
                    {
                        Session.Character.MinilandObjects.Remove(item: minilandObject);
                        Session.SendPacket(packet: minilandObject.GenerateMinilandEffect(removed: true));
                        Session.SendPacket(packet: Session.Character.GenerateMinilandPoint());
                        Session.SendPacket(packet: minilandObject.GenerateMinilandObject(deleted: true));
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "MINILAND_NEED_LOCK"), type: 0));
                }
            }
        }

        /// <summary>
        ///     useobj packet
        /// </summary>
        /// <param name="packet"></param>
        public void UseMinilandObject(UseobjPacket packet)
        {
            var client =
                ServerManager.Instance.Sessions.FirstOrDefault(predicate: s =>
                    s.Character?.Miniland == Session.Character.MapInstance);
            var minilandObjectItem =
                client?.Character.Inventory.LoadBySlotAndType(slot: packet.Slot, type: InventoryType.Miniland);
            if (minilandObjectItem != null)
            {
                var minilandObject =
                    client.Character.MinilandObjects.Find(match: s => s.ItemInstanceId == minilandObjectItem.Id);
                if (minilandObject != null)
                {
                    if (!minilandObjectItem.Item.IsMinilandObject)
                    {
                        var game = (byte)(minilandObject.ItemInstance.Item.EquipmentSlot == 0
                            ? 4 + minilandObject.ItemInstance.ItemVNum % 10
                            : (int)minilandObject.ItemInstance.Item.EquipmentSlot / 3);
                        const bool full = false;
                        Session.SendPacket(
                            packet:
                            $"mlo_info {(client == Session ? 1 : 0)} {minilandObjectItem.ItemVNum} {packet.Slot} {Session.Character.MinilandPoint} {(minilandObjectItem.DurabilityPoint < 1000 ? 1 : 0)} {(full ? 1 : 0)} 0 {GetMinilandMaxPoint(game: game)[0]} {GetMinilandMaxPoint(game: game)[0] + 1} {GetMinilandMaxPoint(game: game)[1]} {GetMinilandMaxPoint(game: game)[1] + 1} {GetMinilandMaxPoint(game: game)[2]} {GetMinilandMaxPoint(game: game)[2] + 2} {GetMinilandMaxPoint(game: game)[3]} {GetMinilandMaxPoint(game: game)[3] + 1} {GetMinilandMaxPoint(game: game)[4]} {GetMinilandMaxPoint(game: game)[4] + 1} {GetMinilandMaxPoint(game: game)[5]}");
                    }
                    else
                    {
                        Session.SendPacket(packet: Session.Character.GenerateStashAll());
                    }
                }
            }
        }

        static Gift GetMinilandGift(short game, int point)
        {
            var gifts = new List<Gift>();
            var rand = new Random();
            switch (game)
            {
                case 3117:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2099, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2100, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2102, amount: 3));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 1031, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1032, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1033, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1034, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2205, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2205, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2189, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2034, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2205, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2189, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2034, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2105, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2205, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2189, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2034, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2193, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3118:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2099, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2100, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2102, amount: 3));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2206, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2032, amount: 3));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2206, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2106, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2038, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2206, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2190, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2039, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2109, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2206, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2190, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2040, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2194, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3119:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2099, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2100, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2102, amount: 3));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2027, amount: 15));
                            gifts.Add(item: new Gift(vnum: 2207, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2207, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2046, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2191, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2207, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2047, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2191, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2117, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2207, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2048, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2191, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2195, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3120:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2099, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2100, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2102, amount: 3));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2208, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2017, amount: 10));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2208, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2192, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2042, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2208, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2192, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2043, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2118, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2208, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2192, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2044, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3121:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2099, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2100, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2102, amount: 4));
                            gifts.Add(item: new Gift(vnum: 1031, amount: 3));
                            gifts.Add(item: new Gift(vnum: 1032, amount: 3));
                            gifts.Add(item: new Gift(vnum: 1033, amount: 3));
                            gifts.Add(item: new Gift(vnum: 1034, amount: 3));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2034, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2205, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2189, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2035, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2193, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2275, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2036, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2193, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2037, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2193, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1029, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2197, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3122:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2099, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2100, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2102, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2032, amount: 4));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2038, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2206, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2190, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2039, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2194, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2105, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2040, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2194, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2041, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2194, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1029, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2198, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3123:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2099, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2100, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2102, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2047, amount: 15));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2046, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2205, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2189, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2047, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2195, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2117, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2048, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2195, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2049, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2195, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1029, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2199, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3124:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2099, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2100, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2102, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2017, amount: 10));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2042, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2192, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2189, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2043, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2118, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2044, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2045, amount: 3));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1029, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2200, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3125:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2034, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2189, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2205, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2035, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2105, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2036, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2193, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2037, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2193, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2201, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2226, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1029, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2213, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2193, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2034, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2226, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1030, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3126:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2038, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2106, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2206, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2039, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2109, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2040, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2194, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2040, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2194, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2201, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2231, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1029, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2214, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2194, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2231, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2202, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1030, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3127:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2046, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2207, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2047, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2117, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2048, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2195, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2049, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2195, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2199, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1029, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2216, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2195, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2203, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1030, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3128:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2042, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2192, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2208, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2043, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2118, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2044, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2045, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2200, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1029, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2215, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2204, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1030, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                // GM mini-game
                case 3130:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2042, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2192, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2208, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2043, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2118, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2044, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2045, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2200, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1029, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2215, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2204, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1030, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;

                case 3131:
                    switch (point)
                    {
                        case 0:
                            gifts.Add(item: new Gift(vnum: 2042, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2192, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2208, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 1:
                            gifts.Add(item: new Gift(vnum: 2043, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2118, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 2:
                            gifts.Add(item: new Gift(vnum: 2044, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 3:
                            gifts.Add(item: new Gift(vnum: 2045, amount: 4));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2200, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1028, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1029, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;

                        case 4:
                            gifts.Add(item: new Gift(vnum: 2215, amount: 1));
                            gifts.Add(item: new Gift(vnum: 2196, amount: 2));
                            gifts.Add(item: new Gift(vnum: 2204, amount: 1));
                            gifts.Add(item: new Gift(vnum: 1030, amount: 2));
                            gifts.Add(item: new Gift(vnum: 1013, amount: (byte)(point + 1)));
                            break;
                    }

                    break;
            }

            return gifts.OrderBy(keySelector: s => rand.Next()).FirstOrDefault();
        }

        static int[] GetMinilandMaxPoint(byte game)
        {
            switch (game)
            {
                case 0:
                    return new[] { 999, 4999, 7999, 11999, 15999, 1000000 };

                case 1:
                    return new[] { 999, 4999, 9999, 13999, 17999, 1000000 };

                case 2:
                    return new[] { 999, 3999, 7999, 14999, 24999, 1000000 };

                case 3:
                    return new[] { 999, 3999, 7999, 11999, 19999, 1000000 };

                case 4:
                case 5:
                    return new[] { 999, 4999, 7999, 11999, 15999, 1000000 };

                default:
                    return new[] { 999, 4999, 7999, 14999, 24999, 1000000 };
            }
        }

        #endregion
    }
}