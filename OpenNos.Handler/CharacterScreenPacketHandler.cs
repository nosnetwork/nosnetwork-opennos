﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using OpenNos.Core;
using OpenNos.Core.Cryptography;
using OpenNos.Core.Handling;
using OpenNos.Core.Interfaces.Packets.ClientPackets;
using OpenNos.Core.Threading;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Buff;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;

namespace OpenNos.Handler
{
    public class CharacterScreenPacketHandler : IPacketHandler
    {
        #region Instantiation

        public CharacterScreenPacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        public void CreateCharacterAction(ICharacterCreatePacket characterCreatePacket, ClassType classType)
        {
            if (Session.HasCurrentMapInstance) return;

            Logger.LogUserEvent(logEvent: "CREATECHARACTER", caller: Session.GenerateIdentity(),
                data:
                $"[CreateCharacter]Name: {characterCreatePacket.Name} Slot: {characterCreatePacket.Slot} Gender: {characterCreatePacket.Gender} HairStyle: {characterCreatePacket.HairStyle} HairColor: {characterCreatePacket.HairColor}");

            if (characterCreatePacket.Slot <= 3
                && DaoFactory.CharacterDao.LoadBySlot(accountId: Session.Account.AccountId,
                    slot: characterCreatePacket.Slot) == null
                && characterCreatePacket.Name != null
                && (characterCreatePacket.Gender == GenderType.Male ||
                    characterCreatePacket.Gender == GenderType.Female)
                && (characterCreatePacket.HairStyle == HairStyleType.HairStyleA ||
                    classType != ClassType.MartialArtist && characterCreatePacket.HairStyle == HairStyleType.HairStyleB)
                && Enumerable.Range(start: 0, count: 10).Contains(value: (byte)characterCreatePacket.HairColor) &&
                characterCreatePacket.Name.Length >= 4 && characterCreatePacket.Name.Length <= 14)
            {
                if (classType == ClassType.MartialArtist)
                {
                    var characterDTOs = DaoFactory.CharacterDao.LoadByAccount(accountId: Session.Account.AccountId);

#pragma warning disable CS0652 // Der Vergleich mit einer ganzzahligen Konstante ist nutzlos. Die Konstante befindet sich außerhalb des Bereichs vom Typ "byte".
                    if (!characterDTOs.Any(predicate: s => s.Level >= 1000)) return;
#pragma warning restore CS0652 // Der Vergleich mit einer ganzzahligen Konstante ist nutzlos. Die Konstante befindet sich außerhalb des Bereichs vom Typ "byte".

                    if (characterDTOs.Any(predicate: s => s.Class == ClassType.MartialArtist))
                    {
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "MARTIAL_ARTIST_ALREADY_EXISTING")));
                        return;
                    }
                }

                var regex = new Regex(pattern: @"^[A-Za-z0-9_áéíóúÁÉÍÓÚäëïöüÄËÏÖÜ]+$");

                if (regex.Matches(input: characterCreatePacket.Name).Count != 1)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "INVALID_CHARNAME")));
                    return;
                }

                if (DaoFactory.CharacterDao.LoadByName(name: characterCreatePacket.Name) != null)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "CHARNAME_ALREADY_TAKEN")));
                    return;
                }

                var characterDTO = new CharacterDto
                {
                    AccountId = Session.Account.AccountId,
                    Slot = characterCreatePacket.Slot,
                    Class = classType,
                    Gender = characterCreatePacket.Gender,
                    HairStyle = characterCreatePacket.HairStyle,
                    HairColor = characterCreatePacket.HairColor,
                    Name = characterCreatePacket.Name,
                    MapId = 1,
                    MapX = 79, //ServerManager.RandomNumber<short>(78, 81),
                    MapY = 116, //ServerManager.RandomNumber<short>(114, 118),
                    MaxMateCount = 10,
                    MaxPartnerCount = 3,
                    SpPoint = 10000,
                    SpAdditionPoint = 0,
                    MinilandMessage = "Welcome",
                    State = CharacterState.Active,
                    MinilandPoint = 2000,
                    PrestigeLevel = 0
                };

                switch (characterDTO.Class)
                {
                    case ClassType.MartialArtist:
                        {
                            characterDTO.Level = 81;
                            characterDTO.JobLevel = 1;
                            characterDTO.Hp = 9401;
                            characterDTO.Mp = 3156;
                        }
                        break;

                    default:
                        {
                            characterDTO.Level = 1;
                            characterDTO.JobLevel = 1;
                            characterDTO.Hp = 221;
                            characterDTO.Mp = 221;
                        }
                        break;
                }

                DaoFactory.CharacterDao.InsertOrUpdate(character: ref characterDTO);

                if (classType != ClassType.MartialArtist)
                {
                    DaoFactory.CharacterQuestDao.InsertOrUpdate(quest: new CharacterQuestDto
                    {
                        CharacterId = characterDTO.CharacterId,
                        QuestId = 1997,
                        IsMainQuest = true
                    }); //Nizar

                    DaoFactory.QuicklistEntryDao.InsertOrUpdate(dto: new QuicklistEntryDto
                    {
                        CharacterId = characterDTO.CharacterId,
                        Type = 1,
                        Slot = 1,
                        Pos = 1
                    }); //Nizar

                    DaoFactory.QuicklistEntryDao.InsertOrUpdate(dto: new QuicklistEntryDto
                    {
                        CharacterId = characterDTO.CharacterId,
                        Q2 = 1,
                        Slot = 2
                    }); //Nizar

                    DaoFactory.QuicklistEntryDao.InsertOrUpdate(dto: new QuicklistEntryDto
                    {
                        CharacterId = characterDTO.CharacterId,
                        Q2 = 8,
                        Type = 1,
                        Slot = 1,
                        Pos = 16
                    }); //Nizar

                    DaoFactory.QuicklistEntryDao.InsertOrUpdate(dto: new QuicklistEntryDto
                    {
                        CharacterId = characterDTO.CharacterId,
                        Q2 = 9,
                        Type = 1,
                        Slot = 3,
                        Pos = 1
                    }); //Nizar

                    DaoFactory.CharacterSkillDao.InsertOrUpdate(dto: new CharacterSkillDto
                    { CharacterId = characterDTO.CharacterId, SkillVNum = 200 });
                    DaoFactory.CharacterSkillDao.InsertOrUpdate(dto: new CharacterSkillDto
                    { CharacterId = characterDTO.CharacterId, SkillVNum = 201 });
                    DaoFactory.CharacterSkillDao.InsertOrUpdate(dto: new CharacterSkillDto
                    { CharacterId = characterDTO.CharacterId, SkillVNum = 209 });

                    using var inventory = new Inventory(Character: new Character(input: characterDTO));
                    inventory.AddNewToInventory(vnum: 1, amount: 1, type: InventoryType.Wear);
                    inventory.AddNewToInventory(vnum: 8, amount: 1, type: InventoryType.Wear);
                    inventory.AddNewToInventory(vnum: 12, amount: 1, type: InventoryType.Wear);
                    inventory.AddNewToInventory(vnum: 2024, amount: 10, type: InventoryType.Etc);
                    inventory.AddNewToInventory(vnum: 2081, amount: 1, type: InventoryType.Etc);
                    inventory.ForEach(action: i => DaoFactory.ItemInstanceDao.InsertOrUpdate(dto: i));
                    LoadCharacters(packet: characterCreatePacket.OriginalContent);
                }
                else
                {
                    for (short skillVNum = 1525; skillVNum <= 1539; skillVNum++)
                        DaoFactory.CharacterSkillDao.InsertOrUpdate(dto: new CharacterSkillDto
                        {
                            CharacterId = characterDTO.CharacterId,
                            SkillVNum = skillVNum
                        });

                    DaoFactory.CharacterSkillDao.InsertOrUpdate(dto: new CharacterSkillDto
                    { CharacterId = characterDTO.CharacterId, SkillVNum = 1565 });

                    using var inventory = new Inventory(Character: new Character(input: characterDTO));
                    inventory.AddNewToInventory(vnum: 5832, amount: 1, type: InventoryType.Main, Rare: 5);
                    inventory.ForEach(action: i => DaoFactory.ItemInstanceDao.InsertOrUpdate(dto: i));
                    LoadCharacters(packet: characterCreatePacket.OriginalContent);
                }
            }
        }

        /// <summary>
        ///     Char_NEW character creation character
        /// </summary>
        /// <param name="characterCreatePacket"></param>
        public void CreateCharacter(CharacterCreatePacket characterCreatePacket)
        {
            CreateCharacterAction(characterCreatePacket: characterCreatePacket, classType: ClassType.Adventurer);
        }

        /// <summary>
        ///     Char_NEW_JOB character creation character
        /// </summary>
        /// <param name="characterJobCreatePacket"></param>
        public void CreateCharacterJob(CharacterJobCreatePacket characterJobCreatePacket)
        {
            CreateCharacterAction(characterCreatePacket: characterJobCreatePacket, classType: ClassType.MartialArtist);
        }

        /// <summary>
        ///     Char_DEL packet
        /// </summary>
        /// <param name="characterDeletePacket"></param>
        public void DeleteCharacter(CharacterDeletePacket characterDeletePacket)
        {
            if (Session.HasCurrentMapInstance) return;

            if (characterDeletePacket.Password == null) return;

            Logger.LogUserEvent(logEvent: "DELETECHARACTER", caller: Session.GenerateIdentity(),
                data: $"[DeleteCharacter]Name: {characterDeletePacket.Slot}");
            var account = DaoFactory.AccountDao.LoadById(accountId: Session.Account.AccountId);
            if (account == null) return;

            if (account.Password.ToLower() == CryptographyBase.Sha512(inputString: characterDeletePacket.Password))
            {
                var character =
                    DaoFactory.CharacterDao.LoadBySlot(accountId: account.AccountId, slot: characterDeletePacket.Slot);
                if (character == null) return;

                //DAOFactory.GeneralLogDAO.SetCharIdNull(Convert.ToInt64(character.CharacterId));
                DaoFactory.CharacterDao.DeleteByPrimaryKey(accountId: account.AccountId,
                    characterSlot: characterDeletePacket.Slot);
                LoadCharacters(packet: "");
            }
            else
            {
                Session.SendPacket(packet: $"info {Language.Instance.GetMessageFromKey(key: "BAD_PASSWORD")}");
            }
        }

        /// <summary>
        ///     Load Characters, this is the Entrypoint for the Client, Wait for 3 Packets.
        /// </summary>
        /// <param name="packet"></param>
        [Packet(amount: 3, "OpenNos.EntryPoint")]
        public void LoadCharacters(string packet)
        {
            var loginPacketParts = packet.Split(' ');
            var isCrossServerLogin = false;

            // Load account by given SessionId
            if (Session.Account == null)
            {
                var hasRegisteredAccountLogin = true;
                AccountDto account = null;
                if (loginPacketParts.Length > 4)
                {
                    if (loginPacketParts.Length > 7 && loginPacketParts[4] == "DAC"
                                                    && loginPacketParts[8] == "CrossServerAuthenticate")
                    {
                        isCrossServerLogin = true;
                        account = DaoFactory.AccountDao.LoadByName(name: loginPacketParts[5]);
                    }
                    else
                    {
                        account = DaoFactory.AccountDao.LoadByName(name: loginPacketParts[4]);
                    }
                }

                try
                {
                    if (account != null)
                    {
                        if (isCrossServerLogin)
                            hasRegisteredAccountLogin =
                                CommunicationServiceClient.Instance.IsCrossServerLoginPermitted(
                                    accountId: account.AccountId,
                                    sessionId: Session.SessionId);
                        else
                            hasRegisteredAccountLogin =
                                CommunicationServiceClient.Instance.IsLoginPermitted(accountId: account.AccountId,
                                    sessionId: Session.SessionId);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(data: "MS Communication Failed.", ex: ex);
                    Session.Disconnect();
                    return;
                }

                if (loginPacketParts.Length > 4 && hasRegisteredAccountLogin)
                {
                    if (account != null)
                    {
                        if (account.Password.ToLower()
                                .Equals(value: CryptographyBase.Sha512(inputString: loginPacketParts[6]))
                            || isCrossServerLogin)
                        {
                            Session.InitializeAccount(account: new Account(input: account),
                                crossServer: isCrossServerLogin);
                            ServerManager.Instance.CharacterScreenSessions[key: Session.Account.AccountId] = Session;
                        }
                        else
                        {
                            Logger.Debug(data: $"Client {Session.ClientId} forced Disconnection, invalid Password.");
                            Session.Disconnect();
                            return;
                        }
                    }
                    else
                    {
                        Logger.Debug(data: $"Client {Session.ClientId} forced Disconnection, invalid AccountName.");
                        Session.Disconnect();
                        return;
                    }
                }
                else
                {
                    Logger.Debug(
                        data:
                        $"Client {Session.ClientId} forced Disconnection, login has not been registered or Account is already logged in.");
                    Session.Disconnect();
                    return;
                }
            }

            if (isCrossServerLogin)
            {
                if (byte.TryParse(s: loginPacketParts[6], result: out var slot))
                    SelectCharacter(selectPacket: new SelectPacket { Slot = slot });
            }
            else
            {
                // TODO: Wrap Database access up to GO
                var characters = DaoFactory.CharacterDao.LoadByAccount(accountId: Session.Account.AccountId);

                Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ACCOUNT_ARRIVED"),
                    arg0: Session.SessionId));

                // load characterlist packet for each character in CharacterDTO
                Session.SendPacket(packet: "clist_start 0");

                foreach (var character in characters)
                {
                    var inventory =
                        DaoFactory.ItemInstanceDao.LoadByType(characterId: character.CharacterId,
                            type: InventoryType.Wear);

                    var equipment = new ItemInstance[17];

                    foreach (var equipmentEntry in inventory)
                    {
                        // explicit load of iteminstance
                        var currentInstance = new ItemInstance(input: equipmentEntry);

                        if (currentInstance != null)
                            equipment[(short)currentInstance.Item.EquipmentSlot] = currentInstance;
                    }

                    var petlist = "";

                    var mates = DaoFactory.MateDao.LoadByCharacterId(characterId: character.CharacterId).ToList();

                    for (var i = 0; i < 26; i++)
                        //0.2105.1102.319.0.632.0.333.0.318.0.317.0.9.-1.-1.-1.-1.-1.-1.-1.-1.-1.-1.-1.-1
                        petlist += (i != 0 ? "." : "") +
                                   (mates.Count > i
                                       ? $"{mates[index: i].Skin}.{mates[index: i].NpcMonsterVNum}"
                                       : "-1");

                    // 1 1 before long string of -1.-1 = act completion
                    Session.SendPacket(
                        packet:
                        $"clist {character.Slot} {character.Name} 0 {(byte)character.Gender} {(byte)character.HairStyle} {(byte)character.HairColor} 0 {(byte)character.Class} {character.Level} {character.HeroLevel} {equipment[(byte)EquipmentType.Hat]?.ItemVNum ?? -1}.{equipment[(byte)EquipmentType.Armor]?.ItemVNum ?? -1}.{equipment[(byte)EquipmentType.WeaponSkin]?.ItemVNum ?? (equipment[(byte)EquipmentType.MainWeapon]?.ItemVNum ?? -1)}.{equipment[(byte)EquipmentType.SecondaryWeapon]?.ItemVNum ?? -1}.{equipment[(byte)EquipmentType.Mask]?.ItemVNum ?? -1}.{equipment[(byte)EquipmentType.Fairy]?.ItemVNum ?? -1}.{equipment[(byte)EquipmentType.CostumeSuit]?.ItemVNum ?? -1}.{equipment[(byte)EquipmentType.CostumeHat]?.ItemVNum ?? -1} {character.JobLevel}  1 1 {petlist} {(equipment[(byte)EquipmentType.Hat]?.Item.IsColored == true ? equipment[(byte)EquipmentType.Hat].Design : 0)} 0");
                }

                Session.SendPacket(packet: "clist_end");
            }
        }

        /// <summary>
        ///     select packet
        /// </summary>
        /// <param name="selectPacket"></param>
        public void SelectCharacter(SelectPacket selectPacket)
        {
            try
            {
                #region Validate Session

                if (Session?.Account == null
                    || Session.HasSelectedCharacter)
                    return;

                #endregion

                #region Load Character

                var characterDTO =
                    DaoFactory.CharacterDao.LoadBySlot(accountId: Session.Account.AccountId, slot: selectPacket.Slot);

                if (characterDTO == null) return;

                var character = new Character(input: characterDTO);

                #endregion

                #region Unban Character

                if (ServerManager.Instance.BannedCharacters.Contains(item: character.CharacterId))
                    ServerManager.Instance.BannedCharacters.RemoveAll(match: s => s == character.CharacterId);

                #endregion

                #region Initialize Character

                character.Initialize();

                character.MapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(mapId: character.MapId);
                character.PositionX = character.MapX;
                character.PositionY = character.MapY;
                character.Authority = Session.Account.Authority;

                Session.SetCharacter(character: character);

                #endregion

                #region Load General Logs

                character.GeneralLogs = new ThreadSafeGenericList<GeneralLogDto>();
                character.GeneralLogs.AddRange(value: DaoFactory.GeneralLogDao
                    .LoadByAccount(accountId: Session.Account.AccountId)
                    .Where(predicate: s => s.LogType == "DailyReward" || s.CharacterId == character.CharacterId)
                    .ToList());

                #endregion

                #region Reset SpPoint

                if (!Session.Character.GeneralLogs.Any(predicate: s =>
                    s.Timestamp == DateTime.Now && s.LogData == "World" && s.LogType == "Connection"))
                {
                    Session.Character.SpAdditionPoint += (int)(Session.Character.SpPoint / 100D * 20D);
                    Session.Character.SpPoint = 10000;
                }

                #endregion

                #region Other Character Stuffs

                Session.Character.Respawns =
                    DaoFactory.RespawnDao.LoadByCharacter(characterId: Session.Character.CharacterId).ToList();
                Session.Character.StaticBonusList = DaoFactory.StaticBonusDao
                    .LoadByCharacterId(characterId: Session.Character.CharacterId).ToList();
                Session.Character.LoadInventory();
                Session.Character.LoadQuicklists();
                Session.Character.GenerateMiniland();

                #endregion

                #region Quests

                if (!DaoFactory.CharacterQuestDao.LoadByCharacterId(characterId: Session.Character.CharacterId)
                        .Any(predicate: s => s.IsMainQuest)
                    && !DaoFactory.QuestLogDao.LoadByCharacterId(id: Session.Character.CharacterId)
                        .Any(predicate: s => s.QuestId == 1997))
                {
                    var firstQuest = new CharacterQuestDto
                    {
                        CharacterId = Session.Character.CharacterId,
                        QuestId = 1997,
                        IsMainQuest = true //Default = true
                    };

                    DaoFactory.CharacterQuestDao.InsertOrUpdate(quest: firstQuest);
                }

                DaoFactory.CharacterQuestDao.LoadByCharacterId(characterId: Session.Character.CharacterId).ToList()
                    .ForEach(action: qst =>
                        Session.Character.Quests.Add(item: new CharacterQuest(characterQuest: qst)));

                #endregion

                #region Fix Partner Slots

                if (character.MaxPartnerCount < 3) character.MaxPartnerCount = 3;

                #endregion

                #region Load Mates

                DaoFactory.MateDao.LoadByCharacterId(characterId: Session.Character.CharacterId).ToList().ForEach(
                    action: s =>
                    {
                        var mate = new Mate(input: s)
                        {
                            Owner = Session.Character
                        };

                        mate.GenerateMateTransportId();
                        mate.Monster = ServerManager.GetNpcMonster(npcVNum: s.NpcMonsterVNum);

                        Session.Character.Mates.Add(item: mate);
                    });

                #endregion

                #region Load Permanent Buff

                Session.Character.LastPermBuffRefresh = DateTime.Now;

                #endregion

                #region CharacterLife

                Session.Character.Life = Observable.Interval(period: TimeSpan.FromMilliseconds(value: 300))
                    .Subscribe(onNext: x => Session.Character.CharacterLife());

                #endregion

                #region Load Amulet

                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1))
                    .Subscribe(onNext: o =>
                    {
                        var amulet =
                            Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Amulet,
                                type: InventoryType.Wear);

                        if (amulet?.ItemDeleteTime != null || amulet?.DurabilityPoint > 0)
                            Session.Character.AddBuff(indicator: new Buff(id: 62, level: Session.Character.Level),
                                sender: Session.Character.BattleEntity);
                    });

                #endregion

                #region Load Static Buff

                foreach (var staticBuff in DaoFactory.StaticBuffDao.LoadByCharacterId(
                    characterId: Session.Character.CharacterId))
                    if (staticBuff.CardId != 319 /* Wedding */)
                        Session.Character.AddStaticBuff(staticBuff: staticBuff);

                #endregion

                #region Enter the World

                Session.Character.GeneralLogs.Add(value: new GeneralLogDto
                {
                    AccountId = Session.Account.AccountId,
                    CharacterId = Session.Character.CharacterId,
                    IpAddress = Session.IpAddress,
                    LogData = "World",
                    LogType = "Connection",
                    Timestamp = DateTime.Now
                });

                Session.SendPacket(packet: "OK");

                CommunicationServiceClient.Instance.ConnectCharacter(worldId: ServerManager.Instance.WorldId,
                    characterId: character.CharacterId);

                character.Channel = ServerManager.Instance;

                #endregion
            }
            catch (Exception ex)
            {
                Logger.Error(data: "Failed selecting the character.", ex: ex);
            }
            /*finally
            {
                // Suspicious activity detected -- kick!
                if (Session != null && (!Session.HasSelectedCharacter || Session.Character == null))
                {
                    Session.Disconnect();
                }
            }
        }*/

            #endregion
        }
    }
}