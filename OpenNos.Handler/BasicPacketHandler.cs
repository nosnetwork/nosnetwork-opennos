using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.Core.Handling;
using OpenNos.Core.Serializing;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Buff;
using OpenNos.GameObject.Event.ACT4;
using OpenNos.GameObject.Event.ICEBREAKER;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.GameObject.Packets.ClientPackets;
using OpenNos.Log.Networking;
using OpenNos.Log.Shared;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;

namespace OpenNos.Handler
{
    public class BasicPacketHandler : IPacketHandler
    {
        #region Instantiation

        public BasicPacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     $bl packet
        /// </summary>
        /// <param name="blPacket"></param>
        public void BlBlacklistAdd(BlPacket blPacket)
        {
            if (blPacket.CharacterName != null &&
                ServerManager.Instance.GetSessionByCharacterName(name: blPacket.CharacterName) is { } receiverSession)
                BlacklistAdd(blInsPacket: new BlInsPacket { CharacterId = receiverSession.Character.CharacterId });
        }

        /// <summary>
        ///     blins packet
        /// </summary>
        /// <param name="blInsPacket"></param>
        public void BlacklistAdd(BlInsPacket blInsPacket)
        {
            if (Session.Character.CharacterId == blInsPacket.CharacterId) return;

            if (DaoFactory.CharacterDao.LoadById(characterId: blInsPacket.CharacterId) is { } character
                && DaoFactory.AccountDao.LoadById(accountId: character.AccountId).Authority >= AuthorityType.Mod)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_BLACKLIST_TEAM")));
                return;
            }

            Session.Character.AddRelation(characterId: blInsPacket.CharacterId,
                Relation: CharacterRelationType.Blocked);
            Session.SendPacket(
                packet: UserInterfaceHelper.GenerateInfo(
                    message: Language.Instance.GetMessageFromKey(key: "BLACKLIST_ADDED")));
            Session.SendPacket(packet: Session.Character.GenerateBlinit());
        }

        /// <summary>
        ///     bldel packet
        /// </summary>
        /// <param name="blDelPacket"></param>
        public void BlacklistDelete(BlDelPacket blDelPacket)
        {
            Session.Character.DeleteBlackList(characterId: blDelPacket.CharacterId);
            Session.SendPacket(packet: Session.Character.GenerateBlinit());
            Session.SendPacket(
                packet: UserInterfaceHelper.GenerateInfo(
                    message: Language.Instance.GetMessageFromKey(key: "BLACKLIST_DELETED")));
        }

        /// <summary>
        ///     gop packet
        /// </summary>
        /// <param name="characterOptionPacket"></param>
        public void CharacterOptionChange(CharacterOptionPacket characterOptionPacket)
        {
            if (Session.Character == null) return;

            switch (characterOptionPacket.Option)
            {
                case CharacterOption.BuffBlocked:
                    Session.Character.BuffBlocked = characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.BuffBlocked
                            ? "BUFF_BLOCKED"
                            : "BUFF_UNLOCKED"), type: 0));
                    break;

                case CharacterOption.EmoticonsBlocked:
                    Session.Character.EmoticonsBlocked = characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.EmoticonsBlocked
                            ? "EMO_BLOCKED"
                            : "EMO_UNLOCKED"), type: 0));
                    break;

                case CharacterOption.ExchangeBlocked:
                    Session.Character.ExchangeBlocked = !characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.ExchangeBlocked
                            ? "EXCHANGE_BLOCKED"
                            : "EXCHANGE_UNLOCKED"), type: 0));
                    break;

                case CharacterOption.FriendRequestBlocked:
                    Session.Character.FriendRequestBlocked = !characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.FriendRequestBlocked
                            ? "FRIEND_REQ_BLOCKED"
                            : "FRIEND_REQ_UNLOCKED"), type: 0));
                    break;

                case CharacterOption.GroupRequestBlocked:
                    Session.Character.GroupRequestBlocked = !characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.GroupRequestBlocked
                            ? "GROUP_REQ_BLOCKED"
                            : "GROUP_REQ_UNLOCKED"), type: 0));
                    break;

                case CharacterOption.PetAutoRelive:
                    Session.Character.IsPetAutoRelive = characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.IsPetAutoRelive
                            ? "PET_AUTO_RELIVE_ENABLED"
                            : "PET_AUTO_RELIVE_DISABLED"), type: 0));
                    break;

                case CharacterOption.PartnerAutoRelive:
                    Session.Character.IsPartnerAutoRelive = characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.IsPartnerAutoRelive
                            ? "PARTNER_AUTO_RELIVE_ENABLED"
                            : "PARTNER_AUTO_RELIVE_DISABLED"), type: 0));
                    break;

                case CharacterOption.HeroChatBlocked:
                    Session.Character.HeroChatBlocked = characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.HeroChatBlocked
                            ? "HERO_CHAT_BLOCKED"
                            : "HERO_CHAT_UNLOCKED"), type: 0));
                    break;

                case CharacterOption.HpBlocked:
                    Session.Character.HpBlocked = characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.HpBlocked
                            ? "HP_BLOCKED"
                            : "HP_UNLOCKED"),
                        type: 0));
                    break;

                case CharacterOption.MinilandInviteBlocked:
                    Session.Character.MinilandInviteBlocked = characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.MinilandInviteBlocked
                            ? "MINI_INV_BLOCKED"
                            : "MINI_INV_UNLOCKED"), type: 0));
                    break;

                case CharacterOption.MouseAimLock:
                    Session.Character.MouseAimLock = characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.MouseAimLock
                            ? "MOUSE_LOCKED"
                            : "MOUSE_UNLOCKED"), type: 0));
                    break;

                case CharacterOption.QuickGetUp:
                    Session.Character.QuickGetUp = characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.QuickGetUp
                            ? "QUICK_GET_UP_ENABLED"
                            : "QUICK_GET_UP_DISABLED"), type: 0));
                    break;

                case CharacterOption.WhisperBlocked:
                    Session.Character.WhisperBlocked = !characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.WhisperBlocked
                            ? "WHISPER_BLOCKED"
                            : "WHISPER_UNLOCKED"), type: 0));
                    break;

                case CharacterOption.FamilyRequestBlocked:
                    Session.Character.FamilyRequestBlocked = !characterOptionPacket.IsActive;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: Session.Character.FamilyRequestBlocked
                            ? "FAMILY_REQ_LOCKED"
                            : "FAMILY_REQ_UNLOCKED"), type: 0));
                    break;

                case CharacterOption.GroupSharing:
                    var grp = ServerManager.Instance.Groups.Find(
                        match: g => g != null && g.IsMemberOfGroup(entityId: Session.Character.CharacterId));
                    if (grp == null) return;

                    if (!grp.IsLeader(session: Session))
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_MASTER"), type: 0));
                        return;
                    }

                    if (!characterOptionPacket.IsActive)
                    {
                        grp.SharingMode = 1;

                        Session.CurrentMapInstance?.Broadcast(client: Session,
                            content: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "SHARING"), type: 0),
                            receiver: ReceiverType.Group);
                    }
                    else
                    {
                        grp.SharingMode = 0;

                        Session.CurrentMapInstance?.Broadcast(client: Session,
                            content: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "SHARING_BY_ORDER"), type: 0),
                            receiver: ReceiverType.Group);
                    }

                    break;
            }

            Session.SendPacket(packet: Session.Character.GenerateStat());
        }

        /// <summary>
        ///     compl packet
        /// </summary>
        /// <param name="complimentPacket"></param>
        public void Compliment(ComplimentPacket complimentPacket)
        {
            if (complimentPacket != null)
            {
                if (Session.Character.CharacterId == complimentPacket.CharacterId) return;

                var sess = ServerManager.Instance.GetSessionByCharacterId(characterId: complimentPacket.CharacterId);
                if (sess != null)
                {
                    if (Session.Character.Level >= 30)
                    {
                        var dto =
                            Session.Character.GeneralLogs.LastOrDefault(predicate: s =>
                                s.LogData == "World" && s.LogType == "Connection");
                        var lastcompliment =
                            Session.Character.GeneralLogs.LastOrDefault(predicate: s =>
                                s.LogData == "World" && s.LogType == nameof(Compliment));
                        if (dto?.Timestamp.AddMinutes(value: 60) <= DateTime.Now)
                        {
                            if (lastcompliment == null ||
                                lastcompliment.Timestamp.AddDays(value: 1) <= DateTime.Now.Date)
                            {
                                sess.Character.Compliment++;
                                Session.SendPacket(packet: Session.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "COMPLIMENT_GIVEN"),
                                        arg0: sess.Character.Name), type: 12));
                                Session.Character.GeneralLogs.Add(value: new GeneralLogDto
                                {
                                    AccountId = Session.Account.AccountId,
                                    CharacterId = Session.Character.CharacterId,
                                    IpAddress = Session.IpAddress,
                                    LogData = "World",
                                    LogType = nameof(Compliment),
                                    Timestamp = DateTime.Now
                                });

                                Session.CurrentMapInstance?.Broadcast(client: Session,
                                    content: Session.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "COMPLIMENT_RECEIVED"),
                                            arg0: Session.Character.Name), type: 12),
                                    receiver: ReceiverType.OnlySomeone,
                                    characterId: complimentPacket.CharacterId);
                            }
                            else
                            {
                                Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "COMPLIMENT_COOLDOWN"),
                                        type: 11));
                            }
                        }
                        else if (dto != null)
                        {
                            Session.SendPacket(packet: Session.Character.GenerateSay(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "COMPLIMENT_LOGIN_COOLDOWN"),
                                    arg0: (dto.Timestamp.AddMinutes(value: 60) - DateTime.Now).Minutes), type: 11));
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "COMPLIMENT_NOT_MINLVL"),
                                type: 11));
                    }
                }
            }
        }

        /// <summary>
        ///     dir packet
        /// </summary>
        /// <param name="directionPacket"></param>
        public void Dir(DirectionPacket directionPacket)
        {
            if (directionPacket.CharacterId == Session.Character.CharacterId)
            {
                Session.Character.Direction = directionPacket.Direction;
                Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateDir());
            }
        }

        /// <summary>
        ///     $fl packet
        /// </summary>
        /// <param name="flPacket"></param>
        public void FlRelationAdd(FlPacket flPacket)
        {
            if (flPacket.CharacterName != null &&
                ServerManager.Instance.GetSessionByCharacterName(name: flPacket.CharacterName) is { } receiverSession)
                RelationAdd(fInsPacket: new FInsPacket { Type = 1, CharacterId = receiverSession.Character.CharacterId });
        }

        /// <summary>
        ///     fins packet
        /// </summary>
        /// <param name="fInsPacket"></param>
        public void RelationAdd(FInsPacket fInsPacket)
        {
            var characterId = fInsPacket.CharacterId;

            if (Session.Character.CharacterId == characterId) return;

            var otherSession = ServerManager.Instance.GetSessionByCharacterId(characterId: characterId);
            if (otherSession != null)
            {
                if (Session.Character.Timespace != null || otherSession.Character.Timespace != null) return;
                if (!Session.Character.IsFriendlistFull())
                {
                    if (!Session.Character.IsFriendOfCharacter(characterId: characterId) &&
                        (fInsPacket.Type == 1 || fInsPacket.Type == 2)
                        || !Session.Character.IsMarried && (fInsPacket.Type == 34 || fInsPacket.Type == 69))
                    {
                        if (!Session.Character.IsBlockedByCharacter(characterId: characterId))
                        {
                            if (!Session.Character.IsBlockingCharacter(characterId: characterId))
                            {
                                if (otherSession.Character.MarryRequestCharacters.GetAllItems()
                                    .Contains(item: Session.Character.CharacterId))
                                    switch (fInsPacket.Type)
                                    {
                                        case 34:
                                            Session.Character.DeleteRelation(characterId: characterId,
                                                relationType: CharacterRelationType.Friend);
                                            Session.Character.AddRelation(characterId: characterId,
                                                Relation: CharacterRelationType.Spouse);
                                            otherSession.SendPacket(
                                                packet:
                                                $"info {Language.Instance.GetMessageFromKey(key: "MARRIAGE_ACCEPT")}");
                                            ServerManager.Instance.Broadcast(
                                                packet: UserInterfaceHelper.GenerateMsg(
                                                    message: string.Format(
                                                        format: Language.Instance.GetMessageFromKey(
                                                            key: "MARRIAGE_ACCEPT_SHOUT"),
                                                        arg0: Session.Character.Name,
                                                        arg1: otherSession.Character.Name), type: 0));
                                            break;

                                        case 69:
                                            otherSession.SendPacket(
                                                packet:
                                                $"info {Language.Instance.GetMessageFromKey(key: "MARRIAGE_REJECTED")}");
                                            //ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("MARRIAGE_REJECT_SHOUT"), 1));
                                            break;
                                    }

                                if (otherSession.Character.FriendRequestCharacters.GetAllItems()
                                    .Contains(item: Session.Character.CharacterId))
                                {
                                    switch (fInsPacket.Type)
                                    {
                                        case 1:
                                            Session.Character.AddRelation(characterId: characterId,
                                                Relation: CharacterRelationType.Friend);
                                            Session.SendPacket(
                                                packet:
                                                $"info {Language.Instance.GetMessageFromKey(key: "FRIEND_ADDED")}");
                                            otherSession.SendPacket(
                                                packet:
                                                $"info {Language.Instance.GetMessageFromKey(key: "FRIEND_ADDED")}");
                                            break;

                                        case 2:
                                            otherSession.SendPacket(
                                                packet:
                                                $"info {Language.Instance.GetMessageFromKey(key: "FRIEND_REJECTED")}");
                                            break;

                                        default:
                                            if (Session.Character.IsFriendlistFull())
                                            {
                                                Session.SendPacket(
                                                    packet:
                                                    $"info {Language.Instance.GetMessageFromKey(key: "FRIEND_FULL")}");
                                                otherSession.SendPacket(
                                                    packet:
                                                    $"info {Language.Instance.GetMessageFromKey(key: "FRIEND_FULL")}");
                                            }

                                            break;
                                    }
                                }
                                else if (fInsPacket.Type != 34 && fInsPacket.Type != 69)
                                {
                                    if (otherSession.CurrentMapInstance?.MapInstanceType ==
                                        MapInstanceType.TalentArenaMapInstance) return;

                                    if (otherSession.Character.FriendRequestBlocked)
                                    {
                                        Session.SendPacket(
                                            packet:
                                            $"info {Language.Instance.GetMessageFromKey(key: "FRIEND_REJECTED")}");
                                        return;
                                    }

                                    otherSession.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                        dialog:
                                        $"#fins^1^{Session.Character.CharacterId} #fins^2^{Session.Character.CharacterId} {string.Format(format: Language.Instance.GetMessageFromKey(key: "FRIEND_ADD"), arg0: Session.Character.Name)}"));
                                    Session.Character.FriendRequestCharacters.Add(value: characterId);
                                }
                            }
                            else
                            {
                                Session.SendPacket(
                                    packet: $"info {Language.Instance.GetMessageFromKey(key: "BLACKLIST_BLOCKING")}");
                            }
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: $"info {Language.Instance.GetMessageFromKey(key: "BLACKLIST_BLOCKED")}");
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: $"info {Language.Instance.GetMessageFromKey(key: "ALREADY_FRIEND")}");
                    }
                }
                else
                {
                    Session.SendPacket(packet: $"info {Language.Instance.GetMessageFromKey(key: "FRIEND_FULL")}");
                }
            }
        }

        /// <summary>
        ///     fdel packet
        /// </summary>
        /// <param name="fDelPacket"></param>
        public void FriendDelete(FDelPacket fDelPacket)
        {
            if (Session.Character.CharacterRelations.Any(predicate: s =>
                s.RelatedCharacterId == fDelPacket.CharacterId && s.RelationType == CharacterRelationType.Spouse))
            {
                Session.SendPacket(packet: $"info {Language.Instance.GetMessageFromKey(key: "CANT_DELETE_COUPLE")}");
                return;
            }

            Session.Character.DeleteRelation(characterId: fDelPacket.CharacterId,
                relationType: CharacterRelationType.Friend);
            Session.SendPacket(
                packet: UserInterfaceHelper.GenerateInfo(
                    message: Language.Instance.GetMessageFromKey(key: "FRIEND_DELETED")));
        }

        /// <summary>
        ///     csp packet
        /// </summary>
        /// <param name="cspPacket"></param>
        public void SendBubbleMessage(CspPacket cspPacket)
        {
            if (cspPacket.CharacterId == Session.Character.CharacterId && Session.Character.BubbleMessage != null)
                Session.Character.MapInstance.Broadcast(packet: Session.Character.GenerateBubbleMessagePacket());
        }

        /// <summary>
        ///     btk packet
        /// </summary>
        /// <param name="btkPacket"></param>
        public void FriendTalk(BtkPacket btkPacket)
        {
            if (string.IsNullOrEmpty(value: btkPacket.Message)) return;

            var message = btkPacket.Message;
            if (message.Length > 60) message = message.Substring(startIndex: 0, length: 60);

            message = message.Trim();

            var character = DaoFactory.CharacterDao.LoadById(characterId: btkPacket.CharacterId);
            if (character != null)
            {
                var sentChannelId = CommunicationServiceClient.Instance.SendMessageToCharacter(
                    message: new ScsCharacterMessage
                    {
                        DestinationCharacterId = character.CharacterId,
                        SourceCharacterId = Session.Character.CharacterId,
                        SourceWorldId = ServerManager.Instance.WorldId,
                        Message = PacketFactory.Serialize(packet: Session.Character.GenerateTalk(message: message)),
                        Type = MessageType.PrivateChat
                    });

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogChatMessage(logEntry: new LogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        Receiver = character.Name,
                        ReceiverId = character.CharacterId,
                        MessageType = LogType.BuddyTalk,
                        Message = btkPacket.Message
                    });

                if (!sentChannelId.HasValue) //character is even offline on different world
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "FRIEND_OFFLINE")));
            }
        }

        /// <summary>
        ///     pcl packet
        /// </summary>
        /// <param name="getGiftPacket"></param>
        public void GetGift(GetGiftPacket getGiftPacket)
        {
            var giftId = getGiftPacket.GiftId;
            if (Session.Character.MailList.ContainsKey(key: giftId))
            {
                var mail = Session.Character.MailList[key: giftId];
                if (getGiftPacket.Type == 4 && mail.AttachmentVNum != null)
                {
                    if (Session.Character.Inventory.CanAddItem(itemVnum: (short)mail.AttachmentVNum))
                    {
                        var newInv = Session.Character.Inventory.AddNewToInventory(vnum: (short)mail.AttachmentVNum,
                                amount: mail.AttachmentAmount, Upgrade: mail.AttachmentUpgrade,
                                Rare: (sbyte)mail.AttachmentRarity, Design: mail.AttachmentDesign)
                            .FirstOrDefault();
                        if (newInv != null)
                        {
                            if (newInv.Rare != 0) newInv.SetRarityPoint();

                            if (newInv.Item.EquipmentSlot == EquipmentType.Gloves ||
                                newInv.Item.EquipmentSlot == EquipmentType.Boots)
                            {
                                newInv.DarkResistance = (short)(newInv.Item.DarkResistance * newInv.Upgrade);
                                newInv.LightResistance = (short)(newInv.Item.LightResistance * newInv.Upgrade);
                                newInv.WaterResistance = (short)(newInv.Item.WaterResistance * newInv.Upgrade);
                                newInv.FireResistance = (short)(newInv.Item.FireResistance * newInv.Upgrade);
                            }

                            Logger.LogUserEvent(logEvent: "PARCEL_GET", caller: Session.GenerateIdentity(),
                                data:
                                $"IIId: {newInv.Id} ItemVNum: {newInv.ItemVNum} Amount: {mail.AttachmentAmount} Sender: {mail.SenderId}");

                            Session.SendPacket(packet: Session.Character.GenerateSay(
                                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ITEM_GIFTED"),
                                    arg0: newInv.Item.Name,
                                    arg1: mail.AttachmentAmount), type: 12));

                            DaoFactory.MailDao.DeleteById(mailId: mail.MailId);

                            Session.SendPacket(packet: $"parcel 2 1 {giftId}");

                            Session.Character.MailList.Remove(key: giftId);
                        }
                    }
                    else
                    {
                        Session.SendPacket(packet: "parcel 5 1 0");
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                type: 0));
                    }
                }
                else if (getGiftPacket.Type == 5)
                {
                    Session.SendPacket(packet: $"parcel 7 1 {giftId}");

                    if (DaoFactory.MailDao.LoadById(mailId: mail.MailId) != null)
                        DaoFactory.MailDao.DeleteById(mailId: mail.MailId);

                    if (Session.Character.MailList.ContainsKey(key: giftId))
                        Session.Character.MailList.Remove(key: giftId);
                }
            }
        }

        /// <summary>
        ///     ncif packet
        /// </summary>
        /// <param name="ncifPacket"></param>
        public void GetNamedCharacterInformation(NcifPacket ncifPacket)
        {
            switch (ncifPacket.Type)
            {
                // characters
                case 1:
                    Session.SendPacket(packet: ServerManager.Instance
                        .GetSessionByCharacterId(characterId: ncifPacket.TargetId)?.Character
                        ?.GenerateStatInfo());
                    break;

                // npcs/mates
                case 2:
                    if (Session.HasCurrentMapInstance)
                    {
                        Session.CurrentMapInstance.Npcs.Where(predicate: n => n.MapNpcId == (int)ncifPacket.TargetId)
                            .ToList()
                            .ForEach(action: npc =>
                            {
                                var npcinfo = ServerManager.GetNpcMonster(npcVNum: npc.NpcVNum);
                                if (npcinfo == null) return;

                                Session.Character.LastNpcMonsterId = npc.MapNpcId;
                                Session.SendPacket(
                                    packet:
                                    $"st 2 {ncifPacket.TargetId} {npcinfo.Level} {npcinfo.HeroLevel} {(int)((float)npc.CurrentHp / (float)npc.MaxHp * 100)} {(int)((float)npc.CurrentMp / (float)npc.MaxMp * 100)} {npc.CurrentHp} {npc.CurrentMp}{npc.Buff.GetAllItems().Aggregate(seed: "", func: (current, buff) => current + $" {buff.Card.CardId}.{buff.Level}")}");
                            });
                        Parallel.ForEach(source: Session.CurrentMapInstance.Sessions, body: session =>
                        {
                            var mate = session.Character.Mates.Find(
                                match: s => s.MateTransportId == (int)ncifPacket.TargetId);
                            if (mate != null) Session.SendPacket(packet: mate.GenerateStatInfo());
                        });
                    }

                    break;

                // monsters
                case 3:
                    if (Session.HasCurrentMapInstance)
                        Session.CurrentMapInstance.Monsters
                            .Where(predicate: m => m.MapMonsterId == (int)ncifPacket.TargetId)
                            .ToList().ForEach(action: monster =>
                            {
                                var monsterinfo = ServerManager.GetNpcMonster(npcVNum: monster.MonsterVNum);
                                if (monsterinfo == null) return;

                                Session.Character.LastNpcMonsterId = monster.MapMonsterId;
                                Session.SendPacket(
                                    packet:
                                    $"st 3 {ncifPacket.TargetId} {monsterinfo.Level} {monsterinfo.HeroLevel} {(int)((float)monster.CurrentHp / (float)monster.MaxHp * 100)} {(int)((float)monster.CurrentMp / (float)monster.MaxMp * 100)} {monster.CurrentHp} {monster.CurrentMp}{monster.Buff.GetAllItems().Aggregate(seed: "", func: (current, buff) => current + $" {buff.Card.CardId}.{buff.Level}")}");
                            });

                    break;
            }
        }

        /// <summary>
        ///     RstartPacket packet
        /// </summary>
        /// <param name="rStartPacket"></param>
        public void GetRStart(RStartPacket rStartPacket)
        {
            if (Session.Character.Timespace != null)
                if (rStartPacket.Type == 1 && Session.Character.Timespace.InstanceBag != null &&
                    Session.Character.Timespace.InstanceBag.Lock == false)
                {
                    if (Session.Character.Timespace.SpNeeded?[(byte)Session.Character.Class] != 0)
                    {
                        var specialist =
                            Session.Character.Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                                type: InventoryType.Wear);
                        if (specialist == null || specialist.ItemVNum !=
                            Session.Character.Timespace.SpNeeded?[(byte)Session.Character.Class])
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "TS_SP_NEEDED"),
                                    type: 0));
                            return;
                        }
                    }

                    Session.Character.Timespace.InstanceBag.Lock = true;
                    Preq(packet: new PreqPacket());
                    Session.Character.Timespace._mapInstanceDictionary.ToList()
                        .SelectMany(selector: s => s.Value.Sessions)
                        .Where(predicate: s => s.Character?.Timespace != null).ToList().ForEach(action: s =>
                        {
                            s.Character.GeneralLogs.Add(value: new GeneralLogDto
                            {
                                AccountId = s.Account.AccountId,
                                CharacterId = s.Character.CharacterId,
                                IpAddress = s.IpAddress,
                                LogData = s.Character.Timespace.Id.ToString(),
                                LogType = "InstanceEntry",
                                Timestamp = DateTime.Now
                            });
                        });
                }
        }

        /// <summary>
        ///     npinfo packet
        /// </summary>
        /// <param name="npinfoPacket"></param>
        public void GetStats(NpinfoPacket npinfoPacket)
        {
            Session.SendPackets(packets: Session.Character.GenerateStatChar());

            if (npinfoPacket.Page != Session.Character.ScPage)
            {
                Session.Character.ScPage = npinfoPacket.Page;
                Session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());
                Session.SendPackets(packets: Session.Character.GenerateScP(page: npinfoPacket.Page));
                Session.SendPackets(packets: Session.Character.GenerateScN());
            }
        }

        /// <summary>
        ///     $pinv packet
        /// </summary>
        /// <param name="pinvPacket"></param>
        public void PinvGroupJoin(PinvPacket pinvPacket)
        {
            if (pinvPacket.CharacterName != null &&
                ServerManager.Instance.GetSessionByCharacterName(name: pinvPacket.CharacterName) is { } receiverSession)
                GroupJoin(pjoinPacket: new PJoinPacket
                { RequestType = GroupRequestType.Requested, CharacterId = receiverSession.Character.CharacterId });
        }

        /// <summary>
        ///     pjoin packet
        /// </summary>
        /// <param name="pjoinPacket"></param>
        public void GroupJoin(PJoinPacket pjoinPacket)
        {
            if (pjoinPacket != null)
            {
                var createNewGroup = true;
                var targetSession =
                    ServerManager.Instance.GetSessionByCharacterId(characterId: pjoinPacket.CharacterId);

                if (targetSession == null && !pjoinPacket.RequestType.Equals(obj: GroupRequestType.Sharing)
                    || targetSession?.CurrentMapInstance?.MapInstanceType == MapInstanceType.TalentArenaMapInstance
                    || ServerManager.Instance.ArenaMembers.ToList().Any(predicate: s =>
                        s.Session?.Character?.CharacterId == Session.Character.CharacterId)
                    || targetSession != null && ServerManager.Instance.ChannelId == 51 &&
                    targetSession.Character.Faction != Session.Character.Faction
                    || Session.Character.Timespace != null
                    || targetSession?.Character.Timespace != null)
                    return;

                if (pjoinPacket.RequestType.Equals(obj: GroupRequestType.Requested)
                    || pjoinPacket.RequestType.Equals(obj: GroupRequestType.Invited))
                {
                    if (pjoinPacket.CharacterId == 0) return;

                    if (ServerManager.Instance.IsCharactersGroupFull(characterId: pjoinPacket.CharacterId))
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "GROUP_FULL")));
                        return;
                    }

                    if (ServerManager.Instance.IsCharacterMemberOfGroup(characterId: pjoinPacket.CharacterId)
                        && ServerManager.Instance.IsCharacterMemberOfGroup(characterId: Session.Character.CharacterId))
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "ALREADY_IN_GROUP")));
                        return;
                    }

                    if (Session.Character.CharacterId != pjoinPacket.CharacterId && targetSession != null)
                    {
                        if (Session.Character.IsBlockedByCharacter(characterId: pjoinPacket.CharacterId))
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "BLACKLIST_BLOCKED")));
                            return;
                        }

                        if (targetSession.Character.IsBlockedByCharacter(characterId: Session.Character.CharacterId))
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "BLACKLIST_BLOCKING")));
                            return;
                        }

                        if (targetSession.Character.GroupRequestBlocked)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "GROUP_BLOCKED"),
                                    type: 0));
                        }
                        else
                        {
                            // save sent group request to current character
                            Session.Character.GroupSentRequestCharacterIds.Add(value: targetSession.Character
                                .CharacterId);
                            if (Session.Character.Group == null || Session.Character.Group.GroupType == GroupType.Group)
                            {
                                if (targetSession.Character?.Group == null
                                    || targetSession.Character?.Group.GroupType == GroupType.Group)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "GROUP_REQUEST"),
                                            arg0: targetSession.Character.Name)));
                                    targetSession.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                        dialog:
                                        $"#pjoin^3^{Session.Character.CharacterId} #pjoin^4^{Session.Character.CharacterId} {string.Format(format: Language.Instance.GetMessageFromKey(key: "INVITED_YOU"), arg0: Session.Character.Name)}"));
                                }
                            }
                            else if (Session.Character.Group.IsLeader(session: Session))
                            {
                                targetSession.SendPacket(
                                    packet:
                                    $"qna #rd^1^{Session.Character.CharacterId}^1 {string.Format(format: Language.Instance.GetMessageFromKey(key: "INVITE_RAID"), arg0: Session.Character.Name)}");
                            }
                        }
                    }
                }
                else if (pjoinPacket.RequestType.Equals(obj: GroupRequestType.Sharing))
                {
                    if (Session.Character.Group != null)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "GROUP_SHARE_INFO")));
                        Session.Character.Group.Sessions
                            .Where(predicate: s => s.Character.CharacterId != Session.Character.CharacterId).ToList()
                            .ForEach(action: s =>
                            {
                                s.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                    dialog:
                                    $"#pjoin^6^{Session.Character.CharacterId} #pjoin^7^{Session.Character.CharacterId} {string.Format(format: Language.Instance.GetMessageFromKey(key: "INVITED_YOU_SHARE"), arg0: Session.Character.Name)}"));
                                Session.Character.GroupSentRequestCharacterIds.Add(value: s.Character.CharacterId);
                            });
                    }
                }
                else if (pjoinPacket.RequestType.Equals(obj: GroupRequestType.Accepted))
                {
                    if (targetSession?.Character.GroupSentRequestCharacterIds.GetAllItems()
                        .Contains(item: Session.Character.CharacterId) == false)
                        return;

                    try
                    {
                        targetSession?.Character.GroupSentRequestCharacterIds.Remove(
                            match: Session.Character.CharacterId);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex: ex);
                    }

                    if (ServerManager.Instance.IsCharacterMemberOfGroup(characterId: Session.Character.CharacterId)
                        && ServerManager.Instance.IsCharacterMemberOfGroup(characterId: pjoinPacket.CharacterId))
                        // everyone is in group, return
                        return;

                    if (ServerManager.Instance.IsCharactersGroupFull(characterId: pjoinPacket.CharacterId)
                        || ServerManager.Instance.IsCharactersGroupFull(characterId: Session.Character.CharacterId))
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "GROUP_FULL")));
                        targetSession?.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "GROUP_FULL")));
                        return;
                    }

                    // get group and add to group
                    if (ServerManager.Instance.IsCharacterMemberOfGroup(characterId: Session.Character.CharacterId))
                    {
                        // target joins source
                        var currentGroup =
                            ServerManager.Instance.GetGroupByCharacterId(characterId: Session.Character.CharacterId);

                        if (currentGroup != null)
                        {
                            currentGroup.JoinGroup(session: targetSession);
                            targetSession?.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "JOINED_GROUP"),
                                    type: 10));
                            createNewGroup = false;
                        }
                    }
                    else if (ServerManager.Instance.IsCharacterMemberOfGroup(characterId: pjoinPacket.CharacterId))
                    {
                        // source joins target
                        var currentGroup =
                            ServerManager.Instance.GetGroupByCharacterId(characterId: pjoinPacket.CharacterId);

                        if (currentGroup != null)
                        {
                            createNewGroup = false;
                            if (currentGroup.GroupType == GroupType.Group)
                            {
                                currentGroup.JoinGroup(session: Session);
                            }
                            else
                            {
                                if (!currentGroup.Raid.InstanceBag.Lock)
                                {
                                    if (Session.Character.Level < currentGroup.Raid?.LevelMinimum)
                                    {
                                        Session.SendPacket(packet: Session.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(key: "LOW_LVL"), type: 10));
                                        return;
                                    }

                                    if (currentGroup.JoinGroup(session: Session))
                                    {
                                        Session.SendPacket(
                                            packet: Session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(key: "RAID_JOIN"),
                                                type: 10));
                                        if (Session.Character.Level > currentGroup.Raid?.LevelMaximum)
                                        {
                                            Session.SendPacket(packet: Session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(
                                                    key: "RAID_LEVEL_INCORRECT"), type: 10));
                                            if (Session.Character.Level
                                                >= currentGroup.Raid?.LevelMaximum + 10 /* && AlreadySuccededToday*/)
                                            {
                                                //modal 1 ALREADY_SUCCEDED_AS_ASSISTANT
                                            }
                                        }

                                        Session.SendPacket(packet: Session.Character.GenerateRaid(Type: 2));
                                        Session.SendPacket(packet: Session.Character.GenerateRaid(Type: 1));
                                        currentGroup.Sessions.ForEach(action: s =>
                                        {
                                            s.SendPacket(packet: currentGroup.GenerateRdlst());
                                            s.SendPacket(packet: s.Character.GenerateSay(
                                                message: string.Format(
                                                    format: Language.Instance.GetMessageFromKey(key: "JOIN_TEAM"),
                                                    arg0: Session.Character.Name), type: 10));
                                            s.SendPacket(packet: s.Character.GenerateRaid(Type: 0));
                                        });
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(
                                        packet: Session.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(key: "RAID_ALREADY_STARTED"),
                                            type: 10));
                                }
                            }
                        }
                    }

                    if (createNewGroup)
                    {
                        var group = new Group
                        {
                            GroupType = GroupType.Group
                        };
                        group.JoinGroup(characterId: pjoinPacket.CharacterId);
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "GROUP_JOIN"),
                                arg0: targetSession?.Character.Name), type: 10));
                        group.JoinGroup(characterId: Session.Character.CharacterId);
                        ServerManager.Instance.AddGroup(group: group);
                        targetSession?.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "GROUP_ADMIN")));

                        // set back reference to group
                        Session.Character.Group = group;
                        if (targetSession != null) targetSession.Character.Group = group;
                    }

                    if (Session.Character?.Group?.GroupType == GroupType.Group)
                    {
                        // player join group
                        ServerManager.Instance.UpdateGroup(charId: pjoinPacket.CharacterId);
                        Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GeneratePidx());
                    }
                }
                else if (pjoinPacket.RequestType == GroupRequestType.Declined)
                {
                    if (targetSession?.Character.GroupSentRequestCharacterIds.GetAllItems()
                        .Contains(item: Session.Character.CharacterId) == false) return;

                    targetSession?.Character.GroupSentRequestCharacterIds.Remove(match: Session.Character.CharacterId);

                    targetSession?.SendPacket(packet: Session.Character.GenerateSay(
                        message: string.Format(
                            format: Language.Instance.GetMessageFromKey(key: "REFUSED_GROUP_REQUEST"),
                            arg0: Session.Character.Name), type: 10));
                }
                else if (pjoinPacket.RequestType == GroupRequestType.AcceptedShare)
                {
                    if (targetSession?.Character.GroupSentRequestCharacterIds.GetAllItems()
                        .Contains(item: Session.Character.CharacterId) == false) return;

                    targetSession?.Character.GroupSentRequestCharacterIds.Remove(match: Session.Character.CharacterId);

                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ACCEPTED_SHARE"),
                            arg0: targetSession?.Character.Name), type: 0));
                    if (Session.Character?.Group?.IsMemberOfGroup(entityId: pjoinPacket.CharacterId) == true &&
                        targetSession != null)
                    {
                        Session.Character.SetReturnPoint(mapId: targetSession.Character.Return.DefaultMapId,
                            mapX: targetSession.Character.Return.DefaultX,
                            mapY: targetSession.Character.Return.DefaultY);
                        targetSession.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "CHANGED_SHARE"),
                                arg0: targetSession.Character.Name), type: 0));
                    }
                }
                else if (pjoinPacket.RequestType == GroupRequestType.DeclinedShare)
                {
                    if (targetSession?.Character.GroupSentRequestCharacterIds.GetAllItems()
                        .Contains(item: Session.Character.CharacterId) == false)
                        return;

                    targetSession?.Character.GroupSentRequestCharacterIds.Remove(match: Session.Character.CharacterId);

                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "REFUSED_SHARE"), type: 0));
                }
            }
        }

        /// <summary>
        ///     pleave packet
        /// </summary>
        /// <param name="pleavePacket"></param>
        public void GroupLeave(PLeavePacket pleavePacket)
        {
            if (pleavePacket == null) throw new ArgumentNullException(paramName: nameof(pleavePacket));
            ServerManager.Instance.GroupLeave(session: Session);
        }

        /// <summary>
        ///     ; packet
        /// </summary>
        /// <param name="groupSayPacket"></param>
        public void GroupTalk(GroupSayPacket groupSayPacket)
        {
            if (!string.IsNullOrEmpty(value: groupSayPacket.Message))
            {
                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogChatMessage(logEntry: new LogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        Receiver = "",
                        ReceiverId = Session.Character.Group?.GroupId,
                        MessageType = LogType.Group,
                        Message = groupSayPacket.Message
                    });

                ServerManager.Instance.Broadcast(client: Session,
                    content: Session.Character.GenerateSpk(message: groupSayPacket.Message, type: 3),
                    receiver: ReceiverType.Group);
            }
        }

        /// <summary>
        ///     guri packet
        /// </summary>
        /// <param name="guriPacket"></param>
        public void Guri(GuriPacket guriPacket)
        {
            if (guriPacket != null)
            {
                if (guriPacket.Data.HasValue && guriPacket.Type == 10 && guriPacket.Data.Value >= 973
                    && guriPacket.Data.Value <= 999 && !Session.Character.EmoticonsBlocked)
                {
                    if (guriPacket.User == Session.Character.CharacterId)
                    {
                        Session.CurrentMapInstance?.Broadcast(client: Session,
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                callerId: Session.Character.CharacterId,
                                effectId: guriPacket.Data.Value + 4099), receiver: ReceiverType.AllNoEmoBlocked);
                    }
                    else if (int.TryParse(s: guriPacket.User.ToString(), result: out var mateTransportId))
                    {
                        var mate = Session.Character.Mates.Find(match: s => s.MateTransportId == mateTransportId);
                        if (mate != null)
                            Session.CurrentMapInstance?.Broadcast(client: Session,
                                packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                    callerId: mate.MateTransportId,
                                    effectId: guriPacket.Data.Value + 4099), receiver: ReceiverType.AllNoEmoBlocked);
                    }
                }
                else if (guriPacket.Type == 204)
                {
                    if (guriPacket.Argument == 0 && short.TryParse(s: guriPacket.User.ToString(), result: out var slot))
                    {
                        var shell =
                            Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: InventoryType.Equipment);
                        if (shell?.ShellEffects.Count == 0 && shell.Upgrade > 0 && shell.Rare > 0
                            && Session.Character.Inventory.CountItem(itemVNum: 1429) >= shell.Upgrade / 10 + shell.Rare)
                        {
                            if (!ShellGeneratorHelper.Instance.ShellTypes.TryGetValue(key: shell.ItemVNum,
                                    value: out var shellType)
                                )
                                // SHELL TYPE NOT IMPLEMENTED
                                return;

                            /*if (shellType != 8 && shellType != 9)
                            {
                                if (shell.Upgrade < 50)
                                {
                                    return;
                                }
                            }*/

                            /*if (shellType == 8 || shellType == 9)
                            {
                                switch (shell.Upgrade)
                                {
                                    case 25:
                                    case 30:
                                    case 40:
                                    case 55:
                                    case 60:
                                    case 65:
                                    case 70:
                                    case 75:
                                    case 80:
                                    case 85:
                                        break;
                                    default:
                                        Session.Character.Inventory.RemoveItemFromInventory(shell.Id);
                                        Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("STOP_SPAWNING_BROKEN_SHELL"), 0));
                                        return;
                                }
                            }*/

                            var shellOptions =
                                ShellGeneratorHelper.Instance.GenerateShell(shellType: shellType,
                                    shellRarity: shell.Rare, shellLevel: shell.Upgrade);

                            /*if (!shellOptions.Any())
                            {
                                Session.Character.Inventory.RemoveItemFromInventory(shell.Id);
                                Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("STOP_SPAWNING_BROKEN_SHELL"), 0));
                                return;
                            }*/

                            shell.ShellEffects.AddRange(collection: shellOptions);

                            DaoFactory.ShellEffectDao.InsertOrUpdateFromList(shellEffects: shell.ShellEffects,
                                equipmentSerialId: shell.EquipmentSerialId);

                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "OPTION_IDENTIFIED"), type: 0));
                            Session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                callerId: Session.Character.CharacterId, effectId: 3006));
                            Session.Character.Inventory.RemoveItemAmount(vnum: 1429,
                                amount: shell.Upgrade / 10 + shell.Rare);
                        }
                    }
                }
                else if (guriPacket.Type == 205)
                {
                    if (guriPacket.Argument == 0 && short.TryParse(s: guriPacket.User.ToString(), result: out var slot))
                    {
                        const int perfumeVnum = 1428;

                        var perfumeInventoryType = (InventoryType)guriPacket.Argument;

                        var equipmentInstance =
                            Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: perfumeInventoryType);

                        if (equipmentInstance?.BoundCharacterId == null ||
                            equipmentInstance.BoundCharacterId == Session.Character.CharacterId ||
                            equipmentInstance.Item.ItemType != ItemType.Weapon &&
                            equipmentInstance.Item.ItemType != ItemType.Armor) return;

                        var perfumesNeeded =
                            ShellGeneratorHelper.Instance.PerfumeFromItemLevelAndShellRarity(
                                level: equipmentInstance.Item.LevelMinimum, rare: (byte)equipmentInstance.Rare);

                        if (Session.Character.Inventory.CountItem(itemVNum: perfumeVnum) < perfumesNeeded) return;

                        Session.Character.Inventory.RemoveItemAmount(vnum: perfumeVnum, amount: perfumesNeeded);

                        equipmentInstance.BoundCharacterId = Session.Character.CharacterId;

                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "BOUND_TO_YOU"), type: 0));
                    }
                }
                else if (guriPacket.Type == 300)
                {
                    if (guriPacket.Argument == 8023 &&
                        short.TryParse(s: guriPacket.User.ToString(), result: out var slot))
                    {
                        var box = Session.Character.Inventory.LoadBySlotAndType(slot: slot,
                            type: InventoryType.Equipment);
                        if (box != null)
                            box.Item.Use(session: Session, inv: ref box, option: 1,
                                packetsplit: new[] { guriPacket.Data.ToString() });
                    }
                }
                else if (guriPacket.Type == 199 && guriPacket.Argument == 2)
                {
                    if (Session.Character.IsSeal) return;
                    short[] listWingOfFriendship = { 2160, 2312, 10048 };
                    short vnumToUse = -1;
                    foreach (var vnum in listWingOfFriendship)
                        if (Session.Character.Inventory.CountItem(itemVNum: vnum) > 0)
                        {
                            vnumToUse = vnum;
                            break;
                        }

                    var isCouple = Session.Character.IsCoupleOfCharacter(characterId: guriPacket.User);
                    if (vnumToUse != -1 || isCouple)
                    {
                        var session = ServerManager.Instance.GetSessionByCharacterId(characterId: guriPacket.User);
                        if (session != null && !session.Character.IsChangingMapInstance)
                        {
                            if (Session.Character.IsFriendOfCharacter(characterId: guriPacket.User))
                            {
                                if (session.CurrentMapInstance?.MapInstanceType == MapInstanceType.BaseMapInstance)
                                {
                                    if (Session.Character.MapInstance.MapInstanceType
                                        != MapInstanceType.BaseMapInstance
                                        || ServerManager.Instance.ChannelId == 51
                                        && Session.Character.Faction != session.Character.Faction)
                                    {
                                        Session.SendPacket(
                                            packet: Session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(key: "CANT_USE_THAT"),
                                                type: 10));
                                        return;
                                    }

                                    var mapy = session.Character.PositionY;
                                    var mapx = session.Character.PositionX;
                                    var mapId = session.Character.MapInstance.Map.MapId;

                                    ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: mapId,
                                        mapX: mapx, mapY: mapy);
                                    if (!isCouple) Session.Character.Inventory.RemoveItemAmount(vnum: vnumToUse);
                                }
                                else
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "USER_ON_INSTANCEMAP"),
                                        type: 0));
                                }
                            }
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "USER_NOT_CONNECTED"), type: 0));
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "NO_WINGS"), type: 10));
                    }
                }
                else if (guriPacket.Type == 400)
                {
                    if (!Session.HasCurrentMapInstance) return;

                    var mapNpcId = guriPacket.Argument;

                    var npc = Session.CurrentMapInstance.Npcs.Find(match: n => n.MapNpcId.Equals(obj: mapNpcId));

                    if (npc != null && !npc.IsOut)
                    {
                        var mapobject = ServerManager.GetNpcMonster(npcVNum: npc.NpcVNum);

                        var delay = (int)Math.Round(
                            a: (3 + mapobject.RespawnTime / 1000d) * Session.Character.TimesUsed);
                        delay = delay > 11 ? 8 : delay;
                        if (npc.NpcVNum == 1346 || npc.NpcVNum == 1347 || npc.NpcVNum == 2350) delay = 0;
                        if (Session.Character.LastMapObject.AddSeconds(value: delay) < DateTime.Now)
                        {
                            if (mapobject.Drops.Any(predicate: s => s.MonsterVNum != null) && mapobject.VNumRequired >
                                                                                           10
                                                                                           && Session.Character
                                                                                               .Inventory
                                                                                               .CountItem(
                                                                                                   itemVNum: mapobject
                                                                                                       .VNumRequired)
                                                                                           < mapobject.AmountRequired)
                            {
                                if (ServerManager.GetItem(vnum: mapobject.VNumRequired) is { } requiredItem)
                                    Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: string.Format(
                                                format: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_ITEMS"),
                                                arg0: mapobject.AmountRequired, arg1: requiredItem.Name), type: 0));
                                return;
                            }

                            var random = new Random();
                            var randomAmount = ServerManager.RandomNumber() * random.NextDouble();
                            var drops = mapobject.Drops.Where(predicate: s => s.MonsterVNum == npc.NpcVNum).ToList();
                            if (drops.Count > 0)
                            {
                                var count = 0;
                                var probabilities = drops.Sum(selector: s => s.DropChance);
                                var rnd = ServerManager.RandomNumber(min: 0, max: probabilities);
                                var currentrnd = 0;
                                var firstDrop = mapobject.Drops.Find(match: s => s.MonsterVNum == npc.NpcVNum);

                                if (npc.NpcVNum == 2004 /* Ice Flower */ && firstDrop != null)
                                {
                                    var newInv = Session.Character.Inventory
                                        .AddNewToInventory(vnum: firstDrop.ItemVNum, amount: (short)firstDrop.Amount)
                                        .FirstOrDefault();

                                    if (newInv != null)
                                    {
                                        Session.Character.IncrementQuests(type: QuestType.Collect1,
                                            firstData: firstDrop.ItemVNum);
                                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                            message: string.Format(
                                                format: Language.Instance.GetMessageFromKey(key: "RECEIVED_ITEM"),
                                                arg0: $"{newInv.Item.Name} x {firstDrop.Amount}"), type: 0));
                                        Session.SendPacket(packet: Session.Character.GenerateSay(
                                            message: string.Format(
                                                format: Language.Instance.GetMessageFromKey(key: "RECEIVED_ITEM"),
                                                arg0: $"{newInv.Item.Name} x {firstDrop.Amount}"), type: 11));
                                    }
                                    else
                                    {
                                        Session.Character.GiftAdd(itemVNum: firstDrop.ItemVNum,
                                            amount: (short)firstDrop.Amount);
                                    }

                                    Session.CurrentMapInstance.Broadcast(packet: npc.GenerateOut());

                                    return;
                                }

                                if (randomAmount * 1000 <= probabilities)
                                    foreach (var drop in drops.OrderBy(keySelector: s => ServerManager.RandomNumber()))
                                    {
                                        var vnum = drop.ItemVNum;
                                        var amount = (short)drop.Amount;
                                        var dropChance = drop.DropChance;
                                        currentrnd += drop.DropChance;
                                        if (currentrnd >= rnd)
                                        {
                                            var newInv = Session.Character.Inventory
                                                .AddNewToInventory(vnum: vnum, amount: amount)
                                                .FirstOrDefault();
                                            if (newInv != null)
                                            {
                                                if (dropChance != 100000)
                                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                        message: string.Format(
                                                            format: Language.Instance.GetMessageFromKey(
                                                                key: "RECEIVED_ITEM"),
                                                            arg0: $"{newInv.Item.Name} x {amount}"), type: 0));
                                                Session.SendPacket(packet: Session.Character.GenerateSay(
                                                    message: string.Format(
                                                        format: Language.Instance.GetMessageFromKey(
                                                            key: "RECEIVED_ITEM"),
                                                        arg0: $"{newInv.Item.Name} x {amount}"), type: 11));
                                                Session.Character.IncrementQuests(type: QuestType.Collect1,
                                                    firstData: vnum);
                                            }
                                            else
                                            {
                                                Session.Character.GiftAdd(itemVNum: vnum, amount: amount);
                                            }

                                            count++;
                                            if (dropChance != 100000) break;
                                        }
                                    }

                                if (count > 0)
                                {
                                    Session.Character.LastMapObject = DateTime.Now;
                                    Session.Character.TimesUsed++;
                                    if (Session.Character.TimesUsed >= 4) Session.Character.TimesUsed = 0;

                                    if (mapobject.VNumRequired > 10)
                                        Session.Character.Inventory.RemoveItemAmount(vnum: npc.Npc.VNumRequired,
                                            amount: npc.Npc.AmountRequired);

                                    if (npc.NpcVNum == 1346 || npc.NpcVNum == 1347 || npc.NpcVNum == 2350)
                                    {
                                        npc.SetDeathStatement();
                                        npc.RunDeathEvent();
                                        Session.Character.MapInstance.Broadcast(packet: npc.GenerateOut());
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "TRY_FAILED"), type: 0));
                                }
                            }
                            else if (Session.CurrentMapInstance.Npcs.Where(predicate: s =>
                                    s.Npc.Race == 8 && s.Npc.RaceType == 5 && s.MapNpcId != npc.MapNpcId) is { }
                                mapTeleportNpcs)
                            {
                                if (npc.Npc.VNumRequired > 0 && npc.Npc.AmountRequired > 0)
                                {
                                    if (Session.Character.Inventory.CountItem(itemVNum: npc.Npc.VNumRequired) >=
                                        npc.Npc.AmountRequired)
                                    {
                                        if (npc.Npc.AmountRequired > 1)
                                            Session.Character.Inventory.RemoveItemAmount(vnum: npc.Npc.VNumRequired,
                                                amount: npc.Npc.AmountRequired);
                                    }
                                    else
                                    {
                                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "NO_ITEM_REQUIRED"),
                                            type: 0));
                                        return;
                                    }
                                }

                                if (DaoFactory.TeleporterDao.LoadFromNpc(npcId: npc.MapNpcId).FirstOrDefault() is { }
                                    teleport)
                                {
                                    Session.Character.PositionX = teleport.MapX;
                                    Session.Character.PositionY = teleport.MapY;
                                    Session.CurrentMapInstance.Broadcast(packet: Session.Character.GenerateTp());
                                    foreach (var mate in Session.Character.Mates.Where(predicate: s =>
                                        s.IsTeamMember && s.IsAlive)
                                    )
                                    {
                                        mate.PositionX = teleport.MapX;
                                        mate.PositionY = teleport.MapY;
                                        Session.CurrentMapInstance.Broadcast(packet: mate.GenerateTp());
                                    }
                                }
                                else
                                {
                                    MapNpc nearestTeleportNpc = null;
                                    foreach (var teleportNpc in mapTeleportNpcs)
                                        if (nearestTeleportNpc == null)
                                            nearestTeleportNpc = teleportNpc;
                                        else if (
                                            Map.GetDistance(
                                                p: new MapCell { X = npc.MapX, Y = npc.MapY },
                                                q: new MapCell { X = teleportNpc.MapX, Y = teleportNpc.MapY })
                                            <
                                            Map.GetDistance(
                                                p: new MapCell { X = npc.MapX, Y = npc.MapY },
                                                q: new MapCell
                                                { X = nearestTeleportNpc.MapX, Y = nearestTeleportNpc.MapY }))
                                            nearestTeleportNpc = teleportNpc;
                                    if (nearestTeleportNpc != null)
                                    {
                                        Session.Character.PositionX = nearestTeleportNpc.MapX;
                                        Session.Character.PositionY = nearestTeleportNpc.MapY;
                                        Session.CurrentMapInstance.Broadcast(packet: Session.Character.GenerateTp());
                                        foreach (var mate in Session.Character.Mates.Where(predicate: s =>
                                            s.IsTeamMember && s.IsAlive))
                                        {
                                            mate.PositionX = nearestTeleportNpc.MapX;
                                            mate.PositionY = nearestTeleportNpc.MapY;
                                            Session.CurrentMapInstance.Broadcast(packet: mate.GenerateTp());
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "TRY_FAILED_WAIT"),
                                    arg0: (int)(Session.Character.LastMapObject.AddSeconds(value: delay) -
                                                 DateTime.Now)
                                    .TotalSeconds), type: 0));
                        }
                    }
                }
                else if (guriPacket.Type == 1502)
                {
                    short relictVNum = 0;
                    if (guriPacket.Argument == 10000)
                        relictVNum = 1878;
                    else if (guriPacket.Argument == 30000) relictVNum = 1879;
                    if (relictVNum > 0 && Session.Character.Inventory.CountItem(itemVNum: relictVNum) > 0)
                    {
                        var roll = DaoFactory.RollGeneratedItemDao.LoadByItemVNum(vnum: relictVNum);
                        IEnumerable<RollGeneratedItemDto> rollGeneratedItemDtos =
                            roll as IList<RollGeneratedItemDto> ?? roll.ToList();
                        if (!rollGeneratedItemDtos.Any())
                        {
                            Logger.Warn(data: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "NO_HANDLER_RELICT"),
                                arg0: GetType(), arg1: relictVNum));
                            return;
                        }

                        var probabilities = rollGeneratedItemDtos.Sum(selector: s => s.Probability);
                        int rnd;
                        var rnd2 = ServerManager.RandomNumber(min: 0, max: probabilities);
                        var currentrnd = 0;
                        foreach (var rollitem in rollGeneratedItemDtos.OrderBy(keySelector: s =>
                            ServerManager.RandomNumber()))
                        {
                            sbyte rare = 0;
                            if (rollitem.IsRareRandom)
                            {
                                rnd = ServerManager.RandomNumber();

                                for (var j = 7; j >= 0; j--)
                                    if (rnd < ItemHelper.RareRate[j])
                                    {
                                        rare = (sbyte)j;
                                        break;
                                    }

                                if (rare < 1) rare = 1;
                            }

                            if (rollitem.Probability == 10000)
                            {
                                Session.Character.GiftAdd(itemVNum: rollitem.ItemGeneratedVNum,
                                    amount: rollitem.ItemGeneratedAmount,
                                    rare: (byte)rare, design: rollitem.ItemGeneratedDesign);
                                continue;
                            }

                            currentrnd += rollitem.Probability;
                            if (currentrnd < rnd2) continue;
                            Session.Character.GiftAdd(itemVNum: rollitem.ItemGeneratedVNum,
                                amount: rollitem.ItemGeneratedAmount,
                                rare: (byte)rare,
                                design: rollitem.ItemGeneratedDesign); //, rollitem.ItemGeneratedUpgrade);
                            break;
                        }

                        Session.Character.Inventory.RemoveItemAmount(vnum: relictVNum);
                        Session.Character.Gold -= guriPacket.Argument;
                        Session.Character.GenerateGold();
                        Session.SendPacket(packet: "shop_end 1");
                    }
                }
                else if (guriPacket.Type == 501)
                {
                    if (ServerManager.Instance.IceBreakerInWaiting && IceBreaker.Map.Sessions.Count() <
                                                                   IceBreaker.MaxAllowedPlayers
                                                                   && Session.Character.MapInstance.MapInstanceType ==
                                                                   MapInstanceType.BaseMapInstance &&
                                                                   Session.Character.Group?.Raid == null)
                    {
                        if (Session.Character.Gold >= 500)
                        {
                            Session.Character.Gold -= 500;
                            Session.SendPacket(packet: Session.Character.GenerateGold());
                            Session.Character.RemoveVehicle();
                            ServerManager.Instance.TeleportOnRandomPlaceInMap(session: Session,
                                guid: IceBreaker.Map.MapInstanceId);
                            var newIceTeam = new ConcurrentBag<ClientSession>();
                            newIceTeam.Add(item: Session);
                            IceBreaker.IceBreakerTeams.Add(item: newIceTeam);
                        }
                        else
                        {
                            Session.SendPacket(packet: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"));
                        }
                    }
                }
                else if (guriPacket.Type == 502)
                {
                    long? targetId = guriPacket.User;

                    var target = ServerManager.Instance.GetSessionByCharacterId(characterId: targetId.Value);

                    if (target?.Character?.MapInstance == null) return;

                    if (target.Character.MapInstance.MapInstanceType == MapInstanceType.IceBreakerInstance)
                    {
                        if (target.Character.LastPvPKiller == null
                            || target.Character.LastPvPKiller != Session)
                        {
                            IceBreaker.FrozenPlayers.Remove(item: target);
                            IceBreaker.AlreadyFrozenPlayers.Add(item: target);
                            target.Character.NoMove = false;
                            target.Character.NoAttack = false;
                            target.SendPacket(packet: target.Character.GenerateCond());
                            target.Character.MapInstance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_PLAYER_UNFROZEN"),
                                    arg0: target.Character.Name), type: 0));

                            if (!(IceBreaker.IceBreakerTeams.FirstOrDefault(predicate: s =>
                                    s.Contains(value: Session)) ?? throw new InvalidOperationException())
                                .Contains(value: target))
                            {
                                IceBreaker.IceBreakerTeams.Remove(
                                    item: IceBreaker.IceBreakerTeams.FirstOrDefault(predicate: s =>
                                        s.Contains(value: target)));
                                IceBreaker.IceBreakerTeams.FirstOrDefault(predicate: s => s.Contains(value: Session))
                                    ?.Add(item: target);
                            }
                        }
                    }
                    else
                    {
                        target.Character.RemoveBuff(cardId: 569);
                    }
                }
                else if (guriPacket.Type == 506)
                {
                    Session.Character.IsWaitingForEvent |= ServerManager.Instance.EventInWaiting;
                }
                else if (guriPacket.Type == 513)
                {
                    if (Session?.Character?.MapInstance == null) return;

                    if (Session.Character.IsLaurenaMorph())
                    {
                        Session.Character.MapInstance.Broadcast(packet: Session.Character.GenerateEff(effectid: 4054));
                        Session.Character.ClearLaurena();
                    }
                }
                else if (guriPacket.Type == 710)
                {
                    if (guriPacket.Value != null)
                    {
                        // TODO: MAP TELEPORTER
                    }
                }
                else if (guriPacket.Type == 711)
                {
                    var tp = Session.CurrentMapInstance.Npcs
                        .FirstOrDefault(predicate: n =>
                            n.Teleporters.Any(predicate: t => t?.TeleporterId == guriPacket.Argument))
                        ?.Teleporters.FirstOrDefault(predicate: t => t?.Type == TeleporterType.TeleportOnOtherMap);
                    if (tp == null) return;
                    ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: tp.MapId, mapX: tp.MapX,
                        mapY: tp.MapY);
                }
                else if (guriPacket.Type == 750)
                {
                    const short baseVnum = 1623;
                    if (ServerManager.Instance.ChannelId == 51)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "CHANGE_NOT_PERMITTED_ACT4"),
                                type: 0));
                        return;
                    }

                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.Act4ShipAngel
                        || Session.CurrentMapInstance.MapInstanceType == MapInstanceType.Act4ShipDemon)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "CHANGE_NOT_PERMITTED_ACT4SHIP"),
                                type: 0));
                        return;
                    }

                    if (Enum.TryParse(value: guriPacket.Argument.ToString(), result: out FactionType faction)
                        && Session.Character.Inventory.CountItem(itemVNum: baseVnum + (byte)faction) > 0)
                    {
                        if ((byte)faction < 3) // Single family change
                        {
                            if (Session.Character.Faction == faction) return;
                            if (Session.Character.Family != null)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "IN_FAMILY"),
                                        type: 0));
                                return;
                            }

                            Session.Character.Inventory.RemoveItemAmount(vnum: baseVnum + (byte)faction);
                            Session.Character.ChangeFaction(faction: faction);
                        }
                        else // Family faction change
                        {
                            faction -= 2;
                            if ((FactionType)Session.Character.Family.FamilyFaction == faction) return;
                            if (Session.Character.Family == null)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY"),
                                        type: 0));
                                return;
                            }

                            if (Session.Character.FamilyCharacter.Authority != FamilyAuthority.Head)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_HEAD"),
                                        type: 0));
                                return;
                            }

                            if (Session.Character.Family.LastFactionChange > DateTime.Now.AddDays(value: -1).Ticks)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "CHANGE_NOT_PERMITTED"),
                                    type: 0));
                                return;
                            }

                            Session.Character.Inventory.RemoveItemAmount(vnum: baseVnum + (byte)faction + 2);
                            Session.Character.Family.ChangeFaction(faction: (byte)faction, session: Session);
                        }
                    }
                }
                else if (guriPacket.Type == 2)
                {
                    Session.CurrentMapInstance?.Broadcast(
                        packet: UserInterfaceHelper.GenerateGuri(type: 2, argument: 1,
                            callerId: Session.Character.CharacterId),
                        xRangeCoordinate: Session.Character.PositionX, yRangeCoordinate: Session.Character.PositionY);
                }
                else if (guriPacket.Type == 4)
                {
                    const int speakerVNum = 2173;
                    const int limitedSpeakerVNum = 10028;
                    if (guriPacket.Argument == 1)
                    {
                        if (guriPacket.Value.Contains(value: "^"))
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "INVALID_NAME")));
                            return;
                        }

                        short[] listPetnameVNum = { 2157, 10023 };
                        short vnumToUse = -1;
                        foreach (var vnum in listPetnameVNum)
                            if (Session.Character.Inventory.CountItem(itemVNum: vnum) > 0)
                                vnumToUse = vnum;
                        var mate = Session.Character.Mates.Find(match: s => s.MateTransportId == guriPacket.Data);
                        if (mate != null && Session.Character.Inventory.CountItem(itemVNum: vnumToUse) > 0)
                        {
                            mate.Name = guriPacket.Value.Truncate(length: 16);
                            Session.CurrentMapInstance?.Broadcast(packet: mate.GenerateOut(),
                                receiver: ReceiverType.AllExceptMe);
                            if (Session.CurrentMapInstance != null)
                                Parallel.ForEach(
                                    source: Session.CurrentMapInstance.Sessions.Where(predicate: s =>
                                        s.Character != null),
                                    body: s =>
                                    {
                                        if (ServerManager.Instance.ChannelId != 51 ||
                                            Session.Character.Faction == s.Character.Faction)
                                            s.SendPacket(packet: mate.GenerateIn(hideNickname: false,
                                                isAct4: ServerManager.Instance.ChannelId == 51));
                                        else
                                            s.SendPacket(packet: mate.GenerateIn(hideNickname: true,
                                                isAct4: ServerManager.Instance.ChannelId == 51,
                                                receiverAuthority: s.Account.Authority));
                                    });
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "NEW_NAME_PET")));
                            Session.SendPacket(packet: Session.Character.GeneratePinit());
                            Session.SendPackets(packets: Session.Character.GeneratePst());
                            Session.SendPackets(packets: Session.Character.GenerateScP());
                            Session.Character.Inventory.RemoveItemAmount(vnum: vnumToUse);
                        }
                    }

                    // presentation message
                    if (guriPacket.Argument == 2)
                    {
                        var presentationVNum = Session.Character.Inventory.CountItem(itemVNum: 1117) > 0
                            ? 1117
                            : Session.Character.Inventory.CountItem(itemVNum: 9013) > 0
                                ? 9013
                                : -1;
                        if (presentationVNum != -1)
                        {
                            var message = "";
                            var valuesplit = guriPacket.Value?.Split(' ');
                            if (valuesplit == null) return;

                            for (var i = 0; i < valuesplit.Length; i++) message += valuesplit[i] + "^";

                            message = message.Substring(startIndex: 0, length: message.Length - 1); // Remove the last ^
                            message = message.Trim();
                            if (message.Length > 60) message = message.Substring(startIndex: 0, length: 60);

                            Session.Character.Biography = message;
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "INTRODUCTION_SET"),
                                    type: 10));
                            Session.Character.Inventory.RemoveItemAmount(vnum: presentationVNum);
                        }
                    }

                    // Speaker
                    if (guriPacket.Argument == 3 && (Session.Character.Inventory.CountItem(itemVNum: speakerVNum) > 0 ||
                                                     Session.Character.Inventory.CountItem(
                                                         itemVNum: limitedSpeakerVNum) > 0))
                    {
                        string sayPacket;
                        var message =
                            $"<{Language.Instance.GetMessageFromKey(key: "SPEAKER")}> [{Session.Character.Name}]:";
                        byte sayItemInventory = 0;
                        short sayItemSlot = 0;
                        var baseLength = message.Length;
                        var valuesplit = guriPacket.Value?.Split(' ');
                        if (valuesplit == null) return;

                        if (guriPacket.Data == 999 && (valuesplit.Length < 3 ||
                                                       !byte.TryParse(s: valuesplit[0], result: out sayItemInventory) ||
                                                       !short.TryParse(s: valuesplit[1], result: out sayItemSlot)))
                            return;

                        for (var i = 0 + (guriPacket.Data == 999 ? 2 : 0); i < valuesplit.Length; i++)
                            message += valuesplit[i] + " ";

                        if (message.Length > 120 + baseLength)
                            message = message.Substring(startIndex: 0, length: 120 + baseLength);

                        message = message.Trim();

                        if (guriPacket.Data == 999)
                            sayPacket = Session.Character.GenerateSayItem(message: message, type: 13,
                                itemInventory: sayItemInventory, itemSlot: sayItemSlot);
                        else
                            sayPacket = Session.Character.GenerateSay(message: message, type: 13);

                        if (Session.Character.IsMuted())
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "SPEAKER_CANT_BE_USED"),
                                    type: 10));
                            return;
                        }

                        if (Session.Character.Inventory.CountItem(itemVNum: limitedSpeakerVNum) > 0)
                            Session.Character.Inventory.RemoveItemAmount(vnum: limitedSpeakerVNum);
                        else
                            Session.Character.Inventory.RemoveItemAmount(vnum: speakerVNum);

                        if (ServerManager.Instance.ChannelId == 51)
                        {
                            ServerManager.Instance.Broadcast(client: Session, content: sayPacket,
                                receiver: ReceiverType.AllExceptMeAct4);
                            Session.SendPacket(packet: sayPacket);
                        }
                        else
                        {
                            ServerManager.Instance.Broadcast(client: Session, content: sayPacket);
                        }

                        if (ServerManager.Instance.Configuration.UseLogService)
                            LogServiceClient.Instance.LogChatMessage(logEntry: new LogEntry
                            {
                                Sender = Session.Character.Name,
                                SenderId = Session.Character.CharacterId,
                                Receiver = null,
                                ReceiverId = null,
                                MessageType = LogType.Speaker,
                                Message = message
                            });
                    }

                    // Bubble

                    if (guriPacket.Argument == 4)
                    {
                        var bubbleVNum = Session.Character.Inventory.CountItem(itemVNum: 2174) > 0
                            ? 2174
                            : Session.Character.Inventory.CountItem(itemVNum: 10029) > 0
                                ? 10029
                                : -1;
                        if (bubbleVNum != -1)
                        {
                            var message = "";
                            var valuesplit = guriPacket.Value?.Split(' ');
                            if (valuesplit == null) return;

                            for (var i = 0; i < valuesplit.Length; i++) message += valuesplit[i] + "^";

                            message = message.Substring(startIndex: 0, length: message.Length - 1); // Remove the last ^
                            message = message.Trim();
                            if (message.Length > 60) message = message.Substring(startIndex: 0, length: 60);

                            Session.Character.BubbleMessage = message;
                            Session.Character.BubbleMessageEnd = DateTime.Now.AddMinutes(value: 30);
                            Session.SendPacket(packet: $"csp_r {Session.Character.BubbleMessage}");
                            Session.Character.Inventory.RemoveItemAmount(vnum: bubbleVNum);
                        }
                    }
                }
                else if (guriPacket.Type == 199 && guriPacket.Argument == 1)
                {
                    if (Session.Character.IsSeal) return;
                    short[] listWingOfFriendship = { 2160, 2312, 10048 };
                    short vnumToUse = -1;
                    foreach (var vnum in listWingOfFriendship)
                        if (Session.Character.Inventory.CountItem(itemVNum: vnum) > 0)
                            vnumToUse = vnum;
                    var isCouple = Session.Character.IsCoupleOfCharacter(characterId: guriPacket.User);
                    if (vnumToUse != -1 || isCouple)
                    {
                        var session = ServerManager.Instance.GetSessionByCharacterId(characterId: guriPacket.User);
                        if (session != null)
                        {
                            if (Session.Character.IsFriendOfCharacter(characterId: guriPacket.User))
                            {
                                if (session.CurrentMapInstance?.MapInstanceType == MapInstanceType.BaseMapInstance)
                                {
                                    if (Session.Character.MapInstance.MapInstanceType
                                        != MapInstanceType.BaseMapInstance
                                        || ServerManager.Instance.ChannelId == 51
                                        && Session.Character.Faction != session.Character.Faction)
                                    {
                                        Session.SendPacket(
                                            packet: Session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(key: "CANT_USE_THAT"),
                                                type: 10));
                                        return;
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "USER_ON_INSTANCEMAP"),
                                        type: 0));
                                    return;
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "USER_NOT_CONNECTED"), type: 0));
                            return;
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "NO_WINGS"), type: 10));
                        return;
                    }

                    if (!Session.Character.IsFriendOfCharacter(characterId: guriPacket.User))
                    {
                        Session.SendPacket(
                            packet: Language.Instance.GetMessageFromKey(key: "CHARACTER_NOT_IN_FRIENDLIST"));
                        return;
                    }

                    Session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 3000, type: 7,
                        argument: $"#guri^199^2^{guriPacket.User}"));
                }
                else if (guriPacket.Type == 201)
                {
                    if (Session.Character.StaticBonusList.Any(predicate: s =>
                        s.StaticBonusType == StaticBonusType.PetBasket))
                        Session.SendPacket(packet: Session.Character.GenerateStashAll());
                }
                else if (guriPacket.Type == 202)
                {
                    //Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("PARTNER_BACKPACK"), 10));
                    Session.SendPacket(packet: Session.Character.GeneratePStashAll());
                }
                else if (guriPacket.Type == 208 && guriPacket.Argument == 0)
                {
                    if (short.TryParse(s: guriPacket.Value, result: out var mountSlot)
                        && short.TryParse(s: guriPacket.User.ToString(), result: out var pearlSlot))
                    {
                        var mount = Session.Character.Inventory.LoadBySlotAndType(slot: mountSlot,
                            type: InventoryType.Main);
                        var pearl = Session.Character.Inventory.LoadBySlotAndType(slot: pearlSlot,
                            type: InventoryType.Equipment);

                        if (mount?.Item == null || pearl?.Item == null) return;

                        if (!pearl.Item.IsHolder) return;

                        if (pearl.HoldingVNum > 0) return;

                        if (pearl.Item.ItemType == ItemType.Box && pearl.Item.ItemSubType == 4)
                        {
                            if (mount.Item.ItemType != ItemType.Special || mount.Item.ItemSubType != 0 ||
                                mount.Item.Speed < 1) return;

                            Session.Character.Inventory.RemoveItemFromInventory(id: mount.Id);

                            pearl.HoldingVNum = mount.ItemVNum;

                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "MOUNT_SAVED"), type: 0));
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "MOUNT_SAVED"), type: 10));
                        }
                    }
                }
                else if (guriPacket.Type == 209 && guriPacket.Argument == 0)
                {
                    if (short.TryParse(s: guriPacket.Value, result: out var fairySlot)
                        && short.TryParse(s: guriPacket.User.ToString(), result: out var pearlSlot))
                    {
                        var fairy = Session.Character.Inventory.LoadBySlotAndType(slot: fairySlot,
                            type: InventoryType.Equipment);
                        var pearl = Session.Character.Inventory.LoadBySlotAndType(slot: pearlSlot,
                            type: InventoryType.Equipment);

                        if (fairy?.Item == null || pearl?.Item == null) return;

                        if (!pearl.Item.IsHolder) return;

                        if (pearl.HoldingVNum > 0) return;

                        if (pearl.Item.ItemType == ItemType.Box && pearl.Item.ItemSubType == 5)
                        {
                            if (fairy.Item.ItemType != ItemType.Jewelery || fairy.Item.ItemSubType != 3 ||
                                fairy.Item.IsDroppable) return;

                            Session.Character.Inventory.RemoveItemFromInventory(id: fairy.Id);

                            pearl.HoldingVNum = fairy.ItemVNum;
                            pearl.ElementRate = fairy.ElementRate;

                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "FAIRY_SAVED"), type: 0));
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "FAIRY_SAVED"), type: 10));
                        }
                    }
                }
                else if (guriPacket.Type == 203 && guriPacket.Argument == 0)
                {
                    // SP points initialization
                    int[] listPotionResetVNums = { 1366, 1427, 5115, 9040 };
                    var vnumToUse = -1;
                    foreach (var vnum in listPotionResetVNums)
                        if (Session.Character.Inventory.CountItem(itemVNum: vnum) > 0)
                            vnumToUse = vnum;

                    if (vnumToUse != -1)
                    {
                        if (Session.Character.UseSp)
                        {
                            var specialistInstance =
                                Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                                    type: InventoryType.Wear);
                            if (specialistInstance != null)
                            {
                                specialistInstance.SlDamage = 0;
                                specialistInstance.SlDefence = 0;
                                specialistInstance.SlElement = 0;
                                specialistInstance.SlHp = 0;

                                specialistInstance.DamageMinimum = 0;
                                specialistInstance.DamageMaximum = 0;
                                specialistInstance.HitRate = 0;
                                specialistInstance.CriticalLuckRate = 0;
                                specialistInstance.CriticalRate = 0;
                                specialistInstance.DefenceDodge = 0;
                                specialistInstance.DistanceDefenceDodge = 0;
                                specialistInstance.ElementRate = 0;
                                specialistInstance.DarkResistance = 0;
                                specialistInstance.LightResistance = 0;
                                specialistInstance.FireResistance = 0;
                                specialistInstance.WaterResistance = 0;
                                specialistInstance.CriticalDodge = 0;
                                specialistInstance.CloseDefence = 0;
                                specialistInstance.DistanceDefence = 0;
                                specialistInstance.MagicDefence = 0;
                                specialistInstance.Hp = 0;
                                specialistInstance.Mp = 0;

                                Session.Character.Inventory.RemoveItemAmount(vnum: vnumToUse);
                                Session.Character.Inventory.DeleteFromSlotAndType(slot: (byte)EquipmentType.Sp,
                                    type: InventoryType.Wear);
                                Session.Character.Inventory.AddToInventoryWithSlotAndType(
                                    itemInstance: specialistInstance,
                                    type: InventoryType.Wear, slot: (byte)EquipmentType.Sp);
                                Session.SendPacket(packet: Session.Character.GenerateCond());
                                Session.SendPacket(packet: specialistInstance.GenerateSlInfo(session: Session));
                                Session.SendPacket(packet: Session.Character.GenerateLev());
                                Session.SendPackets(packets: Session.Character.GenerateStatChar());
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "POINTS_RESET"),
                                        type: 0));
                            }
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "TRANSFORMATION_NEEDED"),
                                    type: 10));
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_POINTS"),
                                type: 10));
                    }
                }
            }
        }

        /// <summary>
        ///     hero packet
        /// </summary>
        /// <param name="heroPacket"></param>
        public void Hero(HeroPacket heroPacket)
        {
            if (!string.IsNullOrEmpty(value: heroPacket.Message))
            {
                if (Session.Character.IsReputationHero() >= 3 && Session.Character.Reputation > 5000000)
                {
                    heroPacket.Message = heroPacket.Message.Trim();
                    ServerManager.Instance.Broadcast(client: Session,
                        content: $"msg 5 [{Session.Character.Name}]:{heroPacket.Message}",
                        receiver: ReceiverType.AllNoHeroBlocked);
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_HERO"), type: 11));
                }
            }
        }

        /// <summary>
        ///     PreqPacket packet
        /// </summary>
        /// <param name="packet"></param>
        public void Preq(PreqPacket packet)
        {
            if (Session.Character.IsSeal) return;

            var currentRunningSeconds =
                (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(value: -50)).TotalSeconds;

            var timeSpanSinceLastPortal = currentRunningSeconds - Session.Character.LastPortal;

            if (Session.Account?.Authority != AuthorityType.Administrator
                && (timeSpanSinceLastPortal < 4 || !Session.HasCurrentMapInstance))
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_MOVE"), type: 10));
                return;
            }

            if (Session.CurrentMapInstance.Portals.Concat(second: Session.Character.GetExtraPortal())
                .FirstOrDefault(predicate: s => Session.Character.PositionY >= s.SourceY - 1
                                                && Session.Character.PositionY <= s.SourceY + 1
                                                && Session.Character.PositionX >= s.SourceX - 1
                                                && Session.Character.PositionX <= s.SourceX + 1) is { } portal)
            {
                switch (portal.Type)
                {
                    case (sbyte)PortalType.MapPortal:
                    case (sbyte)PortalType.TsNormal:
                    case (sbyte)PortalType.Open:
                    case (sbyte)PortalType.Miniland:
                    case (sbyte)PortalType.TsEnd:
                        if (portal.DestinationMapId == 232 && Session.Character.HeroLevel < 24) //PortalCap
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "YOU_NEED_HERO_LEVEL_24"),
                                    type: 11));
                            return;
                        }

                        break;
                    case (sbyte)PortalType.Exit:
                    case (sbyte)PortalType.Effect:
                    case (sbyte)PortalType.ShopTeleport:
                        break;

                    case (sbyte)PortalType.Raid:
                        if (Session.Character.Group?.Raid != null)
                        {
                            if (Session.Character.Group.IsLeader(session: Session))
                                Session.SendPacket(
                                    packet:
                                    $"qna #mkraid^0^275 {Language.Instance.GetMessageFromKey(key: "RAID_START_QUESTION")}");
                            else
                                Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_TEAM_LEADER"),
                                        type: 10));
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NEED_TEAM"), type: 10));
                        }

                        return;

                    case (sbyte)PortalType.BlueRaid:
                    case (sbyte)PortalType.DarkRaid:
                        if (!packet.Parameter.HasValue)
                        {
                            Session.SendPacket(
                                packet:
                                $"qna #preq^1 {string.Format(format: Language.Instance.GetMessageFromKey(key: "ACT4_RAID_ENTER"), arg0: Session.Character.Level * 5)}");
                            return;
                        }
                        else
                        {
                            if (packet.Parameter == 1)
                            {
                                if ((int)Session.Character.Faction == portal.Type - 9 &&
                                    Session.Character.Family?.Act4Raid != null)
                                {
                                    if (Session.Character.Level > 49)
                                    {
                                        if (Session.Character.Reputation > 10000)
                                        {
                                            Session.Character.GetReputation(amount: Session.Character.Level * -5);

                                            Session.Character.LastPortal = currentRunningSeconds;

                                            switch (Session.Character.Family.Act4Raid.MapInstanceType)
                                            {
                                                case MapInstanceType.Act4Morcos:
                                                    ServerManager.Instance.ChangeMapInstance(
                                                        characterId: Session.Character.CharacterId,
                                                        mapInstanceId: Session.Character.Family.Act4Raid.MapInstanceId,
                                                        mapX: 43, mapY: 179);
                                                    break;

                                                case MapInstanceType.Act4Hatus:
                                                    ServerManager.Instance.ChangeMapInstance(
                                                        characterId: Session.Character.CharacterId,
                                                        mapInstanceId: Session.Character.Family.Act4Raid.MapInstanceId,
                                                        mapX: 15, mapY: 9);
                                                    break;

                                                case MapInstanceType.Act4Calvina:
                                                    ServerManager.Instance.ChangeMapInstance(
                                                        characterId: Session.Character.CharacterId,
                                                        mapInstanceId: Session.Character.Family.Act4Raid.MapInstanceId,
                                                        mapX: 24, mapY: 6);
                                                    break;

                                                case MapInstanceType.Act4Berios:
                                                    ServerManager.Instance.ChangeMapInstance(
                                                        characterId: Session.Character.CharacterId,
                                                        mapInstanceId: Session.Character.Family.Act4Raid.MapInstanceId,
                                                        mapX: 20, mapY: 20);
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            Session.SendPacket(
                                                packet: Session.Character.GenerateSay(
                                                    message: Language.Instance.GetMessageFromKey(key: "LOW_REP"),
                                                    type: 10));
                                        }
                                    }
                                    else
                                    {
                                        Session.SendPacket(
                                            packet: Session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(key: "LOW_LVL"),
                                                type: 10));
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(
                                        packet: Session.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(key: "PORTAL_BLOCKED"),
                                            type: 10));
                                }
                            }
                        }

                        return;

                    default:
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "PORTAL_BLOCKED"), type: 10));
                        return;
                }

                if (Session?.CurrentMapInstance?.MapInstanceType == MapInstanceType.TimeSpaceInstance
                    && Session?.Character?.Timespace != null && !Session.Character.Timespace.InstanceBag.Lock)
                {
                    if (Session.Character.CharacterId == Session.Character.Timespace.InstanceBag.CreatorId)
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                            dialog:
                            $"#rstart^1 rstart {Language.Instance.GetMessageFromKey(key: "FIRST_ROOM_START")}"));

                    return;
                }

                if (Session?.CurrentMapInstance?.MapInstanceType != MapInstanceType.BaseMapInstance &&
                    portal.DestinationMapId == 134)
                    if (!packet.Parameter.HasValue)
                    {
                        Session.SendPacket(
                            packet: $"qna #preq^1 {Language.Instance.GetMessageFromKey(key: "ACT4_RAID_EXIT")}");
                        return;
                    }

                portal.OnTraversalEvents.ForEach(action: e => EventHelper.Instance.RunEvent(evt: e));
                if (portal.DestinationMapInstanceId == default) return;

                if (ServerManager.Instance.ChannelId == 51)
                {
                    if (Session.Character.Faction == FactionType.Angel && portal.DestinationMapId == 131
                        || Session.Character.Faction == FactionType.Demon && portal.DestinationMapId == 130)
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "PORTAL_BLOCKED"), type: 10));
                        return;
                    }

                    if ((portal.DestinationMapId == 130 || portal.DestinationMapId == 131)
                        && timeSpanSinceLastPortal < 15)
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "CANT_MOVE"), type: 10));
                        return;
                    }
                }

                Session.SendPacket(packet: Session.CurrentMapInstance.GenerateRsfn());

                if (ServerManager.GetMapInstance(id: portal.SourceMapInstanceId).MapInstanceType
                    != MapInstanceType.BaseMapInstance
                    && ServerManager.GetMapInstance(id: portal.SourceMapInstanceId).MapInstanceType
                    != MapInstanceType.CaligorInstance
                    && ServerManager.GetMapInstance(id: portal.DestinationMapInstanceId).MapInstanceType
                    == MapInstanceType.BaseMapInstance)
                {
                    ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: Session.Character.MapId,
                        mapX: Session.Character.MapX, mapY: Session.Character.MapY);
                }
                else if (portal.DestinationMapInstanceId == Session.Character.Miniland.MapInstanceId)
                {
                    ServerManager.Instance.JoinMiniland(session: Session, minilandOwner: Session);
                }
                else if (portal.DestinationMapId == 20000)
                {
                    var sess = ServerManager.Instance.Sessions.FirstOrDefault(predicate: s =>
                        s.Character.Miniland.MapInstanceId == portal.DestinationMapInstanceId);
                    if (sess != null) ServerManager.Instance.JoinMiniland(session: Session, minilandOwner: sess);
                }
                else
                {
                    if (ServerManager.Instance.ChannelId == 51)
                    {
                        var destinationX = portal.DestinationX;
                        var destinationY = portal.DestinationY;

                        if (portal.DestinationMapInstanceId == CaligorRaid.CaligorMapInstance?.MapInstanceId
                        ) /* Caligor Raid Map */
                        {
                            if (!packet.Parameter.HasValue)
                            {
                                Session.SendPacket(
                                    packet:
                                    $"qna #preq^1 {Language.Instance.GetMessageFromKey(key: "CALIGOR_RAID_ENTER")}");
                                return;
                            }
                        }
                        else if (portal.DestinationMapId == 153) /* Unknown Land */
                        {
                            if (Session.Character.MapInstance == CaligorRaid.CaligorMapInstance &&
                                !packet.Parameter.HasValue)
                            {
                                Session.SendPacket(
                                    packet:
                                    $"qna #preq^1 {Language.Instance.GetMessageFromKey(key: "CALIGOR_RAID_EXIT")}");
                                return;
                            }

                            if (Session.Character.MapInstance != CaligorRaid.CaligorMapInstance)
                                if (destinationX <= 0 && destinationY <= 0)
                                    switch (Session.Character.Faction)
                                    {
                                        case FactionType.Angel:
                                            destinationX = 50;
                                            destinationY = 172;
                                            break;
                                        case FactionType.Demon:
                                            destinationX = 130;
                                            destinationY = 172;
                                            break;
                                    }
                        }

                        ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                            mapInstanceId: portal.DestinationMapInstanceId, mapX: destinationX, mapY: destinationY);
                    }
                    else
                    {
                        ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                            mapInstanceId: portal.DestinationMapInstanceId, mapX: portal.DestinationX,
                            mapY: portal.DestinationY);
                    }
                }

                Session.Character.LastPortal = currentRunningSeconds;
            }
        }

        /// <summary>
        ///     pulse packet
        /// </summary>
        /// <param name="pulsepacket"></param>
        public void Pulse(PulsePacket pulsepacket)
        {
            if (pulsepacket == null) throw new ArgumentNullException(paramName: nameof(pulsepacket));
            if (Session.Character.LastPulse.AddMilliseconds(value: 80000) >= DateTime.Now
                && DateTime.Now >= Session.Character.LastPulse.AddMilliseconds(value: 40000))
                Session.Character.LastPulse = DateTime.Now;
            else
                Session.Disconnect();

            Session.Character.MuteMessage();
            Session.Character.DeleteTimeout();
            CommunicationServiceClient.Instance.PulseAccount(accountId: Session.Account.AccountId);
        }

        /// <summary>
        ///     rlPacket packet
        /// </summary>
        /// <param name="rlPacket"></param>
        public void RaidListRegister(RlPacket rlPacket)
        {
            switch (rlPacket.Type)
            {
                case 0: // Show the Raid List
                    if (Session.Character.Group?.IsLeader(session: Session) == true
                        && Session.Character.Group.GroupType != GroupType.Group
                        && ServerManager.Instance.GroupList.Any(predicate: s =>
                            s.GroupId == Session.Character.Group.GroupId))
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateRl(type: 1));
                    else if (Session.Character.Group != null && Session.Character.Group.GroupType != GroupType.Group
                                                             && Session.Character.Group.IsLeader(session: Session))
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateRl(type: 2));
                    else if (Session.Character.Group != null)
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateRl(type: 3));
                    else
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateRl(type: 0));

                    break;

                case 1: // Register a team
                    if (Session.Character.Group != null
                        && Session.Character.Group.GroupType != GroupType.Group
                        && Session.Character.Group.IsLeader(session: Session)
                        && !ServerManager.Instance.GroupList.Any(predicate: s =>
                            s.GroupId == Session.Character.Group.GroupId))
                    {
                        ServerManager.Instance.GroupList.Add(item: Session.Character.Group);
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateRl(type: 1));
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "RAID_REGISTERED")));
                        ServerManager.Instance.Broadcast(client: Session,
                            content:
                            $"qnaml 100 #rl {string.Format(format: Language.Instance.GetMessageFromKey(key: "SEARCH_TEAM_MEMBERS"), arg0: Session.Character.Name, arg1: Session.Character.Group.Raid?.Label)}",
                            receiver: ReceiverType.AllExceptGroup);
                    }

                    break;

                case 2: // Cancel the team registration
                    if (Session.Character.Group != null
                        && Session.Character.Group.GroupType != GroupType.Group
                        && Session.Character.Group.IsLeader(session: Session)
                        && ServerManager.Instance.GroupList.Any(predicate: s =>
                            s.GroupId == Session.Character.Group.GroupId))
                    {
                        ServerManager.Instance.GroupList.Remove(item: Session.Character.Group);
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateRl(type: 2));
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "RAID_UNREGISTERED")));
                    }

                    break;

                case 3: // Become a team member
                    var targetSession = ServerManager.Instance.GetSessionByCharacterName(name: rlPacket.CharacterName);

                    if (targetSession?.Character?.Group == null) return;

                    if (targetSession.Character.CharacterId == Session.Character.CharacterId) return;

                    if (!targetSession.Character.Group.IsLeader(session: targetSession)) return;

                    if (!ServerManager.Instance.GroupList.Any(predicate: group =>
                        group.GroupId == targetSession.Character.Group.GroupId)) return;

                    targetSession.Character.GroupSentRequestCharacterIds.Add(value: Session.Character.CharacterId);

                    GroupJoin(pjoinPacket: new PJoinPacket
                    {
                        RequestType = GroupRequestType.Accepted,
                        CharacterId = targetSession.Character.CharacterId
                    });

                    break;
            }
        }

        /// <summary>
        ///     rdPacket packet
        /// </summary>
        /// <param name="rdPacket"></param>
        public void RaidManage(RdPacket rdPacket)
        {
            Group grp;
            switch (rdPacket.Type)
            {
                // Join Raid
                case 1:
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.RaidInstance) return;

                    var target = ServerManager.Instance.GetSessionByCharacterId(characterId: rdPacket.CharacterId);
                    if (rdPacket.Parameter == null && target?.Character?.Group == null &&
                        Session.Character.Group != null && Session.Character.Group.IsLeader(session: Session) &&
                        Session.Character.Group?.Sessions.FirstOrDefault() == Session)
                        GroupJoin(pjoinPacket: new PJoinPacket
                        {
                            RequestType = GroupRequestType.Invited,
                            CharacterId = rdPacket.CharacterId
                        });
                    else if (Session.Character.Group == null)
                        GroupJoin(pjoinPacket: new PJoinPacket
                        {
                            RequestType = GroupRequestType.Accepted,
                            CharacterId = rdPacket.CharacterId
                        });

                    break;

                // Leave Raid
                case 2:
                    if (Session.Character.Group == null) return;

                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "LEFT_RAID")),
                            type: 0));
                    if (Session?.CurrentMapInstance?.MapInstanceType == MapInstanceType.RaidInstance)
                        ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId,
                            mapId: Session.Character.MapId,
                            mapX: Session.Character.MapX, mapY: Session.Character.MapY);

                    grp = Session.Character.Group;
                    Session.SendPacket(packet: Session.Character.GenerateRaid(Type: 1, exit: true));
                    Session.SendPacket(packet: Session.Character.GenerateRaid(Type: 2, exit: true));
                    if (grp != null)
                    {
                        grp.LeaveGroup(session: Session);
                        grp.Sessions.ForEach(action: s =>
                        {
                            s.SendPacket(packet: grp.GenerateRdlst());
                            s.SendPacket(packet: s.Character.GenerateRaid(Type: 0));
                        });
                    }

                    break;

                // Kick from Raid
                case 3:
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.RaidInstance) return;

                    if (Session.Character.Group?.IsLeader(session: Session) == true)
                    {
                        var chartokick =
                            ServerManager.Instance.GetSessionByCharacterId(characterId: rdPacket.CharacterId);
                        if (chartokick.Character?.Group == null) return;

                        chartokick.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "KICK_RAID"), type: 0));
                        grp = chartokick.Character?.Group;
                        chartokick.SendPacket(packet: chartokick.Character?.GenerateRaid(Type: 1, exit: true));
                        chartokick.SendPacket(packet: chartokick.Character?.GenerateRaid(Type: 2, exit: true));
                        grp?.LeaveGroup(session: chartokick);
                        grp?.Sessions.ForEach(action: s =>
                        {
                            s.SendPacket(packet: grp.GenerateRdlst());
                            s.SendPacket(packet: s.Character.GenerateRaid(Type: 0));
                        });
                    }

                    break;

                // Disolve Raid
                case 4:
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.RaidInstance) return;

                    if (Session.Character.Group?.IsLeader(session: Session) == true)
                    {
                        grp = Session.Character.Group;

                        var grpmembers = new ClientSession[40];
                        grp.Sessions.CopyTo(grpmembers: grpmembers);
                        foreach (var targetSession in grpmembers)
                            if (targetSession != null)
                            {
                                targetSession.SendPacket(
                                    packet: targetSession.Character.GenerateRaid(Type: 1, exit: true));
                                targetSession.SendPacket(
                                    packet: targetSession.Character.GenerateRaid(Type: 2, exit: true));
                                targetSession.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "RAID_DISOLVED"), type: 0));
                                grp.LeaveGroup(session: targetSession);
                            }

                        ServerManager.Instance.GroupList.RemoveAll(match: s => s.GroupId == grp.GroupId);
                        ServerManager.Instance.ThreadSafeGroupList.Remove(key: grp.GroupId);
                    }

                    break;
            }
        }

        public void QtPacket(QtPacket qtPacket)
        {
            switch (qtPacket.Type)
            {
                // On Target Dest
                case 1:
                    Session.Character.IncrementQuests(type: QuestType.GoTo,
                        firstData: Session.CurrentMapInstance.Map.MapId,
                        secondData: Session.Character.PositionX, thirdData: Session.Character.PositionY);
                    break;

                // Give Up Quest
                case 3:
                    var charQuest =
                        Session.Character.Quests?.FirstOrDefault(predicate: q => q.QuestNumber == qtPacket.Data);
                    if (charQuest == null || charQuest.IsMainQuest) return;
                    Session.Character.RemoveQuest(questId: charQuest.QuestId, IsGivingUp: true);
                    break;

                // Ask for rewards
                case 4:
                    break;
            }
        }

        /// <summary>
        ///     req_info packet
        /// </summary>
        /// <param name="reqInfoPacket"></param>
        public void ReqInfo(ReqInfoPacket reqInfoPacket)
        {
            if (Session.Character != null)
            {
                if (reqInfoPacket.Type == 6)
                {
                    if (reqInfoPacket.MateVNum.HasValue)
                    {
                        var mate = Session.CurrentMapInstance?.Sessions?.FirstOrDefault(predicate: s =>
                                s?.Character?.Mates != null && s.Character.Mates.Any(predicate: o =>
                                    o.MateTransportId == reqInfoPacket.MateVNum.Value))?
                            .Character.Mates
                            .FirstOrDefault(predicate: m => m.MateTransportId == reqInfoPacket.MateVNum.Value);

                        if (mate != null) Session.SendPacket(packet: mate.GenerateEInfo());
                    }
                }
                else if (reqInfoPacket.Type == 5)
                {
                    var npc = ServerManager.GetNpcMonster(npcVNum: (short)reqInfoPacket.TargetVNum);

                    if (Session.CurrentMapInstance?.GetMonsterById(mapMonsterId: Session.Character.LastNpcMonsterId)
                            is { } monster &&
                        monster.Monster?.OriginalNpcMonsterVNum == reqInfoPacket.TargetVNum)
                        npc = ServerManager.GetNpcMonster(npcVNum: monster.Monster.NpcMonsterVNum);

                    if (npc != null) Session.SendPacket(packet: npc.GenerateEInfo());
                }
                else if (reqInfoPacket.Type == 12)
                {
                    if (Session.Character.Inventory != null)
                        Session.SendPacket(packet: Session.Character.Inventory
                            .LoadBySlotAndType(slot: (short)reqInfoPacket.TargetVNum, type: InventoryType.Equipment)
                            ?.GenerateReqInfo());
                }
                else
                {
                    if (ServerManager.Instance.GetSessionByCharacterId(characterId: reqInfoPacket.TargetVNum)
                        ?.Character is { } character) Session.SendPacket(packet: character.GenerateReqInfo());
                }
            }
        }

        /// <summary>
        ///     rest packet
        /// </summary>
        /// <param name="sitpacket"></param>
        public void Rest(SitPacket sitpacket)
        {
            if (Session.Character.MeditationDictionary.Count != 0) Session.Character.MeditationDictionary.Clear();

            sitpacket.Users?.ForEach(action: u =>
            {
                if (u.UserType == 1)
                    Session.Character.Rest();
                else
                    Session.CurrentMapInstance.Broadcast(packet: Session.Character.Mates
                        .Find(match: s => s.MateTransportId == (int)u.UserId)
                        ?.GenerateRest(ownerSit: sitpacket.Users[index: 0] != u));
            });
        }

        /// <summary>
        ///     revival packet
        /// </summary>
        /// <param name="revivalPacket"></param>
        public void Revive(RevivalPacket revivalPacket)
        {
            if (Session.Character.Hp > 0) return;

            switch (revivalPacket.Type)
            {
                case 0:
                    switch (Session.CurrentMapInstance.MapInstanceType)
                    {
                        case MapInstanceType.LodInstance:
                            const int saver = 1211;
                            if (Session.Character.Inventory.CountItem(itemVNum: saver) < 1)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_SAVER"),
                                        type: 0));
                                ServerManager.Instance.ReviveFirstPosition(characterId: Session.Character.CharacterId);
                            }
                            else
                            {
                                Session.Character.Inventory.RemoveItemAmount(vnum: saver);
                                Session.Character.Hp = (int)Session.Character.HPLoad();
                                Session.Character.Mp = (int)Session.Character.MPLoad();
                                Session.CurrentMapInstance?.Broadcast(client: Session,
                                    content: Session.Character.GenerateRevive());
                                Session.SendPacket(packet: Session.Character.GenerateStat());
                            }

                            break;

                        case MapInstanceType.Act4Berios:
                        case MapInstanceType.Act4Calvina:
                        case MapInstanceType.Act4Hatus:
                        case MapInstanceType.Act4Morcos:
                            if (Session.Character.Reputation < Session.Character.Level * 10)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_REPUT"),
                                        type: 0));
                                ServerManager.Instance.ReviveFirstPosition(characterId: Session.Character.CharacterId);
                            }
                            else
                            {
                                Session.Character.GetReputation(amount: Session.Character.Level * -10);
                                Session.Character.Hp = (int)Session.Character.HPLoad();
                                Session.Character.Mp = (int)Session.Character.MPLoad();
                                Session.CurrentMapInstance?.Broadcast(client: Session,
                                    content: Session.Character.GenerateRevive());
                                Session.SendPacket(packet: Session.Character.GenerateStat());
                            }

                            break;

                        case MapInstanceType.CaligorInstance:
                            Session.Character.Hp = (int)Session.Character.HPLoad();
                            Session.Character.Mp = (int)Session.Character.MPLoad();
                            short x = 0;
                            short y = 0;
                            switch (Session.Character.Faction)
                            {
                                case FactionType.Angel:
                                    x = 50;
                                    y = 172;
                                    break;
                                case FactionType.Demon:
                                    x = 130;
                                    y = 172;
                                    break;
                            }

                            ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                                mapInstanceId: Session.Character.MapInstance.MapInstanceId, mapX: x, mapY: y);
                            Session.Character.AddBuff(indicator: new Buff(id: 169, level: Session.Character.Level),
                                sender: Session.Character.BattleEntity);
                            break;

                        default:
                            const int seed = 1012;
                            if (Session.Character.Inventory.CountItem(itemVNum: seed) < 10 &&
                                Session.Character.Level > 20)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_POWER_SEED"),
                                    type: 0));
                                ServerManager.Instance.ReviveFirstPosition(characterId: Session.Character.CharacterId);
                                Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_SEED_SAY"),
                                        type: 0));
                            }
                            else
                            {
                                if (Session.Character.Level > 20)
                                {
                                    Session.SendPacket(packet: Session.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "SEED_USED"), arg0: 10),
                                        type: 10));
                                    Session.Character.Inventory.RemoveItemAmount(vnum: seed, amount: 10);
                                    Session.Character.Hp = (int)(Session.Character.HPLoad() / 2);
                                    Session.Character.Mp = (int)(Session.Character.MPLoad() / 2);
                                    Session.Character.AddBuff(
                                        indicator: new Buff(id: 44, level: Session.Character.Level),
                                        sender: Session.Character.BattleEntity);
                                }
                                else
                                {
                                    Session.Character.Hp = (int)Session.Character.HPLoad();
                                    Session.Character.Mp = (int)Session.Character.MPLoad();
                                }

                                Session.CurrentMapInstance?.Broadcast(client: Session,
                                    content: Session.Character.GenerateTp());
                                Session.CurrentMapInstance?.Broadcast(client: Session,
                                    content: Session.Character.GenerateRevive());
                                Session.SendPacket(packet: Session.Character.GenerateStat());
                            }

                            break;
                    }

                    break;

                case 1:
                    ServerManager.Instance.ReviveFirstPosition(characterId: Session.Character.CharacterId);
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                        if (Session.Character.Level > 20)
                            Session.Character.AddBuff(indicator: new Buff(id: 44, level: Session.Character.Level),
                                sender: Session.Character.BattleEntity);
                    break;

                case 2:
                    if ((Session.CurrentMapInstance == ServerManager.Instance.ArenaInstance ||
                         Session.CurrentMapInstance == ServerManager.Instance.FamilyArenaInstance) &&
                        Session.Character.Gold >= 100)
                    {
                        Session.Character.Hp = (int)Session.Character.HPLoad();
                        Session.Character.Mp = (int)Session.Character.MPLoad();
                        Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateTp());
                        Session.CurrentMapInstance?.Broadcast(client: Session,
                            content: Session.Character.GenerateRevive());
                        Session.SendPacket(packet: Session.Character.GenerateStat());
                        Session.Character.Gold -= 100;
                        Session.SendPacket(packet: Session.Character.GenerateGold());
                        Session.Character.LastPVPRevive = DateTime.Now;
                        Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 5)).Subscribe(onNext: observer =>
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "PVP_ACTIVE"), type: 10)));
                    }
                    else
                    {
                        ServerManager.Instance.ReviveFirstPosition(characterId: Session.Character.CharacterId);
                    }

                    break;
            }

            Session.Character.BattleEntity.SendBuffsPacket();
        }

        /// <summary>
        ///     say packet
        /// </summary>
        /// <param name="sayPacket"></param>
        public void Say(SayPacket sayPacket)
        {
            if (string.IsNullOrEmpty(value: sayPacket.Message)) return;

            var penalty = Session.Account.PenaltyLogs.OrderByDescending(keySelector: s => s.DateEnd).FirstOrDefault();
            var message = sayPacket.Message;
            if (Session.Character.IsMuted() && penalty != null)
            {
                if (Session.Character.Gender == GenderType.Female)
                {
                    var member = ServerManager.Instance.ArenaTeams.ToList()
                        .FirstOrDefault(predicate: s => s.Any(predicate: e => e.Session == Session));
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance &&
                        member != null)
                    {
                        var member2 = member.FirstOrDefault(predicate: o => o.Session == Session);
                        member.Replace(predicate: s =>
                                member2 != null && s.ArenaTeamType == member2.ArenaTeamType && s != member2)
                            .Replace(predicate: s =>
                                s.ArenaTeamType == member.FirstOrDefault(predicate: o => o.Session == Session)
                                    ?.ArenaTeamType)
                            .ToList().ForEach(action: o =>
                                o.Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "MUTED_FEMALE"),
                                        type: 1)));
                    }
                    else
                    {
                        Session.CurrentMapInstance?.Broadcast(client: Session,
                            content: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "MUTED_FEMALE"), type: 1));
                    }

                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                            arg0: (penalty.DateEnd - DateTime.Now).ToString(format: @"hh\:mm\:ss")), type: 11));
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                            arg0: (penalty.DateEnd - DateTime.Now).ToString(format: @"hh\:mm\:ss")), type: 12));
                }
                else
                {
                    var member = ServerManager.Instance.ArenaTeams.ToList()
                        .FirstOrDefault(predicate: s => s.Any(predicate: e => e.Session == Session));
                    if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance &&
                        member != null)
                    {
                        var member2 = member.FirstOrDefault(predicate: o => o.Session == Session);
                        member.Replace(predicate: s =>
                                member2 != null && s.ArenaTeamType == member2.ArenaTeamType && s != member2)
                            .Replace(predicate: s =>
                                s.ArenaTeamType == member.FirstOrDefault(predicate: o => o.Session == Session)
                                    ?.ArenaTeamType)
                            .ToList().ForEach(action: o =>
                                o.Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "MUTED_MALE"),
                                        type: 1)));
                    }
                    else
                    {
                        Session.CurrentMapInstance?.Broadcast(client: Session,
                            content: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "MUTED_MALE"), type: 1));
                    }

                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                            arg0: (penalty.DateEnd - DateTime.Now).ToString(format: @"hh\:mm\:ss")), type: 11));
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                            arg0: (penalty.DateEnd - DateTime.Now).ToString(format: @"hh\:mm\:ss")), type: 12));
                }
            }
            else
            {
                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogChatMessage(logEntry: new LogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        Receiver = Session.CurrentMapInstance?.Map.Name,
                        ReceiverId = Session.CurrentMapInstance?.Map.MapId,
                        MessageType = LogType.Map,
                        Message = message
                    });
                var type = CharacterHelper.AuthorityChatColor(authority: Session.Character.Authority);

                ConcurrentBag<ArenaTeamMember> member;
                lock (ServerManager.Instance.ArenaTeams)
                {
                    member = ServerManager.Instance.ArenaTeams.ToList()
                        .FirstOrDefault(predicate: s => s.Any(predicate: e => e.Session == Session));
                }

                if (Session.Character.Authority >= AuthorityType.Gs)
                {
                    type = CharacterHelper.AuthorityChatColor(authority: Session.Character.Authority);
                    if (Session.CurrentMapInstance != null &&
                        Session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance &&
                        member != null)
                    {
                        var member2 = member.FirstOrDefault(predicate: o => o.Session == Session);
                        member.Replace(predicate: s =>
                                member2 != null && s.ArenaTeamType == member2.ArenaTeamType && s != member2)
                            .Replace(predicate: s =>
                                s.ArenaTeamType == member.FirstOrDefault(predicate: o => o.Session == Session)
                                    ?.ArenaTeamType)
                            .ToList().ForEach(action: o =>
                                o.Session.SendPacket(
                                    packet: Session.Character.GenerateSay(message: message.Trim(), type: 1)));
                    }
                    else
                    {
                        Session.CurrentMapInstance?.Broadcast(client: Session,
                            content: Session.Character.GenerateSay(message: message.Trim(), type: 1),
                            receiver: ReceiverType.AllExceptMe);
                    }

                    message = $"[{Session.Character.Authority} {Session.Character.Name}]: " + message;
                }

                if (Session.CurrentMapInstance != null &&
                    Session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance &&
                    member != null)
                {
                    var member2 = member.FirstOrDefault(predicate: o => o.Session == Session);
                    member.Where(predicate: s => s.ArenaTeamType == member2?.ArenaTeamType && s != member2).ToList()
                        .ForEach(action: o =>
                            o.Session.SendPacket(packet: Session.Character.GenerateSay(message: message.Trim(),
                                type: type,
                                ignoreNickname: Session.Account.Authority >= AuthorityType.Gs)));
                }
                else if (ServerManager.Instance.ChannelId == 51 && Session.Account.Authority < AuthorityType.Mod)
                {
                    Session.CurrentMapInstance?.Broadcast(client: Session,
                        content: Session.Character.GenerateSay(message: message.Trim(), type: type),
                        receiver: ReceiverType.AllExceptMeAct4);
                }
                else
                {
                    Session.CurrentMapInstance?.Broadcast(client: Session,
                        content: Session.Character.GenerateSay(message: message.Trim(), type: type,
                            ignoreNickname: Session.Character.Authority >= AuthorityType.Gs),
                        receiver: ReceiverType.AllExceptMe);
                }
            }
        }

        /// <summary>
        ///     pst packet
        /// </summary>
        /// <param name="pstPacket"></param>
        public void SendMail(PstPacket pstPacket)
        {
            if (pstPacket?.Data != null)
            {
                var receiver = DaoFactory.CharacterDao.LoadByName(name: pstPacket.Receiver);
                if (receiver != null)
                {
                    var datasplit = pstPacket.Data.Split(' ');
                    if (datasplit.Length < 2) return;

                    if (datasplit[1].Length > 250)
                        //PenaltyLogDTO log = new PenaltyLogDTO
                        //{
                        //    AccountId = Session.Character.AccountId,
                        //    Reason = "You are an idiot!",
                        //    Penalty = PenaltyType.Banned,
                        //    DateStart = DateTime.Now,
                        //    DateEnd = DateTime.Now.AddYears(69),
                        //    AdminName = "Your mom's ass"
                        //};
                        //Session.Character.InsertOrUpdatePenalty(log);
                        //ServerManager.Instance.Kick(Session.Character.Name);
                        return;

                    var headWearable =
                        Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Hat,
                            type: InventoryType.Wear);
                    var color = headWearable?.Item.IsColored == true
                        ? (byte)headWearable.Design
                        : (byte)Session.Character.HairColor;
                    var mailcopy = new MailDto
                    {
                        AttachmentAmount = 0,
                        IsOpened = false,
                        Date = DateTime.Now,
                        Title = datasplit[0],
                        Message = datasplit[1],
                        ReceiverId = receiver.CharacterId,
                        SenderId = Session.Character.CharacterId,
                        IsSenderCopy = true,
                        SenderClass = Session.Character.Class,
                        SenderGender = Session.Character.Gender,
                        SenderHairColor = Enum.IsDefined(enumType: typeof(HairColorType), value: color)
                            ? (HairColorType)color
                            : 0,
                        SenderHairStyle = Session.Character.HairStyle,
                        EqPacket = Session.Character.GenerateEqListForPacket(),
                        SenderMorphId = Session.Character.Morph == 0
                            ? (short)-1
                            : (short)(Session.Character.Morph > short.MaxValue ? 0 : Session.Character.Morph)
                    };
                    var mail = new MailDto
                    {
                        AttachmentAmount = 0,
                        IsOpened = false,
                        Date = DateTime.Now,
                        Title = datasplit[0],
                        Message = datasplit[1],
                        ReceiverId = receiver.CharacterId,
                        SenderId = Session.Character.CharacterId,
                        IsSenderCopy = false,
                        SenderClass = Session.Character.Class,
                        SenderGender = Session.Character.Gender,
                        SenderHairColor = Enum.IsDefined(enumType: typeof(HairColorType), value: color)
                            ? (HairColorType)color
                            : 0,
                        SenderHairStyle = Session.Character.HairStyle,
                        EqPacket = Session.Character.GenerateEqListForPacket(),
                        SenderMorphId = Session.Character.Morph == 0
                            ? (short)-1
                            : (short)(Session.Character.Morph > short.MaxValue ? 0 : Session.Character.Morph)
                    };

                    MailServiceClient.Instance.SendMail(mail: mailcopy);
                    MailServiceClient.Instance.SendMail(mail: mail);

                    //Session.Character.MailList.Add((Session.Character.MailList.Count > 0 ? Session.Character.MailList.OrderBy(s => s.Key).Last().Key : 0) + 1, mailcopy);
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "MAILED"),
                        type: 11));
                    //Session.SendPacket(Session.Character.GeneratePost(mailcopy, 2));
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                }
            }
            else if (pstPacket != null && int.TryParse(s: pstPacket.Id.ToString(), result: out var id)
                                       && byte.TryParse(s: pstPacket.Type.ToString(), result: out var type))
            {
                if (pstPacket.Argument == 3)
                {
                    if (Session.Character.MailList.ContainsKey(key: id))
                    {
                        if (!Session.Character.MailList[key: id].IsOpened)
                        {
                            Session.Character.MailList[key: id].IsOpened = true;
                            var mailupdate = Session.Character.MailList[key: id];
                            DaoFactory.MailDao.InsertOrUpdate(mail: ref mailupdate);
                        }

                        Session.SendPacket(
                            packet: Session.Character.GeneratePostMessage(mailDTO: Session.Character.MailList[key: id],
                                type: type));
                    }
                }
                else if (pstPacket.Argument == 2)
                {
                    if (Session.Character.MailList.ContainsKey(key: id))
                    {
                        var mail = Session.Character.MailList[key: id];
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "MAIL_DELETED"), type: 11));
                        Session.SendPacket(packet: $"post 2 {type} {id}");
                        if (DaoFactory.MailDao.LoadById(mailId: mail.MailId) != null)
                            DaoFactory.MailDao.DeleteById(mailId: mail.MailId);

                        if (Session.Character.MailList.ContainsKey(key: id)) Session.Character.MailList.Remove(key: id);
                    }
                }
            }
        }

        /// <summary>
        ///     qset packet
        /// </summary>
        /// <param name="qSetPacket"></param>
        public void SetQuicklist(QSetPacket qSetPacket)
        {
            short data1 = 0, data2 = 0, type = qSetPacket.Type, q1 = qSetPacket.Q1, q2 = qSetPacket.Q2;
            if (qSetPacket.Data1.HasValue) data1 = qSetPacket.Data1.Value;

            if (qSetPacket.Data2.HasValue) data2 = qSetPacket.Data2.Value;

            switch (type)
            {
                case 0:
                case 1:

                    // client says qset 0 1 3 2 6 answer -> qset 1 3 0.2.6.0
                    Session.Character.QuicklistEntries.RemoveAll(match: n =>
                        n.Q1 == q1 && n.Q2 == q2
                                   && (Session.Character.UseSp ? n.Morph == Session.Character.Morph : n.Morph == 0));
                    Session.Character.QuicklistEntries.Add(item: new QuicklistEntryDto
                    {
                        CharacterId = Session.Character.CharacterId,
                        Type = type,
                        Q1 = q1,
                        Q2 = q2,
                        Slot = data1,
                        Pos = data2,
                        Morph = Session.Character.UseSp ? (short)Session.Character.Morph : (short)0
                    });
                    Session.SendPacket(packet: $"qset {q1} {q2} {type}.{data1}.{data2}.0");
                    break;

                case 2:

                    // DragDrop / Reorder qset type to1 to2 from1 from2 vars -> q1 q2 data1 data2
                    var qlFrom = Session.Character.QuicklistEntries.SingleOrDefault(predicate: n =>
                        n.Q1 == data1 && n.Q2 == data2
                                      && (Session.Character.UseSp ? n.Morph == Session.Character.Morph : n.Morph == 0));
                    if (qlFrom != null)
                    {
                        var qlTo = Session.Character.QuicklistEntries.SingleOrDefault(predicate: n =>
                            n.Q1 == q1 && n.Q2 == q2 && (Session.Character.UseSp
                                ? n.Morph == Session.Character.Morph
                                : n.Morph == 0));
                        qlFrom.Q1 = q1;
                        qlFrom.Q2 = q2;
                        if (qlTo == null)
                        {
                            // Put 'from' to new position (datax)
                            Session.SendPacket(
                                packet: $"qset {qlFrom.Q1} {qlFrom.Q2} {qlFrom.Type}.{qlFrom.Slot}.{qlFrom.Pos}.0");

                            // old 'from' is now empty.
                            Session.SendPacket(packet: $"qset {data1} {data2} 7.7.-1.0");
                        }
                        else
                        {
                            // Put 'from' to new position (datax)
                            Session.SendPacket(
                                packet: $"qset {qlFrom.Q1} {qlFrom.Q2} {qlFrom.Type}.{qlFrom.Slot}.{qlFrom.Pos}.0");

                            // 'from' is now 'to' because they exchanged
                            qlTo.Q1 = data1;
                            qlTo.Q2 = data2;
                            Session.SendPacket(
                                packet: $"qset {qlTo.Q1} {qlTo.Q2} {qlTo.Type}.{qlTo.Slot}.{qlTo.Pos}.0");
                        }
                    }

                    break;

                case 3:

                    // Remove from Quicklist
                    Session.Character.QuicklistEntries.RemoveAll(match: n =>
                        n.Q1 == q1 && n.Q2 == q2
                                   && (Session.Character.UseSp ? n.Morph == Session.Character.Morph : n.Morph == 0));
                    Session.SendPacket(packet: $"qset {q1} {q2} 7.7.-1.0");
                    break;

                default:
                    return;
            }
        }

        /// <summary>
        ///     game_start packet
        /// </summary>
        /// <param name="gameStartPacket"></param>
        [SuppressMessage(category: "ReSharper", checkId: "PossibleMultipleEnumeration")]
        public void StartGame(GameStartPacket gameStartPacket)
        {
            if (gameStartPacket == null) throw new ArgumentNullException(paramName: nameof(gameStartPacket));
            if (Session?.Character == null || Session.IsOnMap || !Session.HasSelectedCharacter)
                // character should have been selected in SelectCharacter
                return;

            var shouldRespawn = false;

            if (Session.Character.MapInstance?.Map?.MapTypes != null)
                if (Session.Character.MapInstance.Map.MapTypes.Any(predicate: m =>
                        m.MapTypeId == (short)MapTypeEnum.Act4)
                    && ServerManager.Instance.ChannelId != 51)
                {
                    if (ServerManager.Instance.IsAct4Online())
                    {
                        Session.Character.ChangeChannel(ip: ServerManager.Instance.Configuration.Act4Ip,
                            port: ServerManager.Instance.Configuration.Act4Port, mode: 2);
                        return;
                    }

                    shouldRespawn = true;
                }

            Session.CurrentMapInstance = Session.Character.MapInstance;

            if (ServerManager.Instance.Configuration.SceneOnCreate
                && Session.Character.GeneralLogs.CountLinq(predicate: s => s.LogType == "Connection") < 2)
                Session.SendPacket(packet: "scene 40");

            Session.SendPacket(packet: Session.Character.GenerateSay(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "WELCOME_SERVER"),
                    arg0: ServerManager.Instance.ServerGroup), type: 10));

            Session.Character.LoadSpeed();
            Session.Character.LoadSkills();
            Session.SendPacket(packet: Session.Character.GenerateTit());
            Session.SendPacket(packet: Session.Character.GenerateSpPoint());
            Session.SendPacket(packet: "rsfi 1 1 0 9 0 9");

            Session.Character.Quests?.Where(predicate: q => q?.Quest?.TargetMap != null).ToList()
                .ForEach(action: qst => Session.SendPacket(packet: qst.Quest.TargetPacket()));

            if (Session.Character.Hp <= 0 && (!Session.Character.IsSeal || ServerManager.Instance.ChannelId != 51))
            {
                ServerManager.Instance.ReviveFirstPosition(characterId: Session.Character.CharacterId);
            }
            else
            {
                if (shouldRespawn)
                {
                    var resp = Session.Character.Respawn;
                    var x = (short)(resp.DefaultX + ServerManager.RandomNumber(min: -3, max: 3));
                    var y = (short)(resp.DefaultY + ServerManager.RandomNumber(min: -3, max: 3));
                    ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: resp.DefaultMapId,
                        mapX: x, mapY: y);
                }
                else
                {
                    ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId);
                }
            }

            Session.SendPacket(packet: Session.Character.GenerateSki());
            Session.SendPacket(
                packet:
                $"fd {Session.Character.Reputation} 0 {(int)Session.Character.Dignity} {Math.Abs(value: Session.Character.GetDignityIco())}");
            Session.SendPacket(packet: Session.Character.GenerateFd());
            Session.SendPacket(packet: "rage 0 250000");
            Session.SendPacket(packet: "rank_cool 0 0 18000");
            var specialistInstance = Session.Character.Inventory.LoadBySlotAndType(slot: 8, type: InventoryType.Wear);

            var medal = Session.Character.StaticBonusList.Find(match: s =>
                s.StaticBonusType == StaticBonusType.BazaarMedalGold ||
                s.StaticBonusType == StaticBonusType.BazaarMedalSilver);

            if (medal != null)
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "LOGIN_MEDAL"), type: 12));

            if (Session.Character.StaticBonusList.Any(predicate: s => s.StaticBonusType == StaticBonusType.PetBasket))
                Session.SendPacket(packet: "ib 1278 1");

            if (Session.Character.MapInstance?.Map?.MapTypes?.Any(predicate: m =>
                m.MapTypeId == (short)MapTypeEnum.CleftOfDarkness) == true) Session.SendPacket(packet: "bc 0 0 0");

            if (specialistInstance != null) Session.SendPacket(packet: Session.Character.GenerateSpPoint());

            Session.SendPacket(packet: "scr 0 0 0 0 0 0");
            for (var i = 0; i < 10; i++)
                Session.SendPacket(packet: $"bn {i} {Language.Instance.GetMessageFromKey(key: $"BN{i}")}");

            Session.SendPacket(packet: Session.Character.GenerateExts());
            Session.SendPacket(packet: Session.Character.GenerateMlinfo());
            Session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());

            Session.SendPacket(packet: Session.Character.GeneratePinit());
            Session.SendPackets(packets: Session.Character.GeneratePst());

            Session.SendPacket(packet: "zzim");
            Session.SendPacket(
                packet:
                $"twk 1 {Session.Character.CharacterId} {Session.Account.Name} {Session.Character.Name} shtmxpdlfeoqkr");

            var familyId = DaoFactory.FamilyCharacterDao.LoadByCharacterId(characterId: Session.Character.CharacterId)
                ?.FamilyId;
            if (familyId.HasValue) Session.Character.Family = ServerManager.Instance.FamilyList[key: familyId.Value];

            if (Session.Character.Family != null && Session.Character.FamilyCharacter != null)
            {
                if (Session.Character.Faction != (FactionType)Session.Character.Family.FamilyFaction)
                    Session.Character.Faction
                        = (FactionType)Session.Character.Family.FamilyFaction;

                Session.SendPacket(packet: Session.Character.GenerateGInfo());
                Session.SendPackets(packets: Session.Character.GetFamilyHistory());
                Session.SendPacket(packet: Session.Character.GenerateFamilyMember());
                Session.SendPacket(packet: Session.Character.GenerateFamilyMemberMessage());
                Session.SendPacket(packet: Session.Character.GenerateFamilyMemberExp());

                if (!string.IsNullOrWhiteSpace(value: Session.Character.Family.FamilyMessage))
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(message: "--- Family Message ---\n" +
                                                                          Session.Character.Family.FamilyMessage));
            }

            Session.SendPacket(packet: Session.Character.GetSqst());
            Session.SendPacket(packet: "act6");
            Session.SendPacket(packet: Session.Character.GenerateFaction());
            Session.SendPackets(packets: Session.Character.GenerateScP());
            Session.SendPackets(packets: Session.Character.GenerateScN());
#pragma warning disable 618
            Session.Character.GenerateStartupInventory();
#pragma warning restore 618

            Session.SendPacket(packet: Session.Character.GenerateGold());
            Session.SendPackets(packets: Session.Character.GenerateQuicklist());

            var clinit = "clinit";
            var flinit = "flinit";
            var kdlinit = "kdlinit";
            foreach (var character in ServerManager.Instance.TopComplimented)
                clinit +=
                    $" {character.CharacterId}|{character.Level}|{character.HeroLevel}|{character.Compliment}|{character.Name}";

            foreach (var character in ServerManager.Instance.TopReputation)
                flinit +=
                    $" {character.CharacterId}|{character.Level}|{character.HeroLevel}|{character.Reputation}|{character.Name}";

            foreach (var character in ServerManager.Instance.TopPoints)
                kdlinit +=
                    $" {character.CharacterId}|{character.Level}|{character.HeroLevel}|{character.Act4Points}|{character.Name}";

            Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateGidx());

            Session.SendPacket(packet: Session.Character.GenerateFinit());
            Session.SendPacket(packet: Session.Character.GenerateBlinit());
            Session.SendPacket(packet: clinit);
            Session.SendPacket(packet: flinit);
            Session.SendPacket(packet: kdlinit);

            Session.Character.LastPVPRevive = DateTime.Now;

            var warning = DaoFactory.PenaltyLogDao.LoadByAccount(accountId: Session.Character.AccountId)
                .Where(predicate: p => p.Penalty == PenaltyType.Warning);
            if (warning.Any())
                Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "WARNING_INFO"),
                        arg0: warning.Count())));

            // finfo - friends info
            Session.Character.LoadMail();
            Session.Character.LoadSentMail();
            Session.Character.DeleteTimeout();

            /*QuestModel quest = ServerManager.Instance.QuestList.Where(s => s.QuestGiver.Type == QuestGiverType.InitialQuest).FirstOrDefault(); //Nizar
            if(quest != null)
            {
                quest = quest.Copy();

                int current = 0;
                int max = 0;

                if (quest.KillObjectives != null)
                {
                    max = quest.KillObjectives[0].GoalAmount;
                    current = quest.KillObjectives[0].CurrentAmount;
                }

                if(quest.WalkObjective != null)
                {
                    Session.SendPacket($"target {quest.WalkObjective.MapX} {quest.WalkObjective.MapY} {quest.WalkObjective.MapId} {quest.QuestDataVNum}");
                }

            //    //Quest Packet Definition: qstlist {Unknown}.{QuestVNUM}.{QuestVNUM}.{GoalType}.{Current}.{Goal}.{Finished}.{GoalType}.{Current}.{Goal}.{Finished}.{GoalType}.{Current}.{Goal}.{Finished}.{ShowDialog}
            //    //Same for qsti
                Session.SendPacket($"qstlist 5.{quest.QuestDataVNum}.{quest.QuestDataVNum}.{quest.QuestGoalType}.{current}.{max}.0.0.0.0.0.0.0.0.0.1");

            }*/

            if (Session.Character.Quests.Any()) Session.SendPacket(packet: Session.Character.GenerateQuestsPacket());

            if (Session.Character.IsSeal)
            {
                if (ServerManager.Instance.ChannelId == 51)
                    Session.Character.SetSeal();
                else
                    Session.Character.IsSeal = false;
            }
        }

        /// <summary>
        ///     walk packet
        /// </summary>
        /// <param name="walkPacket"></param>
        public void Walk(WalkPacket walkPacket)
        {
            if (Session.Character.CanMove())
            {
                if (Session.Character.MeditationDictionary.Count != 0) Session.Character.MeditationDictionary.Clear();

                // ReSharper disable once UnusedVariable
                var currentRunningSeconds =
                    (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(value: -50)).TotalSeconds;
                Map.GetDistance(p: new MapCell { X = Session.Character.PositionX, Y = Session.Character.PositionY },
                    q: new MapCell { X = walkPacket.XCoordinate, Y = walkPacket.YCoordinate });

                if (Session.HasCurrentMapInstance
                    && !Session.CurrentMapInstance.Map.IsBlockedZone(x: walkPacket.XCoordinate,
                        y: walkPacket.YCoordinate)
                    && !Session.Character.IsChangingMapInstance && !Session.Character.HasShopOpened)
                {
                    Session.Character.PyjamaDead = false;
                    if (!Session.Character.InvisibleGm)
                        Session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.Move(type: UserType.Player,
                            callerId: Session.Character.CharacterId, positionX: walkPacket.XCoordinate,
                            positionY: walkPacket.YCoordinate,
                            speed: Session.Character.Speed));
                    Session.SendPacket(packet: Session.Character.GenerateCond());
                    Session.Character.WalkDisposable?.Dispose();
                    Walk();
                    var interval = 100 - Session.Character.Speed * 5 + 100 > 0
                        ? 100 - Session.Character.Speed * 5 + 100
                        : 0;
                    Session.Character.WalkDisposable = Observable
                        .Interval(period: TimeSpan.FromMilliseconds(value: interval))
                        .Subscribe(onNext: obs => { Walk(); });

                    void Walk()
                    {
                        var nextCell =
                            Map.GetNextStep(
                                start: new MapCell { X = Session.Character.PositionX, Y = Session.Character.PositionY },
                                end: new MapCell { X = walkPacket.XCoordinate, Y = walkPacket.YCoordinate }, steps: 1);

                        Session.Character.GetDir(pX: Session.Character.PositionX, pY: Session.Character.PositionY,
                            nX: nextCell.X,
                            nY: nextCell.Y);

                        if (Session.Character.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                        {
                            Session.Character.MapX = nextCell.X;
                            Session.Character.MapY = nextCell.Y;
                        }

                        Session.Character.PositionX = nextCell.X;
                        Session.Character.PositionY = nextCell.Y;

                        Session.Character.LastMove = DateTime.Now;

                        if (Session.Character.IsVehicled)
                            Session.Character.Mates.Where(predicate: s => s.IsTeamMember || s.IsTemporalMate).ToList()
                                .ForEach(action: s =>
                                {
                                    s.PositionX = Session.Character.PositionX;
                                    s.PositionY = Session.Character.PositionY;
                                });

                        if (Session.Character.LastMonsterAggro.AddSeconds(value: 5) > DateTime.Now)
                            Session.Character.UpdateBushFire();

                        Session.CurrentMapInstance?.OnAreaEntryEvents
                            ?.Where(predicate: s => s.InZone(positionX: Session.Character.PositionX,
                                positionY: Session.Character.PositionY)).ToList()
                            .ForEach(action: e =>
                                e.Events.ForEach(action: evt => EventHelper.Instance.RunEvent(evt: evt)));
                        Session.CurrentMapInstance?.OnAreaEntryEvents?.RemoveAll(match: s =>
                            s.InZone(positionX: Session.Character.PositionX, positionY: Session.Character.PositionY));

                        Session.CurrentMapInstance?.OnMoveOnMapEvents?.ForEach(action: e =>
                            EventHelper.Instance.RunEvent(evt: e));
                        Session.CurrentMapInstance?.OnMoveOnMapEvents?.RemoveAll(match: s => s != null);

                        if (Session.Character.PositionX == walkPacket.XCoordinate &&
                            Session.Character.PositionY == walkPacket.YCoordinate)
                            Session.Character.WalkDisposable?.Dispose();
                    }
                }
            }
        }

        /// <summary>
        ///     / packet
        /// </summary>
        /// <param name="whisperPacket"></param>
        public void Whisper(WhisperPacket whisperPacket)
        {
            try
            {
                // TODO: Implement WhisperSupport
                if (string.IsNullOrEmpty(value: whisperPacket.Message)) return;

                var characterName =
                    whisperPacket.Message.Split(' ')[
                            whisperPacket.Message.StartsWith(value: "GM ",
                                comparisonType: StringComparison.CurrentCulture)
                                ? 1
                                : 0]
                        .Replace(oldValue: "[Angel]", newValue: "").Replace(oldValue: "[Demon]", newValue: "");

                Enum.GetNames(enumType: typeof(AuthorityType)).ToList()
                    .ForEach(action: at => characterName = characterName.Replace(oldValue: $"[{at}]", newValue: ""));

                var message = "";
                var packetsplit = whisperPacket.Message.Split(' ');
                for (var i = packetsplit[0] == "GM" ? 2 : 1; i < packetsplit.Length; i++)
                    message += packetsplit[i] + " ";

                if (message.Length > 60) message = message.Substring(startIndex: 0, length: 60);

                message = message.Trim();
                Session.SendPacket(packet: Session.Character.GenerateSpk(message: message, type: 5));
                var receiver = DaoFactory.CharacterDao.LoadByName(name: characterName);
                int? sentChannelId = null;
                if (receiver != null)
                {
                    if (receiver.CharacterId == Session.Character.CharacterId) return;

                    if (Session.Character.IsBlockedByCharacter(characterId: receiver.CharacterId))
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "BLACKLIST_BLOCKED")));
                        return;
                    }

                    var receiverSession =
                        ServerManager.Instance.GetSessionByCharacterId(characterId: receiver.CharacterId);
                    if (receiverSession?.CurrentMapInstance?.Map.MapId == Session.CurrentMapInstance?.Map.MapId
                        && Session.Account.Authority >= AuthorityType.Mod)
                        receiverSession?.SendPacket(packet: Session.Character.GenerateSay(message: message, type: 2));

                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogChatMessage(logEntry: new LogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            Receiver = receiver.Name,
                            ReceiverId = receiver.CharacterId,
                            MessageType = LogType.Whisper,
                            Message = message
                        });

                    sentChannelId = CommunicationServiceClient.Instance.SendMessageToCharacter(
                        message: new ScsCharacterMessage
                        {
                            DestinationCharacterId = receiver.CharacterId,
                            SourceCharacterId = Session.Character.CharacterId,
                            SourceWorldId = ServerManager.Instance.WorldId,
                            Message = Session.Character.Authority >= AuthorityType.Gs
                                ? Session.Character.GenerateSay(
                                    message:
                                    $"(whisper)(From {Session.Character.Authority} {Session.Character.Name}):{message}",
                                    type: 11)
                                : Session.Character.GenerateSpk(message: message,
                                    type: Session.Account.Authority >= AuthorityType.Mod ? 15 : 5),
                            Type = Enum.GetNames(enumType: typeof(AuthorityType)).Any(predicate: a =>
                                   {
                                       if (a.Equals(value: packetsplit[0]))
                                       {
                                           Enum.TryParse(value: a, result: out AuthorityType auth);
                                           if (auth >= AuthorityType.Mod) return true;
                                       }

                                       return false;
                                   })
                                   || Session.Account.Authority >= AuthorityType.Mod
                                ? MessageType.WhisperGm
                                : MessageType.Whisper
                        });
                }

                if (sentChannelId == null)
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_CONNECTED")));
            }
            catch (Exception e)
            {
                Logger.Error(data: "Whisper failed.", ex: e);
            }
        }

        /// <summary>
        ///     Anti-Cheat Heartbeat Packet
        /// </summary>
        /// <param name="ntcpAcPacket"></param>
        public void NtcpAcPacket(NtcpAcPacket ntcpAcPacket)
        {
            if (Session == null || ntcpAcPacket == null || ntcpAcPacket.Type != 1) return;

            #region Packet Manipulation

            if (ntcpAcPacket.TrapValue != 0 || ntcpAcPacket.Crc32?.Length != 8)
            {
                Session.BanForCheatUsing(detectionCode: 1);
                return;
            }

            var data = Convert.FromBase64String(s: ntcpAcPacket.Data);
            var encryptedKey = Convert.FromBase64String(s: ntcpAcPacket.EncryptedKey);

            if (!AntiCheatHelper.IsValidSignature(signature: ntcpAcPacket.Signature, data: data,
                    encryptedKey: encryptedKey, crc32: ntcpAcPacket.Crc32)
                || data.Length != encryptedKey.Length)
            {
                Session.BanForCheatUsing(detectionCode: 2);
                return;
            }

            #endregion

            #region Crc32

            if (string.IsNullOrEmpty(value: Session.Crc32))
            {
                Session.Crc32 = ntcpAcPacket.Crc32;
            }
            else if (ntcpAcPacket.Crc32 != Session.Crc32)
            {
                Session.BanForCheatUsing(detectionCode: 3);
                return;
            }

            #endregion

            #region Data

            if (Encoding.UTF8.GetString(bytes: data) !=
                AntiCheatHelper.Encrypt(data: Encoding.UTF8.GetBytes(s: Session.LastAntiCheatData),
                    encryptedKey: encryptedKey))
            {
                Session.BanForCheatUsing(detectionCode: 4);
                return;
            }

            #endregion

            #region IsAntiCheatAlive

            Session.IsAntiCheatAlive = true;

            #endregion
        }

        #endregion
    }
}