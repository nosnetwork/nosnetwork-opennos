﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.Core.Handling;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.GameObject.Packets.ServerPackets;

namespace OpenNos.Handler
{
    internal class ScriptedInstancePacketHandler : IPacketHandler
    {
        #region Instantiation

        public ScriptedInstancePacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        public void ButtonCancel(BscPacket packet)
        {
            switch (packet.Type)
            {
                case 2:
                    var arenamember = ServerManager.Instance.ArenaMembers.ToList()
                        .FirstOrDefault(predicate: s => s.Session == Session);
                    if (arenamember?.GroupId != null)
                        if (packet.Option != 1)
                        {
                            Session.SendPacket(
                                packet:
                                $"qna #bsc^2^1 {Language.Instance.GetMessageFromKey(key: "ARENA_PENALTY_NOTICE")}");
                            return;
                        }

                    Session.Character.LeaveTalentArena();
                    break;
            }
        }

        public void Call(TaCallPacket packet)
        {
            try
            {
                var arenateam = ServerManager.Instance.ArenaTeams.ToList()
                    .FirstOrDefault(predicate: s => s.Any(predicate: o => o.Session == Session));
                if (arenateam == null ||
                    Session.CurrentMapInstance.MapInstanceType != MapInstanceType.TalentArenaMapInstance) return;

                IEnumerable<ArenaTeamMember> ownteam = arenateam.Replace(predicate: s =>
                    s.ArenaTeamType == arenateam?.FirstOrDefault(predicate: e => e.Session == Session)?.ArenaTeamType);
                var client = ownteam.Where(predicate: s => s.Session != Session).OrderBy(keySelector: s => s.Order)
                    .Skip(count: packet.CalledIndex)
                    .FirstOrDefault()?.Session;
                var memb = arenateam.FirstOrDefault(predicate: s => s.Session == client);
                if (client == null || client.CurrentMapInstance != Session.CurrentMapInstance || memb == null ||
                    memb.LastSummoned != null || ownteam.Sum(selector: s => s.SummonCount) >= 5) return;

                memb.SummonCount++;
                arenateam.ToList().ForEach(action: arenauser =>
                {
                    arenauser.Session.SendPacket(
                        packet: arenauser.Session.Character.GenerateTaP(tatype: 2, showOponent: true));
                });
                var arenaTeamMember = arenateam.FirstOrDefault(predicate: s => s.Session == client);
                if (arenaTeamMember != null) arenaTeamMember.LastSummoned = DateTime.Now;

                Session.CurrentMapInstance.Broadcast(packet: Session.Character.GenerateEff(effectid: 4432));

                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 0)).Subscribe(onNext: o =>
                {
                    client.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ARENA_CALLED"),
                                arg0: 3), type: 0));
                    client.SendPacket(
                        packet: client.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ARENA_CALLED"),
                                arg0: 3), type: 10));
                });

                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: o =>
                {
                    client.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ARENA_CALLED"),
                                arg0: 2), type: 0));
                    client.SendPacket(
                        packet: client.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ARENA_CALLED"),
                                arg0: 2), type: 10));
                });

                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 2)).Subscribe(onNext: o =>
                {
                    client.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ARENA_CALLED"),
                                arg0: 1), type: 0));
                    client.SendPacket(
                        packet: client.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ARENA_CALLED"),
                                arg0: 1), type: 10));
                });

                var x = Session.Character.PositionX;
                var y = Session.Character.PositionY;
                const byte TIMER = 30;
                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 3)).Subscribe(onNext: o =>
                {
                    Session.CurrentMapInstance.Broadcast(packet: $"ta_t 0 {client.Character.CharacterId} {TIMER}");
                    client.Character.PositionX = x;
                    client.Character.PositionY = y;
                    Session.CurrentMapInstance.Broadcast(packet: client.Character.GenerateTp());

                    client.SendPacket(
                        packet: UserInterfaceHelper.Instance.GenerateTaSt(watch: TalentArenaOptionType.Nothing));
                });

                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: TIMER + 3)).Subscribe(onNext: o =>
                {
                    var lastsummoned = arenateam.FirstOrDefault(predicate: s => s.Session == client)?.LastSummoned;
                    if (lastsummoned == null ||
                        ((DateTime)lastsummoned).AddSeconds(value: TIMER) >= DateTime.Now) return;

                    var firstOrDefault = arenateam.FirstOrDefault(predicate: s => s.Session == client);
                    if (firstOrDefault != null) firstOrDefault.LastSummoned = null;

                    var bufftodisable = new List<BuffType> { BuffType.Bad };
                    client.Character.DisableBuffs(types: bufftodisable);
                    client.Character.Hp = (int)client.Character.HPLoad();
                    client.Character.Mp = (int)client.Character.MPLoad();
                    client.SendPacket(packet: client.Character.GenerateStat());

                    client.Character.PositionX = memb.ArenaTeamType == ArenaTeamType.Erenia ? (short)120 : (short)19;
                    client.Character.PositionY = memb.ArenaTeamType == ArenaTeamType.Erenia ? (short)39 : (short)40;
                    Session?.CurrentMapInstance?.Broadcast(packet: client?.Character?.GenerateTp());
                    client.SendPacket(
                        packet: UserInterfaceHelper.Instance.GenerateTaSt(watch: TalentArenaOptionType.Watch));
                });
            }
            catch
            {
            }
        }

        /// <summary>
        ///     RSelPacket packet
        /// </summary>
        /// <param name="escapePacket"></param>
        public void Escape(EscapePacket escapePacket)
        {
            if (escapePacket == null) throw new ArgumentNullException(paramName: nameof(escapePacket));
            if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.TimeSpaceInstance)
            {
                ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: Session.Character.MapId,
                    mapX: Session.Character.MapX, mapY: Session.Character.MapY);
                Session.Character.Timespace = null;
            }
            else if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.RaidInstance)
            {
                ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: Session.Character.MapId,
                    mapX: Session.Character.MapX, mapY: Session.Character.MapY);
                ServerManager.Instance.GroupLeave(session: Session);
            }
        }

        /// <summary>
        ///     mkraid packet
        /// </summary>
        /// <param name="mkRaidPacket"></param>
        public void GenerateRaid(MkRaidPacket mkRaidPacket)
        {
            if (mkRaidPacket == null) throw new ArgumentNullException(paramName: nameof(mkRaidPacket));
            if (Session.Character.Group?.Raid != null && Session.Character.Group.IsLeader(session: Session))
            {
                if (Session.Character.MapId == Session.Character.Group.Raid.MapId
                    && Map.GetDistance(
                        p: new MapCell { X = Session.Character.PositionX, Y = Session.Character.PositionY },
                        q: new MapCell
                        {
                            X = Session.Character.Group.Raid.PositionX,
                            Y = Session.Character.Group.Raid.PositionY
                        }) < 2)
                {
                    if ((Session.Character.Group.SessionCount > 0 || Session.Character.Authority >= AuthorityType.Gm)
                        && Session.Character.Group.Sessions.All(predicate: s =>
                            s.CurrentMapInstance == Session.CurrentMapInstance))
                    {
                        if (Session.Character.Group.Raid.FirstMap == null)
                            Session.Character.Group.Raid.LoadScript(mapinstancetype: MapInstanceType.RaidInstance,
                                creator: Session.Character);

                        if (Session.Character.Group.Raid.FirstMap == null) return;

                        Session.Character.Group.Raid.InstanceBag.Lock = true;

                        //Session.Character.Group.Characters.Where(s => s.CurrentMapInstance != Session.CurrentMapInstance).ToList().ForEach(
                        //session =>
                        //{
                        //    Session.Character.Group.LeaveGroup(session);
                        //    session.SendPacket(session.Character.GenerateRaid(1, true));
                        //    session.SendPacket(session.Character.GenerateRaid(2, true));
                        //});

                        Session.Character.Group.Raid.InstanceBag.Lives = (short)Session.Character.Group.SessionCount;

                        foreach (var session in Session.Character.Group.Sessions.GetAllItems())
                            if (session != null)
                            {
                                ServerManager.Instance.ChangeMapInstance(characterId: session.Character.CharacterId,
                                    mapInstanceId: session.Character.Group.Raid.FirstMap.MapInstanceId,
                                    mapX: session.Character.Group.Raid.StartX,
                                    mapY: session.Character.Group.Raid.StartY);
                                session.SendPacket(packet: "raidbf 0 0 25");
                                session.SendPacket(packet: session.Character.Group.GeneraterRaidmbf(session: session));
                                session.SendPacket(packet: session.Character.GenerateRaid(Type: 5));
                                session.SendPacket(packet: session.Character.GenerateRaid(Type: 4));
                                session.SendPacket(packet: session.Character.GenerateRaid(Type: 3));
                                if (session.Character.Group.Raid.DailyEntries > 0)
                                {
                                    var entries = session.Character.Group.Raid.DailyEntries -
                                                  session.Character.GeneralLogs.CountLinq(predicate: s =>
                                                      s.LogType == "InstanceEntry" &&
                                                      short.Parse(s: s.LogData) == session.Character.Group.Raid.Id &&
                                                      s.Timestamp.Date == DateTime.Today);
                                    session.SendPacket(packet: session.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "INSTANCE_ENTRIES"),
                                            arg0: entries),
                                        type: 10));
                                }
                            }

                        ServerManager.Instance.GroupList.Remove(item: Session.Character.Group);

                        Logger.LogUserEvent(logEvent: "RAID_START", caller: Session.GenerateIdentity(),
                            data: $"RaidId: {Session.Character.Group.GroupId}");
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "RAID_TEAM_NOT_READY"),
                                type: 0));
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_PORTAL"), type: 0));
                }
            }
        }

        /// <summary>
        ///     RSelPacket packet
        /// </summary>
        /// <param name="rSelPacket"></param>
        public void GetGift(RSelPacket rSelPacket)
        {
            if (rSelPacket == null) throw new ArgumentNullException(paramName: nameof(rSelPacket));
            if (Session.Character.Timespace?.FirstMap?.MapInstanceType == MapInstanceType.TimeSpaceInstance)
            {
                ServerManager.GetBaseMapInstanceIdByMapId(mapId: Session.Character.MapId);
                if (Session.Character.Timespace?.FirstMap.InstanceBag.EndState == 5 ||
                    Session.Character.Timespace?.FirstMap.InstanceBag.EndState == 6)
                    if (!Session.Character.TimespaceRewardGotten)
                    {
                        Session.Character.TimespaceRewardGotten = true;
                        Session.Character.GetReputation(amount: Session.Character.Timespace.Reputation);

                        Session.Character.Gold =
                            Session.Character.Gold + Session.Character.Timespace.Gold
                            > ServerManager.Instance.Configuration.MaxGold
                                ? ServerManager.Instance.Configuration.MaxGold
                                : Session.Character.Gold + Session.Character.Timespace.Gold;
                        Session.SendPacket(packet: Session.Character.GenerateGold());
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "GOLD_TS_END"),
                                arg0: Session.Character.Timespace.Gold), type: 10));

                        var rand = new Random().Next(maxValue: Session.Character.Timespace.DrawItems.Count);
                        var repay = "repay ";
                        if (Session.Character.Timespace.DrawItems.Count > 0)
                            Session.Character.GiftAdd(itemVNum: Session.Character.Timespace.DrawItems[index: rand].VNum,
                                amount: Session.Character.Timespace.DrawItems[index: rand].Amount,
                                design: Session.Character.Timespace.DrawItems[index: rand].Design,
                                forceRandom: Session.Character.Timespace.DrawItems[index: rand].IsRandomRare);

                        for (var i = 0; i < 3; i++)
                        {
                            var gift = Session.Character.Timespace.GiftItems.ElementAtOrDefault(index: i);
                            repay += gift == null ? "-1.0.0 " : $"{gift.VNum}.0.{gift.Amount} ";
                            if (gift != null)
                                Session.Character.GiftAdd(itemVNum: gift.VNum, amount: gift.Amount, design: gift.Design,
                                    forceRandom: gift.IsRandomRare);
                        }

                        // TODO: Add HasAlreadyDone
                        for (var i = 0; i < 2; i++)
                        {
                            var gift = Session.Character.Timespace.SpecialItems.ElementAtOrDefault(index: i);
                            repay += gift == null ? "-1.0.0 " : $"{gift.VNum}.0.{gift.Amount} ";
                            if (gift != null)
                                Session.Character.GiftAdd(itemVNum: gift.VNum, amount: gift.Amount, design: gift.Design,
                                    forceRandom: gift.IsRandomRare);
                        }

                        if (Session.Character.Timespace.DrawItems.Count > 0)
                            repay +=
                                $"{Session.Character.Timespace.DrawItems[index: rand].VNum}.0.{Session.Character.Timespace.DrawItems[index: rand].Amount}";
                        else
                            repay +=
                                "-1.0.0";
                        Session.SendPacket(packet: repay);
                        Session.Character.Timespace.FirstMap.InstanceBag.EndState = 6;
                    }
            }
        }

        /// <summary>
        ///     treq packet
        /// </summary>
        /// <param name="treqPacket"></param>
        public void GetTreq(TreqPacket treqPacket)
        {
            var timespace = Session.CurrentMapInstance.ScriptedInstances
                .Find(match: s => treqPacket.X == s.PositionX && treqPacket.Y == s.PositionY).Copy();

            if (timespace != null)
            {
                if (treqPacket.StartPress == 1 || treqPacket.RecordPress == 1)
                    Session.Character.EnterInstance(input: timespace);
                else
                    Session.SendPacket(packet: timespace.GenerateRbr());
            }
        }

        /// <summary>
        ///     wreq packet
        /// </summary>
        /// <param name="packet"></param>
        public void GetWreq(WreqPacket packet)
        {
            var CharPositionX = Session.Character.PositionX;
            var CharPositionY = Session.Character.PositionY;
            foreach (var portal in Session.CurrentMapInstance.ScriptedInstances)
                if (CharPositionY >= portal.PositionY - 1 && CharPositionY
                                                          <= portal.PositionY + 1
                                                          && CharPositionX
                                                          >= portal.PositionX - 1
                                                          && CharPositionX
                                                          <= portal.PositionX + 1)
                    switch (packet.Value)
                    {
                        case 0:
                            if (packet.Param != 1
                                && Session.Character.Group?.Sessions.Find(predicate: s =>
                                    s.CurrentMapInstance.InstanceBag?.Lock == false
                                    && s.CurrentMapInstance.MapInstanceType == MapInstanceType.TimeSpaceInstance
                                    && s.Character.CharacterId != Session.Character.CharacterId
                                    && s.Character.Timespace?.Id == portal.Id
                                    && !s.Character.Timespace.IsIndividual) is ClientSession TeamMemberInInstance)
                            {
                                if (portal.DailyEntries > 0)
                                {
                                    var entries = portal.DailyEntries - Session.Character.GeneralLogs.CountLinq(
                                        predicate: s =>
                                            s.LogType == "InstanceEntry" &&
                                            short.Parse(s: s.LogData) == portal.Id &&
                                            s.Timestamp.Date == DateTime.Today);
                                    Session.SendPacket(packet: Session.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "INSTANCE_ENTRIES"),
                                            arg0: entries),
                                        type: 10));
                                }

                                Session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                    dialog:
                                    $"#wreq^3^{TeamMemberInInstance.Character.CharacterId} #wreq^0^1 {Language.Instance.GetMessageFromKey(key: "ASK_JOIN_TEAM_TS")}"));
                            }
                            else
                            {
                                if (portal.DailyEntries > 0)
                                {
                                    var entries = portal.DailyEntries - Session.Character.GeneralLogs.CountLinq(
                                        predicate: s =>
                                            s.LogType == "InstanceEntry" &&
                                            short.Parse(s: s.LogData) == portal.Id &&
                                            s.Timestamp.Date == DateTime.Today);
                                    Session.SendPacket(packet: Session.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "INSTANCE_ENTRIES"),
                                            arg0: entries),
                                        type: 10));
                                }

                                Session.SendPacket(packet: portal.GenerateRbr());
                            }

                            break;

                        case 1:
                            if (!packet.Param.HasValue) Session.Character.EnterInstance(input: portal);
                            break;

                        case 3:
                            var clientSession =
                                Session.Character.Group?.Sessions.Find(predicate: s =>
                                    s.Character.CharacterId == packet.Param);
                            if (clientSession != null && clientSession.CurrentMapInstance.InstanceBag?.Lock == false &&
                                clientSession.Character?.Timespace is ScriptedInstance TeamTimeSpace &&
                                !TeamTimeSpace.IsIndividual)
                            {
                                if (portal.Id == TeamTimeSpace.Id)
                                {
                                    if (Session.Character.Level < TeamTimeSpace.LevelMinimum)
                                    {
                                        Session.SendPacket(
                                            packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "TOO_LOW_LVL"),
                                                type: 0));
                                        return;
                                    }

                                    if (Session.Character.Level > TeamTimeSpace.LevelMaximum)
                                    {
                                        Session.SendPacket(
                                            packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "TOO_HIGH_LVL"),
                                                type: 0));
                                        return;
                                    }

                                    var entries = TeamTimeSpace.DailyEntries -
                                                  Session.Character.GeneralLogs.CountLinq(predicate: s =>
                                                      s.LogType == "InstanceEntry" &&
                                                      short.Parse(s: s.LogData) == TeamTimeSpace.Id &&
                                                      s.Timestamp.Date == DateTime.Today);
                                    if (TeamTimeSpace.DailyEntries == 0 || entries > 0)
                                    {
                                        foreach (var gift in TeamTimeSpace.RequiredItems)
                                        {
                                            if (Session.Character.Inventory.CountItem(itemVNum: gift.VNum) <
                                                gift.Amount)
                                            {
                                                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                    message: string.Format(
                                                        format: Language.Instance.GetMessageFromKey(
                                                            key: "NO_ITEM_REQUIRED"),
                                                        arg0: ServerManager.GetItem(vnum: gift.VNum).Name), type: 0));
                                                return;
                                            }

                                            Session.Character.Inventory.RemoveItemAmount(vnum: gift.VNum,
                                                amount: gift.Amount);
                                        }

                                        Session?.SendPackets(packets: TeamTimeSpace.GenerateMinimap());
                                        Session?.SendPacket(packet: TeamTimeSpace.GenerateMainInfo());
                                        Session?.SendPacket(packet: TeamTimeSpace.FirstMap.InstanceBag.GenerateScore());
                                        if (TeamTimeSpace.StartX != 0 || TeamTimeSpace.StartY != 0)
                                            ServerManager.Instance.ChangeMapInstance(
                                                characterId: Session.Character.CharacterId,
                                                mapInstanceId: clientSession.CurrentMapInstance.MapInstanceId,
                                                mapX: TeamTimeSpace.StartX,
                                                mapY: TeamTimeSpace.StartY);
                                        else
                                            ServerManager.Instance.TeleportOnRandomPlaceInMap(session: Session,
                                                guid: clientSession.CurrentMapInstance.MapInstanceId);
                                        Session.Character.Timespace = TeamTimeSpace;
                                    }
                                    else
                                    {
                                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(
                                                key: "INSTANCE_NO_MORE_ENTRIES"), type: 0));
                                        Session.SendPacket(packet: Session.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(
                                                key: "INSTANCE_NO_MORE_ENTRIES"), type: 10));
                                    }
                                }
                            }
                            else
                            {
                                GetWreq(packet: new WreqPacket { Value = 0, Param = 1 });
                            }

                            // TODO: Implement
                            break;
                    }
        }

        /// <summary>
        ///     GitPacket packet
        /// </summary>
        /// <param name="packet"></param>
        public void Git(GitPacket packet)
        {
            var button = Session.CurrentMapInstance.Buttons.Find(match: s => s.MapButtonId == packet.ButtonId);
            if (button != null)
            {
                if (Session.Character.IsVehicled)
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "CANT_DO_VEHICLED"), type: 10));
                    return;
                }

                Session.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Object,
                    callerId: button.MapButtonId));
                button.RunAction();
                Session.CurrentMapInstance.Broadcast(packet: button.GenerateIn());
            }
        }

        /// <summary>
        ///     rxitPacket packet
        /// </summary>
        /// <param name="rxitPacket"></param>
        public void InstanceExit(RaidExitPacket rxitPacket)
        {
            if (rxitPacket?.State == 1)
            {
                if (Session.CurrentMapInstance?.MapInstanceType == MapInstanceType.TimeSpaceInstance &&
                    Session.Character.Timespace != null)
                {
                    if (Session.CurrentMapInstance.InstanceBag.Lock)
                    {
                        //5seed
                        if (Session.Character.Inventory.CountItem(itemVNum: 1012) >= 5)
                        {
                            Session.CurrentMapInstance.InstanceBag.DeadList.Add(item: Session.Character.CharacterId);
                            Session.Character.Dignity -= 20;
                            if (Session.Character.Dignity < -1000) Session.Character.Dignity = -1000;
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "DIGNITY_LOST"), arg0: 20),
                                    type: 11));
                            Session.Character.Inventory.RemoveItemAmount(vnum: 1012, amount: 5);
                        }
                        else
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "NO_ITEM_REQUIRED"),
                                    arg0: ServerManager.GetItem(vnum: 1012).Name), type: 0));
                            return;
                        }
                    }

                    ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: Session.Character.MapId,
                        mapX: Session.Character.MapX, mapY: Session.Character.MapY);
                }

                if (Session.CurrentMapInstance?.MapInstanceType == MapInstanceType.RaidInstance)
                {
                    /*if (Session.CurrentMapInstance.InstanceBag.Lock)
                    {
                        //5seed
                        if (Session.Character.Inventory.CountItem(1012) >= 5)
                        {
                            Session.CurrentMapInstance.InstanceBag.DeadList.Add(Session.Character.CharacterId);
                            Session.Character.Dignity -= 20;
                            if (Session.Character.Dignity < -1000)
                            {
                                Session.Character.Dignity = -1000;
                            }
                            Session.SendPacket(
                                Session.Character.GenerateSay(
                                    string.Format(Language.Instance.GetMessageFromKey("DIGNITY_LOST"), 20), 11));
                            Session.Character.Inventory.RemoveItemAmount(1012, 5);
                        }
                        else
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                                string.Format(Language.Instance.GetMessageFromKey("NO_ITEM_REQUIRED"),
                                    ServerManager.GetItem(1012).Name), 0));
                            return;
                        }

                    }
                    else
                    {
                        //1seed
                    }*/
                    ServerManager.Instance.GroupLeave(session: Session);
                }
                else if (Session.CurrentMapInstance?.MapInstanceType == MapInstanceType.TalentArenaMapInstance)
                {
                    Session.Character.LeaveTalentArena(surrender: true);
                    ServerManager.Instance.TeleportOnRandomPlaceInMap(session: Session,
                        guid: ServerManager.Instance.ArenaInstance.MapInstanceId);
                }
            }
        }

        public void SearchName(TawPacket packet)
        {
            var at = ServerManager.Instance.ArenaTeams.ToList().FirstOrDefault(predicate: s =>
                s.Any(predicate: o =>
                    o.Session?.Character?.Name == packet.Username && Session.CurrentMapInstance != null));
            if (at != null)
            {
                ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                    mapInstanceId: at.FirstOrDefault(predicate: s => s.Session != null).Session.CurrentMapInstance
                        .MapInstanceId, mapX: 69, mapY: 100);

                var zenas = at.OrderBy(keySelector: s => s.Order).FirstOrDefault(predicate: s =>
                    s.Session != null && !s.Dead && s.ArenaTeamType == ArenaTeamType.Zenas);
                var erenia = at.OrderBy(keySelector: s => s.Order).FirstOrDefault(predicate: s =>
                    s.Session != null && !s.Dead && s.ArenaTeamType == ArenaTeamType.Erenia);
                Session.SendPacket(packet: Session.Character.GenerateTaM(type: 0));
                Session.SendPacket(packet: Session.Character.GenerateTaM(type: 3));
                Session.SendPacket(packet: "taw_sv 0");
                Session.SendPacket(packet: zenas?.Session.Character.GenerateTaP(tatype: 0, showOponent: true));
                Session.SendPacket(packet: erenia?.Session.Character.GenerateTaP(tatype: 2, showOponent: true));
                Session.SendPacket(packet: zenas?.Session.Character.GenerateTaFc(type: 0));
                Session.SendPacket(packet: erenia?.Session.Character.GenerateTaFc(type: 1));
            }
            else
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND_IN_ARENA")));
            }
        }

        #endregion
    }
}