using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Handling;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Battle;
using OpenNos.GameObject.Buff;
using OpenNos.GameObject.Event.ACT4;
using OpenNos.GameObject.Event.ICEBREAKER;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using static OpenNos.Domain.BCardType;

namespace OpenNos.Handler
{
    public class BattlePacketHandler : IPacketHandler
    {
        #region Instantiation

        public BattlePacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     mtlist packet
        /// </summary>
        /// <param name="multiTargetListPacket"></param>
        public void MultiTargetListHit(MultiTargetListPacket multiTargetListPacket)
        {
            if (multiTargetListPacket?.Targets == null || Session?.Character?.MapInstance == null) return;

            if (Session.Character.IsVehicled || Session.Character.MuteMessage())
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel());
                return;
            }

            if ((DateTime.Now - Session.Character.LastTransform).TotalSeconds < 3)
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel());
                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "CANT_ATTACK"),
                    type: 0));
                return;
            }

            if (multiTargetListPacket.TargetsAmount <= 0
                || multiTargetListPacket.TargetsAmount != multiTargetListPacket.Targets.Count)
                return;

            Session.Character.MTListTargetQueue.Clear();

            foreach (var subPacket in multiTargetListPacket.Targets)
                Session.Character.MTListTargetQueue.Push(item: new MtListHitTarget(entityType: subPacket.TargetType,
                    targetId: subPacket.TargetId,
                    targetHitType: TargetHitType.AoeTargetHit));
        }

        /// <summary>
        ///     u_s packet
        /// </summary>
        /// <param name="useSkillPacket"></param>
        public void UseSkill(UseSkillPacket useSkillPacket)
        {
            if (Session.Character.MapInstance == null)
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel());
                return;
            }

            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT"), type: 0));
                Session.SendPacket(packet: StaticPacketHelper.Cancel());
                return;
            }

            Session.Character.WalkDisposable?.Dispose();
            Session.Character.Direction = Session.Character.BeforeDirection;

            switch (useSkillPacket.UserType)
            {
                case UserType.Npc:
                    {
                        var target = Session.Character.MapInstance.GetNpc(mapNpcId: useSkillPacket.MapMonsterId);

                        if (target != null)
                            if (Session.Character.Morph == 1000099 /* Hamster */ && target.NpcVNum == 2329
                                || Session.Character.Morph == 1000156 /* Bushtail */ && target.NpcVNum == 2330)
                            {
                                Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
                                Session.SendPacket(packet: $"delay 1000 13 #guri^513^2^{target.MapNpcId}");
                                Session.Character.MapInstance.Broadcast(
                                    packet: UserInterfaceHelper.GenerateGuri(type: 2, argument: 1,
                                        callerId: Session.Character.CharacterId),
                                    receiver: ReceiverType.AllExceptMe);
                                return;
                            }
                    }
                    break;
            }

            if (!Session.Character.CanAttack())
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel());
                return;
            }

            switch (useSkillPacket.UserType)
            {
                case UserType.Player:
                    {
                        var target = ServerManager.Instance
                            .GetSessionByCharacterId(characterId: useSkillPacket.MapMonsterId)?.Character;

                        if (target != null && target.CharacterId != Session.Character.CharacterId)
                            if (target.HasBuff(type: CardType.FrozenDebuff,
                                subtype: (byte)AdditionalTypes.FrozenDebuff.EternalIce))
                            {
                                Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
                                Session.SendPacket(packet: $"delay 2000 5 #guri^502^1^{target.CharacterId}");
                                Session.Character.MapInstance.Broadcast(
                                    packet: UserInterfaceHelper.GenerateGuri(type: 2, argument: 1,
                                        callerId: Session.Character.CharacterId),
                                    receiver: ReceiverType.AllExceptMe);
                                return;
                            }
                    }
                    break;
            }

            if (Session.Character.IsLaurenaMorph())
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel());
                return;
            }

            if (Session.Character.CanFight && useSkillPacket != null && !Session.Character.IsSeal)
            {
                if (useSkillPacket.UserType == UserType.Monster)
                {
                    var monsterToAttack =
                        Session.Character.MapInstance.GetMonsterById(mapMonsterId: useSkillPacket.MapMonsterId);
                    if (monsterToAttack != null)
                        if (Session.Character.Quests.Any(predicate: q =>
                            q.Quest.QuestType == (int)QuestType.Required &&
                            q.Quest.QuestObjectives.Any(predicate: s => s.Data == monsterToAttack.MonsterVNum)))
                        {
                            Session.Character.IncrementQuests(type: QuestType.Required,
                                firstData: monsterToAttack.MonsterVNum);
                            Session.SendPacket(packet: StaticPacketHelper.Cancel());
                            return;
                        }
                }

                var skills = Session.Character.GetSkills();

                if (skills != null)
                {
                    var ski = skills.FirstOrDefault(predicate: s =>
                        s?.Skill?.CastId == useSkillPacket.CastId &&
                        (s.Skill?.UpgradeSkill == 0 || s.Skill?.SkillType == 1));

                    if (ski != null)
                    {
                        if (ski.GetSkillBCards().ToList().Any(predicate: s =>
                            s.Type.Equals(obj: (byte)CardType.MeditationSkill)
                            && s.SubType.Equals(
                                obj: (byte)AdditionalTypes.MeditationSkill
                                    .Sacrifice / 10)))
                        {
                            if (Session.Character.MapInstance.BattleEntities.ToList().FirstOrDefault(predicate: s =>
                                    s.UserType == useSkillPacket.UserType &&
                                    s.MapEntityId == useSkillPacket.MapMonsterId)
                                is BattleEntity targetEntity)
                                if (Session.Character.BattleEntity.CanAttackEntity(receiver: targetEntity))
                                {
                                    Session.SendPacket(packet: StaticPacketHelper.Cancel());
                                    return;
                                }
                        }
                        else if (!(ski.Skill.TargetType == 1 && ski.Skill.HitType != 1)
                                 && !(ski.Skill.TargetType == 2 && ski.Skill.HitType == 0)
                                 && !(ski.Skill.TargetType == 1 && ski.Skill.HitType == 1))
                        {
                            if (Session.Character.MapInstance.BattleEntities.ToList().FirstOrDefault(predicate: s =>
                                    s.UserType == useSkillPacket.UserType &&
                                    s.MapEntityId == useSkillPacket.MapMonsterId)
                                is BattleEntity targetEntity)
                                if (!Session.Character.BattleEntity.CanAttackEntity(receiver: targetEntity))
                                {
                                    Session.SendPacket(packet: StaticPacketHelper.Cancel());
                                    return;
                                }
                        }
                    }
                }

                Session.Character.RemoveBuff(cardId: 614);
                Session.Character.RemoveBuff(cardId: 615);
                Session.Character.RemoveBuff(cardId: 616);

                var isMuted = Session.Character.MuteMessage();

                if (isMuted || Session.Character.IsVehicled || Session.Character.InvisibleGm)
                {
                    Session.SendPacket(packet: StaticPacketHelper.Cancel());
                    return;
                }

                if (useSkillPacket.MapX.HasValue && useSkillPacket.MapY.HasValue)
                {
                    Session.Character.PositionX = useSkillPacket.MapX.Value;
                    Session.Character.PositionY = useSkillPacket.MapY.Value;
                }

                if (Session.Character.IsSitting) Session.Character.Rest();

                switch (useSkillPacket.UserType)
                {
                    case UserType.Npc:
                    case UserType.Monster:
                        if (Session.Character.Hp > 0)
                            TargetHit(castingId: useSkillPacket.CastId, targetType: useSkillPacket.UserType,
                                targetId: useSkillPacket.MapMonsterId);

                        break;

                    case UserType.Player:
                        if (Session.Character.Hp > 0)
                        {
                            if (useSkillPacket.MapMonsterId != Session.Character.CharacterId)
                                TargetHit(castingId: useSkillPacket.CastId, targetType: useSkillPacket.UserType,
                                    targetId: useSkillPacket.MapMonsterId,
                                    isPvp: true);
                            else
                                TargetHit(castingId: useSkillPacket.CastId, targetType: useSkillPacket.UserType,
                                    targetId: useSkillPacket.MapMonsterId);
                        }
                        else
                        {
                            Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
                        }

                        break;

                    default:
                        Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
                        return;
                }

                if (skills != null)
                {
                    var ski = skills.FirstOrDefault(predicate: s => s?.Skill?.CastId == useSkillPacket.CastId);

                    if (ski == null || !(ski.Skill.TargetType == 1 && ski.Skill.HitType == 1))
                    {
                        if (Session.Character.MapInstance.BattleEntities.FirstOrDefault(predicate: s =>
                            s.MapEntityId == useSkillPacket.MapMonsterId) is BattleEntity target)
                        {
                            if (target.Hp <= 0) Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
                        }
                        else
                        {
                            Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
                        }
                    }
                }
            }
            else
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
            }
        }

        /// <summary>
        ///     u_as packet
        /// </summary>
        /// <param name="useAoeSkillPacket"></param>
        public void UseZonesSkill(UseAOESkillPacket useAoeSkillPacket)
        {
            Session.Character.Direction = Session.Character.BeforeDirection;

            var isMuted = Session.Character.MuteMessage();
            if (isMuted || Session.Character.IsVehicled)
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel());
            }
            else
            {
                if (Session.Character.LastTransform.AddSeconds(value: 3) > DateTime.Now)
                {
                    Session.SendPacket(packet: StaticPacketHelper.Cancel());
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "CANT_ATTACK"), type: 0));
                    return;
                }

                if (Session.Character.CanFight && Session.Character.Hp > 0)
                    ZoneHit(castingId: useAoeSkillPacket.CastId, x: useAoeSkillPacket.MapX, y: useAoeSkillPacket.MapY);
            }
        }

        /// <summary>
        ///     ob_a packet
        /// </summary>
        /// <param name="useIconFalconSkillPacket"></param>
        public void UseIconFalconSkill(UseIconFalconSkillPacket useIconFalconSkillPacket)
        {
            if (useIconFalconSkillPacket == null)
                throw new ArgumentNullException(paramName: nameof(useIconFalconSkillPacket));
            if (Session.Character.BattleEntity.FalconFocusedEntityId > 0)
            {
                var iconSkillHitRequest = new HitRequest(targetHitType: TargetHitType.SingleTargetHit, session: Session,
                    skill: ServerManager.GetSkill(skillVNum: 1248), skillEffect: 4283);
                if (Session.CurrentMapInstance.BattleEntities.FirstOrDefault(predicate: s =>
                        s.MapEntityId == Session.Character.BattleEntity.FalconFocusedEntityId) is BattleEntity
                    FalconFocusedEntity)
                {
                    Session.SendPacket(packet: "ob_ar");
                    switch (FalconFocusedEntity.EntityType)
                    {
                        case EntityType.Player:
                            PvpHit(hitRequest: iconSkillHitRequest, target: FalconFocusedEntity.Character.Session);
                            break;

                        case EntityType.Monster:
                            FalconFocusedEntity.MapMonster.HitQueue.Enqueue(item: iconSkillHitRequest);
                            break;

                        case EntityType.Mate:
                            FalconFocusedEntity.Mate.HitRequest(hitRequest: iconSkillHitRequest);
                            break;
                    }

                    Session.CurrentMapInstance.Broadcast(client: Session,
                        content:
                        $"eff_ob  {(byte)FalconFocusedEntity.UserType} {FalconFocusedEntity.MapEntityId} 0 4269",
                        receiver: ReceiverType.AllExceptMe);
                }
            }
        }

        void PvpHit(HitRequest hitRequest, ClientSession target)
        {
            if (target?.Character.Hp > 0 && hitRequest?.Session.Character.Hp > 0)
            {
                if (target.Character.IsSitting) target.Character.Rest();

                double cooldownReduction = Session.Character.GetBuff(type: CardType.Morale,
                    subtype: (byte)AdditionalTypes.Morale.SkillCooldownDecreased)[0];

                var increaseEnemyCooldownChance = Session.Character.GetBuff(type: CardType.DarkCloneSummon,
                    subtype: (byte)AdditionalTypes.DarkCloneSummon.IncreaseEnemyCooldownChance);

                if (ServerManager.RandomNumber() < increaseEnemyCooldownChance[0])
                    cooldownReduction -= increaseEnemyCooldownChance[1];

                var hitmode = 0;
                var onyxWings = false;
                var battleEntity = new BattleEntity(character: hitRequest.Session.Character, skill: hitRequest.Skill);
                var battleEntityDefense = new BattleEntity(character: target.Character, skill: null);
                var damage = DamageHelper.Instance.CalculateDamage(attacker: battleEntity,
                    defender: battleEntityDefense, skill: hitRequest.Skill,
                    hitMode: ref hitmode, onyxWings: ref onyxWings);
                if (target.Character.HasGodMode)
                {
                    damage = 0;
                    hitmode = 4;
                }
                else if (target.Character.LastPVPRevive > DateTime.Now.AddSeconds(value: -10)
                         || hitRequest.Session.Character.LastPVPRevive > DateTime.Now.AddSeconds(value: -10))
                {
                    damage = 0;
                    hitmode = 4;
                }

                if (ServerManager.RandomNumber() < target.Character.GetBuff(type: CardType.DarkCloneSummon,
                    subtype: (byte)AdditionalTypes.DarkCloneSummon.ConvertDamageToHpChance)[0])
                {
                    var amount = damage / 2;

                    target.Character.ConvertedDamageToHP += amount;
                    target.Character.MapInstance?.Broadcast(
                        packet: target.Character.GenerateRc(characterHealth: amount));
                    target.Character.Hp += amount;

                    if (target.Character.Hp > target.Character.HPLoad())
                        target.Character.Hp = (int)target.Character.HPLoad();

                    target.SendPacket(packet: target.Character.GenerateStat());

                    damage = 0;
                }

                if (hitmode != 4 && hitmode != 2 && damage > 0)
                {
                    Session.Character.RemoveBuffByBCardTypeSubType(bcardTypes: new List<KeyValuePair<byte, byte>>
                    {
                        new KeyValuePair<byte, byte>(key: (byte) CardType.SpecialActions,
                            value: (byte) AdditionalTypes.SpecialActions.Hide)
                    });
                    target.Character.RemoveBuffByBCardTypeSubType(bcardTypes: new List<KeyValuePair<byte, byte>>
                    {
                        new KeyValuePair<byte, byte>(key: (byte) CardType.SpecialActions,
                            value: (byte) AdditionalTypes.SpecialActions.Hide)
                    });
                    target.Character.RemoveBuff(cardId: 36);
                    target.Character.RemoveBuff(cardId: 548);
                }

                if (Session.Character.Buff.FirstOrDefault(predicate: s => s.Card.BCards.Any(predicate: b =>
                    b.Type == (byte)CardType.FalconSkill &&
                    b.SubType.Equals(obj: (byte)AdditionalTypes.FalconSkill.Hide / 10))) is Buff FalconHideBuff)
                {
                    Session.Character.RemoveBuff(cardId: FalconHideBuff.Card.CardId);
                    Session.Character.AddBuff(indicator: new Buff(id: 560, level: Session.Character.Level),
                        sender: Session.Character.BattleEntity);
                }

                var manaShield = target.Character.GetBuff(type: CardType.LightAndShadow,
                    subtype: (byte)AdditionalTypes.LightAndShadow.InflictDamageToMp);
                if (manaShield[0] != 0 && hitmode != 4)
                {
                    var reduce = damage / 100 * manaShield[0];
                    if (target.Character.Mp < reduce)
                    {
                        reduce = target.Character.Mp;
                        target.Character.Mp = 0;
                    }
                    else
                    {
                        target.Character.DecreaseMp(amount: reduce);
                    }

                    damage -= reduce;
                }

                if (onyxWings && hitmode != 4 && hitmode != 2)
                {
                    var onyxX = (short)(hitRequest.Session.Character.PositionX + 2);
                    var onyxY = (short)(hitRequest.Session.Character.PositionY + 2);
                    var onyxId = target.CurrentMapInstance.GetNextMonsterId();
                    var onyx = new MapMonster
                    {
                        MonsterVNum = 2371,
                        MapX = onyxX,
                        MapY = onyxY,
                        MapMonsterId = onyxId,
                        IsHostile = false,
                        IsMoving = false,
                        ShouldRespawn = false
                    };
                    target.CurrentMapInstance.Broadcast(packet: UserInterfaceHelper.GenerateGuri(type: 31, argument: 1,
                        callerId: hitRequest.Session.Character.CharacterId, value: onyxX, value2: onyxY));
                    onyx.Initialize(currentMapInstance: target.CurrentMapInstance);
                    target.CurrentMapInstance.AddMonster(monster: onyx);
                    target.CurrentMapInstance.Broadcast(packet: onyx.GenerateIn());
                    target.Character.GetDamage(damage: (int)(damage / 2D), damager: battleEntity);
                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 350)).Subscribe(onNext: o =>
                    {
                        target.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: UserType.Monster,
                            callerId: onyxId, secondaryType: 1,
                            targetId: target.Character.CharacterId, skillVNum: -1, cooldown: 0, attackAnimation: -1,
                            skillEffect: hitRequest.Skill.Effect, x: -1, y: -1, isAlive: true, health: 92,
                            damage: (int)(damage / 2D), hitmode: 0, skillType: 0));
                        target.CurrentMapInstance.RemoveMonster(monsterToRemove: onyx);
                        target.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                            callerId: onyx.MapMonsterId));
                    });
                }

                target.Character.GetDamage(damage: damage / 2, damager: battleEntity);
                target.SendPacket(packet: target.Character.GenerateStat());

                // Magical Fetters

                if (damage > 0)
                    if (target.Character.HasMagicalFetters)
                    {
                        // Magic Spell

                        target.Character.AddBuff(indicator: new Buff(id: 617, level: target.Character.Level),
                            sender: target.Character.BattleEntity);

                        var castId = 10 + Session.Character.Element;

                        if (castId == 10) castId += 5; // No element

                        target.Character.LastComboCastId = castId;
                        target.SendPacket(packet: $"mslot {castId} -1");
                    }

                var isAlive = target.Character.Hp > 0;
                if (!isAlive && target.HasCurrentMapInstance)
                {
                    if (target.Character.IsVehicled) target.Character.RemoveVehicle();

                    if (hitRequest.Session.Character != null && hitRequest.SkillBCards.FirstOrDefault(predicate: s =>
                        s.Type == (byte)CardType.TauntSkill &&
                        s.SubType == (byte)AdditionalTypes.TauntSkill.EffectOnKill / 10) is BCard EffectOnKill)
                        if (ServerManager.RandomNumber() < EffectOnKill.FirstData)
                            hitRequest.Session.Character.AddBuff(
                                indicator: new Buff(id: (short)EffectOnKill.SecondData,
                                    level: hitRequest.Session.Character.Level),
                                sender: hitRequest.Session.Character.BattleEntity);

                    target.Character.LastPvPKiller = Session;
                    if (target.CurrentMapInstance.Map?.MapTypes.Any(predicate: s =>
                            s.MapTypeId == (short)MapTypeEnum.Act4)
                        == true)
                    {
                        if (ServerManager.Instance.ChannelId == 51 && ServerManager.Instance.Act4DemonStat.Mode == 0
                                                                   && ServerManager.Instance.Act4AngelStat.Mode == 0)
                            switch (Session.Character.Faction)
                            {
                                case FactionType.Angel:
                                    ServerManager.Instance.Act4AngelStat.Percentage += 100;
                                    break;

                                case FactionType.Demon:
                                    ServerManager.Instance.Act4DemonStat.Percentage += 100;
                                    break;
                            }

                        hitRequest.Session.Character.Act4Kill++;
                        target.Character.Act4Dead++;
                        target.Character.GetAct4Points(point: -1);
                        if (target.Character.Level + 10 >= hitRequest.Session.Character.Level
                            && hitRequest.Session.Character.Level <= target.Character.Level - 10)
                            hitRequest.Session.Character.GetAct4Points(point: 2);

                        if (target.Character.Session.CleanIpAddress != hitRequest.Session.CleanIpAddress)
                        {
                            var levelDifference = target.Character.Level - hitRequest.Session.Character.Level;

                            if (levelDifference < 30)
                            {
                                var ReputationValue = 0;

                                if (levelDifference >= 0)
                                    ReputationValue = 500 + levelDifference * 100;
                                else if (levelDifference > -20)
                                    ReputationValue = 500 - levelDifference * 25;
                                else
                                    ReputationValue -= 150 + -levelDifference * 10;

                                ReputationValue *= ServerManager.Instance.Configuration.RateReputation;

                                if (ReputationValue > 0)
                                {
                                    hitRequest.Session.Character.Reputation += ReputationValue;
                                    hitRequest.Session.SendPacket(packet: hitRequest.Session.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "WIN_REPUT"),
                                            arg0: (short)ReputationValue), type: 12));

                                    var act4RaidPenalty =
                                        target.Character.Faction == FactionType.Angel &&
                                        ServerManager.Instance.Act4DemonStat.Mode == 3
                                        || target.Character.Faction == FactionType.Demon &&
                                        ServerManager.Instance.Act4AngelStat.Mode == 3
                                            ? 2
                                            : 1;

                                    target.Character.Reputation -= ReputationValue /* * act4RaidPenalty*/;
                                    target.SendPacket(packet: target.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "LOSE_REP"),
                                            arg0: (short)ReputationValue /* * act4RaidPenalty*/), type: 11));
                                }
                                else
                                {
                                    hitRequest.Session.Character.Reputation -= ReputationValue;
                                    hitRequest.Session.SendPacket(packet: hitRequest.Session.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "LOSE_REP"),
                                            arg0: (short)ReputationValue), type: 11));
                                }

                                hitRequest.Session.SendPacket(packet: hitRequest.Session.Character.GenerateLev());
                            }
                            else
                            {
                                hitRequest.Session.SendPacket(
                                    packet: hitRequest.Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "TOO_LEVEL_DIFFERENCE"),
                                        type: 11));
                            }
                        }
                        else
                        {
                            hitRequest.Session.SendPacket(
                                packet: hitRequest.Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "TARGET_SAME_IP"), type: 11));
                        }

                        foreach (var sess in ServerManager.Instance.Sessions.Where(
                            predicate: s => s.HasSelectedCharacter))
                            if (sess.Character.Faction == Session.Character.Faction)
                                sess.SendPacket(packet: sess.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(
                                            key: $"ACT4_PVP_KILL{(int)target.Character.Faction}"),
                                        arg0: Session.Character.Name),
                                    type: 12));
                            else if (sess.Character.Faction == target.Character.Faction)
                                sess.SendPacket(packet: sess.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(
                                            key: $"ACT4_PVP_DEATH{(int)target.Character.Faction}"),
                                        arg0: target.Character.Name),
                                    type: 11));

                        target.SendPacket(packet: target.Character.GenerateFd());
                        target.CurrentMapInstance?.Broadcast(client: target,
                            content: target.Character.GenerateIn(InEffect: 1),
                            receiver: ReceiverType.AllExceptMe);
                        target.CurrentMapInstance?.Broadcast(client: target, content: target.Character.GenerateGidx(),
                            receiver: ReceiverType.AllExceptMe);
                        hitRequest.Session.SendPacket(packet: hitRequest.Session.Character.GenerateFd());
                        hitRequest.Session.CurrentMapInstance?.Broadcast(client: hitRequest.Session,
                            content: hitRequest.Session.Character.GenerateIn(InEffect: 1),
                            receiver: ReceiverType.AllExceptMe);
                        hitRequest.Session.CurrentMapInstance?.Broadcast(client: hitRequest.Session,
                            content: hitRequest.Session.Character.GenerateGidx(), receiver: ReceiverType.AllExceptMe);
                        target.Character.DisableBuffs(type: BuffType.All);

                        if (target.Character.MapInstance == CaligorRaid.CaligorMapInstance)
                        {
                            ServerManager.Instance.AskRevive(characterId: target.Character.CharacterId);
                        }
                        else
                        {
                            target.SendPacket(
                                packet: target.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "ACT4_PVP_DIE"), type: 11));
                            target.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "ACT4_PVP_DIE"),
                                    type: 0));

                            Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 2000))
                                .Subscribe(onNext: o => target.Character.SetSeal());
                        }
                    }
                    else if (target.CurrentMapInstance.MapInstanceType == MapInstanceType.IceBreakerInstance)
                    {
                        if (IceBreaker.AlreadyFrozenPlayers.Contains(item: target))
                        {
                            IceBreaker.AlreadyFrozenPlayers.Remove(item: target);
                            target.CurrentMapInstance?.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_PLAYER_OUT"),
                                    arg0: target?.Character?.Name), type: 0));
                            target.Character.Hp = 1;
                            target.Character.Mp = 1;
                            var respawn = target?.Character?.Respawn;
                            ServerManager.Instance.ChangeMap(id: target.Character.CharacterId,
                                mapId: respawn.DefaultMapId);
                            Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2,
                                callerId: target.Character.CharacterId));
                        }
                        else
                        {
                            isAlive = true;
                            IceBreaker.FrozenPlayers.Add(item: target);
                            target.CurrentMapInstance?.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_PLAYER_FROZEN"),
                                    arg0: target?.Character?.Name), type: 0));
                            Task.Run(action: () =>
                            {
                                target.Character.Hp = (int)target.Character.HPLoad();
                                target.Character.Mp = (int)target.Character.MPLoad();
                                target.SendPacket(packet: target?.Character?.GenerateStat());
                                target.Character.NoMove = true;
                                target.Character.NoAttack = true;
                                target.SendPacket(packet: target?.Character?.GenerateCond());
                                while (IceBreaker.FrozenPlayers.Contains(item: target))
                                {
                                    target?.CurrentMapInstance?.Broadcast(
                                        packet: target?.Character?.GenerateEff(effectid: 35));
                                    Thread.Sleep(millisecondsTimeout: 1000);
                                }
                            });
                        }
                    }
                    else
                    {
                        hitRequest.Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "PVP_KILL"),
                                arg0: hitRequest.Session.Character.Name, arg1: target.Character.Name), type: 10));
                        if (target.Character.IsVehicled) target.Character.RemoveVehicle();
                        Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000)).Subscribe(onNext: o =>
                            ServerManager.Instance.AskPvpRevive(characterId: target.Character.CharacterId));
                    }
                }

                battleEntity.BCards.Where(predicate: s => s.CastType == 1).ForEach(action: s =>
                {
                    if (s.Type != (byte)CardType.Buff)
                        s.ApplyBCards(session: target.Character.BattleEntity, sender: Session.Character.BattleEntity);
                });

                hitRequest.SkillBCards.Where(predicate: s =>
                        !s.Type.Equals(obj: (byte)CardType.Buff) && !s.Type.Equals(obj: (byte)CardType.Capture) &&
                        s.CardId == null).ToList()
                    .ForEach(action: s =>
                        s.ApplyBCards(session: target.Character.BattleEntity, sender: Session.Character.BattleEntity));

                if (hitmode != 4 && hitmode != 2)
                {
                    battleEntity.BCards.Where(predicate: s => s.CastType == 1).ForEach(action: s =>
                    {
                        if (s.Type == (byte)CardType.Buff)
                        {
                            var b = new Buff(id: (short)s.SecondData, level: battleEntity.Level);
                            if (b.Card != null)
                                switch (b.Card?.BuffType)
                                {
                                    case BuffType.Bad:
                                        s.ApplyBCards(session: target.Character.BattleEntity,
                                            sender: Session.Character.BattleEntity);
                                        break;

                                    case BuffType.Good:
                                    case BuffType.Neutral:
                                        s.ApplyBCards(session: Session.Character.BattleEntity,
                                            sender: Session.Character.BattleEntity);
                                        break;
                                }
                        }
                    });

                    battleEntityDefense.BCards.Where(predicate: s => s.CastType == 0).ForEach(action: s =>
                    {
                        if (s.Type == (byte)CardType.Buff)
                        {
                            var b = new Buff(id: (short)s.SecondData, level: battleEntityDefense.Level);
                            if (b.Card != null)
                                switch (b.Card?.BuffType)
                                {
                                    case BuffType.Bad:
                                        s.ApplyBCards(session: Session.Character.BattleEntity,
                                            sender: target.Character.BattleEntity);
                                        break;

                                    case BuffType.Good:
                                    case BuffType.Neutral:
                                        s.ApplyBCards(session: target.Character.BattleEntity,
                                            sender: target.Character.BattleEntity);
                                        break;
                                }
                        }
                    });

                    hitRequest.SkillBCards.Where(predicate: s =>
                            s.Type.Equals(obj: (byte)CardType.Buff) &&
                            new Buff(id: (short)s.SecondData, level: Session.Character.Level).Card?.BuffType ==
                            BuffType.Bad)
                        .ToList()
                        .ForEach(action: s => s.ApplyBCards(session: target.Character.BattleEntity,
                            sender: Session.Character.BattleEntity));

                    hitRequest.SkillBCards.Where(predicate: s => s.Type.Equals(obj: (byte)CardType.SniperAttack))
                        .ToList()
                        .ForEach(action: s => s.ApplyBCards(session: target.Character.BattleEntity,
                            sender: Session.Character.BattleEntity));

                    if (battleEntity?.ShellWeaponEffects != null)
                        foreach (var shell in battleEntity.ShellWeaponEffects)
                            switch (shell.Effect)
                            {
                                case (byte)ShellWeaponEffectType.Blackout:
                                    {
                                        var buff = new Buff(id: 7, level: battleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value
                                            - shell.Value
                                            * (battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                   s.Effect == (byte)ShellArmorEffectType.ReducedStun)?.Value
                                               + battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                   s.Effect == (byte)ShellArmorEffectType.ReducedAllStun)?.Value
                                               + battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                       s.Effect == (byte)ShellArmorEffectType.ReducedAllNegativeEffect)
                                                   ?.Value) / 100D)
                                            target.Character.AddBuff(indicator: buff, sender: battleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.DeadlyBlackout:
                                    {
                                        var buff = new Buff(id: 66, level: battleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value
                                            - shell.Value
                                            * (battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                   s.Effect == (byte)ShellArmorEffectType.ReducedAllStun)?.Value
                                               + battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                       s.Effect == (byte)ShellArmorEffectType.ReducedAllNegativeEffect)
                                                   ?.Value) / 100D)
                                            target.Character.AddBuff(indicator: buff, sender: battleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.MinorBleeding:
                                    {
                                        var buff = new Buff(id: 1, level: battleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value
                                            - shell.Value * (battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                                 s.Effect == (byte)ShellArmorEffectType
                                                                     .ReducedMinorBleeding)?.Value
                                                             + battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                                 s.Effect == (byte)ShellArmorEffectType
                                                                     .ReducedBleedingAndMinorBleeding)?.Value
                                                             + battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                                 s.Effect == (byte)ShellArmorEffectType
                                                                     .ReducedAllBleedingType)?.Value
                                                             + battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                                 s.Effect == (byte)ShellArmorEffectType
                                                                     .ReducedAllNegativeEffect)?.Value) / 100D)
                                            target.Character.AddBuff(indicator: buff, sender: battleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.Bleeding:
                                    {
                                        var buff = new Buff(id: 21, level: battleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value
                                            - shell.Value * (battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                                 s.Effect == (byte)ShellArmorEffectType
                                                                     .ReducedBleedingAndMinorBleeding)?.Value
                                                             + battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                                 s.Effect == (byte)ShellArmorEffectType
                                                                     .ReducedAllBleedingType)?.Value
                                                             + battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                                 s.Effect == (byte)ShellArmorEffectType
                                                                     .ReducedAllNegativeEffect)?.Value) / 100D)
                                            target.Character.AddBuff(indicator: buff, sender: battleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.HeavyBleeding:
                                    {
                                        var buff = new Buff(id: 42, level: battleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value
                                            - shell.Value * (battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                                 s.Effect == (byte)ShellArmorEffectType
                                                                     .ReducedAllBleedingType)?.Value
                                                             + battleEntityDefense.ShellArmorEffects?.Find(match: s =>
                                                                 s.Effect == (byte)ShellArmorEffectType
                                                                     .ReducedAllNegativeEffect)?.Value) / 100D)
                                            target.Character.AddBuff(indicator: buff, sender: battleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.Freeze:
                                    {
                                        var buff = new Buff(id: 27, level: battleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value - shell.Value
                                            * (battleEntityDefense
                                                   .ShellArmorEffects?.Find(
                                                       match: s =>
                                                           s.Effect ==
                                                           (byte)
                                                           ShellArmorEffectType
                                                               .ReducedFreeze)
                                                   ?.Value
                                               + battleEntityDefense
                                                   .ShellArmorEffects?.Find(
                                                       match: s =>
                                                           s.Effect ==
                                                           (byte)
                                                           ShellArmorEffectType
                                                               .ReducedAllNegativeEffect)
                                                   ?.Value) / 100D)
                                            target.Character.AddBuff(indicator: buff, sender: battleEntity);

                                        break;
                                    }
                            }
                }

                if (hitmode != 2)
                {
                    switch (hitRequest.TargetHitType)
                    {
                        case TargetHitType.SingleTargetHit:
                            hitRequest.Session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                type: UserType.Player,
                                callerId: hitRequest.Session.Character.CharacterId, secondaryType: 1,
                                targetId: target.Character.CharacterId,
                                skillVNum: hitRequest.Skill.SkillVNum,
                                cooldown: (short)(hitRequest.Skill.Cooldown +
                                                   hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                attackAnimation: hitRequest.Skill.AttackAnimation,
                                skillEffect: hitRequest.SkillEffect, x: hitRequest.Session.Character.PositionX,
                                y: hitRequest.Session.Character.PositionY, isAlive: isAlive,
                                health: (int)(target.Character.Hp / (float)target.Character.HPLoad() * 100),
                                damage: damage, hitmode: hitmode,
                                skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                            break;

                        case TargetHitType.SingleTargetHitCombo:
                            hitRequest.Session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                type: UserType.Player,
                                callerId: hitRequest.Session.Character.CharacterId, secondaryType: 1,
                                targetId: target.Character.CharacterId,
                                skillVNum: hitRequest.Skill.SkillVNum,
                                cooldown: (short)(hitRequest.Skill.Cooldown +
                                                   hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                attackAnimation: hitRequest.SkillCombo.Animation,
                                skillEffect: hitRequest.SkillCombo.Effect, x: hitRequest.Session.Character.PositionX,
                                y: hitRequest.Session.Character.PositionY, isAlive: isAlive,
                                health: (int)(target.Character.Hp / (float)target.Character.HPLoad() * 100),
                                damage: damage, hitmode: hitmode,
                                skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                            break;

                        case TargetHitType.SingleAoeTargetHit:
                            if (hitRequest.ShowTargetHitAnimation)
                            {
                                if (hitRequest.Skill.SkillVNum == 1085 || hitRequest.Skill.SkillVNum == 1091 ||
                                    hitRequest.Skill.SkillVNum == 1060)
                                {
                                    hitRequest.Session.Character.PositionX = target.Character.PositionX;
                                    hitRequest.Session.Character.PositionY = target.Character.PositionY;
                                    hitRequest.Session.CurrentMapInstance.Broadcast(packet: hitRequest.Session.Character
                                        .GenerateTp());
                                }

                                hitRequest.Session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: UserType.Player, callerId: hitRequest.Session.Character.CharacterId,
                                    secondaryType: 1,
                                    targetId: target.Character.CharacterId,
                                    skillVNum: hitRequest.Skill.SkillVNum,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.Skill.AttackAnimation,
                                    skillEffect: hitRequest.SkillEffect,
                                    x: hitRequest.Session.Character.PositionX,
                                    y: hitRequest.Session.Character.PositionY,
                                    isAlive: isAlive,
                                    health: (int)(target.Character.Hp / (float)target.Character.HPLoad() * 100),
                                    damage: damage,
                                    hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                            }
                            else
                            {
                                switch (hitmode)
                                {
                                    case 1:
                                    case 4:
                                        hitmode = 7;
                                        break;

                                    case 2:
                                        hitmode = 2;
                                        break;

                                    case 3:
                                        hitmode = 6;
                                        break;

                                    default:
                                        hitmode = 5;
                                        break;
                                }

                                hitRequest.Session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: UserType.Player, callerId: hitRequest.Session.Character.CharacterId,
                                    secondaryType: 1,
                                    targetId: target.Character.CharacterId,
                                    skillVNum: -1,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.Skill.AttackAnimation,
                                    skillEffect: hitRequest.SkillEffect,
                                    x: hitRequest.Session.Character.PositionX,
                                    y: hitRequest.Session.Character.PositionY,
                                    isAlive: isAlive,
                                    health: (int)(target.Character.Hp / (float)target.Character.HPLoad() * 100),
                                    damage: damage,
                                    hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                            }

                            break;

                        case TargetHitType.AoeTargetHit:
                            switch (hitmode)
                            {
                                case 1:
                                case 4:
                                    hitmode = 7;
                                    break;

                                case 2:
                                    hitmode = 2;
                                    break;

                                case 3:
                                    hitmode = 6;
                                    break;

                                default:
                                    hitmode = 5;
                                    break;
                            }

                            hitRequest.Session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                type: UserType.Player,
                                callerId: hitRequest.Session.Character.CharacterId, secondaryType: 1,
                                targetId: target.Character.CharacterId,
                                skillVNum: hitRequest.Skill.SkillVNum,
                                cooldown: (short)(hitRequest.Skill.Cooldown +
                                                   hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                attackAnimation: hitRequest.Skill.AttackAnimation,
                                skillEffect: hitRequest.SkillEffect, x: hitRequest.Session.Character.PositionX,
                                y: hitRequest.Session.Character.PositionY, isAlive: isAlive,
                                health: (int)(target.Character.Hp / (float)target.Character.HPLoad() * 100),
                                damage: damage, hitmode: hitmode,
                                skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                            break;

                        case TargetHitType.ZoneHit:
                            hitRequest.Session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                type: UserType.Player,
                                callerId: hitRequest.Session.Character.CharacterId, secondaryType: 1,
                                targetId: target.Character.CharacterId,
                                skillVNum: hitRequest.Skill.SkillVNum,
                                cooldown: (short)(hitRequest.Skill.Cooldown +
                                                   hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                attackAnimation: hitRequest.Skill.AttackAnimation,
                                skillEffect: hitRequest.SkillEffect, x: hitRequest.MapX, y: hitRequest.MapY,
                                isAlive: isAlive,
                                health: (int)(target.Character.Hp / (float)target.Character.HPLoad() * 100),
                                damage: damage, hitmode: hitmode,
                                skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                            break;

                        case TargetHitType.SpecialZoneHit:
                            hitRequest.Session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                type: UserType.Player,
                                callerId: hitRequest.Session.Character.CharacterId, secondaryType: 1,
                                targetId: target.Character.CharacterId,
                                skillVNum: hitRequest.Skill.SkillVNum,
                                cooldown: (short)(hitRequest.Skill.Cooldown +
                                                   hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                attackAnimation: hitRequest.Skill.AttackAnimation,
                                skillEffect: hitRequest.SkillEffect, x: hitRequest.Session.Character.PositionX,
                                y: hitRequest.Session.Character.PositionY, isAlive: isAlive,
                                health: (int)(target.Character.Hp / target.Character.HPLoad() * 100), damage: damage,
                                hitmode: hitmode,
                                skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                            break;

                        default:
                            Logger.Warn(data: "Not Implemented TargetHitType Handling!");
                            break;
                    }
                }
                else
                {
                    if (target != null)
                        hitRequest?.Session.SendPacket(
                            packet: StaticPacketHelper.Cancel(type: 2, callerId: target.Character.CharacterId));
                }
            }
            else
            {
                // monster already has been killed, send cancel
                if (target != null)
                    hitRequest?.Session.SendPacket(
                        packet: StaticPacketHelper.Cancel(type: 2, callerId: target.Character.CharacterId));
            }
        }

        void TargetHit(int castingId, UserType targetType, int targetId, bool isPvp = false)
        {
            // O gods of software development and operations, I have sinned. 

            var shouldCancel = true;
            var isSacrificeSkill = false;

            if ((DateTime.Now - Session.Character.LastTransform).TotalSeconds < 3)
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel());
                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "CANT_ATTACK"),
                    type: 0));
                return;
            }

            var skills = Session.Character.GetSkills();

            if (skills != null)
            {
                var ski = skills.FirstOrDefault(predicate: s =>
                    s.Skill?.CastId == castingId && (s.Skill?.UpgradeSkill == 0 || s.Skill?.SkillType == 1));

                if (castingId != 0)
                {
                    Session.SendPacket(packet: "ms_c 0");

                    foreach (var qslot in Session.Character.GenerateQuicklist()) Session.SendPacket(packet: qslot);
                }

                if (ski != null)
                {
                    if (!Session.Character.WeaponLoaded(ski: ski) || !ski.CanBeUsed())
                    {
                        Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                        return;
                    }

                    if (Session.Character.LastSkillComboUse > DateTime.Now
                        && ski.SkillVNum != SkillHelper.GetOriginalSkill(skill: ski.Skill)?.SkillVNum)
                    {
                        Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                        return;
                    }

                    BattleEntity targetEntity = null;

                    switch (targetType)
                    {
                        case UserType.Player:
                            {
                                targetEntity = ServerManager.Instance.GetSessionByCharacterId(characterId: targetId)
                                    ?.Character
                                    ?.BattleEntity;
                            }
                            break;

                        case UserType.Npc:
                            {
                                targetEntity = Session.Character.MapInstance?.Npcs?.ToList()
                                                   .FirstOrDefault(predicate: n => n.MapNpcId == targetId)?.BattleEntity
                                               ?? Session.Character.MapInstance?.Sessions
                                                   ?.Where(predicate: s => s?.Character?.Mates != null)
                                                   .SelectMany(selector: s => s.Character.Mates)
                                                   .FirstOrDefault(predicate: m => m.MateTransportId == targetId)
                                                   ?.BattleEntity;
                            }
                            break;

                        case UserType.Monster:
                            {
                                targetEntity = Session.Character.MapInstance?.Monsters?.ToList()
                                    .FirstOrDefault(
                                        predicate: m => m.Owner?.Character == null && m.MapMonsterId == targetId)
                                    ?.BattleEntity;
                            }
                            break;
                    }

                    if (targetEntity == null)
                    {
                        Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
                        return;
                    }

                    foreach (var bc in ski.GetSkillBCards().ToList().Where(predicate: s =>
                        s.Type.Equals(obj: (byte)CardType.MeditationSkill)
                        && (!s.SubType.Equals(obj: (byte)AdditionalTypes.MeditationSkill.CausingChance / 10) ||
                            SkillHelper.IsCausingChance(skillVNum: ski.SkillVNum))))
                    {
                        shouldCancel = false;

                        if (bc.SubType.Equals(obj: (byte)AdditionalTypes.MeditationSkill.Sacrifice / 10))
                        {
                            isSacrificeSkill = true;
                            if (targetEntity == Session.Character.BattleEntity || targetEntity.MapMonster != null ||
                                targetEntity.MapNpc != null)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "INVALID_TARGET"), type: 0));
                                Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                return;
                            }
                        }

                        bc.ApplyBCards(session: Session.Character.BattleEntity, sender: Session.Character.BattleEntity);
                    }

                    if (ski.Skill.SkillVNum == 1098 && ski.GetSkillBCards().FirstOrDefault(predicate: s =>
                            s.Type.Equals(obj: (byte)CardType.SpecialisationBuffResistance) &&
                            s.SubType.Equals(
                                obj: (byte)AdditionalTypes.SpecialisationBuffResistance.RemoveBadEffects /
                                     10))
                        is BCard RemoveBadEffectsBcard)
                        if (Session.Character.BattleEntity.BCardDisposables[key: RemoveBadEffectsBcard.BCardId] != null)
                        {
                            Session.SendPacket(
                                packet: StaticPacketHelper.SkillResetWithCoolDown(castId: castingId, cooldown: 300));
                            ski.LastUse = DateTime.Now.AddSeconds(value: 29);
                            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 30)).Subscribe(onNext: o =>
                            {
                                var
                                    skill = Session.Character.GetSkills().Find(match: s =>
                                        s.Skill?.CastId
                                        == castingId && (s.Skill?.UpgradeSkill == 0 || s.Skill?.SkillType == 1));
                                if (skill != null && skill.LastUse <= DateTime.Now)
                                    Session.SendPacket(packet: StaticPacketHelper.SkillReset(castId: castingId));
                            });
                            RemoveBadEffectsBcard.ApplyBCards(session: Session.Character.BattleEntity,
                                sender: Session.Character.BattleEntity);
                            Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                            return;
                        }

                    double cooldownReduction = Session.Character.GetBuff(type: CardType.Morale,
                        subtype: (byte)AdditionalTypes.Morale.SkillCooldownDecreased)[0];

                    var increaseEnemyCooldownChance = Session.Character.GetBuff(type: CardType.DarkCloneSummon,
                        subtype: (byte)AdditionalTypes.DarkCloneSummon.IncreaseEnemyCooldownChance);

                    if (ServerManager.RandomNumber() < increaseEnemyCooldownChance[0])
                        cooldownReduction -= increaseEnemyCooldownChance[1];

                    var mpCost = ski.MpCost();
                    short hpCost = 0;

                    mpCost = (short)(mpCost * ((100 - Session.Character.CellonOptions
                        .Where(predicate: s => s.Type == CellonOptionType.MpUsage)
                        .Sum(selector: s => s.Value)) / 100D));

                    if (Session.Character.GetBuff(type: CardType.HealingBurningAndCasting,
                            subtype: (byte)AdditionalTypes.HealingBurningAndCasting.HpDecreasedByConsumingMp)[0] is int
                        HPDecreasedByConsumingMP)
                        if (HPDecreasedByConsumingMP < 0)
                        {
                            var amountDecreased = -(ski.MpCost() * HPDecreasedByConsumingMP / 100);
                            hpCost = (short)amountDecreased;
                            mpCost -= (short)amountDecreased;
                        }

                    if (Session.Character.Mp >= mpCost && Session.Character.Hp > hpCost &&
                        Session.HasCurrentMapInstance)
                    {
                        if (!Session.Character.HasGodMode) Session.Character.DecreaseMp(amount: ski.MpCost());

                        ski.LastUse = DateTime.Now;
                        Session.Character.PyjamaDead = ski.SkillVNum == 801;

                        // Area on attacker
                        if (ski.Skill.TargetType == 1 && ski.Skill.HitType == 1)
                        {
                            if (Session.Character.MapInstance.MapInstanceType ==
                                MapInstanceType.TalentArenaMapInstance && !Session.Character.MapInstance.IsPvp)
                            {
                                Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                return;
                            }

                            if (Session.Character.UseSp && ski.Skill.CastEffect != -1)
                                Session.SendPackets(packets: Session.Character.GenerateQuicklist());

                            Session.SendPacket(packet: Session.Character.GenerateStat());
                            var skillinfo = Session.Character.Skills.FirstOrDefault(predicate: s =>
                                s.Skill.UpgradeSkill == ski.Skill.SkillVNum && s.Skill.Effect > 0
                                                                            && s.Skill.SkillType == 2);

                            Session.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.CastOnTarget(
                                attackerType: UserType.Player,
                                attackerId: Session.Character.CharacterId, defenderType: targetType,
                                defenderId: targetId,
                                castAnimation: ski.Skill.CastAnimation,
                                castEffect: skillinfo?.Skill.CastEffect ?? ski.Skill.CastEffect,
                                skillVNum: ski.Skill.SkillVNum));

                            var skillEffect = skillinfo?.Skill.Effect ?? ski.Skill.Effect;

                            if (Session.Character.BattleEntity.HasBuff(type: CardType.FireCannoneerRangeBuff,
                                    subtype: (byte)AdditionalTypes.FireCannoneerRangeBuff.AoeIncreased) &&
                                ski.Skill.Effect == 4569) skillEffect = 4572;

                            var targetRange = ski.TargetRange();

                            if (targetRange != 0)
                                ski.GetSkillBCards().Where(predicate: s =>
                                        s.Type.Equals(obj: (byte)CardType.Buff) &&
                                        new Buff(id: (short)s.SecondData, level: Session.Character.Level).Card
                                            ?.BuffType ==
                                        BuffType.Good
                                        || s.Type.Equals(obj: (byte)CardType.SpecialEffects2) &&
                                        s.SubType.Equals(
                                            obj: (byte)AdditionalTypes.SpecialEffects2.TeleportInRadius / 10))
                                    .ToList()
                                    .ForEach(action: s =>
                                        s.ApplyBCards(session: Session.Character.BattleEntity,
                                            sender: Session.Character.BattleEntity));

                            Session.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                type: UserType.Player,
                                callerId: Session.Character.CharacterId, secondaryType: 1,
                                targetId: Session.Character.CharacterId, skillVNum: ski.Skill.SkillVNum,
                                cooldown: (short)(ski.Skill.Cooldown + ski.Skill.Cooldown * cooldownReduction / 100D),
                                attackAnimation: ski.Skill.AttackAnimation,
                                skillEffect: skillEffect, x: Session.Character.PositionX,
                                y: Session.Character.PositionY, isAlive: true,
                                health: (int)(Session.Character.Hp / Session.Character.HPLoad() * 100), damage: 0,
                                hitmode: -2,
                                skillType: (byte)(ski.Skill.SkillType - 1)));

                            if (targetRange != 0)
                            {
                                foreach (var character in ServerManager.Instance.Sessions.Where(predicate: s =>
                                    s.CurrentMapInstance == Session.CurrentMapInstance
                                    && s.Character.CharacterId != Session.Character.CharacterId
                                    && s.Character.IsInRange(xCoordinate: Session.Character.PositionX,
                                        yCoordinate: Session.Character.PositionY,
                                        range: ski.TargetRange())))
                                    if (Session.Character.BattleEntity.CanAttackEntity(
                                        receiver: character.Character.BattleEntity)
                                    )
                                        PvpHit(
                                            hitRequest: new HitRequest(targetHitType: TargetHitType.AoeTargetHit,
                                                session: Session, skill: ski.Skill,
                                                skillBCards: ski.GetSkillBCards()),
                                            target: character);

                                foreach (var mon in Session.CurrentMapInstance
                                    .GetMonsterInRangeList(mapX: Session.Character.PositionX,
                                        mapY: Session.Character.PositionY,
                                        distance: ski.TargetRange()).Where(predicate: s =>
                                        Session.Character.BattleEntity.CanAttackEntity(receiver: s.BattleEntity)))
                                    lock (mon.OnHitLockObject)
                                    {
                                        mon.OnReceiveHit(hitRequest: new HitRequest(
                                            targetHitType: TargetHitType.AoeTargetHit, session: Session,
                                            skill: ski.Skill,
                                            skillEffect: skillinfo?.Skill.Effect ?? ski.Skill.Effect));
                                    }

                                foreach (var mate in Session.CurrentMapInstance
                                    .GetListMateInRange(mapX: Session.Character.PositionX,
                                        mapY: Session.Character.PositionY,
                                        distance: ski.TargetRange()).Where(predicate: s =>
                                        Session.Character.BattleEntity.CanAttackEntity(receiver: s.BattleEntity)))
                                    mate.HitRequest(hitRequest: new HitRequest(
                                        targetHitType: TargetHitType.AoeTargetHit, session: Session, skill: ski.Skill,
                                        skillEffect: skillinfo?.Skill.Effect ?? ski.Skill.Effect,
                                        skillBCards: ski.GetSkillBCards()));
                            }
                        }
                        else if (ski.Skill.TargetType == 2 && ski.Skill.HitType == 0 || isSacrificeSkill)
                        {
                            ConcurrentBag<ArenaTeamMember> team = null;
                            if (Session.Character.MapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance)
                                team = ServerManager.Instance.ArenaTeams.ToList()
                                    .FirstOrDefault(predicate: s => s.Any(predicate: o => o.Session == Session));

                            if (Session.Character.BattleEntity.CanAttackEntity(receiver: targetEntity)
                                || team != null && team.FirstOrDefault(predicate: s => s.Session == Session)
                                    ?.ArenaTeamType !=
                                team.FirstOrDefault(predicate: s => s.Session == targetEntity.Character.Session)
                                    ?.ArenaTeamType)
                                targetEntity = Session.Character.BattleEntity;
                            if (Session.Character.MapInstance == ServerManager.Instance.ArenaInstance &&
                                targetEntity.Mate?.Owner != Session.Character &&
                                targetEntity != Session.Character.BattleEntity &&
                                (Session.Character.Group == null ||
                                 !Session.Character.Group.IsMemberOfGroup(entityId: targetEntity.MapEntityId)))
                                targetEntity = Session.Character.BattleEntity;
                            if (Session.Character.MapInstance == ServerManager.Instance.FamilyArenaInstance &&
                                targetEntity.Mate?.Owner != Session.Character &&
                                targetEntity != Session.Character.BattleEntity &&
                                Session.Character.Family !=
                                (targetEntity.Character?.Family ?? targetEntity.Mate?.Owner.Family ??
                                    targetEntity.MapMonster?.Owner?.Character?.Family))
                                targetEntity = Session.Character.BattleEntity;

                            if (targetEntity.Character != null && targetEntity.Character.IsSitting)
                            {
                                targetEntity.Character.IsSitting = false;
                                Session.CurrentMapInstance?.Broadcast(packet: targetEntity.Character.GenerateRest());
                            }

                            if (targetEntity.Mate != null && targetEntity.Mate.IsSitting)
                                Session.CurrentMapInstance?.Broadcast(
                                    packet: targetEntity.Mate.GenerateRest(ownerSit: false));

                            ski.GetSkillBCards().ToList()
                                .Where(predicate: s => !s.Type.Equals(obj: (byte)CardType.MeditationSkill))
                                .ToList()
                                .ForEach(action: s =>
                                    s.ApplyBCards(session: targetEntity, sender: Session.Character.BattleEntity));

                            targetEntity.MapInstance.Broadcast(packet: StaticPacketHelper.CastOnTarget(
                                attackerType: UserType.Player,
                                attackerId: Session.Character.CharacterId, defenderType: targetEntity.UserType,
                                defenderId: targetEntity.MapEntityId,
                                castAnimation: ski.Skill.CastAnimation, castEffect: ski.Skill.CastEffect,
                                skillVNum: ski.Skill.SkillVNum));
                            targetEntity.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                type: UserType.Player,
                                callerId: Session.Character.CharacterId, secondaryType: (byte)targetEntity.UserType,
                                targetId: targetEntity.MapEntityId,
                                skillVNum: ski.Skill.SkillVNum,
                                cooldown: (short)(ski.Skill.Cooldown + ski.Skill.Cooldown * cooldownReduction / 100D),
                                attackAnimation: ski.Skill.AttackAnimation, skillEffect: ski.Skill.Effect,
                                x: targetEntity.PositionX,
                                y: targetEntity.PositionY, isAlive: true,
                                health: (int)(targetEntity.Hp / targetEntity.HPLoad() * 100), damage: 0, hitmode: -1,
                                skillType: (byte)(ski.Skill.SkillType - 1)));
                        }
                        else if (ski.Skill.TargetType == 1 && ski.Skill.HitType != 1)
                        {
                            Session.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.CastOnTarget(
                                attackerType: UserType.Player,
                                attackerId: Session.Character.CharacterId, defenderType: UserType.Player,
                                defenderId: Session.Character.CharacterId,
                                castAnimation: ski.Skill.CastAnimation, castEffect: ski.Skill.CastEffect,
                                skillVNum: ski.Skill.SkillVNum));

                            if (ski.Skill.CastEffect != 0) Thread.Sleep(millisecondsTimeout: ski.Skill.CastTime * 100);

                            Session.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                type: UserType.Player,
                                callerId: Session.Character.CharacterId, secondaryType: 1,
                                targetId: Session.Character.CharacterId, skillVNum: ski.Skill.SkillVNum,
                                cooldown: (short)(ski.Skill.Cooldown + ski.Skill.Cooldown * cooldownReduction / 100D),
                                attackAnimation: ski.Skill.AttackAnimation, skillEffect: ski.Skill.Effect,
                                x: Session.Character.PositionX, y: Session.Character.PositionY, isAlive: true,
                                health: (int)(Session.Character.Hp / Session.Character.HPLoad() * 100), damage: 0,
                                hitmode: -1,
                                skillType: (byte)(ski.Skill.SkillType - 1)));

                            if (ski.SkillVNum != 1330)
                                switch (ski.Skill.HitType)
                                {
                                    case 0:
                                    case 4:
                                        if (Session.Character.Buff.FirstOrDefault(predicate: s =>
                                                s.Card.BCards.Any(predicate: b =>
                                                    b.Type == (byte)CardType.FalconSkill &&
                                                    b.SubType.Equals(
                                                        obj: (byte)AdditionalTypes.FalconSkill.Hide / 10))) is
                                            Buff
                                            FalconHideBuff)
                                        {
                                            Session.Character.RemoveBuff(cardId: FalconHideBuff.Card.CardId);
                                            Session.Character.AddBuff(
                                                indicator: new Buff(id: 560, level: Session.Character.Level),
                                                sender: Session.Character.BattleEntity);
                                        }

                                        break;

                                    case 2:
                                        ConcurrentBag<ArenaTeamMember> team = null;
                                        if (Session.Character.MapInstance.MapInstanceType ==
                                            MapInstanceType.TalentArenaMapInstance)
                                            team = ServerManager.Instance.ArenaTeams.ToList()
                                                .FirstOrDefault(predicate: s =>
                                                    s.Any(predicate: o => o.Session == Session));

                                        var clientSessions =
                                            Session.CurrentMapInstance.Sessions?.Where(predicate: s =>
                                                s.Character.CharacterId != Session.Character.CharacterId &&
                                                s.Character.IsInRange(xCoordinate: Session.Character.PositionX,
                                                    yCoordinate: Session.Character.PositionY,
                                                    range: ski.TargetRange()));
                                        if (clientSessions != null)
                                            foreach (var target in clientSessions)
                                                if (!Session.Character.BattleEntity.CanAttackEntity(receiver: target
                                                        .Character
                                                        .BattleEntity)
                                                    && (team == null ||
                                                        team.FirstOrDefault(predicate: s => s.Session == Session)
                                                            ?.ArenaTeamType ==
                                                        team.FirstOrDefault(predicate: s =>
                                                                s.Session == target.Character.Session)
                                                            ?.ArenaTeamType))
                                                {
                                                    if (Session.Character.MapInstance ==
                                                        ServerManager.Instance.ArenaInstance &&
                                                        (Session.Character.Group == null ||
                                                         !Session.Character.Group.IsMemberOfGroup(entityId: target
                                                             .Character
                                                             .CharacterId))) continue;
                                                    if (Session.Character.MapInstance ==
                                                        ServerManager.Instance.FamilyArenaInstance &&
                                                        Session.Character.Family != target.Character.Family) continue;

                                                    ski.GetSkillBCards().ToList().Where(predicate: s =>
                                                            !s.Type.Equals(obj: (byte)CardType.MeditationSkill))
                                                        .ToList().ForEach(action: s =>
                                                            s.ApplyBCards(session: target.Character.BattleEntity,
                                                                sender: Session.Character.BattleEntity));

                                                    Session.CurrentMapInstance.Broadcast(
                                                        packet: StaticPacketHelper.SkillUsed(
                                                            type: UserType.Player,
                                                            callerId: Session.Character.CharacterId, secondaryType: 1,
                                                            targetId: target.Character.CharacterId,
                                                            skillVNum: ski.Skill.SkillVNum,
                                                            cooldown: (short)(ski.Skill.Cooldown +
                                                                               ski.Skill.Cooldown * cooldownReduction /
                                                                               100D),
                                                            attackAnimation: ski.Skill.AttackAnimation,
                                                            skillEffect: ski.Skill.Effect,
                                                            x: target.Character.PositionX,
                                                            y: target.Character.PositionY, isAlive: true,
                                                            health: (int)(target.Character.Hp /
                                                                target.Character.HPLoad() * 100),
                                                            damage: 0, hitmode: -1,
                                                            skillType: (byte)(ski.Skill.SkillType - 1)));
                                                }

                                        IEnumerable<Mate> mates =
                                            Session.CurrentMapInstance.GetListMateInRange(
                                                mapX: Session.Character.PositionX,
                                                mapY: Session.Character.PositionY, distance: ski.TargetRange());
                                        if (mates != null)
                                            foreach (var target in mates)
                                                if (!Session.Character.BattleEntity.CanAttackEntity(
                                                    receiver: target.BattleEntity)
                                                )
                                                {
                                                    if (Session.Character.MapInstance ==
                                                        ServerManager.Instance.ArenaInstance &&
                                                        (Session.Character.Group == null ||
                                                         !Session.Character.Group.IsMemberOfGroup(entityId: target.Owner
                                                             .CharacterId))) continue;
                                                    if (Session.Character.MapInstance ==
                                                        ServerManager.Instance.FamilyArenaInstance &&
                                                        Session.Character.Family != target.Owner.Family) continue;

                                                    ski.GetSkillBCards().ToList().Where(predicate: s =>
                                                            !s.Type.Equals(obj: (byte)CardType.MeditationSkill))
                                                        .ToList().ForEach(action: s =>
                                                            s.ApplyBCards(session: target.BattleEntity,
                                                                sender: Session.Character.BattleEntity));

                                                    Session.CurrentMapInstance.Broadcast(
                                                        packet: StaticPacketHelper.SkillUsed(
                                                            type: UserType.Player,
                                                            callerId: Session.Character.CharacterId,
                                                            secondaryType: (byte)target.BattleEntity.UserType,
                                                            targetId: target.MateTransportId,
                                                            skillVNum: ski.Skill.SkillVNum,
                                                            cooldown: (short)(ski.Skill.Cooldown +
                                                                               ski.Skill.Cooldown * cooldownReduction /
                                                                               100D),
                                                            attackAnimation: ski.Skill.AttackAnimation,
                                                            skillEffect: ski.Skill.Effect,
                                                            x: target.PositionX, y: target.PositionY, isAlive: true,
                                                            health: (int)(target.Hp / target.HpLoad() * 100),
                                                            damage: 0, hitmode: -1,
                                                            skillType: (byte)(ski.Skill.SkillType - 1)));
                                                }

                                        IEnumerable<MapMonster> monsters =
                                            Session.CurrentMapInstance.GetMonsterInRangeList(
                                                mapX: Session.Character.PositionX, mapY: Session.Character.PositionY,
                                                distance: ski.TargetRange());
                                        if (monsters != null)
                                            foreach (var target in monsters)
                                                if (!Session.Character.BattleEntity.CanAttackEntity(
                                                    receiver: target.BattleEntity)
                                                )
                                                {
                                                    if (target.Owner != null)
                                                    {
                                                        if (target.Owner.Character != null) continue;
                                                        if (Session.Character.MapInstance ==
                                                            ServerManager.Instance.ArenaInstance &&
                                                            (Session.Character.Group == null ||
                                                             !Session.Character.Group.IsMemberOfGroup(entityId: target
                                                                 .Owner
                                                                 .MapEntityId))) continue;
                                                        if (Session.Character.MapInstance ==
                                                            ServerManager.Instance.FamilyArenaInstance &&
                                                            Session.Character.Family != target.Owner.Character?.Family)
                                                            continue;
                                                    }

                                                    ski.GetSkillBCards().ToList().Where(predicate: s =>
                                                            !s.Type.Equals(obj: (byte)CardType.MeditationSkill))
                                                        .ToList().ForEach(action: s =>
                                                            s.ApplyBCards(session: target.BattleEntity,
                                                                sender: Session.Character.BattleEntity));

                                                    Session.CurrentMapInstance.Broadcast(
                                                        packet: StaticPacketHelper.SkillUsed(
                                                            type: UserType.Player,
                                                            callerId: Session.Character.CharacterId,
                                                            secondaryType: (byte)target.BattleEntity.UserType,
                                                            targetId: target.MapMonsterId,
                                                            skillVNum: ski.Skill.SkillVNum,
                                                            cooldown: (short)(ski.Skill.Cooldown +
                                                                               ski.Skill.Cooldown * cooldownReduction /
                                                                               100D),
                                                            attackAnimation: ski.Skill.AttackAnimation,
                                                            skillEffect: ski.Skill.Effect,
                                                            x: target.MapX, y: target.MapY, isAlive: true,
                                                            health: (int)(target.CurrentHp / target.MaxHp * 100),
                                                            damage: 0, hitmode: -1,
                                                            skillType: (byte)(ski.Skill.SkillType - 1)));
                                                }

                                        IEnumerable<MapNpc> npcs =
                                            Session.CurrentMapInstance.GetListNpcInRange(
                                                mapX: Session.Character.PositionX,
                                                mapY: Session.Character.PositionY, distance: ski.TargetRange());
                                        if (npcs != null)
                                            foreach (var target in npcs)
                                                if (!Session.Character.BattleEntity.CanAttackEntity(
                                                    receiver: target.BattleEntity)
                                                )
                                                {
                                                    if (target.Owner != null)
                                                    {
                                                        if (Session.Character.MapInstance ==
                                                            ServerManager.Instance.ArenaInstance &&
                                                            (Session.Character.Group == null ||
                                                             !Session.Character.Group.IsMemberOfGroup(entityId: target
                                                                 .Owner
                                                                 .MapEntityId))) continue;
                                                        if (Session.Character.MapInstance ==
                                                            ServerManager.Instance.FamilyArenaInstance &&
                                                            Session.Character.Family != target.Owner.Character?.Family)
                                                            continue;
                                                    }

                                                    ski.GetSkillBCards().ToList().Where(predicate: s =>
                                                            !s.Type.Equals(obj: (byte)CardType.MeditationSkill))
                                                        .ToList().ForEach(action: s =>
                                                            s.ApplyBCards(session: target.BattleEntity,
                                                                sender: Session.Character.BattleEntity));

                                                    Session.CurrentMapInstance.Broadcast(
                                                        packet: StaticPacketHelper.SkillUsed(
                                                            type: UserType.Player,
                                                            callerId: Session.Character.CharacterId,
                                                            secondaryType: (byte)target.BattleEntity.UserType,
                                                            targetId: target.MapNpcId,
                                                            skillVNum: ski.Skill.SkillVNum,
                                                            cooldown: (short)(ski.Skill.Cooldown +
                                                                               ski.Skill.Cooldown * cooldownReduction /
                                                                               100D),
                                                            attackAnimation: ski.Skill.AttackAnimation,
                                                            skillEffect: ski.Skill.Effect,
                                                            x: target.MapX, y: target.MapY, isAlive: true,
                                                            health: (int)(target.CurrentHp / target.MaxHp * 100),
                                                            damage: 0, hitmode: -1,
                                                            skillType: (byte)(ski.Skill.SkillType - 1)));
                                                }

                                        break;
                                }

                            ski.GetSkillBCards().ToList()
                                .Where(predicate: s => !s.Type.Equals(obj: (byte)CardType.MeditationSkill))
                                .ToList().ForEach(action: s =>
                                    s.ApplyBCards(session: Session.Character.BattleEntity,
                                        sender: Session.Character.BattleEntity));
                        }
                        else if (ski.Skill.TargetType == 0)
                        {
                            if (Session.Character.MapInstance.MapInstanceType ==
                                MapInstanceType.TalentArenaMapInstance && !Session.Character.MapInstance.IsPvp)
                            {
                                Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                return;
                            }

                            if (isPvp)
                            {
                                //ClientSession playerToAttack = ServerManager.Instance.GetSessionByCharacterId(targetId);
                                var playerToAttack = targetEntity.Character?.Session;

                                if (playerToAttack != null && !IceBreaker.FrozenPlayers.Contains(item: playerToAttack))
                                {
                                    if (Map.GetDistance(
                                        p: new MapCell
                                        {
                                            X = Session.Character.PositionX,
                                            Y = Session.Character.PositionY
                                        },
                                        q: new MapCell
                                        {
                                            X = playerToAttack.Character.PositionX,
                                            Y = playerToAttack.Character.PositionY
                                        }) <= ski.Skill.Range + 5)
                                    {
                                        if (Session.Character.UseSp && ski.Skill.CastEffect != -1)
                                            Session.SendPackets(packets: Session.Character.GenerateQuicklist());

                                        if (ski.SkillVNum == 1061)
                                        {
                                            Session.CurrentMapInstance.Broadcast(packet: $"eff 1 {targetId} 4968");
                                            Session.CurrentMapInstance.Broadcast(
                                                packet: $"eff 1 {Session.Character.CharacterId} 4968");
                                        }

                                        Session.SendPacket(packet: Session.Character.GenerateStat());
                                        var characterSkillInfo = Session.Character.Skills.FirstOrDefault(predicate: s =>
                                            s.Skill.UpgradeSkill == ski.Skill.SkillVNum && s.Skill.Effect > 0
                                                                                        && s.Skill.SkillType == 2);
                                        Session.CurrentMapInstance.Broadcast(
                                            packet: StaticPacketHelper.CastOnTarget(attackerType: UserType.Player,
                                                attackerId: Session.Character.CharacterId,
                                                defenderType: UserType.Player, defenderId: targetId,
                                                castAnimation: ski.Skill.CastAnimation,
                                                castEffect: characterSkillInfo?.Skill.CastEffect ??
                                                            ski.Skill.CastEffect,
                                                skillVNum: ski.Skill.SkillVNum));
                                        Session.Character.Skills.Where(predicate: s => s.Id != ski.Id)
                                            .ForEach(action: i => i.Hit = 0);

                                        // Generate scp
                                        if ((DateTime.Now - ski.LastUse).TotalSeconds > 3)
                                            ski.Hit = 0;
                                        else
                                            ski.Hit++;

                                        ski.LastUse = DateTime.Now;

                                        if (ski.Skill.CastEffect != 0)
                                            Thread.Sleep(millisecondsTimeout: ski.Skill.CastTime * 100);

                                        if (ski.Skill.HitType == 3)
                                        {
                                            var count = 0;
                                            if (playerToAttack.CurrentMapInstance == Session.CurrentMapInstance
                                                && playerToAttack.Character.CharacterId !=
                                                Session.Character.CharacterId)
                                            {
                                                if (Session.Character.BattleEntity.CanAttackEntity(
                                                    receiver: playerToAttack
                                                        .Character.BattleEntity))
                                                {
                                                    count++;
                                                    PvpHit(
                                                        hitRequest: new HitRequest(
                                                            targetHitType: TargetHitType.SingleAoeTargetHit,
                                                            session: Session,
                                                            skill: ski.Skill, skillBCards: ski.GetSkillBCards(),
                                                            showTargetAnimation: true), target: playerToAttack);
                                                }
                                                else
                                                {
                                                    Session.SendPacket(
                                                        packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                                }
                                            }

                                            //foreach (long id in Session.Character.MTListTargetQueue.Where(s => s.EntityType == UserType.Player).Select(s => s.TargetId))
                                            foreach (var id in Session.Character.GetMTListTargetQueue_QuickFix(ski: ski,
                                                entityType: UserType.Player))
                                            {
                                                var character =
                                                    ServerManager.Instance.GetSessionByCharacterId(characterId: id);

                                                if (character != null
                                                    && character.CurrentMapInstance == Session.CurrentMapInstance
                                                    && character.Character.CharacterId != Session.Character.CharacterId
                                                    && character != playerToAttack)
                                                    if (Session.Character.BattleEntity.CanAttackEntity(
                                                        receiver: character.Character.BattleEntity))
                                                    {
                                                        count++;
                                                        PvpHit(
                                                            hitRequest: new HitRequest(
                                                                targetHitType: TargetHitType.SingleAoeTargetHit,
                                                                session: Session,
                                                                skill: ski.Skill, showTargetAnimation: count == 1,
                                                                skillBCards: ski.GetSkillBCards()), target: character);
                                                    }
                                            }

                                            if (count == 0)
                                                Session.SendPacket(
                                                    packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                        }
                                        else
                                        {
                                            // check if we will hit mutltiple targets
                                            if (ski.TargetRange() != 0)
                                            {
                                                var skillCombo = ski.Skill.Combos.Find(match: s => ski.Hit == s.Hit);
                                                if (skillCombo != null)
                                                {
                                                    if (ski.Skill.Combos.OrderByDescending(keySelector: s => s.Hit)
                                                            .First().Hit
                                                        == ski.Hit)
                                                        ski.Hit = 0;

                                                    var playersInAoeRange =
                                                        ServerManager.Instance.Sessions.Where(predicate: s =>
                                                            s.CurrentMapInstance == Session.CurrentMapInstance
                                                            && s.Character.CharacterId != Session.Character.CharacterId
                                                            && s != playerToAttack
                                                            && s.Character.IsInRange(
                                                                xCoordinate: playerToAttack.Character.PositionX,
                                                                yCoordinate: playerToAttack.Character.PositionY,
                                                                range: ski.TargetRange()));
                                                    var count = 0;
                                                    if (Session.Character.BattleEntity.CanAttackEntity(
                                                        receiver: playerToAttack
                                                            .Character.BattleEntity))
                                                    {
                                                        count++;
                                                        PvpHit(
                                                            hitRequest: new HitRequest(
                                                                targetHitType: TargetHitType.SingleTargetHitCombo,
                                                                session: Session, skill: ski.Skill,
                                                                skillCombo: skillCombo,
                                                                skillBCards: ski.GetSkillBCards()),
                                                            target: playerToAttack);
                                                    }
                                                    else
                                                    {
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));
                                                    }

                                                    foreach (var character in playersInAoeRange)
                                                        if (Session.Character.BattleEntity.CanAttackEntity(
                                                            receiver: character.Character.BattleEntity))
                                                        {
                                                            count++;
                                                            PvpHit(
                                                                hitRequest: new HitRequest(
                                                                    targetHitType: TargetHitType.SingleTargetHitCombo,
                                                                    session: Session, skill: ski.Skill,
                                                                    skillCombo: skillCombo,
                                                                    showTargetAnimation: count == 1,
                                                                    skillBCards: ski.GetSkillBCards()),
                                                                target: character);
                                                        }

                                                    if (playerToAttack.Character.Hp <= 0 || count == 0)
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));
                                                }
                                                else
                                                {
                                                    var playersInAoeRange =
                                                        ServerManager.Instance.Sessions.Where(predicate: s =>
                                                            s.CurrentMapInstance == Session.CurrentMapInstance
                                                            && s.Character.CharacterId != Session.Character.CharacterId
                                                            && s != playerToAttack
                                                            && s.Character.IsInRange(
                                                                xCoordinate: playerToAttack.Character.PositionX,
                                                                yCoordinate: playerToAttack.Character.PositionY,
                                                                range: ski.TargetRange()));

                                                    var count = 0;
                                                    // hit the targetted player
                                                    if (Session.Character.BattleEntity.CanAttackEntity(
                                                        receiver: playerToAttack
                                                            .Character.BattleEntity))
                                                    {
                                                        count++;
                                                        PvpHit(
                                                            hitRequest: new HitRequest(
                                                                targetHitType: TargetHitType.SingleAoeTargetHit,
                                                                session: Session, skill: ski.Skill,
                                                                showTargetAnimation: true,
                                                                skillBCards: ski.GetSkillBCards()),
                                                            target: playerToAttack);
                                                    }
                                                    else
                                                    {
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));
                                                    }

                                                    //hit all other players
                                                    foreach (var character in playersInAoeRange)
                                                    {
                                                        count++;
                                                        if (Session.Character.BattleEntity.CanAttackEntity(
                                                            receiver: character.Character.BattleEntity))
                                                            PvpHit(
                                                                hitRequest: new HitRequest(
                                                                    targetHitType: TargetHitType.SingleAoeTargetHit,
                                                                    session: Session, skill: ski.Skill,
                                                                    showTargetAnimation: count == 1,
                                                                    skillBCards: ski.GetSkillBCards()),
                                                                target: character);
                                                    }

                                                    if (playerToAttack.Character.Hp <= 0)
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));
                                                }
                                            }
                                            else
                                            {
                                                var skillCombo = ski.Skill.Combos.Find(match: s => ski.Hit == s.Hit);
                                                if (skillCombo != null)
                                                {
                                                    if (ski.Skill.Combos.OrderByDescending(keySelector: s => s.Hit)
                                                            .First().Hit
                                                        == ski.Hit)
                                                        ski.Hit = 0;

                                                    if (Session.Character.BattleEntity.CanAttackEntity(
                                                        receiver: playerToAttack
                                                            .Character.BattleEntity))
                                                        PvpHit(
                                                            hitRequest: new HitRequest(
                                                                targetHitType: TargetHitType.SingleTargetHitCombo,
                                                                session: Session, skill: ski.Skill,
                                                                skillCombo: skillCombo,
                                                                skillBCards: ski.GetSkillBCards()),
                                                            target: playerToAttack);
                                                    else
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));
                                                }
                                                else
                                                {
                                                    if (Session.Character.BattleEntity.CanAttackEntity(
                                                        receiver: playerToAttack
                                                            .Character.BattleEntity))
                                                        PvpHit(
                                                            hitRequest: new HitRequest(
                                                                targetHitType: TargetHitType.SingleTargetHit,
                                                                session: Session, skill: ski.Skill,
                                                                showTargetAnimation: true,
                                                                skillBCards: ski.GetSkillBCards()),
                                                            target: playerToAttack);
                                                    else
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Session.SendPacket(
                                            packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                    }
                                }
                                else if (IceBreaker.FrozenPlayers.Contains(item: playerToAttack))
                                {
                                    Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                    if (playerToAttack.Character.LastPvPKiller == null
                                        || playerToAttack.Character.LastPvPKiller != Session)
                                        Session.SendPacket(
                                            packet: $"delay 2000 5 #guri^502^1^{playerToAttack.Character.CharacterId}");
                                }
                                else
                                {
                                    Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                }
                            }
                            else
                            {
                                var monsterToAttack = targetEntity.MapMonster;

                                if (monsterToAttack != null)
                                {
                                    if (Map.GetDistance(
                                            p: new MapCell
                                            {
                                                X = Session.Character.PositionX,
                                                Y = Session.Character.PositionY
                                            },
                                            q: new MapCell { X = monsterToAttack.MapX, Y = monsterToAttack.MapY }) <=
                                        ski.Skill.Range + 5 + monsterToAttack.Monster.BasicArea)
                                    {
                                        if (Session.Character.UseSp && ski.Skill.CastEffect != -1)
                                            Session.SendPackets(packets: Session.Character.GenerateQuicklist());

                                        #region Taunt

                                        if (ski.SkillVNum == 1061)
                                        {
                                            Session.CurrentMapInstance.Broadcast(packet: $"eff 3 {targetId} 4968");
                                            Session.CurrentMapInstance.Broadcast(
                                                packet: $"eff 1 {Session.Character.CharacterId} 4968");
                                        }

                                        #endregion

                                        ski.GetSkillBCards().ToList().Where(predicate: s => s.CastType == 1).ToList()
                                            .ForEach(action: s => s.ApplyBCards(session: monsterToAttack.BattleEntity,
                                                sender: Session.Character.BattleEntity));

                                        Session.SendPacket(packet: Session.Character.GenerateStat());

                                        var ski2 = Session.Character.Skills.FirstOrDefault(predicate: s =>
                                            s.Skill.UpgradeSkill == ski.Skill.SkillVNum
                                            && s.Skill.Effect > 0 && s.Skill.SkillType == 2);

                                        Session.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.CastOnTarget(
                                            attackerType: UserType.Player, attackerId: Session.Character.CharacterId,
                                            defenderType: UserType.Monster,
                                            defenderId: monsterToAttack.MapMonsterId,
                                            castAnimation: ski.Skill.CastAnimation,
                                            castEffect: ski2?.Skill.CastEffect ?? ski.Skill.CastEffect,
                                            skillVNum: ski.Skill.SkillVNum));

                                        Session.Character.Skills.Where(predicate: x => x.Id != ski.Id)
                                            .ForEach(action: x => x.Hit = 0);

                                        #region Generate scp

                                        if ((DateTime.Now - ski.LastUse).TotalSeconds > 3)
                                            ski.Hit = 0;
                                        else
                                            ski.Hit++;

                                        #endregion

                                        ski.LastUse = DateTime.Now;

                                        if (ski.Skill.CastEffect != 0)
                                            Thread.Sleep(millisecondsTimeout: ski.Skill.CastTime * 100);

                                        if (ski.Skill.HitType == 3)
                                        {
                                            monsterToAttack.HitQueue.Enqueue(item: new HitRequest(
                                                targetHitType: TargetHitType.SingleAoeTargetHit, session: Session,
                                                skill: ski.Skill, skillEffect: ski2?.Skill.Effect ?? ski.Skill.Effect,
                                                showTargetAnimation: true, skillBCards: ski.GetSkillBCards()));

                                            //foreach (long id in Session.Character.MTListTargetQueue.Where(s => s.EntityType == UserType.Monster).Select(s => s.TargetId))
                                            foreach (var id in Session.Character.GetMTListTargetQueue_QuickFix(ski: ski,
                                                entityType: UserType.Monster))
                                            {
                                                var mon = Session.CurrentMapInstance.GetMonsterById(mapMonsterId: id);

                                                if (mon?.CurrentHp > 0)
                                                    mon.HitQueue.Enqueue(item: new HitRequest(
                                                        targetHitType: TargetHitType.SingleAoeTargetHit,
                                                        session: Session,
                                                        skill: ski.Skill,
                                                        skillEffect: ski2?.Skill.Effect ?? ski.Skill.Effect,
                                                        skillBCards: ski.GetSkillBCards()));
                                            }
                                        }
                                        else
                                        {
                                            if (ski.TargetRange() != 0 || ski.Skill.HitType == 1)
                                            {
                                                var skillCombo = ski.Skill.Combos.Find(match: s => ski.Hit == s.Hit);

                                                var monstersInAoeRange = Session.CurrentMapInstance
                                                    ?.GetMonsterInRangeList(mapX: monsterToAttack.MapX,
                                                        mapY: monsterToAttack.MapY,
                                                        distance: ski.TargetRange())?
                                                    .Where(predicate: m =>
                                                        Session.Character.BattleEntity.CanAttackEntity(
                                                            receiver: m.BattleEntity))
                                                    .ToList();

                                                if (skillCombo != null)
                                                {
                                                    if (ski.Skill.Combos.OrderByDescending(keySelector: s => s.Hit)
                                                            .First().Hit ==
                                                        ski.Hit) ski.Hit = 0;

                                                    if (monsterToAttack.IsAlive && monstersInAoeRange?.Count != 0)
                                                        foreach (var mon in monstersInAoeRange)
                                                            mon.HitQueue.Enqueue(item: new HitRequest(
                                                                targetHitType: TargetHitType.SingleTargetHitCombo,
                                                                session: Session,
                                                                skill: ski.Skill, skillCombo: skillCombo,
                                                                skillBCards: ski.GetSkillBCards()));
                                                    else
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));
                                                }
                                                else
                                                {
                                                    monsterToAttack.HitQueue.Enqueue(item: new HitRequest(
                                                        targetHitType: TargetHitType.SingleAoeTargetHit,
                                                        session: Session,
                                                        skill: ski.Skill,
                                                        skillEffect: ski2?.Skill.Effect ?? ski.Skill.Effect,
                                                        showTargetAnimation: true, skillBCards: ski.GetSkillBCards()));

                                                    if (monsterToAttack.IsAlive && monstersInAoeRange?.Count != 0)
                                                        foreach (var mon in monstersInAoeRange.Where(predicate: m =>
                                                            m.MapMonsterId != monsterToAttack.MapMonsterId))
                                                            mon.HitQueue.Enqueue(
                                                                item: new HitRequest(
                                                                    targetHitType: TargetHitType.SingleAoeTargetHit,
                                                                    session: Session, skill: ski.Skill,
                                                                    skillEffect: ski2?.Skill.Effect ?? ski.Skill.Effect,
                                                                    skillBCards: ski.GetSkillBCards()));
                                                    else
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));
                                                }
                                            }
                                            else
                                            {
                                                var skillCombo = ski.Skill.Combos.Find(match: s => ski.Hit == s.Hit);

                                                if (skillCombo != null)
                                                {
                                                    if (ski.Skill.Combos.OrderByDescending(keySelector: s => s.Hit)
                                                            .First().Hit ==
                                                        ski.Hit) ski.Hit = 0;

                                                    monsterToAttack.HitQueue.Enqueue(item: new HitRequest(
                                                        targetHitType: TargetHitType.SingleTargetHitCombo,
                                                        session: Session,
                                                        skill: ski.Skill, skillCombo: skillCombo,
                                                        skillBCards: ski.GetSkillBCards()));
                                                }
                                                else
                                                {
                                                    monsterToAttack.HitQueue.Enqueue(item: new HitRequest(
                                                        targetHitType: TargetHitType.SingleTargetHit, session: Session,
                                                        skill: ski.Skill, skillBCards: ski.GetSkillBCards()));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Session.SendPacket(
                                            packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                    }
                                }
                                else if (targetEntity.Mate is Mate mateToAttack)
                                {
                                    if (!Session.Character.BattleEntity.CanAttackEntity(
                                        receiver: mateToAttack.BattleEntity))
                                    {
                                        Session.Character.Session.SendPacket(
                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                callerId: mateToAttack.BattleEntity.MapEntityId));
                                        return;
                                    }

                                    if (Map.GetDistance(
                                            p: new MapCell
                                            {
                                                X = Session.Character.PositionX,
                                                Y = Session.Character.PositionY
                                            },
                                            q: new MapCell { X = mateToAttack.PositionX, Y = mateToAttack.PositionY })
                                        <= ski.Skill.Range + 5 + mateToAttack.Monster.BasicArea)
                                    {
                                        if (Session.Character.UseSp && ski.Skill.CastEffect != -1)
                                            Session.SendPackets(packets: Session.Character.GenerateQuicklist());

                                        if (ski.SkillVNum == 1061)
                                        {
                                            Session.CurrentMapInstance.Broadcast(packet: $"eff 2 {targetId} 4968");
                                            Session.CurrentMapInstance.Broadcast(
                                                packet: $"eff 1 {Session.Character.CharacterId} 4968");
                                        }

                                        ski.GetSkillBCards().ToList().Where(predicate: s => s.CastType == 1).ToList()
                                            .ForEach(action: s =>
                                                s.ApplyBCards(session: mateToAttack.BattleEntity,
                                                    sender: Session.Character.BattleEntity));

                                        Session.SendPacket(packet: Session.Character.GenerateStat());
                                        var characterSkillInfo = Session.Character.Skills.FirstOrDefault(predicate: s =>
                                            s.Skill.UpgradeSkill == ski.Skill.SkillVNum && s.Skill.Effect > 0
                                                                                        && s.Skill.SkillType == 2);

                                        Session.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.CastOnTarget(
                                            attackerType: UserType.Player, attackerId: Session.Character.CharacterId,
                                            defenderType: UserType.Npc,
                                            defenderId: mateToAttack.MateTransportId,
                                            castAnimation: ski.Skill.CastAnimation,
                                            castEffect: characterSkillInfo?.Skill.CastEffect ?? ski.Skill.CastEffect,
                                            skillVNum: ski.Skill.SkillVNum));
                                        Session.Character.Skills.Where(predicate: s => s.Id != ski.Id)
                                            .ForEach(action: i => i.Hit = 0);

                                        // Generate scp
                                        if ((DateTime.Now - ski.LastUse).TotalSeconds > 3)
                                            ski.Hit = 0;
                                        else
                                            ski.Hit++;

                                        ski.LastUse = DateTime.Now;
                                        if (ski.Skill.CastEffect != 0)
                                            Thread.Sleep(millisecondsTimeout: ski.Skill.CastTime * 100);

                                        if (ski.Skill.HitType == 3)
                                        {
                                            mateToAttack.HitRequest(hitRequest: new HitRequest(
                                                targetHitType: TargetHitType.SingleAoeTargetHit, session: Session,
                                                skill: ski.Skill,
                                                skillEffect: characterSkillInfo?.Skill.Effect ?? ski.Skill.Effect,
                                                showTargetAnimation: true, skillBCards: ski.GetSkillBCards()));

                                            //foreach (long id in Session.Character.MTListTargetQueue.Where(s => s.EntityType == UserType.Monster).Select(s => s.TargetId))
                                            foreach (var id in Session.Character.GetMTListTargetQueue_QuickFix(ski: ski,
                                                entityType: UserType.Monster))
                                            {
                                                var mate = Session.CurrentMapInstance.GetMate(mateTransportId: id);
                                                if (mate != null && mate.Hp > 0 &&
                                                    Session.Character.BattleEntity.CanAttackEntity(
                                                        receiver: mate.BattleEntity))
                                                    mate.HitRequest(hitRequest: new HitRequest(
                                                        targetHitType: TargetHitType.SingleAoeTargetHit,
                                                        session: Session, skill: ski.Skill,
                                                        skillEffect: characterSkillInfo?.Skill.Effect ??
                                                                     ski.Skill.Effect,
                                                        skillBCards: ski.GetSkillBCards()));
                                            }
                                        }
                                        else
                                        {
                                            if (ski.TargetRange() != 0 || ski.Skill.HitType == 1
                                            ) // check if we will hit mutltiple targets
                                            {
                                                var skillCombo = ski.Skill.Combos.Find(match: s => ski.Hit == s.Hit);
                                                if (skillCombo != null)
                                                {
                                                    if (ski.Skill.Combos.OrderByDescending(keySelector: s => s.Hit)
                                                            .First().Hit
                                                        == ski.Hit)
                                                        ski.Hit = 0;

                                                    var monstersInAoeRange = Session.CurrentMapInstance?
                                                        .GetListMateInRange(mapX: mateToAttack.MapX,
                                                            mapY: mateToAttack.MapY, distance: ski.TargetRange()).Where(
                                                            predicate: m =>
                                                                Session.Character.BattleEntity.CanAttackEntity(
                                                                    receiver: m.BattleEntity)).ToList();
                                                    if (monstersInAoeRange.Count != 0)
                                                        foreach (var mate in monstersInAoeRange)
                                                            mate.HitRequest(
                                                                hitRequest: new HitRequest(
                                                                    targetHitType: TargetHitType.SingleTargetHitCombo,
                                                                    session: Session, skill: ski.Skill,
                                                                    skillCombo: skillCombo,
                                                                    skillBCards: ski.GetSkillBCards()));
                                                    else
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));

                                                    if (!mateToAttack.IsAlive)
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));
                                                }
                                                else
                                                {
                                                    var matesInAoeRange = Session.CurrentMapInstance?
                                                        .GetListMateInRange(
                                                            mapX: mateToAttack.MapX,
                                                            mapY: mateToAttack.MapY,
                                                            distance: ski.TargetRange())
                                                        ?.Where(predicate: m =>
                                                            Session.Character.BattleEntity.CanAttackEntity(
                                                                receiver: m.BattleEntity)).ToList();

                                                    //hit the targetted mate
                                                    mateToAttack.HitRequest(
                                                        hitRequest: new HitRequest(
                                                            targetHitType: TargetHitType.SingleAoeTargetHit,
                                                            session: Session,
                                                            skill: ski.Skill,
                                                            skillEffect: characterSkillInfo?.Skill.Effect ??
                                                                         ski.Skill.Effect,
                                                            showTargetAnimation: true,
                                                            skillBCards: ski.GetSkillBCards()));

                                                    //hit all other mates
                                                    if (matesInAoeRange != null && matesInAoeRange.Count != 0)
                                                        foreach (var mate in matesInAoeRange.Where(predicate: m =>
                                                            m.MateTransportId != mateToAttack.MateTransportId)
                                                        ) //exclude targetted mates
                                                            mate.HitRequest(
                                                                hitRequest: new HitRequest(
                                                                    targetHitType: TargetHitType.SingleAoeTargetHit,
                                                                    session: Session, skill: ski.Skill,
                                                                    skillEffect: characterSkillInfo?.Skill.Effect ??
                                                                                 ski.Skill.Effect,
                                                                    skillBCards: ski.GetSkillBCards()));
                                                    else
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));

                                                    if (!mateToAttack.IsAlive)
                                                        Session.SendPacket(
                                                            packet: StaticPacketHelper.Cancel(type: 2,
                                                                callerId: targetId));
                                                }
                                            }
                                            else
                                            {
                                                var skillCombo = ski.Skill.Combos.Find(match: s => ski.Hit == s.Hit);
                                                if (skillCombo != null)
                                                {
                                                    if (ski.Skill.Combos.OrderByDescending(keySelector: s => s.Hit)
                                                            .First().Hit
                                                        == ski.Hit)
                                                        ski.Hit = 0;

                                                    mateToAttack.HitRequest(
                                                        hitRequest: new HitRequest(
                                                            targetHitType: TargetHitType.SingleTargetHitCombo,
                                                            session: Session,
                                                            skill: ski.Skill, skillCombo: skillCombo,
                                                            skillBCards: ski.GetSkillBCards()));
                                                }
                                                else
                                                {
                                                    mateToAttack.HitRequest(
                                                        hitRequest: new HitRequest(
                                                            targetHitType: TargetHitType.SingleTargetHit,
                                                            session: Session,
                                                            skill: ski.Skill, skillBCards: ski.GetSkillBCards()));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Session.SendPacket(
                                            packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                    }
                                }
                                else
                                {
                                    Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                                }
                            }

                            if (ski.Skill.HitType == 3) Session.Character.MTListTargetQueue.Clear();

                            ski.GetSkillBCards().Where(predicate: s =>
                                    s.Type.Equals(obj: (byte)CardType.Buff) &&
                                    new Buff(id: (short)s.SecondData, level: Session.Character.Level).Card?.BuffType ==
                                    BuffType.Good).ToList()
                                .ForEach(action: s =>
                                    s.ApplyBCards(session: Session.Character.BattleEntity,
                                        sender: Session.Character.BattleEntity));
                        }
                        else
                        {
                            Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                        }

                        //if (ski.Skill.UpgradeSkill == 3 && ski.Skill.SkillType == 1)
                        if (ski.Skill.SkillVNum != 1098 && ski.Skill.SkillVNum != 1330)
                            Session.SendPacket(packet: StaticPacketHelper.SkillResetWithCoolDown(castId: castingId,
                                cooldown: (short)(ski.Skill.Cooldown +
                                                   ski.Skill.Cooldown * cooldownReduction / 100D)));

                        var cdResetMilliseconds =
                            (int)((ski.Skill.Cooldown + ski.Skill.Cooldown * cooldownReduction / 100D) * 100);
                        Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: cdResetMilliseconds))
                            .Subscribe(onNext: o =>
                            {
                                sendSkillReset();
                                if (cdResetMilliseconds <= 500)
                                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 500))
                                        .Subscribe(onNext: obs => sendSkillReset());

                                void sendSkillReset()
                                {
                                    var charSkills = Session.Character.GetSkills();

                                    var skill = charSkills.Find(match: s =>
                                        s.Skill?.CastId == castingId &&
                                        (s.Skill?.UpgradeSkill == 0 || s.Skill?.SkillType == 1));

                                    if (skill != null &&
                                        skill.LastUse.AddMilliseconds(
                                            value: (short)(skill.Skill.Cooldown +
                                                            ski.Skill.Cooldown * cooldownReduction / 100D) * 100 -
                                                   100) <=
                                        DateTime.Now)
                                    {
                                        if (cooldownReduction < 0)
                                            skill.LastUse =
                                                DateTime.Now.AddMilliseconds(value: skill.Skill.Cooldown * 100 * -1);

                                        Session.SendPacket(packet: StaticPacketHelper.SkillReset(castId: castingId));
                                    }
                                }
                            });

                        var fairyWings = Session.Character.GetBuff(type: CardType.EffectSummon, subtype: 11);
                        var random = ServerManager.RandomNumber();
                        if (fairyWings[0] > random)
                            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: o =>
                            {
                                if (ski != null)
                                {
                                    ski.LastUse = DateTime.Now.AddMilliseconds(value: ski.Skill.Cooldown * 100 * -1);
                                    Session.SendPacket(packet: StaticPacketHelper.SkillReset(castId: ski.Skill.CastId));
                                }
                            });
                    }
                    else
                    {
                        Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MP"), type: 10));
                    }
                }
            }
            else
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: targetId));
            }

            if (castingId != 0 && castingId < 11 && shouldCancel || Session.Character.SkillComboCount > 7)
            {
                Session.SendPackets(packets: Session.Character.GenerateQuicklist());

                if (!Session.Character.HasMagicSpellCombo
                    && Session.Character.SkillComboCount > 7)
                    Session.SendPacket(packet: "ms_c 1");
            }

            Session.Character.LastSkillUse = DateTime.Now;
        }

        void ZoneHit(int castingId, short x, short y)
        {
            var characterSkill = Session.Character.GetSkills()?.Find(match: s => s.Skill?.CastId == castingId);
            if (characterSkill == null || !Session.Character.WeaponLoaded(ski: characterSkill)
                                       || !Session.HasCurrentMapInstance
                                       || (x != 0 || y != 0) &&
                                       !Session.Character.IsInRange(xCoordinate: x, yCoordinate: y,
                                           range: characterSkill.GetSkillRange() + 1))
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
                return;
            }

            if (characterSkill.CanBeUsed())
            {
                var mpCost = characterSkill.MpCost();
                short hpCost = 0;

                mpCost = (short)(mpCost * ((100 - Session.Character.CellonOptions
                                                .Where(predicate: s => s.Type == CellonOptionType.MpUsage)
                                                .Sum(selector: s => s.Value)) /
                                            100D));

                if (Session.Character.GetBuff(type: CardType.HealingBurningAndCasting,
                        subtype: (byte)AdditionalTypes.HealingBurningAndCasting.HpDecreasedByConsumingMp)[0] is int
                    HPDecreasedByConsumingMP)
                    if (HPDecreasedByConsumingMP < 0)
                    {
                        var amountDecreased = -(characterSkill.MpCost() * HPDecreasedByConsumingMP / 100);
                        hpCost = (short)amountDecreased;
                        mpCost -= (short)amountDecreased;
                    }

                if (Session.Character.Mp >= mpCost && Session.Character.Hp > hpCost && Session.HasCurrentMapInstance)
                {
                    Session.Character.LastSkillUse = DateTime.Now;

                    double cooldownReduction = Session.Character.GetBuff(type: CardType.Morale,
                        subtype: (byte)AdditionalTypes.Morale.SkillCooldownDecreased)[0];

                    var increaseEnemyCooldownChance = Session.Character.GetBuff(type: CardType.DarkCloneSummon,
                        subtype: (byte)AdditionalTypes.DarkCloneSummon.IncreaseEnemyCooldownChance);

                    if (ServerManager.RandomNumber() < increaseEnemyCooldownChance[0])
                        cooldownReduction -= increaseEnemyCooldownChance[1];

                    Session.CurrentMapInstance.Broadcast(
                        packet: $"ct_n 1 {Session.Character.CharacterId} 3 -1 {characterSkill.Skill.CastAnimation}" +
                                $" {characterSkill.Skill.CastEffect} {characterSkill.Skill.SkillVNum}");
                    characterSkill.LastUse = DateTime.Now;
                    if (!Session.Character.HasGodMode) Session.Character.DecreaseMp(amount: characterSkill.MpCost());

                    characterSkill.LastUse = DateTime.Now;
                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: characterSkill.Skill.CastTime * 100))
                        .Subscribe(onNext: o =>
                        {
                            Session.CurrentMapInstance.Broadcast(
                                packet:
                                $"bs 1 {Session.Character.CharacterId} {x} {y} {characterSkill.Skill.SkillVNum}" +
                                $" {(short)(characterSkill.Skill.Cooldown + characterSkill.Skill.Cooldown * cooldownReduction / 100D)} {characterSkill.Skill.AttackAnimation}" +
                                $" {characterSkill.Skill.Effect} 0 0 1 1 0 0 0");

                            var Range = characterSkill.TargetRange();
                            if (characterSkill.GetSkillBCards().Any(predicate: s =>
                                s.Type == (byte)CardType.FalconSkill &&
                                s.SubType == (byte)AdditionalTypes.FalconSkill.FalconFocusLowestHp / 10))
                            {
                                if (Session.CurrentMapInstance.BattleEntities.Where(predicate: s =>
                                        s.IsInRange(xCoordinate: x, yCoordinate: y, range: Range)
                                        && Session.Character.BattleEntity
                                            .CanAttackEntity(receiver: s))
                                    .OrderBy(keySelector: s => s.Hp).FirstOrDefault() is BattleEntity lowestHPEntity)
                                    Session.Character.MTListTargetQueue.Push(item: new MtListHitTarget(
                                        entityType: lowestHPEntity.UserType,
                                        targetId: lowestHPEntity.MapEntityId,
                                        targetHitType: (TargetHitType)characterSkill.Skill.HitType));
                            }
                            else if (Session.Character.MTListTargetQueue.Count == 0)
                            {
                                Session.CurrentMapInstance.BattleEntities
                                    .Where(predicate: s => s.IsInRange(xCoordinate: x, yCoordinate: y, range: Range) &&
                                                           Session.Character.BattleEntity.CanAttackEntity(receiver: s))
                                    .ToList().ForEach(action: s =>
                                        Session.Character.MTListTargetQueue.Push(item: new MtListHitTarget(
                                            entityType: s.UserType,
                                            targetId: s.MapEntityId,
                                            targetHitType: (TargetHitType)characterSkill.Skill.HitType)));
                            }

                            var count = 0;

                            //foreach (long id in Session.Character.MTListTargetQueue.Where(s => s.EntityType == UserType.Monster).Select(s => s.TargetId))
                            foreach (var id in Session.Character.GetMTListTargetQueue_QuickFix(ski: characterSkill,
                                entityType: UserType.Monster))
                            {
                                var mon = Session.CurrentMapInstance.GetMonsterById(mapMonsterId: id);
                                if (mon?.CurrentHp > 0 && mon?.Owner?.MapEntityId != Session.Character.CharacterId)
                                {
                                    count++;
                                    mon.HitQueue.Enqueue(item: new HitRequest(
                                        targetHitType: TargetHitType.SingleAoeTargetHit, session: Session,
                                        skill: characterSkill.Skill, skillEffect: characterSkill.Skill.Effect, mapX: x,
                                        mapY: y,
                                        showTargetAnimation: count == 0, skillBCards: characterSkill.GetSkillBCards()));
                                }
                            }

                            //foreach (long id in Session.Character.MTListTargetQueue.Where(s => s.EntityType == UserType.Player).Select(s => s.TargetId))
                            foreach (var id in Session.Character.GetMTListTargetQueue_QuickFix(ski: characterSkill,
                                entityType: UserType.Player))
                            {
                                var character = ServerManager.Instance.GetSessionByCharacterId(characterId: id);
                                if (character != null && character.CurrentMapInstance == Session.CurrentMapInstance
                                                      && character.Character.CharacterId !=
                                                      Session.Character.CharacterId)
                                    if (Session.Character.BattleEntity.CanAttackEntity(
                                        receiver: character.Character.BattleEntity))
                                    {
                                        count++;
                                        PvpHit(
                                            hitRequest: new HitRequest(targetHitType: TargetHitType.SingleAoeTargetHit,
                                                session: Session, skill: characterSkill.Skill,
                                                skillEffect: characterSkill.Skill.Effect, mapX: x, mapY: y,
                                                showTargetAnimation: count == 0,
                                                skillBCards: characterSkill.GetSkillBCards()),
                                            target: character);
                                    }
                            }

                            characterSkill.GetSkillBCards().ToList().Where(predicate: s =>
                                    s.Type.Equals(obj: (byte)CardType.Buff) &&
                                    new Buff(id: (short)s.SecondData, level: Session.Character.Level).Card.BuffType
                                        .Equals(
                                            obj: BuffType.Good)
                                    || s.Type.Equals(obj: (byte)CardType.FalconSkill) &&
                                    s.SubType.Equals(obj: (byte)AdditionalTypes.FalconSkill.CausingChanceLocation / 10)
                                    || s.Type.Equals(obj: (byte)CardType.FearSkill) &&
                                    s.SubType.Equals(obj: (byte)AdditionalTypes.FearSkill.ProduceWhenAmbushe / 10))
                                .ToList()
                                .ForEach(action: s =>
                                    s.ApplyBCards(session: Session.Character.BattleEntity,
                                        sender: Session.Character.BattleEntity, x: x, y: y));

                            Session.Character.MTListTargetQueue.Clear();
                        });

                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(
                            value: (short)(characterSkill.Skill.Cooldown +
                                            characterSkill.Skill.Cooldown * cooldownReduction / 100D) * 100))
                        .Subscribe(onNext: o =>
                        {
                            var
                                skill = Session.Character.GetSkills().Find(match: s =>
                                    s.Skill?.CastId
                                    == castingId && (s.Skill?.UpgradeSkill == 0 || s.Skill?.SkillType == 1));
                            if (skill != null &&
                                skill.LastUse.AddMilliseconds(
                                    value: (short)(characterSkill.Skill.Cooldown +
                                                    characterSkill.Skill.Cooldown * cooldownReduction / 100D) * 100 -
                                           100) <=
                                DateTime.Now)
                            {
                                if (cooldownReduction < 0)
                                {
                                    skill.LastUse =
                                        DateTime.Now.AddMilliseconds(value: skill.Skill.Cooldown * 100 * -1);
                                    skill.LastUse.AddSeconds(value: cooldownReduction);
                                }

                                Session.SendPacket(packet: StaticPacketHelper.SkillReset(castId: castingId));
                            }
                        });
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MP"), type: 10));
                    Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
                }
            }
            else
            {
                Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
            }
        }

        #endregion
    }
}