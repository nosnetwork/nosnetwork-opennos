﻿using System;
using OpenNos.Core.Handling;
using OpenNos.GameObject;

namespace OpenNos.Handler
{
    public class UselessPacketHandler : IPacketHandler
    {
        #region Instantiation

        public UselessPacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        public ClientSession Session { get; }

        #endregion

        #region Methods

        public void CClose(CClosePacket cClosePacket)
        {
            if (cClosePacket == null) throw new ArgumentNullException(paramName: nameof(cClosePacket));
            // idk
        }

        public void FStashEnd(FStashEndPacket fStashEndPacket)
        {
            if (fStashEndPacket == null) throw new ArgumentNullException(paramName: nameof(fStashEndPacket));
            // idk
        }

        public void FStashEnd(StashEndPacket stashEndPacket)
        {
            if (stashEndPacket == null) throw new ArgumentNullException(paramName: nameof(stashEndPacket));
            // idk
        }

        public void Lbs(LbsPacket lbsPacket)
        {
            if (lbsPacket == null) throw new ArgumentNullException(paramName: nameof(lbsPacket));
            // idk
        }

        public void ShopClose(ShopClosePacket shopClosePacket)
        {
            if (shopClosePacket == null) throw new ArgumentNullException(paramName: nameof(shopClosePacket));
            // Not needed for now.
        }

        public void Snap(SnapPacket snapPacket)
        {
            if (snapPacket == null) throw new ArgumentNullException(paramName: nameof(snapPacket));
            // Not needed for now. (pictures)
        }

        #endregion
    }
}