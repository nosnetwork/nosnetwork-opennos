using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.Core.Handling;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.GameObject.Packets.ClientPackets;
using OpenNos.Log.Networking;
using OpenNos.Log.Shared;
using static OpenNos.Domain.BCardType;

namespace OpenNos.Handler
{
    public class NpcPacketHandler : IPacketHandler
    {
        #region Instantiation

        public NpcPacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     buy packet
        /// </summary>
        /// <param name="buyPacket"></param>
        public void BuyShop(BuyPacket buyPacket)
        {
            if (Session.Character.InExchangeOrTrade) return;

            var amount = buyPacket.Amount;

            switch (buyPacket.Type)
            {
                case BuyShopType.CharacterShop:
                    if (!Session.HasCurrentMapInstance) return;

                    var shop =
                        Session.CurrentMapInstance.UserShops.FirstOrDefault(predicate: mapshop =>
                            mapshop.Value.OwnerId.Equals(obj: buyPacket.OwnerId));
                    var item = shop.Value?.Items.Find(match: i => i.ShopSlot.Equals(obj: buyPacket.Slot));
                    var sess = ServerManager.Instance.GetSessionByCharacterId(characterId: shop.Value?.OwnerId ?? 0);
                    if (sess == null || item == null || amount <= 0 || amount > 32000) return;

                    Logger.LogUserEvent(logEvent: "ITEM_BUY_PLAYERSHOP", caller: Session.GenerateIdentity(),
                        data:
                        $"From: {buyPacket.OwnerId} IIId: {item.ItemInstance.Id} ItemVNum: {item.ItemInstance.ItemVNum} Amount: {buyPacket.Amount} PricePer: {item.Price}");

                    if (amount > item.SellAmount) amount = item.SellAmount;

                    if (item.Price * amount
                        + sess.Character.Gold
                        > ServerManager.Instance.Configuration.MaxGold)
                    {
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateShopMemo(type: 3,
                            message: Language.Instance.GetMessageFromKey(key: "MAX_GOLD")));
                        return;
                    }

                    if (item.Price * amount >= Session.Character.Gold)
                    {
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateShopMemo(type: 3,
                            message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY")));
                        return;
                    }

                    // check if the item has been removed successfully from previous owner and remove it
                    if (BuyValidate(clientSession: Session, shop: shop, slot: buyPacket.Slot, amount: amount))
                    {
                        Session.Character.Gold -= item.Price * amount;
                        Session.SendPacket(packet: Session.Character.GenerateGold());

                        var shop2 =
                            Session.CurrentMapInstance.UserShops.FirstOrDefault(predicate: s =>
                                s.Value.OwnerId.Equals(obj: buyPacket.OwnerId));
                        LoadShopItem(owner: buyPacket.OwnerId, shop: shop2);

                        if (ServerManager.Instance.Configuration.UseLogService)
                            LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                            {
                                Sender = ServerManager.Instance.GetSessionByCharacterId(characterId: buyPacket.OwnerId)
                                    ?.Character
                                    .Name ?? "Unknown",
                                SenderId = buyPacket.OwnerId,
                                Receiver = Session.Character.Name,
                                ReceiverId = Session.Character.CharacterId,
                                PacketType = LogType.PrivateShopSell,
                                Packet =
                                    $"[ITEM_BUY_PLAYERSHOP]IIId: {item.ItemInstance.Id} ItemVNum: {item.ItemInstance.ItemVNum} Amount: {buyPacket.Amount} Rare: {item.ItemInstance.Rare} Upgrade: {item.ItemInstance.Upgrade} PricePer: {item.Price}"
                            });
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                type: 0));
                    }

                    break;

                case BuyShopType.ItemShop:
                    if (!Session.HasCurrentMapInstance) return;

                    var npc =
                        Session.CurrentMapInstance.Npcs.Find(
                            match: n => n.MapNpcId.Equals(obj: (int)buyPacket.OwnerId));
                    if (npc != null)
                    {
                        var dist = Map.GetDistance(
                            p: new MapCell { X = Session.Character.PositionX, Y = Session.Character.PositionY },
                            q: new MapCell { X = npc.MapX, Y = npc.MapY });
                        if (npc.Shop == null || dist > 5) return;

                        if (npc.Shop.ShopSkills.Count > 0)
                        {
                            if (!npc.Shop.ShopSkills.Exists(match: s => s.SkillVNum == buyPacket.Slot)) return;

                            // skill shop
                            if (Session.Character.UseSp)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "REMOVE_SP"),
                                        type: 0));
                                return;
                            }

                            if (Session.Character.Skills.Any(predicate: s =>
                                s.LastUse.AddMilliseconds(value: s.Skill.Cooldown * 100) > DateTime.Now))
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "SKILL_NEED_COOLDOWN"),
                                        type: 0));
                                return;
                            }

                            var skillinfo = ServerManager.GetSkill(skillVNum: buyPacket.Slot);
                            if (Session.Character.Skills.Any(predicate: s => s.SkillVNum == buyPacket.Slot) ||
                                skillinfo == null) return;

                            Logger.LogUserEvent(logEvent: "SKILL_BUY", caller: Session.GenerateIdentity(),
                                data: $"SkillVNum: {skillinfo.SkillVNum} Price: {skillinfo.Price}");

                            if (Session.Character.Gold < skillinfo.Price)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                        type: 0));
                            }
                            else if (Session.Character.GetCP() < skillinfo.CpCost)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_CP"), type: 0));
                            }
                            else
                            {
                                if (skillinfo.SkillVNum < 200)
                                {
                                    var skillMiniumLevel = 0;
                                    if (skillinfo.MinimumSwordmanLevel == 0 && skillinfo.MinimumArcherLevel == 0
                                                                            && skillinfo.MinimumMagicianLevel == 0)
                                        skillMiniumLevel = skillinfo.MinimumAdventurerLevel;
                                    else
                                        switch (Session.Character.Class)
                                        {
                                            case ClassType.Adventurer:
                                                skillMiniumLevel = skillinfo.MinimumAdventurerLevel;
                                                break;

                                            case ClassType.Swordsman:
                                                skillMiniumLevel = skillinfo.MinimumSwordmanLevel;
                                                break;

                                            case ClassType.Archer:
                                                skillMiniumLevel = skillinfo.MinimumArcherLevel;
                                                break;

                                            case ClassType.Magician:
                                                if (skillinfo.MinimumMagicianLevel > 0)
                                                    skillMiniumLevel = skillinfo.MinimumMagicianLevel;
                                                else
                                                    skillMiniumLevel = skillinfo.MinimumAdventurerLevel;
                                                break;
                                        }

                                    if (skillMiniumLevel == 0)
                                    {
                                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "SKILL_CANT_LEARN"),
                                            type: 0));
                                        return;
                                    }

                                    if (Session.Character.Level < skillMiniumLevel)
                                    {
                                        Session.SendPacket(
                                            packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "LOW_LVL"), type: 0));
                                        return;
                                    }

                                    foreach (var skill in Session.Character.Skills.GetAllItems())
                                        if (skillinfo.CastId == skill.Skill.CastId && skill.Skill.SkillVNum < 200)
                                            Session.Character.Skills.Remove(key: skill.SkillVNum);
                                }
                                else
                                {
                                    if ((byte)Session.Character.Class != skillinfo.Class)
                                    {
                                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "SKILL_CANT_LEARN"),
                                            type: 0));
                                        return;
                                    }

                                    if (Session.Character.JobLevel < skillinfo.LevelMinimum)
                                    {
                                        Session.SendPacket(
                                            packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "LOW_JOB_LVL"),
                                                type: 0));
                                        return;
                                    }

                                    if (skillinfo.UpgradeSkill != 0)
                                    {
                                        var oldupgrade = Session.Character.Skills.FirstOrDefault(predicate: s =>
                                            s.Skill.UpgradeSkill == skillinfo.UpgradeSkill
                                            && s.Skill.UpgradeType == skillinfo.UpgradeType &&
                                            s.Skill.UpgradeSkill != 0);
                                        if (oldupgrade != null)
                                            Session.Character.Skills.Remove(key: oldupgrade.SkillVNum);
                                    }
                                }

                                Session.Character.Skills[key: buyPacket.Slot] = new CharacterSkill
                                {
                                    SkillVNum = buyPacket.Slot,
                                    CharacterId = Session.Character.CharacterId
                                };

                                Session.Character.Gold -= skillinfo.Price;
                                Session.SendPacket(packet: Session.Character.GenerateGold());
                                Session.SendPacket(packet: Session.Character.GenerateSki());
                                Session.SendPackets(packets: Session.Character.GenerateQuicklist());
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "SKILL_LEARNED"), type: 0));
                                Session.SendPacket(packet: Session.Character.GenerateLev());
                                Session.SendPackets(packets: Session.Character.GenerateStatChar());
                            }
                        }
                        else if (npc.Shop.ShopItems.Count > 0)
                        {
                            // npc shop
                            var shopItem = npc.Shop.ShopItems.Find(match: it => it.Slot == buyPacket.Slot);
                            if (shopItem == null || amount <= 0 || amount > 32000) return;

                            var iteminfo = ServerManager.GetItem(vnum: shopItem.ItemVNum);
                            var price = iteminfo.Price * amount;
                            var reputprice = iteminfo.ReputPrice * amount;
                            double percent;
                            switch (Session.Character.GetDignityIco())
                            {
                                case 3:
                                    percent = 1.10;
                                    break;

                                case 4:
                                    percent = 1.20;
                                    break;

                                case 5:
                                case 6:
                                    percent = 1.5;
                                    break;

                                default:
                                    percent = 1;
                                    break;
                            }

                            Logger.LogUserEvent(logEvent: "ITEM_BUY_NPCSHOP", caller: Session.GenerateIdentity(),
                                data:
                                $"From: {npc.MapNpcId} ItemVNum: {iteminfo.VNum} Amount: {buyPacket.Amount} PricePer: {price * percent} ");

                            var rare = shopItem.Rare;
                            if (iteminfo.Type == 0) amount = 1;

                            if (iteminfo.ReputPrice == 0)
                            {
                                if (price < 0 || price * percent > Session.Character.Gold)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateShopMemo(type: 3,
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY")));
                                    return;
                                }
                            }
                            else
                            {
                                if (reputprice <= 0 || reputprice > Session.Character.Reputation)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateShopMemo(type: 3,
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_REPUT")));
                                    return;
                                }

                                var ra = (byte)ServerManager.RandomNumber();

                                if (iteminfo.ReputPrice != 0)
                                    for (var i = 0; i < ItemHelper.BuyCraftRareRate.Length; i++)
                                        if (ra <= ItemHelper.BuyCraftRareRate[i])
                                            rare = (sbyte)i;
                            }

                            var newItems = Session.Character.Inventory.AddNewToInventory(
                                vnum: shopItem.ItemVNum, amount: amount, Rare: rare, Upgrade: shopItem.Upgrade,
                                Design: shopItem.Color);
                            if (newItems.Count == 0)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateShopMemo(type: 3,
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE")));
                                return;
                            }

                            if (newItems.Count > 0)
                            {
                                foreach (var itemInst in newItems)
                                    switch (itemInst.Item.EquipmentSlot)
                                    {
                                        case EquipmentType.Armor:
                                        case EquipmentType.MainWeapon:
                                        case EquipmentType.SecondaryWeapon:
                                            itemInst.SetRarityPoint();
                                            if (iteminfo.ReputPrice > 0)
                                                itemInst.BoundCharacterId = Session.Character.CharacterId;
                                            break;

                                        case EquipmentType.Boots:
                                        case EquipmentType.Gloves:
                                            itemInst.FireResistance =
                                                (short)(itemInst.Item.FireResistance * shopItem.Upgrade);
                                            itemInst.DarkResistance =
                                                (short)(itemInst.Item.DarkResistance * shopItem.Upgrade);
                                            itemInst.LightResistance =
                                                (short)(itemInst.Item.LightResistance * shopItem.Upgrade);
                                            itemInst.WaterResistance =
                                                (short)(itemInst.Item.WaterResistance * shopItem.Upgrade);
                                            break;
                                    }

                                if (iteminfo.ReputPrice == 0)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateShopMemo(type: 1,
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "BUY_ITEM_VALID"),
                                            arg0: iteminfo.Name, arg1: amount)));
                                    Session.Character.Gold -= (long)(price * percent);
                                    Session.SendPacket(packet: Session.Character.GenerateGold());
                                }
                                else
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateShopMemo(type: 1,
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "BUY_ITEM_VALID"),
                                            arg0: iteminfo.Name, arg1: amount)));
                                    Session.Character.Reputation -= reputprice;
                                    Session.SendPacket(packet: Session.Character.GenerateFd());
                                    Session.CurrentMapInstance?.Broadcast(client: Session,
                                        content: Session.Character.GenerateIn(InEffect: 1),
                                        receiver: ReceiverType.AllExceptMe);
                                    Session.CurrentMapInstance?.Broadcast(client: Session,
                                        content: Session.Character.GenerateGidx(),
                                        receiver: ReceiverType.AllExceptMe);
                                    Session.SendPacket(
                                        packet: Session.Character.GenerateSay(
                                            message: string.Format(
                                                format: Language.Instance.GetMessageFromKey(key: "REPUT_DECREASED"),
                                                arg0: reputprice), type: 11));
                                }
                            }
                            else
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                        type: 0));
                            }
                        }
                    }

                    break;
            }
        }

        [Packet("m_shop")]
        public void CreateShop(string packet)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            var packetsplit = packet.Split(' ');
            var type = new InventoryType[20];
            var gold = new long[20];
            var slot = new short[20];
            var qty = new short[20];
            var shopname = "";
            if (packetsplit.Length > 2)
            {
                if (!short.TryParse(s: packetsplit[2], result: out var typePacket)) return;

                if (Session.Character.HasShopOpened && typePacket != 1 || !Session.HasCurrentMapInstance
                                                                       || Session.Character.IsExchanging ||
                                                                       Session.Character.ExchangeInfo != null)
                    return;

                if (Session.CurrentMapInstance.Portals.Any(predicate: por =>
                    Session.Character.PositionX < por.SourceX + 6 && Session.Character.PositionX > por.SourceX - 6
                                                                  && Session.Character.PositionY < por.SourceY + 6 &&
                                                                  Session.Character.PositionY > por.SourceY - 6))
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "SHOP_NEAR_PORTAL"), type: 0));
                    return;
                }

                if (Session.Character.Group != null && Session.Character.Group?.GroupType != GroupType.Group)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "SHOP_NOT_ALLOWED_IN_RAID"),
                            type: 0));
                    return;
                }

                if (!Session.CurrentMapInstance.ShopAllowed)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "SHOP_NOT_ALLOWED"), type: 0));
                    return;
                }

                switch (typePacket)
                {
                    case 2:
                        Session.SendPacket(packet: "ishop");
                        break;

                    case 0:
                        if (Session.CurrentMapInstance.UserShops.Any(predicate: s =>
                            s.Value.OwnerId == Session.Character.CharacterId))
                            return;

                        var myShop = new MapShop();

                        if (packetsplit.Length > 82)
                        {
                            short shopSlot = 0;

                            for (short j = 3, i = 0; j < 82; j += 4, i++)
                            {
                                Enum.TryParse(value: packetsplit[j], result: out type[i]);
                                short.TryParse(s: packetsplit[j + 1], result: out slot[i]);
                                short.TryParse(s: packetsplit[j + 2], result: out qty[i]);

                                long.TryParse(s: packetsplit[j + 3], result: out gold[i]);
                                if (gold[i] < 0) return;

                                if (qty[i] > 0)
                                {
                                    var inv = Session.Character.Inventory.LoadBySlotAndType(slot: slot[i],
                                        type: type[i]);
                                    if (inv != null)
                                    {
                                        if (inv.Amount < qty[i]) return;

                                        if (myShop.Items.Where(predicate: s => s.ItemInstance.ItemVNum == inv.ItemVNum)
                                                .Sum(selector: s => s.SellAmount) + qty[i] >
                                            Session.Character.Inventory.CountItem(itemVNum: inv.ItemVNum)) return;

                                        if (!inv.Item.IsTradable || inv.IsBound)
                                        {
                                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(
                                                    key: "SHOP_ONLY_TRADABLE_ITEMS"), type: 0));
                                            Session.SendPacket(packet: "shop_end 0");
                                            return;
                                        }

                                        var personalshopitem = new PersonalShopItem
                                        {
                                            ShopSlot = shopSlot,
                                            Price = gold[i],
                                            ItemInstance = inv,
                                            SellAmount = qty[i]
                                        };
                                        myShop.Items.Add(item: personalshopitem);
                                        shopSlot++;
                                    }
                                }
                            }
                        }

                        if (myShop.Items.Count != 0)
                        {
                            if (!myShop.Items.Any(predicate: s =>
                                !s.ItemInstance.Item.IsSoldable || s.ItemInstance.IsBound))
                            {
                                for (var i = 83; i < packetsplit.Length; i++) shopname += $"{packetsplit[i]} ";

                                // trim shopname
                                shopname = shopname.TrimEnd(' ');

                                // create default shopname if it's empty
                                if (string.IsNullOrWhiteSpace(value: shopname) || string.IsNullOrEmpty(value: shopname))
                                    shopname = Language.Instance.GetMessageFromKey(key: "SHOP_PRIVATE_SHOP");

                                // truncate the string to a max-length of 20
                                shopname = shopname.Truncate(length: 20);
                                myShop.OwnerId = Session.Character.CharacterId;
                                myShop.Name = shopname;
                                Session.CurrentMapInstance.UserShops.Add(
                                    key: Session.CurrentMapInstance.LastUserShopId++,
                                    value: myShop);

                                Session.Character.HasShopOpened = true;

                                Session.CurrentMapInstance?.Broadcast(client: Session,
                                    content: Session.Character.GeneratePlayerFlag(pflag: Session.CurrentMapInstance
                                        .LastUserShopId),
                                    receiver: ReceiverType.AllExceptMe);
                                Session.CurrentMapInstance?.Broadcast(
                                    packet: Session.Character.GenerateShop(shopname: shopname));
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateInfo(
                                        message: Language.Instance.GetMessageFromKey(key: "SHOP_OPEN")));

                                Session.Character.IsSitting = true;
                                Session.Character.IsShopping = true;

                                Session.Character.LoadSpeed();
                                Session.SendPacket(packet: Session.Character.GenerateCond());
                                Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateRest());
                            }
                            else
                            {
                                Session.SendPacket(packet: "shop_end 0");
                                Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "ITEM_NOT_SOLDABLE"),
                                        type: 10));
                            }
                        }
                        else
                        {
                            Session.SendPacket(packet: "shop_end 0");
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "SHOP_EMPTY"), type: 10));
                        }

                        break;
                    case 1:
                        Session.Character.CloseShop();
                        break;
                }
            }
        }

        /// <summary>
        ///     n_run packet
        /// </summary>
        /// <param name="packet"></param>
        public void NpcRunFunction(NRunPacket packet)
        {
            Session.Character.LastNRunId = packet.NpcId;
            Session.Character.LastItemVNum = 0;

            if (Session.Character.Hp > 0) NRunHandler.NRun(Session: Session, packet: packet);
        }

        /// <summary>
        ///     pdtse packet
        /// </summary>
        /// <param name="pdtsePacket"></param>
        public void Pdtse(PdtsePacket pdtsePacket)
        {
            if (!Session.HasCurrentMapInstance) return;

            var vNum = pdtsePacket.VNum;
            var recipe = ServerManager.Instance.GetAllRecipes().Find(match: s => s.ItemVNum == vNum);
            if (Session.CurrentMapInstance.GetNpc(mapNpcId: Session.Character.LastNpcMonsterId)?.Recipes
                .Find(match: s => s.ItemVNum == vNum) is Recipe recipeNpc) recipe = recipeNpc;
            if (ServerManager.Instance.GetRecipesByItemVNum(itemVNum: Session.Character.LastItemVNum)
                ?.Find(match: s => s.ItemVNum == vNum) is Recipe recipeScroll) recipe = recipeScroll;
            if (pdtsePacket.Type == 1)
            {
                if (recipe?.Amount > 0)
                {
                    var recipePacket = $"m_list 3 {recipe.Amount}";
                    foreach (var ite in recipe.Items.Where(predicate: s =>
                        s.ItemVNum != Session.Character.LastItemVNum || Session.Character.LastItemVNum == 0))
                        if (ite.Amount > 0)
                            recipePacket += $" {ite.ItemVNum} {ite.Amount}";

                    recipePacket += " -1";
                    Session.SendPacket(packet: recipePacket);
                }
            }
            else if (recipe != null)
            {
                // sequential
                //pdtse 0 4955 0 0 1
                // random
                //pdtse 0 4955 0 0 2
                if (recipe.Items.Count < 1 || recipe.Amount <= 0 || recipe.Items.Any(predicate: ite =>
                    Session.Character.Inventory.CountItem(itemVNum: ite.ItemVNum) < ite.Amount))
                    return;

                if (Session.Character.LastItemVNum != 0)
                {
                    if (!ServerManager.Instance.ItemHasRecipe(itemVNum: Session.Character.LastItemVNum)) return;
                    short npcVNum = 0;
                    switch (Session.Character.LastItemVNum)
                    {
                        case 1375:
                            npcVNum = 956;
                            break;
                        case 1376:
                            npcVNum = 957;
                            break;
                        case 1437:
                            npcVNum = 959;
                            break;
                    }

                    if (npcVNum != 0 &&
                        (Session.Character.BattleEntity.IsCampfire(vnum: npcVNum) &&
                         Session.CurrentMapInstance.GetNpc(mapNpcId: Session.Character.LastNpcMonsterId) == null ||
                         Session.CurrentMapInstance.GetNpc(mapNpcId: Session.Character.LastNpcMonsterId)?.NpcVNum !=
                         npcVNum)) return;

                    Session.Character.LastItemVNum = 0;
                }
                else if (!ServerManager.Instance.MapNpcHasRecipe(mapNpcId: Session.Character.LastNpcMonsterId))
                {
                    return;
                }

                switch (recipe.ItemVNum)
                {
                    case 2802:
                        if (Session.Character.Inventory.CountItem(itemVNum: recipe.ItemVNum) >= 5)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "ALREADY_HAVE_MAX_AMOUNT"),
                                    type: 0));
                            return;
                        }

                        break;
                    case 5935:
                        if (Session.Character.Inventory.CountItem(itemVNum: recipe.ItemVNum) >= 3)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "ALREADY_HAVE_MAX_AMOUNT"),
                                    type: 0));
                            return;
                        }

                        break;
                    case 5936:
                    case 5937:
                    case 5938:
                    case 5939:
                        if (Session.Character.Inventory.CountItem(itemVNum: recipe.ItemVNum) >= 10)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "ALREADY_HAVE_MAX_AMOUNT"),
                                    type: 0));
                            return;
                        }

                        break;
                    case 5940:
                        if (Session.Character.Inventory.CountItem(itemVNum: recipe.ItemVNum) >= 4)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "ALREADY_HAVE_MAX_AMOUNT"),
                                    type: 0));
                            return;
                        }

                        break;
                    case 5942:
                    case 5943:
                    case 5944:
                    case 5945:
                    case 5946:
                        if (Session.Character.Inventory.CountItem(itemVNum: recipe.ItemVNum) >= 1)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "ALREADY_HAVE_MAX_AMOUNT"),
                                    type: 0));
                            return;
                        }

                        break;
                }

                var item = ServerManager.GetItem(vnum: recipe.ItemVNum);
                if (item == null) return;

                sbyte rare = 0;
                if (item.EquipmentSlot == EquipmentType.Armor
                    || item.EquipmentSlot == EquipmentType.MainWeapon
                    || item.EquipmentSlot == EquipmentType.SecondaryWeapon)
                {
                    var ra = (byte)ServerManager.RandomNumber();

                    for (var i = 0; i < ItemHelper.BuyCraftRareRate.Length; i++)
                        if (ra <= ItemHelper.BuyCraftRareRate[i])
                            rare = (sbyte)i;
                }

                var inv = Session.Character.Inventory
                    .AddNewToInventory(vnum: recipe.ItemVNum, amount: recipe.Amount, Rare: rare)
                    .FirstOrDefault();
                if (inv != null)
                {
                    if (inv.Item.EquipmentSlot == EquipmentType.Armor
                        || inv.Item.EquipmentSlot == EquipmentType.MainWeapon
                        || inv.Item.EquipmentSlot == EquipmentType.SecondaryWeapon)
                    {
                        inv.SetRarityPoint();
                        if (inv.Item.IsHeroic)
                        {
                            inv.GenerateHeroicShell(protection: RarifyProtection.None, forced: true);
                            inv.BoundCharacterId = Session.Character.CharacterId;
                        }
                    }

                    foreach (var ite in recipe.Items)
                        Session.Character.Inventory.RemoveItemAmount(vnum: ite.ItemVNum, amount: ite.Amount);

                    // pdti {WindowType} {inv.ItemVNum} {recipe.Amount} {Unknown} {inv.Upgrade} {inv.Rare}
                    Session.SendPacket(packet: $"pdti 11 {inv.ItemVNum} {recipe.Amount} 29 {inv.Upgrade} {inv.Rare}");
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: 19, argument: 1,
                        callerId: Session.Character.CharacterId, value: 1324));
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "CRAFTED_OBJECT"),
                            arg0: inv.Item.Name,
                            arg1: recipe.Amount), type: 0));
                    Session.Character.IncrementQuests(type: QuestType.Product, firstData: inv.ItemVNum,
                        secondData: recipe.Amount);
                }
                else
                {
                    Session.SendPacket(packet: "shop_end 0");
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"), type: 0));
                }
            }
        }

        /// <summary>
        ///     ptctl packet
        /// </summary>
        /// <param name="ptCtlPacket"></param>
        public void PetMove(PtCtlPacket ptCtlPacket)
        {
            if (ptCtlPacket.PacketEnd == null || ptCtlPacket.Amount < 1) return;
            var packetsplit = ptCtlPacket.PacketEnd.Split(' ');
            for (var i = 0; i < ptCtlPacket.Amount * 3; i += 3)
                if (packetsplit.Length >= ptCtlPacket.Amount * 3 && int.TryParse(s: packetsplit[i],
                                                                     result: out var petId)
                                                                 && short.TryParse(s: packetsplit[i + 1],
                                                                     result: out var positionX)
                                                                 && short.TryParse(s: packetsplit[i + 2],
                                                                     result: out var positionY))
                {
                    var mate = Session.Character.Mates.Find(match: s => s.MateTransportId == petId);
                    if (mate != null && mate.IsAlive &&
                        !mate.HasBuff(type: CardType.Move, subtype: (byte)AdditionalTypes.Move.MovementImpossible) &&
                        mate.Owner.Session.HasCurrentMapInstance && !mate.Owner.IsChangingMapInstance
                        && Session.CurrentMapInstance?.Map?.IsBlockedZone(x: positionX, y: positionY) == false)
                    {
                        if (mate.Loyalty > 0)
                        {
                            mate.PositionX = positionX;
                            mate.PositionY = positionY;
                            if (Session.Character.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                            {
                                mate.MapX = positionX;
                                mate.MapY = positionY;
                            }

                            Session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.Move(type: UserType.Npc,
                                callerId: petId,
                                positionX: positionX,
                                positionY: positionY, speed: mate.Monster.Speed));
                            if (mate.LastMonsterAggro.AddSeconds(value: 5) > DateTime.Now) mate.UpdateBushFire();

                            Session.CurrentMapInstance?.OnMoveOnMapEvents?.ForEach(
                                action: e => EventHelper.Instance.RunEvent(evt: e));
                            Session.CurrentMapInstance?.OnMoveOnMapEvents?.RemoveAll(match: s => s != null);
                        }

                        Session.SendPacket(packet: mate.GenerateCond());
                    }
                }
        }

        /// <summary>
        ///     say_p packet
        /// </summary>
        /// <param name="sayPPacket"></param>
        public void PetTalk(SayPPacket sayPPacket)
        {
            if (string.IsNullOrEmpty(value: sayPPacket.Message)) return;

            var mate = Session.Character.Mates.Find(match: s => s.MateTransportId == sayPPacket.PetId);
            if (mate != null)
            {
                var penalty = Session.Account.PenaltyLogs.OrderByDescending(keySelector: s => s.DateEnd)
                    .FirstOrDefault();
                var message = sayPPacket.Message;
                if (Session.Character.IsMuted() && penalty != null)
                {
                    if (Session.Character.Gender == GenderType.Female)
                    {
                        var member = ServerManager.Instance.ArenaTeams.ToList()
                            .FirstOrDefault(predicate: s => s.Any(predicate: e => e.Session == Session));
                        if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance &&
                            member != null)
                        {
                            var member2 = member.FirstOrDefault(predicate: o => o.Session == Session);
                            member
                                .Replace(predicate: s =>
                                    member2 != null && s.ArenaTeamType == member2.ArenaTeamType && s != member2)
                                .Replace(predicate: s =>
                                    s.ArenaTeamType == member.FirstOrDefault(predicate: o => o.Session == Session)
                                        ?.ArenaTeamType)
                                .ToList().ForEach(action: o =>
                                    o.Session.SendPacket(
                                        packet: Session.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(key: "MUTED_FEMALE"),
                                            type: 1)));
                        }
                        else
                        {
                            Session.CurrentMapInstance?.Broadcast(client: Session,
                                content: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "MUTED_FEMALE"), type: 1));
                        }

                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                                arg0: (penalty.DateEnd - DateTime.Now).ToString(format: @"hh\:mm\:ss")), type: 11));
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                                arg0: (penalty.DateEnd - DateTime.Now).ToString(format: @"hh\:mm\:ss")), type: 12));
                    }
                    else
                    {
                        var member = ServerManager.Instance.ArenaTeams.ToList()
                            .FirstOrDefault(predicate: s => s.Any(predicate: e => e.Session == Session));
                        if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance &&
                            member != null)
                        {
                            var member2 = member.FirstOrDefault(predicate: o => o.Session == Session);
                            member
                                .Replace(predicate: s =>
                                    member2 != null && s.ArenaTeamType == member2.ArenaTeamType && s != member2)
                                .Replace(predicate: s =>
                                    s.ArenaTeamType == member.FirstOrDefault(predicate: o => o.Session == Session)
                                        ?.ArenaTeamType)
                                .ToList().ForEach(action: o =>
                                    o.Session.SendPacket(
                                        packet: Session.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(key: "MUTED_MALE"),
                                            type: 1)));
                        }
                        else
                        {
                            Session.CurrentMapInstance?.Broadcast(client: Session,
                                content: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "MUTED_MALE"), type: 1));
                        }

                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                                arg0: (penalty.DateEnd - DateTime.Now).ToString(format: @"hh\:mm\:ss")), type: 11));
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                                arg0: (penalty.DateEnd - DateTime.Now).ToString(format: @"hh\:mm\:ss")), type: 12));
                    }
                }
                else
                {
                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogChatMessage(logEntry: new LogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            Receiver = Session.CurrentMapInstance?.Map.Name,
                            ReceiverId = Session.CurrentMapInstance?.Map.MapId,
                            MessageType = LogType.Map,
                            Message = message
                        });

                    Session.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.Say(type: (byte)UserType.Npc,
                        callerId: mate.MateTransportId,
                        secondaryType: 2, message: message));
                }
            }
        }

        /// <summary>
        ///     sell packet
        /// </summary>
        /// <param name="sellPacket"></param>
        public void SellShop(SellPacket sellPacket)
        {
            if (Session.Character.ExchangeInfo?.ExchangeList.Count > 0 || Session.Character.IsShopping) return;

            if (sellPacket.Amount.HasValue && sellPacket.Slot.HasValue)
            {
                var type = (InventoryType)sellPacket.Data;

                if (type == InventoryType.Bazaar) return;

                short amount = sellPacket.Amount.Value, slot = sellPacket.Slot.Value;

                if (amount < 1) return;

                var inv = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: type);

                if (inv == null || amount > inv.Amount) return;

                if (Session.Character.MinilandObjects.Any(predicate: s => s.ItemInstanceId == inv.Id)) return;

                if (!inv.Item.IsSoldable)
                {
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateShopMemo(type: 2,
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ITEM_NOT_SOLDABLE"))));
                    return;
                }

                var price = inv.Item.SellToNpcPrice;

                if (price < 1) price = 1;

                if (Session.Character.Gold + price * amount > ServerManager.Instance.Configuration.MaxGold)
                {
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "MAX_GOLD"),
                        type: 0));
                    return;
                }

                Session.Character.Gold += price * amount;
                Session.SendPacket(packet: UserInterfaceHelper.GenerateShopMemo(type: 1,
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SELL_ITEM_VALID"),
                        arg0: inv.Item.Name, arg1: amount)));

                Session.Character.Inventory.RemoveItemFromInventory(id: inv.Id, amount: amount);
                Session.SendPacket(packet: Session.Character.GenerateGold());
            }
            else
            {
                var vnum = sellPacket.Data;

                var skill = Session.Character.Skills[key: vnum];

                if (skill == null || vnum == 200 + 20 * (byte)Session.Character.Class ||
                    vnum == 201 + 20 * (byte)Session.Character.Class) return;

                Session.Character.Gold -= skill.Skill.Price;
                Session.SendPacket(packet: Session.Character.GenerateGold());

                foreach (var loadedSkill in Session.Character.Skills.GetAllItems())
                    if (skill.Skill.SkillVNum == loadedSkill.Skill.UpgradeSkill)
                        Session.Character.Skills.Remove(key: loadedSkill.SkillVNum);

                Session.Character.Skills.Remove(key: skill.SkillVNum);
                Session.SendPacket(packet: Session.Character.GenerateSki());
                Session.SendPackets(packets: Session.Character.GenerateQuicklist());
                Session.SendPacket(packet: Session.Character.GenerateLev());
            }
        }

        /// <summary>
        ///     shopping packet
        /// </summary>
        /// <param name="shoppingPacket"></param>
        public void Shopping(ShoppingPacket shoppingPacket)
        {
            byte type = shoppingPacket.Type, typeshop = 0;
            var npcId = shoppingPacket.NpcId;
            if (Session.Character.IsShopping || !Session.HasCurrentMapInstance) return;

            var mapnpc = Session.CurrentMapInstance.Npcs.Find(match: n => n.MapNpcId.Equals(obj: npcId));
            if (mapnpc?.Shop == null) return;

            var shoplist = "";
            foreach (var item in mapnpc.Shop.ShopItems.Where(predicate: s => s.Type.Equals(obj: type)))
            {
                var iteminfo = ServerManager.GetItem(vnum: item.ItemVNum);
                typeshop = 100;
                double percent = 1;
                switch (Session.Character.GetDignityIco())
                {
                    case 3:
                        percent = 1.1;
                        typeshop = 110;
                        break;

                    case 4:
                        percent = 1.2;
                        typeshop = 120;
                        break;

                    case 5:
                        percent = 1.5;
                        typeshop = 150;
                        break;

                    case 6:
                        percent = 1.5;
                        typeshop = 150;
                        break;

                    default:
                        if (Session.CurrentMapInstance.Map.MapTypes.Any(predicate: s =>
                            s.MapTypeId == (short)MapTypeEnum.Act4))
                        {
                            percent *= 1.5;
                            typeshop = 150;
                        }

                        break;
                }

                if (Session.CurrentMapInstance.Map.MapTypes.Any(predicate: s =>
                    s.MapTypeId == (short)MapTypeEnum.Act4 && Session.Character.GetDignityIco() == 3))
                {
                    percent = 1.6;
                    typeshop = 160;
                }
                else if (Session.CurrentMapInstance.Map.MapTypes.Any(predicate: s =>
                    s.MapTypeId == (short)MapTypeEnum.Act4 && Session.Character.GetDignityIco() == 4))
                {
                    percent = 1.7;
                    typeshop = 170;
                }
                else if (Session.CurrentMapInstance.Map.MapTypes.Any(predicate: s =>
                    s.MapTypeId == (short)MapTypeEnum.Act4 && Session.Character.GetDignityIco() == 5))
                {
                    percent = 2;
                    typeshop = 200;
                }
                else if
                (Session.CurrentMapInstance.Map.MapTypes.Any(predicate: s =>
                    s.MapTypeId == (short)MapTypeEnum.Act4 && Session.Character.GetDignityIco() == 6))
                {
                    percent = 2;
                    typeshop = 200;
                }

                if (iteminfo.ReputPrice > 0 && iteminfo.Type == 0)
                    shoplist +=
                        $" {(byte)iteminfo.Type}.{item.Slot}.{item.ItemVNum}.{item.Rare}.{(iteminfo.IsColored ? item.Color : item.Upgrade)}.{iteminfo.ReputPrice}";
                else if (iteminfo.ReputPrice > 0 && iteminfo.Type != 0)
                    shoplist += $" {(byte)iteminfo.Type}.{item.Slot}.{item.ItemVNum}.-1.{iteminfo.ReputPrice}";
                else if (iteminfo.Type != 0)
                    shoplist += $" {(byte)iteminfo.Type}.{item.Slot}.{item.ItemVNum}.-1.{iteminfo.Price * percent}";
                else
                    shoplist +=
                        $" {(byte)iteminfo.Type}.{item.Slot}.{item.ItemVNum}.{item.Rare}.{(iteminfo.IsColored ? item.Color : item.Upgrade)}.{iteminfo.Price * percent}";
            }

            foreach (var skill in mapnpc.Shop.ShopSkills.Where(predicate: s => s.Type.Equals(obj: type)))
            {
                var skillinfo = ServerManager.GetSkill(skillVNum: skill.SkillVNum);

                if (skill.Type != 0)
                {
                    typeshop = 1;
                    if (skillinfo.Class == (byte)Session.Character.Class) shoplist += $" {skillinfo.SkillVNum}";
                }
                else
                {
                    shoplist += $" {skillinfo.SkillVNum}";
                }
            }

            Session.SendPacket(packet: $"n_inv 2 {mapnpc.MapNpcId} 0 {typeshop}{shoplist}");
        }

        /// <summary>
        ///     npc_req packet
        /// </summary>
        /// <param name="requestNpcPacket"></param>
        public void ShowShop(RequestNpcPacket requestNpcPacket)
        {
            var owner = requestNpcPacket.Owner;
            if (!Session.HasCurrentMapInstance) return;

            if (requestNpcPacket.Type == 1)
            {
                // User Shop
                var shopList =
                    Session.CurrentMapInstance.UserShops.FirstOrDefault(predicate: s =>
                        s.Value.OwnerId.Equals(obj: owner));
                LoadShopItem(owner: owner, shop: shopList);
            }
            else
            {
                // Npc Shop , ignore if has drop
                var npc = Session.CurrentMapInstance.Npcs.Find(match: n =>
                    n.MapNpcId.Equals(obj: (int)requestNpcPacket.Owner));
                if (npc == null) return;

                var tp = npc.Teleporters?.FirstOrDefault(predicate: t => t?.Type == TeleporterType.TeleportOnMap);
                if (tp != null)
                {
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000, type: 1,
                        argument: $"#guri^710^{tp.MapX}^{tp.MapY}^{npc.MapNpcId}"));
                    return;
                }

                tp = npc.Teleporters?.FirstOrDefault(predicate: t => t?.Type == TeleporterType.TeleportOnOtherMap);
                if (tp != null)
                {
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000, type: 1,
                        argument: $"#guri^711^{tp.TeleporterId}"));
                    return;
                }

                if (npc.Owner != null && Session.Character.BattleEntity.IsSignpost(vnum: npc.NpcVNum))
                {
                    if (npc.Owner.Character?.Session != null && npc.Owner.Character?.Session == Session)
                        ServerManager.Instance.JoinMiniland(session: Session,
                            minilandOwner: npc.Owner.Character?.Session);
                    else
                        Session.SendPacket(
                            packet:
                            $"qna #mjoin^1^{npc.Owner.MapEntityId}^1  {npc.Owner.Character.Name} : {npc.Owner.Character.MinilandMessage} ");
                    return;
                }

                if (npc.Owner != null && npc.BattleEntity.IsCampfire(vnum: npc.NpcVNum))
                {
                    short itemVNum = 0;
                    switch (npc.NpcVNum)
                    {
                        case 956:
                            itemVNum = 1375;
                            break;
                        case 957:
                            itemVNum = 1376;
                            break;
                        case 959:
                            itemVNum = 1437;
                            break;
                    }

                    if (itemVNum != 0)
                    {
                        Session.Character.LastItemVNum = itemVNum;
                        Session.SendPacket(packet: "wopen 27 0");
                        var recipelist = "m_list 2";
                        var recipeList = ServerManager.Instance.GetRecipesByItemVNum(itemVNum: itemVNum);
                        recipelist = recipeList.Where(predicate: s => s.Amount > 0)
                            .Aggregate(seed: recipelist, func: (current, s) => current + $" {s.ItemVNum}");
                        recipelist += " -100";
                        Session.SendPacket(packet: recipelist);
                    }
                }

                #region Quest

                if (Session.Character.Quests.FirstOrDefault(predicate: s =>
                            s.Quest.QuestRewards.Any(predicate: r => r.RewardType == 7 && r.Data == 5513)) is
                        CharacterQuest
                        sp6Quest
                    && sp6Quest.Quest.QuestObjectives.Any(predicate: s =>
                        s.SpecialData != null && s.Data == npc.NpcVNum &&
                        Session.Character.Inventory.CountItem(itemVNum: s.SpecialData.Value) >= s.Objective))
                {
                    short spVNum = 0;
                    switch (Session.Character.Class)
                    {
                        case ClassType.Swordsman:
                            spVNum = 4494;
                            break;
                        case ClassType.Archer:
                            spVNum = 4495;
                            break;
                        case ClassType.Magician:
                            spVNum = 4496;
                            break;
                    }

                    if (spVNum != 0)
                    {
                        var newItem = new ItemInstance(vNum: spVNum, amount: 1);
                        newItem.SpLevel = 80;
                        newItem.Upgrade = 10;
                        newItem.SlDamage = 152;
                        newItem.SlDefence = 0;
                        newItem.SlElement = 70;
                        newItem.SlHp = 38;

                        var newInv = Session.Character.Inventory.AddToInventory(newItem: newItem);
                        if (newInv.Count > 0)
                        {
                            Session.Character.Session.SendPacket(packet: Session.Character.GenerateSay(
                                message:
                                $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {newItem.Item.Name}",
                                type: 10));
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                    type: 0));
                            return;
                        }
                    }
                }

                Session.Character.IncrementQuests(type: QuestType.Dialog1, firstData: npc.NpcVNum);
                Session.Character.IncrementQuests(type: QuestType.Wear, firstData: npc.NpcVNum);
                Session.Character.IncrementQuests(type: QuestType.Brings, firstData: npc.NpcVNum);
                Session.Character.IncrementQuests(type: QuestType.Required, firstData: npc.NpcVNum);

                if (Session.Character.Quests.Any(predicate: s => s.QuestId == 6001 && npc.NpcVNum == 2051))
                    Session.SendPacket(packet: $"npc_req 2 {npc.MapNpcId} 512");

                if (Session.Character.LastQuest.AddSeconds(value: 1) > DateTime.Now) return;

                #endregion

                if (Session.Character.Quests.FirstOrDefault(predicate: s => s.Quest.DialogNpcVNum == npc.NpcVNum) is
                    CharacterQuest
                    npcDialogQuest && npcDialogQuest.Quest.DialogNpcId != null)
                {
                    Session.SendPacket(packet: $"npc_req 2 {npc.MapNpcId} {npcDialogQuest.Quest.DialogNpcId}");
                }
                else if ((npc.Npc.Drops.Any(predicate: s => s.MonsterVNum != null) ||
                          npc.NpcVNum == 856) && npc.Npc.Race == 8
                                              && (npc.Npc.RaceType ==
                                                  7 || npc.Npc
                                                      .RaceType == 5))
                {
                    if (npc.Npc.VNumRequired > 10 &&
                        Session.Character.Inventory.CountItem(itemVNum: npc.Npc.VNumRequired) <
                        npc.Npc.AmountRequired)
                    {
                        if (ServerManager.GetItem(vnum: npc.Npc.VNumRequired) is Item requiredItem)
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_ITEMS"),
                                        arg0: npc.Npc.AmountRequired, arg1: requiredItem.Name), type: 0));
                        return;
                    }

                    Session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000, type: 4,
                        argument: $"#guri^400^{npc.MapNpcId}"));
                }
                else if (npc.Npc.VNumRequired > 0 && npc.Npc.Race == 8
                                                  && (npc.Npc.RaceType == 7 || npc.Npc.RaceType == 5))
                {
                    if (npc.Npc.VNumRequired > 10 &&
                        Session.Character.Inventory.CountItem(itemVNum: npc.Npc.VNumRequired) <
                        npc.Npc.AmountRequired)
                    {
                        if (ServerManager.GetItem(vnum: npc.Npc.VNumRequired) is Item requiredItem)
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_ITEMS"),
                                        arg0: npc.Npc.AmountRequired, arg1: requiredItem.Name), type: 0));
                        return;
                    }

                    Session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 6000, type: 4,
                        argument: $"#guri^400^{npc.MapNpcId}"));
                }
                else if (npc.Npc.MaxHp == 0 && npc.Npc.Drops.All(predicate: s => s.MonsterVNum == null) &&
                         npc.Npc.Race == 8
                         && (npc.Npc.RaceType == 7 || npc.Npc.RaceType == 5))
                {
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000, type: 1,
                        argument: $"#guri^710^{npc.MapX}^{npc.MapY}^{npc.MapNpcId}")); // #guri^710^DestinationX^DestinationY^MapNpcId
                }
                else if (!string.IsNullOrEmpty(value: npc.GetNpcDialog()))
                {
                    Session.SendPacket(packet: npc.GetNpcDialog());
                }
            }
        }

        bool BuyValidate(ClientSession clientSession, KeyValuePair<long, MapShop> shop, short slot,
            short amount)
        {
            if (!clientSession.HasCurrentMapInstance) return false;

            var shopitem = clientSession.CurrentMapInstance.UserShops[key: shop.Key].Items
                .Find(match: i => i.ShopSlot.Equals(obj: slot));
            if (shopitem == null) return false;

            var id = shopitem.ItemInstance.Id;

            var shopOwnerSession = ServerManager.Instance.GetSessionByCharacterId(characterId: shop.Value.OwnerId);
            if (shopOwnerSession == null) return false;

            if (amount > shopitem.SellAmount) amount = shopitem.SellAmount;

            var sellerItem = shopOwnerSession.Character.Inventory.GetItemInstanceById(id: id);
            if (sellerItem == null || sellerItem.Amount < amount) return false;

            var inv = shopitem.ItemInstance.Type == InventoryType.Equipment
                ? clientSession.Character.Inventory.AddToInventory(newItem: shopitem.ItemInstance)
                : clientSession.Character.Inventory.AddNewToInventory(vnum: shopitem.ItemInstance.ItemVNum,
                    amount: amount,
                    type: shopitem.ItemInstance.Type);

            if (inv.Count == 0) return false;

            shopOwnerSession.Character.Gold += shopitem.Price * amount;
            shopOwnerSession.SendPacket(packet: shopOwnerSession.Character.GenerateGold());
            shopOwnerSession.SendPacket(packet: UserInterfaceHelper.GenerateShopMemo(type: 1,
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "BUY_ITEM"),
                    arg0: Session.Character.Name,
                    arg1: shopitem.ItemInstance.Item.Name, arg2: amount)));
            clientSession.CurrentMapInstance.UserShops[key: shop.Key].Sell += shopitem.Price * amount;

            if (shopitem.ItemInstance.Type != InventoryType.Equipment)
            {
                // remove sold amount of items
                shopOwnerSession.Character.Inventory.RemoveItemFromInventory(id: id, amount: amount);

                // remove sold amount from sellamount
                shopitem.SellAmount -= amount;
            }
            else
            {
                // remove equipment
                shopOwnerSession.Character.Inventory.Remove(key: shopitem.ItemInstance.Id);

                // send empty slot to owners inventory
                shopOwnerSession.SendPacket(
                    packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: shopitem.ItemInstance.Type,
                        slot: shopitem.ItemInstance.Slot));

                // remove the sell amount
                shopitem.SellAmount = 0;
            }

            // remove item from shop if the amount the user wanted to sell has been sold
            if (shopitem.SellAmount == 0)
                clientSession.CurrentMapInstance.UserShops[key: shop.Key].Items.Remove(item: shopitem);

            // update currently sold item
            shopOwnerSession.SendPacket(packet: $"sell_list {shop.Value.Sell} {slot}.{amount}.{shopitem.SellAmount}");

            // end shop
            if (!clientSession.CurrentMapInstance.UserShops[key: shop.Key].Items.Any(predicate: s => s.SellAmount > 0))
                shopOwnerSession.Character.CloseShop();

            return true;
        }

        void LoadShopItem(long owner, KeyValuePair<long, MapShop> shop)
        {
            var packetToSend = $"n_inv 1 {owner} 0 0";

            if (shop.Value?.Items != null)
                foreach (var item in shop.Value.Items)
                    if (item != null)
                    {
                        if (item.ItemInstance.Item.Type == InventoryType.Equipment)
                            packetToSend +=
                                $" 0.{item.ShopSlot}.{item.ItemInstance.ItemVNum}.{item.ItemInstance.Rare}.{item.ItemInstance.Upgrade}.{item.Price}";
                        else
                            packetToSend +=
                                $" {(byte)item.ItemInstance.Item.Type}.{item.ShopSlot}.{item.ItemInstance.ItemVNum}.{item.SellAmount}.{item.Price}.-1";
                    }
                    else
                    {
                        packetToSend += " -1";
                    }

#pragma warning disable CS1030 // #warning: "Check this"
#warning Check this
            packetToSend +=
#pragma warning restore CS1030 // #warning: "Check this"
                " -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1";

            Session.SendPacket(packet: packetToSend);
        }

        #endregion
    }
}