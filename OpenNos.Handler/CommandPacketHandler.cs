﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Handling;
using OpenNos.Core.Serializing;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Buff;
using OpenNos.GameObject.CommandPackets;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Log.Networking;
using OpenNos.Log.Shared;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;
using MapInstance = OpenNos.GameObject.MapInstance;

namespace OpenNos.Handler
{
    public class CommandPacketHandler : IPacketHandler
    {
        #region Instantiation

        public CommandPacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        public void AddUserLog(AddUserLogPacket addUserLogPacket)
        {
            if (addUserLogPacket == null
                || string.IsNullOrEmpty(value: addUserLogPacket.Username))
                return;

            ClientSession.UserLog.Add(item: addUserLogPacket.Username);

            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                    type: 10));
        }

        public void UserLog(UserLogPacket userLogPacket)
        {
            if (userLogPacket == null) return;

            var n = 1;

            foreach (var username in ClientSession.UserLog)
                Session.SendPacket(packet: Session.Character.GenerateSay(message: $"{n++}- {username}", type: 12));

            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                    type: 10));
        }

        public void RemoveUserLog(RemoveUserLogPacket removeUserLogPacket)
        {
            if (removeUserLogPacket == null
                || string.IsNullOrEmpty(value: removeUserLogPacket.Username))
                return;

            if (ClientSession.UserLog.Contains(item: removeUserLogPacket.Username))
                ClientSession.UserLog.RemoveAll(match: username => username == removeUserLogPacket.Username);

            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                    type: 10));
        }

        public void PartnerSpXp(PartnerSpXpPacket partnerSpXpPacket)
        {
            if (partnerSpXpPacket == null) return;

            var mate = Session.Character.Mates?.ToList()
                .FirstOrDefault(predicate: s => s.IsTeamMember && s.MateType == MateType.Partner);

            if (mate?.Sp != null)
            {
                mate.Sp.FullXp();
                Session.SendPacket(packet: mate.GenerateScPacket());
            }
        }

        public void Act4Stat(Act4StatPacket packet)
        {
            if (packet != null && ServerManager.Instance.ChannelId == 51)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Act4Stat]Faction: {packet.Faction} Value: {packet.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = packet.OriginalHeader
                    });
                switch (packet.Faction)
                {
                    case 1:
                        ServerManager.Instance.Act4AngelStat.Percentage = packet.Value;
                        break;

                    case 2:
                        ServerManager.Instance.Act4DemonStat.Percentage = packet.Value;
                        break;
                }

                Parallel.ForEach(source: ServerManager.Instance.Sessions,
                    body: sess => sess.SendPacket(packet: sess.Character.GenerateFc()));
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Act4StatPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $AddMonster Command
        /// </summary>
        /// <param name="addMonsterPacket"></param>
        public void AddMonster(AddMonsterPacket addMonsterPacket)
        {
            if (addMonsterPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[AddMonster]NpcMonsterVNum: {addMonsterPacket.MonsterVNum} IsMoving: {addMonsterPacket.IsMoving}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[AddMonster]NpcMonsterVNum: {addMonsterPacket.MonsterVNum} IsMoving: {addMonsterPacket.IsMoving}"
                    });

                if (!Session.HasCurrentMapInstance) return;

                var npcmonster = ServerManager.GetNpcMonster(npcVNum: addMonsterPacket.MonsterVNum);
                if (npcmonster == null) return;

                var monst = new MapMonsterDto
                {
                    MonsterVNum = addMonsterPacket.MonsterVNum,
                    MapY = Session.Character.PositionY,
                    MapX = Session.Character.PositionX,
                    MapId = Session.Character.MapInstance.Map.MapId,
                    Position = Session.Character.Direction,
                    IsMoving = addMonsterPacket.IsMoving,
                    MapMonsterId = ServerManager.Instance.GetNextMobId()
                };
                if (!DaoFactory.MapMonsterDao.DoesMonsterExist(mapMonsterId: monst.MapMonsterId))
                {
                    DaoFactory.MapMonsterDao.Insert(mapMonster: monst);
                    if (DaoFactory.MapMonsterDao.LoadById(mapMonsterId: monst.MapMonsterId) is MapMonsterDto monsterDTO)
                    {
                        var monster = new MapMonster(input: monsterDTO);
                        monster.Initialize(currentMapInstance: Session.CurrentMapInstance);
                        Session.CurrentMapInstance.AddMonster(monster: monster);
                        Session.CurrentMapInstance?.Broadcast(packet: monster.GenerateIn());
                    }
                }

                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: AddMonsterPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $AddNpc Command
        /// </summary>
        /// <param name="addNpcPacket"></param>
        public void AddNpc(AddNpcPacket addNpcPacket)
        {
            if (addNpcPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[AddNpc]NpcMonsterVNum: {addNpcPacket.NpcVNum} IsMoving: {addNpcPacket.IsMoving}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[AddNpc]NpcMonsterVNum: {addNpcPacket.NpcVNum} IsMoving: {addNpcPacket.IsMoving}"
                    });

                if (!Session.HasCurrentMapInstance) return;

                var npcmonster = ServerManager.GetNpcMonster(npcVNum: addNpcPacket.NpcVNum);
                if (npcmonster == null) return;

                var newNpc = new MapNpcDto
                {
                    NpcVNum = addNpcPacket.NpcVNum,
                    MapY = Session.Character.PositionY,
                    MapX = Session.Character.PositionX,
                    MapId = Session.Character.MapInstance.Map.MapId,
                    Position = Session.Character.Direction,
                    IsMoving = addNpcPacket.IsMoving,
                    MapNpcId = ServerManager.Instance.GetNextNpcId()
                };
                if (!DaoFactory.MapNpcDao.DoesNpcExist(mapNpcId: newNpc.MapNpcId))
                {
                    DaoFactory.MapNpcDao.Insert(npc: newNpc);
                    if (DaoFactory.MapNpcDao.LoadById(mapNpcId: newNpc.MapNpcId) is MapNpcDto npcDTO)
                    {
                        var npc = new MapNpc(input: npcDTO);
                        npc.Initialize(currentMapInstance: Session.CurrentMapInstance);
                        Session.CurrentMapInstance.AddNpc(npc: npc);
                        Session.CurrentMapInstance?.Broadcast(packet: npc.GenerateIn());
                    }
                }

                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: AddNpcPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $AddPartner Command
        /// </summary>
        /// <param name="addPartnerPacket"></param>
        public void AddPartner(AddPartnerPacket addPartnerPacket)
        {
            if (addPartnerPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[AddPartner]NpcMonsterVNum: {addPartnerPacket.MonsterVNum} Level: {addPartnerPacket.Level}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[AddPartner]NpcMonsterVNum: {addPartnerPacket.MonsterVNum} Level: {addPartnerPacket.Level}"
                    });

                AddMate(vnum: addPartnerPacket.MonsterVNum, level: addPartnerPacket.Level, mateType: MateType.Partner);
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: AddPartnerPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $AddPet Command
        /// </summary>
        /// <param name="addPetPacket"></param>
        public void AddPet(AddPetPacket addPetPacket)
        {
            if (addPetPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[AddPet]NpcMonsterVNum: {addPetPacket.MonsterVNum} Level: {addPetPacket.Level}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[AddPet]NpcMonsterVNum: {addPetPacket.MonsterVNum} Level: {addPetPacket.Level}"
                    });

                AddMate(vnum: addPetPacket.MonsterVNum, level: addPetPacket.Level, mateType: MateType.Pet);
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: AddPartnerPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $AddPortal Command
        /// </summary>
        /// <param name="addPortalPacket"></param>
        public void AddPortal(AddPortalPacket addPortalPacket)
        {
            if (addPortalPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[AddPortal]DestinationMapId: {addPortalPacket.DestinationMapId} DestinationMapX: {addPortalPacket.DestinationX} DestinationY: {addPortalPacket.DestinationY}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[AddPortal]DestinationMapId: {addPortalPacket.DestinationMapId} DestinationMapX: {addPortalPacket.DestinationX} DestinationY: {addPortalPacket.DestinationY}"
                    });

                AddPortal(destinationMapId: addPortalPacket.DestinationMapId,
                    destinationX: addPortalPacket.DestinationX, destinationY: addPortalPacket.DestinationY,
                    type: addPortalPacket.PortalType == null ? (short)-1 : (short)addPortalPacket.PortalType,
                    insertToDatabase: true);
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: AddPortalPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $AddShellEffect Command
        /// </summary>
        /// <param name="addShellEffectPacket"></param>
        public void AddShellEffect(AddShellEffectPacket addShellEffectPacket)
        {
            if (addShellEffectPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[AddShellEffect]Slot: {addShellEffectPacket.Slot} EffectLevel: {addShellEffectPacket.EffectLevel} Effect: {addShellEffectPacket.Effect} Value: {addShellEffectPacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[AddShellEffect]Slot: {addShellEffectPacket.Slot} EffectLevel: {addShellEffectPacket.EffectLevel} Effect: {addShellEffectPacket.Effect} Value: {addShellEffectPacket.Value}"
                    });

                try
                {
                    var instance =
                        Session.Character.Inventory.LoadBySlotAndType(slot: addShellEffectPacket.Slot,
                            type: InventoryType.Equipment);
                    if (instance != null)
                        instance.ShellEffects.Add(item: new ShellEffectDto
                        {
                            EffectLevel = (ShellEffectLevelType)addShellEffectPacket.EffectLevel,
                            Effect = addShellEffectPacket.Effect,
                            Value = addShellEffectPacket.Value,
                            EquipmentSerialId = instance.EquipmentSerialId
                        });
                }
                catch (Exception)
                {
                    Session.SendPacket(packet: Session.Character.GenerateSay(message: AddShellEffectPacket.ReturnHelp(),
                        type: 10));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: AddShellEffectPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $AddSkill Command
        /// </summary>
        /// <param name="addSkillPacket"></param>
        public void AddSkill(AddSkillPacket addSkillPacket)
        {
            if (addSkillPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[AddSkill]SkillVNum: {addSkillPacket.SkillVNum}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[AddSkill]SkillVNum: {addSkillPacket.SkillVNum}"
                    });

                Session.Character.AddSkill(skillVNum: addSkillPacket.SkillVNum);
                Session.SendPacket(packet: Session.Character.GenerateSki());
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: AddSkillPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $ArenaWinner Command
        /// </summary>
        /// <param name="arenaWinner"></param>
        public void ArenaWinner(ArenaWinnerPacket arenaWinner)
        {
            if (arenaWinner == null) throw new ArgumentNullException(paramName: nameof(arenaWinner));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[ArenaWinner]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[ArenaWinner]"
                });

            Session.Character.ArenaWinner = Session.Character.ArenaWinner == 0 ? 1 : 0;
            Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateCMode());
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                    type: 10));
        }

        /// <summary>
        ///     $Ban Command
        /// </summary>
        /// <param name="banPacket"></param>
        public void Ban(BanPacket banPacket)
        {
            if (banPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Ban]CharacterName: {banPacket.CharacterName} Reason: {banPacket.Reason} Until: {(banPacket.Duration == 0 ? DateTime.Now.AddYears(value: 15) : DateTime.Now.AddDays(value: banPacket.Duration))}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Ban]CharacterName: {banPacket.CharacterName} Reason: {banPacket.Reason} Until: {(banPacket.Duration == 0 ? DateTime.Now.AddYears(value: 15) : DateTime.Now.AddDays(value: banPacket.Duration))}"
                    });

                BanMethod(characterName: banPacket.CharacterName, duration: banPacket.Duration,
                    reason: banPacket.Reason);
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: BanPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Bank Command
        /// </summary>
        /// <param name="bankPacket"></param>
        public void BankManagement(BankPacket bankPacket)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            if (bankPacket != null)
                switch (bankPacket.Mode?.ToLower())
                {
                    case "balance":
                        {
                            Logger.LogEvent(logEvent: "BANK",
                                data: $"[{Session.GenerateIdentity()}][Balance]Balance: {Session.Character.GoldBank}");

                            if (ServerManager.Instance.Configuration.UseLogService)
                                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                {
                                    Sender = Session.Character.Name,
                                    SenderId = Session.Character.CharacterId,
                                    PacketType = LogType.UserCommand,
                                    Packet = $"[{Session.GenerateIdentity()}][Balance]Balance: {Session.Character.GoldBank}"
                                });

                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: $"Current Balance: {Session.Character.GoldBank} Gold.", type: 10));
                            return;
                        }
                    case "deposit":
                        {
                            if (bankPacket.Param1 != null
                                && (long.TryParse(s: bankPacket.Param1, result: out var amount) || string.Equals(
                                    a: bankPacket.Param1,
                                    b: "all", comparisonType: StringComparison.OrdinalIgnoreCase)))
                            {
                                if (string.Equals(a: bankPacket.Param1, b: "all",
                                        comparisonType: StringComparison.OrdinalIgnoreCase)
                                    && Session.Character.Gold > 0)
                                {
                                    Logger.LogEvent(logEvent: "BANK",
                                        data:
                                        $"[{Session.GenerateIdentity()}][Deposit]Amount: {Session.Character.Gold} OldBank: {Session.Character.GoldBank} NewBank: {Session.Character.GoldBank + Session.Character.Gold}");

                                    if (ServerManager.Instance.Configuration.UseLogService)
                                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                        {
                                            Sender = Session.Character.Name,
                                            SenderId = Session.Character.CharacterId,
                                            PacketType = LogType.UserCommand,
                                            Packet =
                                                $"[{Session.GenerateIdentity()}][Deposit]Amount: {Session.Character.Gold} OldBank: {Session.Character.GoldBank} NewBank: {Session.Character.GoldBank + Session.Character.Gold}"
                                        });

                                    Session.SendPacket(
                                        packet: Session.Character.GenerateSay(
                                            message: $"Deposited ALL({Session.Character.Gold}) Gold.",
                                            type: 10));
                                    Session.Character.GoldBank += Session.Character.Gold;
                                    Session.Character.Gold = 0;
                                    Session.SendPacket(packet: Session.Character.GenerateGold());
                                    Session.SendPacket(
                                        packet: Session.Character.GenerateSay(
                                            message: $"New Balance: {Session.Character.GoldBank} Gold.",
                                            type: 10));
                                }
                                else if (amount <= Session.Character.Gold && Session.Character.Gold > 0)
                                {
                                    if (amount < 1)
                                    {
                                        Logger.LogEvent(logEvent: "BANK",
                                            data:
                                            $"[{Session.GenerateIdentity()}][Illegal]Mode: {bankPacket.Mode} Param1: {bankPacket.Param1} Param2: {bankPacket.Param2}");

                                        if (ServerManager.Instance.Configuration.UseLogService)
                                            LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                            {
                                                Sender = Session.Character.Name,
                                                SenderId = Session.Character.CharacterId,
                                                PacketType = LogType.UserCommand,
                                                Packet =
                                                    $"[{Session.GenerateIdentity()}][Illegal]Mode: {bankPacket.Mode} Param1: {bankPacket.Param1} Param2: {bankPacket.Param2}"
                                            });

                                        Session.SendPacket(packet: Session.Character.GenerateSay(
                                            message: "I'm afraid I can't let you do that. This incident has been logged.",
                                            type: 10));
                                    }
                                    else
                                    {
                                        Logger.LogEvent(logEvent: "BANK",
                                            data:
                                            $"[{Session.GenerateIdentity()}][Deposit]Amount: {amount} OldBank: {Session.Character.GoldBank} NewBank: {Session.Character.GoldBank + amount}");

                                        if (ServerManager.Instance.Configuration.UseLogService)
                                            LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                            {
                                                Sender = Session.Character.Name,
                                                SenderId = Session.Character.CharacterId,
                                                PacketType = LogType.UserCommand,
                                                Packet =
                                                    $"[{Session.GenerateIdentity()}][Deposit]Amount: {amount} OldBank: {Session.Character.GoldBank} NewBank: {Session.Character.GoldBank + amount}"
                                            });

                                        Session.SendPacket(
                                            packet: Session.Character.GenerateSay(message: $"Deposited {amount} Gold.",
                                                type: 10));
                                        Session.Character.GoldBank += amount;
                                        Session.Character.Gold -= amount;
                                        Session.SendPacket(packet: Session.Character.GenerateGold());
                                        Session.SendPacket(
                                            packet: Session.Character.GenerateSay(
                                                message: $"New Balance: {Session.Character.GoldBank} Gold.", type: 10));
                                    }
                                }
                            }

                            return;
                        }
                    case "withdraw":
                        {
                            if (bankPacket.Param1 != null && long.TryParse(s: bankPacket.Param1, result: out var amount)
                                                          && amount <= Session.Character.GoldBank &&
                                                          Session.Character.GoldBank > 0
                                                          && Session.Character.Gold + amount <=
                                                          ServerManager.Instance.Configuration.MaxGold)
                            {
                                if (amount < 1)
                                {
                                    Logger.LogEvent(logEvent: "BANK",
                                        data:
                                        $"[{Session.GenerateIdentity()}][Illegal]Mode: {bankPacket.Mode} Param1: {bankPacket.Param1} Param2: {bankPacket.Param2}");

                                    if (ServerManager.Instance.Configuration.UseLogService)
                                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                        {
                                            Sender = Session.Character.Name,
                                            SenderId = Session.Character.CharacterId,
                                            PacketType = LogType.UserCommand,
                                            Packet =
                                                $"[{Session.GenerateIdentity()}][Illegal]Mode: {bankPacket.Mode} Param1: {bankPacket.Param1} Param2: {bankPacket.Param2}"
                                        });

                                    Session.SendPacket(packet: Session.Character.GenerateSay(
                                        message: "I'm afraid I can't let you do that. This incident has been logged.",
                                        type: 10));
                                }
                                else
                                {
                                    Logger.LogEvent(logEvent: "BANK",
                                        data:
                                        $"[{Session.GenerateIdentity()}][Withdraw]Amount: {amount} OldBank: {Session.Character.GoldBank} NewBank: {Session.Character.GoldBank - amount}");

                                    if (ServerManager.Instance.Configuration.UseLogService)
                                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                        {
                                            Sender = Session.Character.Name,
                                            SenderId = Session.Character.CharacterId,
                                            PacketType = LogType.UserCommand,
                                            Packet =
                                                $"[{Session.GenerateIdentity()}][Withdraw]Amount: {amount} OldBank: {Session.Character.GoldBank} NewBank: {Session.Character.GoldBank - amount}"
                                        });

                                    Session.SendPacket(
                                        packet: Session.Character.GenerateSay(message: $"Withdrawn {amount} Gold.",
                                            type: 10));
                                    Session.Character.GoldBank -= amount;
                                    Session.Character.Gold += amount;
                                    Session.SendPacket(packet: Session.Character.GenerateGold());
                                    Session.SendPacket(
                                        packet: Session.Character.GenerateSay(
                                            message: $"New Balance: {Session.Character.GoldBank} Gold.",
                                            type: 10));
                                }
                            }

                            return;
                        }
                    case "send":
                        {
                            if (bankPacket.Param1 != null)
                            {
                                var amount = bankPacket.Param2;
                                var receiver =
                                    ServerManager.Instance.GetSessionByCharacterName(name: bankPacket.Param1);
                                if (amount <= Session.Character.GoldBank && Session.Character.GoldBank > 0
                                                                         && receiver != null)
                                {
                                    if (amount < 1)
                                    {
                                        Logger.LogEvent(logEvent: "BANK",
                                            data:
                                            $"[{Session.GenerateIdentity()}][Illegal]Mode: {bankPacket.Mode} Param1: {bankPacket.Param1} Param2: {bankPacket.Param2}");

                                        if (ServerManager.Instance.Configuration.UseLogService)
                                            LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                            {
                                                Sender = Session.Character.Name,
                                                SenderId = Session.Character.CharacterId,
                                                PacketType = LogType.UserCommand,
                                                Packet =
                                                    $"[{Session.GenerateIdentity()}][Illegal]Mode: {bankPacket.Mode} Param1: {bankPacket.Param1} Param2: {bankPacket.Param2}"
                                            });

                                        Session.SendPacket(packet: Session.Character.GenerateSay(
                                            message: "I'm afraid I can't let you do that. This incident has been logged.",
                                            type: 10));
                                    }
                                    else
                                    {
                                        Logger.LogEvent(logEvent: "BANK",
                                            data:
                                            $"[{Session.GenerateIdentity()}][Send]Amount: {amount} OldBankSender: {Session.Character.GoldBank} NewBankSender: {Session.Character.GoldBank - amount} OldBankReceiver: {receiver.Character.GoldBank} NewBankReceiver: {receiver.Character.GoldBank + amount}");

                                        if (ServerManager.Instance.Configuration.UseLogService)
                                            LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                            {
                                                Sender = Session.Character.Name,
                                                SenderId = Session.Character.CharacterId,
                                                PacketType = LogType.UserCommand,
                                                Packet =
                                                    $"[{Session.GenerateIdentity()}][Send]Amount: {amount} OldBankSender: {Session.Character.GoldBank} NewBankSender: {Session.Character.GoldBank - amount} OldBankReceiver: {receiver.Character.GoldBank} NewBankReceiver: {receiver.Character.GoldBank + amount}"
                                            });

                                        Session.SendPacket(
                                            packet: Session.Character.GenerateSay(
                                                message: $"Sent {amount} Gold to {receiver.Character.Name}", type: 10));
                                        receiver.SendPacket(
                                            packet: Session.Character.GenerateSay(
                                                message: $"Received {amount} Gold from {Session.Character.Name}",
                                                type: 10));
                                        Session.Character.GoldBank -= amount;
                                        receiver.Character.GoldBank += amount;
                                        Session.SendPacket(
                                            packet: Session.Character.GenerateSay(
                                                message: $"New Balance: {Session.Character.GoldBank} Gold.", type: 10));
                                        receiver.SendPacket(
                                            packet: Session.Character.GenerateSay(
                                                message: $"New Balance: {receiver.Character.GoldBank} Gold.", type: 10));
                                    }
                                }
                            }

                            return;
                        }
                    default:
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(message: BankPacket.ReturnHelp(), type: 10));
                            return;
                        }
                }

            Session.SendPacket(packet: Session.Character.GenerateSay(message: BankPacket.ReturnHelp(), type: 10));
        }

        /// <summary>
        ///     $BlockExp Command
        /// </summary>
        /// <param name="blockExpPacket"></param>
        public void BlockExp(BlockExpPacket blockExpPacket)
        {
            if (blockExpPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[BlockExp]CharacterName: {blockExpPacket.CharacterName} Reason: {blockExpPacket.Reason} Until: {DateTime.Now.AddMinutes(value: blockExpPacket.Duration)}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[BlockExp]CharacterName: {blockExpPacket.CharacterName} Reason: {blockExpPacket.Reason} Until: {DateTime.Now.AddMinutes(value: blockExpPacket.Duration)}"
                    });

                if (blockExpPacket.Duration == 0) blockExpPacket.Duration = 60;

                blockExpPacket.Reason = blockExpPacket.Reason?.Trim();
                var character = DaoFactory.CharacterDao.LoadByName(name: blockExpPacket.CharacterName);
                if (character != null)
                {
                    var session =
                        ServerManager.Instance.Sessions.FirstOrDefault(predicate: s =>
                            s.Character?.Name == blockExpPacket.CharacterName);
                    session?.SendPacket(packet: blockExpPacket.Duration == 1
                        ? UserInterfaceHelper.GenerateInfo(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTED_SINGULAR"),
                                arg0: blockExpPacket.Reason))
                        : UserInterfaceHelper.GenerateInfo(message: string.Format(
                            format: Language.Instance.GetMessageFromKey(key: "MUTED_PLURAL"),
                            arg0: blockExpPacket.Reason,
                            arg1: blockExpPacket.Duration)));
                    var log = new PenaltyLogDto
                    {
                        AccountId = character.AccountId,
                        Reason = blockExpPacket.Reason,
                        Penalty = PenaltyType.BlockExp,
                        DateStart = DateTime.Now,
                        DateEnd = DateTime.Now.AddMinutes(value: blockExpPacket.Duration),
                        AdminName = Session.Character.Name
                    };
                    Character.InsertOrUpdatePenalty(log: log);
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                            type: 10));
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: BlockExpPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $BlockFExp Command
        /// </summary>
        /// <param name="blockFExpPacket"></param>
        public void BlockFExp(BlockFExpPacket blockFExpPacket)
        {
            if (blockFExpPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[BlockFExp]CharacterName: {blockFExpPacket.CharacterName} Reason: {blockFExpPacket.Reason} Until: {DateTime.Now.AddMinutes(value: blockFExpPacket.Duration)}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[BlockFExp]CharacterName: {blockFExpPacket.CharacterName} Reason: {blockFExpPacket.Reason} Until: {DateTime.Now.AddMinutes(value: blockFExpPacket.Duration)}"
                    });

                if (blockFExpPacket.Duration == 0) blockFExpPacket.Duration = 60;

                blockFExpPacket.Reason = blockFExpPacket.Reason?.Trim();
                var character = DaoFactory.CharacterDao.LoadByName(name: blockFExpPacket.CharacterName);
                if (character != null)
                {
                    var session =
                        ServerManager.Instance.Sessions.FirstOrDefault(predicate: s =>
                            s.Character?.Name == blockFExpPacket.CharacterName);
                    session?.SendPacket(packet: blockFExpPacket.Duration == 1
                        ? UserInterfaceHelper.GenerateInfo(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTED_SINGULAR"),
                                arg0: blockFExpPacket.Reason))
                        : UserInterfaceHelper.GenerateInfo(message: string.Format(
                            format: Language.Instance.GetMessageFromKey(key: "MUTED_PLURAL"),
                            arg0: blockFExpPacket.Reason,
                            arg1: blockFExpPacket.Duration)));
                    var log = new PenaltyLogDto
                    {
                        AccountId = character.AccountId,
                        Reason = blockFExpPacket.Reason,
                        Penalty = PenaltyType.BlockFExp,
                        DateStart = DateTime.Now,
                        DateEnd = DateTime.Now.AddMinutes(value: blockFExpPacket.Duration),
                        AdminName = Session.Character.Name
                    };
                    Character.InsertOrUpdatePenalty(log: log);
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                            type: 10));
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: BlockFExpPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $BlockPM Command
        /// </summary>
        /// <param name="blockPmPacket"></param>
        public void BlockPm(BlockPMPacket blockPmPacket)
        {
            if (blockPmPacket == null) throw new ArgumentNullException(paramName: nameof(blockPmPacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[BlockPM]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[BlockPM]"
                });

            if (!Session.Character.GmPvtBlock)
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(
                    message: Language.Instance.GetMessageFromKey(key: "GM_BLOCK_ENABLE"),
                    type: 10));
                Session.Character.GmPvtBlock = true;
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "GM_BLOCK_DISABLE"), type: 10));
                Session.Character.GmPvtBlock = false;
            }
        }

        /// <summary>
        ///     $BlockRep Command
        /// </summary>
        /// <param name="blockRepPacket"></param>
        public void BlockRep(BlockRepPacket blockRepPacket)
        {
            if (blockRepPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[BlockRep]CharacterName: {blockRepPacket.CharacterName} Reason: {blockRepPacket.Reason} Until: {DateTime.Now.AddMinutes(value: blockRepPacket.Duration)}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[BlockRep]CharacterName: {blockRepPacket.CharacterName} Reason: {blockRepPacket.Reason} Until: {DateTime.Now.AddMinutes(value: blockRepPacket.Duration)}"
                    });

                if (blockRepPacket.Duration == 0) blockRepPacket.Duration = 60;

                blockRepPacket.Reason = blockRepPacket.Reason?.Trim();
                var character = DaoFactory.CharacterDao.LoadByName(name: blockRepPacket.CharacterName);
                if (character != null)
                {
                    var session =
                        ServerManager.Instance.Sessions.FirstOrDefault(predicate: s =>
                            s.Character?.Name == blockRepPacket.CharacterName);
                    session?.SendPacket(packet: blockRepPacket.Duration == 1
                        ? UserInterfaceHelper.GenerateInfo(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTED_SINGULAR"),
                                arg0: blockRepPacket.Reason))
                        : UserInterfaceHelper.GenerateInfo(message: string.Format(
                            format: Language.Instance.GetMessageFromKey(key: "MUTED_PLURAL"),
                            arg0: blockRepPacket.Reason,
                            arg1: blockRepPacket.Duration)));
                    var log = new PenaltyLogDto
                    {
                        AccountId = character.AccountId,
                        Reason = blockRepPacket.Reason,
                        Penalty = PenaltyType.BlockRep,
                        DateStart = DateTime.Now,
                        DateEnd = DateTime.Now.AddMinutes(value: blockRepPacket.Duration),
                        AdminName = Session.Character.Name
                    };
                    Character.InsertOrUpdatePenalty(log: log);
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                            type: 10));
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: BlockRepPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Buff packet
        /// </summary>
        /// <param name="buffPacket"></param>
        public void Buff(BuffPacket buffPacket)
        {
            if (buffPacket != null)
            {
                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Buff]CardId: {buffPacket.CardId} Level: {buffPacket.Level}"
                    });

                var buff = new Buff(id: buffPacket.CardId, level: buffPacket.Level ?? 1);
                Session.Character.AddBuff(indicator: buff, sender: Session.Character.BattleEntity);
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: BuffPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $ChangeClass Command
        /// </summary>
        /// <param name="changeClassPacket"></param>
        public void ChangeClass(ChangeClassPacket changeClassPacket)
        {
            if (changeClassPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[ChangeClass]Class: {changeClassPacket.ClassType}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[ChangeClass]Class: {changeClassPacket.ClassType}"
                    });
                Session.Character.ChangeClass(characterClass: changeClassPacket.ClassType, fromCommand: true);
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ChangeClassPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $ChangeDignity Command
        /// </summary>
        /// <param name="changeDignityPacket"></param>
        public void ChangeDignity(ChangeDignityPacket changeDignityPacket)
        {
            if (changeDignityPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[ChangeDignity]Dignity: {changeDignityPacket.Dignity}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[ChangeDignity]Dignity: {changeDignityPacket.Dignity}"
                    });

                if (changeDignityPacket.Dignity >= -1000 && changeDignityPacket.Dignity <= 100)
                {
                    Session.Character.Dignity = changeDignityPacket.Dignity;
                    Session.SendPacket(packet: Session.Character.GenerateFd());
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "DIGNITY_CHANGED"), type: 12));
                    Session.CurrentMapInstance?.Broadcast(client: Session,
                        content: Session.Character.GenerateIn(InEffect: 1),
                        receiver: ReceiverType.AllExceptMe);
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateGidx(),
                        receiver: ReceiverType.AllExceptMe);
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "BAD_DIGNITY"), type: 11));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ChangeDignityPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $FLvl Command
        /// </summary>
        /// <param name="changeFairyLevelPacket"></param>
        public void ChangeFairyLevel(ChangeFairyLevelPacket changeFairyLevelPacket)
        {
            var fairy =
                Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Fairy,
                    type: InventoryType.Wear);
            if (changeFairyLevelPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[FLvl]FairyLevel: {changeFairyLevelPacket.FairyLevel}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[FLvl]FairyLevel: {changeFairyLevelPacket.FairyLevel}"
                    });

                if (fairy != null)
                {
                    var fairylevel = changeFairyLevelPacket.FairyLevel;
                    fairylevel -= fairy.Item.ElementRate;
                    fairy.ElementRate = fairylevel;
                    fairy.Xp = 0;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "FAIRY_LEVEL_CHANGED"),
                            arg0: fairy.Item.Name),
                        type: 10));
                    Session.SendPacket(packet: Session.Character.GeneratePairy());
                }
                else
                {
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "NO_FAIRY"),
                        type: 10));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: ChangeFairyLevelPacket.ReturnHelp(),
                    type: 10));
            }
        }

        /// <summary>
        ///     $ChangeSex Command
        /// </summary>
        /// <param name="changeSexPacket"></param>
        public void ChangeGender(ChangeSexPacket changeSexPacket)
        {
            if (changeSexPacket == null) throw new ArgumentNullException(paramName: nameof(changeSexPacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[ChangeSex]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[ChangeSex]"
                });

            Session.Character.ChangeSex();
        }

        /// <summary>
        ///     $HeroLvl Command
        /// </summary>
        /// <param name="changeHeroLevelPacket"></param>
        public void ChangeHeroLevel(ChangeHeroLevelPacket changeHeroLevelPacket)
        {
            if (changeHeroLevelPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[HeroLvl]HeroLevel: {changeHeroLevelPacket.HeroLevel}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[HeroLvl]HeroLevel: {changeHeroLevelPacket.HeroLevel}"
                    });

                if (changeHeroLevelPacket.HeroLevel <= 255)
                {
                    Session.Character.HeroLevel = changeHeroLevelPacket.HeroLevel;
                    Session.Character.HeroXp = 0;
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "HEROLEVEL_CHANGED"), type: 0));
                    Session.SendPacket(packet: Session.Character.GenerateLev());
                    Session.SendPackets(packets: Session.Character.GenerateStatChar());
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateIn(),
                        receiver: ReceiverType.AllExceptMe);
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateGidx(),
                        receiver: ReceiverType.AllExceptMe);
                    Session.CurrentMapInstance?.Broadcast(
                        packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: Session.Character.CharacterId, effectId: 6),
                        xRangeCoordinate: Session.Character.PositionX, yRangeCoordinate: Session.Character.PositionY);
                    Session.CurrentMapInstance?.Broadcast(
                        packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: Session.Character.CharacterId, effectId: 198),
                        xRangeCoordinate: Session.Character.PositionX, yRangeCoordinate: Session.Character.PositionY);
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ChangeHeroLevelPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $JLvl Command
        /// </summary>
        /// <param name="changeJobLevelPacket"></param>
        public void ChangeJobLevel(ChangeJobLevelPacket changeJobLevelPacket)
        {
            if (changeJobLevelPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[JLvl]JobLevel: {changeJobLevelPacket.JobLevel}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[JLvl] JobLevel: {changeJobLevelPacket.JobLevel}"
                    });

                if ((Session.Character.Class == 0 && changeJobLevelPacket.JobLevel <= 20
                     || Session.Character.Class != 0 && changeJobLevelPacket.JobLevel <= 255)
                    && changeJobLevelPacket.JobLevel > 0)
                {
                    Session.Character.JobLevel = changeJobLevelPacket.JobLevel;
                    Session.Character.JobLevelXp = 0;
                    Session.Character.ResetSkills();
                    Session.SendPacket(packet: Session.Character.GenerateLev());
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "JOBLEVEL_CHANGED"), type: 0));
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateIn(),
                        receiver: ReceiverType.AllExceptMe);
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateGidx(),
                        receiver: ReceiverType.AllExceptMe);
                    Session.CurrentMapInstance?.Broadcast(
                        packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: Session.Character.CharacterId, effectId: 8),
                        xRangeCoordinate: Session.Character.PositionX, yRangeCoordinate: Session.Character.PositionY);
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ChangeJobLevelPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Lvl Command
        /// </summary>
        /// <param name="changeLevelPacket"></param>
        public void ChangeLevel(ChangeLevelPacket changeLevelPacket)
        {
            if (changeLevelPacket != null && !Session.Character.IsSeal)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Lvl]Level: {changeLevelPacket.Level}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Lvl]Level: {changeLevelPacket.Level}"
                    });

                if (changeLevelPacket.Level > 0)
                {
                    Session.Character.Level = Math.Min(val1: changeLevelPacket.Level,
                        val2: ServerManager.Instance.Configuration.MaxLevel);
                    Session.Character.LevelXp = 0;
                    Session.Character.Hp = (int)Session.Character.HPLoad();
                    Session.Character.Mp = (int)Session.Character.MPLoad();
                    Session.SendPacket(packet: Session.Character.GenerateStat());
                    Session.SendPackets(packets: Session.Character.GenerateStatChar());
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "LEVEL_CHANGED"), type: 0));
                    Session.SendPacket(packet: Session.Character.GenerateLev());
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateIn(),
                        receiver: ReceiverType.AllExceptMe);
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateGidx(),
                        receiver: ReceiverType.AllExceptMe);
                    Session.CurrentMapInstance?.Broadcast(
                        packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: Session.Character.CharacterId, effectId: 6),
                        xRangeCoordinate: Session.Character.PositionX, yRangeCoordinate: Session.Character.PositionY);
                    Session.CurrentMapInstance?.Broadcast(
                        packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: Session.Character.CharacterId, effectId: 198),
                        xRangeCoordinate: Session.Character.PositionX, yRangeCoordinate: Session.Character.PositionY);
                    ServerManager.Instance.UpdateGroup(charId: Session.Character.CharacterId);
                    if (Session.Character.Family != null)
                    {
                        ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId);
                        CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                        {
                            DestinationCharacterId = Session.Character.Family.FamilyId,
                            SourceCharacterId = Session.Character.CharacterId,
                            SourceWorldId = ServerManager.Instance.WorldId,
                            Message = "fhis_stc",
                            Type = MessageType.Family
                        });
                    }

                    Session.Character.LevelRewards(Level: Session.Character.Level);
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ChangeLevelPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $ChangeRep Command
        /// </summary>
        /// <param name="changeReputationPacket"></param>
        public void ChangeReputation(ChangeReputationPacket changeReputationPacket)
        {
            if (changeReputationPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[ChangeRep]Reputation: {changeReputationPacket.Reputation}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[ChangeRep]Reputation: {changeReputationPacket.Reputation}"
                    });

                if (changeReputationPacket.Reputation > 0)
                {
                    Session.Character.Reputation = changeReputationPacket.Reputation;
                    Session.SendPacket(packet: Session.Character.GenerateFd());
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "REP_CHANGED"), type: 0));
                    Session.CurrentMapInstance?.Broadcast(client: Session,
                        content: Session.Character.GenerateIn(InEffect: 1),
                        receiver: ReceiverType.AllExceptMe);
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateGidx(),
                        receiver: ReceiverType.AllExceptMe);
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: ChangeReputationPacket.ReturnHelp(),
                    type: 10));
            }
        }

        /// <summary>
        ///     $SPLvl Command
        /// </summary>
        /// <param name="changeSpecialistLevelPacket"></param>
        public void ChangeSpecialistLevel(ChangeSpecialistLevelPacket changeSpecialistLevelPacket)
        {
            if (changeSpecialistLevelPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[SPLvl]SpecialistLevel: {changeSpecialistLevelPacket.SpecialistLevel}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[SPLvl]SpecialistLevel: {changeSpecialistLevelPacket.SpecialistLevel}"
                    });

                var sp =
                    Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                        type: InventoryType.Wear);
                if (sp != null && Session.Character.UseSp)
                {
                    if (changeSpecialistLevelPacket.SpecialistLevel <= 255
                        && changeSpecialistLevelPacket.SpecialistLevel > 0)
                    {
                        sp.SpLevel = changeSpecialistLevelPacket.SpecialistLevel;
                        sp.Xp = 0;
                        Session.SendPacket(packet: Session.Character.GenerateLev());
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "SPLEVEL_CHANGED"), type: 0));
                        Session.Character.LearnSPSkill();
                        Session.SendPacket(packet: Session.Character.GenerateSki());
                        Session.SendPackets(packets: Session.Character.GenerateQuicklist());
                        Session.Character.SkillsSp.ForEach(action: s => s.LastUse = DateTime.Now.AddDays(value: -1));
                        Session.CurrentMapInstance?.Broadcast(client: Session,
                            content: Session.Character.GenerateIn(InEffect: 1),
                            receiver: ReceiverType.AllExceptMe);
                        Session.CurrentMapInstance?.Broadcast(client: Session,
                            content: Session.Character.GenerateGidx(),
                            receiver: ReceiverType.AllExceptMe);
                        Session.CurrentMapInstance?.Broadcast(
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                callerId: Session.Character.CharacterId, effectId: 8),
                            xRangeCoordinate: Session.Character.PositionX,
                            yRangeCoordinate: Session.Character.PositionY);
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                    }
                }
                else
                {
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "NO_SP"),
                        type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ChangeSpecialistLevelPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $ChannelInfo Command
        /// </summary>
        /// <param name="channelInfoPacket"></param>
        public void ChannelInfo(ChannelInfoPacket channelInfoPacket)
        {
            if (channelInfoPacket == null) throw new ArgumentNullException(paramName: nameof(channelInfoPacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[ChannelInfo]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[ChannelInfo]"
                });

            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"-----------Channel Info-----------\n-------------Channel:{ServerManager.Instance.ChannelId}-------------",
                type: 11));
            foreach (var session in ServerManager.Instance.Sessions)
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(
                        message:
                        $"CharacterName: {session.Character.Name} | CharacterId: {session.Character.CharacterId} | SessionId: {session.SessionId}",
                        type: 12));

            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: "----------------------------------------", type: 11));
        }

        /// <summary>
        ///     $ServerInfo Command
        /// </summary>
        /// <param name="serverInfoPacket"></param>
        public void ServerInfo(ServerInfoPacket serverInfoPacket)
        {
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[ServerInfo]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = $"[ServerInfo] ChannelId: {serverInfoPacket.ChannelId ?? 0}"
                });

            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: "------------Server Info------------", type: 11));

            long ActualChannelId = 0;

            CommunicationServiceClient.Instance.GetOnlineCharacters()
                .Where(predicate: s => serverInfoPacket.ChannelId == null || s[1] == serverInfoPacket.ChannelId)
                .OrderBy(keySelector: s => s[1])
                .ToList().ForEach(action: s =>
                {
                    if (s[1] > ActualChannelId)
                    {
                        if (ActualChannelId > 0)
                            Session.SendPacket(packet: Session.Character.GenerateSay(
                                message: "----------------------------------------",
                                type: 11));
                        ActualChannelId = s[1];
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: $"-------------Channel:{ActualChannelId}-------------", type: 11));
                    }

                    var Character = DaoFactory.CharacterDao.LoadById(characterId: s[0]);
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message:
                            $"CharacterName: {Character.Name} | CharacterId: {Character.CharacterId} | SessionId: {s[2]}",
                            type: 12));
                });

            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: "----------------------------------------", type: 11));
        }

        /// <summary>
        ///     $CharEdit Command
        /// </summary>
        /// <param name="characterEditPacket"></param>
        public void CharacterEdit(CharacterEditPacket characterEditPacket)
        {
            if (characterEditPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[CharEdit]Property: {characterEditPacket.Property} Value: {characterEditPacket.Data}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[CharEdit]Property: {characterEditPacket.Property} Value: {characterEditPacket.Data}"
                    });

                if (characterEditPacket.Property != null && !string.IsNullOrEmpty(value: characterEditPacket.Data))
                {
                    var propertyInfo = Session.Character.GetType().GetProperty(name: characterEditPacket.Property);
                    if (propertyInfo != null)
                    {
                        propertyInfo.SetValue(obj: Session.Character,
                            value: Convert.ChangeType(value: characterEditPacket.Data,
                                conversionType: propertyInfo.PropertyType));
                        ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId);
                        Session.Character.Save();
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "DONE"),
                            type: 10));
                    }
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: CharacterEditPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $CharStat Command
        /// </summary>
        /// <param name="characterStatsPacket"></param>
        public void CharStat(CharacterStatsPacket characterStatsPacket)
        {
            var returnHelp = CharacterStatsPacket.ReturnHelp();
            if (characterStatsPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[CharStat]CharacterName: {characterStatsPacket.CharacterName}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[CharStat]CharacterName: {characterStatsPacket.CharacterName}"
                    });

                var name = characterStatsPacket.CharacterName;
                if (!string.IsNullOrWhiteSpace(value: name))
                {
                    if (ServerManager.Instance.GetSessionByCharacterName(name: name) != null)
                    {
                        var character = ServerManager.Instance.GetSessionByCharacterName(name: name).Character;
                        SendStats(characterDto: character);
                    }
                    else if (DaoFactory.CharacterDao.LoadByName(name: name) != null)
                    {
                        var characterDto = DaoFactory.CharacterDao.LoadByName(name: name);
                        SendStats(characterDto: characterDto);
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                    }
                }
                else
                {
                    Session.SendPacket(packet: Session.Character.GenerateSay(message: returnHelp, type: 10));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: returnHelp, type: 10));
            }
        }

        /// <summary>
        ///     $Clear Command
        /// </summary>
        /// <param name="clearInventoryPacket"></param>
        public void ClearInventory(ClearInventoryPacket clearInventoryPacket)
        {
            if (clearInventoryPacket != null && clearInventoryPacket.InventoryType != InventoryType.Wear)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Clear]InventoryType: {clearInventoryPacket.InventoryType}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Clear]InventoryType: {clearInventoryPacket.InventoryType}"
                    });

                Parallel.ForEach(
                    source: Session.Character.Inventory.Where(predicate: s =>
                        s.Type == clearInventoryPacket.InventoryType),
                    body: inv =>
                    {
                        Session.Character.Inventory.DeleteById(id: inv.Id);
                        Session.SendPacket(
                            packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: inv.Type,
                                slot: inv.Slot));
                    });
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ClearInventoryPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $ClearMap packet
        /// </summary>
        /// <param name="clearMapPacket"></param>
        public void ClearMap(ClearMapPacket clearMapPacket)
        {
            if (clearMapPacket != null && Session.HasCurrentMapInstance)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[ClearMap]MapId: {Session.CurrentMapInstance.MapInstanceId}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[ClearMap]MapId: {Session.CurrentMapInstance.MapInstanceId}"
                    });

                Parallel.ForEach(
                    source: Session.CurrentMapInstance.Monsters.Where(predicate: s => s.ShouldRespawn != true),
                    body: monster =>
                    {
                        Session.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                            callerId: monster.MapMonsterId));
                        monster.SetDeathStatement();
                        Session.CurrentMapInstance.RemoveMonster(monsterToRemove: monster);
                    });
                Parallel.ForEach(source: Session.CurrentMapInstance.DroppedList.GetAllItems(), body: drop =>
                {
                    Session.CurrentMapInstance.Broadcast(
                        packet: StaticPacketHelper.Out(type: UserType.Object, callerId: drop.TransportId));
                    Session.CurrentMapInstance.DroppedList.Remove(key: drop.TransportId);
                });
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ClearMapPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Clone Command
        /// </summary>
        /// <param name="cloneItemPacket"></param>
        public void CloneItem(CloneItemPacket cloneItemPacket)
        {
            if (cloneItemPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Clone]Slot: {cloneItemPacket.Slot}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Clone]Slot: {cloneItemPacket.Slot}"
                    });

                var item =
                    Session.Character.Inventory.LoadBySlotAndType(slot: cloneItemPacket.Slot,
                        type: InventoryType.Equipment);
                if (item != null)
                {
                    item = item.DeepCopy();
                    item.Id = Guid.NewGuid();
                    Session.Character.Inventory.AddToInventory(newItem: item);
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: CloneItemPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Help Command
        /// </summary>
        /// <param name="helpPacket"></param>
        public void Command(HelpPacket helpPacket)
        {
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[Help]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[Help]"
                });

            // get commands
            var classes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(selector: t => t.GetTypes()).Where(
                predicate: t =>
                    t.IsClass && t.Namespace == "OpenNos.GameObject.CommandPackets"
                              && (((PacketHeaderAttribute)Array.Find(array: t.GetCustomAttributes(inherit: true),
                                  match: ca => ca.GetType().Equals(o: typeof(PacketHeaderAttribute))))?.Authorities)
                              .Any(predicate: c => Session.Account.Authority.Equals(obj: c)
                                                   || Session.Account.Authority.Equals(obj: AuthorityType.Administrator)
                                                   || c.Equals(obj: AuthorityType.User) &&
                                                   Session.Account.Authority >= AuthorityType.User
                                                   || c.Equals(obj: AuthorityType.Mod) &&
                                                   Session.Account.Authority >= AuthorityType.Mod &&
                                                   Session.Account.Authority <= AuthorityType.Gm
                                                   || c.Equals(obj: AuthorityType.Gm) &&
                                                   Session.Account.Authority >= AuthorityType.Gm
                                                   || c.Equals(obj: AuthorityType.Administrator) &&
                                                   Session.Account.Authority >= AuthorityType.Administrator
                              )).ToList();
            var messages = new List<string>();
            foreach (var type in classes)
            {
                var classInstance = Activator.CreateInstance(type: type);
                var classType = classInstance.GetType();
                var method = classType.GetMethod(name: "ReturnHelp");
                if (method != null) messages.Add(item: method.Invoke(obj: classInstance, parameters: null).ToString());
            }

            // send messages
            messages.Sort();
            if (helpPacket.Contents == "*" || string.IsNullOrEmpty(value: helpPacket.Contents))
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: "-------------Commands Info-------------",
                        type: 11));
                foreach (var message in messages)
                    Session.SendPacket(packet: Session.Character.GenerateSay(message: message, type: 12));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: "-------------Command Info-------------", type: 11));
                foreach (var message in messages.Where(predicate: s =>
                    s.IndexOf(value: helpPacket.Contents, comparisonType: StringComparison.OrdinalIgnoreCase) >= 0))
                    Session.SendPacket(packet: Session.Character.GenerateSay(message: message, type: 12));
            }

            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: "-----------------------------------------------",
                    type: 11));
        }

        /// <summary>
        ///     $CreateItem Packet
        /// </summary>
        /// <param name="createItemPacket"></param>
        public void CreateItem(CreateItemPacket createItemPacket)
        {
            if (createItemPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[CreateItem]ItemVNum: {createItemPacket.VNum} Amount/Design: {createItemPacket.Design} Upgrade: {createItemPacket.Upgrade}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[CreateItem]ItemVNum: {createItemPacket.VNum} Amount/Design: {createItemPacket.Design} Upgrade: {createItemPacket.Upgrade}"
                    });

                var vnum = createItemPacket.VNum;
                short amount = 1;
                sbyte rare = 0;
                byte upgrade = 0, design = 0;
                if (vnum == 1046) return; // cannot create gold as item, use $Gold instead

                var iteminfo = ServerManager.GetItem(vnum: vnum);
                if (iteminfo != null)
                {
                    if (iteminfo.IsColored || iteminfo.ItemType == ItemType.Box && iteminfo.ItemSubType == 3)
                    {
                        if (createItemPacket.Design.HasValue)
                        {
                            rare = (sbyte)ServerManager.RandomNumber();
                            if (rare > 90)
                                rare = 7;
                            else if (rare > 80)
                                rare = 6;
                            else
                                rare = (sbyte)ServerManager.RandomNumber(min: 1, max: 6);
                            design = (byte)createItemPacket.Design.Value;
                        }
                    }
                    else if (iteminfo.Type == 0)
                    {
                        if (createItemPacket.Upgrade.HasValue)
                        {
                            if (iteminfo.EquipmentSlot != EquipmentType.Sp)
                                upgrade = createItemPacket.Upgrade.Value;
                            else
                                design = createItemPacket.Upgrade.Value;

                            if (iteminfo.EquipmentSlot != EquipmentType.Sp && upgrade == 0
                                                                           && iteminfo.BasicUpgrade != 0)
                                upgrade = iteminfo.BasicUpgrade;
                        }

                        if (createItemPacket.Design.HasValue)
                        {
                            if (iteminfo.EquipmentSlot == EquipmentType.Sp)
                                upgrade = (byte)createItemPacket.Design.Value;
                            else
                                rare = (sbyte)createItemPacket.Design.Value;
                        }
                    }

                    if (createItemPacket.Design.HasValue && !createItemPacket.Upgrade.HasValue)
                        amount = createItemPacket.Design.Value > 999 ? (short)999 : createItemPacket.Design.Value;

                    var inv = Session.Character.Inventory
                        .AddNewToInventory(vnum: vnum, amount: amount, Rare: rare, Upgrade: upgrade, Design: design)
                        .FirstOrDefault();
                    if (inv != null)
                    {
                        var wearable = Session.Character.Inventory.LoadBySlotAndType(slot: inv.Slot, type: inv.Type);
                        if (wearable != null)
                            switch (wearable.Item.EquipmentSlot)
                            {
                                case EquipmentType.Armor:
                                case EquipmentType.MainWeapon:
                                case EquipmentType.SecondaryWeapon:
                                    wearable.SetRarityPoint();
                                    break;

                                case EquipmentType.Boots:
                                case EquipmentType.Gloves:
                                    wearable.FireResistance = (short)(wearable.Item.FireResistance * upgrade);
                                    wearable.DarkResistance = (short)(wearable.Item.DarkResistance * upgrade);
                                    wearable.LightResistance = (short)(wearable.Item.LightResistance * upgrade);
                                    wearable.WaterResistance = (short)(wearable.Item.WaterResistance * upgrade);
                                    break;
                            }

                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message:
                            $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {iteminfo.Name} x {amount}",
                            type: 12));
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                type: 0));
                    }
                }
                else
                {
                    UserInterfaceHelper.GenerateMsg(message: Language.Instance.GetMessageFromKey(key: "NO_ITEM"),
                        type: 0);
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: CreateItemPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Demote Command
        /// </summary>
        /// <param name="demotePacket"></param>
        public void Demote(DemotePacket demotePacket)
        {
            if (demotePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Demote]CharacterName: {demotePacket.CharacterName}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Demote]CharacterName: {demotePacket.CharacterName}"
                    });

                var name = demotePacket.CharacterName;
                try
                {
                    var account =
                        DaoFactory.AccountDao.LoadById(accountId: DaoFactory.CharacterDao.LoadByName(name: name)
                            .AccountId);
                    if (account?.Authority > AuthorityType.User)
                    {
                        if (Session.Account.Authority >= account?.Authority)
                        {
                            var newAuthority = AuthorityType.User; // AUTHORITY
                            if (account.Authority == AuthorityType.Administrator)
                                newAuthority = AuthorityType.Em;
                            else if (account.Authority == AuthorityType.Em)
                                newAuthority = AuthorityType.Gm;
                            else if (account.Authority == AuthorityType.Gm)
                                newAuthority = AuthorityType.Mod;
                            else if (account.Authority == AuthorityType.Mod)
                                newAuthority = AuthorityType.Gs;
                            else if (account.Authority == AuthorityType.Gs)
                                newAuthority = AuthorityType.User;
                            else
                                newAuthority = AuthorityType.User;

                            account.Authority = newAuthority;
                            DaoFactory.AccountDao.InsertOrUpdate(account: ref account);
                            var session =
                                ServerManager.Instance.Sessions.FirstOrDefault(
                                    predicate: s => s.Character?.Name == name);
                            if (session != null)
                            {
                                session.Account.Authority = newAuthority;
                                session.Character.Authority = newAuthority;
                                if (session.Character.InvisibleGm)
                                {
                                    session.Character.Invisible = false;
                                    session.Character.InvisibleGm = false;
                                    Session.Character.Mates.Where(predicate: m => m.IsTeamMember).ToList().ForEach(
                                        action: m =>
                                            Session.CurrentMapInstance?.Broadcast(packet: m.GenerateIn(),
                                                receiver: ReceiverType.AllExceptMe));
                                    Session.CurrentMapInstance?.Broadcast(client: Session,
                                        content: Session.Character.GenerateIn(),
                                        receiver: ReceiverType.AllExceptMe);
                                    Session.CurrentMapInstance?.Broadcast(client: Session,
                                        content: Session.Character.GenerateGidx(),
                                        receiver: ReceiverType.AllExceptMe);
                                }

                                ServerManager.Instance.ChangeMap(id: session.Character.CharacterId);
                                DaoFactory.AccountDao.WriteGeneralLog(accountId: session.Account.AccountId,
                                    ipAddress: session.IpAddress,
                                    characterId: session.Character.CharacterId, logType: GeneralLogType.Demotion,
                                    logData: $"by: {Session.Character.Name}");
                            }
                            else
                            {
                                DaoFactory.AccountDao.WriteGeneralLog(accountId: account.AccountId,
                                    ipAddress: "127.0.0.1", characterId: null,
                                    logType: GeneralLogType.Demotion, logData: $"by: {Session.Character.Name}");
                            }

                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "DONE"), type: 10));
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "CANT_DO_THAT"), type: 10));
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                    }
                }
                catch
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: DemotePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $DropRate Command
        /// </summary>
        /// <param name="dropRatePacket"></param>
        public void DropRate(DropRatePacket dropRatePacket)
        {
            if (dropRatePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[DropRate]Value: {dropRatePacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[DropRate]Value: {dropRatePacket.Value}"
                    });

                if (dropRatePacket.Value <= 1000)
                {
                    ServerManager.Instance.Configuration.RateDrop = dropRatePacket.Value;
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "DROP_RATE_CHANGED"), type: 0));
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: DropRatePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Effect Command
        /// </summary>
        /// <param name="effectCommandpacket"></param>
        public void Effect(EffectCommandPacket effectCommandpacket)
        {
            if (effectCommandpacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Effect]EffectId: {effectCommandpacket.EffectId}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Effect]EffectId: {effectCommandpacket.EffectId}"
                    });

                Session.CurrentMapInstance?.Broadcast(
                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                        callerId: Session.Character.CharacterId,
                        effectId: effectCommandpacket.EffectId), xRangeCoordinate: Session.Character.PositionX,
                    yRangeCoordinate: Session.Character.PositionY);
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: EffectCommandPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Faction Command
        /// </summary>
        /// <param name="factionPacket"></param>
        public void Faction(FactionPacket factionPacket)
        {
            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[Faction]"
                });

            if (ServerManager.Instance.ChannelId == 51)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "CHANGE_NOT_PERMITTED_ACT4"),
                        type: 0));
                return;
            }

            if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.Act4ShipAngel
                || Session.CurrentMapInstance.MapInstanceType == MapInstanceType.Act4ShipDemon)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "CHANGE_NOT_PERMITTED_ACT4SHIP"),
                        type: 0));
                return;
            }

            if (factionPacket != null)
            {
                Session.SendPacket(packet: "scr 0 0 0 0 0 0 0");
                if (Session.Character.Faction == FactionType.Angel)
                {
                    Session.Character.Faction = FactionType.Demon;
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "GET_PROTECTION_POWER_2"),
                            type: 0));
                }
                else
                {
                    Session.Character.Faction = FactionType.Angel;
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "GET_PROTECTION_POWER_1"),
                            type: 0));
                }

                Session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                    callerId: Session.Character.CharacterId, effectId: 4799 + (byte)Session.Character.Faction));
                Session.SendPacket(packet: Session.Character.GenerateFaction());
                if (ServerManager.Instance.ChannelId == 51) Session.SendPacket(packet: Session.Character.GenerateFc());
            }
        }

        /// <summary>
        ///     $FamilyFaction Command
        /// </summary>
        /// <param name="familyFactionPacket"></param>
        public void FamilyFaction(FamilyFactionPacket familyFactionPacket)
        {
            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[FamilyFaction]"
                });

            if (ServerManager.Instance.ChannelId == 51)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "CHANGE_NOT_PERMITTED_ACT4"),
                        type: 0));
                return;
            }

            if (Session.CurrentMapInstance.MapInstanceType == MapInstanceType.Act4ShipAngel
                || Session.CurrentMapInstance.MapInstanceType == MapInstanceType.Act4ShipDemon)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "CHANGE_NOT_PERMITTED_ACT4SHIP"),
                        type: 0));
                return;
            }

            if (familyFactionPacket != null)
            {
                if (string.IsNullOrEmpty(value: familyFactionPacket.FamilyName) && Session.Character.Family != null)
                {
                    Session.Character.Family.ChangeFaction(
                        faction: Session.Character.Family.FamilyFaction == 1 ? (byte)2 : (byte)1, session: Session);
                    return;
                }

                var family =
                    ServerManager.Instance.FamilyList.FirstOrDefault(predicate: s =>
                        s.Name == familyFactionPacket.FamilyName);
                if (family != null)
                    family.ChangeFaction(faction: family.FamilyFaction == 1 ? (byte)2 : (byte)1, session: Session);
                else
                    Session.SendPacket(packet: Session.Character.GenerateSay(message: "Family not found.", type: 10));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: FamilyFactionPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $FairyXPRate Command
        /// </summary>
        /// <param name="fairyXpRatePacket"></param>
        public void FairyXpRate(FairyXpRatePacket fairyXpRatePacket)
        {
            if (fairyXpRatePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[FairyXPRate]Value: {fairyXpRatePacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[FairyXPRate]Value: {fairyXpRatePacket.Value}"
                    });

                if (fairyXpRatePacket.Value <= 1000)
                {
                    ServerManager.Instance.Configuration.RateFairyXp = fairyXpRatePacket.Value;
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "FAIRYXP_RATE_CHANGED"),
                            type: 0));
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: FairyXpRatePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Gift Command
        /// </summary>
        /// <param name="giftPacket"></param>
        public void Gift(GiftPacket giftPacket)
        {
            if (giftPacket != null)
            {
                var Amount = giftPacket.Amount;

                if (Amount <= 0) Amount = 1;

                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Gift]CharacterName: {giftPacket.CharacterName} ItemVNum: {giftPacket.VNum} Amount: {Amount} Rare: {giftPacket.Rare} Upgrade: {giftPacket.Upgrade}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Gift]CharacterName: {giftPacket.CharacterName} ItemVNum: {giftPacket.VNum} Amount: {Amount} Rare: {giftPacket.Rare} Upgrade: {giftPacket.Upgrade}"
                    });

                if (giftPacket.CharacterName == "*")
                {
                    if (Session.HasCurrentMapInstance)
                    {
                        Parallel.ForEach(source: Session.CurrentMapInstance.Sessions,
                            body: session => Session.Character.SendGift(id: session.Character.CharacterId,
                                vnum: giftPacket.VNum,
                                amount: Amount, rare: giftPacket.Rare, upgrade: giftPacket.Upgrade,
                                design: giftPacket.Design, isNosmall: false));
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "GIFT_SENT"), type: 10));
                    }
                }
                else if (giftPacket.CharacterName == "ALL")
                {
                    int levelMin = giftPacket.ReceiverLevelMin;
                    var levelMax = giftPacket.ReceiverLevelMax == 0 ? 99 : giftPacket.ReceiverLevelMax;

                    DaoFactory.CharacterDao.LoadAll().ToList().ForEach(action: chara =>
                    {
                        if (chara.Level >= levelMin && chara.Level <= levelMax)
                            Session.Character.SendGift(id: chara.CharacterId, vnum: giftPacket.VNum, amount: Amount,
                                rare: giftPacket.Rare, upgrade: giftPacket.Upgrade, design: giftPacket.Design,
                                isNosmall: false);
                    });
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "GIFT_SENT"), type: 10));
                }
                else
                {
                    var chara = DaoFactory.CharacterDao.LoadByName(name: giftPacket.CharacterName);
                    if (chara != null)
                    {
                        Session.Character.SendGift(id: chara.CharacterId, vnum: giftPacket.VNum, amount: Amount,
                            rare: giftPacket.Rare, upgrade: giftPacket.Upgrade, design: giftPacket.Design,
                            isNosmall: false);
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "GIFT_SENT"), type: 10));
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "USER_NOT_CONNECTED"),
                                type: 0));
                    }
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: GiftPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $GodMode Command
        /// </summary>
        /// <param name="godModePacket"></param>
        public void GodMode(GodModePacket godModePacket)
        {
            if (godModePacket == null) throw new ArgumentNullException(paramName: nameof(godModePacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[GodMode]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[GodMode]"
                });

            Session.Character.HasGodMode = !Session.Character.HasGodMode;
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                    type: 10));
        }

        /// <summary>
        ///     $Gold Command
        /// </summary>
        /// <param name="goldPacket"></param>
        public void Gold(GoldPacket goldPacket)
        {
            if (goldPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Gold]Amount: {goldPacket.Amount}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Gold]Amount: {goldPacket.Amount}"
                    });

                var gold = goldPacket.Amount;
                var maxGold = ServerManager.Instance.Configuration.MaxGold;
                gold = gold > maxGold ? maxGold : gold;
                if (gold >= 0)
                {
                    Session.Character.Gold = gold;
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "GOLD_SET"),
                        type: 0));
                    Session.SendPacket(packet: Session.Character.GenerateGold());
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: GoldPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $GoldDropRate Command
        /// </summary>
        /// <param name="goldDropRatePacket"></param>
        public void GoldDropRate(GoldDropRatePacket goldDropRatePacket)
        {
            if (goldDropRatePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[GoldDropRate]Value: {goldDropRatePacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[GoldDropRate]Value: {goldDropRatePacket.Value}"
                    });

                if (goldDropRatePacket.Value <= 1000)
                {
                    ServerManager.Instance.Configuration.RateGoldDrop = goldDropRatePacket.Value;
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "GOLD_DROP_RATE_CHANGED"),
                            type: 0));
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: GoldDropRatePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $GoldRate Command
        /// </summary>
        /// <param name="goldRatePacket"></param>
        public void GoldRate(GoldRatePacket goldRatePacket)
        {
            if (goldRatePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[GoldRate]Value: {goldRatePacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[GoldRate]Value: {goldRatePacket.Value}"
                    });

                if (goldRatePacket.Value <= 1000)
                {
                    ServerManager.Instance.Configuration.RateGold = goldRatePacket.Value;

                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "GOLD_RATE_CHANGED"), type: 0));
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: GoldRatePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $ReputationRate Command
        /// </summary>
        /// <param name="reputationRatePacket"></param>
        public void ReputationRate(ReputationRatePacket reputationRatePacket)
        {
            if (reputationRatePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[ReputationRate]Value: {reputationRatePacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[ReputationRate]Value: {reputationRatePacket.Value}"
                    });

                if (reputationRatePacket.Value <= 1000)
                {
                    ServerManager.Instance.Configuration.RateReputation = reputationRatePacket.Value;

                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "REPUTATION_RATE_CHANGED"),
                            type: 0));
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: GoldRatePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Guri Command
        /// </summary>
        /// <param name="guriCommandPacket"></param>
        public void Guri(GuriCommandPacket guriCommandPacket)
        {
            if (guriCommandPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Guri]Type: {guriCommandPacket.Type} Value: {guriCommandPacket.Value} Arguments: {guriCommandPacket.Argument}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Guri]Type: {guriCommandPacket.Type} Value: {guriCommandPacket.Value} Arguments: {guriCommandPacket.Argument}"
                    });

                Session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: guriCommandPacket.Type,
                    argument: guriCommandPacket.Argument,
                    callerId: Session.Character.CharacterId, value: guriCommandPacket.Value));
            }

            Session.Character.GenerateSay(message: GuriCommandPacket.ReturnHelp(), type: 10);
        }

        /// <summary>
        ///     $HairColor Command
        /// </summary>
        /// <param name="hairColorPacket"></param>
        public void Haircolor(HairColorPacket hairColorPacket)
        {
            if (hairColorPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[HairColor]HairColor: {hairColorPacket.HairColor}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[HairColor]HairColor: {hairColorPacket.HairColor}"
                    });

                Session.Character.HairColor = hairColorPacket.HairColor;
                Session.SendPacket(packet: Session.Character.GenerateEq());
                Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateIn());
                Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateGidx());
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: HairColorPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $HairStyle Command
        /// </summary>
        /// <param name="hairStylePacket"></param>
        public void Hairstyle(HairStylePacket hairStylePacket)
        {
            if (hairStylePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[HairStyle]HairStyle: {hairStylePacket.HairStyle}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[HairStyle]HairStyle: {hairStylePacket.HairStyle}"
                    });

                Session.Character.HairStyle = hairStylePacket.HairStyle;
                Session.SendPacket(packet: Session.Character.GenerateEq());
                Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateIn());
                Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateGidx());
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: HairStylePacket.ReturnHelp(), type: 10));
            }
        }

        public void HelpMe(HelpMePacket packet)
        {
            if (packet != null && !string.IsNullOrWhiteSpace(value: packet.Message))
            {
                var count = 0;
                foreach (var team in ServerManager.Instance.Sessions.Where(predicate: s =>
                    s.Account.Authority >= AuthorityType.Gm))
                    if (team.HasSelectedCharacter)
                    {
                        count++;

                        // TODO: move that to resx soo we follow i18n
                        team.SendPacket(packet: team.Character.GenerateSay(
                            message: $"User {Session.Character.Name} needs your help!",
                            type: 12));
                        team.SendPacket(
                            packet: team.Character.GenerateSay(message: $"Reason: {packet.Message}", type: 12));
                        team.SendPacket(
                            packet: team.Character.GenerateSay(
                                message: "Please inform the family chat when you take care of!", type: 12));
                        team.SendPacket(
                            packet: Session.Character.GenerateSpk(message: "Click this message to start chatting.",
                                type: 5));
                        team.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: $"User {Session.Character.Name} needs your help!", type: 0));
                    }

                if (count != 0)
                {
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: $"{count} Team members were informed! You should get a message shortly.", type: 10));
                }
                else
                {
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message:
                        "Sadly, there are no online team member right now. Please ask for help on our Discord Server at:",
                        type: 10));
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: "https://discord.gg/aujJkHN", type: 10));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: HelpMePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $HeroXPRate Command
        /// </summary>
        /// <param name="heroXpRatePacket"></param>
        public void HeroXpRate(HeroXpRatePacket heroXpRatePacket)
        {
            if (heroXpRatePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[HeroXPRate]Value: {heroXpRatePacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[HeroXPRate]Value: {heroXpRatePacket.Value}"
                    });

                if (heroXpRatePacket.Value <= 1000)
                {
                    ServerManager.Instance.Configuration.RateHeroicXp = heroXpRatePacket.Value;
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "HEROXP_RATE_CHANGED"), type: 0));
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: HeroXpRatePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Invisible Command
        /// </summary>
        /// <param name="invisiblePacket"></param>
        public void Invisible(InvisiblePacket invisiblePacket)
        {
            if (invisiblePacket == null) throw new ArgumentNullException(paramName: nameof(invisiblePacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[Invisible]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[Invisible]"
                });

            Session.Character.Invisible = !Session.Character.Invisible;
            Session.Character.InvisibleGm = !Session.Character.InvisibleGm;
            Session.SendPacket(packet: Session.Character.GenerateInvisible());
            Session.SendPacket(packet: Session.Character.GenerateEq());
            Session.SendPacket(packet: Session.Character.GenerateCMode());

            if (Session.Character.InvisibleGm)
            {
                Session.Character.Mates.Where(predicate: s => s.IsTeamMember).ToList()
                    .ForEach(action: s => Session.CurrentMapInstance?.Broadcast(packet: s.GenerateOut()));
                Session.CurrentMapInstance?.Broadcast(client: Session,
                    content: StaticPacketHelper.Out(type: UserType.Player, callerId: Session.Character.CharacterId),
                    receiver: ReceiverType.AllExceptMe);
            }
            else
            {
                foreach (var teamMate in Session.Character.Mates.Where(predicate: m => m.IsTeamMember))
                {
                    teamMate.PositionX = Session.Character.PositionX;
                    teamMate.PositionY = Session.Character.PositionY;
                    teamMate.UpdateBushFire();
                    Parallel.ForEach(
                        source: Session.CurrentMapInstance.Sessions.Where(predicate: s => s.Character != null),
                        body: s =>
                        {
                            if (ServerManager.Instance.ChannelId != 51 ||
                                Session.Character.Faction == s.Character.Faction)
                                s.SendPacket(packet: teamMate.GenerateIn(hideNickname: false,
                                    isAct4: ServerManager.Instance.ChannelId == 51));
                            else
                                s.SendPacket(packet: teamMate.GenerateIn(hideNickname: true,
                                    isAct4: ServerManager.Instance.ChannelId == 51,
                                    receiverAuthority: s.Account.Authority));
                        });
                    Session.SendPacket(packet: Session.Character.GeneratePinit());
                    Session.Character.Mates.ForEach(action: s => Session.SendPacket(packet: s.GenerateScPacket()));
                    Session.SendPackets(packets: Session.Character.GeneratePst());
                }

                Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateIn(),
                    receiver: ReceiverType.AllExceptMe);
                Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateGidx(),
                    receiver: ReceiverType.AllExceptMe);
            }
        }

        /// <summary>
        ///     $ItemRain Command
        /// </summary>
        /// <param name="itemRainPacket"></param>
        public void ItemRain(ItemRainPacket itemRainPacket)
        {
            if (itemRainPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[ItemRain]ItemVNum: {itemRainPacket.VNum} Amount: {itemRainPacket.Amount} Count: {itemRainPacket.Count} Time: {itemRainPacket.Time}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[ItemRain]ItemVNum: {itemRainPacket.VNum} Amount: {itemRainPacket.Amount} Count: {itemRainPacket.Count} Time: {itemRainPacket.Time}"
                    });

                var vnum = itemRainPacket.VNum;
                var amount = itemRainPacket.Amount;
                if (amount > 999) amount = 999;
                var count = itemRainPacket.Count;
                var time = itemRainPacket.Time;

                var instance = Session.CurrentMapInstance;

                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 0)).Subscribe(onNext: observer =>
                {
                    for (var i = 0; i < count; i++)
                    {
                        var cell = instance.Map.GetRandomPosition();
                        var droppedItem = new MonsterMapItem(x: cell.X, y: cell.Y, itemVNum: vnum, amount: amount);
                        instance.DroppedList[key: droppedItem.TransportId] = droppedItem;
                        instance.Broadcast(
                            packet:
                            $"drop {droppedItem.ItemVNum} {droppedItem.TransportId} {droppedItem.PositionX} {droppedItem.PositionY} {(droppedItem.GoldAmount > 1 ? droppedItem.GoldAmount : droppedItem.Amount)} 0 -1");

                        Thread.Sleep(millisecondsTimeout: time * 1000 / count);
                    }
                });
            }
        }

        /// <summary>
        ///     $Kick Command
        /// </summary>
        /// <param name="kickPacket"></param>
        public void Kick(KickPacket kickPacket)
        {
            if (kickPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Kick]CharacterName: {kickPacket.CharacterName}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Kick]CharacterName: {kickPacket.CharacterName}"
                    });

                if (kickPacket.CharacterName == "*")
                    Parallel.ForEach(source: ServerManager.Instance.Sessions, body: session => session.Disconnect());

                ServerManager.Instance.Kick(characterName: kickPacket.CharacterName);
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: KickPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $KickSession Command
        /// </summary>
        /// <param name="kickSessionPacket"></param>
        public void KickSession(KickSessionPacket kickSessionPacket)
        {
            if (kickSessionPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Kick]AccountName: {kickSessionPacket.AccountName} SessionId: {kickSessionPacket.SessionId}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Kick]AccountName: {kickSessionPacket.AccountName} SessionId: {kickSessionPacket.SessionId}"
                    });

                if (kickSessionPacket.SessionId.HasValue) //if you set the sessionId, remove account verification
                    kickSessionPacket.AccountName = "";

                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
                var account = DaoFactory.AccountDao.LoadByName(name: kickSessionPacket.AccountName);
                CommunicationServiceClient.Instance.KickSession(accountId: account?.AccountId,
                    sessionId: kickSessionPacket.SessionId);
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: KickSessionPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Kill Command
        /// </summary>
        /// <param name="killPacket"></param>
        public void Kill(KillPacket killPacket)
        {
            if (killPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Kill]CharacterName: {killPacket.CharacterName}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Kill]CharacterName: {killPacket.CharacterName}"
                    });

                var sess = ServerManager.Instance.GetSessionByCharacterName(name: killPacket.CharacterName);
                if (sess != null)
                {
                    if (sess.Character.HasGodMode) return;

                    if (sess.Character.Hp < 1) return;

                    sess.Character.Hp = 0;
                    sess.Character.LastDefence = DateTime.Now;
                    Session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(type: UserType.Player,
                        callerId: Session.Character.CharacterId, secondaryType: 1, targetId: sess.Character.CharacterId,
                        skillVNum: 1114, cooldown: 4, attackAnimation: 11, skillEffect: 4260, x: 0, y: 0,
                        isAlive: false, health: 0,
                        damage: 60000, hitmode: 3, skillType: 0));
                    sess.SendPacket(packet: sess.Character.GenerateStat());
                    if (sess.Character.IsVehicled) sess.Character.RemoveVehicle();
                    ServerManager.Instance.AskRevive(characterId: sess.Character.CharacterId);
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                            type: 10));
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_CONNECTED"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: KillPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $PenaltyLog Command
        /// </summary>
        /// <param name="penaltyLogPacket"></param>
        public void ListPenalties(PenaltyLogPacket penaltyLogPacket)
        {
            var returnHelp = CharacterStatsPacket.ReturnHelp();
            if (penaltyLogPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[PenaltyLog]CharacterName: {penaltyLogPacket.CharacterName}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[PenaltyLog]CharacterName: {penaltyLogPacket.CharacterName}"
                    });

                var name = penaltyLogPacket.CharacterName;
                if (!string.IsNullOrEmpty(value: name))
                {
                    var character = DaoFactory.CharacterDao.LoadByName(name: name);
                    if (character != null)
                    {
                        var separatorSent = false;

                        void WritePenalty(PenaltyLogDto penalty)
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(message: $"Type: {penalty.Penalty}", type: 13));
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(message: $"AdminName: {penalty.AdminName}",
                                    type: 13));
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(message: $"Reason: {penalty.Reason}", type: 13));
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(message: $"DateStart: {penalty.DateStart}",
                                    type: 13));
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(message: $"DateEnd: {penalty.DateEnd}",
                                    type: 13));
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(message: "----- ------- -----", type: 13));
                            separatorSent = true;
                        }

                        IEnumerable<PenaltyLogDto> penaltyLogs = ServerManager.Instance.PenaltyLogs
                            .Where(predicate: s => s.AccountId == character.AccountId).ToList();

                        //PenaltyLogDTO penalty = penaltyLogs.LastOrDefault(s => s.DateEnd > DateTime.Now);
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(message: "----- PENALTIES -----", type: 13));

                        #region Warnings

                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(message: "----- WARNINGS -----", type: 13));
                        foreach (var penaltyLog in penaltyLogs.Where(predicate: s => s.Penalty == PenaltyType.Warning)
                            .OrderBy(keySelector: s => s.DateStart))
                            WritePenalty(penalty: penaltyLog);

                        if (!separatorSent)
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(message: "----- ------- -----", type: 13));

                        separatorSent = false;

                        #endregion

                        #region Mutes

                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(message: "----- MUTES -----", type: 13));
                        foreach (var penaltyLog in penaltyLogs.Where(predicate: s => s.Penalty == PenaltyType.Muted)
                            .OrderBy(keySelector: s => s.DateStart))
                            WritePenalty(penalty: penaltyLog);

                        if (!separatorSent)
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(message: "----- ------- -----", type: 13));

                        separatorSent = false;

                        #endregion

                        #region Bans

                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(message: "----- BANS -----", type: 13));
                        foreach (var penaltyLog in penaltyLogs.Where(predicate: s => s.Penalty == PenaltyType.Banned)
                            .OrderBy(keySelector: s => s.DateStart))
                            WritePenalty(penalty: penaltyLog);

                        if (!separatorSent)
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(message: "----- ------- -----", type: 13));

                        #endregion

                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(message: "----- SUMMARY -----", type: 13));
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: $"Warnings: {penaltyLogs.Count(predicate: s => s.Penalty == PenaltyType.Warning)}",
                            type: 13));
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: $"Mutes: {penaltyLogs.Count(predicate: s => s.Penalty == PenaltyType.Muted)}",
                                type: 13));
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: $"Bans: {penaltyLogs.Count(predicate: s => s.Penalty == PenaltyType.Banned)}",
                                type: 13));
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(message: "----- ------- -----", type: 13));
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                    }
                }
                else
                {
                    Session.SendPacket(packet: Session.Character.GenerateSay(message: returnHelp, type: 10));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: returnHelp, type: 10));
            }
        }

        /// <summary>
        ///     $MapDance Command
        /// </summary>
        /// <param name="mapDancePacket"></param>
        public void MapDance(MapDancePacket mapDancePacket)
        {
            if (mapDancePacket == null) throw new ArgumentNullException(paramName: nameof(mapDancePacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[MapDance]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[MapDance]"
                });

            if (Session.HasCurrentMapInstance)
            {
                Session.CurrentMapInstance.IsDancing = !Session.CurrentMapInstance.IsDancing;
                if (Session.CurrentMapInstance.IsDancing)
                {
                    Session.Character.Dance();
                    Session.CurrentMapInstance?.Broadcast(packet: "dance 2");
                }
                else
                {
                    Session.Character.Dance();
                    Session.CurrentMapInstance?.Broadcast(packet: "dance");
                }

                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
        }

        /// <summary>
        ///     $MapPVP Command
        /// </summary>
        /// <param name="mapPvpPacket"></param>
        public void MapPvp(MapPVPPacket mapPvpPacket)
        {
            if (mapPvpPacket == null) throw new ArgumentNullException(paramName: nameof(mapPvpPacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[MapPVP]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[MapPVP]"
                });

            Session.CurrentMapInstance.IsPvp = !Session.CurrentMapInstance.IsPvp;
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                    type: 10));
        }

        /// <summary>
        ///     $Morph Command
        /// </summary>
        /// <param name="morphPacket"></param>
        public void Morph(MorphPacket morphPacket)
        {
            if (morphPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Morph]MorphId: {morphPacket.MorphId} MorphDesign: {morphPacket.MorphDesign} Upgrade: {morphPacket.Upgrade} MorphId: {morphPacket.ArenaWinner}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Morph]MorphId: {morphPacket.MorphId} MorphDesign: {morphPacket.MorphDesign} Upgrade: {morphPacket.Upgrade} MorphId: {morphPacket.ArenaWinner}"
                    });

                if (morphPacket.MorphId < 30 && morphPacket.MorphId > 0)
                {
                    Session.Character.UseSp = true;
                    Session.Character.Morph = morphPacket.MorphId;
                    Session.Character.MorphUpgrade = morphPacket.Upgrade;
                    Session.Character.MorphUpgrade2 = morphPacket.MorphDesign;
                    Session.Character.ArenaWinner = morphPacket.ArenaWinner;
                    Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateCMode());
                }
                else if (morphPacket.MorphId > 30)
                {
                    Session.Character.IsVehicled = true;
                    Session.Character.Morph = morphPacket.MorphId;
                    Session.Character.ArenaWinner = morphPacket.ArenaWinner;
                    Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateCMode());
                }
                else
                {
                    Session.Character.IsVehicled = false;
                    Session.Character.UseSp = false;
                    Session.Character.ArenaWinner = 0;
                    Session.SendPacket(packet: Session.Character.GenerateCond());
                    Session.SendPacket(packet: Session.Character.GenerateLev());
                    Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateCMode());
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: MorphPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Mute Command
        /// </summary>
        /// <param name="mutePacket"></param>
        public void Mute(MutePacket mutePacket)
        {
            if (mutePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Mute]CharacterName: {mutePacket.CharacterName} Reason: {mutePacket.Reason} Until: {DateTime.Now.AddMinutes(value: mutePacket.Duration)}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Mute]CharacterName: {mutePacket.CharacterName} Reason: {mutePacket.Reason} Until: {DateTime.Now.AddMinutes(value: mutePacket.Duration)}"
                    });

                if (mutePacket.Duration == 0) mutePacket.Duration = 60;

                mutePacket.Reason = mutePacket.Reason?.Trim();
                MuteMethod(characterName: mutePacket.CharacterName, reason: mutePacket.Reason,
                    duration: mutePacket.Duration);
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: MutePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Packet Command
        /// </summary>
        /// <param name="packetCallbackPacket"></param>
        public void PacketCallBack(PacketCallbackPacket packetCallbackPacket)
        {
            if (packetCallbackPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Packet]Packet: {packetCallbackPacket.Packet}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Packet]Packet: {packetCallbackPacket.Packet}"
                    });

                Session.SendPacket(packet: packetCallbackPacket.Packet);
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: packetCallbackPacket.Packet, type: 10));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: PacketCallbackPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Maintenance Command
        /// </summary>
        /// <param name="maintenancePacket"></param>
        public void PlanMaintenance(MaintenancePacket maintenancePacket)
        {
            if (maintenancePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Maintenance]Delay: {maintenancePacket.Delay} Duration: {maintenancePacket.Duration} Reason: {maintenancePacket.Reason}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Maintenance]Delay: {maintenancePacket.Delay} Duration: {maintenancePacket.Duration} Reason: {maintenancePacket.Reason}"
                    });

                var dateStart = DateTime.Now.AddMinutes(value: maintenancePacket.Delay);
                var maintenance = new MaintenanceLogDto
                {
                    DateEnd = dateStart.AddMinutes(value: maintenancePacket.Duration),
                    DateStart = dateStart,
                    Reason = maintenancePacket.Reason
                };
                DaoFactory.MaintenanceLogDao.Insert(maintenanceLog: maintenance);
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: MaintenancePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $PortalTo Command
        /// </summary>
        /// <param name="portalToPacket"></param>
        public void PortalTo(PortalToPacket portalToPacket)
        {
            if (portalToPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[PortalTo]DestinationMapId: {portalToPacket.DestinationMapId} DestinationMapX: {portalToPacket.DestinationX} DestinationY: {portalToPacket.DestinationY}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[PortalTo]DestinationMapId: {portalToPacket.DestinationMapId} DestinationMapX: {portalToPacket.DestinationX} DestinationY: {portalToPacket.DestinationY}"
                    });

                AddPortal(destinationMapId: portalToPacket.DestinationMapId, destinationX: portalToPacket.DestinationX,
                    destinationY: portalToPacket.DestinationY,
                    type: portalToPacket.PortalType == null ? (short)-1 : (short)portalToPacket.PortalType,
                    insertToDatabase: false);
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: PortalToPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Position Command
        /// </summary>
        /// <param name="positionPacket"></param>
        public void Position(PositionPacket positionPacket)
        {
            if (positionPacket == null) throw new ArgumentNullException(paramName: nameof(positionPacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[Position]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[Position]"
                });

            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"Map:{Session.Character.MapInstance.Map.MapId} - X:{Session.Character.PositionX} - Y:{Session.Character.PositionY} - Dir:{Session.Character.Direction} - Cell:{Session.CurrentMapInstance.Map.JaggedGrid[Session.Character.PositionX][Session.Character.PositionY]?.Value}",
                type: 12));
        }

        /// <summary>
        ///     $Promote Command
        /// </summary>
        /// <param name="promotePacket"></param>
        public void Promote(PromotePacket promotePacket)
        {
            if (promotePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Promote]CharacterName: {promotePacket.CharacterName}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Promote]CharacterName: {promotePacket.CharacterName}"
                    });

                var name = promotePacket.CharacterName;
                try
                {
                    var account =
                        DaoFactory.AccountDao.LoadById(accountId: DaoFactory.CharacterDao.LoadByName(name: name)
                            .AccountId);
                    if (account?.Authority >= AuthorityType.User)
                    {
                        if (account.Authority < Session.Account.Authority)
                        {
                            var newAuthority = AuthorityType.User;
                            if (account.Authority == AuthorityType.User)
                                newAuthority = AuthorityType.Gs;
                            else if (account.Authority == AuthorityType.Gs)
                                newAuthority = AuthorityType.Mod;
                            else if (account.Authority == AuthorityType.Mod)
                                newAuthority = AuthorityType.Gm;
                            else if (account.Authority == AuthorityType.Gm)
                                newAuthority = AuthorityType.Administrator;
                            else if (account.Authority == AuthorityType.Administrator)
                                newAuthority = AuthorityType.User;
                            else
                                newAuthority = account.Authority;

                            account.Authority = newAuthority;
                            DaoFactory.AccountDao.InsertOrUpdate(account: ref account);
                            var session =
                                ServerManager.Instance.Sessions.FirstOrDefault(
                                    predicate: s => s.Character?.Name == name);

                            if (session != null)
                            {
                                session.Account.Authority = newAuthority;
                                session.Character.Authority = newAuthority;
                                ServerManager.Instance.ChangeMap(id: session.Character.CharacterId);
                                DaoFactory.AccountDao.WriteGeneralLog(accountId: session.Account.AccountId,
                                    ipAddress: session.IpAddress,
                                    characterId: session.Character.CharacterId, logType: GeneralLogType.Promotion,
                                    logData: $"by: {Session.Character.Name}");
                            }
                            else
                            {
                                DaoFactory.AccountDao.WriteGeneralLog(accountId: account.AccountId,
                                    ipAddress: "127.0.0.1", characterId: null,
                                    logType: GeneralLogType.Promotion, logData: $"by: {Session.Character.Name}");
                            }

                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "DONE"), type: 10));
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "CANT_DO_THAT"), type: 10));
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                    }
                }
                catch
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: PromotePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Rarify Command
        /// </summary>
        /// <param name="rarifyPacket"></param>
        public void Rarify(RarifyPacket rarifyPacket)
        {
            if (rarifyPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Rarify]Slot: {rarifyPacket.Slot} Mode: {rarifyPacket.Mode} Protection: {rarifyPacket.Protection}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Rarify]Slot: {rarifyPacket.Slot} Mode: {rarifyPacket.Mode} Protection: {rarifyPacket.Protection}"
                    });

                if (rarifyPacket.Slot >= 0)
                {
                    var wearableInstance =
                        Session.Character.Inventory.LoadBySlotAndType(slot: rarifyPacket.Slot, type: 0);
                    wearableInstance?.RarifyItem(session: Session, mode: rarifyPacket.Mode,
                        protection: rarifyPacket.Protection);
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: RarifyPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $RemoveMob Packet
        /// </summary>
        /// <param name="removeMobPacket"></param>
        public void RemoveMob(RemoveMobPacket removeMobPacket)
        {
            if (removeMobPacket == null) throw new ArgumentNullException(paramName: nameof(removeMobPacket));
            if (Session.HasCurrentMapInstance)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[RemoveMob]NpcMonsterId: {Session.Character.LastNpcMonsterId}");

                var monster =
                    Session.CurrentMapInstance.GetMonsterById(mapMonsterId: Session.Character.LastNpcMonsterId);
                var npc = Session.CurrentMapInstance.GetNpc(mapNpcId: Session.Character.LastNpcMonsterId);
                if (monster != null)
                {
                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            PacketType = LogType.GmCommand,
                            Packet =
                                $"[RemoveMob]NpcMonsterId: {Session.Character.LastNpcMonsterId} MonsterVNum: {monster.MonsterVNum} MapId: {monster.MapId} X: {monster.MapX} Y: {monster.MapY}"
                        });

                    var distance = Map.GetDistance(p: new MapCell
                    {
                        X = Session.Character.PositionX,
                        Y = Session.Character.PositionY
                    }, q: new MapCell
                    {
                        X = monster.MapX,
                        Y = monster.MapY
                    });
                    if (distance > 10)
                    {
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "TOO_FAR")),
                            type: 11));
                        return;
                    }

                    if (monster.IsAlive)
                    {
                        Session.CurrentMapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                            callerId: monster.MapMonsterId));
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MONSTER_REMOVED"),
                                monster.MapMonsterId,
                                monster.Monster.Name, monster.MapId, monster.MapX, monster.MapY), type: 12));
                        Session.CurrentMapInstance.RemoveMonster(monsterToRemove: monster);
                        Session.CurrentMapInstance.RemovedMobNpcList.Add(item: monster);
                        if (DaoFactory.MapMonsterDao.LoadById(mapMonsterId: monster.MapMonsterId) != null)
                            DaoFactory.MapMonsterDao.DeleteById(mapMonsterId: monster.MapMonsterId);
                    }
                    else
                    {
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "MONSTER_NOT_ALIVE")), type: 11));
                    }
                }
                else if (npc != null)
                {
                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            PacketType = LogType.GmCommand,
                            Packet =
                                $"[RemoveMob]NpcMonsterId: {Session.Character.LastNpcMonsterId} NpcVNum: {npc.NpcVNum} MapId: {npc.MapId} X: {npc.MapX} Y: {npc.MapY}"
                        });

                    var distance = Map.GetDistance(p: new MapCell
                    {
                        X = Session.Character.PositionX,
                        Y = Session.Character.PositionY
                    }, q: new MapCell
                    {
                        X = npc.MapX,
                        Y = npc.MapY
                    });
                    if (distance > 5)
                    {
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "TOO_FAR")),
                            type: 11));
                        return;
                    }

                    if (!npc.IsMate && !npc.IsDisabled && !npc.IsProtected)
                    {
                        Session.CurrentMapInstance.Broadcast(
                            packet: StaticPacketHelper.Out(type: UserType.Npc, callerId: npc.MapNpcId));
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "NPCMONSTER_REMOVED"), npc.MapNpcId,
                                npc.Npc.Name, npc.MapId, npc.MapX, npc.MapY), type: 12));
                        Session.CurrentMapInstance.RemoveNpc(npcToRemove: npc);
                        Session.CurrentMapInstance.RemovedMobNpcList.Add(item: npc);
                        if (DaoFactory.ShopDao.LoadByNpc(mapNpcId: npc.MapNpcId) != null)
                            DaoFactory.ShopDao.DeleteByNpcId(mapNpcId: npc.MapNpcId);

                        if (DaoFactory.MapNpcDao.LoadById(mapNpcId: npc.MapNpcId) != null)
                            DaoFactory.MapNpcDao.DeleteById(mapNpcId: npc.MapNpcId);
                    }
                    else
                    {
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "NPC_CANNOT_BE_REMOVED")), type: 11));
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "NPCMONSTER_NOT_FOUND"), type: 11));
                }
            }
        }

        /// <summary>
        ///     $RemovePortal Command
        /// </summary>
        /// <param name="removePortalPacket"></param>
        public void RemovePortal(RemovePortalPacket removePortalPacket)
        {
            if (removePortalPacket == null) throw new ArgumentNullException(paramName: nameof(removePortalPacket));
            if (Session.HasCurrentMapInstance)
            {
                var portal = Session.CurrentMapInstance.Portals.Find(match: s =>
                    s.SourceMapInstanceId == Session.Character.MapInstanceId && Map.GetDistance(
                        p: new MapCell { X = s.SourceX, Y = s.SourceY },
                        q: new MapCell { X = Session.Character.PositionX, Y = Session.Character.PositionY }) < 10);
                if (portal != null)
                {
                    Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                        data:
                        $"[RemovePortal]MapId: {portal.SourceMapId} MapX: {portal.SourceX} MapY: {portal.SourceY}");

                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            PacketType = LogType.GmCommand,
                            Packet =
                                $"[RemovePortal]MapId: {portal.SourceMapId} MapX: {portal.SourceX} MapY: {portal.SourceY}"
                        });

                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "NEAREST_PORTAL"),
                            arg0: portal.SourceMapId,
                            arg1: portal.SourceX, arg2: portal.SourceY), type: 12));
                    portal.IsDisabled = true;
                    Session.CurrentMapInstance?.Broadcast(packet: portal.GenerateGp());
                    Session.CurrentMapInstance.Portals.Remove(item: portal);
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "NO_PORTAL_FOUND"), type: 11));
                }
            }
        }

        /// <summary>
        ///     $Resize Command
        /// </summary>
        /// <param name="resizePacket"></param>
        public void Resize(ResizePacket resizePacket)
        {
            if (resizePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Resize]Size: {resizePacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Resize]Size: {resizePacket.Value}"
                    });

                if (resizePacket.Value >= 0)
                {
                    Session.Character.Size = resizePacket.Value;
                    Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateScal());
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: ResizePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Restart Command
        /// </summary>
        /// <param name="restartPacket"></param>
        public void Restart(RestartPacket restartPacket)
        {
            var time = restartPacket.Time > 0 ? restartPacket.Time : 5;

            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                data: $"[Restart]Time: {time}");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = $"[Restart]Time: {time}"
                });

            if (ServerManager.Instance.TaskShutdown != null)
            {
                ServerManager.Instance.ShutdownStop = true;
                ServerManager.Instance.TaskShutdown = null;
            }
            else
            {
                ServerManager.Instance.IsReboot = true;
                ServerManager.Instance.TaskShutdown = ServerManager.Instance.ShutdownTaskAsync(Time: time);
            }

            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                    type: 10));
        }

        /// <summary>
        ///     $RestartAll Command
        /// </summary>
        /// <param name="restartAllPacket"></param>
        public void RestartAll(RestartAllPacket restartAllPacket)
        {
            if (restartAllPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[RestartAll]");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = "[RestartAll]"
                    });

                var worldGroup = !string.IsNullOrEmpty(value: restartAllPacket.WorldGroup)
                    ? restartAllPacket.WorldGroup
                    : ServerManager.Instance.ServerGroup;

                var time = restartAllPacket.Time;

                if (time < 1) time = 5;

                CommunicationServiceClient.Instance.Restart(worldGroup: worldGroup, time: time);

                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: RestartAllPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $SearchItem Command
        /// </summary>
        /// <param name="searchItemPacket"></param>
        public void SearchItem(SearchItemPacket searchItemPacket)
        {
            if (searchItemPacket != null)
            {
                var contents = searchItemPacket.Contents;
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[SearchItem]Contents: {(string.IsNullOrEmpty(value: contents) ? "none" : contents)}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[SearchItem]Contents: {(string.IsNullOrEmpty(value: contents) ? "none" : contents)}"
                    });

                var name = "";
                byte page = 0;
                if (!string.IsNullOrEmpty(value: contents))
                {
                    var packetsplit = contents.Split(' ');
                    var withPage = byte.TryParse(s: packetsplit[0], result: out page);
                    name = packetsplit.Length == 1 && withPage
                        ? ""
                        : packetsplit.Skip(count: withPage ? 1 : 0).Aggregate(func: (a, b) => a + ' ' + b);
                }

                IEnumerable<ItemDto> itemlist = DaoFactory.ItemDao.FindByName(name: name)
                    .OrderBy(keySelector: s => s.VNum)
                    .Skip(count: page * 200).Take(count: 200).ToList();
                if (itemlist.Any())
                    foreach (var item in itemlist)
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message:
                            $"[SearchItem:{page}]Item: {(string.IsNullOrEmpty(value: item.Name) ? "none" : item.Name)} VNum: {item.VNum}",
                            type: 12));
                else
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "ITEM_NOT_FOUND"), type: 11));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: SearchItemPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $SearchMonster Command
        /// </summary>
        /// <param name="searchMonsterPacket"></param>
        public void SearchMonster(SearchMonsterPacket searchMonsterPacket)
        {
            if (searchMonsterPacket != null)
            {
                var contents = searchMonsterPacket.Contents;
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[SearchMonster]Contents: {(string.IsNullOrEmpty(value: contents) ? "none" : contents)}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[SearchMonster]Contents: {(string.IsNullOrEmpty(value: contents) ? "none" : contents)}"
                    });

                var name = "";
                byte page = 0;
                if (!string.IsNullOrEmpty(value: contents))
                {
                    var packetsplit = contents.Split(' ');
                    var withPage = byte.TryParse(s: packetsplit[0], result: out page);
                    name = packetsplit.Length == 1 && withPage
                        ? ""
                        : packetsplit.Skip(count: withPage ? 1 : 0).Aggregate(func: (a, b) => a + ' ' + b);
                }

                IEnumerable<NpcMonsterDto> monsterlist = DaoFactory.NpcMonsterDao.FindByName(name: name)
                    .OrderBy(keySelector: s => s.NpcMonsterVNum).Skip(count: page * 200).Take(count: 200).ToList();
                if (monsterlist.Any())
                    foreach (var npcMonster in monsterlist)
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message:
                            $"[SearchMonster:{page}]Monster: {(string.IsNullOrEmpty(value: npcMonster.Name) ? "none" : npcMonster.Name)} VNum: {npcMonster.NpcMonsterVNum}",
                            type: 12));
                else
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "MONSTER_NOT_FOUND"), type: 11));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: SearchMonsterPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $SetPerfection Command
        /// </summary>
        /// <param name="setPerfectionPacket"></param>
        public void SetPerfection(SetPerfectionPacket setPerfectionPacket)
        {
            if (setPerfectionPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[SetPerfection]Slot: {setPerfectionPacket.Slot} Type: {setPerfectionPacket.Type} Value: {setPerfectionPacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[SetPerfection]Slot: {setPerfectionPacket.Slot} Type: {setPerfectionPacket.Type} Value: {setPerfectionPacket.Value}"
                    });

                if (setPerfectionPacket.Slot >= 0)
                {
                    var specialistInstance =
                        Session.Character.Inventory.LoadBySlotAndType(slot: setPerfectionPacket.Slot, type: 0);

                    if (specialistInstance != null)
                        switch (setPerfectionPacket.Type)
                        {
                            case 0:
                                specialistInstance.SpStoneUpgrade = setPerfectionPacket.Value;
                                break;

                            case 1:
                                specialistInstance.SpDamage = setPerfectionPacket.Value;
                                break;

                            case 2:
                                specialistInstance.SpDefence = setPerfectionPacket.Value;
                                break;

                            case 3:
                                specialistInstance.SpElement = setPerfectionPacket.Value;
                                break;

                            case 4:
                                specialistInstance.SpHp = setPerfectionPacket.Value;
                                break;

                            case 5:
                                specialistInstance.SpFire = setPerfectionPacket.Value;
                                break;

                            case 6:
                                specialistInstance.SpWater = setPerfectionPacket.Value;
                                break;

                            case 7:
                                specialistInstance.SpLight = setPerfectionPacket.Value;
                                break;

                            case 8:
                                specialistInstance.SpDark = setPerfectionPacket.Value;
                                break;

                            default:
                                Session.SendPacket(packet: Session.Character.GenerateSay(
                                    message: UpgradeCommandPacket.ReturnHelp(),
                                    type: 10));
                                break;
                        }
                    else
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(message: UpgradeCommandPacket.ReturnHelp(),
                                type: 10));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: UpgradeCommandPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Shout Command
        /// </summary>
        /// <param name="shoutPacket"></param>
        public void Shout(ShoutPacket shoutPacket)
        {
            if (shoutPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Shout]Message: {shoutPacket.Message}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Shout]Message: {shoutPacket.Message}"
                    });

                CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                {
                    DestinationCharacterId = null,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message = shoutPacket.Message,
                    Type = MessageType.Shout
                });
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: ShoutPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $ShoutHere Command
        /// </summary>
        /// <param name="shoutHerePacket"></param>
        public void ShoutHere(ShoutHerePacket shoutHerePacket)
        {
            if (shoutHerePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[ShoutHere]Message: {shoutHerePacket.Message}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[ShoutHere]Message: {shoutHerePacket.Message}"
                    });

                ServerManager.Shout(message: shoutHerePacket.Message);
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ShoutHerePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Shutdown Command
        /// </summary>
        /// <param name="shutdownPacket"></param>
        public void Shutdown(ShutdownPacket shutdownPacket)
        {
            if (shutdownPacket == null) throw new ArgumentNullException(paramName: nameof(shutdownPacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[Shutdown]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[Shutdown]"
                });

            if (ServerManager.Instance.TaskShutdown != null)
            {
                ServerManager.Instance.ShutdownStop = true;
                ServerManager.Instance.TaskShutdown = null;
            }
            else
            {
                ServerManager.Instance.TaskShutdown = ServerManager.Instance.ShutdownTaskAsync();
                ServerManager.Instance.TaskShutdown.Start();
            }
        }

        /// <summary>
        ///     $ShutdownAll Command
        /// </summary>
        /// <param name="shutdownAllPacket"></param>
        public void ShutdownAll(ShutdownAllPacket shutdownAllPacket)
        {
            if (shutdownAllPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[ShutdownAll]");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = "[ShutdownAll]"
                    });

                if (!string.IsNullOrEmpty(value: shutdownAllPacket.WorldGroup))
                    CommunicationServiceClient.Instance.Shutdown(worldGroup: shutdownAllPacket.WorldGroup);
                else
                    CommunicationServiceClient.Instance.Shutdown(worldGroup: ServerManager.Instance.ServerGroup);

                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ShutdownAllPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Sort Command
        /// </summary>
        /// <param name="sortPacket"></param>
        public void Sort(SortPacket sortPacket)
        {
            if (sortPacket?.InventoryType.HasValue == true)
            {
                Logger.LogUserEvent(logEvent: "USERCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Sort]InventoryType: {sortPacket.InventoryType}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.UserCommand,
                        Packet = $"[Sort]InventoryType: {sortPacket.InventoryType}"
                    });

                if (sortPacket.InventoryType == InventoryType.Equipment
                    || sortPacket.InventoryType == InventoryType.Etc || sortPacket.InventoryType == InventoryType.Main)
                    Session.Character.Inventory.Reorder(session: Session,
                        inventoryType: sortPacket.InventoryType.Value);
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: SortPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Speed Command
        /// </summary>
        /// <param name="speedPacket"></param>
        public void Speed(SpeedPacket speedPacket)
        {
            if (speedPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Speed]Value: {speedPacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Speed]Value: {speedPacket.Value}"
                    });

                if (speedPacket.Value < 60)
                {
                    Session.Character.Speed = speedPacket.Value;
                    Session.Character.IsCustomSpeed = true;
                    Session.SendPacket(packet: Session.Character.GenerateCond());
                }

                if (speedPacket.Value == 0)
                {
                    Session.Character.IsCustomSpeed = false;
                    Session.Character.LoadSpeed();
                    Session.SendPacket(packet: Session.Character.GenerateCond());
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: SpeedPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $SPRefill Command
        /// </summary>
        /// <param name="spRefillPacket"></param>
        public void SpRefill(SPRefillPacket spRefillPacket)
        {
            if (spRefillPacket == null) throw new ArgumentNullException(paramName: nameof(spRefillPacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[SPRefill]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[SPRefill]"
                });

            Session.Character.SpPoint = 10000;
            Session.Character.SpAdditionPoint = 1000000;
            Session.SendPacket(
                packet: UserInterfaceHelper.GenerateMsg(message: Language.Instance.GetMessageFromKey(key: "SP_REFILL"),
                    type: 0));
            Session.SendPacket(packet: Session.Character.GenerateSpPoint());
        }

        /// <summary>
        ///     $Event Command
        /// </summary>
        /// <param name="eventPacket"></param>
        public void StartEvent(EventPacket eventPacket)
        {
            if (eventPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Event]EventType: {eventPacket.EventType.ToString()}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Event]EventType: {eventPacket.EventType.ToString()}"
                    });

                if (eventPacket.LvlBracket >= 0)
                    EventHelper.GenerateEvent(type: eventPacket.EventType, lvlBracket: eventPacket.LvlBracket);
                else
                    EventHelper.GenerateEvent(type: eventPacket.EventType);
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: EventPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $GlobalEvent Command
        /// </summary>
        /// <param name="globalEventPacket"></param>
        public void StartGlobalEvent(GlobalEventPacket globalEventPacket)
        {
            if (globalEventPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[GlobalEvent]EventType: {globalEventPacket.EventType.ToString()}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[GlobalEvent]EventType: {globalEventPacket.EventType.ToString()}"
                    });

                CommunicationServiceClient.Instance.RunGlobalEvent(eventType: globalEventPacket.EventType);
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: EventPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Stat Command
        /// </summary>
        /// <param name="statCommandPacket"></param>
        public void Stat(StatCommandPacket statCommandPacket)
        {
            if (statCommandPacket == null) throw new ArgumentNullException(paramName: nameof(statCommandPacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[Stat]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[Stat]"
                });

            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"{Language.Instance.GetMessageFromKey(key: "XP_RATE_NOW")}: {ServerManager.Instance.Configuration.RateXp} ",
                type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"{Language.Instance.GetMessageFromKey(key: "DROP_RATE_NOW")}: {ServerManager.Instance.Configuration.RateDrop} ",
                type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"{Language.Instance.GetMessageFromKey(key: "GOLD_RATE_NOW")}: {ServerManager.Instance.Configuration.RateGold} ",
                type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"{Language.Instance.GetMessageFromKey(key: "GOLD_DROPRATE_NOW")}: {ServerManager.Instance.Configuration.RateGoldDrop} ",
                type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"{Language.Instance.GetMessageFromKey(key: "HERO_XPRATE_NOW")}: {ServerManager.Instance.Configuration.RateHeroicXp} ",
                type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"{Language.Instance.GetMessageFromKey(key: "FAIRYXP_RATE_NOW")}: {ServerManager.Instance.Configuration.RateFairyXp} ",
                type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"{Language.Instance.GetMessageFromKey(key: "REPUTATION_RATE_NOW")}: {ServerManager.Instance.Configuration.RateReputation} ",
                type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"{Language.Instance.GetMessageFromKey(key: "SERVER_WORKING_TIME")}: {(Process.GetCurrentProcess().StartTime - DateTime.Now).ToString(format: @"d\ hh\:mm\:ss")} ",
                type: 13));

            foreach (var message in CommunicationServiceClient.Instance.RetrieveServerStatistics())
                Session.SendPacket(packet: Session.Character.GenerateSay(message: message, type: 13));
        }

        /// <summary>
        ///     $Sudo Command
        /// </summary>
        /// <param name="sudoPacket"></param>
        public void SudoCommand(SudoPacket sudoPacket)
        {
            if (sudoPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Sudo]CharacterName: {sudoPacket.CharacterName} CommandContents:{sudoPacket.CommandContents}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Sudo]CharacterName: {sudoPacket.CharacterName} CommandContents:{sudoPacket.CommandContents}"
                    });

                if (sudoPacket.CharacterName == "*")
                {
                    foreach (var sess in Session.CurrentMapInstance.Sessions.ToList()
                        .Where(predicate: s => s.Character?.Authority <= Session.Character.Authority))
                        sess.ReceivePacket(packet: sudoPacket.CommandContents, ignoreAuthority: true);
                }
                else
                {
                    var session = ServerManager.Instance.GetSessionByCharacterName(name: sudoPacket.CharacterName);

                    if (session != null && !string.IsNullOrWhiteSpace(value: sudoPacket.CommandContents))
                    {
                        if (session.Character?.Authority <= Session.Character.Authority)
                            session.ReceivePacket(packet: sudoPacket.CommandContents, ignoreAuthority: true);
                        else
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "CANT_DO_THAT"),
                                    type: 0));
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "USER_NOT_CONNECTED"),
                                type: 0));
                    }
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: SudoPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Mob Command
        /// </summary>
        /// <param name="mobPacket"></param>
        public void Mob(MobPacket mobPacket)
        {
            if (mobPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Mob]NpcMonsterVNum: {mobPacket.NpcMonsterVNum} Amount: {mobPacket.Amount} IsMoving: {mobPacket.IsMoving}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Mob]NpcMonsterVNum: {mobPacket.NpcMonsterVNum} Amount: {mobPacket.Amount} IsMoving: {mobPacket.IsMoving}"
                    });

                if (Session.IsOnMap && Session.HasCurrentMapInstance)
                {
                    var npcmonster = ServerManager.GetNpcMonster(npcVNum: mobPacket.NpcMonsterVNum);
                    if (npcmonster == null) return;

                    var random = new Random();
                    for (var i = 0; i < mobPacket.Amount; i++)
                    {
                        var possibilities = new List<MapCell>();
                        for (short x = -4; x < 5; x++)
                            for (short y = -4; y < 5; y++)
                                possibilities.Add(item: new MapCell { X = x, Y = y });

                        foreach (var possibilitie in possibilities.OrderBy(keySelector: s => random.Next()))
                        {
                            var mapx = (short)(Session.Character.PositionX + possibilitie.X);
                            var mapy = (short)(Session.Character.PositionY + possibilitie.Y);
                            if (!Session.CurrentMapInstance?.Map.IsBlockedZone(x: mapx, y: mapy) ?? false) break;
                        }

                        if (Session.CurrentMapInstance != null)
                        {
                            var monster = new MapMonster
                            {
                                MonsterVNum = mobPacket.NpcMonsterVNum,
                                MapY = Session.Character.PositionY,
                                MapX = Session.Character.PositionX,
                                MapId = Session.Character.MapInstance.Map.MapId,
                                Position = Session.Character.Direction,
                                IsMoving = mobPacket.IsMoving,
                                MapMonsterId = Session.CurrentMapInstance.GetNextMonsterId(),
                                ShouldRespawn = false
                            };
                            monster.Initialize(currentMapInstance: Session.CurrentMapInstance);
                            Session.CurrentMapInstance.AddMonster(monster: monster);
                            Session.CurrentMapInstance.Broadcast(packet: monster.GenerateIn());
                        }
                    }
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: MobPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $MobRain Command
        /// </summary>
        /// <param name="mobRain"></param>
        public void MobRain(MobRainPacket mobRainPacket)
        {
            if (mobRainPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[MobRain]NpcMonsterVNum: {mobRainPacket.NpcMonsterVNum} Amount: {mobRainPacket.Amount} IsMoving: {mobRainPacket.IsMoving}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[MobRain]NpcMonsterVNum: {mobRainPacket.NpcMonsterVNum} Amount: {mobRainPacket.Amount} IsMoving: {mobRainPacket.IsMoving}"
                    });

                if (Session.IsOnMap && Session.HasCurrentMapInstance)
                {
                    var npcmonster = ServerManager.GetNpcMonster(npcVNum: mobRainPacket.NpcMonsterVNum);
                    if (npcmonster == null) return;

                    var SummonParameters = new List<MonsterToSummon>();
                    SummonParameters.AddRange(collection: Session.Character.MapInstance.Map.GenerateMonsters(
                        vnum: mobRainPacket.NpcMonsterVNum, amount: mobRainPacket.Amount, move: mobRainPacket.IsMoving,
                        deathEvents: new List<EventContainer>()));
                    EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromSeconds(value: 1),
                        evt: new EventContainer(mapInstance: Session.CurrentMapInstance,
                            eventActionType: EventActionType.Spawnmonsters,
                            param: SummonParameters));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: MobRainPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $SNPC Command
        /// </summary>
        /// <param name="NpcPacket"></param>
        public void Npc(NPCPacket NpcPacket)
        {
            if (NpcPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[NPC]NpcMonsterVNum: {NpcPacket.NpcMonsterVNum} Amount: {NpcPacket.Amount} IsMoving: {NpcPacket.IsMoving}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[NPC]NpcMonsterVNum: {NpcPacket.NpcMonsterVNum} Amount: {NpcPacket.Amount} IsMoving: {NpcPacket.IsMoving}"
                    });

                if (Session.IsOnMap && Session.HasCurrentMapInstance)
                {
                    var npcmonster = ServerManager.GetNpcMonster(npcVNum: NpcPacket.NpcMonsterVNum);
                    if (npcmonster == null) return;

                    var random = new Random();
                    for (var i = 0; i < NpcPacket.Amount; i++)
                    {
                        var possibilities = new List<MapCell>();
                        for (short x = -4; x < 5; x++)
                            for (short y = -4; y < 5; y++)
                                possibilities.Add(item: new MapCell { X = x, Y = y });

                        foreach (var possibilitie in possibilities.OrderBy(keySelector: s => random.Next()))
                        {
                            var mapx = (short)(Session.Character.PositionX + possibilitie.X);
                            var mapy = (short)(Session.Character.PositionY + possibilitie.Y);
                            if (!Session.CurrentMapInstance?.Map.IsBlockedZone(x: mapx, y: mapy) ?? false) break;
                        }

                        if (Session.CurrentMapInstance != null)
                        {
                            var npc = new MapNpc
                            {
                                NpcVNum = NpcPacket.NpcMonsterVNum,
                                MapY = Session.Character.PositionY,
                                MapX = Session.Character.PositionX,
                                MapId = Session.Character.MapInstance.Map.MapId,
                                Position = Session.Character.Direction,
                                IsMoving = NpcPacket.IsMoving,
                                ShouldRespawn = false,
                                MapNpcId = Session.CurrentMapInstance.GetNextNpcId()
                            };
                            npc.Initialize(currentMapInstance: Session.CurrentMapInstance);
                            Session.CurrentMapInstance.AddNpc(npc: npc);
                            Session.CurrentMapInstance.Broadcast(packet: npc.GenerateIn());
                        }
                    }
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: NPCPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Teleport Command
        /// </summary>
        /// <param name="teleportPacket"></param>
        public void Teleport(TeleportPacket teleportPacket)
        {
            if (teleportPacket != null)
            {
                if (Session.Character.HasShopOpened || Session.Character.InExchangeOrTrade)
                    Session.Character.DisposeShopAndExchange();

                if (Session.Character.IsChangingMapInstance) return;

                var session = ServerManager.Instance.GetSessionByCharacterName(name: teleportPacket.Data);

                if (session != null)
                {
                    Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                        data: $"[Teleport]CharacterName: {teleportPacket.Data}");

                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            PacketType = LogType.GmCommand,
                            Packet = $"[Teleport]CharacterName: {teleportPacket.Data}"
                        });

                    var mapX = session.Character.PositionX;
                    var mapY = session.Character.PositionY;
                    if (session.Character.Miniland == session.Character.MapInstance)
                        ServerManager.Instance.JoinMiniland(session: Session, minilandOwner: session);
                    else
                        ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                            mapInstanceId: session.Character.MapInstanceId, mapX: mapX, mapY: mapY);
                }
                else if (short.TryParse(s: teleportPacket.Data, result: out var mapId))
                {
                    Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                        data:
                        $"[Teleport]MapId: {teleportPacket.Data} MapX: {teleportPacket.X} MapY: {teleportPacket.Y}");

                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            PacketType = LogType.GmCommand,
                            Packet =
                                $"[Teleport]MapId: {teleportPacket.Data} MapX: {teleportPacket.X} MapY: {teleportPacket.Y}"
                        });

                    if (ServerManager.GetBaseMapInstanceIdByMapId(mapId: mapId) != default)
                    {
                        if (teleportPacket.X == 0 && teleportPacket.Y == 0)
                            ServerManager.Instance.TeleportOnRandomPlaceInMap(session: Session,
                                guid: ServerManager.GetBaseMapInstanceIdByMapId(mapId: mapId));
                        else
                            ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: mapId,
                                mapX: teleportPacket.X,
                                mapY: teleportPacket.Y);
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "MAP_NOT_FOUND"), type: 0));
                    }
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: TeleportPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Summon Command
        /// </summary>
        /// <param name="summonPacket"></param>
        public void Summon(SummonPacket summonPacket)
        {
            var random = new Random();
            if (summonPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Summon]CharacterName: {summonPacket.CharacterName}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Summon]CharacterName: {summonPacket.CharacterName}"
                    });

                if (summonPacket.CharacterName == "*")
                {
                    Parallel.ForEach(
                        source: ServerManager.Instance.Sessions.Where(predicate: s =>
                            s.Character != null && s.Character.CharacterId != Session.Character.CharacterId),
                        body: session =>
                        {
                            // clear any shop or trade on target character
                            Session.Character.DisposeShopAndExchange();
                            if (!session.Character.IsChangingMapInstance && Session.HasCurrentMapInstance)
                            {
                                var possibilities = new List<MapCell>();
                                for (short x = -6, y = -6; x < 6 && y < 6; x++, y++)
                                    possibilities.Add(item: new MapCell { X = x, Y = y });

                                var mapXPossibility = Session.Character.PositionX;
                                var mapYPossibility = Session.Character.PositionY;
                                foreach (var possibility in possibilities.OrderBy(keySelector: s => random.Next()))
                                {
                                    mapXPossibility = (short)(Session.Character.PositionX + possibility.X);
                                    mapYPossibility = (short)(Session.Character.PositionY + possibility.Y);
                                    if (!Session.CurrentMapInstance.Map.IsBlockedZone(x: mapXPossibility,
                                        y: mapYPossibility))
                                        break;
                                }

                                if (Session.Character.Miniland == Session.Character.MapInstance)
                                    ServerManager.Instance.JoinMiniland(session: session, minilandOwner: Session);
                                else
                                    ServerManager.Instance.ChangeMapInstance(characterId: session.Character.CharacterId,
                                        mapInstanceId: Session.Character.MapInstanceId, mapX: mapXPossibility,
                                        mapY: mapYPossibility);
                            }
                        });
                }
                else
                {
                    var targetSession =
                        ServerManager.Instance.GetSessionByCharacterName(name: summonPacket.CharacterName);
                    if (targetSession?.Character.IsChangingMapInstance == false)
                    {
                        Session.Character.DisposeShopAndExchange();
                        ServerManager.Instance.ChangeMapInstance(characterId: targetSession.Character.CharacterId,
                            mapInstanceId: Session.Character.MapInstanceId,
                            mapX: (short)(Session.Character.PositionX + 1),
                            mapY: (short)(Session.Character.PositionY + 1));
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "USER_NOT_CONNECTED"),
                                type: 0));
                    }
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: SummonPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Unban Command
        /// </summary>
        /// <param name="unbanPacket"></param>
        public void Unban(UnbanPacket unbanPacket)
        {
            if (unbanPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Unban]CharacterName: {unbanPacket.CharacterName}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Unban]CharacterName: {unbanPacket.CharacterName}"
                    });

                var name = unbanPacket.CharacterName;
                var chara = DaoFactory.CharacterDao.LoadByName(name: name);
                if (chara != null)
                {
                    var log = ServerManager.Instance.PenaltyLogs.Find(match: s =>
                        s.AccountId == chara.AccountId && s.Penalty == PenaltyType.Banned && s.DateEnd > DateTime.Now);
                    if (log != null)
                    {
                        log.DateEnd = DateTime.Now.AddSeconds(value: -1);
                        Character.InsertOrUpdatePenalty(log: log);
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "DONE"),
                            type: 10));
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "USER_NOT_BANNED"), type: 10));
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: UnbanPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Undercover Command
        /// </summary>
        /// <param name="undercoverPacket"></param>
        public void Undercover(UndercoverPacket undercoverPacket)
        {
            if (undercoverPacket == null) throw new ArgumentNullException(paramName: nameof(undercoverPacket));
            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[Undercover]");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[Undercover]"
                });

            Session.Character.Undercover = !Session.Character.Undercover;
            ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                mapInstanceId: Session.CurrentMapInstance.MapInstanceId, mapX: Session.Character.PositionX,
                mapY: Session.Character.PositionY);
        }

        /// <summary>
        ///     $Unmute Command
        /// </summary>
        /// <param name="unmutePacket"></param>
        public void Unmute(UnmutePacket unmutePacket)
        {
            if (unmutePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Unmute]CharacterName: {unmutePacket.CharacterName}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Unmute]CharacterName: {unmutePacket.CharacterName}"
                    });

                var name = unmutePacket.CharacterName;
                var chara = DaoFactory.CharacterDao.LoadByName(name: name);
                if (chara != null)
                {
                    if (ServerManager.Instance.PenaltyLogs.Any(predicate: s =>
                        s.AccountId == chara.AccountId && s.Penalty == (byte)PenaltyType.Muted
                                                       && s.DateEnd > DateTime.Now))
                    {
                        var log = ServerManager.Instance.PenaltyLogs.Find(match: s =>
                            s.AccountId == chara.AccountId && s.Penalty == (byte)PenaltyType.Muted
                                                           && s.DateEnd > DateTime.Now);
                        if (log != null)
                        {
                            log.DateEnd = DateTime.Now.AddSeconds(value: -1);
                            Character.InsertOrUpdatePenalty(log: log);
                        }

                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "DONE"),
                            type: 10));
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "USER_NOT_MUTED"), type: 10));
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: UnmutePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $AddMonster Command
        /// </summary>
        /// <param name="backMobPacket"></param>
        public void BackMob(BackMobPacket backMobPacket)
        {
            if (backMobPacket != null)
            {
                if (!Session.HasCurrentMapInstance) return;

                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[BackMob]");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = "[BackMob]"
                    });

                var lastObject = Session.CurrentMapInstance.RemovedMobNpcList.LastOrDefault();

                if (lastObject is MapMonster mapMonster)
                {
                    var backMonst = new MapMonsterDto
                    {
                        MonsterVNum = mapMonster.MonsterVNum,
                        MapX = mapMonster.MapX,
                        MapY = mapMonster.MapY,
                        MapId = Session.Character.MapInstance.Map.MapId,
                        Position = Session.Character.Direction,
                        IsMoving = mapMonster.IsMoving,
                        MapMonsterId = ServerManager.Instance.GetNextMobId()
                    };
                    if (!DaoFactory.MapMonsterDao.DoesMonsterExist(mapMonsterId: backMonst.MapMonsterId))
                    {
                        DaoFactory.MapMonsterDao.Insert(mapMonster: backMonst);
                        if (DaoFactory.MapMonsterDao.LoadById(mapMonsterId: backMonst.MapMonsterId) is MapMonsterDto
                            monsterDTO)
                        {
                            var monster = new MapMonster(input: monsterDTO);
                            monster.Initialize(currentMapInstance: Session.CurrentMapInstance);
                            Session.CurrentMapInstance.AddMonster(monster: monster);
                            Session.CurrentMapInstance?.Broadcast(packet: monster.GenerateIn());
                            Session.CurrentMapInstance.RemovedMobNpcList.Remove(item: mapMonster);
                            Session.SendPacket(packet: Session.Character.GenerateSay(
                                message: $"MapMonster VNum: {backMonst.MonsterVNum} recovered sucessfully", type: 10));
                        }
                    }
                }
                else if (lastObject is MapNpc mapNpc)
                {
                    var backNpc = new MapNpcDto
                    {
                        NpcVNum = mapNpc.NpcVNum,
                        MapX = mapNpc.MapX,
                        MapY = mapNpc.MapY,
                        MapId = Session.Character.MapInstance.Map.MapId,
                        Position = Session.Character.Direction,
                        IsMoving = mapNpc.IsMoving,
                        MapNpcId = ServerManager.Instance.GetNextMobId()
                    };
                    if (!DaoFactory.MapNpcDao.DoesNpcExist(mapNpcId: backNpc.MapNpcId))
                    {
                        DaoFactory.MapNpcDao.Insert(npc: backNpc);
                        if (DaoFactory.MapNpcDao.LoadById(mapNpcId: backNpc.MapNpcId) is MapNpcDto npcDTO)
                        {
                            var npc = new MapNpc(input: npcDTO);
                            npc.Initialize(currentMapInstance: Session.CurrentMapInstance);
                            Session.CurrentMapInstance.AddNpc(npc: npc);
                            Session.CurrentMapInstance?.Broadcast(packet: npc.GenerateIn());
                            Session.CurrentMapInstance.RemovedMobNpcList.Remove(item: mapNpc);
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: $"MapNpc VNum: {backNpc.NpcVNum} recovered sucessfully",
                                    type: 10));
                        }
                    }
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: BackMobPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Unstuck Command
        /// </summary>
        /// <param name="unstuckPacket"></param>
        public void Unstuck(UnstuckPacket unstuckPacket)
        {
            if (unstuckPacket == null) throw new ArgumentNullException(paramName: nameof(unstuckPacket));
            if (Session?.Character != null)
            {
                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.UserCommand,
                        Packet = "[Unstuck]"
                    });

                if (Session.Character.Miniland == Session.Character.MapInstance)
                {
                    ServerManager.Instance.JoinMiniland(session: Session, minilandOwner: Session);
                }
                else if (!Session.Character.IsSeal
                         && !Session.CurrentMapInstance.MapInstanceType.Equals(obj: MapInstanceType
                             .TalentArenaMapInstance)
                         && !Session.CurrentMapInstance.MapInstanceType.Equals(obj: MapInstanceType.IceBreakerInstance))
                {
                    ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                        mapInstanceId: Session.Character.MapInstanceId, mapX: Session.Character.PositionX,
                        mapY: Session.Character.PositionY,
                        noAggroLoss: true);
                    Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2));
                }
            }
        }

        /// <summary>
        ///     $Upgrade Command
        /// </summary>
        /// <param name="upgradePacket"></param>
        public void Upgrade(UpgradeCommandPacket upgradePacket)
        {
            if (upgradePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Upgrade]Slot: {upgradePacket.Slot} Mode: {upgradePacket.Mode} Protection: {upgradePacket.Protection}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Upgrade]Slot: {upgradePacket.Slot} Mode: {upgradePacket.Mode} Protection: {upgradePacket.Protection}"
                    });

                if (upgradePacket.Slot >= 0)
                {
                    var wearableInstance =
                        Session.Character.Inventory.LoadBySlotAndType(slot: upgradePacket.Slot, type: 0);
                    wearableInstance?.UpgradeItem(session: Session, mode: upgradePacket.Mode,
                        protection: upgradePacket.Protection, isCommand: true);
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: UpgradeCommandPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $MapStat Command
        /// </summary>
        /// <param name="mapStatPacket"></param>
        public void MapStats(MapStatisticsPacket mapStatPacket)
        {
            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.GmCommand,
                    Packet = "[MapStat]"
                });

            // lower the boilerplate
            void SendMapStats(MapDto map, MapInstance mapInstance)
            {
                if (map != null && mapInstance != null)
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: "-------------MapData-------------", type: 10));
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: $"MapId: {map.MapId}\n" +
                                 $"MapMusic: {map.Music}\n" +
                                 $"MapName: {map.Name}\n" +
                                 $"MapShopAllowed: {map.ShopAllowed}", type: 10));
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: "---------------------------------", type: 10));
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: "---------MapInstanceData---------", type: 10));
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: $"MapInstanceId: {mapInstance.MapInstanceId}\n" +
                                 $"MapInstanceType: {mapInstance.MapInstanceType}\n" +
                                 $"MapMonsterCount: {mapInstance.Monsters.Count}\n" +
                                 $"MapNpcCount: {mapInstance.Npcs.Count}\n" +
                                 $"MapPortalsCount: {mapInstance.Portals.Count}\n" +
                                 $"MapInstanceUserShopCount: {mapInstance.UserShops.Count}\n" +
                                 $"SessionCount: {mapInstance.Sessions.Count()}\n" +
                                 $"MapInstanceXpRate: {mapInstance.XpRate}\n" +
                                 $"MapInstanceDropRate: {mapInstance.DropRate}\n" +
                                 $"MapInstanceMusic: {mapInstance.InstanceMusic}\n" +
                                 $"ShopsAllowed: {mapInstance.ShopAllowed}\n" +
                                 $"DropAllowed: {mapInstance.DropAllowed}\n" +
                                 $"IsPVP: {mapInstance.IsPvp}\n" +
                                 $"IsSleeping: {mapInstance.IsSleeping}\n" +
                                 $"Dance: {mapInstance.IsDancing}", type: 10));
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: "---------------------------------", type: 10));
                }
            }

            if (mapStatPacket != null)
            {
                if (mapStatPacket.MapId.HasValue)
                {
                    var map = DaoFactory.MapDao.LoadById(mapId: mapStatPacket.MapId.Value);
                    var mapInstance = ServerManager.GetMapInstanceByMapId(mapId: mapStatPacket.MapId.Value);
                    if (map != null && mapInstance != null) SendMapStats(map: map, mapInstance: mapInstance);
                }
                else if (Session.HasCurrentMapInstance)
                {
                    var map = DaoFactory.MapDao.LoadById(mapId: Session.CurrentMapInstance.Map.MapId);
                    if (map != null) SendMapStats(map: map, mapInstance: Session.CurrentMapInstance);
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: MapStatisticsPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Warn Command
        /// </summary>
        /// <param name="warningPacket"></param>
        public void Warn(WarningPacket warningPacket)
        {
            if (warningPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Warn]CharacterName: {warningPacket.CharacterName} Reason: {warningPacket.Reason}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Warn]CharacterName: {warningPacket.CharacterName} Reason: {warningPacket.Reason}"
                    });

                var characterName = warningPacket.CharacterName;
                var character = DaoFactory.CharacterDao.LoadByName(name: characterName);
                if (character != null)
                {
                    var session = ServerManager.Instance.GetSessionByCharacterName(name: characterName);
                    session?.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "WARNING"),
                            arg0: warningPacket.Reason)));
                    Character.InsertOrUpdatePenalty(log: new PenaltyLogDto
                    {
                        AccountId = character.AccountId,
                        Reason = warningPacket.Reason,
                        Penalty = PenaltyType.Warning,
                        DateStart = DateTime.Now,
                        DateEnd = DateTime.Now,
                        AdminName = Session.Character.Name
                    });
                    switch (DaoFactory.PenaltyLogDao.LoadByAccount(accountId: character.AccountId)
                        .Count(predicate: p => p.Penalty == PenaltyType.Warning))
                    {
                        case 1:
                            break;

                        case 2:
                            MuteMethod(characterName: characterName, reason: "Auto-Warning mute: 2 strikes",
                                duration: 30);
                            break;

                        case 3:
                            MuteMethod(characterName: characterName, reason: "Auto-Warning mute: 3 strikes",
                                duration: 60);
                            break;

                        case 4:
                            MuteMethod(characterName: characterName, reason: "Auto-Warning mute: 4 strikes",
                                duration: 720);
                            break;

                        case 5:
                            MuteMethod(characterName: characterName, reason: "Auto-Warning mute: 5 strikes",
                                duration: 1440);
                            break;

                        case 69:
                            BanMethod(characterName: characterName, duration: 7, reason: "LOL SIXTY NINE AMIRITE?");
                            break;

                        default:
                            MuteMethod(characterName: characterName, reason: "You've been THUNDERSTRUCK",
                                duration: 6969); // imagined number as for I = √(-1), complex z = a + bi
                            break;
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"), type: 10));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: WarningPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $WigColor Command
        /// </summary>
        /// <param name="wigColorPacket"></param>
        public void WigColor(WigColorPacket wigColorPacket)
        {
            if (wigColorPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[WigColor]Color: {wigColorPacket.Color}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[WigColor]Color: {wigColorPacket.Color}"
                    });

                var wig =
                    Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Hat,
                        type: InventoryType.Wear);
                if (wig != null)
                {
                    wig.Design = wigColorPacket.Color;
                    Session.SendPacket(packet: Session.Character.GenerateEq());
                    Session.SendPacket(packet: Session.Character.GenerateEquipment());
                    Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateIn());
                    Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateGidx());
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "NO_WIG"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: WigColorPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $XpRate Command
        /// </summary>
        /// <param name="xpRatePacket"></param>
        public void XpRate(XpRatePacket xpRatePacket)
        {
            if (xpRatePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[XpRate]Value: {xpRatePacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[XpRate]Value: {xpRatePacket.Value}"
                    });

                if (xpRatePacket.Value <= 1000)
                {
                    ServerManager.Instance.Configuration.RateXp = xpRatePacket.Value;

                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "XP_RATE_CHANGED"), type: 0));
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "WRONG_VALUE"), type: 0));
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: XpRatePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Zoom Command
        /// </summary>
        /// <param name="zoomPacket"></param>
        public void Zoom(ZoomPacket zoomPacket)
        {
            if (zoomPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Zoom]Value: {zoomPacket.Value}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[Zoom]Value: {zoomPacket.Value}"
                    });

                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateGuri(type: 15, argument: zoomPacket.Value,
                        callerId: Session.Character.CharacterId));
            }

            Session.Character.GenerateSay(message: ZoomPacket.ReturnHelp(), type: 10);
        }

        /// <summary>
        ///     $Act4 Command
        /// </summary>
        /// <param name="act4Packet"></param>
        public void Act4(Act4Packet act4Packet)
        {
            if (act4Packet != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[Act4]");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = "[Act4]"
                    });

                if (ServerManager.Instance.IsAct4Online())
                {
                    switch (Session.Character.Faction)
                    {
                        case FactionType.None:
                            ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: 145, mapX: 51,
                                mapY: 41);
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: "You need to be part of a faction to join Act 4"));
                            return;

                        case FactionType.Angel:
                            Session.Character.MapId = 130;
                            Session.Character.MapX = 12;
                            Session.Character.MapY = 40;
                            break;

                        case FactionType.Demon:
                            Session.Character.MapId = 131;
                            Session.Character.MapX = 12;
                            Session.Character.MapY = 40;
                            break;
                    }

                    Session.Character.ChangeChannel(ip: ServerManager.Instance.Configuration.Act4Ip,
                        port: ServerManager.Instance.Configuration.Act4Port, mode: 1);
                }
                else
                {
                    ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: 145, mapX: 51, mapY: 41);
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "ACT4_OFFLINE")));
                }
            }

            Session.Character.GenerateSay(message: Act4Packet.ReturnHelp(), type: 10);
        }

        /// <summary>
        ///     $LeaveAct4 Command
        /// </summary>
        /// <param name="leaveAct4Packet"></param>
        public void LeaveAct4(LeaveAct4Packet leaveAct4Packet)
        {
            if (leaveAct4Packet != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[LeaveAct4]");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = "[LeaveAct4]"
                    });

                if (Session.Character.Channel.ChannelId == 51)
                {
                    var connection =
                        CommunicationServiceClient.Instance.RetrieveOriginWorld(accountId: Session.Character.AccountId);
                    if (string.IsNullOrWhiteSpace(value: connection)) return;
                    Session.Character.MapId = 145;
                    Session.Character.MapX = 51;
                    Session.Character.MapY = 41;
                    var port = Convert.ToInt32(value: connection.Split(':')[1]);
                    Session.Character.ChangeChannel(ip: connection.Split(':')[0], port: port, mode: 3);
                }
            }

            Session.Character.GenerateSay(message: LeaveAct4Packet.ReturnHelp(), type: 10);
        }

        /// <summary>
        ///     $Miniland Command
        /// </summary>
        /// <param name="minilandPacket"></param>
        public void Miniland(MinilandPacket minilandPacket)
        {
            if (minilandPacket != null)
            {
                if (string.IsNullOrEmpty(value: minilandPacket.CharacterName))
                {
                    Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[Miniland]");

                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            PacketType = LogType.GmCommand,
                            Packet = "[Miniland]"
                        });

                    ServerManager.Instance.JoinMiniland(session: Session, minilandOwner: Session);
                }
                else
                {
                    var session = ServerManager.Instance.GetSessionByCharacterName(name: minilandPacket.CharacterName);
                    if (session != null)
                    {
                        Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                            data: $"[Miniland]CharacterName: {minilandPacket.CharacterName}");

                        if (ServerManager.Instance.Configuration.UseLogService)
                            LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                            {
                                Sender = Session.Character.Name,
                                SenderId = Session.Character.CharacterId,
                                PacketType = LogType.GmCommand,
                                Packet = $"[Miniland]CharacterName: {minilandPacket.CharacterName}"
                            });

                        ServerManager.Instance.JoinMiniland(session: Session, minilandOwner: session);
                    }
                }
            }

            Session.Character.GenerateSay(message: MinilandPacket.ReturnHelp(), type: 10);
        }

        /// <summary>
        ///     $Gogo Command
        /// </summary>
        /// <param name="gogoPacket"></param>
        public void Gogo(GogoPacket gogoPacket)
        {
            if (gogoPacket != null)
            {
                if (Session.Character.HasShopOpened || Session.Character.InExchangeOrTrade)
                    Session.Character.DisposeShopAndExchange();

                if (Session.Character.IsChangingMapInstance) return;

                if (Session.CurrentMapInstance != null)
                {
                    Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                        data:
                        $"[Gogo]MapId: {Session.CurrentMapInstance.Map.MapId} MapX: {gogoPacket.X} MapY: {gogoPacket.Y}");

                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            PacketType = LogType.GmCommand,
                            Packet =
                                $"[Gogo]MapId: {Session.CurrentMapInstance.Map.MapId} MapX: {gogoPacket.X} MapY: {gogoPacket.Y}"
                        });

                    if (gogoPacket.X == 0 && gogoPacket.Y == 0)
                        ServerManager.Instance.TeleportOnRandomPlaceInMap(session: Session,
                            guid: Session.CurrentMapInstance.MapInstanceId);
                    else
                        ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                            mapInstanceId: Session.CurrentMapInstance.MapInstanceId, mapX: gogoPacket.X,
                            mapY: gogoPacket.Y);
                }
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: GogoPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $MapReset Command
        /// </summary>
        /// <param name="mapResetPacket"></param>
        public void MapReset(MapResetPacket mapResetPacket)
        {
            if (mapResetPacket != null)
            {
                if (Session.Character.IsChangingMapInstance) return;
                if (Session.CurrentMapInstance != null)
                {
                    Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                        data: $"[MapReset]MapId: {Session.CurrentMapInstance.Map.MapId}");

                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            PacketType = LogType.GmCommand,
                            Packet = $"[MapReset]MapId: {Session.CurrentMapInstance.Map.MapId}"
                        });

                    var newMapInstance = ServerManager.ResetMapInstance(baseMapInstance: Session.CurrentMapInstance);

                    Parallel.ForEach(source: Session.CurrentMapInstance.Sessions, body: sess =>
                        ServerManager.Instance.ChangeMapInstance(characterId: sess.Character.CharacterId,
                            mapInstanceId: newMapInstance.MapInstanceId, mapX: sess.Character.PositionX,
                            mapY: sess.Character.PositionY));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: MapResetPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Drop Command
        /// </summary>
        /// <param name="dropPacket"></param>
        public void Drop(DropPacket dropPacket)
        {
            if (dropPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Drop]ItemVNum: {dropPacket.VNum} Amount: {dropPacket.Amount} Count: {dropPacket.Count} Time: {dropPacket.Time}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet =
                            $"[Drop]ItemVNum: {dropPacket.VNum} Amount: {dropPacket.Amount} Count: {dropPacket.Count} Time: {dropPacket.Time}"
                    });

                var vnum = dropPacket.VNum;
                var amount = dropPacket.Amount;
                if (amount < 1)
                    amount = 1;
                else if (amount > 999) amount = 999;
                var count = dropPacket.Count;
                if (count < 1) count = 1;
                var time = dropPacket.Time;

                var instance = Session.CurrentMapInstance;

                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 0)).Subscribe(onNext: observer =>
                {
                    {
                        for (var i = 0; i < count; i++)
                        {
                            var droppedItem = new MonsterMapItem(x: Session.Character.PositionX,
                                y: Session.Character.PositionY, itemVNum: vnum, amount: amount);
                            instance.DroppedList[key: droppedItem.TransportId] = droppedItem;
                            instance.Broadcast(
                                packet:
                                $"drop {droppedItem.ItemVNum} {droppedItem.TransportId} {droppedItem.PositionX} {droppedItem.PositionY} {(droppedItem.GoldAmount > 1 ? droppedItem.GoldAmount : droppedItem.Amount)} 0 -1");

                            Thread.Sleep(millisecondsTimeout: time * 1000 / count);
                        }
                    }
                });
            }
        }

        /// <summary>
        ///     $ChangeShopName Packet
        /// </summary>
        /// <param name="changeShopNamePacket"></param>
        public void ChangeShopName(ChangeShopNamePacket changeShopNamePacket)
        {
            if (Session.HasCurrentMapInstance)
            {
                if (!string.IsNullOrEmpty(value: changeShopNamePacket.Name))
                {
                    if (Session.CurrentMapInstance.GetNpc(mapNpcId: Session.Character.LastNpcMonsterId) is MapNpc npc)
                    {
                        if (npc.Shop is Shop shop)
                        {
                            Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                                data: $"[ChangeShopName]ShopId: {shop.ShopId} Name: {changeShopNamePacket.Name}");

                            if (ServerManager.Instance.Configuration.UseLogService)
                                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                {
                                    Sender = Session.Character.Name,
                                    SenderId = Session.Character.CharacterId,
                                    PacketType = LogType.GmCommand,
                                    Packet = $"[ChangeShopName]ShopId: {shop.ShopId} Name: {changeShopNamePacket.Name}"
                                });

                            if (DaoFactory.ShopDao.LoadById(shopId: shop.ShopId) is ShopDto shopDTO)
                            {
                                shop.Name = changeShopNamePacket.Name;
                                shopDTO.Name = changeShopNamePacket.Name;
                                DaoFactory.ShopDao.Update(shop: ref shopDTO);

                                Session.CurrentMapInstance.Broadcast(
                                    packet:
                                    $"shop 2 {npc.MapNpcId} {npc.Shop.ShopId} {npc.Shop.MenuType} {npc.Shop.ShopType} {npc.Shop.Name}");
                            }
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "NPCMONSTER_NOT_FOUND"),
                                type: 11));
                    }
                }
                else
                {
                    Session.SendPacket(packet: Session.Character.GenerateSay(message: ChangeShopNamePacket.ReturnHelp(),
                        type: 10));
                }
            }
        }

        /// <summary>
        ///     $CustomNpcMonsterName Packet
        /// </summary>
        /// <param name="changeNpcMonsterNamePacket"></param>
        public void CustomNpcMonsterName(ChangeNpcMonsterNamePacket changeNpcMonsterNamePacket)
        {
            if (Session.HasCurrentMapInstance)
            {
                if (Session.CurrentMapInstance.GetNpc(mapNpcId: Session.Character.LastNpcMonsterId) is MapNpc npc)
                {
                    Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                        data: $"[CustomNpcName]MapNpcId: {npc.MapNpcId} Name: {changeNpcMonsterNamePacket.Name}");

                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            PacketType = LogType.GmCommand,
                            Packet = $"[CustomNpcName]MapNpcId: {npc.MapNpcId} Name: {changeNpcMonsterNamePacket.Name}"
                        });

                    if (DaoFactory.MapNpcDao.LoadById(mapNpcId: npc.MapNpcId) is MapNpcDto npcDTO)
                    {
                        npc.Name = changeNpcMonsterNamePacket.Name;
                        npcDTO.Name = changeNpcMonsterNamePacket.Name;
                        DaoFactory.MapNpcDao.Update(mapNpc: ref npcDTO);

                        Session.CurrentMapInstance.Broadcast(packet: npc.GenerateIn());
                    }
                }
                else if (Session.CurrentMapInstance.GetMonsterById(mapMonsterId: Session.Character.LastNpcMonsterId) is
                    MapMonster
                    monster)
                {
                    Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                        data:
                        $"[CustomNpcName]MapMonsterId: {monster.MapMonsterId} Name: {changeNpcMonsterNamePacket.Name}");

                    if (ServerManager.Instance.Configuration.UseLogService)
                        LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                        {
                            Sender = Session.Character.Name,
                            SenderId = Session.Character.CharacterId,
                            PacketType = LogType.GmCommand,
                            Packet =
                                $"[CustomNpcName]MapMonsterId: {monster.MapMonsterId} Name: {changeNpcMonsterNamePacket.Name}"
                        });

                    if (DaoFactory.MapMonsterDao.LoadById(mapMonsterId: monster.MapMonsterId) is MapMonsterDto
                        monsterDTO)
                    {
                        monster.Name = changeNpcMonsterNamePacket.Name;
                        monsterDTO.Name = changeNpcMonsterNamePacket.Name;
                        DaoFactory.MapMonsterDao.Update(mapMonster: ref monsterDTO);

                        Session.CurrentMapInstance.Broadcast(packet: monster.GenerateIn());
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "NPCMONSTER_NOT_FOUND"), type: 11));
                }
            }
        }

        /// <summary>
        ///     $AddQuest
        /// </summary>
        /// <param name="addQuestPacket"></param>
        public void AddQuest(AddQuestPacket addQuestPacket)
        {
            if (addQuestPacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[AddQuest]QuestId: {addQuestPacket.QuestId}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[AddQuest]QuestId: {addQuestPacket.QuestId}"
                    });

                if (ServerManager.Instance.Quests.Any(predicate: q => q.QuestId == addQuestPacket.QuestId))
                {
                    Session.Character.AddQuest(questId: addQuestPacket.QuestId);
                    return;
                }

                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: "This Quest doesn't exist", type: 11));
            }
        }


        /// <summary>
        ///     $ClassPack
        /// </summary>
        /// <param name="classPackPacket"></param>
        public void ClassPack(ClassPackPacket classPackPacket)
        {
            if (classPackPacket != null)
            {
                if (classPackPacket.Class < 1 || classPackPacket.Class > 3)
                {
                    Session.SendPacket(packet: Session.Character.GenerateSay(message: "Invalid class", type: 11));
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: ClassPackPacket.ReturnHelp(), type: 10));
                    return;
                }

                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[ClassPack]Class: {classPackPacket.Class}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GmCommand,
                        Packet = $"[ClassPack]Class: {classPackPacket.Class}"
                    });

                switch (classPackPacket.Class)
                {
                    case 1:
                        Session.Character.Inventory.AddNewToInventory(vnum: 4075);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4076);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4129);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4130);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4131);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4132);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1685, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1686, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5087, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5203, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5372, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5431, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5432, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5498, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5499, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5553, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5560, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5591, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5837, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4875, amount: 1, Upgrade: 14);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4873, amount: 1, Upgrade: 14);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1012, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1012, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1244, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1244, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2072, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2071, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2070, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2160, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4138);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4146);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4142);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4150);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4353);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4124);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4172);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4183);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4187);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4283);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4285);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4177);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4179);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4244);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4252);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4256);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4248);
                        Session.Character.Inventory.AddNewToInventory(vnum: 3116);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1277, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1274, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1280, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2419, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1914);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1296, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5916, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 3001);
                        Session.Character.Inventory.AddNewToInventory(vnum: 3003);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4490);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4699);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4099, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 900, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 907, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 908, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4883, amount: 1, type: null, Rare: 7,
                            Upgrade: 10);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4889, amount: 1, type: null, Rare: 7,
                            Upgrade: 10);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4895, amount: 1, type: null, Rare: 7,
                            Upgrade: 10);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4371);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4353);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4277);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4309);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4271);
                        Session.Character.Inventory.AddNewToInventory(vnum: 901, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 902, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 909, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 910, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4500, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4497, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4493, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4489, amount: 1, Upgrade: 15);
                        break;
                    case 2:
                        Session.Character.Inventory.AddNewToInventory(vnum: 4075);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4076);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4129);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4130);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4131);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4132);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1685, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1686, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5087, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5203, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5372, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5431, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5432, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5498, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5499, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5553, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5560, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5591, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5837, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4875, amount: 1, Upgrade: 14);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4873, amount: 1, Upgrade: 14);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1012, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1012, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1244, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1244, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2072, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2071, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2070, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2160, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4138);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4146);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4142);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4150);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4353);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4124);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4172);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4183);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4187);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4283);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4285);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4177);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4179);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4244);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4252);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4256);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4248);
                        Session.Character.Inventory.AddNewToInventory(vnum: 3116);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1277, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1274, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1280, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2419, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1914);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1296, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5916, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 3001);
                        Session.Character.Inventory.AddNewToInventory(vnum: 3003);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4490);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4699);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4099, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 900, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 907, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 908, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4885, amount: 1, type: null, Rare: 7,
                            Upgrade: 10);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4890, amount: 1, type: null, Rare: 7,
                            Upgrade: 10);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4897, amount: 1, type: null, Rare: 7,
                            Upgrade: 10);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4372);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4310);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4354);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4279);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4273);
                        Session.Character.Inventory.AddNewToInventory(vnum: 903, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 904, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 911, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 912, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4501, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4498, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4488, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4492, amount: 1, Upgrade: 15);
                        break;
                    case 3:
                        Session.Character.Inventory.AddNewToInventory(vnum: 4075);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4076);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4129);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4130);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4131);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4132);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1685, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1686, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5087, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5203, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5372, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5431, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5432, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5498, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5499, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5553, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5560, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5591, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5837, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4875, amount: 1, Upgrade: 14);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4873, amount: 1, Upgrade: 14);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1012, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1012, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1244, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1244, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2072, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2071, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2070, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2160, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4138);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4146);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4142);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4150);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4353);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4124);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4172);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4183);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4187);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4283);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4285);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4177);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4179);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4244);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4252);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4256);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4248);
                        Session.Character.Inventory.AddNewToInventory(vnum: 3116);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1277, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1274, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1280, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 2419, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1914);
                        Session.Character.Inventory.AddNewToInventory(vnum: 1296, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 5916, amount: 999);
                        Session.Character.Inventory.AddNewToInventory(vnum: 3001);
                        Session.Character.Inventory.AddNewToInventory(vnum: 3003);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4490);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4699);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4099, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 900, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 907, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 908, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4887, amount: 1, type: null, Rare: 7,
                            Upgrade: 10);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4892, amount: 1, type: null, Rare: 7,
                            Upgrade: 10);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4899, amount: 1, type: null, Rare: 7,
                            Upgrade: 10);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4311);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4373);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4281);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4355);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4275);
                        Session.Character.Inventory.AddNewToInventory(vnum: 905, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 906, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 913, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 914, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4502, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4499, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4491, amount: 1, Upgrade: 15);
                        Session.Character.Inventory.AddNewToInventory(vnum: 4487, amount: 1, Upgrade: 15);
                        break;
                }
            }
            else
            {
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: ClassPackPacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     $Home Command
        /// </summary>
        /// <param name="homePacket"></param>
        public void Home(HomePacket homePacket)
        {
            if (homePacket != null)
            {
                Logger.LogUserEvent(logEvent: "GMCOMMAND", caller: Session.GenerateIdentity(), data: "[Home]");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.ModeratorCommand,
                        Packet = "[Home]"
                    });

                ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: 0, mapX: 24, mapY: 24);
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: HomePacket.ReturnHelp(), type: 10));
            }
        }

        /// <summary>
        ///     private addMate method
        /// </summary>
        /// <param name="vnum"></param>
        /// <param name="level"></param>
        /// <param name="mateType"></param>
        void AddMate(short vnum, byte level, MateType mateType)
        {
            var mateNpc = ServerManager.GetNpcMonster(npcVNum: vnum);
            if (Session.CurrentMapInstance == Session.Character.Miniland && mateNpc != null)
            {
                level = level == 0 ? (byte)1 : level;
                var mate = new Mate(owner: Session.Character, npcMonster: mateNpc, level: level, matetype: mateType);
                Session.Character.AddPet(mate: mate);
            }
            else
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "NOT_IN_MINILAND"), type: 0));
            }
        }

        /*/// <summary>
        /// $ReloadSI Command
        /// </summary>
        /// <param name="reloadSIPacket"></param>
        public void ReloadSI(ReloadSIPacket reloadSIPacket)
        {
            if (reloadSIPacket != null)
            {
                Logger.LogUserEvent("GMCOMMAND", Session.GenerateIdentity(), $"[ReloadSI]");

                if (ServerManager.Instance.Configuration.UseLogService)
                {
                    LogServiceClient.Instance.LogPacket(new PacketLogEntry()
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        PacketType = LogType.GMCommand,
                        Packet = $"[ReloadSI]"
                    });
                }

                ServerManager.Instance.LoadScriptedInstances();
                Session.SendPacket(Session.Character.GenerateSay(Language.Instance.GetMessageFromKey("DONE"), 10));
            }
            else
            {
                Session.SendPacket(Session.Character.GenerateSay(ReloadSIPacket.ReturnHelp(), 10));
            }
        }*/

        /// <summary>
        ///     private add portal command
        /// </summary>
        /// <param name="destinationMapId"></param>
        /// <param name="destinationX"></param>
        /// <param name="destinationY"></param>
        /// <param name="type"></param>
        /// <param name="insertToDatabase"></param>
        void AddPortal(short destinationMapId, short destinationX, short destinationY, short type,
            bool insertToDatabase)
        {
            if (Session.HasCurrentMapInstance)
            {
                var portal = new Portal
                {
                    SourceMapId = Session.Character.MapId,
                    SourceX = Session.Character.PositionX,
                    SourceY = Session.Character.PositionY,
                    DestinationMapId = destinationMapId,
                    DestinationX = destinationX,
                    DestinationY = destinationY,
                    DestinationMapInstanceId = insertToDatabase ? Guid.Empty :
                        destinationMapId == 20000 ? Session.Character.Miniland.MapInstanceId : Guid.Empty,
                    Type = type
                };
                if (insertToDatabase) DaoFactory.PortalDao.Insert(portal: portal);

                Session.CurrentMapInstance.Portals.Add(item: portal);
                Session.CurrentMapInstance?.Broadcast(packet: portal.GenerateGp());
            }
        }

        /// <summary>
        ///     private ban method
        /// </summary>
        /// <param name="characterName"></param>
        /// <param name="duration"></param>
        /// <param name="reason"></param>
        void BanMethod(string characterName, int duration, string reason)
        {
            var character = DaoFactory.CharacterDao.LoadByName(name: characterName);
            if (character != null)
            {
                ServerManager.Instance.Kick(characterName: characterName);
                var log = new PenaltyLogDto
                {
                    AccountId = character.AccountId,
                    Reason = reason?.Trim(),
                    Penalty = PenaltyType.Banned,
                    DateStart = DateTime.Now,
                    DateEnd = duration == 0 ? DateTime.Now.AddYears(value: 15) : DateTime.Now.AddDays(value: duration),
                    AdminName = Session.Character.Name
                };
                Character.InsertOrUpdatePenalty(log: log);
                ServerManager.Instance.BannedCharacters.Add(item: character.CharacterId);
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(
                    message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"),
                    type: 10));
            }
        }

        /// <summary>
        ///     private mute method
        /// </summary>
        /// <param name="characterName"></param>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        void MuteMethod(string characterName, string reason, int duration)
        {
            var characterToMute = DaoFactory.CharacterDao.LoadByName(name: characterName);
            if (characterToMute != null)
            {
                var session = ServerManager.Instance.GetSessionByCharacterName(name: characterName);
                if (session?.Character.IsMuted() == false)
                    session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTED_PLURAL"),
                            arg0: reason, arg1: duration)));

                var log = new PenaltyLogDto
                {
                    AccountId = characterToMute.AccountId,
                    Reason = reason,
                    Penalty = PenaltyType.Muted,
                    DateStart = DateTime.Now,
                    DateEnd = DateTime.Now.AddMinutes(value: duration),
                    AdminName = Session.Character.Name
                };
                Character.InsertOrUpdatePenalty(log: log);
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "DONE"),
                        type: 10));
            }
            else
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(
                    message: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"),
                    type: 10));
            }
        }

        /// <summary>
        ///     Helper method used for sending stats of desired character
        /// </summary>
        /// <param name="characterDto"></param>
        void SendStats(CharacterDto characterDto)
        {
            Session.SendPacket(packet: Session.Character.GenerateSay(message: "----- CHARACTER -----", type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(message: $"Name: {characterDto.Name}", type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"Id: {characterDto.CharacterId}", type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"State: {characterDto.State}", type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"Gender: {characterDto.Gender}", type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"Class: {characterDto.Class}", type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"Level: {characterDto.Level}", type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"JobLevel: {characterDto.JobLevel}", type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"HeroLevel: {characterDto.HeroLevel}", type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(message: $"Gold: {characterDto.Gold}", type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"Bio: {characterDto.Biography}", type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"MapId: {characterDto.MapId}", type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(message: $"MapX: {characterDto.MapX}", type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(message: $"MapY: {characterDto.MapY}", type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(message: $"Reputation: {characterDto.Reputation}",
                type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"Dignity: {characterDto.Dignity}", type: 13));
            Session.SendPacket(
                packet: Session.Character.GenerateSay(message: $"Rage: {characterDto.RagePoint}", type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(message: $"Compliment: {characterDto.Compliment}",
                type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(
                message:
                $"Fraction: {(characterDto.Faction == FactionType.Demon ? Language.Instance.GetMessageFromKey(key: "DEMON") : Language.Instance.GetMessageFromKey(key: "ANGEL"))}",
                type: 13));
            Session.SendPacket(packet: Session.Character.GenerateSay(message: "----- --------- -----", type: 13));
            var account = DaoFactory.AccountDao.LoadById(accountId: characterDto.AccountId);
            if (account != null)
            {
                Session.SendPacket(packet: Session.Character.GenerateSay(message: "----- ACCOUNT -----", type: 13));
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: $"Id: {account.AccountId}", type: 13));
                Session.SendPacket(packet: Session.Character.GenerateSay(message: $"Name: {account.Name}", type: 13));
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: $"Authority: {account.Authority}", type: 13));
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(message: $"RegistrationIP: {account.RegistrationIp}",
                        type: 13));
                Session.SendPacket(packet: Session.Character.GenerateSay(message: $"Email: {account.Email}", type: 13));
                Session.SendPacket(packet: Session.Character.GenerateSay(message: "----- ------- -----", type: 13));
                IEnumerable<PenaltyLogDto> penaltyLogs = ServerManager.Instance.PenaltyLogs
                    .Where(predicate: s => s.AccountId == account.AccountId).ToList();
                var penalty = penaltyLogs.LastOrDefault(predicate: s => s.DateEnd > DateTime.Now);
                Session.SendPacket(packet: Session.Character.GenerateSay(message: "----- PENALTY -----", type: 13));
                if (penalty != null)
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: $"Type: {penalty.Penalty}", type: 13));
                    Session.SendPacket(packet: Session.Character.GenerateSay(message: $"AdminName: {penalty.AdminName}",
                        type: 13));
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: $"Reason: {penalty.Reason}", type: 13));
                    Session.SendPacket(packet: Session.Character.GenerateSay(message: $"DateStart: {penalty.DateStart}",
                        type: 13));
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(message: $"DateEnd: {penalty.DateEnd}", type: 13));
                }

                Session.SendPacket(
                    packet: Session.Character.GenerateSay(
                        message: $"Bans: {penaltyLogs.Count(predicate: s => s.Penalty == PenaltyType.Banned)}",
                        type: 13));
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(
                        message: $"Mutes: {penaltyLogs.Count(predicate: s => s.Penalty == PenaltyType.Muted)}",
                        type: 13));
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(
                        message: $"Warnings: {penaltyLogs.Count(predicate: s => s.Penalty == PenaltyType.Warning)}",
                        type: 13));
                Session.SendPacket(packet: Session.Character.GenerateSay(message: "----- ------- -----", type: 13));
            }

            Session.SendPacket(packet: Session.Character.GenerateSay(message: "----- SESSION -----", type: 13));
            foreach (var connection in CommunicationServiceClient.Instance.RetrieveOnlineCharacters(
                characterId: characterDto
                    .CharacterId))
                if (connection != null)
                {
                    var character = DaoFactory.CharacterDao.LoadById(characterId: connection[0]);
                    if (character != null)
                    {
                        Session.SendPacket(
                            packet: Session.Character.GenerateSay(message: $"Character Name: {character.Name}",
                                type: 13));
                        Session.SendPacket(packet: Session.Character.GenerateSay(message: $"ChannelId: {connection[1]}",
                            type: 13));
                        Session.SendPacket(packet: Session.Character.GenerateSay(message: "-----", type: 13));
                    }
                }

            Session.SendPacket(packet: Session.Character.GenerateSay(message: "----- ------------ -----", type: 13));
        }

        #endregion
    }
}