﻿using OpenNos.Core.Handling;
using OpenNos.GameObject;

namespace OpenNos.Handler
{
    public class CustomPacketHandler : IPacketHandler
    {
        #region Instantiation

        public CustomPacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        // Your custom packet code written here. Put Packet Definitions in OpenNos.GameObject/Packets/CustomPackets

        #endregion
    }
}