﻿using System;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.Core;
using OpenNos.Core.Handling;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Log.Networking;
using OpenNos.Log.Shared;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;

namespace OpenNos.Handler
{
    public class BazaarPacketHandler : IPacketHandler
    {
        #region Instantiation

        public BazaarPacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     c_buy packet
        /// </summary>
        /// <param name="cBuyPacket"></param>
        public void BuyBazaar(CBuyPacket cBuyPacket)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            var bz = DaoFactory.BazaarItemDao.LoadById(bazaarItemId: cBuyPacket.BazaarId);
            if (bz != null && cBuyPacket.Amount > 0)
            {
                var price = cBuyPacket.Amount * bz.Price;

                if (Session.Character.Gold >= price)
                {
                    var bzcree = new BazaarItemLink { BazaarItem = bz };
                    if (DaoFactory.CharacterDao.LoadById(characterId: bz.SellerId) != null)
                    {
                        bzcree.Owner = DaoFactory.CharacterDao.LoadById(characterId: bz.SellerId)?.Name;
                        bzcree.Item =
                            new ItemInstance(input: DaoFactory.ItemInstanceDao.LoadById(id: bz.ItemInstanceId));
                    }
                    else
                    {
                        return;
                    }

                    if (cBuyPacket.Amount <= bzcree.Item.Amount)
                    {
                        if (!Session.Character.Inventory.CanAddItem(itemVnum: bzcree.Item.ItemVNum))
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                    type: 0));
                            return;
                        }

                        if (bzcree.Item != null)
                        {
                            if (bz.IsPackage && cBuyPacket.Amount != bz.Amount) return;

                            var bzitemdto =
                                DaoFactory.ItemInstanceDao.LoadById(id: bzcree.BazaarItem.ItemInstanceId);
                            if (bzitemdto.Amount < cBuyPacket.Amount) return;

                            // Edit this soo we dont generate new guid every single time we take something out.
                            var newBz = bzcree.Item.DeepCopy();
                            newBz.Id = Guid.NewGuid();
                            newBz.Amount = cBuyPacket.Amount;
                            newBz.Type = newBz.Item.Type;
                            var newInv = Session.Character.Inventory.AddToInventory(newItem: newBz);

                            if (newInv.Count > 0)
                            {
                                bzitemdto.Amount -= cBuyPacket.Amount;
                                Session.Character.Gold -= price;
                                Session.SendPacket(packet: Session.Character.GenerateGold());
                                DaoFactory.ItemInstanceDao.InsertOrUpdate(dto: bzitemdto);
                                ServerManager.Instance.BazaarRefresh(bazaarItemId: bzcree.BazaarItem.BazaarItemId);
                                Session.SendPacket(
                                    packet:
                                    $"rc_buy 1 {bzcree.Item.Item.VNum} {bzcree.Owner} {cBuyPacket.Amount} {cBuyPacket.Price} 0 0 0");

                                Session.SendPacket(packet: Session.Character.GenerateSay(
                                    message:
                                    $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {bzcree.Item.Item.Name} x {cBuyPacket.Amount}",
                                    type: 10));

                                CommunicationServiceClient.Instance.SendMessageToCharacter(
                                    message: new ScsCharacterMessage
                                    {
                                        DestinationCharacterId = bz.SellerId,
                                        SourceWorldId = ServerManager.Instance.WorldId,
                                        Message = StaticPacketHelper.Say(type: 1, callerId: bz.SellerId,
                                            secondaryType: 12,
                                            message: string.Format(
                                                format: Language.Instance.GetMessageFromKey(key: "BAZAAR_ITEM_SOLD"),
                                                arg0: cBuyPacket.Amount, arg1: bzcree.Item.Item.Name)),
                                        Type = MessageType.Other
                                    });

                                Logger.LogUserEvent(logEvent: "BAZAAR_BUY", caller: Session.GenerateIdentity(),
                                    data:
                                    $"BazaarId: {cBuyPacket.BazaarId} VNum: {cBuyPacket.VNum} Amount: {cBuyPacket.Amount} Price: {cBuyPacket.Price}");

                                if (ServerManager.Instance.Configuration.UseLogService)
                                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                    {
                                        Sender = bzcree.Owner,
                                        SenderId = bz.SellerId,
                                        Receiver = Session.Character.Name,
                                        ReceiverId = Session.Character.CharacterId,
                                        PacketType = LogType.BazaarSell,
                                        Packet =
                                            $"[BAZAAR_BUY]BazaarId: {cBuyPacket.BazaarId} OldIIId: {bzcree.Item.Id} NewIIId: {newBz.Id} VNum: {cBuyPacket.VNum} Amount: {cBuyPacket.Amount} Rare: {newBz.Rare} Upgrade: {newBz.Upgrade} Price: {cBuyPacket.Price}"
                                    });
                            }
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateModal(
                                message: Language.Instance.GetMessageFromKey(key: "STATE_CHANGED"), type: 1));
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"), type: 10));
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateModal(
                            message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"), type: 1));
                }
            }
            else
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateModal(
                        message: Language.Instance.GetMessageFromKey(key: "STATE_CHANGED"), type: 1));
            }
        }

        /// <summary>
        ///     c_scalc packet
        /// </summary>
        /// <param name="cScalcPacket"></param>
        public void GetBazaar(CScalcPacket cScalcPacket)
        {
            if (!Session.Character.CanUseNosBazaar())
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "INFO_BAZAAR")));
                return;
            }

            SpinWait.SpinUntil(condition: () => !ServerManager.Instance.InBazaarRefreshMode);

            var bazaarItemDTO = DaoFactory.BazaarItemDao.LoadById(bazaarItemId: cScalcPacket.BazaarId);

            if (bazaarItemDTO != null)
            {
                var itemInstanceDTO = DaoFactory.ItemInstanceDao.LoadById(id: bazaarItemDTO.ItemInstanceId);

                if (itemInstanceDTO == null) return;

                var itemInstance = new ItemInstance(input: itemInstanceDTO);

                if (itemInstance == null) return;

                if (bazaarItemDTO.SellerId != Session.Character.CharacterId) return;

                if ((bazaarItemDTO.DateStart.AddHours(value: bazaarItemDTO.Duration)
                    .AddDays(value: bazaarItemDTO.MedalUsed ? 30 : 7) - DateTime.Now).TotalMinutes <= 0) return;

                var soldAmount = bazaarItemDTO.Amount - itemInstance.Amount;
                var taxes = bazaarItemDTO.MedalUsed ? 0 : (long)(bazaarItemDTO.Price * 0.10 * soldAmount);
                var price = bazaarItemDTO.Price * soldAmount - taxes;

                var name = itemInstance.Item?.Name ?? "None";

                if (itemInstance.Amount == 0 || Session.Character.Inventory.CanAddItem(itemVnum: itemInstance.ItemVNum))
                {
                    if (Session.Character.Gold + price <= ServerManager.Instance.Configuration.MaxGold)
                    {
                        Session.Character.Gold += price;
                        Session.SendPacket(packet: Session.Character.GenerateGold());
                        Session.SendPacket(packet: Session.Character.GenerateSay(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "REMOVE_FROM_BAZAAR"), arg0: price),
                            type: 10));

                        // Edit this soo we dont generate new guid every single time we take something out.
                        if (itemInstance.Amount != 0)
                        {
                            var newItemInstance = itemInstance.DeepCopy();
                            newItemInstance.Id = Guid.NewGuid();
                            newItemInstance.Type = newItemInstance.Item.Type;
                            Session.Character.Inventory.AddToInventory(newItem: newItemInstance);
                        }

                        Session.SendPacket(packet: UserInterfaceHelper.GenerateBazarRecollect(
                            pricePerUnit: bazaarItemDTO.Price, soldAmount: soldAmount,
                            amount: bazaarItemDTO.Amount, taxes: taxes, totalPrice: price, name: name));

                        Logger.LogUserEvent(logEvent: "BAZAAR_REMOVE", caller: Session.GenerateIdentity(),
                            data:
                            $"BazaarId: {cScalcPacket.BazaarId}, IId: {itemInstance.Id} VNum: {itemInstance.ItemVNum} Amount: {bazaarItemDTO.Amount} RemainingAmount: {itemInstance.Amount} Price: {bazaarItemDTO.Price}");

                        if (DaoFactory.BazaarItemDao.LoadById(bazaarItemId: bazaarItemDTO.BazaarItemId) != null)
                            DaoFactory.BazaarItemDao.Delete(bazaarItemId: bazaarItemDTO.BazaarItemId);

                        DaoFactory.ItemInstanceDao.Delete(id: itemInstance.Id);

                        Session.Character.Inventory.RemoveItemFromInventory(id: itemInstance.Id,
                            amount: itemInstance.Amount);

                        ServerManager.Instance.BazaarRefresh(bazaarItemId: bazaarItemDTO.BazaarItemId);

                        Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000))
                            .Subscribe(onNext: o => RefreshPersonalBazarList(csListPacket: new CSListPacket()));
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "MAX_GOLD"), type: 0));
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateBazarRecollect(
                            pricePerUnit: bazaarItemDTO.Price, soldAmount: 0,
                            amount: bazaarItemDTO.Amount, taxes: 0, totalPrice: 0, name: name));
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE")));
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateBazarRecollect(
                        pricePerUnit: bazaarItemDTO.Price, soldAmount: 0,
                        amount: bazaarItemDTO.Amount, taxes: 0, totalPrice: 0, name: name));
                }
            }
            else
            {
                Session.SendPacket(packet: UserInterfaceHelper.GenerateBazarRecollect(pricePerUnit: 0, soldAmount: 0,
                    amount: 0, taxes: 0, totalPrice: 0, name: "None"));
            }
        }

        /// <summary>
        ///     c_skill packet
        /// </summary>
        /// <param name="cSkillPacket"></param>
        public void OpenBazaar(CSkillPacket cSkillPacket)
        {
            if (cSkillPacket == null) throw new ArgumentNullException(paramName: nameof(cSkillPacket));
            SpinWait.SpinUntil(condition: () => !ServerManager.Instance.InBazaarRefreshMode);

            var medal = Session.Character.StaticBonusList.Find(match: s =>
                s.StaticBonusType == StaticBonusType.BazaarMedalGold ||
                s.StaticBonusType == StaticBonusType.BazaarMedalSilver);

            if (medal != null)
            {
                var medalType = medal.StaticBonusType == StaticBonusType.BazaarMedalGold
                    ? MedalType.Gold
                    : MedalType.Silver;

                var time = (int)(medal.DateEnd - DateTime.Now).TotalHours;

                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "NOTICE_BAZAAR"),
                    type: 0));
                Session.SendPacket(packet: $"wopen 32 {(byte)medalType} {time}");
            }
            else
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "INFO_BAZAAR")));
            }
        }

        /// <summary>
        ///     c_blist packet
        /// </summary>
        /// <param name="cbListPacket"></param>
        public void RefreshBazarList(CBListPacket cbListPacket)
        {
            if (!Session.Character.CanUseNosBazaar())
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "INFO_BAZAAR")));
                return;
            }

            SpinWait.SpinUntil(condition: () => !ServerManager.Instance.InBazaarRefreshMode);
            Session.SendPacket(packet: UserInterfaceHelper.GenerateRcbList(packet: cbListPacket));
        }

        /// <summary>
        ///     c_slist packet
        /// </summary>
        /// <param name="csListPacket"></param>
        public void RefreshPersonalBazarList(CSListPacket csListPacket)
        {
            if (!Session.Character.CanUseNosBazaar())
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "INFO_BAZAAR")));
                return;
            }

            SpinWait.SpinUntil(condition: () => !ServerManager.Instance.InBazaarRefreshMode);
            Session.SendPacket(packet: Session.Character.GenerateRCSList(packet: csListPacket));
        }

        /// <summary>
        ///     c_reg packet
        /// </summary>
        /// <param name="cRegPacket"></param>
        public void SellBazaar(CRegPacket cRegPacket)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            if (!Session.Character.CanUseNosBazaar())
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "INFO_BAZAAR")));
                return;
            }

            SpinWait.SpinUntil(condition: () => !ServerManager.Instance.InBazaarRefreshMode);
            var medal = Session.Character.StaticBonusList.Find(match: s =>
                s.StaticBonusType == StaticBonusType.BazaarMedalGold
                || s.StaticBonusType == StaticBonusType.BazaarMedalSilver);

            var price = cRegPacket.Price * cRegPacket.Amount;
            var taxmax = price > 100000 ? price / 200 : 500;
            var taxmin = price >= 4000
                ? 60 + (price - 4000) / 2000 * 30 > 10000 ? 10000 : 60 + (price - 4000) / 2000 * 30
                : 50;
            var tax = medal == null ? taxmax : taxmin;
            var maxGold = ServerManager.Instance.Configuration.MaxGold;
            if (Session.Character.Gold < tax || cRegPacket.Amount <= 0
                                             || Session.Character.ExchangeInfo?.ExchangeList.Count > 0 ||
                                             Session.Character.IsShopping)
                return;

            var it = Session.Character.Inventory.LoadBySlotAndType(slot: cRegPacket.Slot,
                type: cRegPacket.Inventory == 4 ? 0 : (InventoryType)cRegPacket.Inventory);

            if (it == null || !it.Item.IsSoldable || !it.Item.IsTradable || it.IsBound) return;

            if (Session.Character.Inventory.CountBazaarItems()
                >= 10 * (medal == null ? 2 : 10))
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "LIMIT_EXCEEDED"), type: 0));
                return;
            }

            if (cRegPacket.Price >= (medal == null ? 1000000 : maxGold))
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "PRICE_EXCEEDED"), type: 0));
                return;
            }

            if (cRegPacket.Price <= 0) return;

            var bazaar = Session.Character.Inventory.AddIntoBazaarInventory(
                inventory: cRegPacket.Inventory == 4 ? 0 : (InventoryType)cRegPacket.Inventory, slot: cRegPacket.Slot,
                amount: cRegPacket.Amount);
            if (bazaar == null) return;

            //BecksFix
            if (cRegPacket.Inventory == 9)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(message: "Oh my gosh you found a Dupe tho", type: 0));
                return;
            }

            short duration;
            switch (cRegPacket.Durability)
            {
                case 1:
                    duration = 24;
                    break;

                case 2:
                    duration = 168;
                    break;

                case 3:
                    duration = 360;
                    break;

                case 4:
                    duration = 720;
                    break;

                default:
                    return;
            }

            DaoFactory.ItemInstanceDao.InsertOrUpdate(dto: bazaar);

            var bazaarItem = new BazaarItemDto
            {
                Amount = bazaar.Amount,
                DateStart = DateTime.Now,
                Duration = duration,
                IsPackage = cRegPacket.IsPackage != 0,
                MedalUsed = medal != null,
                Price = cRegPacket.Price,
                SellerId = Session.Character.CharacterId,
                ItemInstanceId = bazaar.Id
            };

            DaoFactory.BazaarItemDao.InsertOrUpdate(bazaarItem: ref bazaarItem);
            ServerManager.Instance.BazaarRefresh(bazaarItemId: bazaarItem.BazaarItemId);

            Session.Character.Gold -= tax;
            Session.SendPacket(packet: Session.Character.GenerateGold());

            Session.SendPacket(packet: Session.Character.GenerateSay(
                message: Language.Instance.GetMessageFromKey(key: "OBJECT_IN_BAZAAR"),
                type: 10));
            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                message: Language.Instance.GetMessageFromKey(key: "OBJECT_IN_BAZAAR"),
                type: 0));

            Logger.LogUserEvent(logEvent: "BAZAAR_INSERT", caller: Session.GenerateIdentity(),
                data:
                $"BazaarId: {bazaarItem.BazaarItemId}, IIId: {bazaarItem.ItemInstanceId} VNum: {bazaar.ItemVNum} Amount: {cRegPacket.Amount} Price: {cRegPacket.Price} Time: {duration}");

            Session.SendPacket(packet: "rc_reg 1");
        }

        /// <summary>
        ///     c_mod packet
        /// </summary>
        /// <param name="cModPacket"></param>
        public void ModPriceBazaar(CModPacket cModPacket)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            if (!Session.Character.CanUseNosBazaar())
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "INFO_BAZAAR")));
                return;
            }

            var bz = DaoFactory.BazaarItemDao.LoadById(bazaarItemId: cModPacket.BazaarId);
            if (bz != null)
            {
                if (bz.SellerId != Session.Character.CharacterId) return;

                var itemInstance = new ItemInstance(input: DaoFactory.ItemInstanceDao.LoadById(id: bz.ItemInstanceId));
                if (itemInstance == null || bz.Amount != itemInstance.Amount) return;

                if ((bz.DateStart.AddHours(value: bz.Duration).AddDays(value: bz.MedalUsed ? 30 : 7) - DateTime.Now)
                    .TotalMinutes <=
                    0) return;

                if (cModPacket.Price <= 0) return;

                var medal = Session.Character.StaticBonusList.Find(match: s =>
                    s.StaticBonusType == StaticBonusType.BazaarMedalGold
                    || s.StaticBonusType == StaticBonusType.BazaarMedalSilver);
                if (cModPacket.Price >= (medal == null ? 1000000 : ServerManager.Instance.Configuration.MaxGold))
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "PRICE_EXCEEDED"), type: 0));
                    return;
                }

                bz.Price = cModPacket.Price;

                DaoFactory.BazaarItemDao.InsertOrUpdate(bazaarItem: ref bz);
                ServerManager.Instance.BazaarRefresh(bazaarItemId: bz.BazaarItemId);

                Session.SendPacket(packet: Session.Character.GenerateSay(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "OBJECT_MOD_IN_BAZAAR"),
                        arg0: bz.Price),
                    type: 10));
                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "OBJECT_MOD_IN_BAZAAR"),
                        arg0: bz.Price),
                    type: 0));

                Logger.LogUserEvent(logEvent: "BAZAAR_MOD", caller: Session.GenerateIdentity(),
                    data:
                    $"BazaarId: {bz.BazaarItemId}, IIId: {bz.ItemInstanceId} VNum: {itemInstance.ItemVNum} Amount: {bz.Amount} Price: {bz.Price} Time: {bz.Duration}");

                RefreshPersonalBazarList(csListPacket: new CSListPacket());
            }
        }

        #endregion
    }
}