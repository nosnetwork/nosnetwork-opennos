﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Handling;
using OpenNos.Core.Threading;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.GameObject.Packets.ClientPackets;
using OpenNos.Log.Networking;
using OpenNos.Log.Shared;
using static OpenNos.Domain.BCardType;

namespace OpenNos.Handler
{
    public class InventoryPacketHandler : IPacketHandler
    {
        #region Instantiation

        public InventoryPacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     b_i packet
        /// </summary>
        /// <param name="bIPacket"></param>
        public void AskToDelete(BIPacket bIPacket)
        {
            if (bIPacket != null)
                switch (bIPacket.Option)
                {
                    case null:
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                            dialog:
                            $"#b_i^{(byte)bIPacket.InventoryType}^{bIPacket.Slot}^1 #b_i^0^0^5 {Language.Instance.GetMessageFromKey(key: "ASK_TO_DELETE")}"));
                        break;

                    case 1:
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                            dialog:
                            $"#b_i^{(byte)bIPacket.InventoryType}^{bIPacket.Slot}^2 #b_i^{(byte)bIPacket.InventoryType}^{bIPacket.Slot}^5 {Language.Instance.GetMessageFromKey(key: "SURE_TO_DELETE")}"));
                        break;

                    case 2:
                        if (Session.Character.InExchangeOrTrade ||
                            bIPacket.InventoryType == InventoryType.Bazaar) return;

                        var delInstance =
                            Session.Character.Inventory.LoadBySlotAndType(slot: bIPacket.Slot,
                                type: bIPacket.InventoryType);
                        Session.Character.DeleteItem(type: bIPacket.InventoryType, slot: bIPacket.Slot);

                        if (delInstance != null)
                            Logger.LogUserEvent(logEvent: "ITEM_DELETE", caller: Session.GenerateIdentity(),
                                data:
                                $"[DeleteItem]IIId: {delInstance.Id} ItemVNum: {delInstance.ItemVNum} Amount: {delInstance.Amount} MapId: {Session.CurrentMapInstance?.Map.MapId} MapX: {Session.Character.PositionX} MapY: {Session.Character.PositionY}");

                        break;
                }
        }

        /// <summary>
        ///     deposit packet
        /// </summary>
        /// <param name="depositPacket"></param>
        public void Deposit(DepositPacket depositPacket)
        {
            if (depositPacket != null)
            {
                if (depositPacket.Inventory == InventoryType.Bazaar
                    || depositPacket.Inventory == InventoryType.FamilyWareHouse
                    || depositPacket.Inventory == InventoryType.Miniland)
                    return;

                var item =
                    Session.Character.Inventory.LoadBySlotAndType(slot: depositPacket.Slot,
                        type: depositPacket.Inventory);
                var itemdest = Session.Character.Inventory.LoadBySlotAndType(slot: depositPacket.NewSlot,
                    type: depositPacket.PartnerBackpack ? InventoryType.PetWarehouse : InventoryType.Warehouse);

                // check if the destination slot is out of range
                if (depositPacket.NewSlot >= (depositPacket.PartnerBackpack
                    ? Session.Character.StaticBonusList.Any(predicate: s =>
                        s.StaticBonusType == StaticBonusType.PetBackPack)
                        ? 50
                        : 0
                    : Session.Character.WareHouseSize))
                    return;

                // check if the character is allowed to move the item
                if (Session.Character.InExchangeOrTrade) return;

                // actually move the item from source to destination
                Session.Character.Inventory.DepositItem(inventory: depositPacket.Inventory, slot: depositPacket.Slot,
                    amount: depositPacket.Amount, NewSlot: depositPacket.NewSlot, item: ref item,
                    itemdest: ref itemdest, PartnerBackpack: depositPacket.PartnerBackpack);
                Logger.LogUserEvent(logEvent: "STASH_DEPOSIT", caller: Session.GenerateIdentity(),
                    data:
                    $"[Deposit]OldIIId: {item?.Id} NewIIId: {itemdest?.Id} Amount: {depositPacket.Amount} PartnerBackpack: {depositPacket.PartnerBackpack}");
            }
        }

        /// <summary>
        ///     eqinfo packet
        /// </summary>
        /// <param name="equipmentInfoPacket"></param>
        public void EquipmentInfo(EquipmentInfoPacket equipmentInfoPacket)
        {
            if (equipmentInfoPacket != null)
            {
                var isNpcShopItem = false;
                ItemInstance inventory = null;
                switch (equipmentInfoPacket.Type)
                {
                    case 0:
                        inventory = Session.Character.Inventory.LoadBySlotAndType(slot: equipmentInfoPacket.Slot,
                            type: InventoryType.Wear);
                        break;

                    case 1:
                        inventory = Session.Character.Inventory.LoadBySlotAndType(slot: equipmentInfoPacket.Slot,
                            type: InventoryType.Equipment);
                        break;

                    case 2:
                        isNpcShopItem = true;
                        if (ServerManager.GetItem(vnum: equipmentInfoPacket.Slot) != null)
                        {
                            inventory = new ItemInstance(vNum: equipmentInfoPacket.Slot, amount: 1);
                            break;
                        }

                        return;

                    case 5:
                        if (Session.Character.ExchangeInfo != null)
                        {
                            var sess =
                                ServerManager.Instance.GetSessionByCharacterId(characterId: Session.Character
                                    .ExchangeInfo
                                    .TargetCharacterId);
                            if (sess?.Character.ExchangeInfo?.ExchangeList?.ElementAtOrDefault(
                                index: equipmentInfoPacket
                                    .Slot) != null)
                            {
                                var id = sess.Character.ExchangeInfo.ExchangeList[index: equipmentInfoPacket.Slot].Id;

                                inventory = sess.Character.Inventory.GetItemInstanceById(id: id);
                            }
                        }

                        break;

                    case 6:
                        if (equipmentInfoPacket.ShopOwnerId != null)
                        {
                            var shop =
                                Session.CurrentMapInstance.UserShops.FirstOrDefault(predicate: mapshop =>
                                    mapshop.Value.OwnerId.Equals(obj: equipmentInfoPacket.ShopOwnerId));
                            var item =
                                shop.Value?.Items.Find(match: i => i.ShopSlot.Equals(obj: equipmentInfoPacket.Slot));
                            if (item != null) inventory = item.ItemInstance;
                        }

                        break;

                    case 7:
                        inventory = Session.Character.Inventory.LoadBySlotAndType(slot: equipmentInfoPacket.MateSlot,
                            type: (InventoryType)(12 + equipmentInfoPacket.Slot));
                        break;

                    case 10:
                        inventory = Session.Character.Inventory.LoadBySlotAndType(slot: equipmentInfoPacket.Slot,
                            type: InventoryType.Specialist);
                        break;

                    case 11:
                        inventory = Session.Character.Inventory.LoadBySlotAndType(slot: equipmentInfoPacket.Slot,
                            type: InventoryType.Costume);
                        break;
                }

                if (inventory?.Item != null)
                {
                    if (inventory.IsEmpty || isNpcShopItem)
                    {
                        Session.SendPacket(packet: inventory.GenerateEInfo());
                        return;
                    }

                    Session.SendPacket(packet: inventory.Item.EquipmentSlot != EquipmentType.Sp
                        ? inventory.GenerateEInfo()
                        : inventory.Item.SpType == 0 && inventory.Item.ItemSubType == 4
                            ? inventory.GeneratePslInfo()
                            : inventory.GenerateSlInfo(session: Session));
                }
            }
        }

        // TODO: TRANSLATE IT TO PACKETDEFINITION!
        [Packet("exc_list")]
        public void ExchangeList(string packet)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            Logger.LogUserEvent(logEvent: "EXC_LIST", caller: Session.GenerateIdentity(),
                data: $"Packet string: {packet}");

            if (Session.Character.ExchangeInfo == null) return;
            var targetSession =
                ServerManager.Instance.GetSessionByCharacterId(characterId: Session.Character.ExchangeInfo
                    .TargetCharacterId);
            if (Session.Character.HasShopOpened || targetSession?.Character.HasShopOpened == true)
            {
                CloseExchange(session: Session, targetSession: targetSession);
                return;
            }

            var packetsplit = packet.Split(' ');
            if (packetsplit.Length < 4)
            {
                Session.SendPacket(packet: "exc_close 0");
                Session.CurrentMapInstance?.Broadcast(client: Session, content: "exc_close 0",
                    receiver: ReceiverType.OnlySomeone,
                    characterName: "", characterId: Session.Character.ExchangeInfo.TargetCharacterId);

                if (targetSession != null) targetSession.Character.ExchangeInfo = null;
                Session.Character.ExchangeInfo = null;
                return;
            }

            if (!long.TryParse(s: packetsplit[2], result: out var gold)) return;

            if (!long.TryParse(s: packetsplit[3], result: out var bankGold)) return;

            var type = new byte[10];
            short[] slot = new short[10], qty = new short[10];
            var packetList = "";

            if (gold < 0 || gold > Session.Character.Gold || Session.Character.ExchangeInfo == null
                || Session.Character.ExchangeInfo.ExchangeList.Count > 0)
                return;

            if (bankGold < 0 || bankGold > Session.Character.GoldBank || Session.Character.ExchangeInfo == null
                || Session.Character.ExchangeInfo.ExchangeList.Count > 0)
                return;

            for (int j = 7, i = 0; j <= packetsplit.Length && i < 10; j += 3, i++)
            {
                byte.TryParse(s: packetsplit[j - 3], result: out type[i]);
                short.TryParse(s: packetsplit[j - 2], result: out slot[i]);
                short.TryParse(s: packetsplit[j - 1], result: out qty[i]);
                if ((InventoryType)type[i] == InventoryType.Bazaar)
                {
                    CloseExchange(session: Session, targetSession: targetSession);
                    return;
                }

                var item = Session.Character.Inventory.LoadBySlotAndType(slot: slot[i], type: (InventoryType)type[i]);
                if (item == null) return;

                if (qty[i] <= 0 || item.Amount < qty[i]) return;

                var it = item.DeepCopy();
                if (it.Item.IsTradable && !it.IsBound)
                {
                    it.Amount = qty[i];
                    Session.Character.ExchangeInfo.ExchangeList.Add(item: it);
                    if (type[i] != 0)
                        packetList += $"{i}.{type[i]}.{it.ItemVNum}.{qty[i]} ";
                    else
                        packetList += $"{i}.{type[i]}.{it.ItemVNum}.{it.Rare}.{it.Upgrade} ";
                }
                else if (it.IsBound)
                {
                    Session.SendPacket(packet: "exc_close 0");
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: "exc_close 0",
                        receiver: ReceiverType.OnlySomeone,
                        characterName: "", characterId: Session.Character.ExchangeInfo.TargetCharacterId);

                    if (targetSession != null) targetSession.Character.ExchangeInfo = null;
                    Session.Character.ExchangeInfo = null;
                    return;
                }
            }

            Session.Character.ExchangeInfo.Gold = gold;
            Session.Character.ExchangeInfo.BankGold = bankGold;
            Session.CurrentMapInstance?.Broadcast(client: Session,
                content: $"exc_list 1 {Session.Character.CharacterId} {gold} {bankGold} {packetList}",
                receiver: ReceiverType.OnlySomeone,
                characterName: "", characterId: Session.Character.ExchangeInfo.TargetCharacterId);
            Session.Character.ExchangeInfo.Validated = true;
        }

        /// <summary>
        ///     req_exc packet
        /// </summary>
        /// <param name="exchangeRequestPacket"></param>
        public void ExchangeRequest(ExchangeRequestPacket exchangeRequestPacket)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            if (exchangeRequestPacket != null)
            {
                var sess = ServerManager.Instance.GetSessionByCharacterId(
                    characterId: exchangeRequestPacket.CharacterId);

                if (sess != null && Session.Character.MapInstanceId != sess.Character.MapInstanceId)
                {
                    sess.Character.ExchangeInfo = null;
                    Session.Character.ExchangeInfo = null;
                }
                else
                {
                    switch (exchangeRequestPacket.RequestType)
                    {
                        case RequestExchangeType.Requested:
                            if (!Session.HasCurrentMapInstance) return;

                            var targetSession =
                                Session.CurrentMapInstance.GetSessionByCharacterId(
                                    characterId: exchangeRequestPacket.CharacterId);
                            if (targetSession?.Account == null) return;

                            if (targetSession.Account.IsLimited)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "CANNOT_TRADE_LIMITED_ACCOUNT")));
                                return;
                            }

                            if (targetSession.CurrentMapInstance?.MapInstanceType ==
                                MapInstanceType.TalentArenaMapInstance) return;

                            if (targetSession.Character.Group != null
                                && targetSession.Character.Group?.GroupType != GroupType.Group)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "EXCHANGE_NOT_ALLOWED_IN_RAID"),
                                    type: 0));
                                return;
                            }

                            if (Session.Character.Group != null
                                && Session.Character.Group?.GroupType != GroupType.Group)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(
                                        key: "EXCHANGE_NOT_ALLOWED_WITH_RAID_MEMBER"), type: 0));
                                return;
                            }

                            if (Session.Character.IsBlockedByCharacter(characterId: exchangeRequestPacket.CharacterId))
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateInfo(
                                        message: Language.Instance.GetMessageFromKey(key: "BLACKLIST_BLOCKED")));
                                return;
                            }

                            if (Session.Character.Speed == 0 || targetSession.Character.Speed == 0)
                                Session.Character.ExchangeBlocked = true;

                            if (targetSession.Character.LastSkillUse.AddSeconds(value: 20) > DateTime.Now
                                || targetSession.Character.LastDefence.AddSeconds(value: 20) > DateTime.Now)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "PLAYER_IN_BATTLE"),
                                        arg0: targetSession.Character.Name)));
                                return;
                            }

                            if (Session.Character.LastSkillUse.AddSeconds(value: 20) > DateTime.Now
                                || Session.Character.LastDefence.AddSeconds(value: 20) > DateTime.Now)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateInfo(
                                        message: Language.Instance.GetMessageFromKey(key: "IN_BATTLE")));
                                return;
                            }

                            if (Session.Character.HasShopOpened || targetSession.Character.HasShopOpened)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "HAS_SHOP_OPENED"),
                                        type: 10));
                                return;
                            }

                            if (targetSession.Character.ExchangeBlocked)
                            {
                                Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "TRADE_BLOCKED"),
                                        type: 11));
                            }
                            else
                            {
                                if (Session.Character.InExchangeOrTrade || targetSession.Character.InExchangeOrTrade)
                                {
                                    Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateModal(
                                            message: Language.Instance.GetMessageFromKey(key: "ALREADY_EXCHANGE"),
                                            type: 0));
                                }
                                else
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateModal(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "YOU_ASK_FOR_EXCHANGE"),
                                            arg0: targetSession.Character.Name), type: 0));

                                    Logger.LogUserEvent(logEvent: "TRADE_REQUEST", caller: Session.GenerateIdentity(),
                                        data: $"[ExchangeRequest][{targetSession.GenerateIdentity()}]");

                                    Session.Character.TradeRequests.Add(value: targetSession.Character.CharacterId);
                                    targetSession.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                        dialog:
                                        $"#req_exc^2^{Session.Character.CharacterId} #req_exc^5^{Session.Character.CharacterId} {string.Format(format: Language.Instance.GetMessageFromKey(key: "INCOMING_EXCHANGE"), arg0: Session.Character.Name)}"));
                                }
                            }

                            break;

                        case RequestExchangeType.Confirmed: // click Trade button in exchange window
                            if (Session.HasCurrentMapInstance && Session.HasSelectedCharacter
                                                              && Session.Character.ExchangeInfo != null
                                                              && Session.Character.ExchangeInfo.TargetCharacterId
                                                              != Session.Character.CharacterId)
                            {
                                if (!Session.HasCurrentMapInstance) return;

                                targetSession =
                                    Session.CurrentMapInstance.GetSessionByCharacterId(characterId: Session.Character
                                        .ExchangeInfo
                                        .TargetCharacterId);

                                if (targetSession == null) return;

                                if (Session.Character.Group != null
                                    && Session.Character.Group?.GroupType != GroupType.Group)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(
                                            key: "EXCHANGE_NOT_ALLOWED_IN_RAID"), type: 0));
                                    return;
                                }

                                if (targetSession.Character.Group != null
                                    && targetSession.Character.Group?.GroupType != GroupType.Group)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(
                                            key: "EXCHANGE_NOT_ALLOWED_WITH_RAID_MEMBER"),
                                        type: 0));
                                    return;
                                }

                                if (Session.IsDisposing || targetSession.IsDisposing)
                                {
                                    CloseExchange(session: Session, targetSession: targetSession);
                                    return;
                                }

                                lock (targetSession.Character.Inventory)
                                {
                                    lock (Session.Character.Inventory)
                                    {
                                        var targetExchange = targetSession.Character.ExchangeInfo;
                                        var inventory = targetSession.Character.Inventory;

                                        var gold = targetSession.Character.Gold;
                                        var goldBank = targetSession.Character.GoldBank;
                                        var maxGold = ServerManager.Instance.Configuration.MaxGold;

                                        if (targetExchange == null || Session.Character.ExchangeInfo == null) return;

                                        if (Session.Character.ExchangeInfo.Validated && targetExchange.Validated)
                                        {
                                            Logger.LogUserEvent(logEvent: "TRADE_ACCEPT",
                                                caller: Session.GenerateIdentity(),
                                                data: $"[ExchangeAccept][{targetSession.GenerateIdentity()}]");
                                            try
                                            {
                                                Session.Character.ExchangeInfo.Confirmed = true;
                                                if (targetExchange.Confirmed
                                                    && Session.Character.ExchangeInfo.Confirmed)
                                                {
                                                    Session.SendPacket(packet: "exc_close 1");
                                                    targetSession.SendPacket(packet: "exc_close 1");

                                                    var continues = true;
                                                    var goldmax = false;
                                                    if (!Session.Character.Inventory.EnoughPlace(
                                                        itemInstances: targetExchange
                                                            .ExchangeList))
                                                        continues = false;

                                                    continues &=
                                                        inventory.EnoughPlace(itemInstances: Session.Character
                                                            .ExchangeInfo
                                                            .ExchangeList);
                                                    goldmax |= Session.Character.ExchangeInfo.Gold + gold > maxGold;
                                                    goldmax |= Session.Character.ExchangeInfo.BankGold + goldBank >
                                                               maxGold;
                                                    if (Session.Character.ExchangeInfo.Gold > Session.Character.Gold
                                                        || Session.Character.ExchangeInfo.BankGold >
                                                        Session.Character.GoldBank)
                                                        return;

                                                    goldmax |= targetExchange.Gold + Session.Character.Gold > maxGold;
                                                    goldmax |= targetExchange.BankGold + Session.Character.GoldBank >
                                                               maxGold;
                                                    if (!continues || goldmax)
                                                    {
                                                        var message = !continues
                                                            ? UserInterfaceHelper.GenerateMsg(
                                                                message: Language.Instance.GetMessageFromKey(
                                                                    key: "NOT_ENOUGH_PLACE"),
                                                                type: 0)
                                                            : UserInterfaceHelper.GenerateMsg(
                                                                message: Language.Instance.GetMessageFromKey(
                                                                    key: "MAX_GOLD"), type: 0);
                                                        Session.SendPacket(packet: message);
                                                        targetSession.SendPacket(packet: message);
                                                        CloseExchange(session: Session, targetSession: targetSession);
                                                    }
                                                    else if (Session.Character.Gold <
                                                        Session.Character.ExchangeInfo.Gold || targetSession.Character
                                                                                                .Gold < targetExchange
                                                                                                .Gold
                                                                                            || Session.Character
                                                                                                .GoldBank <
                                                                                            Session.Character
                                                                                                .ExchangeInfo
                                                                                                .BankGold ||
                                                                                            targetSession.Character
                                                                                                .GoldBank <
                                                                                            targetExchange.BankGold)
                                                    {
                                                        var message =
                                                            UserInterfaceHelper.GenerateMsg(
                                                                message: Language.Instance.GetMessageFromKey(
                                                                    key: "ERROR_ON_EXANGE"),
                                                                type: 0);
                                                        Session.SendPacket(packet: message);
                                                        targetSession.SendPacket(packet: message);
                                                        CloseExchange(session: Session, targetSession: targetSession);
                                                    }
                                                    else
                                                    {
                                                        if (Session.Character.ExchangeInfo.ExchangeList.Any(
                                                            predicate: ei =>
                                                                !(ei.Item.IsTradable || ei.IsBound)))
                                                        {
                                                            Session.SendPacket(
                                                                packet: UserInterfaceHelper.GenerateMsg(
                                                                    message: Language.Instance.GetMessageFromKey(
                                                                        key: "ITEM_NOT_TRADABLE"), type: 0));
                                                            CloseExchange(session: Session,
                                                                targetSession: targetSession);
                                                        }

                                                        if (targetSession.Character.ExchangeInfo.ExchangeList.Any(
                                                            predicate: ei =>
                                                                !(ei.Item.IsTradable || ei.IsBound)))
                                                        {
                                                            targetSession.SendPacket(
                                                                packet: UserInterfaceHelper.GenerateMsg(
                                                                    message: Language.Instance.GetMessageFromKey(
                                                                        key: "ITEM_NOT_TRADABLE"), type: 0));
                                                            CloseExchange(session: targetSession,
                                                                targetSession: Session);
                                                        }
                                                        else // all items can be traded
                                                        {
                                                            Session.Character.IsExchanging =
                                                                targetSession.Character.IsExchanging = true;

                                                            // exchange all items from target to source
                                                            Exchange(sourceSession: targetSession,
                                                                targetSession: Session);

                                                            // exchange all items from source to target
                                                            Exchange(sourceSession: Session,
                                                                targetSession: targetSession);

                                                            Session.Character.IsExchanging =
                                                                targetSession.Character.IsExchanging = false;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                                                        message: string.Format(
                                                            format: Language.Instance.GetMessageFromKey(
                                                                key: "IN_WAITING_FOR"),
                                                            arg0: targetSession.Character.Name)));
                                                }
                                            }
                                            catch (NullReferenceException nre)
                                            {
                                                Logger.Error(ex: nre);
                                            }
                                        }
                                    }
                                }
                            }

                            break;

                        case RequestExchangeType.Cancelled: // cancel trade thru exchange window
                            if (Session.HasCurrentMapInstance && Session.Character.ExchangeInfo != null)
                            {
                                targetSession =
                                    Session.CurrentMapInstance.GetSessionByCharacterId(characterId: Session.Character
                                        .ExchangeInfo
                                        .TargetCharacterId);
                                CloseExchange(session: Session, targetSession: targetSession);
                            }

                            break;

                        case RequestExchangeType.List:
                            if (sess != null &&
                                (!Session.Character.InExchangeOrTrade || !sess.Character.InExchangeOrTrade))
                            {
                                var otherSession =
                                    ServerManager.Instance.GetSessionByCharacterId(
                                        characterId: exchangeRequestPacket.CharacterId);
                                if (exchangeRequestPacket.CharacterId == Session.Character.CharacterId
                                    || Session.Character.Speed == 0 || otherSession == null
                                    || otherSession.Character.TradeRequests.All(predicate: s =>
                                        s != Session.Character.CharacterId)
                                )
                                    return;

                                if (Session.Character.Group != null
                                    && Session.Character.Group?.GroupType != GroupType.Group)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(
                                            key: "EXCHANGE_NOT_ALLOWED_IN_RAID"), type: 0));
                                    return;
                                }

                                if (otherSession.Character.Group != null
                                    && otherSession.Character.Group?.GroupType != GroupType.Group)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(
                                            key: "EXCHANGE_NOT_ALLOWED_WITH_RAID_MEMBER"),
                                        type: 0));
                                    return;
                                }

                                Session.SendPacket(packet: $"exc_list 1 {exchangeRequestPacket.CharacterId} -1");
                                Session.Character.ExchangeInfo = new ExchangeInfo
                                {
                                    TargetCharacterId = exchangeRequestPacket.CharacterId,
                                    Confirmed = false
                                };
                                sess.Character.ExchangeInfo = new ExchangeInfo
                                {
                                    TargetCharacterId = Session.Character.CharacterId,
                                    Confirmed = false
                                };
                                Session.CurrentMapInstance?.Broadcast(client: Session,
                                    content: $"exc_list 1 {Session.Character.CharacterId} -1",
                                    receiver: ReceiverType.OnlySomeone,
                                    characterName: "", characterId: exchangeRequestPacket.CharacterId);
                            }
                            else
                            {
                                Session.CurrentMapInstance?.Broadcast(client: Session,
                                    content: UserInterfaceHelper.GenerateModal(
                                        message: Language.Instance.GetMessageFromKey(key: "ALREADY_EXCHANGE"), type: 0),
                                    receiver: ReceiverType.OnlySomeone, characterName: "",
                                    characterId: exchangeRequestPacket.CharacterId);
                            }

                            break;

                        case RequestExchangeType.Declined:
                            if (sess != null) sess.Character.ExchangeInfo = null;
                            Session.Character.ExchangeInfo = null;
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "YOU_REFUSED"), type: 10));
                            if (sess != null)
                                sess.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "EXCHANGE_REFUSED"),
                                            arg0: Session.Character.Name), type: 10));

                            break;

                        default:
                            Logger.Warn(
                                data:
                                $"Exchange-Request-Type not implemented. RequestType: {exchangeRequestPacket.RequestType})");
                            break;
                    }
                }
            }
        }

        /// <summary>
        ///     get packet
        /// </summary>
        /// <param name="getPacket"></param>
        public void GetItem(GetPacket getPacket)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            if (getPacket == null || Session.Character.LastSkillUse.AddSeconds(value: 1) > DateTime.Now
                                  || Session.Character.IsVehicled
                                  && Session.CurrentMapInstance?.MapInstanceType != MapInstanceType.EventGameInstance
                                  || !Session.HasCurrentMapInstance
                                  || Session.Character.IsSeal
                                  || Session.CurrentMapInstance.MapInstanceType == MapInstanceType.TimeSpaceInstance &&
                                  Session.CurrentMapInstance.InstanceBag.EndState != 0)
                return;

            if (getPacket.TransportId < 100000)
            {
                var button =
                    Session.CurrentMapInstance.Buttons.Find(match: s => s.MapButtonId == getPacket.TransportId);
                if (button != null)
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 2000, type: 1,
                        argument: $"#git^{button.MapButtonId}"));
            }
            else
            {
                lock (Session.CurrentMapInstance.DroppedList)
                {
                    if (!Session.CurrentMapInstance.DroppedList.ContainsKey(key: getPacket.TransportId)) return;

                    var mapItem = Session.CurrentMapInstance.DroppedList[key: getPacket.TransportId];

                    if (mapItem != null)
                    {
                        var canpick = false;
                        switch (getPacket.PickerType)
                        {
                            case 1:
                                canpick = Session.Character.IsInRange(xCoordinate: mapItem.PositionX,
                                    yCoordinate: mapItem.PositionY, range: 8);
                                break;

                            case 2:
                                var mate = Session.Character.Mates.Find(match: s =>
                                    s.MateTransportId == getPacket.PickerId && s.CanPickUp);
                                if (mate != null)
                                    canpick = mate.IsInRange(xCoordinate: mapItem.PositionX,
                                        yCoordinate: mapItem.PositionY, range: 8);

                                break;
                        }

                        if (canpick && Session.HasCurrentMapInstance)
                        {
                            if (mapItem is MonsterMapItem item)
                            {
                                var monsterMapItem = item;
                                if (Session.CurrentMapInstance.MapInstanceType != MapInstanceType.LodInstance
                                    && monsterMapItem.OwnerId.HasValue && monsterMapItem.OwnerId.Value != -1)
                                {
                                    var group = ServerManager.Instance.Groups.Find(match: g =>
                                        g.IsMemberOfGroup(entityId: monsterMapItem.OwnerId.Value)
                                        && g.IsMemberOfGroup(entityId: Session.Character.CharacterId));
                                    if (item.CreatedDate.AddSeconds(value: 30) > DateTime.Now
                                        && !(monsterMapItem.OwnerId == Session.Character.CharacterId
                                             || group?.SharingMode == (byte)GroupSharingType.Everyone))
                                    {
                                        Session.SendPacket(
                                            packet: Session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(key: "NOT_YOUR_ITEM"),
                                                type: 10));
                                        return;
                                    }
                                }

                                // initialize and rarify
                                item.Rarify(session: null);
                            }

                            if (mapItem.ItemVNum != 1046)
                            {
                                var mapItemInstance = mapItem.GetItemInstance();

                                if (mapItemInstance?.Item == null) return;

                                if (mapItemInstance.Item.ItemType == ItemType.Map || mapItem.IsQuest)
                                {
                                    if (mapItem is MonsterMapItem)
                                    {
                                        Session.Character.IncrementQuests(type: QuestType.Collect1,
                                            firstData: mapItem.ItemVNum);
                                        if (mapItem.IsQuest)
                                        {
                                            Session.Character.IncrementQuests(type: QuestType.Collect2,
                                                firstData: mapItem.ItemVNum);
                                            Session.Character.IncrementQuests(type: QuestType.Collect4,
                                                firstData: mapItem.ItemVNum);
                                        }
                                    }

                                    if (mapItemInstance.Item.Effect == 71)
                                    {
                                        Session.Character.SpPoint += mapItem.GetItemInstance().Item.EffectValue;
                                        if (Session.Character.SpPoint > 10000) Session.Character.SpPoint = 10000;

                                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                            message: string.Format(
                                                format: Language.Instance.GetMessageFromKey(key: "SP_POINTSADDED"),
                                                arg0: mapItem.GetItemInstance().Item.EffectValue), type: 0));
                                        Session.SendPacket(packet: Session.Character.GenerateSpPoint());
                                    }

                                    #region Flower Quest

                                    if (mapItem.ItemVNum == 1086 && ServerManager.Instance.FlowerQuestId != null)
                                        Session.Character.AddQuest(
                                            questId: (long)ServerManager.Instance.FlowerQuestId);

                                    #endregion

                                    Session.CurrentMapInstance.DroppedList.Remove(key: getPacket.TransportId);

                                    Session.CurrentMapInstance?.Broadcast(
                                        packet: StaticPacketHelper.GenerateGet(pickerType: getPacket.PickerType,
                                            pickerId: getPacket.PickerId,
                                            itemId: getPacket.TransportId));

                                    if (getPacket.PickerType == 2)
                                        Session.CurrentMapInstance?.Broadcast(
                                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                                callerId: getPacket.PickerId, effectId: 5004));
                                }
                                else
                                {
                                    lock (Session.Character.Inventory)
                                    {
                                        long characterDropperId = 0;
                                        if (mapItemInstance.CharacterId > 0)
                                            characterDropperId = mapItemInstance.CharacterId;
                                        var amount = mapItem.Amount;
                                        var inv = Session.Character.Inventory.AddToInventory(newItem: mapItemInstance)
                                            .FirstOrDefault();
                                        if (inv != null)
                                        {
                                            Session.CurrentMapInstance.DroppedList.Remove(key: getPacket.TransportId);

                                            Session.CurrentMapInstance?.Broadcast(
                                                packet: StaticPacketHelper.GenerateGet(pickerType: getPacket.PickerType,
                                                    pickerId: getPacket.PickerId,
                                                    itemId: getPacket.TransportId));

                                            if (getPacket.PickerType == 2)
                                            {
                                                Session.CurrentMapInstance?.Broadcast(
                                                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                                        callerId: getPacket.PickerId,
                                                        effectId: 5004));
                                                Session.SendPacket(packet: Session.Character.GenerateIcon(type: 1,
                                                    value: 1, itemVNum: inv.ItemVNum));
                                            }

                                            Session.SendPacket(packet: Session.Character.GenerateSay(
                                                message:
                                                $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {inv.Item.Name} x {amount}",
                                                type: 12));
                                            if (Session.CurrentMapInstance.MapInstanceType ==
                                                MapInstanceType.LodInstance)
                                                Session.CurrentMapInstance?.Broadcast(
                                                    packet: Session.Character.GenerateSay(
                                                        message:
                                                        $"{string.Format(format: Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED_LOD"), arg0: Session.Character.Name)}: {inv.Item.Name} x {mapItem.Amount}",
                                                        type: 10));

                                            if (characterDropperId > 0)
                                                if (ServerManager.Instance.Configuration.UseLogService)
                                                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                                    {
                                                        Sender =
                                                            ServerManager.Instance
                                                                .GetSessionByCharacterId(
                                                                    characterId: characterDropperId)?.Character
                                                                ?.Name ?? DaoFactory.CharacterDao
                                                                .LoadById(characterId: characterDropperId)?.Name ??
                                                            "Unknown",
                                                        SenderId = characterDropperId,
                                                        Receiver = Session.Character.Name,
                                                        ReceiverId = Session.Character.CharacterId,
                                                        PacketType = LogType.Drop,
                                                        Packet =
                                                            $"[GET_DROP]IIId: {inv.Id} ItemVNum: {inv.ItemVNum} Amount: {amount} Rare: {inv.Rare} Upgrade: {inv.Upgrade}"
                                                    });

                                            Logger.LogUserEvent(logEvent: "CHARACTER_ITEM_GET",
                                                caller: Session.GenerateIdentity(),
                                                data:
                                                $"[GetItem]IIId: {inv.Id} ItemVNum: {inv.ItemVNum} Amount: {amount}");
                                        }
                                        else
                                        {
                                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                                type: 0));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // handle gold drop
                                var maxGold = ServerManager.Instance.Configuration.MaxGold;

                                var multiplier = 1 + Session.Character.GetBuff(type: CardType.Item,
                                    subtype: (byte)AdditionalTypes.Item.IncreaseEarnedGold)[0] / 100D;
                                multiplier +=
                                    (Session.Character.ShellEffectMain.FirstOrDefault(predicate: s =>
                                        s.Effect == (byte)ShellWeaponEffectType.GainMoreGold)?.Value ?? 0) / 100D;

                                if (mapItem is MonsterMapItem droppedGold
                                    && Session.Character.Gold + droppedGold.GoldAmount * multiplier <= maxGold)
                                {
                                    if (getPacket.PickerType == 2)
                                        Session.SendPacket(
                                            packet: Session.Character.GenerateIcon(type: 1, value: 1, itemVNum: 1046));

                                    Session.Character.Gold += (int)(droppedGold.GoldAmount * multiplier);

                                    Logger.LogUserEvent(logEvent: "CHARACTER_ITEM_GET",
                                        caller: Session.GenerateIdentity(),
                                        data: $"[GetItem]Gold: {(int)(droppedGold.GoldAmount * multiplier)})");

                                    Session.SendPacket(packet: Session.Character.GenerateSay(
                                        message:
                                        $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {mapItem.GetItemInstance().Item.Name} x {droppedGold.GoldAmount}{(multiplier > 1 ? $" + {(int)(droppedGold.GoldAmount * multiplier) - droppedGold.GoldAmount}" : "")}",
                                        type: 12));
                                }
                                else
                                {
                                    Session.Character.Gold = maxGold;
                                    Logger.LogUserEvent(logEvent: "CHARACTER_ITEM_GET",
                                        caller: Session.GenerateIdentity(), data: "[MaxGold]");
                                    Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "MAX_GOLD"),
                                            type: 0));
                                }

                                Session.SendPacket(packet: Session.Character.GenerateGold());
                                Session.CurrentMapInstance.DroppedList.Remove(key: getPacket.TransportId);

                                Session.CurrentMapInstance?.Broadcast(
                                    packet: StaticPacketHelper.GenerateGet(pickerType: getPacket.PickerType,
                                        pickerId: getPacket.PickerId,
                                        itemId: getPacket.TransportId));

                                if (getPacket.PickerType == 2)
                                    Session.CurrentMapInstance?.Broadcast(
                                        packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                            callerId: getPacket.PickerId, effectId: 5004));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     mve packet
        /// </summary>
        /// <param name="mvePacket"></param>
        public void MoveEquipment(MvePacket mvePacket)
        {
            if (mvePacket != null)
                lock (Session.Character.Inventory)
                {
                    if (mvePacket.Slot.Equals(obj: mvePacket.DestinationSlot)
                        && mvePacket.InventoryType.Equals(obj: mvePacket.DestinationInventoryType))
                        return;

                    if (mvePacket.DestinationSlot > 48 + (Session.Character.HaveBackpack() ? 1 : 0) * 12) return;

                    if (Session.Character.InExchangeOrTrade) return;

                    var sourceItem =
                        Session.Character.Inventory.LoadBySlotAndType(slot: mvePacket.Slot,
                            type: mvePacket.InventoryType);
                    if (sourceItem?.Item.ItemType == ItemType.Specialist
                        || sourceItem?.Item.ItemType == ItemType.Fashion)
                    {
                        var inv = Session.Character.Inventory.MoveInInventory(sourceSlot: mvePacket.Slot,
                            sourceType: mvePacket.InventoryType, targetType: mvePacket.DestinationInventoryType,
                            targetSlot: mvePacket.DestinationSlot,
                            wear: false);
                        if (inv != null)
                        {
                            Session.SendPacket(packet: inv.GenerateInventoryAdd());
                            Session.SendPacket(
                                packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(
                                    type: mvePacket.InventoryType,
                                    slot: mvePacket.Slot));
                        }
                    }
                }
        }

        /// <summary>
        ///     mvi packet
        /// </summary>
        /// <param name="mviPacket"></param>
        public void MoveItem(MviPacket mviPacket)
        {
            if (mviPacket != null)
            {
                if (mviPacket.InventoryType != InventoryType.Equipment
                    && mviPacket.InventoryType != InventoryType.Main
                    && mviPacket.InventoryType != InventoryType.Etc
                    && mviPacket.InventoryType != InventoryType.Miniland)
                    return;

                if (mviPacket.Amount < 1) return;

                if (mviPacket.Slot == mviPacket.DestinationSlot) return;

                lock (Session.Character.Inventory)
                {
                    // check if the destination slot is out of range
                    if (mviPacket.DestinationSlot > 48 + (Session.Character.HaveBackpack() ? 1 : 0) * 12) return;

                    // check if the character is allowed to move the item
                    if (Session.Character.InExchangeOrTrade) return;

                    // actually move the item from source to destination
                    Session.Character.Inventory.MoveItem(sourcetype: mviPacket.InventoryType,
                        desttype: mviPacket.InventoryType,
                        sourceSlot: mviPacket.Slot, amount: mviPacket.Amount,
                        destinationSlot: mviPacket.DestinationSlot, sourceInventory: out var previousInventory,
                        destinationInventory: out var newInventory);
                    if (newInventory == null) return;

                    Session.SendPacket(packet: newInventory.GenerateInventoryAdd());

                    Session.SendPacket(packet: previousInventory != null
                        ? previousInventory.GenerateInventoryAdd()
                        : UserInterfaceHelper.Instance.GenerateInventoryRemove(type: mviPacket.InventoryType,
                            slot: mviPacket.Slot));
                }
            }
        }

        /// <summary>
        ///     put packet
        /// </summary>
        /// <param name="putPacket"></param>
        public void PutItem(PutPacket putPacket)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            if (putPacket == null || Session.Character.HasShopOpened) return;

            lock (Session.Character.Inventory)
            {
                var invitem =
                    Session.Character.Inventory.LoadBySlotAndType(slot: putPacket.Slot, type: putPacket.InventoryType);
                if (invitem?.Item.IsDroppable == true && invitem.Item.IsTradable
                                                      && !Session.Character.InExchangeOrTrade &&
                                                      putPacket.InventoryType != InventoryType.Bazaar)
                {
                    if (putPacket.Amount > 0 && putPacket.Amount < 1000)
                    {
                        if (Session.Character.MapInstance.DroppedList.Count < 200 && Session.HasCurrentMapInstance)
                        {
                            var droppedItem = Session.CurrentMapInstance.PutItem(type: putPacket.InventoryType,
                                slot: putPacket.Slot, amount: putPacket.Amount, inv: ref invitem, session: Session);
                            if (droppedItem == null)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "ITEM_NOT_DROPPABLE_HERE"),
                                    type: 0));
                                return;
                            }

                            Session.SendPacket(packet: invitem.GenerateInventoryAdd());

                            if (invitem.Amount == 0)
                                Session.Character.DeleteItem(type: invitem.Type, slot: invitem.Slot);

                            Logger.LogUserEvent(logEvent: "CHARACTER_ITEM_DROP", caller: Session.GenerateIdentity(),
                                data:
                                $"[PutItem]IIId: {invitem.Id} ItemVNum: {droppedItem.ItemVNum} Amount: {droppedItem.Amount} MapId: {Session.CurrentMapInstance.Map.MapId} MapX: {droppedItem.PositionX} MapY: {droppedItem.PositionY}");
                            Session.CurrentMapInstance?.Broadcast(
                                packet:
                                $"drop {droppedItem.ItemVNum} {droppedItem.TransportId} {droppedItem.PositionX} {droppedItem.PositionY} {droppedItem.Amount} 0 -1");
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "DROP_MAP_FULL"),
                                    type: 0));
                        }
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "BAD_DROP_AMOUNT"), type: 0));
                    }
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "ITEM_NOT_DROPPABLE"), type: 0));
                }
            }
        }

        /// <summary>
        ///     remove packet
        /// </summary>
        /// <param name="removePacket"></param>
        public void Remove(RemovePacket removePacket)
        {
            if (removePacket != null)
            {
                InventoryType equipment;
                Mate mate = null;
                if (removePacket.Type > 0)
                {
                    equipment = (InventoryType)(12 + removePacket.Type);
                    mate = Session.Character.Mates.Find(match: s =>
                        s.MateType == MateType.Partner && s.PetId == removePacket.Type - 1);
                    if (mate.IsTemporalMate) return;
                }
                else
                {
                    equipment = InventoryType.Wear;
                }

                if (Session.HasCurrentMapInstance
                    && Session.CurrentMapInstance.UserShops.FirstOrDefault(predicate: mapshop =>
                        mapshop.Value.OwnerId.Equals(obj: Session.Character.CharacterId)).Value == null
                    && (Session.Character.ExchangeInfo == null
                        || (Session.Character.ExchangeInfo?.ExchangeList).Count == 0))
                {
                    var inventory =
                        Session.Character.Inventory.LoadBySlotAndType(slot: removePacket.InventorySlot,
                            type: equipment);
                    if (inventory != null)
                    {
                        var currentRunningSeconds =
                            (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(value: -50)).TotalSeconds;
                        var timeSpanSinceLastSpUsage = currentRunningSeconds - Session.Character.LastSp;
                        if (removePacket.Type == 0)
                        {
                            if (removePacket.InventorySlot == (byte)EquipmentType.Sp && Session.Character.UseSp &&
                                !Session.Character.IsSeal)
                            {
                                if (Session.Character.IsVehicled)
                                {
                                    Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "REMOVE_VEHICLE"),
                                            type: 0));
                                    return;
                                }

                                if (Session.Character.LastSkillUse.AddSeconds(value: 2) > DateTime.Now) return;

                                if (Session.Character.Timespace != null &&
                                    Session.Character.Timespace.SpNeeded?[(byte)Session.Character.Class] != 0 &&
                                    Session.Character.Timespace.InstanceBag.Lock) return;

                                if (!Session.Character.RemoveSp(vnum: inventory.ItemVNum, forced: false)) return;

                                Session.Character.LastSp =
                                    (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(value: -50))
                                    .TotalSeconds;
                            }
                            else if (removePacket.InventorySlot == (byte)EquipmentType.Sp
                                     && !Session.Character.UseSp
                                     && timeSpanSinceLastSpUsage <= Session.Character.SpCooldown)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "SP_INLOADING"),
                                        arg0: Session.Character.SpCooldown -
                                              (int)Math.Round(value: timeSpanSinceLastSpUsage, digits: 0)),
                                    type: 0));
                                return;
                            }
                            else if (removePacket.InventorySlot == (byte)EquipmentType.Fairy
                                     && Session.Character.IsUsingFairyBooster)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(
                                        key: "REMOVE_FAIRY_WHILE_USING_BOOSTER"), type: 0));
                                return;
                            }

                            if ((inventory.ItemDeleteTime >= DateTime.Now || inventory.DurabilityPoint > 0) &&
                                Session.Character.Buff.ContainsKey(key: 62)) Session.Character.RemoveBuff(cardId: 62);

                            Session.Character.EquipmentBCards.RemoveAll(match: o => o.ItemVNum == inventory.ItemVNum);
                        }

                        var inv = Session.Character.Inventory.MoveInInventory(sourceSlot: removePacket.InventorySlot,
                            sourceType: equipment, targetType: InventoryType.Equipment);

                        if (inv == null)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                    type: 0));
                            return;
                        }

                        if (inv.Slot != -1) Session.SendPacket(packet: inventory.GenerateInventoryAdd());

                        if (removePacket.Type == 0)
                        {
                            Session.SendPackets(packets: Session.Character.GenerateStatChar());
                            Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateEq());
                            Session.SendPacket(packet: Session.Character.GenerateEquipment());
                            Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GeneratePairy());
                        }
                        else if (mate != null)
                        {
                            switch (inv.Item.EquipmentSlot)
                            {
                                case EquipmentType.Armor:
                                    mate.ArmorInstance = null;
                                    break;

                                case EquipmentType.MainWeapon:
                                    mate.WeaponInstance = null;
                                    break;

                                case EquipmentType.Gloves:
                                    mate.GlovesInstance = null;
                                    break;

                                case EquipmentType.Boots:
                                    mate.BootsInstance = null;
                                    break;

                                case EquipmentType.Sp:
                                    {
                                        if (mate.IsUsingSp)
                                        {
                                            mate.RemoveSp();
                                            mate.StartSpCooldown();
                                        }

                                        mate.Sp = null;
                                    }
                                    break;
                            }

                            mate.BattleEntity.BCards.RemoveAll(match: o => o.ItemVNum == inventory.HoldingVNum);
                            Session.SendPacket(packet: mate.GenerateScPacket());
                        }

                        var ring = Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Ring,
                            type: InventoryType.Wear);
                        var bracelet =
                            Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Bracelet,
                                type: InventoryType.Wear);
                        var necklace =
                            Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Necklace,
                                type: InventoryType.Wear);
                        Session.Character.CellonOptions.Clear();
                        if (ring != null) Session.Character.CellonOptions.AddRange(value: ring.CellonOptions);
                        if (bracelet != null) Session.Character.CellonOptions.AddRange(value: bracelet.CellonOptions);
                        if (necklace != null) Session.Character.CellonOptions.AddRange(value: necklace.CellonOptions);
                        Session.SendPacket(packet: Session.Character.GenerateStat());
                    }
                }
            }
        }

        /// <summary>
        ///     repos packet
        /// </summary>
        /// <param name="reposPacket"></param>
        public void Repos(ReposPacket reposPacket)
        {
            if (reposPacket != null)
            {
                Logger.LogUserEvent(logEvent: "STASH_REPOS", caller: Session.GenerateIdentity(),
                    data:
                    $"[ItemReposition]OldSlot: {reposPacket.OldSlot} NewSlot: {reposPacket.NewSlot} Amount: {reposPacket.Amount} PartnerBackpack: {reposPacket.PartnerBackpack}");

                if (reposPacket.Amount < 1) return;

                if (reposPacket.OldSlot == reposPacket.NewSlot) return;

                // check if the destination slot is out of range
                if (reposPacket.NewSlot >= (reposPacket.PartnerBackpack
                    ? Session.Character.StaticBonusList.Any(predicate: s =>
                        s.StaticBonusType == StaticBonusType.PetBackPack)
                        ? 50
                        : 0
                    : Session.Character.WareHouseSize))
                    return;

                // check if the character is allowed to move the item
                if (Session.Character.InExchangeOrTrade) return;

                // actually move the item from source to destination
                Session.Character.Inventory.MoveItem(
                    sourcetype: reposPacket.PartnerBackpack ? InventoryType.PetWarehouse : InventoryType.Warehouse,
                    desttype: reposPacket.PartnerBackpack ? InventoryType.PetWarehouse : InventoryType.Warehouse,
                    sourceSlot: reposPacket.OldSlot, amount: reposPacket.Amount, destinationSlot: reposPacket.NewSlot,
                    sourceInventory: out var previousInventory,
                    destinationInventory: out var newInventory);

                if (newInventory == null) return;

                Session.SendPacket(packet: reposPacket.PartnerBackpack
                    ? newInventory.GeneratePStash()
                    : newInventory.GenerateStash());
                Session.SendPacket(packet: previousInventory != null
                    ? reposPacket.PartnerBackpack
                        ? previousInventory.GeneratePStash()
                        : previousInventory.GenerateStash()
                    : reposPacket.PartnerBackpack
                        ? UserInterfaceHelper.Instance.GeneratePStashRemove(slot: reposPacket.OldSlot)
                        : UserInterfaceHelper.Instance.GenerateStashRemove(slot: reposPacket.OldSlot));
            }
        }

        /// <summary>
        ///     sortopen packet
        /// </summary>
        /// <param name="sortOpenPacket"></param>
        public void SortOpen(SortOpenPacket sortOpenPacket)
        {
            if (sortOpenPacket != null)
            {
                var gravity = true;
                while (gravity)
                {
                    gravity = false;
                    for (short i = 0; i < 2; i++)
                    {
                        for (short x = 0; x < 44; x++)
                        {
                            var type = i == 0 ? InventoryType.Specialist : InventoryType.Costume;
                            if (Session.Character.Inventory.LoadBySlotAndType(slot: x, type: type) == null
                                && Session.Character.Inventory.LoadBySlotAndType(slot: (short)(x + 1), type: type)
                                != null)
                            {
                                Session.Character.Inventory.MoveItem(sourcetype: type, desttype: type,
                                    sourceSlot: (short)(x + 1), amount: 1, destinationSlot: x,
                                    sourceInventory: out var _, destinationInventory: out var invdest);
                                Session.SendPacket(packet: invdest.GenerateInventoryAdd());
                                Session.Character.DeleteItem(type: type, slot: (short)(x + 1));
                                gravity = true;
                            }
                        }

                        Session.Character.Inventory.Reorder(session: Session,
                            inventoryType: i == 0 ? InventoryType.Specialist : InventoryType.Costume);
                    }
                }
            }
        }

        /// <summary>
        ///     s_carrier packet
        /// </summary>
        /// <param name="specialistHolderPacket"></param>
        public void SpecialistHolder(SpecialistHolderPacket specialistHolderPacket)
        {
            if (specialistHolderPacket != null)
            {
                var specialist =
                    Session.Character.Inventory.LoadBySlotAndType(slot: specialistHolderPacket.Slot,
                        type: InventoryType.Equipment);

                var holder = Session.Character.Inventory.LoadBySlotAndType(slot: specialistHolderPacket.HolderSlot,
                    type: InventoryType.Equipment);

                if (specialist?.Item == null || holder?.Item == null) return;

                if (!holder.Item.IsHolder) return;

                if (holder.HoldingVNum > 0) return;

                if (holder.Item.ItemType == ItemType.Box && holder.Item.ItemSubType == 2)
                {
                    if (specialist.Item.ItemType != ItemType.Specialist || !specialist.Item.IsSoldable ||
                        specialist.Item.Class == 0) return;

                    if (specialist.ItemVNum >= 4494 && specialist.ItemVNum <= 4496)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "CANT_HOLD_SP")));
                        Session.SendPacket(packet: "shop_end 2");
                        return;
                    }

                    Session.Character.Inventory.RemoveItemFromInventory(id: specialist.Id);

                    holder.HoldingVNum = specialist.ItemVNum;
                    holder.SlDamage = specialist.SlDamage;
                    holder.SlDefence = specialist.SlDefence;
                    holder.SlElement = specialist.SlElement;
                    holder.SlHp = specialist.SlHp;
                    holder.SpDamage = specialist.SpDamage;
                    holder.SpDark = specialist.SpDark;
                    holder.SpDefence = specialist.SpDefence;
                    holder.SpElement = specialist.SpElement;
                    holder.SpFire = specialist.SpFire;
                    holder.SpHp = specialist.SpHp;
                    holder.SpLevel = specialist.SpLevel;
                    holder.SpLight = specialist.SpLight;
                    holder.SpStoneUpgrade = specialist.SpStoneUpgrade;
                    holder.SpWater = specialist.SpWater;
                    holder.Upgrade = specialist.Upgrade;
                    holder.Xp = specialist.Xp;
                    holder.EquipmentSerialId = specialist.EquipmentSerialId;

                    Session.SendPacket(packet: "shop_end 2");
                }
            }
        }

        /// <summary>
        ///     sl packet
        /// </summary>
        /// <param name="spTransformPacket"></param>
        public void SpTransform(SpTransformPacket spTransformPacket)
        {
            if (spTransformPacket != null && !Session.Character.IsSeal && !Session.Character.IsMorphed)
            {
                var specialistInstance =
                    Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                        type: InventoryType.Wear);

                if (spTransformPacket.Type == 10)
                {
                    short specialistDamage = spTransformPacket.SpecialistDamage,
                        specialistDefense = spTransformPacket.SpecialistDefense,
                        specialistElement = spTransformPacket.SpecialistElement,
                        specialistHealpoints = spTransformPacket.SpecialistHP;
                    var transportId = spTransformPacket.TransportId;
                    if (!Session.Character.UseSp || specialistInstance == null
                                                 || transportId != specialistInstance.TransportId)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "SPUSE_NEEDED"), type: 0));
                        return;
                    }

                    if (CharacterHelper.SPPoint(spLevel: specialistInstance.SpLevel,
                            upgrade: specialistInstance.Upgrade)
                        - specialistInstance.SlDamage - specialistInstance.SlHp - specialistInstance.SlElement
                        - specialistInstance.SlDefence - specialistDamage - specialistDefense - specialistElement
                        - specialistHealpoints < 0)
                        return;

                    if (specialistDamage < 0 || specialistDefense < 0 || specialistElement < 0
                        || specialistHealpoints < 0)
                        return;

                    specialistInstance.SlDamage += specialistDamage;
                    specialistInstance.SlDefence += specialistDefense;
                    specialistInstance.SlElement += specialistElement;
                    specialistInstance.SlHp += specialistHealpoints;

                    var mainWeapon =
                        Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon,
                            type: InventoryType.Wear);
                    var secondaryWeapon =
                        Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon,
                            type: InventoryType.Wear);
                    var effects = new List<ShellEffectDto>();
                    if (mainWeapon?.ShellEffects != null) effects.AddRange(collection: mainWeapon.ShellEffects);

                    if (secondaryWeapon?.ShellEffects != null)
                        effects.AddRange(collection: secondaryWeapon.ShellEffects);

                    int GetShellWeaponEffectValue(ShellWeaponEffectType effectType)
                    {
                        return effects.Where(predicate: s => s.Effect == (byte)effectType)
                            .OrderByDescending(keySelector: s => s.Value)
                            .FirstOrDefault()?.Value ?? 0;
                    }

                    var slElement = CharacterHelper.SlPoint(spPoint: specialistInstance.SlElement, mode: 2)
                                    + GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlElement)
                                    + GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlGlobal);
                    var slHp = CharacterHelper.SlPoint(spPoint: specialistInstance.SlHp, mode: 3)
                               + GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.Slhp)
                               + GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlGlobal);
                    var slDefence = CharacterHelper.SlPoint(spPoint: specialistInstance.SlDefence, mode: 1)
                                    + GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlDefence)
                                    + GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlGlobal);
                    var slHit = CharacterHelper.SlPoint(spPoint: specialistInstance.SlDamage, mode: 0)
                                + GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlDamage)
                                + GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlGlobal);

                    #region slHit

                    specialistInstance.DamageMinimum = 0;
                    specialistInstance.DamageMaximum = 0;
                    specialistInstance.HitRate = 0;
                    specialistInstance.CriticalLuckRate = 0;
                    specialistInstance.CriticalRate = 0;
                    specialistInstance.DefenceDodge = 0;
                    specialistInstance.DistanceDefenceDodge = 0;
                    specialistInstance.ElementRate = 0;
                    specialistInstance.DarkResistance = 0;
                    specialistInstance.LightResistance = 0;
                    specialistInstance.FireResistance = 0;
                    specialistInstance.WaterResistance = 0;
                    specialistInstance.CriticalDodge = 0;
                    specialistInstance.CloseDefence = 0;
                    specialistInstance.DistanceDefence = 0;
                    specialistInstance.MagicDefence = 0;
                    specialistInstance.Hp = 0;
                    specialistInstance.Mp = 0;

                    if (slHit >= 1)
                    {
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                    }

                    if (slHit >= 10) specialistInstance.HitRate += 10;

                    if (slHit >= 20) specialistInstance.CriticalLuckRate += 2;

                    if (slHit >= 30)
                    {
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                        specialistInstance.HitRate += 10;
                    }

                    if (slHit >= 40) specialistInstance.CriticalRate += 10;

                    if (slHit >= 50)
                    {
                        specialistInstance.Hp += 200;
                        specialistInstance.Mp += 200;
                    }

                    if (slHit >= 60) specialistInstance.HitRate += 15;

                    if (slHit >= 70)
                    {
                        specialistInstance.HitRate += 15;
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                    }

                    if (slHit >= 80) specialistInstance.CriticalLuckRate += 3;

                    if (slHit >= 90) specialistInstance.CriticalRate += 20;

                    if (slHit >= 100)
                    {
                        specialistInstance.CriticalLuckRate += 3;
                        specialistInstance.CriticalRate += 20;
                        specialistInstance.Hp += 200;
                        specialistInstance.Mp += 200;
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                        specialistInstance.HitRate += 20;
                    }

                    #endregion

                    #region slDefence

                    if (slDefence >= 10)
                    {
                        specialistInstance.DefenceDodge += 5;
                        specialistInstance.DistanceDefenceDodge += 5;
                    }

                    if (slDefence >= 20) specialistInstance.CriticalDodge += 2;

                    if (slDefence >= 30) specialistInstance.Hp += 100;

                    if (slDefence >= 40) specialistInstance.CriticalDodge += 2;

                    if (slDefence >= 50)
                    {
                        specialistInstance.DefenceDodge += 5;
                        specialistInstance.DistanceDefenceDodge += 5;
                    }

                    if (slDefence >= 60) specialistInstance.Hp += 200;

                    if (slDefence >= 70) specialistInstance.CriticalDodge += 3;

                    if (slDefence >= 75)
                    {
                        specialistInstance.FireResistance += 2;
                        specialistInstance.WaterResistance += 2;
                        specialistInstance.LightResistance += 2;
                        specialistInstance.DarkResistance += 2;
                    }

                    if (slDefence >= 80)
                    {
                        specialistInstance.DefenceDodge += 10;
                        specialistInstance.DistanceDefenceDodge += 10;
                        specialistInstance.CriticalDodge += 3;
                    }

                    if (slDefence >= 90)
                    {
                        specialistInstance.FireResistance += 3;
                        specialistInstance.WaterResistance += 3;
                        specialistInstance.LightResistance += 3;
                        specialistInstance.DarkResistance += 3;
                    }

                    if (slDefence >= 95) specialistInstance.Hp += 300;

                    if (slDefence >= 100)
                    {
                        specialistInstance.DefenceDodge += 20;
                        specialistInstance.DistanceDefenceDodge += 20;
                        specialistInstance.FireResistance += 5;
                        specialistInstance.WaterResistance += 5;
                        specialistInstance.LightResistance += 5;
                        specialistInstance.DarkResistance += 5;
                    }

                    #endregion

                    #region slHp

                    if (slHp >= 5)
                    {
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                    }

                    if (slHp >= 10)
                    {
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                    }

                    if (slHp >= 15)
                    {
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                    }

                    if (slHp >= 20)
                    {
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                        specialistInstance.CloseDefence += 10;
                        specialistInstance.DistanceDefence += 10;
                        specialistInstance.MagicDefence += 10;
                    }

                    if (slHp >= 25)
                    {
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                    }

                    if (slHp >= 30)
                    {
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                    }

                    if (slHp >= 35)
                    {
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                    }

                    if (slHp >= 40)
                    {
                        specialistInstance.DamageMinimum += 5;
                        specialistInstance.DamageMaximum += 5;
                        specialistInstance.CloseDefence += 15;
                        specialistInstance.DistanceDefence += 15;
                        specialistInstance.MagicDefence += 15;
                    }

                    if (slHp >= 45)
                    {
                        specialistInstance.DamageMinimum += 10;
                        specialistInstance.DamageMaximum += 10;
                    }

                    if (slHp >= 50)
                    {
                        specialistInstance.DamageMinimum += 10;
                        specialistInstance.DamageMaximum += 10;
                        specialistInstance.FireResistance += 2;
                        specialistInstance.WaterResistance += 2;
                        specialistInstance.LightResistance += 2;
                        specialistInstance.DarkResistance += 2;
                    }

                    if (slHp >= 55)
                    {
                        specialistInstance.DamageMinimum += 10;
                        specialistInstance.DamageMaximum += 10;
                    }

                    if (slHp >= 60)
                    {
                        specialistInstance.DamageMinimum += 10;
                        specialistInstance.DamageMaximum += 10;
                    }

                    if (slHp >= 65)
                    {
                        specialistInstance.DamageMinimum += 10;
                        specialistInstance.DamageMaximum += 10;
                    }

                    if (slHp >= 70)
                    {
                        specialistInstance.DamageMinimum += 10;
                        specialistInstance.DamageMaximum += 10;
                        specialistInstance.CloseDefence += 20;
                        specialistInstance.DistanceDefence += 20;
                        specialistInstance.MagicDefence += 20;
                    }

                    if (slHp >= 75)
                    {
                        specialistInstance.DamageMinimum += 15;
                        specialistInstance.DamageMaximum += 15;
                    }

                    if (slHp >= 80)
                    {
                        specialistInstance.DamageMinimum += 15;
                        specialistInstance.DamageMaximum += 15;
                    }

                    if (slHp >= 85)
                    {
                        specialistInstance.DamageMinimum += 15;
                        specialistInstance.DamageMaximum += 15;
                        specialistInstance.CriticalDodge++;
                    }

                    if (slHp >= 86) specialistInstance.CriticalDodge++;

                    if (slHp >= 87) specialistInstance.CriticalDodge++;

                    if (slHp >= 88) specialistInstance.CriticalDodge++;

                    if (slHp >= 90)
                    {
                        specialistInstance.DamageMinimum += 15;
                        specialistInstance.DamageMaximum += 15;
                        specialistInstance.CloseDefence += 25;
                        specialistInstance.DistanceDefence += 25;
                        specialistInstance.MagicDefence += 25;
                    }

                    if (slHp >= 91)
                    {
                        specialistInstance.DefenceDodge += 2;
                        specialistInstance.DistanceDefenceDodge += 2;
                    }

                    if (slHp >= 92)
                    {
                        specialistInstance.DefenceDodge += 2;
                        specialistInstance.DistanceDefenceDodge += 2;
                    }

                    if (slHp >= 93)
                    {
                        specialistInstance.DefenceDodge += 2;
                        specialistInstance.DistanceDefenceDodge += 2;
                    }

                    if (slHp >= 94)
                    {
                        specialistInstance.DefenceDodge += 2;
                        specialistInstance.DistanceDefenceDodge += 2;
                    }

                    if (slHp >= 95)
                    {
                        specialistInstance.DamageMinimum += 20;
                        specialistInstance.DamageMaximum += 20;
                        specialistInstance.DefenceDodge += 2;
                        specialistInstance.DistanceDefenceDodge += 2;
                    }

                    if (slHp >= 96)
                    {
                        specialistInstance.DefenceDodge += 2;
                        specialistInstance.DistanceDefenceDodge += 2;
                    }

                    if (slHp >= 97)
                    {
                        specialistInstance.DefenceDodge += 2;
                        specialistInstance.DistanceDefenceDodge += 2;
                    }

                    if (slHp >= 98)
                    {
                        specialistInstance.DefenceDodge += 2;
                        specialistInstance.DistanceDefenceDodge += 2;
                    }

                    if (slHp >= 99)
                    {
                        specialistInstance.DefenceDodge += 2;
                        specialistInstance.DistanceDefenceDodge += 2;
                    }

                    if (slHp >= 100)
                    {
                        specialistInstance.FireResistance += 3;
                        specialistInstance.WaterResistance += 3;
                        specialistInstance.LightResistance += 3;
                        specialistInstance.DarkResistance += 3;
                        specialistInstance.CloseDefence += 30;
                        specialistInstance.DistanceDefence += 30;
                        specialistInstance.MagicDefence += 30;
                        specialistInstance.DamageMinimum += 20;
                        specialistInstance.DamageMaximum += 20;
                        specialistInstance.DefenceDodge += 2;
                        specialistInstance.DistanceDefenceDodge += 2;
                        specialistInstance.CriticalDodge++;
                    }

                    #endregion

                    #region slElement

                    if (slElement >= 1) specialistInstance.ElementRate += 2;

                    if (slElement >= 10) specialistInstance.Mp += 100;

                    if (slElement >= 20) specialistInstance.MagicDefence += 5;

                    if (slElement >= 30)
                    {
                        specialistInstance.FireResistance += 2;
                        specialistInstance.WaterResistance += 2;
                        specialistInstance.LightResistance += 2;
                        specialistInstance.DarkResistance += 2;
                        specialistInstance.ElementRate += 2;
                    }

                    if (slElement >= 40) specialistInstance.Mp += 100;

                    if (slElement >= 50) specialistInstance.MagicDefence += 5;

                    if (slElement >= 60)
                    {
                        specialistInstance.FireResistance += 3;
                        specialistInstance.WaterResistance += 3;
                        specialistInstance.LightResistance += 3;
                        specialistInstance.DarkResistance += 3;
                        specialistInstance.ElementRate += 2;
                    }

                    if (slElement >= 70) specialistInstance.Mp += 100;

                    if (slElement >= 80) specialistInstance.MagicDefence += 5;

                    if (slElement >= 90)
                    {
                        specialistInstance.FireResistance += 4;
                        specialistInstance.WaterResistance += 4;
                        specialistInstance.LightResistance += 4;
                        specialistInstance.DarkResistance += 4;
                        specialistInstance.ElementRate += 2;
                    }

                    if (slElement >= 100)
                    {
                        specialistInstance.FireResistance += 6;
                        specialistInstance.WaterResistance += 6;
                        specialistInstance.LightResistance += 6;
                        specialistInstance.DarkResistance += 6;
                        specialistInstance.MagicDefence += 5;
                        specialistInstance.Mp += 200;
                        specialistInstance.ElementRate += 2;
                    }

                    #endregion

                    Session.SendPackets(packets: Session.Character.GenerateStatChar());
                    Session.SendPacket(packet: Session.Character.GenerateStat());
                    Session.SendPacket(packet: specialistInstance.GenerateSlInfo(session: Session));
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "POINTS_SET"), type: 0));
                }
                else if (!Session.Character.IsSitting)
                {
                    if (Session.Character.Buff.Any(predicate: s => s.Card.BuffType == BuffType.Bad))
                    {
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "CANT_TRASFORM_WITH_DEBUFFS"),
                            type: 0));
                        return;
                    }

                    if (Session.Character.Skills.Any(predicate: s => !s.CanBeUsed()))
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "SKILLS_IN_LOADING"),
                                type: 0));
                        return;
                    }

                    if (specialistInstance == null)
                    {
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "NO_SP"),
                            type: 0));
                        return;
                    }

                    if (Session.Character.IsVehicled)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "REMOVE_VEHICLE"), type: 0));
                        return;
                    }

                    var currentRunningSeconds =
                        (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(value: -50)).TotalSeconds;

                    if (Session.Character.UseSp)
                    {
                        if (Session.Character.Timespace != null &&
                            Session.Character.Timespace.SpNeeded?[(byte)Session.Character.Class] != 0 &&
                            Session.Character.Timespace.InstanceBag.Lock) return;
                        Session.Character.LastSp = currentRunningSeconds;
                        Session.Character.RemoveSp(vnum: specialistInstance.ItemVNum, forced: false);
                    }
                    else
                    {
                        if (Session.Character.LastMove.AddSeconds(value: 1) >= DateTime.Now
                            || Session.Character.LastSkillUse.AddSeconds(value: 2) >= DateTime.Now)
                            return;

                        if (Session.Character.SpPoint == 0 && Session.Character.SpAdditionPoint == 0)
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "SP_NOPOINTS"), type: 0));

                        var timeSpanSinceLastSpUsage = currentRunningSeconds - Session.Character.LastSp;
                        if (timeSpanSinceLastSpUsage >= Session.Character.SpCooldown)
                        {
                            if (spTransformPacket.Type == 1)
                            {
                                var delay = DateTime.Now.AddSeconds(value: -6);
                                if (Session.Character.LastDelay > delay
                                    && Session.Character.LastDelay < delay.AddSeconds(value: 2))
                                    ChangeSp();
                            }
                            else
                            {
                                Session.Character.LastDelay = DateTime.Now;
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateDelay(delay: 5000, type: 3, argument: "#sl^1"));
                                Session.CurrentMapInstance?.Broadcast(
                                    packet: UserInterfaceHelper.GenerateGuri(type: 2, argument: 1,
                                        callerId: Session.Character.CharacterId),
                                    xRangeCoordinate: Session.Character.PositionX,
                                    yRangeCoordinate: Session.Character.PositionY);
                            }
                        }
                        else
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SP_INLOADING"),
                                    arg0: Session.Character.SpCooldown -
                                          (int)Math.Round(value: timeSpanSinceLastSpUsage, digits: 0)), type: 0));
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     up_gr packet
        /// </summary>
        /// <param name="upgradePacket"></param>
        public void Upgrade(UpgradePacket upgradePacket)
        {
            if (upgradePacket == null || Session.Character.ExchangeInfo?.ExchangeList.Count > 0
                                      || Session.Character.Speed == 0 ||
                                      Session.Character.LastDelay.AddSeconds(value: 5) > DateTime.Now)
                return;

            var inventoryType = upgradePacket.InventoryType;
            byte uptype = upgradePacket.UpgradeType, slot = upgradePacket.Slot;
            Session.Character.LastDelay = DateTime.Now;
            ItemInstance inventory;
            switch (uptype)
            {
                case 0:
                    inventory = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (inventory != null)
                        if ((inventory.Item.EquipmentSlot == EquipmentType.Armor
                             || inventory.Item.EquipmentSlot == EquipmentType.MainWeapon
                             || inventory.Item.EquipmentSlot == EquipmentType.SecondaryWeapon)
                            && inventory.Item.ItemType != ItemType.Shell &&
                            inventory.Item.Type == InventoryType.Equipment)
                            inventory.ConvertToPartnerEquipment(session: Session);
                    break;

                case 1:
                    inventory = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (inventory != null)
                        if ((inventory.Item.EquipmentSlot == EquipmentType.Armor
                             || inventory.Item.EquipmentSlot == EquipmentType.MainWeapon
                             || inventory.Item.EquipmentSlot == EquipmentType.SecondaryWeapon)
                            && inventory.Item.ItemType != ItemType.Shell &&
                            inventory.Item.Type == InventoryType.Equipment)
                            inventory.UpgradeItem(session: Session, mode: UpgradeMode.Normal,
                                protection: UpgradeProtection.None);
                    break;

                case 3:

                    //up_gr 3 0 0 7 1 1 20 99
                    var originalSplit = upgradePacket.OriginalContent.Split(' ');
                    if (originalSplit.Length == 10
                        && byte.TryParse(s: originalSplit[5], result: out var firstSlot)
                        && byte.TryParse(s: originalSplit[8], result: out var secondSlot))
                    {
                        inventory = Session.Character.Inventory.LoadBySlotAndType(slot: firstSlot,
                            type: InventoryType.Equipment);
                        if (inventory != null
                            && (inventory.Item.EquipmentSlot == EquipmentType.Necklace
                                || inventory.Item.EquipmentSlot == EquipmentType.Bracelet
                                || inventory.Item.EquipmentSlot == EquipmentType.Ring)
                            && inventory.Item.ItemType != ItemType.Shell &&
                            inventory.Item.Type == InventoryType.Equipment)
                        {
                            var cellon =
                                Session.Character.Inventory.LoadBySlotAndType(slot: secondSlot,
                                    type: InventoryType.Main);
                            if (cellon?.ItemVNum > 1016 && cellon.ItemVNum < 1027)
                                inventory.OptionItem(session: Session, cellonVNum: cellon.ItemVNum);
                        }
                    }

                    break;

                case 7:
                    inventory = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (inventory != null)
                    {
                        if (inventory.Item.EquipmentSlot == EquipmentType.Armor ||
                            inventory.Item.EquipmentSlot == EquipmentType.MainWeapon ||
                            inventory.Item.EquipmentSlot == EquipmentType.SecondaryWeapon)
                        {
                            var mode = RarifyMode.Normal;
                            var protection = RarifyProtection.None;
                            var amulet = Session.Character.Inventory.LoadBySlotAndType(
                                slot: (short)EquipmentType.Amulet,
                                type: InventoryType.Wear);
                            if (amulet != null)
                                switch (amulet.Item.Effect)
                                {
                                    case 791:
                                        protection = RarifyProtection.RedAmulet;
                                        break;
                                    case 792:
                                        protection = RarifyProtection.BlueAmulet;
                                        break;
                                    case 794:
                                        protection = RarifyProtection.HeroicAmulet;
                                        break;
                                    case 795:
                                        protection = RarifyProtection.RandomHeroicAmulet;
                                        break;
                                    case 796:
                                        if (inventory.Item.IsHeroic) mode = RarifyMode.Success;
                                        break;
                                }

                            inventory.RarifyItem(session: Session, mode: mode, protection: protection);
                        }

                        Session.SendPacket(packet: "shop_end 1");
                    }

                    break;

                case 8:
                    inventory = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (upgradePacket.InventoryType2 != null && upgradePacket.Slot2 != null)
                    {
                        var inventory2 =
                            Session.Character.Inventory.LoadBySlotAndType(slot: (byte)upgradePacket.Slot2,
                                type: (InventoryType)upgradePacket.InventoryType2);

                        if (inventory != null && inventory2 != null && !Equals(objA: inventory, objB: inventory2))
                            inventory.Sum(session: Session, itemToSum: inventory2);
                    }

                    break;

                case 9:
                    var specialist = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (specialist != null)
                    {
                        if (specialist.Rare != -2)
                        {
                            if (specialist.Item.EquipmentSlot == EquipmentType.Sp)
                                specialist.UpgradeSp(session: Session, protect: UpgradeProtection.None);
                        }
                        else
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "CANT_UPGRADE_DESTROYED_SP"),
                                type: 0));
                        }
                    }

                    break;

                case 20:
                    inventory = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (inventory != null)
                        if ((inventory.Item.EquipmentSlot == EquipmentType.Armor
                             || inventory.Item.EquipmentSlot == EquipmentType.MainWeapon
                             || inventory.Item.EquipmentSlot == EquipmentType.SecondaryWeapon)
                            && inventory.Item.ItemType != ItemType.Shell &&
                            inventory.Item.Type == InventoryType.Equipment)
                            inventory.UpgradeItem(session: Session, mode: UpgradeMode.Normal,
                                protection: UpgradeProtection.Protected);
                    break;

                case 21:
                    inventory = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (inventory != null)
                        if ((inventory.Item.EquipmentSlot == EquipmentType.Armor
                             || inventory.Item.EquipmentSlot == EquipmentType.MainWeapon
                             || inventory.Item.EquipmentSlot == EquipmentType.SecondaryWeapon)
                            && inventory.Item.ItemType != ItemType.Shell &&
                            inventory.Item.Type == InventoryType.Equipment)
                            inventory.RarifyItem(session: Session, mode: RarifyMode.Normal,
                                protection: RarifyProtection.Scroll);
                    break;

                case 25:
                    specialist = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (specialist != null)
                    {
                        if (specialist.Rare != -2)
                        {
                            if (specialist.Upgrade > 9)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "MUST_USE_ITEM"),
                                        arg0: ServerManager.GetItem(vnum: 1364).Name), type: 0));
                                return;
                            }

                            if (specialist.Item.EquipmentSlot == EquipmentType.Sp)
                                specialist.UpgradeSp(session: Session, protect: UpgradeProtection.Protected);
                        }
                        else
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "CANT_UPGRADE_DESTROYED_SP"),
                                type: 0));
                        }
                    }

                    break;

                case 26:
                    specialist = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (specialist != null)
                    {
                        if (specialist.Rare != -2)
                        {
                            if (specialist.Upgrade <= 9)
                            {
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "MUST_USE_ITEM"),
                                        arg0: ServerManager.GetItem(vnum: 1363).Name), type: 0));
                                return;
                            }

                            if (specialist.Item.EquipmentSlot == EquipmentType.Sp)
                                specialist.UpgradeSp(session: Session, protect: UpgradeProtection.Protected);
                        }
                        else
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "CANT_UPGRADE_DESTROYED_SP"),
                                type: 0));
                        }
                    }

                    break;

                case 38:
                    specialist = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (specialist != null)
                    {
                        if (specialist.Rare != -2)
                        {
                            if (specialist.Item.EquipmentSlot == EquipmentType.Sp)
                                specialist.UpgradeSp(session: Session, protect: UpgradeProtection.Event);
                        }
                        else
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "CANT_UPGRADE_DESTROYED_SP"),
                                type: 0));
                        }
                    }

                    break;

                //auto Perfection
                case 41:
                    specialist = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (specialist != null)
                    {
                        if (specialist.Rare != -2)
                        {
                            if (specialist.Item.EquipmentSlot == EquipmentType.Sp)
                            {
                                var start = DateTime.Now;
                                while (DateTime.Now - start < TimeSpan.FromSeconds(value: 2))
                                    specialist.PerfectSp(session: Session);
                            }
                        }
                        else
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "CANT_UPGRADE_DESTROYED_SP"),
                                type: 0));
                        }
                    }

                    break; //Activate for Autoperfection

                /*case 41:
                    specialist = Session.Character.Inventory.LoadBySlotAndType(slot, inventoryType);
                    if (specialist != null)
                    {
                        if (specialist.Rare != -2)
                        {
                            if (specialist.Item.EquipmentSlot == EquipmentType.Sp)
                            {
                                specialist.PerfectSP(Session);
                            }
                        }
                        else
                        {
                            Session.SendPacket(UserInterfaceHelper.GenerateMsg(
                                Language.Instance.GetMessageFromKey("CANT_UPGRADE_DESTROYED_SP"), 0));
                        }
                    }
                    break;*/ //Activate dont Autoperfection

                case 43:
                    inventory = Session.Character.Inventory.LoadBySlotAndType(slot: slot, type: inventoryType);
                    if (inventory != null)
                        if ((inventory.Item.EquipmentSlot == EquipmentType.Armor
                             || inventory.Item.EquipmentSlot == EquipmentType.MainWeapon
                             || inventory.Item.EquipmentSlot == EquipmentType.SecondaryWeapon)
                            && inventory.Item.ItemType != ItemType.Shell &&
                            inventory.Item.Type == InventoryType.Equipment)
                            inventory.UpgradeItem(session: Session, mode: UpgradeMode.Reduced,
                                protection: UpgradeProtection.Protected);
                    break;
            }
        }

        /// <summary>
        ///     u_i packet
        /// </summary>
        /// <param name="useItemPacket"></param>
        public void UseItem(UseItemPacket useItemPacket)
        {
            if (useItemPacket == null || (byte)useItemPacket.Type >= 9) return;

            var itemInstance =
                Session.Character.Inventory.LoadBySlotAndType(slot: useItemPacket.Slot, type: useItemPacket.Type);

            var packet = useItemPacket.OriginalContent.Split(' ', '^');

            if (packet.Length >= 2 && packet[1].Length > 0)
                itemInstance?.Item.Use(session: Session, inv: ref itemInstance,
                    option: packet[1][index: 0] == '#' ? (byte)255 : (byte)0, packetsplit: packet);
        }

        /// <summary>
        ///     wear packet
        /// </summary>
        /// <param name="wearPacket"></param>
        public void Wear(WearPacket wearPacket)
        {
            if (wearPacket == null || Session.Character.ExchangeInfo?.ExchangeList.Count > 0
                                   || Session.Character.Speed == 0)
                return;

            if (Session.HasCurrentMapInstance && Session.CurrentMapInstance.UserShops
                    .FirstOrDefault(predicate: mapshop =>
                        mapshop.Value.OwnerId.Equals(obj: Session.Character.CharacterId)).Value
                == null)
            {
                var inv =
                    Session.Character.Inventory.LoadBySlotAndType(slot: wearPacket.InventorySlot,
                        type: InventoryType.Equipment);
                if (inv?.Item != null)
                {
                    inv.Item.Use(session: Session, inv: ref inv, option: wearPacket.Type);
                    Session.Character.LoadSpeed();
                    Session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                        callerId: Session.Character.CharacterId,
                        effectId: 123));

                    var ring = Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Ring,
                        type: InventoryType.Wear);
                    var bracelet =
                        Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Bracelet,
                            type: InventoryType.Wear);
                    var necklace =
                        Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Necklace,
                            type: InventoryType.Wear);
                    Session.Character.CellonOptions.Clear();
                    if (ring != null) Session.Character.CellonOptions.AddRange(value: ring.CellonOptions);
                    if (bracelet != null) Session.Character.CellonOptions.AddRange(value: bracelet.CellonOptions);
                    if (necklace != null) Session.Character.CellonOptions.AddRange(value: necklace.CellonOptions);
                    Session.SendPacket(packet: Session.Character.GenerateStat());
                }
            }
        }

        /// <summary>
        ///     withdraw packet
        /// </summary>
        /// <param name="withdrawPacket"></param>
        public void Withdraw(WithdrawPacket withdrawPacket)
        {
            if (withdrawPacket != null)
            {
                var previousInventory = Session.Character.Inventory.LoadBySlotAndType(slot: withdrawPacket.Slot,
                    type: withdrawPacket.PetBackpack ? InventoryType.PetWarehouse : InventoryType.Warehouse);
                if (withdrawPacket.Amount <= 0 || previousInventory == null
                                               || withdrawPacket.Amount > previousInventory.Amount
                                               || !Session.Character.Inventory.CanAddItem(
                                                   itemVnum: previousInventory.ItemVNum))
                    return;

                var item2 = previousInventory.DeepCopy();
                item2.Id = Guid.NewGuid();
                item2.Amount = withdrawPacket.Amount;
                Logger.LogUserEvent(logEvent: "STASH_WITHDRAW", caller: Session.GenerateIdentity(),
                    data:
                    $"[Withdraw]OldIIId: {previousInventory.Id} NewIIId: {item2.Id} Amount: {withdrawPacket.Amount} PartnerBackpack: {withdrawPacket.PetBackpack}");
                Session.Character.Inventory.RemoveItemFromInventory(id: previousInventory.Id,
                    amount: withdrawPacket.Amount);
                Session.Character.Inventory.AddToInventory(newItem: item2, type: item2.Item.Type);
                Session.Character.Inventory.LoadBySlotAndType(slot: withdrawPacket.Slot,
                    type: withdrawPacket.PetBackpack ? InventoryType.PetWarehouse : InventoryType.Warehouse);
                if (previousInventory.Amount > 0)
                    Session.SendPacket(packet: withdrawPacket.PetBackpack
                        ? previousInventory.GeneratePStash()
                        : previousInventory.GenerateStash());
                else
                    Session.SendPacket(packet: withdrawPacket.PetBackpack
                        ? UserInterfaceHelper.Instance.GeneratePStashRemove(slot: withdrawPacket.Slot)
                        : UserInterfaceHelper.Instance.GenerateStashRemove(slot: withdrawPacket.Slot));
            }
        }

        /// <summary>
        ///     changesp private method
        /// </summary>
        void ChangeSp()
        {
            var sp =
                Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);
            var fairy =
                Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Fairy,
                    type: InventoryType.Wear);
            if (sp != null)
            {
                if (Session.Character.GetReputationIco() < sp.Item.ReputationMinimum)
                {
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "LOW_REP"),
                        type: 0));
                    return;
                }

                if (fairy != null && sp.Item.Element != 0 && fairy.Item.Element != sp.Item.Element
                    && fairy.Item.Element != sp.Item.SecondaryElement)
                {
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "BAD_FAIRY"),
                        type: 0));
                    return;
                }

                if (new[] { 4494, 4495, 4496 }.Contains(value: sp.ItemVNum))
                {
                    if (Session.Character.Timespace == null) return;

                    if (ServerManager.Instance.TimeSpaces.Any(predicate: s =>
                        s.SpNeeded?[(byte)Session.Character.Class] == sp.ItemVNum))
                    {
                        if (Session.Character.Timespace.SpNeeded?[(byte)Session.Character.Class] !=
                            sp.ItemVNum) return;
                    }
                    else
                    {
                        return;
                    }
                }

                Session.Character.DisableBuffs(type: BuffType.All);
                Session.Character.EquipmentBCards.AddRange(collection: sp.Item.BCards);
                Session.Character.LastTransform = DateTime.Now;
                Session.Character.UseSp = true;
                Session.Character.Morph = sp.Item.Morph;
                Session.Character.MorphUpgrade = sp.Upgrade;
                Session.Character.MorphUpgrade2 = sp.Design;
                Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateCMode());
                Session.SendPacket(packet: Session.Character.GenerateLev());
                Session.CurrentMapInstance?.Broadcast(
                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                        callerId: Session.Character.CharacterId, effectId: 196),
                    xRangeCoordinate: Session.Character.PositionX, yRangeCoordinate: Session.Character.PositionY);
                Session.CurrentMapInstance?.Broadcast(
                    packet: UserInterfaceHelper.GenerateGuri(type: 6, argument: 1,
                        callerId: Session.Character.CharacterId), xRangeCoordinate: Session.Character.PositionX,
                    yRangeCoordinate: Session.Character.PositionY);
                Session.SendPacket(packet: Session.Character.GenerateSpPoint());
                Session.Character.LoadSpeed();
                Session.SendPacket(packet: Session.Character.GenerateCond());
                Session.SendPacket(packet: Session.Character.GenerateStat());
                Session.SendPackets(packets: Session.Character.GenerateStatChar());
                Session.Character.SkillsSp = new ThreadSafeSortedList<int, CharacterSkill>();
                Parallel.ForEach(source: ServerManager.GetAllSkill(), body: skill =>
                {
                    if (skill.Class == Session.Character.Morph + 31 && sp.SpLevel >= skill.LevelMinimum)
                        Session.Character.SkillsSp[key: skill.SkillVNum] = new CharacterSkill
                        {
                            SkillVNum = skill.SkillVNum,
                            CharacterId = Session.Character.CharacterId
                        };
                });
                Session.SendPacket(packet: Session.Character.GenerateSki());
                Session.SendPackets(packets: Session.Character.GenerateQuicklist());
                Logger.LogUserEvent(logEvent: "CHARACTER_SPECIALIST_CHANGE", caller: Session.GenerateIdentity(),
                    data: $"Specialist: {sp.Item.Morph}");
            }
        }

        /// <summary>
        ///     exchange closure method
        /// </summary>
        /// <param name="session"></param>
        /// <param name="targetSession"></param>
        static void CloseExchange(ClientSession session, ClientSession targetSession)
        {
            if (targetSession?.Character.ExchangeInfo != null)
            {
                targetSession.SendPacket(packet: "exc_close 0");
                targetSession.Character.ExchangeInfo = null;
            }

            if (session?.Character.ExchangeInfo != null)
            {
                session.SendPacket(packet: "exc_close 0");
                session.Character.ExchangeInfo = null;
            }
        }

        /// <summary>
        ///     exchange initialization method
        /// </summary>
        /// <param name="sourceSession"></param>
        /// <param name="targetSession"></param>
        static void Exchange(ClientSession sourceSession, ClientSession targetSession)
        {
            if (sourceSession?.Character.ExchangeInfo == null) return;

            var data = "";

            // remove all items from source session
            foreach (var item in sourceSession.Character.ExchangeInfo.ExchangeList)
            {
                var invtemp = sourceSession.Character.Inventory.GetItemInstanceById(id: item.Id);
                if (invtemp?.Amount >= item.Amount)
                    sourceSession.Character.Inventory.RemoveItemFromInventory(id: invtemp.Id, amount: item.Amount);
                else
                    return;
            }

            // add all items to target session
            foreach (var item in sourceSession.Character.ExchangeInfo.ExchangeList)
            {
                var item2 = item.DeepCopy();
                item2.Id = Guid.NewGuid();
                data +=
                    $"[OldIIId: {item.Id} NewIIId: {item2.Id} ItemVNum: {item.ItemVNum} Amount: {item.Amount} Rare: {item.Rare} Upgrade: {item.Upgrade}]";
                var inv = targetSession.Character.Inventory.AddToInventory(newItem: item2);
                if (inv.Count == 0)
                {
                    // do what?
                }
            }

            data += $"[Gold: {sourceSession.Character.ExchangeInfo.Gold}]";
            data += $"[BankGold: {sourceSession.Character.ExchangeInfo.BankGold}]";

            // handle gold
            sourceSession.Character.Gold -= sourceSession.Character.ExchangeInfo.Gold;
            sourceSession.Character.GoldBank -= sourceSession.Character.ExchangeInfo.BankGold;
            sourceSession.SendPacket(packet: sourceSession.Character.GenerateGold());
            targetSession.Character.Gold += sourceSession.Character.ExchangeInfo.Gold;
            targetSession.Character.GoldBank += sourceSession.Character.ExchangeInfo.BankGold;
            targetSession.SendPacket(packet: targetSession.Character.GenerateGold());


            // all items and gold from sourceSession have been transferred, clean exchange info

            Logger.LogUserEvent(logEvent: "TRADE_COMPLETE", caller: sourceSession.GenerateIdentity(),
                data: $"[{targetSession.GenerateIdentity()}]Data: {data}");

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = sourceSession.Character.Name,
                    SenderId = sourceSession.Character.CharacterId,
                    Receiver = targetSession.Character.Name,
                    ReceiverId = targetSession.Character.CharacterId,
                    PacketType = LogType.Trade,
                    Packet = $"[TRADE_COMPLETE]Data: {data}"
                });

            sourceSession.Character.ExchangeInfo = null;
        }

        #endregion
    }
}