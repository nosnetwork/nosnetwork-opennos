﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Handling;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Buff;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.GameObject.Packets.ClientPackets;
using static OpenNos.Domain.BCardType;

namespace OpenNos.Handler
{
    public class MatePacketHandler : IPacketHandler
    {
        #region Properties

        public MatePacketHandler(ClientSession session)
        {
            Session = session;
        }

        ClientSession Session { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     ps_op packet
        /// </summary>
        /// <param name="partnerSkillOpenPacket"></param>
        public void PartnerSkillOpen(PartnerSkillOpenPacket partnerSkillOpenPacket)
        {
            if (partnerSkillOpenPacket == null
                || partnerSkillOpenPacket.CastId < 0
                || partnerSkillOpenPacket.CastId > 2)
                return;

            var mate = Session?.Character?.Mates?.ToList().FirstOrDefault(predicate: s =>
                s.IsTeamMember && s.MateType == MateType.Partner && s.PetId == partnerSkillOpenPacket.PetId);

            if (mate?.Sp == null || mate.IsUsingSp) return;

            if (!mate.Sp.CanLearnSkill()) return;

            var partnerSkill = mate.Sp.GetSkill(castId: partnerSkillOpenPacket.CastId);

            if (partnerSkill != null) return;

            if (partnerSkillOpenPacket.JustDoIt)
            {
                if (mate.Sp.AddSkill(castId: partnerSkillOpenPacket.CastId))
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateModal(
                            message: Language.Instance.GetMessageFromKey(key: "PSP_SKILL_LEARNED"), type: 1));
                    mate.Sp.ResetXp();
                }

                Session.SendPacket(packet: mate.GenerateScPacket());
            }
            else
            {
                Session.SendPacket(
                    packet: $"pdelay 3000 12 #ps_op^{partnerSkillOpenPacket.PetId}^{partnerSkillOpenPacket.CastId}^1");
                Session.CurrentMapInstance?.Broadcast(
                    packet: UserInterfaceHelper.GenerateGuri(type: 2, argument: 2, callerId: mate.MateTransportId),
                    xRangeCoordinate: mate.PositionX, yRangeCoordinate: mate.PositionY);
            }
        }

        /// <summary>
        ///     u_ps packet
        /// </summary>
        /// <param name="usePartnerSkillPacket"></param>
        public void UseSkill(UsePartnerSkillPacket usePartnerSkillPacket)
        {
            #region Invalid packet

            if (usePartnerSkillPacket == null) return;

            #endregion

            #region Mate not found (or invalid)

            var mate = Session?.Character?.Mates?.ToList().FirstOrDefault(predicate: s =>
                s.IsTeamMember && s.MateType == MateType.Partner &&
                s.MateTransportId == usePartnerSkillPacket.TransportId);

            if (mate?.Monster == null) return;

            #endregion

            #region Not using PSP

            if (mate.Sp == null || !mate.IsUsingSp) return;

            #endregion

            #region Skill not found

            var partnerSkill = mate.Sp.GetSkill(castId: usePartnerSkillPacket.CastId);

            if (partnerSkill == null) return;

            #endregion

            #region Convert PartnerSkill to Skill

            var skill = PartnerSkillHelper.ConvertToNormalSkill(partnerSkill: partnerSkill);

            #endregion

            #region Battle entities

            var battleEntityAttacker = mate.BattleEntity;
            BattleEntity battleEntityDefender = null;

            switch (usePartnerSkillPacket.TargetType)
            {
                case UserType.Player:
                    {
                        var target =
                            Session.Character.MapInstance?.GetCharacterById(characterId: usePartnerSkillPacket.TargetId);
                        battleEntityDefender = target?.BattleEntity;
                    }
                    break;

                case UserType.Npc:
                    {
                        var target =
                            Session.Character.MapInstance?.GetMate(mateTransportId: usePartnerSkillPacket.TargetId);
                        battleEntityDefender = target?.BattleEntity;
                    }
                    break;

                case UserType.Monster:
                    {
                        var target =
                            Session.Character.MapInstance?.GetMonsterById(mapMonsterId: usePartnerSkillPacket.TargetId);
                        battleEntityDefender = target?.BattleEntity;
                    }
                    break;
            }

            #endregion

            #region Attack

            PartnerSkillTargetHit(battleEntityAttacker: battleEntityAttacker,
                battleEntityDefender: battleEntityDefender, skill: skill);

            #endregion
        }


        /// <summary>
        ///     u_pet packet
        /// </summary>
        /// <param name="upetPacket"></param>
        public void SpecialSkill(UpetPacket upetPacket)
        {
            if (upetPacket == null) return;

            var penalty = Session.Account.PenaltyLogs.OrderByDescending(keySelector: s => s.DateEnd).FirstOrDefault();
            if (Session.Character.IsMuted() && penalty != null)
            {
                if (Session.Character.Gender == GenderType.Female)
                {
                    Session.CurrentMapInstance?.Broadcast(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "MUTED_FEMALE"), type: 1));
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                            arg0: (penalty.DateEnd - DateTime.Now).ToString(format: "hh\\:mm\\:ss")), type: 11));
                }
                else
                {
                    Session.CurrentMapInstance?.Broadcast(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "MUTED_MALE"), type: 1));
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                            arg0: (penalty.DateEnd - DateTime.Now).ToString(format: "hh\\:mm\\:ss")), type: 11));
                }

                return;
            }

            var attacker =
                Session.Character.Mates.FirstOrDefault(predicate: x => x.MateTransportId == upetPacket.MateTransportId);
            if (attacker == null) return;

            NpcMonsterSkill mateSkill = null;
            if (attacker.Monster.Skills.Any())
                mateSkill = attacker.Monster.Skills.FirstOrDefault(predicate: x => x.Rate == 0);

            if (mateSkill == null)
                mateSkill = new NpcMonsterSkill
                {
                    SkillVNum = 200
                };

            if (attacker.IsSitting) return;

            switch (upetPacket.TargetType)
            {
                case UserType.Monster:
                    if (attacker.Hp > 0)
                    {
                        var target = Session?.CurrentMapInstance?.GetMonsterById(mapMonsterId: upetPacket.TargetId);
                        attacker.TargetHit(target: target.BattleEntity, npcMonsterSkill: mateSkill);
                    }

                    return;

                case UserType.Npc:
                    return;

                case UserType.Player:
                    if (attacker.Hp > 0)
                    {
                        var target = Session?.CurrentMapInstance
                            ?.GetSessionByCharacterId(characterId: upetPacket.TargetId)
                            .Character;
                        attacker.TargetHit(target: target.BattleEntity, npcMonsterSkill: mateSkill);
                    }

                    return;

                case UserType.Object:
                    return;

                default:
                    return;
            }
        }

        /// <summary>
        ///     suctl packet
        /// </summary>
        /// <param name="suctlPacket"></param>
        public void Attack(SuctlPacket suctlPacket)
        {
            if (suctlPacket == null) return;

            if (suctlPacket.TargetType != UserType.Npc
                && Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT"), type: 0));
                return;
            }

            var penalty = Session.Account.PenaltyLogs.OrderByDescending(keySelector: s => s.DateEnd).FirstOrDefault();
            if (Session.Character.IsMuted() && penalty != null)
            {
                if (Session.Character.Gender == GenderType.Female)
                {
                    Session.CurrentMapInstance?.Broadcast(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "MUTED_FEMALE"), type: 1));
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                            arg0: (penalty.DateEnd - DateTime.Now).ToString(format: "hh\\:mm\\:ss")), type: 11));
                }
                else
                {
                    Session.CurrentMapInstance?.Broadcast(
                        packet: Session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "MUTED_MALE"), type: 1));
                    Session.SendPacket(packet: Session.Character.GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                            arg0: (penalty.DateEnd - DateTime.Now).ToString(format: "hh\\:mm\\:ss")), type: 11));
                }

                return;
            }

            var attacker = Session.Character.Mates.Find(match: x => x.MateTransportId == suctlPacket.MateTransportId);

            if (attacker != null &&
                !attacker.HasBuff(type: CardType.SpecialAttack, subtype: (byte)AdditionalTypes.SpecialAttack.NoAttack))
            {
                IEnumerable<NpcMonsterSkill> mateSkills = attacker.Skills;

                if (mateSkills != null)
                {
                    NpcMonsterSkill skill = null;

                    var PossibleSkills = mateSkills.Where(predicate: s =>
                            (DateTime.Now - s.LastSkillUse).TotalMilliseconds >= 1000 * s.Skill.Cooldown || s.Rate == 0)
                        .ToList();

                    foreach (var ski in PossibleSkills.OrderBy(keySelector: rnd => ServerManager.RandomNumber()))
                        if (ski.Rate == 0)
                        {
                            skill = ski;
                        }
                        else if (ServerManager.RandomNumber() < ski.Rate)
                        {
                            skill = ski;
                            break;
                        }

                    switch (suctlPacket.TargetType)
                    {
                        case UserType.Monster:
                            if (attacker.Hp > 0)
                            {
                                var target =
                                    Session.CurrentMapInstance?.GetMonsterById(mapMonsterId: suctlPacket.TargetId);
                                if (target != null)
                                    if (attacker.BattleEntity.CanAttackEntity(receiver: target.BattleEntity))
                                        attacker.TargetHit(target: target.BattleEntity, npcMonsterSkill: skill);
                            }

                            return;

                        case UserType.Npc:
                            if (attacker.Hp > 0)
                            {
                                var target = Session.CurrentMapInstance?.GetMate(mateTransportId: suctlPacket.TargetId);
                                if (target != null)
                                {
                                    if (attacker.Owner.BattleEntity.CanAttackEntity(receiver: target.BattleEntity))
                                        attacker.TargetHit(target: target.BattleEntity, npcMonsterSkill: skill);
                                    else
                                        Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2,
                                            callerId: target.CharacterId));
                                }
                            }

                            return;

                        case UserType.Player:
                            if (attacker.Hp > 0)
                            {
                                var target = Session.CurrentMapInstance
                                    ?.GetSessionByCharacterId(characterId: suctlPacket.TargetId)
                                    ?.Character;
                                if (target != null)
                                {
                                    if (attacker.Owner.BattleEntity.CanAttackEntity(receiver: target.BattleEntity))
                                        attacker.TargetHit(target: target.BattleEntity, npcMonsterSkill: skill);
                                    else
                                        Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2,
                                            callerId: target.CharacterId));
                                }
                            }

                            return;

                        case UserType.Object:
                            return;
                    }
                }
            }
        }

        /// <summary>
        ///     psl packet
        /// </summary>
        /// <param name="pslPacket"></param>
        public void Psl(PslPacket pslPacket)
        {
            var mate = Session?.Character?.Mates?.ToList()
                .Find(match: s => s.IsTeamMember && s.MateType == MateType.Partner);

            if (mate == null) return;

            if (mate.Sp == null)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(message: Language.Instance.GetMessageFromKey(key: "NO_PSP"),
                        type: 0));
                return;
            }

            if (!mate.IsUsingSp && !mate.CanUseSp())
            {
                var spRemainingCooldown = mate.GetSpRemainingCooldown();
                Session.SendPacket(packet: Session.Character.GenerateSay(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "STAY_TIME"),
                        arg0: spRemainingCooldown), type: 11));
                Session.SendPacket(packet: $"psd {spRemainingCooldown}");
                return;
            }

            if (pslPacket.Type == 0)
            {
                if (mate.IsUsingSp)
                {
                    mate.RemoveSp();
                    mate.StartSpCooldown();
                }
                else
                {
                    Session.SendPacket(packet: "pdelay 5000 3 #psl^1");
                    Session.CurrentMapInstance?.Broadcast(
                        packet: UserInterfaceHelper.GenerateGuri(type: 2, argument: 2, callerId: mate.MateTransportId),
                        xRangeCoordinate: mate.PositionX, yRangeCoordinate: mate.PositionY);
                }
            }
            else
            {
                mate.IsUsingSp = true;

                Session.SendPacket(packet: mate.GenerateCond());
                Session.Character.MapInstance.Broadcast(
                    packet: mate.GenerateCMode(morphId: mate.Sp.Instance.Item.Morph));
                Session.SendPacket(packet: mate.Sp.GeneratePski());
                Session.SendPacket(packet: mate.GenerateScPacket());
                Session.Character.MapInstance.Broadcast(packet: mate.GenerateOut());

                var isAct4 = ServerManager.Instance.ChannelId == 51;

                Parallel.ForEach(source: Session.CurrentMapInstance.Sessions.Where(predicate: s => s.Character != null),
                    body: s =>
                    {
                        if (!isAct4 || Session.Character.Faction == s.Character.Faction)
                            s.SendPacket(packet: mate.GenerateIn(hideNickname: false, isAct4: isAct4));
                        else
                            s.SendPacket(packet: mate.GenerateIn(hideNickname: true, isAct4: isAct4,
                                receiverAuthority: s.Account.Authority));
                    });

                Session.SendPacket(packet: Session.Character.GeneratePinit());
                Session.Character.MapInstance.Broadcast(
                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc, callerId: mate.MateTransportId,
                        effectId: 196));
            }
        }

        void PartnerSkillTargetHit(BattleEntity battleEntityAttacker, BattleEntity battleEntityDefender,
            Skill skill, bool isRecursiveCall = false)
        {
            #region Invalid entities

            if (battleEntityAttacker?.MapInstance == null
                || battleEntityAttacker.Mate?.Owner?.BattleEntity == null
                || battleEntityAttacker.Mate.Monster == null)
                return;

            if (battleEntityDefender?.MapInstance == null) return;

            #endregion

            #region Maps NOT matching

            if (battleEntityAttacker.MapInstance != battleEntityDefender.MapInstance) return;

            #endregion

            #region Invalid skill

            if (skill == null) return;

            #endregion

            #region Invalid state

            if (battleEntityAttacker.Hp < 1 || battleEntityAttacker.Mate.IsSitting) return;

            if (battleEntityDefender.Hp < 1) return;

            #endregion

            #region Can NOT attack

            if ((skill.TargetType != 1 || !battleEntityDefender.Equals(obj: battleEntityAttacker)) &&
                !battleEntityAttacker.CanAttackEntity(receiver: battleEntityDefender)
                || battleEntityAttacker.HasBuff(type: CardType.SpecialAttack,
                    subtype: (byte)AdditionalTypes.SpecialAttack.NoAttack))
                return;

            #endregion

            #region Cooldown

            if (!isRecursiveCall && skill.PartnerSkill != null && !skill.PartnerSkill.CanBeUsed()) return;

            #endregion

            #region Enemy too far

            if (skill.TargetType == 0 &&
                battleEntityAttacker.GetDistance(other: battleEntityDefender) > skill.Range) return;

            #endregion

            #region Mp NOT enough

            if (!isRecursiveCall && battleEntityAttacker.Mp < skill.MpCost) return;

            #endregion

            lock (battleEntityDefender.PVELockObject)
            {
                if (!isRecursiveCall)
                {
                    #region Update skill LastUse

                    if (skill.PartnerSkill != null) skill.PartnerSkill.LastUse = DateTime.Now;

                    #endregion

                    #region Decrease MP

                    battleEntityAttacker.DecreaseMp(amount: skill.MpCost);

                    #endregion

                    #region Cast on target

                    battleEntityAttacker.MapInstance.Broadcast(packet: StaticPacketHelper.CastOnTarget(
                        attackerType: battleEntityAttacker.UserType, attackerId: battleEntityAttacker.MapEntityId,
                        defenderType: battleEntityDefender.UserType, defenderId: battleEntityDefender.MapEntityId,
                        castAnimation: skill.CastAnimation, castEffect: skill.CastEffect,
                        skillVNum: skill.SkillVNum));

                    #endregion

                    #region Show icon

                    battleEntityAttacker.MapInstance.Broadcast(
                        packet: StaticPacketHelper.GenerateEff(effectType: battleEntityAttacker.UserType,
                            callerId: battleEntityAttacker.MapEntityId,
                            effectId: 5005));

                    #endregion
                }

                #region Calculate damage

                var hitMode = 0;
                var onyxWings = false;
                var hasAbsorbed = false;

                var damage = DamageHelper.Instance.CalculateDamage(attacker: battleEntityAttacker,
                    defender: battleEntityDefender,
                    skill: skill, hitMode: ref hitMode, onyxWings: ref onyxWings /*, ref hasAbsorbed*/);

                #endregion

                if (hitMode != 4)
                {
                    #region ConvertDamageToHPChance

                    if (battleEntityDefender.Character is Character target)
                    {
                        var convertDamageToHpChance = target.GetBuff(type: CardType.DarkCloneSummon,
                            subtype: (byte)AdditionalTypes.DarkCloneSummon.ConvertDamageToHpChance);

                        if (ServerManager.RandomNumber() < convertDamageToHpChance[0])
                        {
                            var amount = damage;

                            if (target.Hp + amount > target.HPLoad()) amount = (int)target.HPLoad() - target.Hp;

                            target.Hp += amount;
                            target.ConvertedDamageToHP += amount;
                            target.MapInstance.Broadcast(packet: target.GenerateRc(characterHealth: amount));
                            target.Session?.SendPacket(packet: target.GenerateStat());

                            damage = 0;
                        }
                    }

                    #endregion

                    #region InflictDamageToMP

                    if (damage > 0)
                    {
                        var inflictDamageToMp = battleEntityDefender.GetBuff(type: CardType.LightAndShadow,
                            subtype: (byte)AdditionalTypes.LightAndShadow.InflictDamageToMp);

                        if (inflictDamageToMp[0] != 0)
                        {
                            var amount = Math.Min(val1: (int)(damage / 100D * inflictDamageToMp[0]),
                                val2: battleEntityDefender.Mp);
                            battleEntityDefender.DecreaseMp(amount: amount);

                            damage -= amount;
                        }
                    }

                    #endregion
                }

                #region Stand up

                battleEntityDefender.Character?.StandUp();

                #endregion

                #region Cast effect

                var castTime = 0;

                if (!isRecursiveCall && skill.CastEffect != 0)
                {
                    battleEntityAttacker.MapInstance.Broadcast(packet: StaticPacketHelper.GenerateEff(
                            effectType: battleEntityAttacker.UserType, callerId: battleEntityAttacker.MapEntityId,
                            effectId: skill.CastEffect), xRangeCoordinate: battleEntityAttacker.PositionX,
                        yRangeCoordinate: battleEntityAttacker.PositionY);

                    castTime = skill.CastTime * 100;
                }

                #endregion

                #region Use skill

                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: castTime)).Subscribe(onNext: o =>
                    PartnerSkillTargetHit2(battleEntityAttacker: battleEntityAttacker,
                        battleEntityDefender: battleEntityDefender, skill: skill,
                        isRecursiveCall: isRecursiveCall, damage: damage, hitMode: hitMode, hasAbsorbed: hasAbsorbed));

                #endregion
            }
        }

        void PartnerSkillTargetHit2(BattleEntity battleEntityAttacker, BattleEntity battleEntityDefender,
            Skill skill, bool isRecursiveCall, int damage, int hitMode, bool hasAbsorbed)
        {
            #region BCards

            var bcards = new List<BCard>();

            if (battleEntityAttacker.Mate.Monster.BCards != null)
                bcards.AddRange(collection: battleEntityAttacker.Mate.Monster.BCards.ToList());

            if (skill.BCards != null) bcards.AddRange(collection: skill.BCards.ToList());

            #endregion

            #region Owner

            var attackerOwner = battleEntityAttacker.Mate.Owner;

            #endregion

            lock (battleEntityDefender.PVELockObject)
            {
                #region Battle logic

                if (isRecursiveCall || skill.TargetType == 0)
                {
                    battleEntityDefender.GetDamage(damage: damage, damager: battleEntityAttacker);

                    battleEntityAttacker.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(
                        attackerType: battleEntityAttacker.UserType, attackerId: battleEntityAttacker.MapEntityId,
                        defenderType: battleEntityDefender.UserType, defenderId: battleEntityDefender.MapEntityId,
                        skillVNum: skill.SkillVNum,
                        cooldown: skill.Cooldown, attackAnimation: skill.AttackAnimation, skillEffect: skill.Effect,
                        x: battleEntityDefender.PositionX, y: battleEntityDefender.PositionY,
                        isAlive: battleEntityDefender.Hp > 0,
                        hpPercent: battleEntityDefender.HpPercent(),
                        damage: damage, hitMode: hitMode, skillType: skill.SkillType));

                    if (battleEntityDefender.Character != null)
                        battleEntityDefender.Character.Session?.SendPacket(
                            packet: battleEntityDefender.Character.GenerateStat());

                    if (battleEntityDefender.MapMonster != null && attackerOwner.BattleEntity != null)
                        battleEntityDefender.MapMonster.AddToDamageList(damagerEntity: attackerOwner.BattleEntity,
                            damage: damage);

                    bcards.ForEach(action: bcard =>
                    {
                        if (bcard.Type == Convert.ToByte(value: CardType.Buff) &&
                            new Buff(id: Convert.ToInt16(value: bcard.SecondData), level: battleEntityAttacker.Level)
                                .Card?.BuffType !=
                            BuffType.Bad)
                        {
                            if (!isRecursiveCall)
                                bcard.ApplyBCards(session: battleEntityAttacker, sender: battleEntityAttacker);
                        }
                        else if (battleEntityDefender.Hp > 0)
                        {
                            if (hitMode != 4 && !hasAbsorbed)
                                bcard.ApplyBCards(session: battleEntityDefender, sender: battleEntityAttacker);
                        }
                    });

                    if (battleEntityDefender.Hp > 0 && hitMode != 4 && !hasAbsorbed)
                        battleEntityDefender.BCards?.ToList().ForEach(action: bcard =>
                        {
                            if (bcard.Type == Convert.ToByte(value: CardType.Buff))
                            {
                                if (new Buff(id: Convert.ToInt16(value: bcard.SecondData),
                                        level: battleEntityDefender.Level).Card
                                    ?.BuffType != BuffType.Bad)
                                    bcard.ApplyBCards(session: battleEntityDefender, sender: battleEntityDefender);
                                else
                                    bcard.ApplyBCards(session: battleEntityAttacker, sender: battleEntityDefender);
                            }
                        });
                }
                else if (skill.HitType == 1 && skill.TargetRange > 0)
                {
                    battleEntityAttacker.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(
                        attackerType: battleEntityAttacker.UserType, attackerId: battleEntityAttacker.MapEntityId,
                        defenderType: battleEntityAttacker.UserType, defenderId: battleEntityAttacker.MapEntityId,
                        skillVNum: skill.SkillVNum,
                        cooldown: skill.Cooldown, attackAnimation: skill.AttackAnimation, skillEffect: skill.Effect,
                        x: battleEntityAttacker.PositionX, y: battleEntityAttacker.PositionY,
                        isAlive: battleEntityAttacker.Hp > 0,
                        hpPercent: battleEntityAttacker.HpPercent(),
                        damage: damage, hitMode: hitMode, skillType: skill.SkillType));

                    if (battleEntityAttacker.Hp > 0)
                        bcards.ForEach(action: bcard =>
                        {
                            if (bcard.Type == Convert.ToByte(value: CardType.Buff) &&
                                new Buff(id: Convert.ToInt16(value: bcard.SecondData),
                                        level: battleEntityAttacker.Level).Card
                                    ?.BuffType !=
                                BuffType.Bad)
                                bcard.ApplyBCards(session: battleEntityAttacker, sender: battleEntityAttacker);
                        });

                    battleEntityAttacker.MapInstance
                        .GetBattleEntitiesInRange(pos: battleEntityAttacker.GetPos(), distance: skill.TargetRange)
                        .ToList()
                        .ForEach(action: battleEntityInRange =>
                        {
                            if (!battleEntityInRange.Equals(obj: battleEntityAttacker))
                                PartnerSkillTargetHit(battleEntityAttacker: battleEntityAttacker,
                                    battleEntityDefender: battleEntityInRange, skill: skill, isRecursiveCall: true);
                        });
                }

                #endregion

                #region Skill reset

                if (!isRecursiveCall && (skill.Class == 28 || skill.Class == 29))
                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: skill.Cooldown * 100))
                        .Subscribe(onNext: o => attackerOwner.Session?.SendPacket(packet: $"psr {skill.CastId}"));

                #endregion

                #region Hp <= 0

                if (battleEntityDefender.Hp <= 0)
                    switch (battleEntityDefender.EntityType)
                    {
                        case EntityType.Player:
                            {
                                var target = battleEntityDefender.Character;

                                if (target != null)
                                {
                                    if (target.IsVehicled) target.RemoveVehicle();

                                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000))
                                        .Subscribe(onNext: o =>
                                            ServerManager.Instance.AskPvpRevive(characterId: target.CharacterId));
                                }
                            }
                            break;

                        case EntityType.Mate:
                            break;

                        case EntityType.Npc:
                            battleEntityDefender.MapNpc?.RunDeathEvent();
                            break;

                        case EntityType.Monster:
                            {
                                battleEntityDefender.MapMonster?.SetDeathStatement();
                                attackerOwner.GenerateKillBonus(monsterToAttack: battleEntityDefender.MapMonster,
                                    Killer: battleEntityAttacker);
                            }
                            break;
                    }

                #endregion
            }
        }

        #endregion
    }
}