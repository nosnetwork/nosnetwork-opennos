﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Handling;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.GameObject.Packets.ClientPackets;
using OpenNos.Log.Networking;
using OpenNos.Log.Shared;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;

namespace OpenNos.Handler
{
    public class FamilyPacketHandler : IPacketHandler
    {
        #region Instantiation

        public FamilyPacketHandler(ClientSession session)
        {
            Session = session;
        }

        #endregion

        #region Properties

        ClientSession Session { get; }

        #endregion

        #region Methods

        /// <summary>
        ///     fauth packet
        /// </summary>
        /// <param name="fAuthPacket"></param>
        public void ChangeAuthority(FAuthPacket fAuthPacket)
        {
            if (Session.Character.Family == null ||
                Session.Character.FamilyCharacter.Authority != FamilyAuthority.Head &&
                Session.Character.FamilyCharacter.Authority != FamilyAuthority.Familydeputy) return;

            Session.Character.Family.InsertFamilyLog(logtype: FamilyLogType.RightChanged,
                characterName: Session.Character.Name,
                authority: fAuthPacket.MemberType, righttype: fAuthPacket.AuthorityId + 1,
                rightvalue: fAuthPacket.Value);
            switch (fAuthPacket.MemberType)
            {
                case FamilyAuthority.Familykeeper:
                    switch (fAuthPacket.AuthorityId)
                    {
                        case 0:
                            Session.Character.Family.ManagerCanInvite = fAuthPacket.Value == 1;
                            break;

                        case 1:
                            Session.Character.Family.ManagerCanNotice = fAuthPacket.Value == 1;
                            break;

                        case 2:
                            Session.Character.Family.ManagerCanShout = fAuthPacket.Value == 1;
                            break;

                        case 3:
                            Session.Character.Family.ManagerCanGetHistory = fAuthPacket.Value == 1;
                            break;

                        case 4:
                            Session.Character.Family.ManagerAuthorityType = (FamilyAuthorityType)fAuthPacket.Value;
                            break;
                    }

                    break;

                case FamilyAuthority.Member:
                    switch (fAuthPacket.AuthorityId)
                    {
                        case 0:
                            Session.Character.Family.MemberCanGetHistory = fAuthPacket.Value == 1;
                            break;

                        case 1:
                            Session.Character.Family.MemberAuthorityType = (FamilyAuthorityType)fAuthPacket.Value;
                            break;
                    }

                    break;
            }

            FamilyDto fam = Session.Character.Family;
            DaoFactory.FamilyDao.InsertOrUpdate(family: ref fam);
            ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId);
            CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
            {
                DestinationCharacterId = fam.FamilyId,
                SourceCharacterId = Session.Character.CharacterId,
                SourceWorldId = ServerManager.Instance.WorldId,
                Message = "fhis_stc",
                Type = MessageType.Family
            });
            Session.SendPacket(packet: Session.Character.GenerateGInfo());
        }

        /// <summary>
        ///     glmk packet
        /// </summary>
        /// <param name="createFamilyPacket"></param>
        public void CreateFamily(CreateFamilyPacket createFamilyPacket)
        {
            if (Session.Character.Group?.GroupType == GroupType.Group && Session.Character.Group.SessionCount == 3)
            {
                foreach (var session in Session.Character.Group.Sessions.GetAllItems())
                    if (session.Character.Family != null || session.Character.FamilyCharacter != null)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "PARTY_MEMBER_IN_FAMILY")));
                        return;
                    }
                    else if (session.Character.LastFamilyLeave > DateTime.Now.AddDays(value: -1).Ticks)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "PARTY_MEMBER_HAS_PENALTY")));
                        return;
                    }

                if (Session.Character.Gold < 200000)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY")));
                    return;
                }

                var name = createFamilyPacket.CharacterName;
                if (DaoFactory.FamilyDao.LoadByName(name: name) != null)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "FAMILY_NAME_ALREADY_USED")));
                    return;
                }

                Session.Character.Gold -= 200000;
                Session.SendPacket(packet: Session.Character.GenerateGold());
                var family = new FamilyDto
                {
                    Name = name,
                    FamilyExperience = 0,
                    FamilyLevel = 1,
                    FamilyMessage = "",
                    FamilyFaction = Session.Character.Faction != FactionType.None
                        ? (byte)Session.Character.Faction
                        : (byte)ServerManager.RandomNumber(min: 1, max: 2),
                    MaxSize = 50
                };
                DaoFactory.FamilyDao.InsertOrUpdate(family: ref family);

                Logger.LogUserEvent(logEvent: "GUILDCREATE", caller: Session.GenerateIdentity(),
                    data: $"[FamilyCreate][{family.FamilyId}]");

                ServerManager.Instance.Broadcast(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "FAMILY_FOUNDED"),
                            arg0: name), type: 0));
                foreach (var session in Session.Character.Group.Sessions.GetAllItems())
                {
                    session.Character.ChangeFaction(faction: FactionType.None);
                    var familyCharacter = new FamilyCharacterDto
                    {
                        CharacterId = session.Character.CharacterId,
                        DailyMessage = "",
                        Experience = 0,
                        Authority = Session.Character.CharacterId == session.Character.CharacterId
                            ? FamilyAuthority.Head
                            : FamilyAuthority.Familydeputy,
                        FamilyId = family.FamilyId,
                        Rank = 0
                    };
                    DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref familyCharacter);
                }

                ServerManager.Instance.FamilyRefresh(familyId: family.FamilyId);
                CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                {
                    DestinationCharacterId = family.FamilyId,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message = "fhis_stc",
                    Type = MessageType.Family
                });
                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 5)).Subscribe(onNext: o =>
                    ServerManager.Instance.FamilyRefresh(familyId: family.FamilyId));
                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 10)).Subscribe(onNext: o =>
                    ServerManager.Instance.FamilyRefresh(familyId: family.FamilyId));
            }
        }

        [Packet("%Familyshout")]
        public void FamilyCall(string packet)
        {
            if (Session.Character.Family != null && Session.Character.FamilyCharacter != null)
                if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familydeputy
                    || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familykeeper
                    && Session.Character.Family.ManagerCanShout
                    || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head)
                {
                    var msg = "";
                    var i = 0;
                    foreach (var str in packet.Split(' '))
                    {
                        if (i > 1) msg += str + " ";

                        i++;
                    }

                    Logger.LogUserEvent(logEvent: "GUILDCOMMAND", caller: Session.GenerateIdentity(),
                        data: $"[FamilyShout][{Session.Character.Family.FamilyId}]Message: {msg}");
                    CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                    {
                        DestinationCharacterId = Session.Character.Family.FamilyId,
                        SourceCharacterId = Session.Character.CharacterId,
                        SourceWorldId = ServerManager.Instance.WorldId,
                        Message = UserInterfaceHelper.GenerateMsg(
                            message: $"<{Language.Instance.GetMessageFromKey(key: "FAMILYCALL")}> {msg}", type: 0),
                        Type = MessageType.Family
                    });
                }
        }

        /// <summary>
        ///     today_cts packet
        /// </summary>
        /// <param name="todayPacket"></param>
        public void FamilyChangeMessage(TodayPacket todayPacket)
        {
            if (todayPacket == null) throw new ArgumentNullException(paramName: nameof(todayPacket));
            Session.SendPacket(packet: "today_stc");
        }

        /// <summary>
        ///     : packet
        /// </summary>
        /// <param name="familyChatPacket"></param>
        public void FamilyChat(FamilyChatPacket familyChatPacket)
        {
            if (string.IsNullOrEmpty(value: familyChatPacket.Message)) return;

            if (Session.Character.Family != null && Session.Character.FamilyCharacter != null)
            {
                var msg = familyChatPacket.Message;
                var ccmsg = $"[{Session.Character.Name}]:{msg}";
                if (Session.Account.Authority >= AuthorityType.Mod)
                    ccmsg = $"[{Session.Account.Authority.ToString()} {Session.Character.Name}]:{msg}";

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogChatMessage(logEntry: new LogEntry
                    {
                        Sender = Session.Character.Name,
                        SenderId = Session.Character.CharacterId,
                        Receiver = Session.Character.Family.Name,
                        ReceiverId = Session.Character.Family.FamilyId,
                        MessageType = LogType.Family,
                        Message = familyChatPacket.Message
                    });

                CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                {
                    DestinationCharacterId = Session.Character.Family.FamilyId,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message = ccmsg,
                    Type = MessageType.FamilyChat
                });
                Parallel.ForEach(source: ServerManager.Instance.Sessions.ToList(), body: session =>
                {
                    if (session.HasSelectedCharacter && session.Character.Family != null
                                                     && Session.Character.Family != null
                                                     && session.Character.Family?.FamilyId ==
                                                     Session.Character.Family?.FamilyId)
                    {
                        if (Session.HasCurrentMapInstance && session.HasCurrentMapInstance
                                                          && Session.CurrentMapInstance == session.CurrentMapInstance)
                        {
                            if (Session.Account.Authority != AuthorityType.Gs && !Session.Character.InvisibleGm)
                                session.SendPacket(packet: Session.Character.GenerateSay(message: msg, type: 6));
                            else
                                session.SendPacket(packet: Session.Character.GenerateSay(message: ccmsg, type: 6,
                                    ignoreNickname: true));
                        }
                        else
                        {
                            session.SendPacket(packet: Session.Character.GenerateSay(message: ccmsg, type: 6));
                        }

                        if (!Session.Character.InvisibleGm)
                            session.SendPacket(packet: Session.Character.GenerateSpk(message: msg, type: 1));
                    }
                });
            }
        }

        /// <summary>
        ///     f_deposit packet
        /// </summary>
        /// <param name="fDepositPacket"></param>
        public void FamilyDeposit(FDepositPacket fDepositPacket)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            if (fDepositPacket == null) return;

            if (Session.Character.Family == null
                || !(Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head
                     || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familydeputy
                     || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Member
                     && Session.Character.Family.MemberAuthorityType != FamilyAuthorityType.None
                     || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familykeeper
                     && Session.Character.Family.ManagerAuthorityType != FamilyAuthorityType.None))
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_RIGHT")));
                return;
            }

            var item =
                Session.Character.Inventory.LoadBySlotAndType(slot: fDepositPacket.Slot,
                    type: fDepositPacket.Inventory);
            var itemdest =
                Session.Character.Family.Warehouse.LoadBySlotAndType(slot: fDepositPacket.NewSlot,
                    type: InventoryType.FamilyWareHouse);

            if (itemdest != null) return;

            // check if the destination slot is out of range
            if (fDepositPacket.NewSlot > Session.Character.Family.WarehouseSize) return;

            // check if the character is allowed to move the item
            if (Session.Character.InExchangeOrTrade) return;

            // actually move the item from source to destination
            Session.Character.Inventory.FDepositItem(inventory: fDepositPacket.Inventory, slot: fDepositPacket.Slot,
                amount: fDepositPacket.Amount, newSlot: fDepositPacket.NewSlot, item: ref item, itemdest: ref itemdest);
        }

        /// <summary>
        ///     glrm packet
        /// </summary>
        /// <param name="familyDissmissPacket"></param>
        public void FamilyDismiss(FamilyDismissPacket familyDissmissPacket)
        {
            if (familyDissmissPacket == null) throw new ArgumentNullException(paramName: nameof(familyDissmissPacket));
            if (Session.Character.Family == null || Session.Character.FamilyCharacter == null
                                                 || Session.Character.FamilyCharacter.Authority != FamilyAuthority.Head)
                return;

            var fam = Session.Character.Family;

            fam.FamilyCharacters.ForEach(action: s =>
                DaoFactory.FamilyCharacterDao.Delete(characterId: s.Character.CharacterId));
            fam.FamilyLogs.ForEach(action: s => DaoFactory.FamilyLogDao.Delete(familyLogId: s.FamilyLogId));
            DaoFactory.FamilyDao.Delete(familyId: fam.FamilyId);
            ServerManager.Instance.FamilyRefresh(familyId: fam.FamilyId);

            Logger.LogUserEvent(logEvent: "GUILDDISMISS", caller: Session.GenerateIdentity(),
                data: $"[FamilyDismiss][{fam.FamilyId}]");

            var sessions = ServerManager.Instance.Sessions
                .Where(predicate: s => s.Character?.Family != null && s.Character.Family.FamilyId == fam.FamilyId)
                .ToList();

            CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
            {
                DestinationCharacterId = fam.FamilyId,
                SourceCharacterId = Session.Character.CharacterId,
                SourceWorldId = ServerManager.Instance.WorldId,
                Message = "fhis_stc",
                Type = MessageType.Family
            });

            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 3)).Subscribe(onNext: observer =>
                sessions.ForEach(action: s =>
                {
                    if (s?.Character != null) s.CurrentMapInstance?.Broadcast(packet: s.Character.GenerateGidx());
                }));
        }

        [Packet("%Familydismiss", "%FamilyKick")]
        public void FamilyKick(string packet)
        {
            var packetsplit = packet.Split(' ');

            if (packetsplit.Length == 3)
            {
                if (Session.Character.Family?.FamilyCharacters == null ||
                    Session.Character.FamilyCharacter == null) return;

                if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Member
                    || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familykeeper)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "NOT_ALLOWED_KICK"))));
                    return;
                }

                var characterName = packetsplit[2];

                Logger.LogUserEvent(logEvent: "GUILDCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[FamilyKick][{Session.Character.Family.FamilyId}]CharacterName: {characterName}");

                var familyCharacter =
                    Session.Character.Family.FamilyCharacters.FirstOrDefault(predicate: s =>
                        s.Character.Name == characterName);

                if (familyCharacter?.FamilyId != Session.Character.Family.FamilyId) return;

                if (familyCharacter.Authority == FamilyAuthority.Head)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "CANT_KICK_HEAD")));
                    return;
                }

                if (familyCharacter.CharacterId == Session.Character.CharacterId)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "CANT_KICK_YOURSELF")));
                    return;
                }

                var kickSession =
                    ServerManager.Instance.GetSessionByCharacterId(characterId: familyCharacter.CharacterId);

                if (kickSession != null)
                {
                    DaoFactory.FamilyCharacterDao.Delete(characterId: familyCharacter.CharacterId);

                    Session.Character.Family.InsertFamilyLog(logtype: FamilyLogType.FamilyManaged,
                        characterName: familyCharacter.Character.Name);

                    kickSession.Character.Family = null;
                    kickSession.Character.LastFamilyLeave = DateTime.Now.Ticks;

                    Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 5)).Subscribe(onNext: o =>
                        ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId));
                }
                else
                {
                    if (CommunicationServiceClient.Instance.IsCharacterConnected(
                        worldGroup: ServerManager.Instance.ServerGroup,
                        characterId: familyCharacter.CharacterId))
                    {
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(
                                key: "CANT_KICK_PLAYER_ONLINE_OTHER_CHANNEL")));
                        return;
                    }

                    DaoFactory.FamilyCharacterDao.Delete(characterId: familyCharacter.CharacterId);

                    Session.Character.Family.InsertFamilyLog(logtype: FamilyLogType.FamilyManaged,
                        characterName: familyCharacter.Character.Name);

                    var familyCharacterDTO = familyCharacter.Character;

                    familyCharacterDTO.LastFamilyLeave = DateTime.Now.Ticks;

                    DaoFactory.CharacterDao.InsertOrUpdate(character: ref familyCharacterDTO);
                }
            }
        }

        [Packet("%Familyleave")]
        public void FamilyLeave(string packet)
        {
            var packetsplit = packet.Split(' ');
            if (packetsplit.Length == 2)
            {
                if (Session.Character.Family == null || Session.Character.FamilyCharacter == null) return;

                if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "CANNOT_LEAVE_FAMILY")));
                    return;
                }

                var familyId = Session.Character.Family.FamilyId;

                DaoFactory.FamilyCharacterDao.Delete(characterId: Session.Character.CharacterId);

                Logger.LogUserEvent(logEvent: "GUILDCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[FamilyLeave][{Session.Character.Family.FamilyId}]");
                Logger.LogUserEvent(logEvent: "GUILDLEAVE", caller: Session.GenerateIdentity(),
                    data: $"[FamilyLeave][{Session.Character.Family.FamilyId}]");

                Session.Character.Family.InsertFamilyLog(logtype: FamilyLogType.FamilyManaged,
                    characterName: Session.Character.Name);
                Session.Character.LastFamilyLeave = DateTime.Now.Ticks;
                Session.Character.Family = null;
                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 5)).Subscribe(onNext: o =>
                    ServerManager.Instance.FamilyRefresh(familyId: familyId));
                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 10)).Subscribe(onNext: o =>
                    ServerManager.Instance.FamilyRefresh(familyId: familyId));
            }
        }

        /// <summary>
        ///     glist packet
        /// </summary>
        /// <param name="gListPacket"></param>
        public void FamilyList(GListPacket gListPacket)
        {
            if (Session.Character.Family != null && Session.Character.FamilyCharacter != null && gListPacket.Type == 2)
            {
                Session.SendPacket(packet: Session.Character.GenerateGInfo());
                Session.SendPacket(packet: Session.Character.GenerateFamilyMember());
                Session.SendPacket(packet: Session.Character.GenerateFamilyMemberMessage());
                Session.SendPacket(packet: Session.Character.GenerateFamilyMemberExp());
            }
        }

        /// <summary>
        ///     fmg packet
        /// </summary>
        /// <param name="familyManagementPacket"></param>
        public void FamilyManagement(FamilyManagementPacket familyManagementPacket)
        {
            if (Session.Character.Family == null) return;

            Logger.LogUserEvent(logEvent: "GUILDMGMT", caller: Session.GenerateIdentity(),
                data:
                $"[FamilyManagement][{Session.Character.Family.FamilyId}]TargetId: {familyManagementPacket.TargetId} AuthorityType: {familyManagementPacket.FamilyAuthorityType.ToString()}");

            if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Member
                || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familykeeper)
                return;

            var targetId = familyManagementPacket.TargetId;
            if (DaoFactory.FamilyCharacterDao.LoadByCharacterId(characterId: targetId)?.FamilyId
                != Session.Character.FamilyCharacter.FamilyId)
                return;

            var famChar = DaoFactory.FamilyCharacterDao.LoadByCharacterId(characterId: targetId);
            if (famChar.Authority == familyManagementPacket.FamilyAuthorityType) return;
            switch (familyManagementPacket.FamilyAuthorityType)
            {
                case FamilyAuthority.Head:
                    if (Session.Character.FamilyCharacter.Authority != FamilyAuthority.Head)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_FAMILY_HEAD")));
                        return;
                    }

                    if (famChar.Authority != FamilyAuthority.Familydeputy)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "ONLY_PROMOTE_ASSISTANT")));
                        return;
                    }

                    famChar.Authority = FamilyAuthority.Head;
                    DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref famChar);

                    Session.Character.Family.Warehouse.ForEach(action: s =>
                    {
                        s.CharacterId = famChar.CharacterId;
                        DaoFactory.ItemInstanceDao.InsertOrUpdate(dto: s);
                    });
                    Session.Character.FamilyCharacter.Authority = FamilyAuthority.Familydeputy;
                    var chara2 = Session.Character.FamilyCharacter;
                    DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref chara2);
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "DONE")));
                    break;

                case FamilyAuthority.Familydeputy:
                    if (Session.Character.FamilyCharacter.Authority != FamilyAuthority.Head)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_FAMILY_HEAD")));
                        return;
                    }

                    if (famChar.Authority == FamilyAuthority.Head)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "HEAD_UNDEMOTABLE")));
                        return;
                    }

                    if (DaoFactory.FamilyCharacterDao.LoadByFamilyId(familyId: Session.Character.Family.FamilyId)
                        .Count(predicate: s => s.Authority == FamilyAuthority.Familydeputy) == 2)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "ALREADY_TWO_ASSISTANT")));
                        return;
                    }

                    famChar.Authority = FamilyAuthority.Familydeputy;
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "DONE")));

                    DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref famChar);
                    break;

                case FamilyAuthority.Familykeeper:
                    if (famChar.Authority == FamilyAuthority.Head)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "HEAD_UNDEMOTABLE")));
                        return;
                    }

                    if (famChar.Authority == FamilyAuthority.Familydeputy
                        && Session.Character.FamilyCharacter.Authority != FamilyAuthority.Head)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "ASSISTANT_UNDEMOTABLE")));
                        return;
                    }

                    famChar.Authority = FamilyAuthority.Familykeeper;
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "DONE")));
                    DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref famChar);
                    break;

                case FamilyAuthority.Member:
                    if (famChar.Authority == FamilyAuthority.Head)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "HEAD_UNDEMOTABLE")));
                        return;
                    }

                    if (famChar.Authority == FamilyAuthority.Familydeputy
                        && Session.Character.FamilyCharacter.Authority != FamilyAuthority.Head)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "ASSISTANT_UNDEMOTABLE")));
                        return;
                    }

                    famChar.Authority = FamilyAuthority.Member;
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "DONE")));

                    DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref famChar);
                    break;
            }

            var character = DaoFactory.CharacterDao.LoadById(characterId: targetId);
            var targetSession = ServerManager.Instance.GetSessionByCharacterId(characterId: targetId);

            Session.Character.Family.InsertFamilyLog(logtype: FamilyLogType.AuthorityChanged,
                characterName: Session.Character.Name,
                characterName2: character.Name, authority: familyManagementPacket.FamilyAuthorityType);
            targetSession?.CurrentMapInstance?.Broadcast(packet: targetSession?.Character.GenerateGidx());
            if (familyManagementPacket.FamilyAuthorityType == FamilyAuthority.Head)
                Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateGidx());
        }

        [Packet("%Aviso", "%Notice")]
        public void FamilyMessage(string packet)
        {
            if (Session.Character.Family != null && Session.Character.FamilyCharacter != null)
                if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familydeputy
                    || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familykeeper
                    && Session.Character.Family.ManagerCanShout
                    || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head)
                {
                    var msg = "";
                    var i = 0;
                    foreach (var str in packet.Split(' '))
                    {
                        if (i > 1) msg += str + " ";

                        i++;
                    }

                    Logger.LogUserEvent(logEvent: "GUILDCOMMAND", caller: Session.GenerateIdentity(),
                        data: $"[FamilyMessage][{Session.Character.Family.FamilyId}]Message: {msg}");

                    Session.Character.Family.FamilyMessage = msg;
                    FamilyDto fam = Session.Character.Family;
                    DaoFactory.FamilyDao.InsertOrUpdate(family: ref fam);
                    ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId);
                    CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                    {
                        DestinationCharacterId = Session.Character.Family.FamilyId,
                        SourceCharacterId = Session.Character.CharacterId,
                        SourceWorldId = ServerManager.Instance.WorldId,
                        Message = "fhis_stc",
                        Type = MessageType.Family
                    });
                    if (!string.IsNullOrWhiteSpace(value: msg))
                        CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                        {
                            DestinationCharacterId = Session.Character.Family.FamilyId,
                            SourceCharacterId = Session.Character.CharacterId,
                            SourceWorldId = ServerManager.Instance.WorldId,
                            Message = UserInterfaceHelper.GenerateInfo(
                                message: "--- Family Message ---\n" + Session.Character.Family.FamilyMessage),
                            Type = MessageType.Family
                        });
                }
        }

        /// <summary>
        ///     frank_cts packet
        /// </summary>
        /// <param name="frankCtsPacket"></param>
        public void FamilyRank(FrankCtsPacket frankCtsPacket)
        {
            Session.SendPacket(packet: UserInterfaceHelper.GenerateFrank(type: frankCtsPacket.Type));
        }

        /// <summary>
        ///     fhis_cts packet
        /// </summary>
        /// <param name="fhistCtsPacket"></param>
        public void FamilyRefreshHist(FhistCtsPacket fhistCtsPacket)
        {
            if (fhistCtsPacket == null) throw new ArgumentNullException(paramName: nameof(fhistCtsPacket));
            Session.SendPackets(packets: Session.Character.GetFamilyHistory());
        }

        /// <summary>
        ///     f_repos packet
        /// </summary>
        /// <param name="fReposPacket"></param>
        public void FamilyRepos(FReposPacket fReposPacket)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            if (fReposPacket == null) return;

            if (fReposPacket.Amount < 1) return;

            if (Session.Character.Family == null
                || !(Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head
                     || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familydeputy
                     || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Member
                     && Session.Character.Family.MemberAuthorityType == FamilyAuthorityType.All
                     || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familykeeper
                     && Session.Character.Family.ManagerAuthorityType == FamilyAuthorityType.All))
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_RIGHT")));
                return;
            }

            // check if the character is allowed to move the item
            if (Session.Character.InExchangeOrTrade) return;

            if (fReposPacket.NewSlot > Session.Character.Family.WarehouseSize) return;

            var sourceInventory =
                Session.Character.Family.Warehouse.LoadBySlotAndType(slot: fReposPacket.OldSlot,
                    type: InventoryType.FamilyWareHouse);
            var destinationInventory =
                Session.Character.Family.Warehouse.LoadBySlotAndType(slot: fReposPacket.NewSlot,
                    type: InventoryType.FamilyWareHouse);

            if (sourceInventory != null && fReposPacket.Amount <= sourceInventory.Amount)
            {
                if (destinationInventory == null)
                {
                    destinationInventory = sourceInventory.DeepCopy();
                    sourceInventory.Amount -= fReposPacket.Amount;
                    destinationInventory.Amount = fReposPacket.Amount;
                    destinationInventory.Slot = fReposPacket.NewSlot;
                    if (sourceInventory.Amount > 0)
                        destinationInventory.Id = Guid.NewGuid();
                    else
                        sourceInventory = null;
                }
                else
                {
                    if (destinationInventory.ItemVNum == sourceInventory.ItemVNum
                        && (byte)sourceInventory.Item.Type != 0)
                    {
                        if (destinationInventory.Amount + fReposPacket.Amount > 999)
                        {
                            int saveItemCount = destinationInventory.Amount;
                            destinationInventory.Amount = 999;
                            sourceInventory.Amount = (short)(saveItemCount + sourceInventory.Amount - 999);
                        }
                        else
                        {
                            destinationInventory.Amount += fReposPacket.Amount;
                            sourceInventory.Amount -= fReposPacket.Amount;
                            if (sourceInventory.Amount == 0)
                            {
                                DaoFactory.ItemInstanceDao.Delete(id: sourceInventory.Id);
                                sourceInventory = null;
                            }
                        }
                    }
                    else
                    {
                        destinationInventory.Slot = fReposPacket.OldSlot;
                        sourceInventory.Slot = fReposPacket.NewSlot;
                    }
                }
            }

            if (sourceInventory?.Amount > 0) DaoFactory.ItemInstanceDao.InsertOrUpdate(dto: sourceInventory);

            if (destinationInventory?.Amount > 0) DaoFactory.ItemInstanceDao.InsertOrUpdate(dto: destinationInventory);

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.FamilyStorage,
                    Packet =
                        $"[FAMILY_REPOS]OldIIId: {sourceInventory?.Id.ToString() ?? ""} NewIIId: {destinationInventory?.Id.ToString() ?? ""} ItemVNum: {destinationInventory?.ItemVNum ?? -1} Amount: {fReposPacket.Amount} Rare: {destinationInventory?.Rare ?? -1} Upgrade: {destinationInventory?.Upgrade ?? -1}"
                });

            Session.Character.Family.SendPacket(packet: destinationInventory != null
                ? destinationInventory.GenerateFStash()
                : UserInterfaceHelper.Instance.GenerateFStashRemove(slot: fReposPacket.NewSlot));
            Session.Character.Family.SendPacket(packet: sourceInventory != null
                ? sourceInventory.GenerateFStash()
                : UserInterfaceHelper.Instance.GenerateFStashRemove(slot: fReposPacket.OldSlot));

            ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId);
        }

        /// <summary>
        ///     f_withdraw packet
        /// </summary>
        /// <param name="fWithdrawPacket"></param>
        public void FamilyWithdraw(FWithdrawPacket fWithdrawPacket)
        {
            if (Session.Account.IsLimited)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                return;
            }

            if (fWithdrawPacket == null) return;

            if (Session.Character.Family == null
                || !(Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head
                     || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familydeputy
                     || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Member
                     && Session.Character.Family.MemberAuthorityType == FamilyAuthorityType.All
                     || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familykeeper
                     && Session.Character.Family.ManagerAuthorityType == FamilyAuthorityType.All))
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_RIGHT")));
                return;
            }

            var previousInventory =
                Session.Character.Family.Warehouse.LoadBySlotAndType(slot: fWithdrawPacket.Slot,
                    type: InventoryType.FamilyWareHouse);
            if (fWithdrawPacket.Amount <= 0 || previousInventory == null
                                            || fWithdrawPacket.Amount > previousInventory.Amount)
                return;

            var item2 = previousInventory.DeepCopy();
            item2.Id = Guid.NewGuid();
            item2.Amount = fWithdrawPacket.Amount;
            item2.CharacterId = Session.Character.CharacterId;

            previousInventory.Amount -= fWithdrawPacket.Amount;
            if (previousInventory.Amount <= 0) previousInventory = null;

            var newInv = Session.Character.Inventory.AddToInventory(newItem: item2, type: item2.Item.Type);
            Session.Character.Family.SendPacket(
                packet: UserInterfaceHelper.Instance.GenerateFStashRemove(slot: fWithdrawPacket.Slot));
            if (previousInventory != null)
            {
                DaoFactory.ItemInstanceDao.InsertOrUpdate(dto: previousInventory);
            }
            else
            {
                var fhead =
                    Session.Character.Family.FamilyCharacters.Find(match: s => s.Authority == FamilyAuthority.Head);
                if (fhead == null) return;

                DaoFactory.ItemInstanceDao.DeleteFromSlotAndType(characterId: fhead.CharacterId,
                    slot: fWithdrawPacket.Slot,
                    type: InventoryType.FamilyWareHouse);
            }

            if (ServerManager.Instance.Configuration.UseLogService)
                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                {
                    Sender = Session.Character.Name,
                    SenderId = Session.Character.CharacterId,
                    PacketType = LogType.FamilyStorage,
                    Packet =
                        $"[FAMILY_WITHDRAW]OldIIId: {previousInventory?.Id.ToString() ?? ""} NewIIId: {item2.Id} ItemVNum: {item2.ItemVNum} Amount: {fWithdrawPacket.Amount} Rare: {item2.Rare} Upgrade: {item2.Upgrade}"
                });

            Session.Character.Family.InsertFamilyLog(logtype: FamilyLogType.WareHouseRemoved,
                characterName: Session.Character.Name,
                message: $"{item2.ItemVNum}|{fWithdrawPacket.Amount}");
        }

        [Packet("%Familyinvite")]
        public void InviteFamily(string packet)
        {
            var packetsplit = packet.Split(' ');

            if (packetsplit.Length != 3) return;

            if (Session.Character.Family == null || Session.Character.FamilyCharacter == null) return;

            if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Member
                || Session.Character.FamilyCharacter.Authority == FamilyAuthority.Familykeeper
                && !Session.Character.Family.ManagerCanInvite)
            {
                Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                    message: string.Format(
                        format: Language.Instance.GetMessageFromKey(key: "FAMILY_INVITATION_NOT_ALLOWED"))));
                return;
            }

            Logger.LogUserEvent(logEvent: "GUILDCOMMAND", caller: Session.GenerateIdentity(),
                data: $"[FamilyInvite][{Session.Character.Family.FamilyId}]Message: {packetsplit[2]}");
            var otherSession = ServerManager.Instance.GetSessionByCharacterName(name: packetsplit[2]);
            if (otherSession == null)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "USER_NOT_FOUND"))));
                return;
            }

            if (otherSession.CurrentMapInstance?.MapInstanceType == MapInstanceType.TalentArenaMapInstance) return;

            if (otherSession.Character.FamilyRequestBlocked)
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "FAMILY_BLOCKED"),
                        type: 0));

            if (Session.Character.IsBlockedByCharacter(characterId: otherSession.Character.CharacterId))
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "BLACKLIST_BLOCKED")));
                return;
            }

            if (Session.Character.Family.FamilyCharacters.Count + 1 > Session.Character.Family.MaxSize)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "FAMILY_FULL")));
                return;
            }

            if (otherSession.Character.Family != null || otherSession.Character.FamilyCharacter != null)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "ALREADY_IN_FAMILY")));
                return;
            }

            if (otherSession.Character.LastFamilyLeave > DateTime.Now.AddDays(value: -1).Ticks)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_ENTER_FAMILY")));
                return;
            }

            if (ServerManager.Instance.ChannelId == 51 &&
                otherSession.Character.Faction != Session.Character.Faction) return;

            Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "FAMILY_INVITED"),
                    arg0: otherSession.Character.Name)));
            otherSession.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                dialog:
                $"#gjoin^1^{Session.Character.CharacterId} #gjoin^2^{Session.Character.CharacterId} {string.Format(format: Language.Instance.GetMessageFromKey(key: "ASK_FAMILY_INVITED"), arg0: Session.Character.Family.Name)}"));
            Session.Character.FamilyInviteCharacters.Add(value: otherSession.Character.CharacterId);
        }

        /// <summary>
        ///     gjoin packet
        /// </summary>
        /// <param name="joinFamilyPacket"></param>
        public void JoinFamily(JoinFamilyPacket joinFamilyPacket)
        {
            var characterId = joinFamilyPacket.CharacterId;

            if (joinFamilyPacket.Type == 1)
            {
                if (Session.Character.Family != null) return;

                var inviteSession = ServerManager.Instance.GetSessionByCharacterId(characterId: characterId);

                if (inviteSession?.Character.FamilyInviteCharacters.GetAllItems()
                        .Contains(item: Session.Character.CharacterId) == true
                    && inviteSession.Character.Family != null
                    && inviteSession.Character.Family.FamilyCharacters != null)
                {
                    if (inviteSession.Character.Family.FamilyCharacters.Count + 1 >
                        inviteSession.Character.Family.MaxSize) return;

                    var familyCharacter = new FamilyCharacterDto
                    {
                        CharacterId = Session.Character.CharacterId,
                        DailyMessage = "",
                        Experience = 0,
                        Authority = FamilyAuthority.Member,
                        FamilyId = inviteSession.Character.Family.FamilyId,
                        Rank = 0
                    };

                    DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref familyCharacter);

                    inviteSession.Character.Family.InsertFamilyLog(logtype: FamilyLogType.UserManaged,
                        characterName: inviteSession.Character.Name, characterName2: Session.Character.Name);

                    Logger.LogUserEvent(logEvent: "GUILDJOIN", caller: Session.GenerateIdentity(),
                        data: $"[FamilyJoin][{inviteSession.Character.Family.FamilyId}]");

                    CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                    {
                        DestinationCharacterId = inviteSession.Character.Family.FamilyId,
                        SourceCharacterId = Session.Character.CharacterId,
                        SourceWorldId = ServerManager.Instance.WorldId,
                        Message = UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "FAMILY_JOINED"),
                                arg0: Session.Character.Name,
                                arg1: inviteSession.Character.Family.Name), type: 0),
                        Type = MessageType.Family
                    });

                    var familyId = inviteSession.Character.Family.FamilyId;

                    Session.Character.Family = inviteSession.Character.Family;
                    Session.Character.ChangeFaction(
                        faction: (FactionType)inviteSession.Character.Family.FamilyFaction);
                    Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 5)).Subscribe(onNext: o =>
                        ServerManager.Instance.FamilyRefresh(familyId: familyId));
                    Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 10)).Subscribe(onNext: o =>
                        ServerManager.Instance.FamilyRefresh(familyId: familyId));

                    Session.SendPacket(packet: Session.Character.GenerateFamilyMember());
                    Session.SendPacket(packet: Session.Character.GenerateFamilyMemberMessage());
                    Session.SendPacket(packet: Session.Character.GenerateFamilyMemberExp());
                }
            }
        }

        [Packet("%Género", "%Sex")]
        public void ResetSex(string packet)
        {
            var packetsplit = packet.Split(' ');
            if (packetsplit.Length != 3) return;

            if (Session.Character.Family != null && Session.Character.FamilyCharacter != null
                                                 && Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head
                                                 && byte.TryParse(s: packetsplit[2], result: out var rank))
            {
                foreach (var familyCharacter in Session.Character.Family.FamilyCharacters)
                {
                    FamilyCharacterDto familyCharacterDto = familyCharacter;
                    familyCharacterDto.Rank = 0;
                    DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref familyCharacterDto);
                }

                Logger.LogUserEvent(logEvent: "GUILDCOMMAND", caller: Session.GenerateIdentity(),
                    data: $"[Sex][{Session.Character.Family.FamilyId}]");

                FamilyDto fam = Session.Character.Family;
                fam.FamilyHeadGender = (GenderType)rank;
                DaoFactory.FamilyDao.InsertOrUpdate(family: ref fam);
                ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId);
                CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                {
                    DestinationCharacterId = fam.FamilyId,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message = "fhis_stc",
                    Type = MessageType.Family
                });
                Session.SendPacket(packet: Session.Character.GenerateFamilyMember());
                Session.SendPacket(packet: Session.Character.GenerateFamilyMemberMessage());

                CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                {
                    DestinationCharacterId = fam.FamilyId,
                    SourceCharacterId = Session.Character.CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message = UserInterfaceHelper.GenerateMsg(
                        message: string.Format(
                            format: Language.Instance.GetMessageFromKey(key: "FAMILY_HEAD_CHANGE_GENDER")), type: 0),
                    Type = MessageType.Family
                });
            }
        }

        [Packet("%Título", "%Title")]
        public void TitleChange(string packet)
        {
            if (Session.Character.Family != null && Session.Character.FamilyCharacter != null
                                                 && Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head)
            {
                var packetsplit = packet.Split(' ');
                if (packetsplit.Length != 4) return;

                FamilyCharacterDto fchar =
                    Session.Character.Family.FamilyCharacters.Find(match: s => s.Character.Name == packetsplit[2]);
                if (fchar != null && byte.TryParse(s: packetsplit[3], result: out var rank))
                {
                    fchar.Rank = (FamilyMemberRank)rank;

                    Logger.LogUserEvent(logEvent: "GUILDCOMMAND", caller: Session.GenerateIdentity(),
                        data:
                        $"[Title][{Session.Character.Family.FamilyId}]CharacterName: {packetsplit[2]} Title: {fchar.Rank.ToString()}");

                    DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref fchar);
                    ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId);
                    CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                    {
                        DestinationCharacterId = Session.Character.Family.FamilyId,
                        SourceCharacterId = Session.Character.CharacterId,
                        SourceWorldId = ServerManager.Instance.WorldId,
                        Message = "fhis_stc",
                        Type = MessageType.Family
                    });
                    Session.SendPacket(packet: Session.Character.GenerateFamilyMember());
                    Session.SendPacket(packet: Session.Character.GenerateFamilyMemberMessage());
                }
            }
        }

        [Packet("%Hoy", "%Today")]
        public void TodayMessage(string packet)
        {
            if (Session.Character.Family != null && Session.Character.FamilyCharacter != null)
            {
                var msg = "";
                var i = 0;
                foreach (var str in packet.Split(' '))
                {
                    if (i > 1) msg += str + " ";

                    i++;
                }

                Logger.LogUserEvent(logEvent: "GUILDCOMMAND", caller: Session.GenerateIdentity(),
                    data:
                    $"[Today][{Session.Character.Family.FamilyId}]CharacterName: {Session.Character.Name} Title: {msg}");

                var islog = Session.Character.Family.FamilyLogs.Any(predicate: s =>
                    s.FamilyLogType == FamilyLogType.DailyMessage
                    && s.FamilyLogData.StartsWith(value: Session.Character.Name,
                        comparisonType: StringComparison.CurrentCulture)
                    && s.Timestamp.AddDays(value: 1) > DateTime.Now);
                if (!islog)
                {
                    Session.Character.FamilyCharacter.DailyMessage = msg;
                    var fchar = Session.Character.FamilyCharacter;
                    DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref fchar);
                    Session.SendPacket(packet: Session.Character.GenerateFamilyMemberMessage());
                    Session.Character.Family.InsertFamilyLog(logtype: FamilyLogType.DailyMessage,
                        characterName: Session.Character.Name,
                        message: msg);
                }
                else
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "CANT_CHANGE_MESSAGE")));
                }
            }
        }

        #endregion
    }
}