using System;
using System.Configuration;
using System.Linq;
using OpenNos.Core;
using OpenNos.Core.Cryptography;
using OpenNos.Core.Handling;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Packets.ClientPackets;
using OpenNos.Master.Library.Client;

namespace OpenNos.Handler
{
    public class LoginPacketHandler : IPacketHandler
    {
        #region Members

        readonly ClientSession _session;

        #endregion

        #region Instantiation

        public LoginPacketHandler(ClientSession session)
        {
            _session = session;
        }

        #endregion

        #region Methods

        string BuildServersPacket(string username, int sessionId, bool ignoreUserName)
        {
            var channelpacket =
                CommunicationServiceClient.Instance.RetrieveRegisteredWorldServers(username: username,
                    sessionId: sessionId, ignoreUserName: ignoreUserName);

            if (channelpacket == null || !channelpacket.Contains(value: ':'))
            {
                Logger.Debug(
                    data: "Could not retrieve Worldserver groups. Please make sure they've already been registered.");
                _session.SendPacket(packet: $"fail {Language.Instance.GetMessageFromKey(key: "NO_WORLDSERVERS")}");
            }

            return channelpacket;
        }

        /// <summary>
        ///     login packet
        /// </summary>
        /// <param name="loginPacket"></param>
        public void VerifyLogin(LoginPacket loginPacket)
        {
            if (loginPacket == null || loginPacket.Name == null || loginPacket.Password == null) return;

            var user = new UserDto
            {
                Name = loginPacket.Name,
                Password = ConfigurationManager.AppSettings[name: "UseOldCrypto"] == "true"
                    ? CryptographyBase
                        .Sha512(inputString: LoginCryptography.GetPassword(password: loginPacket.Password)).ToUpper()
                    : loginPacket.Password
            };
            if (user == null || user.Name == null || user.Password == null) return;
            var loadedAccount = DaoFactory.AccountDao.LoadByName(name: user.Name);
            if (loadedAccount != null && loadedAccount.Name != user.Name)
            {
                _session.SendPacket(packet: $"failc {(byte)LoginFailType.WrongCaps}");
                return;
            }

            if (loadedAccount?.Password.ToUpper().Equals(value: user.Password) == true)
            {
                var ipAddress = _session.IpAddress;
                DaoFactory.AccountDao.WriteGeneralLog(accountId: loadedAccount.AccountId, ipAddress: ipAddress,
                    characterId: null,
                    logType: GeneralLogType.Connection, logData: "LoginServer");

                if (DaoFactory.PenaltyLogDao.LoadByIp(ip: ipAddress).Count() > 0)
                {
                    _session.SendPacket(packet: $"failc {(byte)LoginFailType.CantConnect}");
                    return;
                }

                //check if the account is connected
                if (!CommunicationServiceClient.Instance.IsAccountConnected(accountId: loadedAccount.AccountId))
                {
                    var type = loadedAccount.Authority;
                    var penalty = DaoFactory.PenaltyLogDao.LoadByAccount(accountId: loadedAccount.AccountId)
                        .FirstOrDefault(predicate: s => s.DateEnd > DateTime.Now && s.Penalty == PenaltyType.Banned);
                    if (penalty != null)
                        _session.SendPacket(packet: $"failc {(byte)LoginFailType.Banned}");
                    else
                        switch (type)
                        {
                            case AuthorityType.Unconfirmed:
                                {
                                    _session.SendPacket(packet: $"failc {(byte)LoginFailType.CantConnect}");
                                }
                                break;

                            case AuthorityType.Banned:
                                {
                                    _session.SendPacket(packet: $"failc {(byte)LoginFailType.Banned}");
                                }
                                break;

                            case AuthorityType.Closed:
                                {
                                    _session.SendPacket(packet: $"failc {(byte)LoginFailType.CantConnect}");
                                }
                                break;

                            default:
                                {
                                    if (loadedAccount.Authority < AuthorityType.Mod)
                                    {
                                        var maintenanceLog = DaoFactory.MaintenanceLogDao.LoadFirst();
                                        if (maintenanceLog != null && maintenanceLog.DateStart < DateTime.Now)
                                        {
                                            _session.SendPacket(packet: $"failc {(byte)LoginFailType.Maintenance}");
                                            return;
                                        }
                                    }

                                    var newSessionId = SessionFactory.Instance.GenerateSessionId();
                                    Logger.Debug(data: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "CONNECTION"), arg0: user.Name,
                                        arg1: newSessionId));
                                    try
                                    {
                                        ipAddress = ipAddress.Substring(startIndex: 6,
                                            length: ipAddress.LastIndexOf(value: ':') - 6);
                                        CommunicationServiceClient.Instance.RegisterAccountLogin(
                                            accountId: loadedAccount.AccountId,
                                            sessionId: newSessionId, ipAddress: ipAddress);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Error(data: "General Error SessionId: " + newSessionId, ex: ex);
                                    }

                                    var clientData = loginPacket.ClientData.Split('.');

                                    if (clientData.Length < 2) clientData = loginPacket.ClientDataOld.Split('.');

                                    var ignoreUserName = short.TryParse(s: clientData[3], result: out var clientVersion)
                                                         && (clientVersion < 3075
                                                             || ConfigurationManager.AppSettings[name: "UseOldCrypto"] ==
                                                             "true");
                                    _session.SendPacket(packet: BuildServersPacket(username: user.Name,
                                        sessionId: newSessionId, ignoreUserName: ignoreUserName));
                                }
                                break;
                        }
                }
                else
                {
                    _session.SendPacket(packet: $"failc {(byte)LoginFailType.AlreadyConnected}");
                }
            }
            else
            {
                _session.SendPacket(packet: $"failc {(byte)LoginFailType.AccountOrPasswordWrong}");
            }
        }

        #endregion
    }
}