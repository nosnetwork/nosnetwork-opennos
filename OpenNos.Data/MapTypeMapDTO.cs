﻿using System;

namespace OpenNos.Data
{
    [Serializable]
    public class MapTypeMapDto
    {
        #region Properties

        public short MapId { get; set; }

        public short MapTypeId { get; set; }

        #endregion
    }
}