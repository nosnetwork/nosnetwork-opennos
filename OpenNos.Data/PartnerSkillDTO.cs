﻿using System;

namespace OpenNos.Data
{
    [Serializable]
    public class PartnerSkillDto
    {
        #region Properties

        public long PartnerSkillId { get; set; }

        public Guid EquipmentSerialId { get; set; }

        public short SkillVNum { get; set; }

        public byte Level { get; set; }

        #endregion
    }
}