﻿using System;

namespace OpenNos.Data
{
    [Serializable]
    public class ComboDto
    {
        #region Properties

        public short Animation { get; set; }

        public int ComboId { get; set; }

        public short Effect { get; set; }

        public short Hit { get; set; }

        public short SkillVNum { get; set; }

        #endregion
    }
}