﻿using System;
using OpenNos.Data.Base;

namespace OpenNos.Data
{
    [Serializable]
    public class CharacterSkillDto : SynchronizableBaseDto
    {
        #region Properties

        public long CharacterId { get; set; }

        public short SkillVNum { get; set; }

        #endregion
    }
}