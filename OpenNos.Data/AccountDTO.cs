﻿using System;
using OpenNos.Domain;

namespace OpenNos.Data
{
    [Serializable]
    public class AccountDto
    {
        string _registrationIp;

        #region Properties

        public long AccountId { get; set; }

        public AuthorityType Authority { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public long ReferrerId { get; set; }

        public string RegistrationIp
        {
            get => _registrationIp;
            set => _registrationIp = value;
        }

        public string VerificationToken { get; set; }

        #endregion
    }
}