﻿using System;

namespace OpenNos.Data.Base
{
    [Serializable]
    public abstract class SynchronizableBaseDto
    {
        #region Instantiation

        protected SynchronizableBaseDto()
        {
            Id = Guid.NewGuid();
        }

        #endregion

        // make unique

        #region Properties

        public Guid Id { get; set; }

        #endregion

        #region Methods

        public override bool Equals(object obj)
        {
            // ReSharper disable once PossibleNullReferenceException
            return ((SynchronizableBaseDto)obj).Id == Id;
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            return Id.GetHashCode();
        }

        #endregion
    }
}