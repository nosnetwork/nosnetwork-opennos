﻿namespace OpenNos.Data.Interfaces
{
    public interface IMapDto
    {
        #region Properties

        byte[] Data { get; set; }

        short MapId { get; set; }

        short GridMapId { get; set; }

        int Music { get; set; }

        string Name { get; set; }

        bool ShopAllowed { get; set; }

        #endregion
    }
}