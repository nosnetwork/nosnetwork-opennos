﻿using System;
using OpenNos.Domain;

namespace OpenNos.Data
{
    [Serializable]
    public class ShellEffectDto
    {
        #region Properties

        public byte Effect { get; set; }

        public ShellEffectLevelType EffectLevel { get; set; }

        public Guid EquipmentSerialId { get; set; }

        public long ShellEffectId { get; set; }

        public short Value { get; set; }

        #endregion
    }
}