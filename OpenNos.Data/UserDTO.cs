﻿using System;

namespace OpenNos.Data
{
    [Serializable]
    public class UserDto
    {
        #region Properties

        public string Name { get; set; }

        public string Password { get; set; }

        public string Unknown { get; set; }

        #endregion
    }
}