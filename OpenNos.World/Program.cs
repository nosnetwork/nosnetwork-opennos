﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using log4net;
using OpenNos.Core;
using OpenNos.Core.Cryptography;
using OpenNos.Core.Serializing;
using OpenNos.DAL.EF.Helpers;
using OpenNos.GameObject;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Handler;
using OpenNos.Log.Networking;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;

namespace OpenNos.World
{
    public static class Program
    {
        #region Delegates

        public delegate bool EventHandler(CtrlType sig);

        #endregion

        #region Enums

        public enum CtrlType
        {
            CtrlCEvent = 0,
            CtrlBreakEvent = 1,
            CtrlCloseEvent = 2,
            CtrlLogoffEvent = 5,
            CtrlShutdownEvent = 6
        }

        #endregion

        #region Classes

        public static class NativeMethods
        {
            #region Methods

            [DllImport(dllName: "Kernel32")]
            internal static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

            #endregion
        }

        #endregion

        #region Members

        static EventHandler _exitHandler;

        public static bool _isDebug;

        static int _port;

#pragma warning disable CS0414 // Dem Feld "Program._ignoreTelemetry" wurde ein Wert zugewiesen, der aber nie verwendet wird.
        public static bool _ignoreTelemetry;
#pragma warning restore CS0414 // Dem Feld "Program._ignoreTelemetry" wurde ein Wert zugewiesen, der aber nie verwendet wird.

        #endregion

        #region Methods

        public static void Main(string[] args)
        {
#if DEBUG
            _isDebug = true;
            Thread.Sleep(millisecondsTimeout: 1000);
#endif
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo(name: "en-US");
            // ReSharper disable once LocalizableElement
            Console.Title = $"OpenNos World Server{(_isDebug ? " Development Environment" : "")}";

            var ignoreStartupMessages = false;
            _port = Convert.ToInt32(value: ConfigurationManager.AppSettings[name: "WorldPort"]);
            var portArgIndex = Array.FindIndex(array: args, match: s => s == "--port");
            if (portArgIndex != -1
                && args.Length >= portArgIndex + 1
                && int.TryParse(s: args[portArgIndex + 1], result: out _port))
                // ReSharper disable once LocalizableElement
                Console.WriteLine(value: "Port override: " + _port);
            foreach (var arg in args)
                switch (arg)
                {
                    case "--nomsg":
                        ignoreStartupMessages = true;
                        break;

                    case "--notelemetry":
                        _ignoreTelemetry = true;
                        break;
                }

            // initialize Logger
            Logger.InitializeLogger(log: LogManager.GetLogger(type: typeof(Program)));

            if (!ignoreStartupMessages)
            {
                var assembly = Assembly.GetExecutingAssembly();
                FileVersionInfo.GetVersionInfo(fileName: assembly.Location);
                var text = "WORLD SERVER by NN Team";
                var offset = Console.WindowWidth / 2 + text.Length / 2;
                var separator = new string(c: '=', count: Console.WindowWidth);
                Console.WriteLine(value: separator + string.Format(format: "{0," + offset + "}\n", arg0: text) +
                                         separator);
            }

            // initialize api
            var authKey = ConfigurationManager.AppSettings[name: "MasterAuthKey"];
            if (CommunicationServiceClient.Instance.Authenticate(authKey: authKey))
                Logger.Info(message: Language.Instance.GetMessageFromKey(key: "API_INITIALIZED"));

            // initialize DB
            if (DataAccessHelper.Initialize())
            {
                // initialilize maps
                ServerManager.Instance.Initialize();
            }
            else
            {
                Console.ReadKey();
                return;
            }

            // TODO: initialize ClientLinkManager initialize PacketSerialization
            PacketFactory.Initialize<WalkPacket>();

            try
            {
                _exitHandler += ExitHandler;
                AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
                NativeMethods.SetConsoleCtrlHandler(handler: _exitHandler, add: true);
            }
            catch (Exception ex)
            {
                Logger.Error(data: "General Error", ex: ex);
            }

            var ipAddress = ConfigurationManager.AppSettings[name: "IPAddress"];
            var publicIp = ConfigurationManager.AppSettings[name: "PublicIP"];

        portloop:
            try
            {
                // ReSharper disable once ObjectCreationAsStatement
                new NetworkManager<WorldCryptography>(ipAddress: ipAddress, port: _port,
                    packetHandler: typeof(CommandPacketHandler),
                    fallbackEncryptor: typeof(LoginCryptography), isWorldServer: true);
            }
            catch (SocketException ex)
            {
                if (ex.ErrorCode == 10048)
                {
                    _port++;
                    Logger.Info(message: "Port already in use! Incrementing...");
                    goto portloop;
                }

                Logger.Error(data: "General Error", ex: ex);
                Environment.Exit(exitCode: ex.ErrorCode);
            }

            ServerManager.Instance.ServerGroup = ConfigurationManager.AppSettings[name: "ServerGroup"];
            const int sessionLimit = 100; // Needs workaround
            var newChannelId = CommunicationServiceClient.Instance.RegisterWorldServer(
                worldServer: new SerializableWorldServer(id: ServerManager.Instance.WorldId, epIp: publicIp,
                    epPort: _port, accountLimit: sessionLimit,
                    worldGroup: ServerManager.Instance.ServerGroup));
            if (newChannelId.HasValue)
            {
                ServerManager.Instance.ChannelId = newChannelId.Value;
                MailServiceClient.Instance.Authenticate(authKey: authKey, serverId: ServerManager.Instance.WorldId);
                ConfigurationServiceClient.Instance.Authenticate(authKey: authKey,
                    serverId: ServerManager.Instance.WorldId);
                ServerManager.Instance.Configuration = ConfigurationServiceClient.Instance.GetConfigurationObject();
                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.Authenticate(authKey: ConfigurationManager.AppSettings[name: "LogKey"]);
                ServerManager.Instance.MallApi =
                    new MallAPIHelper(baseUrl: ServerManager.Instance.Configuration.MallBaseUrl);
                ServerManager.Instance.SynchronizeSheduling();
            }
            else
            {
                Logger.Error(data: "Could not retrieve ChannelId from Web API.");
                Console.ReadKey();
            }
        }

        static bool ExitHandler(CtrlType sig)
        {
            CommunicationServiceClient.Instance.UnregisterWorldServer(worldId: ServerManager.Instance.WorldId);
            ServerManager.Shout(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHUTDOWN_SEC"),
                arg0: 5));
            foreach (var sess in ServerManager.Instance.Sessions) sess.Character?.Dispose();
            ServerManager.Instance.SaveAll();
            Thread.Sleep(millisecondsTimeout: 5000);
            return false;
        }

        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            if (e == null) return;

            ServerManager.Instance.InShutdown = true;
            Logger.Error(ex: (Exception)e.ExceptionObject);

            // ReSharper disable once LocalizableElement
            File.AppendAllText(path: "C:\\WORLD_CRASHLOG.txt", contents: e.ExceptionObject + "\n");

            Logger.Debug(data: "Server crashed! Rebooting gracefully...");
            CommunicationServiceClient.Instance.UnregisterWorldServer(worldId: ServerManager.Instance.WorldId);
            ServerManager.Shout(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHUTDOWN_SEC"),
                arg0: 5));
            ServerManager.Instance.SaveAll();
            foreach (var sess in ServerManager.Instance.Sessions) sess.Character?.Dispose();
            Process.Start(fileName: "OpenNos.World.exe", arguments: $"--nomsg --port {_port}");
            Environment.Exit(exitCode: 1);
        }

        #endregion
    }
}