﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OpenNos.Log.Shared
{
    public class LogFileReader
    {
        public List<LogEntry> ReadChatLogFile(string path)
        {
            using (var stream = File.OpenRead(path: path))
            {
                using (var br = new BinaryReader(input: stream))
                {
                    var header = $"{br.ReadChar()}{br.ReadChar()}{br.ReadChar()}";
                    var version = br.ReadByte();
                    if (header == "ONC")
                        switch (version)
                        {
                            case 1:
                                return ReadChatLogVersion(reader: br);
                            default:
                                throw new InvalidDataException(message: "File Version invalid!");
                        }

                    throw new InvalidDataException(message: "File Header invalid!");
                }
            }
        }

        public List<PacketLogEntry> ReadPacketLogFile(string path)
        {
            using (var stream = File.OpenRead(path: path))
            {
                using (var br = new BinaryReader(input: stream))
                {
                    var header = $"{br.ReadChar()}{br.ReadChar()}{br.ReadChar()}";
                    var version = br.ReadByte();
                    if (header == "ONC")
                        switch (version)
                        {
                            case 1:
                                return ReadPacketLogVersion(reader: br);
                            default:
                                throw new InvalidDataException(message: "File Version invalid!");
                        }

                    throw new InvalidDataException(message: "File Header invalid!");
                }
            }
        }

        List<LogEntry> ReadChatLogVersion(BinaryReader reader)
        {
            var result = new List<LogEntry>();
            var count = reader.ReadInt32();
            while (count != 0)
            {
                var timestamp =
                    new DateTime(year: 1970, month: 1, day: 1, hour: 0, minute: 0, second: 0, kind: DateTimeKind.Utc)
                        .AddMilliseconds(value: reader.ReadDouble());
                var chatLogType = (LogType)reader.ReadByte();
                var sender = reader.ReadString();
                var senderId = reader.ReadInt64();
                var receiver = reader.ReadString();
                var receiverId = reader.ReadInt64();
                var message = reader.ReadString();
                result.Add(item: new LogEntry
                {
                    Timestamp = timestamp,
                    MessageType = chatLogType,
                    Sender = sender,
                    SenderId = senderId,
                    Receiver = receiver,
                    ReceiverId = receiverId,
                    Message = message
                });
                count--;
            }

            return result;
        }

        List<PacketLogEntry> ReadPacketLogVersion(BinaryReader reader)
        {
            var result = new List<PacketLogEntry>();
            var count = reader.ReadInt32();
            while (count != 0)
            {
                var timestamp =
                    new DateTime(year: 1970, month: 1, day: 1, hour: 0, minute: 0, second: 0, kind: DateTimeKind.Utc)
                        .AddMilliseconds(value: reader.ReadDouble());
                var logType = (LogType)reader.ReadByte();
                var sender = reader.ReadString();
                var senderId = reader.ReadInt64();
                var receiver = reader.ReadString();
                var receiverId = reader.ReadInt64();
                var packet = reader.ReadString();
                result.Add(item: new PacketLogEntry
                {
                    Timestamp = timestamp,
                    PacketType = logType,
                    Sender = sender,
                    SenderId = senderId,
                    Receiver = receiver,
                    ReceiverId = receiverId,
                    Packet = packet
                });
                count--;
            }

            return result;
        }
    }
}