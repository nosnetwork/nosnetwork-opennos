﻿namespace OpenNos.Log.Shared
{
    public enum LogType : byte
    {
        Map = 0,
        Speaker = 1,
        Whisper = 2,
        BuddyTalk = 3,
        Group = 4,
        Family = 5,
        Trade = 10,
        BazaarSell = 11,
        PrivateShopSell = 12,
        FamilyStorage = 13,
        Drop = 14,
        ItemCreate = 20,

        // ReSharper disable once UnusedMember.Global
        UserCommand = 50,

        // ReSharper disable once UnusedMember.Global
        ModeratorCommand = 51,

        // ReSharper disable once UnusedMember.Global
        GmCommand = 52,

        // ReSharper disable once UnusedMember.Global
        MallItemBuy = 60,

        // ReSharper disable once UnusedMember.Global
        Packet = 70
    }
}