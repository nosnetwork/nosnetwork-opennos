﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OpenNos.Log.Shared
{
    public class LogFileWriter
    {
        public void WriteChatLogFile(string path, List<LogEntry> logs)
        {
            if (logs.Count > 0)
                using (var stream = File.Create(path: path))
                {
                    using (var bw = new BinaryWriter(output: stream))
                    {
                        bw.Write(value: (byte)0x4F);
                        bw.Write(value: (byte)0x4E);
                        bw.Write(value: (byte)0x43);
                        bw.Write(value: (byte)1);
                        bw.Write(value: logs.Count);
                        foreach (var log in logs)
                        {
                            bw.Write(value: log.Timestamp.ToUniversalTime()
                                .Subtract(value: new DateTime(year: 1970, month: 1, day: 1, hour: 0, minute: 0,
                                    second: 0, kind: DateTimeKind.Utc)).TotalMilliseconds);
                            bw.Write(value: (byte)log.MessageType);
                            bw.Write(value: log.Sender ?? "");
                            bw.Write(value: log.SenderId ?? 0);
                            bw.Write(value: log.Receiver ?? "");
                            bw.Write(value: log.ReceiverId ?? 0);
                            bw.Write(value: log.Message ?? "");
                        }
                    }
                }
        }

        public void WritePacketLogFile(string path, List<PacketLogEntry> logs)
        {
            if (logs.Count > 0)
                using (var stream = File.Create(path: path))
                {
                    using (var bw = new BinaryWriter(output: stream))
                    {
                        bw.Write(value: (byte)0x4F);
                        bw.Write(value: (byte)0x4E);
                        bw.Write(value: (byte)0x43);
                        bw.Write(value: (byte)1);
                        bw.Write(value: logs.Count);
                        foreach (var log in logs)
                        {
                            bw.Write(value: log.Timestamp.ToUniversalTime()
                                .Subtract(value: new DateTime(year: 1970, month: 1, day: 1, hour: 0, minute: 0,
                                    second: 0, kind: DateTimeKind.Utc)).TotalMilliseconds);
                            bw.Write(value: (byte)log.PacketType);
                            bw.Write(value: log.Sender ?? "");
                            bw.Write(value: log.SenderId ?? 0);
                            bw.Write(value: log.Receiver ?? "");
                            bw.Write(value: log.ReceiverId ?? 0);
                            bw.Write(value: log.Packet ?? "");
                        }
                    }
                }
        }
    }
}