using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.ConcurrencyExtensions;
using OpenNos.Core.Extensions;
using OpenNos.Core.Threading;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Battle;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Event.ACT4;
using OpenNos.GameObject.Event.ICEBREAKER;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.GameObject.Packets.ServerPackets;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;
using OpenNos.PathFinder;
using static OpenNos.Domain.BCardType;

namespace OpenNos.GameObject
{
    public class Character : CharacterDto
    {
        #region Members

        readonly object _syncObj = new object();

        bool _isStaticBuffListInitial;

        public int slhpbonus;

        byte _speed;

        #endregion

        #region Instantiation

        public Character()
        {
            GroupSentRequestCharacterIds = new ThreadSafeGenericList<long>();
            FamilyInviteCharacters = new ThreadSafeGenericList<long>();
            TradeRequests = new ThreadSafeGenericList<long>();
            FriendRequestCharacters = new ThreadSafeGenericList<long>();
            MarryRequestCharacters = new ThreadSafeGenericList<long>();
            StaticBonusList = new List<StaticBonusDto>();
            MinilandObjects = new List<MinilandObject>();
            Mates = new List<Mate>();
            LastMonsterAggro = DateTime.Now;
            LastPulse = DateTime.Now;
            LastFreeze = DateTime.Now;
            MTListTargetQueue = new ConcurrentStack<MtListHitTarget>();
            MeditationDictionary = new Dictionary<short, DateTime>();
            PVELockObject = new object();
            SpeedLockObject = new object();
            ShellEffectArmor = new ConcurrentBag<ShellEffectDto>();
            ShellEffectMain = new ConcurrentBag<ShellEffectDto>();
            ShellEffectSecondary = new ConcurrentBag<ShellEffectDto>();
            Quests = new ConcurrentBag<CharacterQuest>();
        }

        public Character(CharacterDto input) : this()
        {
            AccountId = input.AccountId;
            Act4Dead = input.Act4Dead;
            Act4Kill = input.Act4Kill;
            Act4Points = input.Act4Points;
            ArenaWinner = input.ArenaWinner;
            Biography = input.Biography;
            BuffBlocked = input.BuffBlocked;
            CharacterId = input.CharacterId;
            Class = input.Class;
            Compliment = input.Compliment;
            Dignity = input.Dignity;
            EmoticonsBlocked = input.EmoticonsBlocked;
            ExchangeBlocked = input.ExchangeBlocked;
            Faction = input.Faction;
            FamilyRequestBlocked = input.FamilyRequestBlocked;
            FriendRequestBlocked = input.FriendRequestBlocked;
            Gender = input.Gender;
            Gold = input.Gold;
            GoldBank = input.GoldBank;
            GroupRequestBlocked = input.GroupRequestBlocked;
            HairColor = input.HairColor;
            HairStyle = input.HairStyle;
            HeroChatBlocked = input.HeroChatBlocked;
            HeroLevel = input.HeroLevel;
            HeroXp = input.HeroXp;
            Hp = input.Hp;
            HpBlocked = input.HpBlocked;
            IsPetAutoRelive = input.IsPetAutoRelive;
            IsPartnerAutoRelive = input.IsPartnerAutoRelive;
            IsSeal = input.IsSeal;
            JobLevel = input.JobLevel;
            JobLevelXp = input.JobLevelXp;
            LastFamilyLeave = input.LastFamilyLeave;
            Level = input.Level;
            LevelXp = input.LevelXp;
            MapId = input.MapId;
            MapX = input.MapX;
            MapY = input.MapY;
            MasterPoints = input.MasterPoints;
            MasterTicket = input.MasterTicket;
            MaxMateCount = input.MaxMateCount;
            MaxPartnerCount = input.MaxPartnerCount;
            MinilandInviteBlocked = input.MinilandInviteBlocked;
            MinilandMessage = input.MinilandMessage;
            MinilandPoint = input.MinilandPoint;
            MinilandState = input.MinilandState;
            MouseAimLock = input.MouseAimLock;
            Mp = input.Mp;
            Name = input.Name;
            QuickGetUp = input.QuickGetUp;
            RagePoint = input.RagePoint;
            Reputation = input.Reputation;
            Slot = input.Slot;
            SpAdditionPoint = input.SpAdditionPoint;
            SpPoint = input.SpPoint;
            State = input.State;
            TalentLose = input.TalentLose;
            TalentSurrender = input.TalentSurrender;
            TalentWin = input.TalentWin;
            WhisperBlocked = input.WhisperBlocked;
        }

        #endregion

        #region Properties

        public bool IsDisposed { get; private set; }

        public AuthorityType Authority { get; set; }

        public Node[][] BrushFireJagged { get; set; }

        public string BubbleMessage { get; set; }

        public DateTime BubbleMessageEnd { get; set; }

        public ThreadSafeSortedList<short, Buff.Buff> Buff
        {
            get => BattleEntity.Buffs;
        }

        public ThreadSafeSortedList<short, IDisposable> BuffObservables
        {
            get => BattleEntity.BuffObservables;
        }

        public bool CanFight
        {
            get => !IsSitting && ExchangeInfo == null;
        }

        public ThreadSafeGenericList<CellonOptionDto> CellonOptions
        {
            get => BattleEntity.CellonOptions;
        }

        public List<CharacterRelationDto> CharacterRelations
        {
            get
            {
                lock (ServerManager.Instance.CharacterRelations)
                {
                    return ServerManager.Instance.CharacterRelations == null
                        ? new List<CharacterRelationDto>()
                        : ServerManager.Instance.CharacterRelations.Where(predicate: s =>
                            s.CharacterId == CharacterId || s.RelatedCharacterId == CharacterId).ToList();
                }
            }
        }

        public MapCell SavedLocation { get; set; }

        public ServerManager Channel { get; set; }

        public short CurrentMinigame { get; set; }

        public int ChargeValue { get; set; }

        public int ConvertedDamageToHP { get; set; }

        public int DarkResistance { get; set; }

        public int Defence { get; set; }

        public int DefenceRate { get; set; }

        public byte BeforeDirection { get; set; }

        public byte Direction { get; set; }

        public int SecondWeaponCriticalRate { get; set; }

        public int SecondWeaponCriticalChance { get; set; }

        public int DistanceDefence { get; set; }

        public int DistanceDefenceRate { get; set; }

        public int SecondWeaponHitRate { get; set; }

        public byte Element { get; set; }

        public int ElementRate { get; set; }

        public int ElementRateSP { get; private set; }

        public ThreadSafeGenericLockedList<BCard> EquipmentBCards
        {
            get => BattleEntity.BCards;
        }

        public ExchangeInfo ExchangeInfo { get; set; }

        public Family Family { get; set; }

        public FamilyCharacterDto FamilyCharacter
        {
            get => Family?.FamilyCharacters.Find(match: s => s.CharacterId == CharacterId);
        }

        public ThreadSafeGenericList<long> FamilyInviteCharacters { get; set; }

        public int FireResistance { get; set; }

        public int FoodAmount { get; set; }

        public int FoodHp { get; set; }

        public int FoodMp { get; set; }

        public ThreadSafeGenericList<long> FriendRequestCharacters { get; set; }

        public ThreadSafeGenericList<long> MarryRequestCharacters { get; set; }

        public ThreadSafeGenericList<GeneralLogDto> GeneralLogs { get; set; }

        public bool GmPvtBlock { get; set; }

        public Group Group { get; set; }

        public ThreadSafeGenericList<long> GroupSentRequestCharacterIds { get; set; }

        public bool HasGodMode { get; set; }

        public bool HasShopOpened { get; set; }

        public int HitCriticalRate { get; set; }

        public int HitCriticalChance { get; set; }

        public int HitRate { get; set; }

        public bool InExchangeOrTrade
        {
            get => ExchangeInfo != null || Speed == 0;
        }

        public Inventory Inventory { get; set; }

        public bool Invisible { get; set; }

        public bool InvisibleGm { get; set; }

        public bool IsChangingMapInstance { get; set; }

        public bool IsCustomSpeed { get; set; }

        public bool IsDancing { get; set; }

        /// <summary>
        ///     Defines if the Character Is currently sending or getting items thru exchange.
        /// </summary>
        public bool IsExchanging { get; set; }

        public bool IsShopping { get; set; }

        public bool IsSitting { get; set; }

        public bool IsUsingFairyBooster
        {
            get =>
                _isStaticBuffListInitial
                    ? Buff.ContainsKey(key: 131)
                    : DaoFactory.StaticBuffDao.LoadByCharacterId(characterId: CharacterId)
                        .Any(predicate: s => s.CardId.Equals(obj: 131));
        }

        public bool IsVehicled { get; set; }

        public bool IsMorphed { get; set; }

        public bool IsWaitingForEvent { get; set; }

        public DateTime LastDefence { get; set; }

        public DateTime LastDelay { get; set; }

        public DateTime LastEffect { get; set; }

        public DateTime LastFunnelUse { get; set; }

        public DateTime LastHealth { get; set; }

        public DateTime LastLoyalty { get; set; }

        public short LastItemVNum { get; set; }

        public ClientSession LastPvPKiller { get; set; }

        public DateTime LastMapObject { get; set; }

        public DateTime LastMonsterAggro { get; set; }

        public DateTime LastMove { get; set; }

        public int LastNpcMonsterId { get; set; }

        public int LastNRunId { get; set; }

        public DateTime LastPermBuffRefresh { get; set; }

        public double LastPortal { get; set; }

        public DateTime LastPotion { get; set; }

        public DateTime LastPulse { get; set; }

        public DateTime LastFreeze { get; set; }

        public DateTime LastPVPRevive { get; set; }

        public DateTime LastQuestSummon { get; set; }

        public DateTime LastSkillComboUse { get; set; }

        public DateTime LastSkillUse { get; set; }

        public double LastSp { get; set; }

        public DateTime LastSpeedChange { get; set; }

        public DateTime LastSpGaugeRemove { get; set; }

        public DateTime LastQuest { get; set; }

        public DateTime LastTransform { get; set; }

        public DateTime LastVessel { get; set; }

        public int LightResistance { get; set; }

        public int MagicalDefence { get; set; }

        public IDictionary<int, MailDto> MailList { get; set; }

        public MapInstance MapInstance
        {
            get => ServerManager.GetMapInstance(id: MapInstanceId);
        }

        public Guid MapInstanceId { get; set; }

        public List<Mate> Mates { get; set; }

        public int SecondWeaponMaxHit { get; set; }

        public int MaxFood { get; set; }

        public int MaxHit { get; set; }

        public int MaxSnack { get; set; }

        public Dictionary<short, DateTime> MeditationDictionary { get; set; }

        public int MessageCounter { get; set; }

        public int SecondWeaponMinHit { get; set; }

        public int MinHit { get; set; }

        public MinigameLogDto MinigameLog { get; set; }

        public MapInstance Miniland { get; private set; }

        public List<MinilandObject> MinilandObjects { get; set; }

        public int Morph { get; set; }

        public int PreviousMorph { get; set; }

        public int MorphUpgrade { get; set; }

        public int MorphUpgrade2 { get; set; }

        public ConcurrentStack<MtListHitTarget> MTListTargetQueue { get; set; }

        public bool NoAttack { get; set; }

        public bool NoMove { get; set; }

        public int OriginalFaction = -1;

        Random _random;

        public List<EventContainer> OnDeathEvents
        {
            get => BattleEntity.OnDeathEvents;
        }

        public short PositionX { get; set; }

        public short PositionY { get; set; }

        public object PVELockObject { get; set; }

        public object SpeedLockObject { get; set; }

        public ConcurrentBag<CharacterQuest> Quests { get; internal set; }

        public List<QuicklistEntryDto> QuicklistEntries { get; private set; }

        public RespawnMapTypeDto Respawn
        {
            get
            {
                var respawn = new RespawnMapTypeDto
                {
                    DefaultX = 79,
                    DefaultY = 116,
                    DefaultMapId = 1,
                    RespawnMapTypeId = -1
                };

                if (Session.HasCurrentMapInstance && Session.CurrentMapInstance.Map.MapTypes.Count > 0)
                {
                    var respawnmaptype = Session.CurrentMapInstance.Map.MapTypes[index: 0].RespawnMapTypeId;
                    if (respawnmaptype != null)
                    {
                        var resp = Respawns.Find(match: s => s.RespawnMapTypeId == respawnmaptype);
                        if (resp == null)
                        {
                            var defaultresp = Session.CurrentMapInstance.Map.DefaultRespawn;
                            if (defaultresp != null)
                            {
                                respawn.DefaultX = defaultresp.DefaultX;
                                respawn.DefaultY = defaultresp.DefaultY;
                                respawn.DefaultMapId = defaultresp.DefaultMapId;
                                respawn.RespawnMapTypeId = (long)respawnmaptype;
                            }
                        }
                        else
                        {
                            respawn.DefaultX = resp.X;
                            respawn.DefaultY = resp.Y;
                            respawn.DefaultMapId = resp.MapId;
                            respawn.RespawnMapTypeId = (long)respawnmaptype;
                        }
                    }
                }
                else if (Session.HasCurrentMapInstance)
                {
                    var resp = Respawns.Find(match: s => s.RespawnMapTypeId == 0);
                    if (resp != null)
                    {
                        respawn.DefaultX = resp.X;
                        respawn.DefaultY = resp.Y;
                        respawn.DefaultMapId = resp.MapId;
                        respawn.RespawnMapTypeId = 1;
                    }
                }

                return respawn;
            }
        }

        public List<RespawnDto> Respawns { get; set; }

        public RespawnMapTypeDto Return
        {
            get
            {
                var respawn = new RespawnMapTypeDto();
                if (Session.HasCurrentMapInstance && Session.CurrentMapInstance.Map.MapTypes.Count > 0)
                {
                    var respawnmaptype = Session.CurrentMapInstance.Map.MapTypes[index: 0].ReturnMapTypeId;
                    if (respawnmaptype != null)
                    {
                        var resp = Respawns.Find(match: s => s.RespawnMapTypeId == respawnmaptype);
                        if (resp == null)
                        {
                            var defaultresp = Session.CurrentMapInstance.Map.DefaultReturn;
                            if (defaultresp != null)
                            {
                                respawn.DefaultX = defaultresp.DefaultX;
                                respawn.DefaultY = defaultresp.DefaultY;
                                respawn.DefaultMapId = defaultresp.DefaultMapId;
                                respawn.RespawnMapTypeId = (long)respawnmaptype;
                            }
                        }
                        else
                        {
                            respawn.DefaultX = resp.X;
                            respawn.DefaultY = resp.Y;
                            respawn.DefaultMapId = resp.MapId;
                            respawn.RespawnMapTypeId = (long)respawnmaptype;
                        }
                    }
                }
                else if (Session.HasCurrentMapInstance &&
                         Session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                {
                    var resp = Respawns.Find(match: s => s.RespawnMapTypeId == 1);
                    if (resp != null)
                    {
                        respawn.DefaultX = resp.X;
                        respawn.DefaultY = resp.Y;
                        respawn.DefaultMapId = resp.MapId;
                        respawn.RespawnMapTypeId = 1;
                    }
                }

                return respawn;
            }
        }

        public short SaveX { get; set; }

        public short SaveY { get; set; }

        public byte ScPage { get; set; }

        public ClientSession Session { get; private set; }

        public ConcurrentBag<ShellEffectDto> ShellEffectArmor { get; set; }

        public ConcurrentBag<ShellEffectDto> ShellEffectMain { get; set; }

        public ConcurrentBag<ShellEffectDto> ShellEffectSecondary { get; set; }

        public int Size { get; set; } = 10;

        public byte SkillComboCount { get; set; }

        public ThreadSafeSortedList<int, CharacterSkill> Skills { get; private set; }

        public ThreadSafeSortedList<int, CharacterSkill> SkillsSp { get; set; }

        public int SnackAmount { get; set; }

        public int SnackHp { get; set; }

        public int SnackMp { get; set; }

        public int SpCooldown { get; set; }

        public byte Speed
        {
            get
            {
                if (_speed > 59) return 59;
                return _speed;
            }

            set
            {
                LastSpeedChange = DateTime.Now;
                _speed = value > 59 ? (byte)59 : value;
            }
        }

        public List<StaticBonusDto> StaticBonusList { get; set; }

        public ScriptedInstance Timespace { get; set; }

        public bool TimespaceRewardGotten { get; set; }

        public int TimesUsed { get; set; }

        public ThreadSafeGenericList<long> TradeRequests { get; set; }

        public bool Undercover { get; set; }

        public bool UseSp { get; set; }

        public byte VehicleSpeed { private get; set; }

        public Item VehicleItem { get; set; }

        public int WareHouseSize
        {
            get
            {
                var mp = MinilandObjects
                    .Where(predicate: s =>
                        s.ItemInstance.Item.ItemType == ItemType.House && s.ItemInstance.Item.ItemSubType == 2)
                    .OrderByDescending(keySelector: s => s.ItemInstance.Item.MinilandObjectPoint).FirstOrDefault();
                if (mp != null) return mp.ItemInstance.Item.MinilandObjectPoint;
                return 0;
            }
        }

        public int WaterResistance { get; set; }

        public IDisposable Life { get; set; }

        public IDisposable WalkDisposable { get; set; }

        public IDisposable SealDisposable { get; set; }

        public bool PyjamaDead { get; set; }

        #endregion

        #region BattleEntityProperties

        public BattleEntity BattleEntity { get; set; }

        public void AddBuff(Buff.Buff indicator, BattleEntity sender, bool noMessage = false, short x = 0, short y = 0)
        {
            BattleEntity.AddBuff(indicator: indicator, sender: sender, noMessage: noMessage, x: x, y: y);
        }

        public void RemoveBuff(short cardId, bool removePermaBuff = false)
        {
            BattleEntity.RemoveBuff(id: cardId, removePermaBuff: removePermaBuff);
        }

        public int[] GetBuff(CardType type, byte subtype)
        {
            return BattleEntity.GetBuff(type: type, subtype: subtype);
        }

        public bool HasBuff(short cardId)
        {
            return BattleEntity.HasBuff(cardId: cardId);
        }

        public bool HasBuff(CardType type, byte subtype)
        {
            return BattleEntity.HasBuff(type: type, subtype: subtype);
        }

        public int LastComboCastId { get; set; }

        public bool HasMagicalFetters
        {
            get => HasBuff(cardId: 608);
        }

        public bool HasMagicSpellCombo
        {
            get => HasBuff(cardId: 617) && LastComboCastId >= 11 && LastComboCastId <= 15;
        }

        public void DisableBuffs(BuffType type, int level = 100)
        {
            BattleEntity.DisableBuffs(type: type, level: level);
        }

        public void DisableBuffs(List<BuffType> types, int level = 100)
        {
            BattleEntity.DisableBuffs(types: types, level: level);
        }

        public double HPLoad()
        {
            return BattleEntity.HPLoad();
        }

        public double MPLoad()
        {
            return BattleEntity.MPLoad();
        }

        public string GenerateRc(int characterHealth)
        {
            return BattleEntity.GenerateRc(characterHealth: characterHealth);
        }

        public string GenerateDm(int dmg)
        {
            return BattleEntity.GenerateDm(dmg: dmg);
        }

        public void GetDamage(int damage, BattleEntity damager, bool dontKill = false)
        {
            BattleEntity.GetDamage(damage: damage, damager: damager, dontKill: dontKill);
        }

        public void DecreaseMp(int amount)
        {
            BattleEntity.DecreaseMp(amount: amount);
        }

        #endregion

        #region Methods

        public List<long> GetMTListTargetQueue_QuickFix(CharacterSkill ski, UserType entityType)
        {
            var result = new List<long>();

            if (BattleEntity != null
                && MapInstance != null
                && ski?.Skill != null)
                foreach (var targetId in MTListTargetQueue.Where(predicate: target => target.EntityType == entityType
                                                                                      && (byte)target.TargetHitType ==
                                                                                      ski.Skill.HitType)
                    .Select(selector: s => s.TargetId))
                {
                    switch (entityType)
                    {
                        case UserType.Player:
                            {
                                var targetCharacter = MapInstance.GetCharacterById(characterId: targetId);

                                if (targetCharacter?.BattleEntity == null /* Invalid character  */
                                    || targetCharacter.Hp < 1 /* Amen */
                                    || !targetCharacter.IsInRange(xCoordinate: PositionX, yCoordinate: PositionY,
                                        range: ski.Skill.Range) /* Character not in range */
                                    || !BattleEntity.CanAttackEntity(
                                        receiver: targetCharacter.BattleEntity) /* Try again later */
                                )
                                    continue;
                            }
                            break;

                        case UserType.Monster:
                            {
                                var targetMonster = MapInstance.GetMonsterById(mapMonsterId: targetId);

                                if (targetMonster?.BattleEntity == null /* Invalid monster */
                                    || !targetMonster.IsAlive /* Amen */
                                    || targetMonster.CurrentHp < 1 /* Schrödinger's cat */
                                    || !targetMonster.IsInRange(mapX: PositionX, mapY: PositionY,
                                        distance: ski.Skill.Range) /* Monster not in range */
                                    || !BattleEntity.CanAttackEntity(
                                        receiver: targetMonster.BattleEntity) /* Try again later */
                                )
                                    continue;
                            }
                            break;
                    }

                    result.Add(item: targetId);
                }

            return result;
        }

        public bool CanUseNosBazaar()
        {
            if (MapInstance == null) return false;

            var medal = Session.Character.StaticBonusList.Find(match: s =>
                s.StaticBonusType == StaticBonusType.BazaarMedalGold ||
                s.StaticBonusType == StaticBonusType.BazaarMedalSilver);

            if (medal == null)
                // Check if there is NosBazaar in Map
                if (!Session.Character.MapInstance.Npcs.Any(predicate: s => s.NpcVNum == 793))
                    return false;

            return true;
        }

        public void ClearLaurena()
        {
            if (IsLaurenaMorph())
            {
                IsMorphed = false;
                Morph = PreviousMorph;
                PreviousMorph = 0;
                MapInstance?.Broadcast(packet: GenerateCMode());
            }

            RemoveBuff(cardId: 477, removePermaBuff: true);
            RemoveBuff(cardId: 478, removePermaBuff: true);
        }

        public void ResetSkills()
        {
            Skills.ClearAll();

            switch ((byte)Class)
            {
                case 0:
                    {
                        LearnAdventurerSkills(isCommand: true);
                    }
                    break;

                case 1:
                    {
                        Session.Character.AddSkill(skillVNum: 220);
                        Session.Character.AddSkill(skillVNum: 221);
                        Session.Character.AddSkill(skillVNum: 235);
                    }
                    break;

                case 2:
                    {
                        Session.Character.AddSkill(skillVNum: 240);
                        Session.Character.AddSkill(skillVNum: 241);
                        Session.Character.AddSkill(skillVNum: 236);
                    }
                    break;

                case 3:
                    {
                        Session.Character.AddSkill(skillVNum: 260);
                        Session.Character.AddSkill(skillVNum: 261);
                        Session.Character.AddSkill(skillVNum: 237);
                    }
                    break;

                case 4:
                    {
                        Enumerable.Range(start: 1525, count: 15).ToList()
                            .ForEach(action: skillVNum => Session.Character.AddSkill(skillVNum: (short)skillVNum));
                        Session.Character.AddSkill(skillVNum: 1565);
                    }
                    break;
            }

            if (!Session.Character.UseSp)
            {
                Session.SendPacket(packet: Session.Character.GenerateSki());
                Session.SendPackets(packets: Session.Character.GenerateQuicklist());
            }
        }

        public List<CharacterSkill> GetSkills()
        {
            return UseSp
                ? SkillsSp.GetAllItems().Concat(second: Skills.Where(predicate: s => s.SkillVNum < 200)).ToList()
                : Skills.GetAllItems();
        }

        public CharacterSkill GetSkill(short skillVNum)
        {
            return GetSkills()?.FirstOrDefault(predicate: s => s.SkillVNum == skillVNum);
        }

        public CharacterSkill GetSkillByCastId(short castId)
        {
            return GetSkills()?.FirstOrDefault(predicate: s => s.Skill?.CastId == castId);
        }

        public string GeneratePetskill(int VNum = -1)
        {
            return $"petski {VNum}";
        }

        public void AddQuest(long questId, bool isMain = false) //Activated Main Quest
        {
            Session.SendPacket(packet: Session.Character.GenerateSay(message: $"QuestId: {questId}", type: 10));
            var characterQuest = new CharacterQuest(questId: questId, characterId: CharacterId);
            if (Quests.Any(predicate: q => q.QuestId == questId) || characterQuest.Quest == null ||
                isMain && Quests.Any(predicate: q => q.IsMainQuest)
                || Quests.Where(predicate: q => q.Quest.QuestType != (byte)QuestType.WinRaid).ToList().Count >= 5 &&
                characterQuest.Quest.QuestType != (byte)QuestType.WinRaid && !isMain
                || (QuestType)characterQuest.Quest.QuestType == QuestType.FlowerQuest &&
                Quests.Any(predicate: s => (QuestType)s.Quest.QuestType == QuestType.FlowerQuest))
                return;
            if (characterQuest.Quest.LevelMin > Level)
            {
                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "TOO_LOW_LVL"),
                    type: 0));
                return;
            }

            if ( /*ServerManager.Instance.Configuration.MaxLevel == 99 &&*/ characterQuest.Quest.LevelMax < Level)
            {
                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "TOO_HIGH_LVL"),
                    type: 0));
                return;
            }

            var isSpQuest = false;

            if (questId >= 2000 && questId <= 2007 // Pajama
                || questId >= 2008 && questId <= 2013 // SP 1
                || questId >= 2014 && questId <= 2020 // SP 2
                || questId >= 2060 && questId <= 2095 // SP 3
                || questId >= 2100 && questId <= 2134 // SP 4
            )
                isSpQuest = true;

            if (!isSpQuest)
            {
                if (!characterQuest.Quest.IsDaily && !characterQuest.IsMainQuest &&
                    (QuestType)characterQuest.Quest.QuestType != QuestType.FlowerQuest)
                {
                    if (DaoFactory.QuestLogDao.LoadByCharacterId(id: CharacterId)
                        .Any(predicate: s => s.QuestId == questId))
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "QUEST_ALREADY_DONE"),
                                type: 0));
                        return;
                    }
                }
                else if (characterQuest.Quest.IsDaily &&
                         (QuestType)characterQuest.Quest.QuestType != QuestType.FlowerQuest)
                {
                    if (DaoFactory.QuestLogDao.LoadByCharacterId(id: CharacterId).Any(predicate: s =>
                        s.QuestId == questId && s.LastDaily != null &&
                        s.LastDaily.Value.AddHours(value: 24) >= DateTime.Now))
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "QUEST_ALREADY_DONE_TODAY"),
                                type: 0));
                        return;
                    }
                }
            }

            if (characterQuest.Quest.QuestType == (int)QuestType.TimesSpace && ServerManager.Instance.TimeSpaces.All(
                    predicate: si => si.QuestTimeSpaceId !=
                                     (characterQuest.Quest.QuestObjectives.FirstOrDefault()?.SpecialData ?? -1))
                || characterQuest.Quest.QuestType == (int)QuestType.Product ||
                characterQuest.Quest.QuestType == (int)QuestType.Collect3
                || characterQuest.Quest.QuestType == (int)QuestType.TransmitGold ||
                characterQuest.Quest.QuestType == (int)QuestType.TsPoint ||
                characterQuest.Quest.QuestType == (int)QuestType.NumberOfKill
                || characterQuest.Quest.QuestType == (int)QuestType.TargetReput ||
                characterQuest.Quest.QuestType == (int)QuestType.Inspect ||
                characterQuest.Quest.QuestType == (int)QuestType.Needed
                || characterQuest.Quest.QuestType == (int)QuestType.Collect5 ||
                QuestHelper.Instance.SkipQuests.Any(predicate: q => q == characterQuest.QuestId))
            {
                Session.SendPacket(packet: characterQuest.Quest.GetRewardPacket(character: this, onlyItems: true));
                AddQuest(questId: characterQuest.Quest.NextQuestId ?? -1, isMain: isMain);
                return;
            }

            if (characterQuest.Quest.TargetMap != null) Session.SendPacket(packet: characterQuest.Quest.TargetPacket());

            characterQuest.IsMainQuest = isMain;
            Quests.Add(item: characterQuest);
            Session.SendPacket(packet: GenerateQuestsPacket(newQuestId: questId));
            if (characterQuest.Quest.QuestType == (int)QuestType.UnKnow)
                Session.Character.IncrementObjective(quest: characterQuest, isOver: true);
            Session.SendPacket(packet: GetSqst());
        }

        public string GetSqst()
        {
            var questLogs = DaoFactory.QuestLogDao.LoadByCharacterId(id: CharacterId).ToList();
            var quests = Quests.ToList();
            var sqst = "sqst  3 ";
            for (var i = 0; i < 250; i++)
            {
                var tempSqst = "}";
                //string tempSqst = "0";
                var count = 0;
                foreach (var questLog in questLogs)
                    if (i == ServerManager.Instance.GetQuest(questId: questLog.QuestId).SqstPosition)
                    {
                        double test = ServerManager.Instance.GetQuest(questId: questLog.QuestId).SqstPosition;
                        count = ServerManager.Instance.Quests.ToList().Where(predicate: s =>
                            !questLogs.Any(predicate: q => q.QuestId == s.QuestId) && s.SqstPosition == i).Count();
                        var count2 = questLogs.Where(predicate: s =>
                            !quests.Any(predicate: q => q.QuestId == s.QuestId) &&
                            ServerManager.Instance.GetQuest(questId: s.QuestId).SqstPosition == i).Count();

                        // O, v, }, l
                        tempSqst = questLog.QuestId switch
                        {
                            5051 => // Pos 92
                            "O",
                            5053 => // Pos 93
                            "v",
                            5055 => // Pos 93
                            "l",
                            5057 => // Pos 93
                            "}",
                            5059 => // Pos 94
                            "v",
                            5061 => // Pos 94
                            "}",
                            5065 => // Pos 95
                            "v",
                            5067 => // Pos 95
                            "}",
                            5070 => // Pos 95
                            "}" // Maybe } + 1
                            ,
                            5071 => // Pos 96
                            "l",
                            5081 => "O",
                            5105 => "O",
                            var _ => "O"
                        };
                    }

                foreach (var quest in quests)
                    if (i == ServerManager.Instance.GetQuest(questId: quest.Quest.QuestId).SqstPosition)
                    {
                        double test = ServerManager.Instance.GetQuest(questId: quest.Quest.QuestId).SqstPosition;
                        count = ServerManager.Instance.Quests
                            .Where(predicate: s =>
                                s.SqstPosition == i && !questLogs.Any(predicate: q => q.QuestId == s.QuestId)).Count();
                        count = questLogs.Where(predicate: s =>
                            !quests.Any(predicate: q => q.QuestId == s.QuestId) &&
                            ServerManager.Instance.GetQuest(questId: s.QuestId).SqstPosition == i).Count();

                        // 8, u, x
                        if (quest.Quest.QuestType == 22)
                            tempSqst = quest.Quest.QuestId switch
                            {
                                5051 => "8",
                                5053 => "u",
                                5055 => "x",
                                5057 => "8",
                                5059 => "u",
                                5061 => "x",
                                5065 => "u",
                                5067 => "x",
                                5071 => "u",
                                5081 => "8",
                                5105 => "8",
                                var _ => "8"
                            };
                    }

                if (i == 233) tempSqst = "2";
                sqst += tempSqst;
            }

            return sqst;
        }

        public void RemoveQuest(long questId, bool IsGivingUp = false)
        {
            var questToRemove = Quests.FirstOrDefault(predicate: q => q.QuestId == questId);

            if (questToRemove == null) return;

            if (questToRemove.Quest.TargetMap != null)
                Session.SendPacket(packet: questToRemove.Quest.RemoveTargetPacket());

            Quests.RemoveWhere(predicate: s => s.QuestId != questId, queueReturned: out var tmp);
            Quests = tmp;

            Session.SendPacket(packet: GenerateQuestsPacket());

            if (IsGivingUp) return;

            if (questToRemove.Quest.EndDialogId != null)
                Session.SendPacket(packet: GenerateNpcDialog(value: (int)questToRemove.Quest.EndDialogId));

            if (questToRemove.Quest.NextQuestId != null)
                AddQuest(questId: (long)questToRemove.Quest.NextQuestId, isMain: questToRemove.IsMainQuest);

            LogHelper.Instance.InsertQuestLog(characterId: CharacterId, ipAddress: Session.IpAddress,
                questId: questToRemove.Quest.QuestId,
                lastDaily: DateTime.Now);

            Session.SendPacket(packet: GetSqst());

            #region Specialist Card Quest Reward

            switch (questId)
            {
                case 2007: // Pajama
                    {
                        Session.Character.GiftAdd(itemVNum: 900, amount: 1);
                    }
                    break;
                case 2013: // SP 1
                    {
                        switch (Session.Character.Class)
                        {
                            case ClassType.Swordsman:
                                Session.Character.GiftAdd(itemVNum: 901, amount: 1);
                                break;
                            case ClassType.Archer:
                                Session.Character.GiftAdd(itemVNum: 903, amount: 1);
                                break;
                            case ClassType.Magician:
                                Session.Character.GiftAdd(itemVNum: 905, amount: 1);
                                break;
                        }
                    }
                    break;
                case 2020: // SP 2
                    {
                        switch (Session.Character.Class)
                        {
                            case ClassType.Swordsman:
                                Session.Character.GiftAdd(itemVNum: 902, amount: 1);
                                break;
                            case ClassType.Archer:
                                Session.Character.GiftAdd(itemVNum: 904, amount: 1);
                                break;
                            case ClassType.Magician:
                                Session.Character.GiftAdd(itemVNum: 906, amount: 1);
                                break;
                        }
                    }
                    break;
                case 2095: // SP 3
                    {
                        switch (Session.Character.Class)
                        {
                            case ClassType.Swordsman:
                                Session.Character.GiftAdd(itemVNum: 909, amount: 1);
                                break;
                            case ClassType.Archer:
                                Session.Character.GiftAdd(itemVNum: 911, amount: 1);
                                break;
                            case ClassType.Magician:
                                Session.Character.GiftAdd(itemVNum: 913, amount: 1);
                                break;
                        }
                    }
                    break;
                case 2134: // SP 4
                    {
                        switch (Session.Character.Class)
                        {
                            case ClassType.Swordsman:
                                Session.Character.GiftAdd(itemVNum: 910, amount: 1);
                                break;
                            case ClassType.Archer:
                                Session.Character.GiftAdd(itemVNum: 912, amount: 1);
                                break;
                            case ClassType.Magician:
                                Session.Character.GiftAdd(itemVNum: 914, amount: 1);
                                break;
                        }
                    }
                    break;
            }

            #endregion
        }

        public string GenerateBubbleMessagePacket()
        {
            return $"csp {CharacterId} {BubbleMessage}";
        }

        public string GenerateQuestsPacket(long newQuestId = -1)
        {
            short a = 0;
            short b = 6;
            Quests.ToList().ForEach(action: qst =>
            {
                qst.QuestNumber = qst.IsMainQuest
                    ? (short)5
                    : !qst.IsMainQuest && !qst.Quest.IsDaily || qst.Quest.QuestId >= 5000
                        ? b++
                        : a++;
            });
            return
                $"qstlist {Quests.Aggregate(seed: "", func: (current, quest) => current + $" {quest.GetInfoPacket(sendMsg: quest.QuestId == newQuestId)}")}";
        }

        public void IncrementQuests(QuestType type, int firstData = 0, int secondData = 0, int thirdData = 0,
            bool forGroupMember = false)
        {
            foreach (var quest in Quests.Where(predicate: q => q?.Quest?.QuestType == (int)type))
                switch ((QuestType)quest.Quest.QuestType)
                {
                    case QuestType.Capture1:
                    case QuestType.Capture2:
                    case QuestType.WinRaid:
                        quest.Quest.QuestObjectives.Where(predicate: o => o.Data == firstData).ToList()
                            .ForEach(action: d => IncrementObjective(quest: quest, index: d.ObjectiveIndex));
                        break;

                    case QuestType.Collect1:
                    case QuestType.Collect2:
                    case QuestType.Collect3:
                    case QuestType.Collect4:
                    case QuestType.Hunt:
                        quest.Quest.QuestObjectives.Where(predicate: o => o.Data == firstData).ToList()
                            .ForEach(action: d => IncrementObjective(quest: quest, index: d.ObjectiveIndex));
                        if (!forGroupMember)
                            IncrementGroupQuest(type: type, firstData: firstData, secondData: secondData,
                                thirdData: thirdData);
                        break;

                    case QuestType.Product:
                        quest.Quest.QuestObjectives.Where(predicate: o => o.Data == firstData).ToList()
                            .ForEach(action: d =>
                                IncrementObjective(quest: quest, index: d.ObjectiveIndex, amount: secondData));
                        break;

                    case QuestType.Dialog1:
                    case QuestType.Dialog2:
                        quest.Quest.QuestObjectives.Where(predicate: o => o.Data == firstData).ToList().ForEach(
                            action: d =>
                                IncrementObjective(quest: quest, index: d.ObjectiveIndex, isOver: true));
                        break;

                    case QuestType.Wear:
                        if (quest.Quest.QuestObjectives.Any(predicate: q => q.SpecialData == firstData &&
                                                                            (Session.Character.Inventory.Any(
                                                                                 predicate: i =>
                                                                                     i.ItemVNum == q.Data &&
                                                                                     i.Type == InventoryType.Wear) ||
                                                                             (quest.QuestId == 1541 ||
                                                                              quest.QuestId == 1546) &&
                                                                             Class != ClassType.Adventurer)))
                            IncrementObjective(quest: quest, isOver: true);
                        break;

                    case QuestType.Brings:
                    case QuestType.Required:
                        quest.Quest.QuestObjectives.Where(predicate: o => o.Data == firstData).ToList().ForEach(
                            action: d =>
                            {
                                if (Inventory.CountItem(itemVNum: d.SpecialData ?? -1) >= d.Objective)
                                {
                                    Inventory.RemoveItemAmount(vnum: d.SpecialData ?? -1, amount: d.Objective ?? 1);
                                    IncrementObjective(quest: quest, index: d.ObjectiveIndex, amount: d.Objective ?? 1);
                                }
                            });
                        break;

                    case QuestType.GoTo:
                        if (quest.Quest.TargetMap == firstData &&
                            Math.Abs(value: secondData - quest.Quest.TargetX ?? 0) < 3 &&
                            Math.Abs(value: thirdData - quest.Quest.TargetY ?? 0) < 3)
                            IncrementObjective(quest: quest, isOver: true);
                        break;

                    case QuestType.Use:
                        quest.Quest.QuestObjectives
                            .Where(predicate: o => o.Data == firstData &&
                                                   Mates.Any(predicate: m =>
                                                       m.NpcMonsterVNum == o.SpecialData && m.IsTeamMember)).ToList()
                            .ForEach(action: d =>
                                IncrementObjective(quest: quest, index: d.ObjectiveIndex, amount: d.Objective ?? 1));
                        break;

                    case QuestType.FlowerQuest:
                        if (firstData + 10 < Level) continue;
                        IncrementObjective(quest: quest, index: 1);
                        break;

                    case QuestType.TimesSpace:
                        quest.Quest.QuestObjectives.Where(predicate: o => o.SpecialData == firstData).ToList()
                            .ForEach(action: d => IncrementObjective(quest: quest, index: d.ObjectiveIndex));
                        break;

                    //TODO : Later 
                    case QuestType.TsPoint:
                    case QuestType.NumberOfKill:
                    case QuestType.Inspect:
                    case QuestType.Needed:
                    case QuestType.TargetReput:
                    case QuestType.TransmitGold:
                    case QuestType.Collect5:
                        break;
                }
        }

        void IncrementGroupQuest(QuestType type, int firstData = 0, int secondData = 0, int thirdData = 0)
        {
            if (Group != null && Group.GroupType == GroupType.Group)
                foreach (var groupMember in Group.Sessions.Where(predicate: s =>
                    s.Character.MapInstance == MapInstance && s.Character.CharacterId != CharacterId))
                    groupMember.Character.IncrementQuests(type: type, firstData: firstData, secondData: secondData,
                        thirdData: thirdData, forGroupMember: true);
        }

        void IncrementObjective(CharacterQuest quest, byte index = 0, int amount = 1, bool isOver = false)
        {
            var isFinish = isOver;
            Session.SendPacket(packet: quest.GetProgressMessage(index: index, amount: amount));
            quest.Incerment(index: index, amount: amount);
            byte a = 1;
            if (quest.GetObjectives().All(predicate: q =>
                quest.GetObjectiveByIndex(index: a) == null || q >= quest.GetObjectiveByIndex(index: a++).Objective))
                isFinish = true;
            Session.SendPacket(packet: $"qsti {quest.GetInfoPacket(sendMsg: false)}");
            if (!isFinish) return;
            LastQuest = DateTime.Now;
            if (CustomQuestRewards(type: (QuestType)quest.Quest.QuestType, questId: quest.Quest.QuestId))
            {
                RemoveQuest(questId: quest.QuestId);
                return;
            }

            Session.SendPacket(packet: quest.Quest.GetRewardPacket(character: this));
            RemoveQuest(questId: quest.QuestId);
        }

        public bool CustomQuestRewards(QuestType type, long questId)
        {
            switch (type)
            {
                case QuestType.FlowerQuest:
                    GetDignity(amount: 100);
                    if (ServerManager.RandomNumber() < 50)
                    {
                        RemoveBuff(cardId: 379);
                        AddBuff(indicator: new Buff.Buff(id: 378, level: Level), sender: BattleEntity);
                    }
                    else
                    {
                        RemoveBuff(cardId: 378);
                        AddBuff(indicator: new Buff.Buff(id: 379, level: Level), sender: BattleEntity);
                    }

                    return true;
            }

            switch (questId)
            {
                case 2255:
                    var possibleRewards = new short[] { 1894, 1895, 1896, 1897, 1898, 1899, 1900, 1901, 1902, 1903 };
                    GiftAdd(
                        itemVNum: possibleRewards[ServerManager.RandomNumber(min: 0, max: possibleRewards.Length - 1)],
                        amount: 1);
                    return true;
            }

            return false;
        }

        public void RemoveTemporalMates()
        {
            Mates.Where(predicate: s => s.IsTemporalMate).ToList().ForEach(action: m =>
            {
                m.GetInventory().ForEach(action: s => { Inventory.Remove(key: s.Id); });
                Mates.Remove(item: m);
                byte i = 0;
                Mates.Where(predicate: s => s.MateType == MateType.Partner).ToList().ForEach(action: s =>
                {
                    s.GetInventory().ForEach(action: item => item.Type = (InventoryType)(13 + i));
                    s.PetId = i;
                    i++;
                });
                Session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());
                Session.SendPackets(packets: GenerateScP());
                Session.SendPackets(packets: GenerateScN());
                MapInstance.Broadcast(packet: m.GenerateOut());
            });
        }

        public bool AddPet(Mate mate)
        {
            InventoryType newMateInventory;
            newMateInventory = (InventoryType)(13 + mate.PetId);
            if (CanAddMate(mate: mate) || mate.IsTemporalMate)
            {
                Mates.Add(item: mate);
                MapInstance.Broadcast(packet: mate.GenerateIn());
                if (!mate.IsTemporalMate)
                    Session.SendPacket(
                        packet: GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "YOU_GET_PET"),
                                arg0: mate.Name), type: 12));
                Session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());
                Session.SendPackets(packets: GenerateScP());
                Session.SendPackets(packets: GenerateScN());
                /*switch (mate.Monster.AttackClass) //Deactivated (Equipment Bug)
                {
                    case 0:
                        // Partner Basic Weapon
                        mate.WeaponInstance = Inventory.AddNewToInventory(990, 1, newMateInventory).FirstOrDefault();
                        // Partner Basic Armor
                        mate.ArmorInstance = Inventory.AddNewToInventory(997, 1, newMateInventory).FirstOrDefault();
                        break;
                    case 1:
                        // Partner Basic Weapon
                        mate.WeaponInstance = Inventory.AddNewToInventory(991, 1, newMateInventory).FirstOrDefault();
                        // Partner Basic Armor
                        mate.ArmorInstance = Inventory.AddNewToInventory(996, 1, newMateInventory).FirstOrDefault();
                        break;
                    case 2:
                        // Partner Basic Weapon
                        mate.WeaponInstance = Inventory.AddNewToInventory(992, 1, newMateInventory).FirstOrDefault();
                        // Partner Basic Armor
                        mate.ArmorInstance = Inventory.AddNewToInventory(995, 1, newMateInventory).FirstOrDefault();
                        break;
                }*/
                Session.SendPackets(packets: GenerateScN());
                mate.RefreshStats();
                return true;
            }

            return false;
        }

        public void AddPetWithSkill(Mate mate)
        {
            var isUsingMate = true;
            if (!Mates.ToList().Any(predicate: s => s.IsTeamMember && s.MateType == mate.MateType))
            {
                isUsingMate = false;
                mate.IsTeamMember = true;
            }
            else
            {
                mate.BackToMiniland();
            }

            Session.SendPacket(packet: $"ctl 2 {mate.MateTransportId} 3");
            Mates.Add(item: mate);
            Session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());
            Session.SendPackets(packets: GenerateScP());
            Session.SendPackets(packets: GenerateScN());
            if (!isUsingMate)
            {
                Parallel.ForEach(source: Session.CurrentMapInstance.Sessions.Where(predicate: s => s.Character != null),
                    body: s =>
                    {
                        if (ServerManager.Instance.ChannelId != 51 || Session.Character.Faction == s.Character.Faction)
                            s.SendPacket(packet: mate.GenerateIn(hideNickname: false,
                                isAct4: ServerManager.Instance.ChannelId == 51));
                        else
                            s.SendPacket(packet: mate.GenerateIn(hideNickname: true,
                                isAct4: ServerManager.Instance.ChannelId == 51,
                                receiverAuthority: s.Account.Authority));
                    });
                Session.SendPacket(packet: GeneratePinit());
                Session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());
                Session.SendPackets(packets: GenerateScP());
                Session.SendPackets(packets: GenerateScN());
                Session.SendPackets(packets: GeneratePst());
            }
        }

        public bool AddSkill(short skillVNum)
        {
            var skillinfo = ServerManager.GetSkill(skillVNum: skillVNum);
            if (skillinfo == null)
            {
                Session.SendPacket(packet: GenerateSay(
                    message: Language.Instance.GetMessageFromKey(key: "SKILL_DOES_NOT_EXIST"), type: 11));
                return false;
            }

            if (skillinfo.SkillVNum < 200)
            {
                if (Skills.GetAllItems()
                        .Any(predicate: s => skillinfo.CastId == s.Skill.CastId && s.Skill.SkillVNum < 200 &&
                                             s.Skill.UpgradeSkill > skillinfo.UpgradeSkill))
                    // Character already has a better passive skill of the same type.
                    return false;
                foreach (var skill in Skills.GetAllItems()
                    .Where(predicate: s => skillinfo.CastId == s.Skill.CastId && s.Skill.SkillVNum < 200))
                    Skills.Remove(key: skill.SkillVNum);
            }
            else
            {
                if (Skills.ContainsKey(key: skillVNum))
                {
                    Session.SendPacket(packet: GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "SKILL_ALREADY_EXIST"), type: 11));
                    return false;
                }

                if (skillinfo.UpgradeSkill != 0)
                {
                    var oldupgrade = Skills.FirstOrDefault(predicate: s =>
                        s.Skill.UpgradeSkill == skillinfo.UpgradeSkill
                        && s.Skill.UpgradeType == skillinfo.UpgradeType && s.Skill.UpgradeSkill != 0);
                    if (oldupgrade != null) Skills.Remove(key: oldupgrade.SkillVNum);
                }
            }

            Skills[key: skillVNum] = new CharacterSkill
            {
                SkillVNum = skillVNum,
                CharacterId = CharacterId
            };

            Session.SendPackets(packets: GenerateQuicklist());
            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                message: Language.Instance.GetMessageFromKey(key: "SKILL_LEARNED"),
                type: 0));
            return true;
        }

        public void AddRelation(long characterId, CharacterRelationType Relation)
        {
            if (characterId == CharacterId)
                Session.SendPacket(packet: GenerateSay(
                    message: Language.Instance.GetMessageFromKey(key: "CANT_RELATION_YOURSELF"), type: 11));
            var addRelation = new CharacterRelationDto
            {
                CharacterId = CharacterId,
                RelatedCharacterId = characterId,
                RelationType = Relation
            };

            DaoFactory.CharacterRelationDao.InsertOrUpdate(characterRelation: ref addRelation);
            ServerManager.Instance.RelationRefresh(relationId: addRelation.CharacterRelationId);
            Session.SendPacket(packet: GenerateFinit());
            var target =
                ServerManager.Instance.Sessions.FirstOrDefault(predicate: s => s.Character?.CharacterId == characterId);
            target?.SendPacket(packet: target?.Character.GenerateFinit());
        }

        public void AddStaticBuff(StaticBuffDto staticBuff, bool isPermaBuff = false)
        {
            var bf = new Buff.Buff(id: staticBuff.CardId, level: Level, isPermaBuff: isPermaBuff)
            {
                Start = DateTime.Now,
                StaticBuff = true
            };
            var oldbuff = Buff[key: staticBuff.CardId];
            if (oldbuff != null)
            {
                oldbuff.Card.BCards.Where(predicate: s => BattleEntity.BCardDisposables[key: s.BCardId] != null)
                    .ToList()
                    .ForEach(action: b => BattleEntity.BCardDisposables[key: b.BCardId].Dispose());
                oldbuff.StaticVisualEffect?.Dispose();
            }

            if (staticBuff.RemainingTime <= 0)
            {
                bf.RemainingTime = (int)(bf.Card.Duration * 0.6);
                Buff[key: bf.Card.CardId] = bf;
            }
            else if (staticBuff.RemainingTime > 0)
            {
                bf.RemainingTime = staticBuff.CardId == 340
                    ? staticBuff.RemainingTime + (oldbuff == null
                        ? 0
                        : (int)(oldbuff.RemainingTime - (DateTime.Now - oldbuff.Start).TotalSeconds))
                    : staticBuff.RemainingTime;
                Buff[key: bf.Card.CardId] = bf;
            }
            else if (oldbuff != null)
            {
                Buff.Remove(key: bf.Card.CardId);
                var time =
                    (int)((oldbuff.Start.AddSeconds(value: oldbuff.Card.Duration * 6 / 10) - DateTime.Now)
                           .TotalSeconds / 10 *
                           6);
                bf.RemainingTime = bf.Card.Duration * 6 / 10 + (time > 0 ? time : 0);
                Buff[key: bf.Card.CardId] = bf;
            }
            else
            {
                bf.RemainingTime = bf.Card.Duration * 6 / 10;
                Buff[key: bf.Card.CardId] = bf;
            }

            bf.Card.BCards.ForEach(action: c => c.ApplyBCards(session: BattleEntity, sender: BattleEntity));
            if (BuffObservables.ContainsKey(key: bf.Card.CardId))
            {
                BuffObservables[key: bf.Card.CardId].Dispose();
                BuffObservables.Remove(key: bf.Card.CardId);
            }

            if (bf.RemainingTime > 0)
                BuffObservables[key: bf.Card.CardId] = Observable
                    .Timer(dueTime: TimeSpan.FromSeconds(value: bf.RemainingTime)).Subscribe(
                        onNext: o =>
                        {
                            RemoveBuff(cardId: bf.Card.CardId);
                            if (bf.Card.TimeoutBuff != 0 && ServerManager.RandomNumber() < bf.Card.TimeoutBuffChance)
                                AddBuff(indicator: new Buff.Buff(id: bf.Card.TimeoutBuff, level: Level),
                                    sender: BattleEntity);
                        });
            if (!_isStaticBuffListInitial) _isStaticBuffListInitial = true;

            Session.SendPacket(packet: $"vb {bf.Card.CardId} 1 {(bf.RemainingTime <= 0 ? -1 : bf.RemainingTime * 10)}");
            Session.SendPacket(
                packet: GenerateSay(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "UNDER_EFFECT"),
                        arg0: bf.Card.Name), type: 12));

            // Visual Effects (eff packet)
            if (bf.Card.CardId == 319)
                bf.StaticVisualEffect = Observable.Interval(period: TimeSpan.FromSeconds(value: 2)).Subscribe(
                    onNext: o =>
                    {
                        if (!Invisible) Session?.CurrentMapInstance?.Broadcast(packet: GenerateEff(effectid: 881));
                    });
        }

        public bool CanAddMate(Mate mate)
        {
            return mate.MateType == MateType.Pet
                ? MaxMateCount > Mates.Count(predicate: s => s.MateType == MateType.Pet)
                : MaxPartnerCount > Mates.Count(predicate: s => s.MateType == MateType.Partner);
        }

        public void ChangeChannel(string ip, int port, byte mode)
        {
            Session.SendPacket(packet: $"mz {ip} {port} {Slot}");
            Session.SendPacket(packet: $"it {mode}");
            Session.IsDisposing = true;
            CommunicationServiceClient.Instance.RegisterCrossServerAccountLogin(accountId: Session.Account.AccountId,
                sessionId: Session.SessionId);

            //explictly save data before disconnecting to prevent data loss
            Save();

            Session.Disconnect();
        }

        public void ChangeClass(ClassType characterClass, bool fromCommand)
        {
            if (!fromCommand)
            {
                JobLevel = 1;
                JobLevelXp = 0;
            }

            Session.SendPacket(packet: "npinfo 0");
            Session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());

            if (characterClass == (byte)ClassType.Adventurer)
            {
                HairStyle = (byte)HairStyle > 1 ? 0 : HairStyle;
                if (JobLevel > 20) JobLevel = 20;
            }

            LoadSpeed();
            Class = characterClass;
            Hp = (int)HPLoad();
            Mp = (int)MPLoad();
            Session.SendPacket(packet: GenerateTit());
            Session.SendPacket(packet: GenerateStat());
            Session.CurrentMapInstance?.Broadcast(client: Session, content: GenerateEq());
            Session.CurrentMapInstance?.Broadcast(
                packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player, callerId: CharacterId, effectId: 8),
                xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                message: Language.Instance.GetMessageFromKey(key: "CLASS_CHANGED"),
                type: 0));
            Session.CurrentMapInstance?.Broadcast(
                packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player, callerId: CharacterId,
                    effectId: 196),
                xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);

            ChangeFaction(faction: Session.Character.Family == null
                ? (FactionType)ServerManager.RandomNumber(min: 1, max: 2)
                : (FactionType)Session.Character.Family.FamilyFaction);

            Session.SendPacket(packet: GenerateCond());
            Session.SendPacket(packet: GenerateLev());
            Session.CurrentMapInstance?.Broadcast(client: Session, content: GenerateCMode());
            Session.CurrentMapInstance?.Broadcast(client: Session, content: GenerateIn(),
                receiver: ReceiverType.AllExceptMe);
            Session.CurrentMapInstance?.Broadcast(client: Session, content: GenerateGidx(),
                receiver: ReceiverType.AllExceptMe);
            Session.CurrentMapInstance?.Broadcast(
                packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player, callerId: CharacterId, effectId: 6),
                xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
            Session.CurrentMapInstance?.Broadcast(
                packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player, callerId: CharacterId,
                    effectId: 198),
                xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
            Session.Character.ResetSkills();

            foreach (var quicklists in DaoFactory.QuicklistEntryDao.LoadByCharacterId(characterId: CharacterId)
                .Where(predicate: quicklists => QuicklistEntries.Any(predicate: qle => qle.Id == quicklists.Id)))
                DaoFactory.QuicklistEntryDao.Delete(id: quicklists.Id);

            QuicklistEntries = new List<QuicklistEntryDto>
            {
                new QuicklistEntryDto
                {
                    CharacterId = CharacterId,
                    Q1 = 0,
                    Q2 = 9,
                    Type = 1,
                    Slot = 3,
                    Pos = 1
                }
            };

            Session.SendPackets(packets: GenerateQuicklist());

            if (ServerManager.Instance.Groups.Any(predicate: s =>
                s.IsMemberOfGroup(session: Session) && s.GroupType == GroupType.Group))
                Session.CurrentMapInstance?.Broadcast(client: Session, content: $"pidx 1 1.{CharacterId}",
                    receiver: ReceiverType.AllExceptMe);
        }

        public void CheckHuntQuest()
        {
            var quest = Quests?.FirstOrDefault(predicate: q =>
                q?.Quest?.QuestType == (int)QuestType.Hunt && q.Quest?.TargetMap == MapInstance?.Map?.MapId &&
                Math.Abs(value: PositionX - q.Quest?.TargetX ?? 0) < 2 &&
                Math.Abs(value: PositionY - q.Quest?.TargetY ?? 0) < 2);
            if (quest == null) return;
            var monsters = new List<MonsterToSummon>();
            if (!MapInstance.Monsters.Any(predicate: s =>
                s.MonsterVNum == (short)(quest.GetObjectiveByIndex(index: 1)?.Data ?? -1) &&
                Math.Abs(value: s.MapX - quest.Quest.TargetX ?? 0) < 4 &&
                Math.Abs(value: s.MapY - quest.Quest.TargetY ?? 0) < 4))
            {
                for (var a = 0; a < quest.GetObjectiveByIndex(index: 1)?.Objective / 2 + 1; a++)
                    monsters.Add(item: new MonsterToSummon(
                        vnum: (short)(quest.GetObjectiveByIndex(index: 1)?.Data ?? -1),
                        spawnCell: new MapCell
                        {
                            X = (short)(PositionX + ServerManager.RandomNumber(min: -2, max: 3)),
                            Y = (short)(PositionY + ServerManager.RandomNumber(min: -2, max: 3))
                        }, target: BattleEntity, move: true));
                EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: MapInstance,
                    eventActionType: EventActionType.Spawnmonsters,
                    param: monsters.AsEnumerable()));
            }
        }

        public void ChangeFaction(FactionType faction)
        {
            if (Faction == faction) return;

            if (Channel.ChannelId == 51)
            {
                var connection = CommunicationServiceClient.Instance.RetrieveOriginWorld(accountId: AccountId);
                if (string.IsNullOrWhiteSpace(value: connection)) return;
                MapId = 145;
                MapX = 51;
                MapY = 41;
                var port = Convert.ToInt32(value: connection.Split(':')[1]);
                ChangeChannel(ip: connection.Split(':')[0], port: port, mode: 3);
            }

            Faction = faction;
            Act4Kill = 0;
            Act4Dead = 0;
            Act4Points = 0;
            Session.SendPacket(packet: "scr 0 0 0 0 0 0");
            Session.SendPacket(packet: GenerateFaction());
            Session.SendPackets(packets: GenerateStatChar());

            if (faction != FactionType.None)
            {
                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: $"GET_PROTECTION_POWER_{(int)Faction}"),
                    type: 0));
                var effectId = 4799 + (int)faction;
                if (Family != null) effectId += 2;

                Session.SendPacket(packet: GenerateEff(effectid: effectId));
            }

            Session.SendPacket(packet: GenerateCond());
            Session.SendPacket(packet: GenerateLev());
        }

        public void ChangeSex()
        {
            Gender = Gender == GenderType.Female ? GenderType.Male : GenderType.Female;
            if (IsVehicled) Morph = Gender == GenderType.Female ? Morph + 1 : Morph - 1;
            Session.SendPacket(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "SEX_CHANGED"), type: 0));
            Session.SendPacket(packet: GenerateEq());
            Session.SendPacket(packet: GenerateGender());
            Session.CurrentMapInstance?.Broadcast(client: Session, content: GenerateIn(),
                receiver: ReceiverType.AllExceptMe);
            Session.CurrentMapInstance?.Broadcast(client: Session, content: GenerateGidx(),
                receiver: ReceiverType.AllExceptMe);
            Session.CurrentMapInstance?.Broadcast(packet: GenerateCMode());
            Session.CurrentMapInstance?.Broadcast(
                packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player, callerId: CharacterId,
                    effectId: 196),
                xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
        }

        public void CharacterLife()
        {
            if (Hp == 0 && LastHealth.AddSeconds(value: 2) <= DateTime.Now)
            {
                Mp = 0;
                Session.SendPacket(packet: GenerateStat());
                LastHealth = DateTime.Now;
            }
            else
            {
                #region Tart Hapendam's Martial Arts

                if (Level >= 1 && Level < 55)
                {
                    if (!HasBuff(cardId: 684))
                        AddBuff(indicator: new Buff.Buff(id: 684, level: Level), sender: BattleEntity);
                }
                else
                {
                    if (HasBuff(cardId: 684)) RemoveBuff(cardId: 684);
                }

                #endregion

                if (BubbleMessage != null && BubbleMessageEnd <= DateTime.Now) BubbleMessage = null;

                if (CurrentMinigame != 0 && LastEffect.AddSeconds(value: 3) <= DateTime.Now)
                {
                    Session.CurrentMapInstance?.Broadcast(
                        packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player, callerId: CharacterId,
                            effectId: CurrentMinigame));
                    LastEffect = DateTime.Now;
                }

                if (LastEffect.AddMilliseconds(value: 400) <= DateTime.Now && MessageCounter > 0) MessageCounter--;

                if (MapInstance != null
                    && HasBuff(type: CardType.FrozenDebuff, subtype: (byte)AdditionalTypes.FrozenDebuff.EternalIce)
                    && LastFreeze.AddSeconds(value: 1) <= DateTime.Now)
                {
                    LastFreeze = DateTime.Now;
                    MapInstance.Broadcast(packet: GenerateEff(effectid: 35));
                }

                if (MapInstance == Miniland && LastLoyalty.AddSeconds(value: 10) <= DateTime.Now)
                {
                    LastLoyalty = DateTime.Now;
                    Mates.ForEach(action: m =>
                    {
                        m.Loyalty += 1000;
                        if (m.Loyalty > 1000) m.Loyalty = 1000;
                    });
                    Session.SendPackets(packets: GenerateScP());
                    Session.SendPackets(packets: GenerateScN());
                }

                if (LastEffect.AddSeconds(value: 5) <= DateTime.Now)
                {
                    if (Session.CurrentMapInstance?.MapInstanceType == MapInstanceType.RaidInstance)
                        Session.SendPacket(packet: GenerateRaid(Type: 3));

                    var ring = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Ring, type: InventoryType.Wear);
                    var bracelet =
                        Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Bracelet, type: InventoryType.Wear);
                    var necklace =
                        Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Necklace, type: InventoryType.Wear);
                    CellonOptions.Clear();
                    if (ring != null) CellonOptions.AddRange(value: ring.CellonOptions);
                    if (bracelet != null) CellonOptions.AddRange(value: bracelet.CellonOptions);
                    if (necklace != null) CellonOptions.AddRange(value: necklace.CellonOptions);

                    if (!Invisible)
                    {
                        var amulet = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Amulet,
                            type: InventoryType.Wear);
                        if (amulet != null)
                        {
                            if (amulet.ItemVNum == 4503 || amulet.ItemVNum == 4504)
                                Session.CurrentMapInstance?.Broadcast(
                                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                        callerId: CharacterId,
                                        effectId: amulet.Item.EffectValue +
                                                  (Class == ClassType.Adventurer ? 0 : (byte)Class - 1)),
                                    xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
                            else
                                Session.CurrentMapInstance?.Broadcast(
                                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                        callerId: CharacterId,
                                        effectId: amulet.Item.EffectValue), xRangeCoordinate: PositionX,
                                    yRangeCoordinate: PositionY);
                        }

                        if (Group != null && (Group.GroupType == GroupType.Team ||
                                              Group.GroupType == GroupType.BigTeam ||
                                              Group.GroupType == GroupType.GiantTeam))
                            try
                            {
                                Session.CurrentMapInstance?.Broadcast(client: Session,
                                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                        callerId: CharacterId,
                                        effectId: 828 + (Group.IsLeader(session: Session) ? 1 : 0)),
                                    receiver: ReceiverType.AllExceptGroup);
                                Session.CurrentMapInstance?.Broadcast(client: Session,
                                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                        callerId: CharacterId,
                                        effectId: 830 + (Group.IsLeader(session: Session) ? 1 : 0)),
                                    receiver: ReceiverType.Group);
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex: ex);
                            }

                        Mates.Where(predicate: s => s.CanPickUp).ToList().ForEach(action: s =>
                            Session.CurrentMapInstance?.Broadcast(
                                packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                    callerId: s.MateTransportId, effectId: 3007)));
                        Mates.Where(predicate: s => s.IsTsProtected).ToList().ForEach(action: s =>
                            Session.CurrentMapInstance?.Broadcast(
                                packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                    callerId: s.MateTransportId, effectId: 825)));
                        Mates.Where(predicate: s => s.MateType == MateType.Pet && s.Loyalty <= 0).ToList().ForEach(
                            action: s =>
                                Session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                    callerId: s.MateTransportId, effectId: 5003)));
                    }

                    LastEffect = DateTime.Now;
                }

                foreach (var mate in Mates.Where(predicate: m => m.IsTeamMember))
                    if (mate.LastHealth.AddSeconds(value: mate.IsSitting ? 1.5 : 2) <= DateTime.Now)
                    {
                        mate.LastHealth = DateTime.Now;
                        if (mate.LastDefence.AddSeconds(value: 4) <= DateTime.Now &&
                            mate.LastSkillUse.AddSeconds(value: 2) <= DateTime.Now && mate.Hp > 0)
                        {
                            mate.Hp += mate.Hp + mate.HealthHpLoad() < mate.HpLoad()
                                ? mate.HealthHpLoad()
                                : mate.HpLoad() - mate.Hp;
                            mate.Mp += mate.Mp + mate.HealthMpLoad() < mate.MpLoad()
                                ? mate.HealthMpLoad()
                                : mate.MpLoad() - mate.Mp;
                        }

                        Session.SendPackets(packets: GeneratePst());
                    }

                if (LastHealth.AddSeconds(value: 2) <= DateTime.Now ||
                    IsSitting && LastHealth.AddSeconds(value: 1.5) <= DateTime.Now)
                {
                    LastHealth = DateTime.Now;

                    if (Session.HealthStop)
                    {
                        Session.HealthStop = false;
                        return;
                    }

                    if (LastDefence.AddSeconds(value: 4) <= DateTime.Now &&
                        LastSkillUse.AddSeconds(value: 2) <= DateTime.Now &&
                        Hp > 0)
                    {
                        var change = false;

                        if (Hp + HealthHPLoad() < HPLoad())
                        {
                            change = true;
                            Hp += HealthHPLoad();
                        }
                        else
                        {
                            change |= Hp != (int)HPLoad();
                            Hp = (int)HPLoad();
                        }

                        if (Mp + HealthMPLoad() < MPLoad())
                        {
                            Mp += HealthMPLoad();
                            change = true;
                        }
                        else
                        {
                            change |= Mp != (int)MPLoad();
                            Mp = (int)MPLoad();
                        }

                        if (change) Session.SendPacket(packet: GenerateStat());
                    }
                }

                if (Session.Character.LastQuestSummon.AddSeconds(value: 7) < DateTime.Now
                ) // Quest in which you make monster spawn
                {
                    Session.Character.CheckHuntQuest();
                    Session.Character.LastQuestSummon = DateTime.Now;
                }

                if (MeditationDictionary.Count != 0)
                {
                    if (MeditationDictionary.ContainsKey(key: 534) && MeditationDictionary[key: 534] < DateTime.Now)
                    {
                        Session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: CharacterId, effectId: 4344));
                        AddBuff(indicator: new Buff.Buff(id: 534, level: Level), sender: BattleEntity);
                        if (BuffObservables.ContainsKey(key: 533))
                        {
                            BuffObservables[key: 533].Dispose();
                            BuffObservables.Remove(key: 533);
                        }

                        MeditationDictionary.Remove(key: 534);
                    }
                    else if (MeditationDictionary.ContainsKey(key: 533) &&
                             MeditationDictionary[key: 533] < DateTime.Now)
                    {
                        Session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: CharacterId, effectId: 4343));
                        AddBuff(indicator: new Buff.Buff(id: 533, level: Level), sender: BattleEntity);
                        if (BuffObservables.ContainsKey(key: 532))
                        {
                            BuffObservables[key: 532].Dispose();
                            BuffObservables.Remove(key: 532);
                        }

                        MeditationDictionary.Remove(key: 533);
                    }
                    else if (MeditationDictionary.ContainsKey(key: 532) &&
                             MeditationDictionary[key: 532] < DateTime.Now)
                    {
                        Session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: CharacterId, effectId: 4343));
                        AddBuff(indicator: new Buff.Buff(id: 532, level: Level), sender: BattleEntity);
                        if (BuffObservables.ContainsKey(key: 534))
                        {
                            BuffObservables[key: 534].Dispose();
                            BuffObservables.Remove(key: 534);
                        }

                        MeditationDictionary.Remove(key: 532);
                    }
                }

                if (HasMagicSpellCombo)
                {
                    Session.SendPacket(packet: $"mslot {LastComboCastId} 0");
                }
                else if (SkillComboCount > 0 && LastSkillComboUse.AddSeconds(value: 5) < DateTime.Now)
                {
                    SkillComboCount = 0;
                    Session.SendPackets(packets: GenerateQuicklist());
                    Session.SendPacket(packet: "ms_c 1");
                }

                if (LastPermBuffRefresh.AddSeconds(value: 2) <= DateTime.Now)
                {
                    LastPermBuffRefresh = DateTime.Now;

                    foreach (var bcard in EquipmentBCards.Where(predicate: b =>
                        b.Type.Equals(obj: CardType.Buff) &&
                        new Buff.Buff(id: (short)b.CardId, level: Level).Card?.BuffType == BuffType.Good))
                        bcard.ApplyBCards(session: BattleEntity, sender: BattleEntity);

                    if (UseSp)
                    {
                        var specialist =
                            Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);
                        if (specialist == null) return;
                        switch (specialist.Design)
                        {
                            case 6:
                                if (!Buff.ContainsKey(key: 387))
                                    AddBuff(indicator: new Buff.Buff(id: 387, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;

                            case 7:
                                if (!Buff.ContainsKey(key: 395))
                                    AddBuff(indicator: new Buff.Buff(id: 395, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;

                            case 8:
                                if (!Buff.ContainsKey(key: 396))
                                    AddBuff(indicator: new Buff.Buff(id: 396, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;

                            case 9:
                                if (!Buff.ContainsKey(key: 397))
                                    AddBuff(indicator: new Buff.Buff(id: 397, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;

                            case 10:
                                if (!Buff.ContainsKey(key: 398))
                                    AddBuff(indicator: new Buff.Buff(id: 398, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;

                            case 11:
                                if (!Buff.ContainsKey(key: 410))
                                    AddBuff(indicator: new Buff.Buff(id: 410, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;

                            case 12:
                                if (!Buff.ContainsKey(key: 411))
                                    AddBuff(indicator: new Buff.Buff(id: 411, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;

                            case 13:
                                if (!Buff.ContainsKey(key: 444))
                                    AddBuff(indicator: new Buff.Buff(id: 444, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;

                            case 14:
                                if (!Buff.ContainsKey(key: 663))
                                    AddBuff(indicator: new Buff.Buff(id: 663, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;

                            case 15:
                                if (!Buff.ContainsKey(key: 686))
                                    AddBuff(indicator: new Buff.Buff(id: 686, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;
                            case 16: //Lightning Wings Buff
                                if (!Buff.ContainsKey(key: 755))
                                    AddBuff(indicator: new Buff.Buff(id: 755, level: Level), sender: BattleEntity,
                                        noMessage: true);
                                break;
                        }
                    }
                }

                if (UseSp)
                {
                    var specialist =
                        Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);
                    if (specialist == null) return;
                    if (LastSpGaugeRemove <=
                        new DateTime(year: 0001, month: 01, day: 01, hour: 00, minute: 00, second: 00))
                        LastSpGaugeRemove = DateTime.Now;
                    if (LastSkillUse.AddSeconds(value: 15) >= DateTime.Now &&
                        LastSpGaugeRemove.AddSeconds(value: 1) <= DateTime.Now)
                    {
                        byte spType = 0;

                        if (specialist.Item.Morph > 1 && specialist.Item.Morph < 8 ||
                            specialist.Item.Morph > 9 && specialist.Item.Morph < 16)
                            spType = 3;
                        else if (specialist.Item.Morph > 16 && specialist.Item.Morph < 29)
                            spType = 2;
                        else if (specialist.Item.Morph == 9) spType = 1;
                        if (SpPoint >= spType)
                        {
                            SpPoint -= spType;
                        }
                        else if (SpPoint < spType && SpPoint != 0)
                        {
                            spType -= (byte)SpPoint;
                            SpPoint = 0;
                            SpAdditionPoint -= spType;
                        }
                        else if (SpPoint == 0 && SpAdditionPoint >= spType)
                        {
                            SpAdditionPoint -= spType;
                        }
                        else if (SpPoint == 0 && SpAdditionPoint < spType)
                        {
                            SpAdditionPoint = 0;

                            var currentRunningSeconds =
                                (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(value: -50))
                                .TotalSeconds;

                            if (UseSp)
                            {
                                LastSp = currentRunningSeconds;
                                if (Session?.HasSession == true)
                                {
                                    if (IsVehicled) return;
                                    UseSp = false;
                                    LoadSpeed();
                                    Session.SendPacket(packet: GenerateCond());
                                    Session.SendPacket(packet: GenerateLev());
                                    SpCooldown = 30;
                                    if (SkillsSp != null)
                                        foreach (var ski in SkillsSp.Where(predicate: s => !s.CanBeUsed()))
                                        {
                                            var time = ski.Skill.Cooldown;
                                            var temp = (ski.LastUse - DateTime.Now).TotalMilliseconds + time * 100;
                                            temp /= 1000;
                                            SpCooldown = temp > SpCooldown ? (int)temp : SpCooldown;
                                        }

                                    Session.SendPacket(packet: GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "STAY_TIME"),
                                            arg0: SpCooldown),
                                        type: 11));
                                    Session.SendPacket(packet: $"sd {SpCooldown}");
                                    Session.CurrentMapInstance?.Broadcast(packet: GenerateCMode());
                                    Session.CurrentMapInstance?.Broadcast(
                                        packet: UserInterfaceHelper.GenerateGuri(type: 6, argument: 1,
                                            callerId: CharacterId), xRangeCoordinate: PositionX,
                                        yRangeCoordinate: PositionY);

                                    // ms_c
                                    Session.SendPacket(packet: GenerateSki());
                                    Session.SendPackets(packets: GenerateQuicklist());
                                    Session.SendPacket(packet: GenerateStat());
                                    Session.SendPackets(packets: GenerateStatChar());

                                    Logger.LogUserEvent(logEvent: "CHARACTER_SPECIALIST_RETURN",
                                        caller: Session.GenerateIdentity(),
                                        data: $"SpCooldown: {SpCooldown}");

                                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: SpCooldown * 1000))
                                        .Subscribe(onNext: o =>
                                        {
                                            Session.SendPacket(packet: GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(
                                                    key: "TRANSFORM_DISAPPEAR"), type: 11));
                                            Session.SendPacket(packet: "sd 0");
                                        });
                                }
                            }
                        }

                        Session.SendPacket(packet: GenerateSpPoint());
                        LastSpGaugeRemove = DateTime.Now;
                    }
                }
            }
        }

        public void CloseExchangeOrTrade()
        {
            if (InExchangeOrTrade)
            {
                var targetSessionId = ExchangeInfo?.TargetCharacterId;

                if (targetSessionId.HasValue && Session.HasCurrentMapInstance)
                {
                    var targetSession =
                        Session.CurrentMapInstance.GetSessionByCharacterId(characterId: targetSessionId.Value);

                    if (targetSession == null) return;

                    Session.SendPacket(packet: "exc_close 0");
                    targetSession.SendPacket(packet: "exc_close 0");
                    ExchangeInfo = null;
                    targetSession.Character.ExchangeInfo = null;
                }
            }
        }

        public void CloseShop()
        {
            if (HasShopOpened && Session.HasCurrentMapInstance)
            {
                var shop = Session.CurrentMapInstance.UserShops.FirstOrDefault(predicate: mapshop =>
                    mapshop.Value.OwnerId.Equals(obj: CharacterId));
                if (!shop.Equals(obj: default))
                {
                    Session.CurrentMapInstance.UserShops.Remove(key: shop.Key);

                    // declare that the shop cannot be closed
                    HasShopOpened = false;

                    Session.CurrentMapInstance?.Broadcast(packet: GenerateShopEnd());
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: GeneratePlayerFlag(pflag: 0),
                        receiver: ReceiverType.AllExceptMe);
                    IsSitting = false;
                    IsShopping = false; // close shop by character will always completely close the shop

                    LoadSpeed();
                    Session.SendPacket(packet: GenerateCond());
                    Session.CurrentMapInstance?.Broadcast(packet: GenerateRest());
                }
            }
        }

        public void Dance()
        {
            IsDancing = !IsDancing;
        }

        public Character DeepCopy()
        {
            return (Character)MemberwiseClone();
        }

        public void DeleteBlackList(long characterId)
        {
            var chara = CharacterRelations.Find(match: s => s.RelatedCharacterId == characterId);
            if (chara != null)
            {
                var id = chara.CharacterRelationId;
                DaoFactory.CharacterRelationDao.Delete(characterRelationId: id);
                ServerManager.Instance.RelationRefresh(relationId: id);
                Session.SendPacket(packet: GenerateBlinit());
            }
        }

        public void DeleteItem(InventoryType type, short slot)
        {
            if (Inventory != null)
            {
                Inventory.DeleteFromSlotAndType(slot: slot, type: type);
                Session.SendPacket(
                    packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: type, slot: slot));
            }
        }

        public void DeleteItemByItemInstanceId(Guid id)
        {
            if (Inventory != null)
            {
                var result = Inventory.DeleteById(id: id);
                Session.SendPacket(
                    packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: result.Item2,
                        slot: result.Item1));
            }
        }

        public void DeleteRelation(long characterId, CharacterRelationType relationType)
        {
            var chara = CharacterRelations.Find(match: s =>
                (s.RelatedCharacterId == characterId || s.CharacterId == characterId) &&
                s.RelationType == relationType);
            if (chara != null)
            {
                var id = chara.CharacterRelationId;
                var charac = DaoFactory.CharacterDao.LoadById(characterId: characterId);
                DaoFactory.CharacterRelationDao.Delete(characterRelationId: id);
                ServerManager.Instance.RelationRefresh(relationId: id);

                Session.SendPacket(packet: GenerateFinit());
                if (charac != null)
                {
                    var lst = ServerManager.Instance.CharacterRelations.Where(predicate: s =>
                        s.CharacterId == characterId || s.RelatedCharacterId == characterId).ToList();
                    var result = "finit";
                    foreach (var relation in lst.Where(predicate: c =>
                        c.RelationType == CharacterRelationType.Friend ||
                        c.RelationType == CharacterRelationType.Spouse))
                    {
                        var id2 = relation.RelatedCharacterId == charac.CharacterId
                            ? relation.CharacterId
                            : relation.RelatedCharacterId;
                        var isOnline =
                            CommunicationServiceClient.Instance.IsCharacterConnected(
                                worldGroup: ServerManager.Instance.ServerGroup,
                                characterId: id2);
                        result +=
                            $" {id2}|{(short)relation.RelationType}|{(isOnline ? 1 : 0)}|{DaoFactory.CharacterDao.LoadById(characterId: id2).Name}";
                    }

                    var sentChannelId = CommunicationServiceClient.Instance.SendMessageToCharacter(
                        message: new ScsCharacterMessage
                        {
                            DestinationCharacterId = charac.CharacterId,
                            SourceCharacterId = CharacterId,
                            SourceWorldId = ServerManager.Instance.WorldId,
                            Message = result,
                            Type = MessageType.PrivateChat
                        });
                }
            }
        }

        public void DeleteTimeout()
        {
            if (Inventory == null) return;

            foreach (var item in Inventory.GetAllItems())
                if ((item.IsBound || item.Item.ItemType == ItemType.Box) && item.ItemDeleteTime != null &&
                    item.ItemDeleteTime < DateTime.Now)
                {
                    Inventory.DeleteById(id: item.Id);

                    EquipmentBCards.RemoveAll(match: o => o.ItemVNum == item.ItemVNum);

                    if (item.Type == InventoryType.Wear)
                        Session.SendPacket(packet: GenerateEquipment());
                    else
                        Session.SendPacket(
                            packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: item.Type,
                                slot: item.Slot));
                    Session.SendPacket(
                        packet: GenerateSay(message: Language.Instance.GetMessageFromKey(key: "ITEM_TIMEOUT"),
                            type: 10));
                }
        }

        public void DisposeShopAndExchange()
        {
            CloseShop();
            CloseExchangeOrTrade();
        }

        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                if (OriginalFaction != -1) Faction = (FactionType)OriginalFaction;
                DisposeShopAndExchange();
                GroupSentRequestCharacterIds.Clear();
                FamilyInviteCharacters.Clear();
                FriendRequestCharacters.Clear();
                Life.Dispose();
                WalkDisposable?.Dispose();
                SealDisposable?.Dispose();
                MarryRequestCharacters.Clear();

                Mates.Where(predicate: s => s.IsTeamMember).ToList().ForEach(action: s =>
                {
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: s.GenerateOut(),
                        receiver: ReceiverType.AllExceptMe);
                    s.ReviveDisposable?.Dispose();
                });
                Session.CurrentMapInstance?.Broadcast(client: Session,
                    content: StaticPacketHelper.Out(type: UserType.Player, callerId: CharacterId),
                    receiver: ReceiverType.AllExceptMe);

                if (Hp < 1) Hp = 1;

                if (ServerManager.Instance.Groups.Any(predicate: s => s.IsMemberOfGroup(entityId: CharacterId)))
                    ServerManager.Instance.GroupLeave(session: Session);

                LeaveTalentArena(surrender: true);
                LeaveIceBreaker();
                BattleEntity.DisableBuffs(type: BuffType.All);
                BattleEntity.RemoveOwnedMonsters();
                BattleEntity.RemoveOwnedNpcs();
                RemoveTemporalMates();

                BattleEntity.ClearOwnFalcon();
                BattleEntity.ClearEnemyFalcon();
                BattleEntity.ClearSacrificeBuff();

                if (MapInstance != null)
                {
                    if (MapInstance.MapInstanceId == Family?.Act4RaidBossMap?.MapInstanceId
                        || MapInstance.MapInstanceId == Family?.Act4Raid?.MapInstanceId)
                    {
                        var x = (short)(39 + ServerManager.RandomNumber(min: -2, max: 3));
                        var y = (short)(42 + ServerManager.RandomNumber(min: -2, max: 3));
                        if (Faction == FactionType.Angel)
                        {
                            MapId = 130;
                            MapX = x;
                            MapY = y;
                        }
                        else if (Faction == FactionType.Demon)
                        {
                            MapId = 131;
                            MapX = x;
                            MapY = y;
                        }
                    }

                    if (MapInstance.MapInstanceType == MapInstanceType.TimeSpaceInstance ||
                        MapInstance.MapInstanceType == MapInstanceType.RaidInstance)
                    {
                        MapInstance.InstanceBag.DeadList.Add(item: CharacterId);
                        if (MapInstance.MapInstanceType == MapInstanceType.RaidInstance)
                            Group?.Sessions.ForEach(action: s =>
                            {
                                if (s != null)
                                {
                                    s.SendPacket(packet: s.Character.Group.GeneraterRaidmbf(session: s));
                                    s.SendPacket(packet: s.Character.Group.GenerateRdlst());
                                }
                            });
                    }

                    if (Miniland != null) ServerManager.RemoveMapInstance(mapId: Miniland.MapInstanceId);
                }
            }
        }

        public void SetSeal()
        {
            Hp = 0;
            Mp = 0;
            MapInstance.Broadcast(packet: GenerateRevive());
            MapInstance.Broadcast(client: Session, content: $"c_mode 1 {CharacterId} 1564 0 0 0");
            IsSeal = true;
            SealDisposable?.Dispose();
            SealDisposable = Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 30000)).Subscribe(onNext: o =>
            {
                var x = (short)(39 + ServerManager.RandomNumber(min: -2, max: 3));
                var y = (short)(42 + ServerManager.RandomNumber(min: -2, max: 3));

                IsSeal = false;

                Hp = (int)HPLoad();
                Mp = (int)MPLoad();
                if (Faction == FactionType.Angel)
                {
                    ServerManager.Instance.ChangeMap(id: CharacterId, mapId: 130, mapX: x, mapY: y);
                }
                else if (Faction == FactionType.Demon)
                {
                    ServerManager.Instance.ChangeMap(id: CharacterId, mapId: 131, mapX: x, mapY: y);
                }
                else
                {
                    MapId = 145;
                    MapX = 51;
                    MapY = 41;
                    var connection =
                        CommunicationServiceClient.Instance.RetrieveOriginWorld(accountId: Session.Account.AccountId);
                    if (string.IsNullOrWhiteSpace(value: connection)) return;

                    var port = Convert.ToInt32(value: connection.Split(':')[1]);
                    Session.Character.ChangeChannel(ip: connection.Split(':')[0], port: port, mode: 3);
                    return;
                }

                MapInstance?.Broadcast(client: Session, content: GenerateTp());
                MapInstance?.Broadcast(packet: GenerateRevive());
                Session.SendPacket(packet: GenerateStat());
            });
        }

        public static string GenerateAct()
        {
            return "act 6";
        }

        public string GenerateAt()
        {
            return
                $"at {CharacterId} {MapInstance.Map.GridMapId} {PositionX} {PositionY} {Direction} 0 {MapInstance?.InstanceMusic ?? 0} 2 -1";
        }

        public string GenerateBlinit()
        {
            var result = "blinit";

            foreach (var relation in CharacterRelations.Where(predicate: s =>
                s.CharacterId == CharacterId && s.RelationType == CharacterRelationType.Blocked))
                result +=
                    $" {relation.RelatedCharacterId}|{DaoFactory.CharacterDao.LoadById(characterId: relation.RelatedCharacterId)?.Name}";

            return result;
        }

        public string GenerateCInfo()
        {
            return
                $"c_info {(Authority > AuthorityType.User && !Undercover ? Authority == AuthorityType.Gs ? $"[{Authority}]" + Name : Name : Authority == AuthorityType.Em ? $"[{Authority}]" + Name : Name)} - -1 {(Family != null && FamilyCharacter != null && !Undercover ? $"{Family.FamilyId} {Family.Name}({Language.Instance.GetMessageFromKey(key: FamilyCharacter.Authority.ToString().ToUpper())})" : "-1 -")} {CharacterId} {(Invisible && Authority >= AuthorityType.Mod ? 6 : 0)} {(byte)Gender} {(byte)HairStyle} {(byte)HairColor} {(byte)Class} {(GetDignityIco() == 1 ? GetReputationIco() : -GetDignityIco())} {Compliment} {(UseSp || IsVehicled ? Morph : 0)} {(Invisible ? 1 : 0)} {Family?.FamilyLevel ?? 0} {(UseSp ? MorphUpgrade : 0)} {ArenaWinner}"; // AUTHORITY
        }

        public string GenerateCMap()
        {
            return
                $"c_map 0 {MapInstance.Map.MapId} {(MapInstance.MapInstanceType != MapInstanceType.BaseMapInstance ? 1 : 0)}";
        }

        public bool IsLaurenaMorph()
        {
            return Morph == 1000099 /* Hamster */ || Morph == 1000156 /* Bushtail */;
        }

        public string GenerateCMode()
        {
            var item = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Wings, type: InventoryType.Wear);

            return !IsSeal
                ? $"c_mode 1 {CharacterId} {(UseSp || IsVehicled || IsMorphed ? Morph : 0)} {(!IsLaurenaMorph() && UseSp ? MorphUpgrade : 0)} {(!IsLaurenaMorph() && UseSp ? MorphUpgrade2 : 0)} {ArenaWinner} {Size} {item?.Item.Morph ?? 0}"
                : "";
        }

        public string GenerateCond()
        {
            return $"cond 1 {CharacterId} {(!IsLaurenaMorph() && !CanAttack() ? 1 : 0)} {(!CanMove() ? 1 : 0)} {Speed}";
        }

        public bool CanAttack()
        {
            return !NoAttack && !HasBuff(type: CardType.SpecialAttack,
                       subtype: (byte)AdditionalTypes.SpecialAttack.NoAttack) &&
                   !HasBuff(type: CardType.FrozenDebuff, subtype: (byte)AdditionalTypes.FrozenDebuff.EternalIce);
        }

        public bool CanMove()
        {
            return !NoMove && !HasBuff(type: CardType.Move, subtype: (byte)AdditionalTypes.Move.MovementImpossible) &&
                   !HasBuff(type: CardType.FrozenDebuff, subtype: (byte)AdditionalTypes.FrozenDebuff.EternalIce);
        }

        public string GenerateDG()
        {
            byte raidType = 0;

            if (ServerManager.Instance.Act4RaidStart.AddMinutes(value: 60) < DateTime.Now)
                ServerManager.Instance.Act4RaidStart = DateTime.Now;

            var seconds = (ServerManager.Instance.Act4RaidStart.AddMinutes(value: 60) - DateTime.Now).TotalSeconds;

            switch (Family?.Act4Raid?.MapInstanceType)
            {
                case MapInstanceType.Act4Morcos:
                    raidType = 1;
                    break;

                case MapInstanceType.Act4Hatus:
                    raidType = 2;
                    break;

                case MapInstanceType.Act4Calvina:
                    raidType = 3;
                    break;

                case MapInstanceType.Act4Berios:
                    raidType = 4;
                    break;
            }

            return $"dg {raidType} {(seconds > 1800 ? 1 : 2)} {(int)seconds} 0";
        }

        public void GenerateDignity(NpcMonster monsterinfo)
        {
            if (Level < monsterinfo.Level && Dignity < 100 && Level > 20)
            {
                Dignity += (float)0.5;

                if (Dignity == (int)Dignity)
                {
                    Session.SendPacket(packet: GenerateFd());
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: GenerateIn(InEffect: 1),
                        receiver: ReceiverType.AllExceptMe);
                    Session.CurrentMapInstance?.Broadcast(client: Session, content: GenerateGidx(),
                        receiver: ReceiverType.AllExceptMe);
                    Session.SendPacket(
                        packet: GenerateSay(message: Language.Instance.GetMessageFromKey(key: "RESTORE_DIGNITY"),
                            type: 11));
                }
            }
        }

        public void GetDignity(int amount)
        {
            Dignity += amount;

            if (Dignity > 100) Dignity = 100;

            Session.SendPacket(packet: GenerateFd());
            Session.CurrentMapInstance?.Broadcast(client: Session, content: GenerateIn(InEffect: 1),
                receiver: ReceiverType.AllExceptMe);
            Session.CurrentMapInstance?.Broadcast(client: Session, content: GenerateGidx(),
                receiver: ReceiverType.AllExceptMe);
            Session.SendPacket(packet: GenerateSay(
                message: $"{Language.Instance.GetMessageFromKey(key: "RESTORE_DIGNITY")} (+{amount})",
                type: 11));
        }

        public string GenerateDir()
        {
            return $"dir 1 {CharacterId} {Direction}";
        }

        public string GenerateEq()
        {
            int color = (byte)HairColor;

            var head = Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Hat, type: InventoryType.Wear);

            if (head?.Item.IsColored == true) color = head.Design;

            return
                $"eq {CharacterId} {(Invisible && Authority >= AuthorityType.Mod ? 6 : Undercover ? (byte)AuthorityType.User : Authority < AuthorityType.User ? (byte)AuthorityType.User : Authority >= AuthorityType.Mod ? 2 : (byte)Authority)} {(byte)Gender} {(byte)HairStyle} {color} {(byte)Class} {GenerateEqListForPacket()} {(!InvisibleGm ? GenerateEqRareUpgradeForPacket() : null)}";
            //return $"eq {CharacterId} {(Invisible ? 6 : 0)} {(byte)Gender} {(byte)HairStyle} {color} {(byte)Class} {GenerateEqListForPacket()} {(!InvisibleGm ? GenerateEqRareUpgradeForPacket() : null)}";
        }

        public EffectPacket GenerateEff(int effectid)
        {
            return new EffectPacket
            {
                EffectType = UserType.Player,
                CallerId = CharacterId,
                EffectId = effectid
            };
        }

        public string GenerateEqListForPacket()
        {
            var invarray = new string[17];

            if (Inventory != null)
                for (short i = 0; i < invarray.Length; i++)
                {
                    var item = Inventory.LoadBySlotAndType(slot: i, type: InventoryType.Wear);

                    if (item != null)
                        invarray[i] = item.ItemVNum.ToString();
                    else
                        invarray[i] = "-1";
                }

            return
                $"{invarray[(byte)EquipmentType.Hat]}.{invarray[(byte)EquipmentType.Armor]}.{invarray[(byte)EquipmentType.MainWeapon]}.{invarray[(byte)EquipmentType.SecondaryWeapon]}.{invarray[(byte)EquipmentType.Mask]}.{invarray[(byte)EquipmentType.Fairy]}.{invarray[(byte)EquipmentType.CostumeSuit]}.{invarray[(byte)EquipmentType.CostumeHat]}.{invarray[(byte)EquipmentType.WeaponSkin]}.{invarray[(byte)EquipmentType.Wings]}";
        }

        public string GenerateEqRareUpgradeForPacket()
        {
            sbyte weaponRare = 0;
            byte weaponUpgrade = 0;
            sbyte armorRare = 0;
            byte armorUpgrade = 0;

            if (Inventory != null)
                for (short i = 0; i < 16; i++)
                {
                    var wearable = Inventory.LoadBySlotAndType(slot: i, type: InventoryType.Wear);

                    if (wearable != null)
                        switch (wearable.Item.EquipmentSlot)
                        {
                            case EquipmentType.MainWeapon:
                                weaponRare = wearable.Rare;
                                weaponUpgrade = wearable.Upgrade;
                                break;

                            case EquipmentType.Armor:
                                armorRare = wearable.Rare;
                                armorUpgrade = wearable.Upgrade;
                                break;
                        }
                }

            return $"{weaponUpgrade}{weaponRare} {armorUpgrade}{armorRare}";
        }

        public string GenerateEquipment()
        {
            var eqlist = "";

            EquipmentBCards.Lock(action: () =>
            {
                EquipmentBCards.Clear();
                ShellEffectArmor.Clear();
                ShellEffectMain.Clear();
                ShellEffectSecondary.Clear();

                if (Inventory != null)
                    for (short i = 0; i < 17; i++)
                    {
                        var item = Inventory.LoadBySlotAndType(slot: i, type: InventoryType.Wear);
                        if (item != null)
                        {
                            if (item.Item.EquipmentSlot != EquipmentType.Sp)
                            {
                                EquipmentBCards.AddRange(collection: item.Item.BCards);
                                switch (item.Item.ItemType)
                                {
                                    case ItemType.Armor:
                                        foreach (var dto in item.ShellEffects) ShellEffectArmor.Add(item: dto);
                                        break;
                                    case ItemType.Weapon:
                                        switch (item.Item.EquipmentSlot)
                                        {
                                            case EquipmentType.MainWeapon:
                                                foreach (var dto in item.ShellEffects) ShellEffectMain.Add(item: dto);
                                                break;

                                            case EquipmentType.SecondaryWeapon:
                                                foreach (var dto in item.ShellEffects)
                                                    ShellEffectSecondary.Add(item: dto);
                                                break;
                                        }

                                        break;
                                }
                            }

                            eqlist +=
                                $" {i}.{item.Item.VNum}.{item.Rare}.{(item.Item.IsColored ? item.Design : item.Upgrade)}.0";
                        }
                    }
            });

            return $"equip {GenerateEqRareUpgradeForPacket()}{eqlist}";
        }

        public string GenerateExts()
        {
            return
                $"exts 0 {48 + (HaveBackpack() ? 1 : 0) * 12} {48 + (HaveBackpack() ? 1 : 0) * 12} {48 + (HaveBackpack() ? 1 : 0) * 12}";
        }

        public string GenerateFaction()
        {
            return $"fs {(byte)Faction}";
        }

        public string GenerateFamilyMember()
        {
            var str = "gmbr 0";
            try
            {
                if (Family?.FamilyCharacters != null)
                    foreach (var TargetCharacter in Family?.FamilyCharacters)
                    {
                        var isOnline =
                            CommunicationServiceClient.Instance.IsCharacterConnected(
                                worldGroup: ServerManager.Instance.ServerGroup,
                                characterId: TargetCharacter.CharacterId);
                        str +=
                            $" {TargetCharacter.Character.CharacterId}|{Family.FamilyId}|{TargetCharacter.Character.Name}|{TargetCharacter.Character.Level}|{(byte)TargetCharacter.Character.Class}|{(byte)TargetCharacter.Authority}|{(byte)TargetCharacter.Rank}|{(isOnline ? 1 : 0)}|{TargetCharacter.Character.HeroLevel}";
                    }
            }
            catch (Exception ex)
            {
                Logger.Error(ex: ex);
            }

            return str;
        }

        public string GenerateFamilyMemberExp()
        {
            var str = "gexp";
            try
            {
                if (Family?.FamilyCharacters != null)
                    foreach (var TargetCharacter in Family?.FamilyCharacters)
                        str += $" {TargetCharacter.CharacterId}|{TargetCharacter.Experience}";
            }
            catch (Exception ex)
            {
                Logger.Error(ex: ex);
            }

            return str;
        }

        public string GenerateFamilyMemberMessage()
        {
            var str = "gmsg";
            try
            {
                if (Family?.FamilyCharacters != null)
                    foreach (var TargetCharacter in Family?.FamilyCharacters)
                        str += $" {TargetCharacter.CharacterId}|{TargetCharacter.DailyMessage}";
            }
            catch (Exception ex)
            {
                Logger.Error(ex: ex);
            }

            return str;
        }

        public List<string> GenerateFamilyWarehouseHist()
        {
            if (Family != null)
            {
                var packetList = new List<string>();
                var packet = "";
                var i = 0;
                var amount = -1;
                var warehouseLogs = Family.FamilyLogs
                    .Where(predicate: s => s.FamilyLogType == FamilyLogType.WareHouseAdded ||
                                           s.FamilyLogType == FamilyLogType.WareHouseRemoved)
                    .OrderByDescending(keySelector: s => s.Timestamp)
                    .Take(count: 100).ToList();
                foreach (var log in warehouseLogs)
                {
                    packet +=
                        $" {(log.FamilyLogType == FamilyLogType.WareHouseAdded ? 0 : 1)}|{log.FamilyLogData}|{(int)(DateTime.Now - log.Timestamp).TotalHours}";
                    i++;
                    if (i == 50)
                    {
                        i = 0;
                        packetList.Add(item: $"fslog_stc {amount}{packet}");
                        amount++;
                    }
                    else if (i == warehouseLogs.Count)
                    {
                        packetList.Add(item: $"fslog_stc {amount}{packet}");
                    }
                }

                return packetList;
            }

            return new List<string>();
        }

        public bool GenerateFamilyXp(int FXP, short InstanceId = -1)
        {
            if (!Session.Account.PenaltyLogs.Any(predicate: s =>
                    s.Penalty == PenaltyType.BlockFExp && s.DateEnd > DateTime.Now) &&
                Family != null && FamilyCharacter != null
                && (InstanceId == -1 || Session.Character.GeneralLogs.CountLinq(predicate: s =>
                    s.LogType == "InstanceEntry" && short.Parse(s: s.LogData) == InstanceId &&
                    s.Timestamp.Date == DateTime.Today) == 0))
            {
                var famchar = FamilyCharacter;
                FamilyDto fam = Family;
                fam.FamilyExperience += FXP;
                famchar.Experience += FXP;
                if (CharacterHelper.LoadFamilyXPData(familyLevel: Family.FamilyLevel) <= fam.FamilyExperience)
                {
                    fam.FamilyExperience -= CharacterHelper.LoadFamilyXPData(familyLevel: Family.FamilyLevel);
                    fam.FamilyLevel++;
                    Family.InsertFamilyLog(logtype: FamilyLogType.FamilyLevelUp, level: fam.FamilyLevel);
                    CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                    {
                        DestinationCharacterId = Family.FamilyId,
                        SourceCharacterId = CharacterId,
                        SourceWorldId = ServerManager.Instance.WorldId,
                        Message = UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "FAMILY_UP"), type: 0),
                        Type = MessageType.Family
                    });
                }

                DaoFactory.FamilyCharacterDao.InsertOrUpdate(character: ref famchar);
                DaoFactory.FamilyDao.InsertOrUpdate(family: ref fam);
                ServerManager.Instance.FamilyRefresh(familyId: Family.FamilyId);
                CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                {
                    DestinationCharacterId = Family.FamilyId,
                    SourceCharacterId = CharacterId,
                    SourceWorldId = ServerManager.Instance.WorldId,
                    Message = "fhis_stc",
                    Type = MessageType.Family
                });
                if (FXP > 1000)
                {
                    var value = FXP - FXP % 1000;
                    Session.Character.Family.InsertFamilyLog(logtype: FamilyLogType.FamilyXp,
                        characterName: Session.Character.Name,
                        experience: value);
                }
                else if (famchar.Experience % 1000 == 0)
                {
                    Session.Character.Family.InsertFamilyLog(logtype: FamilyLogType.FamilyXp,
                        characterName: Session.Character.Name,
                        experience: 1000);
                }

                return true;
            }

            return false;
        }

        public string GenerateFc()
        {
            return
                $"fc {(byte)Faction} {ServerManager.Instance.Act4AngelStat.MinutesUntilReset} {ServerManager.Instance.Act4AngelStat.Percentage / 100} {ServerManager.Instance.Act4AngelStat.Mode}" +
                $" {ServerManager.Instance.Act4AngelStat.CurrentTime} {ServerManager.Instance.Act4AngelStat.TotalTime} {Convert.ToByte(value: ServerManager.Instance.Act4AngelStat.IsMorcos)}" +
                $" {Convert.ToByte(value: ServerManager.Instance.Act4AngelStat.IsHatus)} {Convert.ToByte(value: ServerManager.Instance.Act4AngelStat.IsCalvina)} {Convert.ToByte(value: ServerManager.Instance.Act4AngelStat.IsBerios)}" +
                $" 0 {ServerManager.Instance.Act4DemonStat.Percentage / 100} {ServerManager.Instance.Act4DemonStat.Mode} {ServerManager.Instance.Act4DemonStat.CurrentTime} {ServerManager.Instance.Act4DemonStat.TotalTime}" +
                $" {Convert.ToByte(value: ServerManager.Instance.Act4DemonStat.IsMorcos)} {Convert.ToByte(value: ServerManager.Instance.Act4DemonStat.IsHatus)} {Convert.ToByte(value: ServerManager.Instance.Act4DemonStat.IsCalvina)} " +
                $"{Convert.ToByte(value: ServerManager.Instance.Act4DemonStat.IsBerios)} 0";

            //return $"fc {Faction} 0 69 0 0 0 1 1 1 1 0 34 0 0 0 1 1 1 1 0";
        }

        public string GenerateFd()
        {
            return $"fd {Reputation} {GetReputationIco()} {(int)Dignity} {Math.Abs(value: GetDignityIco())}";
        }

        public string GenerateFinfo(long? relatedCharacterLoggedId, bool isConnected)
        {
            var result = "finfo";
            foreach (var relation in CharacterRelations.Where(predicate: c =>
                c.RelationType == CharacterRelationType.Friend || c.RelationType == CharacterRelationType.Spouse))
                if (relatedCharacterLoggedId.HasValue &&
                    (relatedCharacterLoggedId.Value == relation.RelatedCharacterId ||
                     relatedCharacterLoggedId.Value == relation.CharacterId))
                    result += $" {relatedCharacterLoggedId}.{(isConnected ? 1 : 0)}";
            return result;
        }

        public string GenerateFinit()
        {
            var result = "finit";
            foreach (var relation in CharacterRelations.ToList().Where(predicate: c =>
                c.RelationType == CharacterRelationType.Friend || c.RelationType == CharacterRelationType.Spouse))
            {
                var id = relation.RelatedCharacterId == CharacterId
                    ? relation.CharacterId
                    : relation.RelatedCharacterId;
                if (DaoFactory.CharacterDao.LoadById(characterId: id) is CharacterDto character)
                {
                    var isOnline =
                        CommunicationServiceClient.Instance.IsCharacterConnected(
                            worldGroup: ServerManager.Instance.ServerGroup,
                            characterId: id);
                    result += $" {id}|{(short)relation.RelationType}|{(isOnline ? 1 : 0)}|{character.Name}";
                }
            }

            return result;
        }

        public string GenerateFStashAll()
        {
            var stash = $"f_stash_all {Family.WarehouseSize}";
            foreach (var item in Family.Warehouse.GetAllItems()) stash += $" {item.GenerateStashPacket()}";
            return stash;
        }

        public string GenerateGender()
        {
            return $"p_sex {(byte)Gender}";
        }

        public string GenerateGExp()
        {
            var str = "gexp";
            foreach (var familyCharacter in Family.FamilyCharacters)
                str += $" {familyCharacter.CharacterId}|{familyCharacter.Experience}";
            return str;
        }

        public string GenerateGidx()
        {
            return Family != null && FamilyCharacter != null
                ? $"gidx 1 {CharacterId} {Family.FamilyId} {Family.Name}({Language.Instance.GetMessageFromKey(key: FamilyCharacter.Authority.ToString().ToUpper())}) {Family.FamilyLevel} 0|0|0"
                : $"gidx 1 {CharacterId} -1 - 0 0|0|0";
        }

        public string GenerateGInfo()
        {
            if (Family != null)
                try
                {
                    var familyCharacter = Family.FamilyCharacters.Find(match: s => s.Authority == FamilyAuthority.Head);
                    if (familyCharacter != null)
                        return
                            $"ginfo {Family.Name} {familyCharacter.Character.Name} {(byte)Family.FamilyHeadGender} {Family.FamilyLevel} {Family.FamilyExperience} {CharacterHelper.LoadFamilyXPData(familyLevel: Family.FamilyLevel)} {Family.FamilyCharacters.Count} {Family.MaxSize} {(byte)FamilyCharacter.Authority} {(Family.ManagerCanInvite ? 1 : 0)} {(Family.ManagerCanNotice ? 1 : 0)} {(Family.ManagerCanShout ? 1 : 0)} {(Family.ManagerCanGetHistory ? 1 : 0)} {(byte)Family.ManagerAuthorityType} {(Family.MemberCanGetHistory ? 1 : 0)} {(byte)Family.MemberAuthorityType} {Family.FamilyMessage.Replace(oldChar: ' ', newChar: '^')}";
                }
                catch (Exception)
                {
                    return "";
                }

            return "";
        }

        public string GenerateGold()
        {
            return $"gold {Gold} 0";
        }

        public string GenerateIcon(int type, int value, short itemVNum)
        {
            return $"icon {type} {CharacterId} {value} {itemVNum}";
        }

        public string GenerateIdentity()
        {
            return $"Character: {Name}";
        }

        public string GenerateIn(bool foe = false, AuthorityType receiverAuthority = AuthorityType.User,
            int InEffect = 0)
        {
            var name = Name;

            if (receiverAuthority >= AuthorityType.Mod)
            {
                foe = false;
                name = $"[{Faction}]{name}";
            }

            if (foe && Authority < AuthorityType.Mod) name = "!§$%&/()=?*+~#";

            var faction = 0;

            if (ServerManager.Instance.ChannelId == 51) faction = (byte)Faction + 2;

            var color = HairStyle == HairStyleType.Hair8 ? 0 : (byte)HairColor;

            ItemInstance fairy = null;

            if (Inventory != null)
            {
                var headWearable =
                    Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Hat, type: InventoryType.Wear);

                if (headWearable?.Item.IsColored == true) color = headWearable.Design;

                fairy = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Fairy, type: InventoryType.Wear);
            }

            return
                $"in 1 {(Authority > AuthorityType.User && !Undercover ? Authority == AuthorityType.Gs ? $"[{Authority}]" + name : name : name)} - {CharacterId} {PositionX} {PositionY} {Direction} {(Undercover ? (byte)AuthorityType.User : Authority >= AuthorityType.Mod ? 2 : (byte)Authority)} {(byte)Gender} {(byte)HairStyle} {color} {(byte)Class} {GenerateEqListForPacket()} {Math.Ceiling(a: Hp / HPLoad() * 100)} {Math.Ceiling(a: Mp / MPLoad() * 100)} {(IsSitting ? 1 : 0)} {(Group?.GroupType == GroupType.Group ? Group?.GroupId ?? -1 : -1)} {(fairy != null && !Undercover ? 4 : 0)} {fairy?.Item.Element ?? 0} 0 {fairy?.Item.Morph ?? 0} {InEffect} {(UseSp || IsVehicled || IsMorphed ? Morph : 0)} {GenerateEqRareUpgradeForPacket()} {(!Undercover ? foe ? -1 : Family?.FamilyId ?? -1 : -1)} {(!Undercover ? foe ? name : Family?.Name ?? "-" : "-")} {(GetDignityIco() == 1 ? GetReputationIco() : -GetDignityIco())} {(Invisible ? 1 : 0)} {(UseSp ? MorphUpgrade : 0)} {faction} {(UseSp ? MorphUpgrade2 : 0)} {Level} {Family?.FamilyLevel ?? 0} 0|0|0 {ArenaWinner} {Compliment} {Size} {HeroLevel}";
        }

        public string GenerateInvisible()
        {
            return $"cl {CharacterId} {(Invisible ? 1 : 0)} {(InvisibleGm ? 1 : 0)}";
        }

        //AUTOLOOT OFF
        public void GenerateKillBonus(MapMonster monsterToAttack, BattleEntity Killer)
        {
            void _handleGoldDrop(DropDto drop, long maxGold, long? dropOwner, short posX, short posY)
            {
                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 500)).Subscribe(onNext: o =>
                {
                    if (Session.HasCurrentMapInstance)
                    {
                        if (CharacterId == dropOwner &&
                            StaticBonusList.Any(predicate: s => s.StaticBonusType == StaticBonusType.AutoLoot))
                        {
                            var multiplier = 1 + Session.Character.GetBuff(type: CardType.Item,
                                subtype: (byte)AdditionalTypes.Item.IncreaseEarnedGold)[0] / 100D;
                            multiplier +=
                                (Session.Character.ShellEffectMain.FirstOrDefault(predicate: s =>
                                    s.Effect == (byte)ShellWeaponEffectType.GainMoreGold)?.Value ?? 0) / 100D;

                            Gold += (int)(drop.Amount * multiplier);

                            if (Gold > maxGold)
                            {
                                Gold = maxGold;
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "MAX_GOLD"),
                                        type: 0));
                            }

                            Session.SendPacket(packet: GenerateSay(
                                message:
                                $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {ServerManager.GetItem(vnum: drop.ItemVNum).Name} x {drop.Amount}{(multiplier > 1 ? $" + {(int)(drop.Amount * multiplier) - drop.Amount}" : "")}",
                                type: 12));
                            Session.SendPacket(packet: GenerateGold());
                            Session.CurrentMapInstance.DropItemByMonster(owner: dropOwner, drop: drop,
                                mapX: monsterToAttack.MapX,
                                mapY: monsterToAttack.MapY);
                        }
                        else
                        {
                            Session.CurrentMapInstance.DropItemByMonster(owner: dropOwner, drop: drop,
                                mapX: monsterToAttack.MapX,
                                mapY: monsterToAttack.MapY);
                        }
                    }
                });
            }

            void _handleItemDrop(DropDto drop, long? owner, short posX, short posY)
            {
                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 500)).Subscribe(onNext: o =>
                {
                    if (Session.HasCurrentMapInstance)
                    {
                        if (CharacterId == owner &&
                            StaticBonusList.Any(predicate: s => s.StaticBonusType == StaticBonusType.AutoLoot))
                            Session.CurrentMapInstance.DropItemByMonster(owner: owner, drop: drop,
                                mapX: monsterToAttack.MapX,
                                mapY: monsterToAttack.MapY,
                                isQuest: Quests.Any(predicate: q =>
                                    (q.Quest.QuestType == (int)QuestType.Collect4 ||
                                     q.Quest.QuestType == (int)QuestType.Collect2 ||
                                     q.Quest?.QuestType == (int)QuestType.Collect1 &&
                                     MapInstance.Map.MapTypes.Any(predicate: s =>
                                         s.MapTypeId != (short)MapTypeEnum.Act4)) &&
                                    q.Quest.QuestObjectives.Any(predicate: qst => qst.Data == drop.ItemVNum)));
                        else
                            Session.CurrentMapInstance.DropItemByMonster(owner: owner, drop: drop,
                                mapX: monsterToAttack.MapX,
                                mapY: monsterToAttack.MapY,
                                isQuest: Quests.Any(predicate: q =>
                                    (q.Quest.QuestType == (int)QuestType.Collect4 ||
                                     q.Quest.QuestType == (int)QuestType.Collect2 ||
                                     q.Quest?.QuestType == (int)QuestType.Collect1 &&
                                     MapInstance.Map.MapTypes.Any(predicate: s =>
                                         s.MapTypeId != (short)MapTypeEnum.Act4)) &&
                                    q.Quest.QuestObjectives.Any(predicate: qst => qst.Data == drop.ItemVNum)));
                    }
                });
            }

            /*public void GenerateKillBonus(MapMonster monsterToAttack, BattleEntity Killer) //AUTOLOOT ON
{
       void _handleGoldDrop(DropDTO drop, long maxGold, long? dropOwner, short posX, short posY)
       {
           Observable.Timer(TimeSpan.FromMilliseconds(500)).Subscribe(o =>
           {
               if (Session.HasCurrentMapInstance)
               {
                   if (CharacterId == dropOwner && StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.AutoLoot))
                   {
                       double multiplier = 1 + (Session.Character.GetBuff(CardType.Item, (byte)AdditionalTypes.Item.IncreaseEarnedGold)[0] / 100D);
                       multiplier += (Session.Character.ShellEffectMain.FirstOrDefault(s => s.Effect == (byte)ShellWeaponEffectType.GainMoreGold)?.Value ?? 0) / 100D;

                       Gold += (int)(drop.Amount * multiplier);

                       if (Gold > maxGold)
                       {
                           Gold = maxGold;
                           Session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("MAX_GOLD"), 0));
                       }

                       Session.SendPacket(GenerateSay($"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {ServerManager.GetItem(drop.ItemVNum).Name} x {drop.Amount}{(multiplier > 1 ? $" + {(int)(drop.Amount * multiplier) - drop.Amount}" : "")}", 12));
                       Session.SendPacket(GenerateGold());
                   }
                   else
                   {
                       Session.CurrentMapInstance.DropItemByMonster(dropOwner, drop, monsterToAttack.MapX, monsterToAttack.MapY);
                   }
               }
           });
       }

       void _handleItemDrop(DropDTO drop, long? owner, short posX, short posY)
       {
           Observable.Timer(TimeSpan.FromMilliseconds(500)).Subscribe(o =>
           {
               if (Session.HasCurrentMapInstance)
               {
                   if (CharacterId == owner && StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.AutoLoot))
                   {
                       GiftAdd(drop.ItemVNum, (byte)drop.Amount);
                   }
                   else
                   {
                       GiftAdd(drop.ItemVNum, (byte)drop.Amount);
                       Session.CurrentMapInstance.DropItemByMonster(owner, drop, monsterToAttack.MapX, monsterToAttack.MapY, Quests.Any(q => (q.Quest.QuestType == (int)QuestType.Collect4 || q.Quest.QuestType == (int)QuestType.Collect2 || (q.Quest?.QuestType == (int)QuestType.Collect1 && MapInstance.Map.MapTypes.Any(s => s.MapTypeId != (short)MapTypeEnum.Act4))) && q.Quest.QuestObjectives.Any(qst => qst.Data == drop.ItemVNum)));
                   }
               }
           });
       }*/

            lock (_syncObj)
            {
                if (monsterToAttack == null || monsterToAttack.IsAlive) return;
                monsterToAttack.RunDeathEvent();

                if (monsterToAttack.GetBuff(type: CardType.SpecialEffects,
                    subtype: (byte)AdditionalTypes.SpecialEffects.DecreaseKillerHp) is int[] DecreaseKillerHp)
                {
                    var EffectResistance = false;
                    if (Killer.MapEntityId != CharacterId)
                    {
                        if (Killer.HasBuff(type: CardType.Buff, subtype: (byte)AdditionalTypes.Buff.EffectResistance))
                            if (ServerManager.RandomNumber() < 90)
                                EffectResistance = true;
                        if (!EffectResistance)
                            if (DecreaseKillerHp[0] > 0)
                                if (!HasGodMode)
                                {
                                    var DecreasedHp = 0;
                                    if (Killer.Hp - Killer.Hp * DecreaseKillerHp[0] / 100 > 1)
                                        DecreasedHp = Killer.Hp * DecreaseKillerHp[0] / 100;
                                    else
                                        DecreasedHp = Killer.Hp - 1;
                                    Killer.GetDamage(damage: DecreasedHp, damager: monsterToAttack.BattleEntity,
                                        dontKill: true);
                                    Session.SendPacket(packet: Killer.GenerateDm(dmg: DecreasedHp));
                                    if (Killer.Mate != null) Session.SendPacket(packet: Killer.Mate.GenerateStatInfo());
                                    Session.SendPacket(packet: new EffectPacket
                                    { EffectType = Killer.UserType, CallerId = Killer.MapEntityId, EffectId = 6007 });
                                }
                    }
                    else
                    {
                        if (HasBuff(type: CardType.Buff, subtype: (byte)AdditionalTypes.Buff.EffectResistance))
                            if (ServerManager.RandomNumber() < 90)
                                EffectResistance = true;
                        if (!EffectResistance)
                            if (DecreaseKillerHp[0] > 0)
                                if (!HasGodMode)
                                {
                                    var DecreasedHp = 0;
                                    if (Hp - Hp * DecreaseKillerHp[0] / 100 > 1)
                                        DecreasedHp = Hp * DecreaseKillerHp[0] / 100;
                                    else
                                        DecreasedHp = Hp - 1;
                                    GetDamage(damage: DecreasedHp, damager: monsterToAttack.BattleEntity,
                                        dontKill: true);
                                    Session.SendPacket(packet: GenerateDm(dmg: DecreasedHp));
                                    Session.SendPacket(packet: GenerateStat());
                                    Session.SendPacket(packet: GenerateEff(effectid: 6007));
                                }
                    }
                }

                var random = new Random(Seed: DateTime.Now.Millisecond & monsterToAttack.MapMonsterId);

                long? dropOwner;

                lock (monsterToAttack.DamageList)
                {
                    dropOwner =
                        monsterToAttack.DamageList.FirstOrDefault(predicate: s => s.Value > 0).Key?.MapEntityId ?? null;
                }

                Group group = null;
                if (dropOwner != null)
                    group = ServerManager.Instance.Groups.Find(match: g =>
                        g.IsMemberOfGroup(entityId: (long)dropOwner) && g.GroupType == GroupType.Group);
                IncrementQuests(type: QuestType.Hunt, firstData: monsterToAttack.MonsterVNum);

                if (ServerManager.Instance.ChannelId == 51)
                {
                    if (ServerManager.Instance.Act4DemonStat.Mode == 0 &&
                        ServerManager.Instance.Act4AngelStat.Mode == 0 && !CaligorRaid.IsRunning)
                    {
                        if (Faction == FactionType.Angel)
                            ServerManager.Instance.Act4AngelStat.Percentage++;
                        else if (Faction == FactionType.Demon) ServerManager.Instance.Act4DemonStat.Percentage++;
                    }

                    if (monsterToAttack.MonsterVNum == 556)
                    {
                        if (ServerManager.Instance.Act4AngelStat.Mode == 1 && Faction != FactionType.Angel)
                            ServerManager.Instance.Act4AngelStat.Mode = 0;
                        if (ServerManager.Instance.Act4DemonStat.Mode == 1 && Faction != FactionType.Demon)
                            ServerManager.Instance.Act4DemonStat.Mode = 0;
                    }
                }

                // end owner set
                if (Session.HasCurrentMapInstance && (MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance ||
                                                      MapInstance.MapInstanceType == MapInstanceType.LodInstance ||
                                                      MapInstance.DropAllowed))
                {
                    var explodeMonsters = new short[] { 1348, 1906 };

                    var droplist = monsterToAttack.Monster.Drops.Where(predicate: s =>
                        !explodeMonsters.Contains(value: monsterToAttack.MonsterVNum) &&
                        Session.CurrentMapInstance.Map.MapTypes.Any(predicate: m => m.MapTypeId == s.MapTypeId) ||
                        s.MapTypeId == null).ToList();

                    var levelDifference = Session.Character.Level - monsterToAttack.Monster.Level;

                    #region Quest

                    Quests.Where(predicate: q =>
                            q.Quest?.QuestType == (int)QuestType.Collect4 ||
                            q.Quest?.QuestType == (int)QuestType.Collect2 ||
                            q.Quest?.QuestType == (int)QuestType.Collect1 &&
                            MapInstance.Map.MapTypes.Any(predicate: s => s.MapTypeId != (short)MapTypeEnum.Act4))
                        .ToList()
                        .ForEach(
                            action: qst =>
                            {
                                qst.Quest.QuestObjectives.ForEach(action: d =>
                                {
                                    if (d.SpecialData == monsterToAttack.MonsterVNum || d.SpecialData == null)
                                        droplist.Add(item: new DropDto
                                        {
                                            ItemVNum = (short)d.Data,
                                            Amount = 1,
                                            MonsterVNum = monsterToAttack.MonsterVNum,
                                            DropChance =
                                                (d.DropRate ?? 100) * 100 *
                                                ServerManager.Instance.Configuration.QuestDropRate // Approx
                                        });
                                });
                            });

                    IncrementQuests(type: QuestType.FlowerQuest, firstData: monsterToAttack.Monster.Level);

                    #endregion

                    if (explodeMonsters.Contains(value: monsterToAttack.MonsterVNum) &&
                        ServerManager.RandomNumber() < 50)
                    {
                        MapInstance.Broadcast(packet: $"eff 3 {monsterToAttack.MapMonsterId} 3619");
                        if (Killer.MapEntityId != CharacterId)
                        {
                            if (!HasGodMode)
                            {
                                var DecreasedHp = 0;
                                if (Killer.Hp - Killer.Hp * 50 / 100 > 1)
                                    DecreasedHp = Killer.Hp * 50 / 100;
                                else
                                    DecreasedHp = Killer.Hp - 1;
                                Killer.GetDamage(damage: DecreasedHp, damager: monsterToAttack.BattleEntity,
                                    dontKill: true);
                                if (Killer.Mate != null) Session.SendPacket(packet: Killer.Mate.GenerateStatInfo());
                            }
                        }
                        else
                        {
                            if (!HasGodMode)
                            {
                                var DecreasedHp = 0;
                                if (Hp - Hp * 50 / 100 > 1)
                                    DecreasedHp = Hp * 50 / 100;
                                else
                                    DecreasedHp = Hp - 1;
                                GetDamage(damage: DecreasedHp, damager: monsterToAttack.BattleEntity, dontKill: true);
                                Session.SendPacket(packet: GenerateStat());
                            }
                        }

                        return;
                    }

                    if (monsterToAttack.Monster.MonsterType != MonsterType.Special)
                    {
                        #region item drop

                        var dropRate = ServerManager.Instance.Configuration.RateDrop + MapInstance.DropRate;
                        var x = 0;
                        var rndamount = ServerManager.RandomNumber() * random.NextDouble();
                        foreach (var drop in droplist.OrderBy(keySelector: s => random.Next()))
                            if (x < 4)
                            {
                                if (!explodeMonsters.Contains(value: monsterToAttack.MonsterVNum))
                                    rndamount = ServerManager.RandomNumber() * random.NextDouble();
                                var divideRate = true;
                                if (MapInstance.Map.MapTypes.Any(predicate: m => m.MapTypeId == (byte)MapTypeEnum.Act4)
                                    || MapInstance.Map.MapId == 20001 // Miniland
                                    || explodeMonsters.Contains(value: monsterToAttack.MonsterVNum))
                                    divideRate = false;
                                var divider = !divideRate ? 1D :
                                    levelDifference >= 20 ? (levelDifference - 19) * 1.2D :
                                    levelDifference <= -20 ? (levelDifference + 19) * 1.2D : 1D;
                                if (rndamount <= (double)drop.DropChance * dropRate / 1000.000 / divider)
                                {
                                    x++;
                                    if (Session.CurrentMapInstance != null)
                                    {
                                        if (monsterToAttack.Monster.MonsterType == MonsterType.Elite)
                                        {
                                            var alreadyGifted = new List<long>();
                                            List<BattleEntity> damagers;

                                            lock (monsterToAttack.DamageList)
                                            {
                                                damagers = monsterToAttack.DamageList.Keys.ToList();
                                            }

                                            foreach (var damager in damagers)
                                                if (!alreadyGifted.Contains(item: damager.MapEntityId))
                                                {
                                                    var giftsession =
                                                        ServerManager.Instance.GetSessionByCharacterId(
                                                            characterId: damager.MapEntityId);
                                                    giftsession?.Character.GiftAdd(itemVNum: drop.ItemVNum,
                                                        amount: (byte)drop.Amount);
                                                    alreadyGifted.Add(item: damager.MapEntityId);
                                                }
                                        }
                                        else if (Session.CurrentMapInstance.Map.MapTypes.Any(predicate: s =>
                                            s.MapTypeId == (short)MapTypeEnum.Act4))
                                        {
                                            var alreadyGifted = new List<long>();
                                            List<Character> hitters;

                                            lock (monsterToAttack.DamageList)
                                            {
                                                hitters = monsterToAttack.DamageList
                                                    .Where(predicate: s => s.Key?.Character != null &&
                                                                           s.Key.Character.MapInstance ==
                                                                           monsterToAttack.MapInstance && s.Value > 0)
                                                    .Select(selector: s => s.Key.Character).ToList();
                                            }

                                            foreach (var hitter in hitters)
                                                if (!alreadyGifted.Contains(item: hitter.CharacterId))
                                                {
                                                    hitter.GiftAdd(itemVNum: drop.ItemVNum, amount: (byte)drop.Amount);
                                                    alreadyGifted.Add(item: hitter.CharacterId);
                                                }
                                        }
                                        else
                                        {
                                            if (group?.GroupType == GroupType.Group)
                                            {
                                                if (group.SharingMode == (byte)GroupSharingType.ByOrder)
                                                {
                                                    dropOwner = group.GetNextOrderedCharacterId(character: this);
                                                    if (dropOwner.HasValue)
                                                        group.Sessions.ForEach(action: s =>
                                                            s.SendPacket(packet: s.Character.GenerateSay(
                                                                message: string.Format(
                                                                    format: Language.Instance
                                                                        .GetMessageFromKey(key: "ITEM_BOUND_TO"),
                                                                    arg0: ServerManager.GetItem(vnum: drop.ItemVNum)
                                                                        .Name,
                                                                    arg1: group.Sessions.Single(predicate: c =>
                                                                            c.Character.CharacterId == dropOwner)
                                                                        .Character.Name, arg2: drop.Amount),
                                                                type: 10)));
                                                }
                                                else
                                                {
                                                    group.Sessions.ForEach(action: s =>
                                                        s.SendPacket(packet: s.Character.GenerateSay(
                                                            message: string.Format(
                                                                format: Language.Instance.GetMessageFromKey(
                                                                    key: "DROPPED_ITEM"),
                                                                arg0: ServerManager.GetItem(vnum: drop.ItemVNum).Name,
                                                                arg1: drop.Amount),
                                                            type: 10)));
                                                }
                                            }

                                            _handleItemDrop(drop: drop, owner: dropOwner, posX: monsterToAttack.MapX,
                                                posY: monsterToAttack.MapY);
                                        }
                                    }

                                    if (explodeMonsters.Contains(value: monsterToAttack.MonsterVNum)) break;
                                }
                                else if (explodeMonsters.Contains(value: monsterToAttack.MonsterVNum))
                                {
                                    rndamount -= (double)drop.DropChance * dropRate / 1000.000 / divider;
                                }
                            }

                        #endregion

                        #region gold drop

                        // gold calculation
                        var gold = GetGold(mapMonster: monsterToAttack);
                        gold *= ServerManager.Instance.Configuration.RateGold;
                        var maxGold = ServerManager.Instance.Configuration.MaxGold;
                        gold = gold > maxGold ? (int)maxGold : gold;
                        var randChance = ServerManager.RandomNumber() * random.NextDouble();

                        if (Session.CurrentMapInstance.MapInstanceType != MapInstanceType.LodInstance && gold > 0 &&
                            randChance <= (int)(ServerManager.Instance.Configuration.RateGoldDrop * 10 *
                                                 (Session.CurrentMapInstance.Map.MapTypes.Any(predicate: s =>
                                                     s.MapTypeId == (short)MapTypeEnum.Act4)
                                                     ? 1
                                                     : CharacterHelper.GoldPenalty(playerLevel: Level,
                                                         monsterLevel: monsterToAttack.Monster.Level))))
                        {
                            var drop2 = new DropDto
                            {
                                Amount = gold,
                                ItemVNum = 1046
                            };

                            if (Session.CurrentMapInstance != null)
                            {
                                if (Session.CurrentMapInstance.Map.MapTypes.Any(predicate: s =>
                                        s.MapTypeId == (short)MapTypeEnum.Act4) ||
                                    monsterToAttack.Monster.MonsterType == MonsterType.Elite)
                                {
                                    var alreadyGifted = new List<long>();
                                    List<BattleEntity> damagers;

                                    lock (monsterToAttack.DamageList)
                                    {
                                        damagers = monsterToAttack.DamageList.Keys.ToList();
                                    }

                                    foreach (var damager in damagers)
                                        if (!alreadyGifted.Contains(item: damager.MapEntityId))
                                        {
                                            var session =
                                                ServerManager.Instance.GetSessionByCharacterId(
                                                    characterId: damager.MapEntityId);
                                            if (session != null)
                                            {
                                                var multiplier =
                                                    1 + GetBuff(type: CardType.Item,
                                                        subtype: (byte)AdditionalTypes.Item.IncreaseEarnedGold)[0] /
                                                    100D;
                                                multiplier +=
                                                    (ShellEffectMain.FirstOrDefault(predicate: s =>
                                                            s.Effect == (byte)ShellWeaponEffectType.GainMoreGold)
                                                        ?.Value ?? 0) / 100D;

                                                session.Character.Gold += (int)(drop2.Amount * multiplier);
                                                if (session.Character.Gold > maxGold)
                                                {
                                                    session.Character.Gold = maxGold;
                                                    session.SendPacket(
                                                        packet: UserInterfaceHelper.GenerateMsg(
                                                            message: Language.Instance.GetMessageFromKey(
                                                                key: "MAX_GOLD"), type: 0));
                                                }

                                                session.SendPacket(packet: session.Character.GenerateSay(
                                                    message:
                                                    $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {ServerManager.GetItem(vnum: drop2.ItemVNum).Name} x {drop2.Amount}{(multiplier > 1 ? $" + {(int)(drop2.Amount * multiplier) - drop2.Amount}" : "")}",
                                                    type: 10));
                                                session.SendPacket(packet: session.Character.GenerateGold());
                                            }

                                            alreadyGifted.Add(item: damager.MapEntityId);
                                        }
                                }
                                else
                                {
                                    if (group != null && MapInstance.MapInstanceType != MapInstanceType.LodInstance)
                                    {
                                        if (group.SharingMode == (byte)GroupSharingType.ByOrder)
                                        {
                                            dropOwner = group.GetNextOrderedCharacterId(character: this);

                                            if (dropOwner.HasValue)
                                                group.Sessions.ForEach(action: s =>
                                                    s.SendPacket(packet: s.Character.GenerateSay(
                                                        message: string.Format(
                                                            format: Language.Instance.GetMessageFromKey(
                                                                key: "ITEM_BOUND_TO"),
                                                            arg0: ServerManager.GetItem(vnum: drop2.ItemVNum).Name,
                                                            arg1: group.Sessions.Single(predicate: c =>
                                                                    c.Character.CharacterId == dropOwner)
                                                                .Character
                                                                .Name, arg2: drop2.Amount), type: 10)));
                                        }
                                        else
                                        {
                                            group.Sessions.ForEach(action: s =>
                                                s.SendPacket(packet: s.Character.GenerateSay(
                                                    message: string.Format(
                                                        format: Language.Instance
                                                            .GetMessageFromKey(key: "DROPPED_ITEM"),
                                                        arg0: ServerManager.GetItem(vnum: drop2.ItemVNum).Name,
                                                        arg1: drop2.Amount),
                                                    type: 10)));
                                        }
                                    }

                                    _handleGoldDrop(drop: drop2, maxGold: maxGold, dropOwner: dropOwner,
                                        posX: monsterToAttack.MapX,
                                        posY: monsterToAttack.MapY);
                                }
                            }
                        }

                        #endregion
                    }
                }

                #region MonsterReputation

                if (Hp > 0) //Reputation overall test []
                {
                    {
                        GetReputation(amount: monsterToAttack.Monster.Level);
                    }
                    GenerateDignity(monsterinfo: monsterToAttack.Monster);
                }

                #endregion

                #region EXP, Reputation and Dignity

                if (Hp > 0 && !monsterToAttack.BattleEntity.IsMateTrainer(vnum: monsterToAttack.MonsterVNum))
                {
                    // If the Halloween event is running then the EXP is disabled in NosVille. -- Is this official-like or VSalu bullshit?
                    if (!ServerManager.Instance.Configuration.HalloweenEvent || MapInstance.Map.MapId != 1)
                        GenerateXp(monster: monsterToAttack);

                    GenerateDignity(monsterinfo: monsterToAttack.Monster);

                    if (MapInstance.IsReputationMap)
                    {
                        if (Group?.GroupType == GroupType.Group)
                            foreach (var targetSession in Group.Sessions.Where(predicate: s =>
                                s.Character.MapInstanceId == MapInstanceId))
                                targetSession.Character.GetReputation(amount: monsterToAttack.Monster.Level / 2);
                        else
                            GetReputation(amount: monsterToAttack.Monster.Level / 2);
                    }
                }

                #endregion
            }
        }

        public string GenerateLev()
        {
            ItemInstance specialist = null;
            if (Inventory != null)
                specialist = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);
            return
                $"lev {Level} {(int)(Level < 100 ? LevelXp : LevelXp / 100)} {(!UseSp || specialist == null ? JobLevel : specialist.SpLevel)} {(!UseSp || specialist == null ? JobLevelXp : specialist.Xp)} {(int)(Level < 100 ? XpLoad() : XpLoad() / 100)} {(!UseSp || specialist == null ? JobXPLoad() : SpXpLoad())} {Reputation} {GetCP()} {(int)(HeroLevel < 100 ? HeroXp : HeroXp / 100)} {HeroLevel} {(int)(HeroLevel < 100 ? HeroXPLoad() : HeroXPLoad() / 100)} 0";
        }

        public string GenerateLevelUp()
        {
            Logger.LogUserEvent(logEvent: "LEVELUP", caller: Session.GenerateIdentity(),
                data:
                $"Level: {Level} JobLevel: {JobLevel} SPLevel: {Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear)?.SpLevel} HeroLevel: {HeroLevel} MapId: {Session.CurrentMapInstance?.Map.MapId} MapX: {PositionX} MapY: {PositionY}");
            return $"levelup {CharacterId}";
        }

        public void GenerateMiniland()
        {
            if (Miniland == null)
            {
                Miniland = ServerManager.GenerateMapInstance(mapId: 20001, type: MapInstanceType.NormalInstance,
                    mapclock: new InstanceBag(),
                    dropAllowed: true);
                foreach (var obj in DaoFactory.MinilandObjectDao.LoadByCharacterId(characterId: CharacterId))
                {
                    var mapobj = new MinilandObject(input: obj);
                    if (mapobj.ItemInstanceId != null)
                    {
                        var item = Inventory.GetItemInstanceById(id: (Guid)mapobj.ItemInstanceId);
                        if (item != null)
                        {
                            mapobj.ItemInstance = item;
                            MinilandObjects.Add(item: mapobj);
                        }
                    }
                }
            }
        }

        public string GenerateMinilandObjectForFriends()
        {
            var mlobjstring = "mltobj";
            var i = 0;
            foreach (var mp in MinilandObjects)
            {
                mlobjstring += $" {mp.ItemInstance.ItemVNum}.{i}.{mp.MapX}.{mp.MapY}";
                i++;
            }

            return mlobjstring;
        }

        public string GenerateMinilandPoint()
        {
            return $"mlpt {MinilandPoint} 100";
        }

        public string GenerateMinimapPosition()
        {
            return MapInstance.MapInstanceType == MapInstanceType.TimeSpaceInstance
                   || MapInstance.MapInstanceType == MapInstanceType.RaidInstance
                ? $"rsfp {MapInstance.MapIndexX} {MapInstance.MapIndexY}"
                : "rsfp 0 -1";
        }

        public string GenerateMlinfo()
        {
            return
                $"mlinfo 3800 {MinilandPoint} 100 {GeneralLogs.CountLinq(predicate: s => s.LogData == nameof(Miniland) && s.Timestamp.Day == DateTime.Now.Day)} {GeneralLogs.CountLinq(predicate: s => s.LogData == nameof(Miniland))} 10 {(byte)MinilandState} {Language.Instance.GetMessageFromKey(key: "WELCOME_MUSIC_INFO")} {MinilandMessage.Replace(oldChar: ' ', newChar: '^')}";
        }

        public string GenerateMlinfobr()
        {
            return
                $"mlinfobr 3800 {Name} {GeneralLogs.CountLinq(predicate: s => s.LogData == nameof(Miniland) && s.Timestamp.Day == DateTime.Now.Day)} {GeneralLogs.CountLinq(predicate: s => s.LogData == nameof(Miniland))} 25 {MinilandMessage.Replace(oldChar: ' ', newChar: '^')}";
        }

        public string GenerateMloMg(MinilandObject mlobj, MinigamePacket packet)
        {
            return
                $"mlo_mg {packet.MinigameVNum} {MinilandPoint} 0 0 {mlobj.ItemInstance.DurabilityPoint} {mlobj.ItemInstance.Item.MinilandObjectPoint}";
        }

        public string GenerateNpcDialog(int value)
        {
            return $"npc_req 1 {CharacterId} {value}";
        }

        public string GeneratePairy()
        {
            ItemInstance fairy = null;
            if (Inventory != null)
                fairy = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Fairy, type: InventoryType.Wear);
            ElementRate = 0;
            Element = 0;
            var shouldChangeMorph = false;

            if (fairy != null)
            {
                //exclude magical fairy
                shouldChangeMorph = IsUsingFairyBooster && fairy.Item.Morph > 4 && fairy.Item.Morph != 9 &&
                                    fairy.Item.Morph != 14;
                ElementRate += fairy.ElementRate + fairy.Item.ElementRate + (IsUsingFairyBooster ? 30 : 0) +
                               GetStuffBuff(type: CardType.PixieCostumeWings,
                                   subtype: (byte)AdditionalTypes.PixieCostumeWings.IncreaseFairyElement)[0];
                Element = fairy.Item.Element;
            }

            return fairy != null
                ? $"pairy 1 {CharacterId} 4 {fairy.Item.Element} {fairy.ElementRate + fairy.Item.ElementRate} {fairy.Item.Morph + (shouldChangeMorph ? 5 : 0)}"
                : $"pairy 1 {CharacterId} 0 0 0 0";
        }

        void GenerateJobXpLevelUp()
        {
            var t = JobXPLoad();
            while (JobLevelXp >= t)
            {
                JobLevelXp -= (long)t;
                JobLevel++;
                t = JobXPLoad();
                if (JobLevel >= 20 && Class == 0)
                {
                    JobLevel = 20;
                    JobLevelXp = 0;
                }
                else if (JobLevel >= ServerManager.Instance.Configuration.MaxJobLevel)
                {
                    JobLevel = ServerManager.Instance.Configuration.MaxJobLevel;
                    JobLevelXp = 0;
                }

                Hp = (int)HPLoad();
                Mp = (int)MPLoad();
                Session.SendPacket(packet: GenerateStat());
                Session.SendPacket(packet: GenerateLevelUp());
                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "JOB_LEVELUP"),
                    type: 0));
                LearnAdventurerSkills();
                Session.SendPackets(packets: GenerateQuicklist());
                Session.CurrentMapInstance?.Broadcast(packet: GenerateEff(effectid: 8), xRangeCoordinate: PositionX,
                    yRangeCoordinate: PositionY);
                Session.CurrentMapInstance?.Broadcast(packet: GenerateEff(effectid: 198), xRangeCoordinate: PositionX,
                    yRangeCoordinate: PositionY);
            }
        }

        void GenerateSpXpLevelUp(ItemInstance specialist)
        {
            var t = SpXpLoad();
            while (UseSp && specialist.Xp >= t)
            {
                specialist.Xp -= (long)t;
                specialist.SpLevel++;
                t = SpXpLoad();
                Session.SendPacket(packet: GenerateStat());
                Session.SendPacket(packet: GenerateLevelUp());
                if (specialist.SpLevel >= ServerManager.Instance.Configuration.MaxSpLevel)
                {
                    specialist.SpLevel = ServerManager.Instance.Configuration.MaxSpLevel;
                    specialist.Xp = 0;
                }

                LearnSPSkill();
                SkillsSp.ForEach(action: s => s.LastUse = DateTime.Now.AddDays(value: -1));
                Session.SendPacket(packet: GenerateSki());
                Session.SendPackets(packets: GenerateQuicklist());

                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "SP_LEVELUP"), type: 0));
                Session.CurrentMapInstance?.Broadcast(
                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player, callerId: CharacterId,
                        effectId: 8),
                    xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
                Session.CurrentMapInstance?.Broadcast(
                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player, callerId: CharacterId,
                        effectId: 198),
                    xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
            }
        }

        void GenerateHeroXpLevelUp()
        {
            var t = HeroXPLoad();
            while (HeroXp >= t)
            {
                HeroXp -= (long)t;
                HeroLevel++;
                t = HeroXPLoad();
                if (HeroLevel >= ServerManager.Instance.Configuration.MaxHeroLevel)
                {
                    HeroLevel = ServerManager.Instance.Configuration.MaxHeroLevel;
                    HeroXp = 0;
                }

                Hp = (int)HPLoad();
                Mp = (int)MPLoad();
                Session.SendPacket(packet: GenerateStat());
                Session.SendPacket(packet: GenerateLevelUp());
                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "HERO_LEVELUP"),
                    type: 0));
                Session.CurrentMapInstance?.Broadcast(
                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player, callerId: CharacterId,
                        effectId: 8),
                    xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
                Session.CurrentMapInstance?.Broadcast(
                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player, callerId: CharacterId,
                        effectId: 198),
                    xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
            }
        }

        void GenerateLevelXpLevelUp()
        {
            var t = XpLoad();
            while (LevelXp >= t)
            {
                LevelXp -= (long)t;
                Level++;
                t = XpLoad();
                if (Level >= ServerManager.Instance.Configuration.MaxLevel)
                {
                    Level = ServerManager.Instance.Configuration.MaxLevel;
                    LevelXp = 0;
                }

                if (Level == ServerManager.Instance.Configuration.HeroicStartLevel && HeroLevel == 0)
                {
                    HeroLevel = 1;
                    HeroXp = 0;
                }

                Hp = (int)HPLoad();
                Mp = (int)MPLoad();
                Session.SendPacket(packet: GenerateStat());
                if (Family != null)
                {
                    if (Level > 20 && Level % 10 == 0)
                    {
                        Family.InsertFamilyLog(logtype: FamilyLogType.LevelUp, characterName: Name, level: Level);
                        GenerateFamilyXp(FXP: 20 * Level);
                    }
                    else if (Level > 80)
                    {
                        Family.InsertFamilyLog(logtype: FamilyLogType.LevelUp, characterName: Name, level: Level);
                    }
                    else
                    {
                        ServerManager.Instance.FamilyRefresh(familyId: Family.FamilyId);
                        CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
                        {
                            DestinationCharacterId = Family.FamilyId,
                            SourceCharacterId = CharacterId,
                            SourceWorldId = ServerManager.Instance.WorldId,
                            Message = "fhis_stc",
                            Type = MessageType.Family
                        });
                    }
                }

                Session.SendPacket(packet: GenerateLevelUp());
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "LEVELUP"), type: 0));
                Session.CurrentMapInstance?.Broadcast(packet: GenerateEff(effectid: 6), xRangeCoordinate: PositionX,
                    yRangeCoordinate: PositionY);
                Session.CurrentMapInstance?.Broadcast(packet: GenerateEff(effectid: 198), xRangeCoordinate: PositionX,
                    yRangeCoordinate: PositionY);
                ServerManager.Instance.UpdateGroup(charId: CharacterId);

                if (Level >= 20) GetReputation(amount: 50);

                LevelRewards(Level: Level);
            }
        }

        public void LevelRewards(int Level)
        {
            switch (Level)
            {
                case 10:
                    switch (Class)
                    {
                        case ClassType.Adventurer:
                            break;
                        case ClassType.Swordsman:
                            break;
                        case ClassType.Archer:
                            break;
                        case ClassType.Magician:
                            break;
                    }

                    break;
                case 20:
                    switch (Class)
                    {
                        case ClassType.Adventurer:
                            break;
                        case ClassType.Swordsman:
                            break;
                        case ClassType.Archer:
                            break;
                        case ClassType.Magician:
                            break;
                    }

                    break;
                case 30:
                    switch (Class)
                    {
                        case ClassType.Adventurer:
                            break;
                        case ClassType.Swordsman:
                            break;
                        case ClassType.Archer:
                            break;
                        case ClassType.Magician:
                            break;
                    }

                    break;
                case 40:
                    switch (Class)
                    {
                        case ClassType.Adventurer:
                            break;
                        case ClassType.Swordsman:
                            break;
                        case ClassType.Archer:
                            break;
                        case ClassType.Magician:
                            break;
                    }

                    break;
                case 50:
                    switch (Class)
                    {
                        case ClassType.Adventurer:
                            break;
                        case ClassType.Swordsman:
                            break;
                        case ClassType.Archer:
                            break;
                        case ClassType.Magician:
                            break;
                    }

                    break;
                case 60:
                    switch (Class)
                    {
                        case ClassType.Adventurer:
                            break;
                        case ClassType.Swordsman:
                            break;
                        case ClassType.Archer:
                            break;
                        case ClassType.Magician:
                            break;
                    }

                    break;
                case 70:
                    switch (Class)
                    {
                        case ClassType.Adventurer:
                            break;
                        case ClassType.Swordsman:
                            break;
                        case ClassType.Archer:
                            break;
                        case ClassType.Magician:
                            break;
                    }

                    break;
                case 80:
                    switch (Class)
                    {
                        case ClassType.Adventurer:
                            break;
                        case ClassType.Swordsman:
                            break;
                        case ClassType.Archer:
                            break;
                        case ClassType.Magician:
                            break;
                    }

                    break;
                case 90:
                    switch (Class)
                    {
                        case ClassType.Adventurer:
                            break;
                        case ClassType.Swordsman:
                            break;
                        case ClassType.Archer:
                            break;
                        case ClassType.Magician:
                            break;
                    }

                    break;
            }
        }

        public string GenerateParcel(MailDto mail)
        {
            return mail.AttachmentVNum != null
                ? $"parcel 1 1 {MailList.First(predicate: s => s.Value.MailId == mail.MailId).Key} {(mail.Title == "NOSMALL" ? 1 : 4)} 0 {mail.Date.ToString(format: "yyMMddHHmm")} {mail.Title} {mail.AttachmentVNum} {mail.AttachmentAmount} {(byte)ServerManager.GetItem(vnum: (short)mail.AttachmentVNum).Type}"
                : "";
        }

        public string GeneratePidx(bool isLeaveGroup = false)
        {
            if (!isLeaveGroup && Group != null)
            {
                var result = $"pidx {Group.GroupId}";
                foreach (var session in Group.Sessions.GetAllItems()
                    .Where(predicate: s => s.Character.CharacterId != CharacterId))
                    if (session.Character != null)
                        result +=
                            $" {(Group.IsMemberOfGroup(entityId: CharacterId) ? 1 : 0)}.{session.Character.CharacterId} ";
                foreach (var session in Group.Sessions.GetAllItems()
                    .Where(predicate: s => s.Character.CharacterId == CharacterId))
                    if (session.Character != null)
                        result +=
                            $" {(Group.IsMemberOfGroup(entityId: CharacterId) ? 1 : 0)}.{session.Character.CharacterId} ";
                return result;
            }

            return $"pidx -1 1.{CharacterId}";
        }

        public string GeneratePinit()
        {
            var grp = ServerManager.Instance.Groups.Find(match: s =>
                s.IsMemberOfGroup(entityId: CharacterId) && s.GroupType == GroupType.Group);

            var mates = Mates.ToList();

            var count = 0;

            var str = "";

            if (mates != null)
                foreach (var mate in mates.Where(predicate: s => s.IsTeamMember)
                    .OrderByDescending(keySelector: s => s.MateType))
                {
                    if ((byte)mate.MateType == 1) count++;

                    str +=
                        $" 2|{mate.MateTransportId}|{(short)mate.MateType}|{mate.Level}|{(mate.IsUsingSp ? mate.Sp.GetName() : mate.Name.Replace(oldChar: ' ', newChar: '^'))}|-1|{(mate.IsUsingSp && mate.Sp != null ? mate.Sp.Instance.Item.Morph : mate.Monster.NpcMonsterVNum)}|0";
                }

            if (grp != null)
            {
                foreach (var groupSessionForId in grp.Sessions.GetAllItems()
                    .Where(predicate: s => s.Character.CharacterId != CharacterId))
                {
                    count++;
                    str +=
                        $" 1|{groupSessionForId.Character.CharacterId}|{count}|{groupSessionForId.Character.Level}|{groupSessionForId.Character.Name}|0|{(byte)groupSessionForId.Character.Gender}|{(byte)groupSessionForId.Character.Class}|{(groupSessionForId.Character.UseSp || groupSessionForId.Character.IsVehicled || groupSessionForId.Character.IsMorphed ? groupSessionForId.Character.Morph : 0)}|{groupSessionForId.Character.HeroLevel}";
                }

                foreach (var groupSessionForId in grp.Sessions.GetAllItems()
                    .Where(predicate: s => s.Character.CharacterId == CharacterId))
                {
                    count++;
                    str +=
                        $" 1|{groupSessionForId.Character.CharacterId}|{count}|{groupSessionForId.Character.Level}|{groupSessionForId.Character.Name}|0|{(byte)groupSessionForId.Character.Gender}|{(byte)groupSessionForId.Character.Class}|{(groupSessionForId.Character.UseSp || groupSessionForId.Character.IsVehicled || groupSessionForId.Character.IsMorphed ? groupSessionForId.Character.Morph : 0)}|{groupSessionForId.Character.HeroLevel}";
                }
            }

            return $"pinit {(grp != null ? count : mates.Count(predicate: s => s.IsTeamMember))} {str}";
        }

        public string GeneratePlayerFlag(long pflag)
        {
            return $"pflag 1 {CharacterId} {pflag}";
        }

        public string GeneratePost(MailDto mail, byte type)
        {
            if (mail != null)
                return
                    $"post 1 {type} {(MailList?.FirstOrDefault(predicate: s => s.Value?.MailId == mail?.MailId))?.Key} 0 {(mail.IsOpened ? 1 : 0)} {mail.Date.ToString(format: "yyMMddHHmm")} {(type == 2 ? DaoFactory.CharacterDao.LoadById(characterId: mail.ReceiverId).Name : DaoFactory.CharacterDao.LoadById(characterId: mail.SenderId).Name)} {mail.Title}";
            return "";
        }

        public string GeneratePostMessage(MailDto mailDTO, byte type)
        {
            var sender = DaoFactory.CharacterDao.LoadById(characterId: mailDTO.SenderId);

            return
                $"post 5 {type} {MailList.First(predicate: s => s.Value == mailDTO).Key} 0 0 {(byte)mailDTO.SenderClass} {(byte)mailDTO.SenderGender} {mailDTO.SenderMorphId} {(byte)mailDTO.SenderHairStyle} {(byte)mailDTO.SenderHairColor} {mailDTO.EqPacket} {sender.Name} {mailDTO.Title} {mailDTO.Message}";
        }

        public List<string> GeneratePst()
        {
            return Mates.Where(predicate: s => s.IsTeamMember).OrderByDescending(keySelector: s => s.MateType).Select(
                    selector: mate =>
                        $"pst 2 {mate.MateTransportId} {(mate.MateType == MateType.Partner ? "0" : "1")} {(int)(mate.Hp / mate.MaxHp * 100)} {(int)(mate.Mp / mate.MaxMp * 100)} {mate.Hp} {mate.Mp} 0 0 0 {mate.Buff.GetAllItems().Aggregate(seed: "", func: (current, buff) => current + $" {buff.Card.CardId}")}")
                .ToList();
        }

        public string GeneratePStashAll()
        {
            var stash =
                $"pstash_all {(StaticBonusList.Any(predicate: s => s.StaticBonusType == StaticBonusType.PetBackPack) ? 50 : 0)}";
            return Inventory.Where(predicate: s => s.Type == InventoryType.PetWarehouse).Aggregate(seed: stash,
                func: (current, item) => current + $" {item.GenerateStashPacket()}");
        }

        public IEnumerable<string> GenerateQuicklist()
        {
            string[] pktQs = { "qslot 0", "qslot 1" };

            for (var i = 0; i < 30; i++)
                for (var j = 0; j < 2; j++)
                {
                    var qi = QuicklistEntries.FirstOrDefault(predicate: n =>
                        n.Q1 == j && n.Q2 == i && n.Morph == (UseSp ? Morph : 0));
                    pktQs[j] += $" {qi?.Type ?? 7}.{qi?.Slot ?? 7}.{qi?.Pos.ToString() ?? "-1"}";
                }

            return pktQs;
        }

        public string GenerateRaid(int Type, bool exit = false)
        {
            var result = "";
            switch (Type)
            {
                case 0:
                    result = "raid 0";
                    Group?.Sessions?.ForEach(action: s => result += $" {s.Character?.CharacterId}");
                    break;

                case 2:
                    result = $"raid 2 {(exit ? "-1" : $"{Group?.Sessions?.FirstOrDefault().Character.CharacterId}")}";
                    break;

                case 1:
                    result = $"raid 1 {(exit ? 0 : 1)}";
                    break;

                case 3:
                    result = "raid 3";
                    Group?.Sessions?.ForEach(action: s =>
                        result +=
                            $" {s.Character?.CharacterId}.{Math.Ceiling(a: s.Character.Hp / s.Character.HPLoad() * 100)}.{Math.Ceiling(a: s.Character.Mp / s.Character.MPLoad() * 100)}");
                    break;

                case 4:
                    result = "raid 4";
                    break;

                case 5:
                    result = "raid 5 1";
                    break;
            }

            return result;
        }

        public static string GenerateRaidBf(byte type)
        {
            return $"raidbf 0 {type} 25 ";
        }

        public string GenerateRCSList(CSListPacket packet)
        {
            var list = "";
            var billist = new BazaarItemLink[ServerManager.Instance.BazaarList.Count + 20];
            ServerManager.Instance.BazaarList.CopyTo(grpmembers: billist);
            foreach (var bz in billist
                .Where(predicate: s => s != null &&
                                       (s.BazaarItem.DateStart.AddHours(value: s.BazaarItem.Duration)
                                           .AddDays(value: s.BazaarItem.MedalUsed ? 30 : 7) - DateTime.Now)
                                       .TotalMinutes > 0 &&
                                       s.BazaarItem.SellerId == CharacterId).Skip(count: packet.Index * 50)
                .Take(count: 50))
                if (bz.Item != null)
                {
                    var soldedAmount = bz.BazaarItem.Amount - bz.Item.Amount;
                    int amount = bz.BazaarItem.Amount;
                    var package = bz.BazaarItem.IsPackage;
                    var isNosbazar = bz.BazaarItem.MedalUsed;
                    var price = bz.BazaarItem.Price;
                    var minutesLeft =
                        (long)(bz.BazaarItem.DateStart.AddHours(value: bz.BazaarItem.Duration) - DateTime.Now)
                        .TotalMinutes;
                    var Status = minutesLeft >= 0
                        ? soldedAmount < amount ? (byte)BazaarType.OnSale : (byte)BazaarType.Solded
                        : (byte)BazaarType.DelayExpired;
                    if (Status == (byte)BazaarType.DelayExpired)
                        minutesLeft =
                            (long)(bz.BazaarItem.DateStart.AddHours(value: bz.BazaarItem.Duration)
                                .AddDays(value: isNosbazar ? 30 : 7) - DateTime.Now).TotalMinutes;
                    var info = "";
                    if (bz.Item.Item.Type == InventoryType.Equipment)
                    {
                        bz.Item.ShellEffects.Clear();
                        bz.Item.ShellEffects.AddRange(
                            collection: DaoFactory.ShellEffectDao.LoadByEquipmentSerialId(
                                id: bz.Item.EquipmentSerialId));
                        info = bz.Item?.GenerateEInfo().Replace(oldChar: ' ', newChar: '^')
                            .Replace(oldValue: "e_info^", newValue: "");
                    }

                    if (packet.Filter == 0 || packet.Filter == Status)
                        list +=
                            $"{bz.BazaarItem.BazaarItemId}|{bz.BazaarItem.SellerId}|{bz.Item.ItemVNum}|{soldedAmount}|{amount}|{(package ? 1 : 0)}|{price}|{Status}|{minutesLeft}|{(isNosbazar ? 1 : 0)}|0|{bz.Item.Rare}|{bz.Item.Upgrade}|{info} ";
                }

            return $"rc_slist {packet.Index} {list}";
        }

        public string GenerateReqInfo()
        {
            ItemInstance fairy = null;
            ItemInstance armor = null;
            ItemInstance weapon2 = null;
            ItemInstance weapon = null;

            if (Inventory != null)
            {
                fairy = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Fairy, type: InventoryType.Wear);
                armor = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Armor, type: InventoryType.Wear);
                weapon2 = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.SecondaryWeapon,
                    type: InventoryType.Wear);
                weapon = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon, type: InventoryType.Wear);
            }

            var isPvpPrimary = false;
            var isPvpSecondary = false;
            var isPvpArmor = false;

            if (weapon?.Item.Name.Contains(value: ": ") == true) isPvpPrimary = true;

            isPvpSecondary |= weapon2?.Item.Name.Contains(value: ": ") == true;
            isPvpArmor |= armor?.Item.Name.Contains(value: ": ") == true;

            return
                $"tc_info {Level} {Name} {fairy?.Item.Element ?? 0} {ElementRate} {(byte)Class} {(byte)Gender} {(Family != null ? $"{Family.FamilyId} {Family.Name}({Language.Instance.GetMessageFromKey(key: FamilyCharacter.Authority.ToString().ToUpper())})" : "-1 -")} {GetReputationIco()} {GetDignityIco()} {(weapon != null ? 1 : 0)} {weapon?.Rare ?? 0} {weapon?.Upgrade ?? 0} {(weapon2 != null ? 1 : 0)} {weapon2?.Rare ?? 0} {weapon2?.Upgrade ?? 0} {(armor != null ? 1 : 0)} {armor?.Rare ?? 0} {armor?.Upgrade ?? 0} {Act4Kill} {Act4Dead} {Reputation} 0 0 0 {(UseSp ? Morph : 0)} {TalentWin} {TalentLose} {TalentSurrender} 0 {MasterPoints} {Compliment} {Act4Points} {(isPvpPrimary ? 1 : 0)} {(isPvpSecondary ? 1 : 0)} {(isPvpArmor ? 1 : 0)} {HeroLevel} {(string.IsNullOrEmpty(value: Biography) ? Language.Instance.GetMessageFromKey(key: "NO_PREZ_MESSAGE") : Biography)}";
        }

        public string GenerateRest()
        {
            return $"rest 1 {CharacterId} {(IsSitting ? 1 : 0)}";
        }

        public string GenerateRevive()
        {
            var lives = MapInstance.InstanceBag.Lives - MapInstance.InstanceBag.DeadList.Count + 1;
            if (MapInstance.MapInstanceType == MapInstanceType.TimeSpaceInstance)
                lives = MapInstance.InstanceBag.Lives -
                    MapInstance.InstanceBag.DeadList.ToList().Count(predicate: s => s == CharacterId) + 1;
            return $"revive 1 {CharacterId} {(lives > 0 ? lives : 0)}";
        }

        public string GenerateSay(string message, int type, bool ignoreNickname = false)
        {
            return $"say {(ignoreNickname ? 2 : 1)} {CharacterId} {type} {message}";
        }

        public string GenerateSayItem(string message, int type, byte itemInventory, short itemSlot,
            bool ignoreNickname = false)
        {
            if (Inventory.LoadBySlotAndType(slot: itemSlot, type: (InventoryType)itemInventory) is ItemInstance item)
                return
                    $"sayitem {(ignoreNickname ? 2 : 1)} {CharacterId} {type} {message.Replace(oldChar: ' ', newChar: '^')} {(item.Item.EquipmentSlot == EquipmentType.Sp ? item.GenerateSlInfo() : item.GenerateEInfo())}";
            return "";
        }

        public string GenerateScal()
        {
            return $"char_sc 1 {CharacterId} {Size}";
        }

        public List<string> GenerateScN()
        {
            var list = new List<string>();
            byte i = 0;
            Mates.Where(predicate: s => s.MateType == MateType.Partner).ToList().ForEach(action: s =>
            {
                s.PetId = i;
                s.LoadInventory();
                list.Add(item: s.GenerateScPacket());
                i++;
            });
            return list;
        }

        public List<string> GenerateScP(byte page = 0)
        {
            var list = new List<string>();

            byte i = 0;

            Mates.Where(predicate: s => s.MateType == MateType.Pet).Skip(count: page * 10).Take(count: 10).ToList()
                .ForEach(action: s =>
                {
                    s.PetId = (byte)(page * 10 + i);
                    list.Add(item: s.GenerateScPacket());
                    i++;
                });

            return list;
        }

        public string GenerateScpStc()
        {
            return $"sc_p_stc {(MaxMateCount - 10) / 10} {MaxPartnerCount - 3}";
        }

        public string GenerateShop(string shopname)
        {
            return $"shop 1 {CharacterId} 1 3 0 {shopname}";
        }

        public string GenerateShopEnd()
        {
            return $"shop 1 {CharacterId} 0 0";
        }

        public string GenerateSki()
        {
            var ski = "ski";

            var skills = GetSkills().OrderBy(keySelector: s => s.Skill.CastId)
                .OrderBy(keySelector: s => s.SkillVNum < 200).ToList();

            if (skills.Count >= 2)
            {
                if (UseSp)
                    ski += $" {skills[index: 0].SkillVNum} {skills[index: 0].SkillVNum}";
                else
                    ski += $" {skills[index: 0].SkillVNum} {skills[index: 1].SkillVNum}";

                ski = skills.Aggregate(seed: ski,
                    func: (packet, characterSKill) => $"{packet} {characterSKill.SkillVNum}");
            }

            return ski;
        }

        public string GenerateSpk(object message, int type)
        {
            return $"spk 1 {CharacterId} {type} {Name} {message}";
        }

        public string GenerateSpPoint()
        {
            return $"sp {SpAdditionPoint} 1000000 {SpPoint} 10000";
        }

        [Obsolete(
            message:
            "GenerateStartupInventory should be used only on startup, for refreshing an inventory slot please use GenerateInventoryAdd instead.")]
        public void GenerateStartupInventory()
        {
            string inv0 = "inv 0",
                inv1 = "inv 1",
                inv2 = "inv 2",
                inv3 = "inv 3",
                inv6 = "inv 6",
                inv7 = "inv 7"; // inv 3 used for miniland objects
            if (Inventory != null)
                foreach (var inv in Inventory.GetAllItems())
                    switch (inv.Type)
                    {
                        case InventoryType.Equipment:
                            if (inv.Item.EquipmentSlot == EquipmentType.Sp)
                                inv0 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Rare}.{inv.Upgrade}.{inv.SpStoneUpgrade}";
                            else
                                inv0 +=
                                    $" {inv.Slot}.{inv.ItemVNum}.{inv.Rare}.{(inv.Item.IsColored ? inv.Design : inv.Upgrade)}.0";
                            break;

                        case InventoryType.Main:
                            inv1 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Amount}.0";
                            break;

                        case InventoryType.Etc:
                            inv2 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Amount}.0";
                            break;

                        case InventoryType.Miniland:
                            inv3 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Amount}";
                            break;

                        case InventoryType.Specialist:
                            inv6 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Rare}.{inv.Upgrade}.{inv.SpStoneUpgrade}";
                            break;

                        case InventoryType.Costume:
                            inv7 += $" {inv.Slot}.{inv.ItemVNum}.{inv.Rare}.{inv.Upgrade}.0";
                            break;
                    }

            Session.SendPacket(packet: inv0);
            Session.SendPacket(packet: inv1);
            Session.SendPacket(packet: inv2);
            Session.SendPacket(packet: inv3);
            Session.SendPacket(packet: inv6);
            Session.SendPacket(packet: inv7);
            Session.SendPacket(packet: GetMinilandObjectList());
        }

        public string GenerateStashAll()
        {
            var stash = $"stash_all {WareHouseSize}";
            foreach (var item in Inventory.Where(predicate: s => s.Type == InventoryType.Warehouse))
                stash += $" {item.GenerateStashPacket()}";
            return stash;
        }

        public string GenerateStat()
        {
            var option =
                (WhisperBlocked ? Math.Pow(x: 2, y: (int)CharacterOption.WhisperBlocked - 1) : 0)
                + (FamilyRequestBlocked ? Math.Pow(x: 2, y: (int)CharacterOption.FamilyRequestBlocked - 1) : 0)
                + (!MouseAimLock ? Math.Pow(x: 2, y: (int)CharacterOption.MouseAimLock - 1) : 0)
                + (MinilandInviteBlocked ? Math.Pow(x: 2, y: (int)CharacterOption.MinilandInviteBlocked - 1) : 0)
                + (ExchangeBlocked ? Math.Pow(x: 2, y: (int)CharacterOption.ExchangeBlocked - 1) : 0)
                + (FriendRequestBlocked ? Math.Pow(x: 2, y: (int)CharacterOption.FriendRequestBlocked - 1) : 0)
                + (EmoticonsBlocked ? Math.Pow(x: 2, y: (int)CharacterOption.EmoticonsBlocked - 1) : 0)
                + (HpBlocked ? Math.Pow(x: 2, y: (int)CharacterOption.HpBlocked - 1) : 0)
                + (BuffBlocked ? Math.Pow(x: 2, y: (int)CharacterOption.BuffBlocked - 1) : 0)
                + (GroupRequestBlocked ? Math.Pow(x: 2, y: (int)CharacterOption.GroupRequestBlocked - 1) : 0)
                + (HeroChatBlocked ? Math.Pow(x: 2, y: (int)CharacterOption.HeroChatBlocked - 1) : 0)
                + (QuickGetUp ? Math.Pow(x: 2, y: (int)CharacterOption.QuickGetUp - 1) : 0)
                + (!IsPetAutoRelive ? 64 : 0)
                + (!IsPartnerAutoRelive ? 128 : 0);
            return $"stat {Hp} {HPLoad()} {Mp} {MPLoad()} 0 {option}";
        }

        public string GenerateAdditionalHpMp()
        {
            return $"guri 4 {Math.Round(a: BattleEntity.AdditionalHp)} {Math.Round(a: BattleEntity.AdditionalMp)}";
        }

        public List<string> GenerateStatChar()
        {
            var weaponUpgrade = 0;
            var secondaryUpgrade = 0;
            var armorUpgrade = 0;
            MinHit = CharacterHelper.MinHit(@class: Class, level: Level);
            MaxHit = CharacterHelper.MaxHit(@class: Class, level: Level);
            HitRate = CharacterHelper.HitRate(@class: Class, level: Level);
            HitCriticalChance = CharacterHelper.HitCriticalRate(@class: Class, level: Level);
            HitCriticalRate = CharacterHelper.HitCritical(@class: Class, level: Level);
            SecondWeaponMinHit = CharacterHelper.MinDistance(@class: Class, level: Level);
            SecondWeaponMaxHit = CharacterHelper.MaxDistance(@class: Class, level: Level);
            SecondWeaponHitRate = CharacterHelper.DistanceRate(@class: Class, level: Level);
            SecondWeaponCriticalChance = CharacterHelper.DistCriticalRate(@class: Class, level: Level);
            SecondWeaponCriticalRate = CharacterHelper.DistCritical(@class: Class, level: Level);
            FireResistance = 0;
            LightResistance = 0;
            WaterResistance = 0;
            DarkResistance = 0;
            Defence = CharacterHelper.Defence(@class: Class, level: Level);
            DefenceRate = CharacterHelper.DefenceRate(@class: Class, level: Level);
            ElementRate = 0;
            ElementRateSP = 0;
            DistanceDefence = CharacterHelper.DistanceDefence(@class: Class, level: Level);
            DistanceDefenceRate = CharacterHelper.DistanceDefenceRate(@class: Class, level: Level);
            MagicalDefence = CharacterHelper.MagicalDefence(@class: Class, level: Level);
            if (UseSp)
            {
                // handle specialist
                var specialist = Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);
                if (specialist != null)
                {
                    MinHit += specialist.DamageMinimum + specialist.SpDamage * 10;
                    MaxHit += specialist.DamageMaximum + specialist.SpDamage * 10;
                    SecondWeaponMinHit += specialist.DamageMinimum + specialist.SpDamage * 10;
                    SecondWeaponMaxHit += specialist.DamageMaximum + specialist.SpDamage * 10;
                    HitCriticalChance += specialist.CriticalLuckRate;
                    HitCriticalRate += specialist.CriticalRate;
                    SecondWeaponCriticalChance += specialist.CriticalLuckRate;
                    SecondWeaponCriticalRate += specialist.CriticalRate;
                    HitRate += specialist.HitRate;
                    SecondWeaponHitRate += specialist.HitRate;
                    DefenceRate += specialist.DefenceDodge;
                    DistanceDefenceRate += specialist.DistanceDefenceDodge;
                    FireResistance += specialist.Item.FireResistance + specialist.SpFire;
                    WaterResistance += specialist.Item.WaterResistance + specialist.SpWater;
                    LightResistance += specialist.Item.LightResistance + specialist.SpLight;
                    DarkResistance += specialist.Item.DarkResistance + specialist.SpDark;
                    ElementRateSP += specialist.ElementRate + specialist.SpElement;
                    Defence += specialist.CloseDefence + specialist.SpDefence * 10;
                    DistanceDefence += specialist.DistanceDefence + specialist.SpDefence * 10;
                    MagicalDefence += specialist.MagicDefence + specialist.SpDefence * 10;

                    var mainWeapon = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon,
                        type: InventoryType.Wear);
                    var secondaryWeapon =
                        Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.SecondaryWeapon,
                            type: InventoryType.Wear);
                    var effects = new List<ShellEffectDto>();
                    if (mainWeapon?.ShellEffects != null) effects.AddRange(collection: mainWeapon.ShellEffects);
                    if (secondaryWeapon?.ShellEffects != null)
                        effects.AddRange(collection: secondaryWeapon.ShellEffects);

                    int GetShellWeaponEffectValue(ShellWeaponEffectType effectType)
                    {
                        return effects?.Where(predicate: s => s.Effect == (byte)effectType)
                            ?.OrderByDescending(keySelector: s => s.Value)
                            ?.FirstOrDefault()?.Value ?? 0;
                    }

                    var point = CharacterHelper.SlPoint(spPoint: specialist.SlDamage, mode: 0) +
                                GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlDamage) +
                                GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlGlobal);
                    if (point > 100) point = 100;
                    ;

                    var p = 0;
                    if (point <= 10)
                        p = point * 5;
                    else if (point <= 20)
                        p = 50 + (point - 10) * 6;
                    else if (point <= 30)
                        p = 110 + (point - 20) * 7;
                    else if (point <= 40)
                        p = 180 + (point - 30) * 8;
                    else if (point <= 50)
                        p = 260 + (point - 40) * 9;
                    else if (point <= 60)
                        p = 350 + (point - 50) * 10;
                    else if (point <= 70)
                        p = 450 + (point - 60) * 11;
                    else if (point <= 80)
                        p = 560 + (point - 70) * 13;
                    else if (point <= 90)
                        p = 690 + (point - 80) * 14;
                    else if (point <= 94)
                        p = 830 + (point - 90) * 15;
                    else if (point <= 95)
                        p = 890 + 16;
                    else if (point <= 97)
                        p = 906 + (point - 95) * 17;
                    else if (point > 97) p = 940 + (point - 97) * 20;

                    MaxHit += p;
                    MinHit += p;
                    SecondWeaponMaxHit += p;
                    SecondWeaponMinHit += p;

                    point = CharacterHelper.SlPoint(spPoint: specialist.SlDefence, mode: 1) +
                            GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlDefence) +
                            GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlGlobal);
                    if (point > 100) point = 100;
                    ;

                    p = 0;
                    if (point <= 10)
                        p = point;
                    else if (point <= 20)
                        p = 10 + (point - 10) * 2;
                    else if (point <= 30)
                        p = 30 + (point - 20) * 3;
                    else if (point <= 40)
                        p = 60 + (point - 30) * 4;
                    else if (point <= 50)
                        p = 100 + (point - 40) * 5;
                    else if (point <= 60)
                        p = 150 + (point - 50) * 6;
                    else if (point <= 70)
                        p = 210 + (point - 60) * 7;
                    else if (point <= 80)
                        p = 280 + (point - 70) * 8;
                    else if (point <= 90)
                        p = 360 + (point - 80) * 9;
                    else if (point > 90) p = 450 + (point - 90) * 10;

                    Defence += p;
                    MagicalDefence += p;
                    DistanceDefence += p;

                    point = CharacterHelper.SlPoint(spPoint: specialist.SlElement, mode: 2) +
                            GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlElement) +
                            GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlGlobal);
                    if (point > 100) point = 100;
                    ;

                    p = point <= 50 ? point : 50 + (point - 50) * 2;
                    ElementRateSP += p;

                    slhpbonus = GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.Slhp) +
                                GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.SlGlobal);
                }
            }

            // TODO: add base stats
            var weapon = Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon, type: InventoryType.Wear);
            if (weapon != null)
            {
                weaponUpgrade = weapon.Upgrade;
                MinHit += weapon.DamageMinimum + weapon.Item.DamageMinimum;
                MaxHit += weapon.DamageMaximum + weapon.Item.DamageMaximum;
                HitRate += weapon.HitRate + weapon.Item.HitRate;
                HitCriticalChance += weapon.CriticalLuckRate + weapon.Item.CriticalLuckRate;
                HitCriticalRate += weapon.CriticalRate + weapon.Item.CriticalRate;

                // maxhp-mp
            }

            var weapon2 =
                Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.SecondaryWeapon, type: InventoryType.Wear);
            if (weapon2 != null)
            {
                secondaryUpgrade = weapon2.Upgrade;
                SecondWeaponMinHit += weapon2.DamageMinimum + weapon2.Item.DamageMinimum;
                SecondWeaponMaxHit += weapon2.DamageMaximum + weapon2.Item.DamageMaximum;
                SecondWeaponHitRate += weapon2.HitRate + weapon2.Item.HitRate;
                SecondWeaponCriticalChance += weapon2.CriticalLuckRate + weapon2.Item.CriticalLuckRate;
                SecondWeaponCriticalRate += weapon2.CriticalRate + weapon2.Item.CriticalRate;

                // maxhp-mp
            }

            var armor = Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Armor, type: InventoryType.Wear);
            if (armor != null)
            {
                armorUpgrade = armor.Upgrade;
                Defence += armor.CloseDefence + armor.Item.CloseDefence;
                DefenceRate += armor.DefenceDodge + armor.Item.DefenceDodge;
                MagicalDefence += armor.MagicDefence + armor.Item.MagicDefence;
                DistanceDefence += armor.DistanceDefence + armor.Item.DistanceDefence;
                DistanceDefenceRate += armor.DistanceDefenceDodge + armor.Item.DistanceDefenceDodge;
            }

            var fairy = Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Fairy, type: InventoryType.Wear);
            if (fairy != null)
                ElementRate += fairy.ElementRate + fairy.Item.ElementRate + (IsUsingFairyBooster ? 30 : 0)
                               + GetStuffBuff(type: CardType.PixieCostumeWings,
                                   subtype: (byte)AdditionalTypes.PixieCostumeWings.IncreaseFairyElement)[0];

            for (short i = 1; i < 14; i++)
            {
                var item = Inventory?.LoadBySlotAndType(slot: i, type: InventoryType.Wear);
                if (item != null && item.Item.EquipmentSlot != EquipmentType.MainWeapon
                                 && item.Item.EquipmentSlot != EquipmentType.SecondaryWeapon
                                 && item.Item.EquipmentSlot != EquipmentType.Armor
                                 && item.Item.EquipmentSlot != EquipmentType.Sp)
                {
                    FireResistance += item.FireResistance + item.Item.FireResistance;
                    LightResistance += item.LightResistance + item.Item.LightResistance;
                    WaterResistance += item.WaterResistance + item.Item.WaterResistance;
                    DarkResistance += item.DarkResistance + item.Item.DarkResistance;
                    Defence += item.CloseDefence + item.Item.CloseDefence;
                    DefenceRate += item.DefenceDodge + item.Item.DefenceDodge;
                    MagicalDefence += item.MagicDefence + item.Item.MagicDefence;
                    DistanceDefence += item.DistanceDefence + item.Item.DistanceDefence;
                    DistanceDefenceRate += item.DistanceDefenceDodge + item.Item.DistanceDefenceDodge;
                }
            }

            //BCards
            var BCardFireResistance =
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.FireIncreased)[0] +
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
            var BCardLightResistance =
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.LightIncreased)[0] +
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
            var BCardWaterResistance =
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.WaterIncreased)[0] +
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
            var BCardDarkResistance =
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.DarkIncreased)[0] +
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];

            var BCardHitCritical =
                GetStuffBuff(type: CardType.Critical, subtype: (byte)AdditionalTypes.Critical.DamageIncreased)[0] +
                GetStuffBuff(type: CardType.Critical,
                    subtype: (byte)AdditionalTypes.Critical.DamageFromCriticalIncreased)[0];
            var BCardHitCriticalRate =
                GetStuffBuff(type: CardType.Critical, subtype: (byte)AdditionalTypes.Critical.InflictingIncreased)[0];

            var BCardHit =
                GetStuffBuff(type: CardType.AttackPower,
                    subtype: (byte)AdditionalTypes.AttackPower.AllAttacksIncreased)[0];
            var BCardSecondHit =
                GetStuffBuff(type: CardType.AttackPower,
                    subtype: (byte)AdditionalTypes.AttackPower.AllAttacksIncreased)[0];

            var BCardHitRate = GetStuffBuff(type: CardType.Target,
                subtype: (byte)AdditionalTypes.Target.AllHitRateIncreased)[0];
            var BCardSecondHitRate =
                GetStuffBuff(type: CardType.Target, subtype: (byte)AdditionalTypes.Target.AllHitRateIncreased)[0];

            var BCardMeleeDodge = GetStuffBuff(type: CardType.DodgeAndDefencePercent,
                subtype: (byte)AdditionalTypes.Target.AllHitRateIncreased)[0];
            var BCardRangeDodge = GetStuffBuff(type: CardType.DodgeAndDefencePercent,
                subtype: (byte)AdditionalTypes.Target.AllHitRateIncreased)[0];

            var BCardMeleeDefence =
                GetStuffBuff(type: CardType.Defence, subtype: (byte)AdditionalTypes.Defence.AllIncreased)[0] +
                GetStuffBuff(type: CardType.Defence, subtype: (byte)AdditionalTypes.Defence.MeleeIncreased)[0];
            var BCardRangeDefence =
                GetStuffBuff(type: CardType.Defence, subtype: (byte)AdditionalTypes.Defence.AllIncreased)[0] +
                GetStuffBuff(type: CardType.Defence, subtype: (byte)AdditionalTypes.Defence.RangedIncreased)[0];
            var BCardMagicDefence =
                GetStuffBuff(type: CardType.Defence, subtype: (byte)AdditionalTypes.Defence.AllIncreased)[0] +
                GetStuffBuff(type: CardType.Defence, subtype: (byte)AdditionalTypes.Defence.MagicalIncreased)[0];

            switch (Class)
            {
                case ClassType.Adventurer:
                case ClassType.Swordsman:
                    BCardHit += GetStuffBuff(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MeleeAttacksIncreased)[0];
                    BCardSecondHit += GetStuffBuff(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.RangedAttacksIncreased)[0];
                    BCardHitRate +=
                        GetStuffBuff(type: CardType.Target,
                            subtype: (byte)AdditionalTypes.Target.MeleeHitRateIncreased)[0];
                    BCardSecondHitRate +=
                        GetStuffBuff(type: CardType.Target,
                            subtype: (byte)AdditionalTypes.Target.RangedHitRateIncreased)[0];
                    break;

                case ClassType.Archer:
                    BCardHit += GetStuffBuff(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.RangedAttacksIncreased)[0];
                    BCardSecondHit += GetStuffBuff(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MeleeAttacksIncreased)[0];
                    BCardHitRate +=
                        GetStuffBuff(type: CardType.Target,
                            subtype: (byte)AdditionalTypes.Target.RangedHitRateIncreased)[0];
                    BCardSecondHitRate +=
                        GetStuffBuff(type: CardType.Target,
                            subtype: (byte)AdditionalTypes.Target.MeleeHitRateIncreased)[0];
                    break;

                case ClassType.Magician:
                    BCardHit += GetStuffBuff(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MagicalAttacksIncreased)[0];
                    BCardSecondHit += GetStuffBuff(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.RangedAttacksIncreased)[0];
                    BCardHitRate += GetStuffBuff(type: CardType.Target,
                        subtype: (byte)AdditionalTypes.Target.MagicalConcentrationIncreased)[0];
                    BCardSecondHitRate +=
                        GetStuffBuff(type: CardType.Target,
                            subtype: (byte)AdditionalTypes.Target.RangedHitRateIncreased)[0];
                    break;
            }

            var type = Class == ClassType.Adventurer ? (byte)0 : (byte)(Class - 1);

            var packets = new List<string>();
            packets.Add(
                item:
                $"sc {type} {(weaponUpgrade == 10 ? weaponUpgrade : weaponUpgrade + GetBuff(type: CardType.AttackPower, subtype: (byte)AdditionalTypes.AttackPower.AttackLevelIncreased)[0])} {MinHit + BCardHit} {MaxHit + BCardHit} {HitRate + BCardHitRate} {HitCriticalChance + BCardHitCriticalRate} {HitCriticalRate + BCardHitCritical} {(Class == ClassType.Archer ? 1 : 0)} {(secondaryUpgrade == 10 ? secondaryUpgrade : secondaryUpgrade + GetBuff(type: CardType.AttackPower, subtype: (byte)AdditionalTypes.AttackPower.AttackLevelIncreased)[0])} {SecondWeaponMinHit + BCardSecondHit} {SecondWeaponMaxHit + BCardSecondHit} {SecondWeaponHitRate + BCardSecondHitRate} {SecondWeaponCriticalChance + BCardHitCriticalRate} {SecondWeaponCriticalRate + BCardHitCritical} {(armorUpgrade == 10 ? armorUpgrade : armorUpgrade + GetBuff(type: CardType.Defence, subtype: (byte)AdditionalTypes.Defence.DefenceLevelIncreased)[0])} {Defence + BCardMeleeDefence} {DefenceRate + BCardMeleeDodge} {DistanceDefence + BCardRangeDefence} {DistanceDefenceRate + BCardRangeDodge} {MagicalDefence + BCardMagicDefence} {FireResistance + BCardFireResistance} {WaterResistance + BCardWaterResistance} {LightResistance + BCardLightResistance} {DarkResistance + BCardDarkResistance}");
            packets.AddRange(collection: GenerateScN());
            packets.AddRange(collection: GenerateScP());

            LoadSpeed();

            return packets;
        }

        public string GenerateStatInfo()
        {
            return
                $"st 1 {CharacterId} {Level} {HeroLevel} {(int)(Hp / (float)HPLoad() * 100)} {(int)(Mp / (float)MPLoad() * 100)} {Hp} {Mp}{Buff.GetAllItems().Where(predicate: s => !s.StaticBuff || new short[] { 339, 340 }.Contains(value: s.Card.CardId)).Aggregate(seed: "", func: (current, buff) => current + $" {buff.Card.CardId}.{buff.Level}")}";
        }

        public string GenerateTaF(byte victoriousteam)
        {
            var tm = ServerManager.Instance.ArenaTeams.ToList()
                .FirstOrDefault(predicate: s => s.Any(predicate: o => o.Session == Session));
            var score1 = 0;
            var score2 = 0;
            var life1 = 0;
            var life2 = 0;
            var call1 = 0;
            var call2 = 0;
            var atype = ArenaTeamType.Erenia;
            if (tm == null)
                return $"ta_f 0 {victoriousteam} {(byte)atype} {score1} {life1} {call1} {score2} {life2} {call2}";

            var tmem = tm.FirstOrDefault(predicate: s => s.Session == Session);
            if (tmem == null)
                return $"ta_f 0 {victoriousteam} {(byte)atype} {score1} {life1} {call1} {score2} {life2} {call2}";

            atype = tmem.ArenaTeamType;
            var ids = tm.Replace(predicate: s => tmem.ArenaTeamType == s.ArenaTeamType)
                .Select(selector: s => s.Session.Character.CharacterId);
            var oposit = tm.Replace(predicate: s => tmem.ArenaTeamType != s.ArenaTeamType);
            var own = tm.Replace(predicate: s => tmem.ArenaTeamType == s.ArenaTeamType);
            score1 = 3 - MapInstance.InstanceBag.DeadList.Count(predicate: s => ids.Contains(value: s));
            score2 = 3 - MapInstance.InstanceBag.DeadList.Count(predicate: s => !ids.Contains(value: s));
            life1 = 3 - own.Count(predicate: s => s.Dead);
            life2 = 3 - oposit.Count(predicate: s => s.Dead);
            call1 = 5 - own.Sum(selector: s => s.SummonCount);
            call2 = 5 - oposit.Sum(selector: s => s.SummonCount);
            return $"ta_f 0 {victoriousteam} {(byte)atype} {score1} {life1} {call1} {score2} {life2} {call2}";
        }

        public string GenerateTaFc(byte type)
        {
            return $"ta_fc {type} {CharacterId}";
        }

        public TalkPacket GenerateTalk(string message)
        {
            return new TalkPacket
            {
                CharacterId = CharacterId,
                Message = message
            };
        }

        public string GenerateTaM(int type)
        {
            var tm = ServerManager.Instance.ArenaTeams.ToList()
                .FirstOrDefault(predicate: s => s.Any(predicate: o => o.Session == Session));
            var score1 = 0;
            var score2 = 0;
            if (tm == null)
                return
                    $"ta_m {type} {score1} {score2} {(type == 3 ? MapInstance.InstanceBag.Clock.SecondsRemaining / 10 : 0)} 0";

            var tmem = tm.FirstOrDefault(predicate: s => s.Session == Session);
            var ids = tm.Replace(predicate: s => tmem != null && tmem.ArenaTeamType != s.ArenaTeamType)
                .Select(selector: s => s.Session.Character.CharacterId);
            score1 = MapInstance.InstanceBag.DeadList.Count(predicate: s => ids.Contains(value: s));
            score2 = MapInstance.InstanceBag.DeadList.Count(predicate: s => !ids.Contains(value: s));
            return
                $"ta_m {type} {score1} {score2} {(type == 3 ? MapInstance.InstanceBag.Clock.SecondsRemaining / 10 : 0)} 0";
        }

        public string GenerateTaP(byte tatype, bool showOponent)
        {
            var arenateam = ServerManager.Instance.ArenaTeams.ToList()
                .FirstOrDefault(predicate: s => s != null && s.Any(predicate: o => o != null && o.Session == Session))
                ?.OrderBy(keySelector: s => s.ArenaTeamType).ToList();
            var type = ArenaTeamType.Erenia;
            var groups = "";
            if (arenateam == null)
                return
                    $"ta_p {tatype} {(byte)type} {5} {5} {groups.TrimEnd(' ')}";

            type = arenateam.FirstOrDefault(predicate: s => s.Session == Session)?.ArenaTeamType ??
                   ArenaTeamType.Erenia;

            var MyTeam = arenateam.Where(predicate: s => s.ArenaTeamType == type && s.Order != null).ToList();
            var EnemyTeam = arenateam.Where(predicate: s => s.ArenaTeamType != type && s.Order != null).ToList();

            for (var i = 0; i < 3; i++)
                if (MyTeam.Where(predicate: s => s.Order == i).FirstOrDefault() is ArenaTeamMember arenamember)
                    groups +=
                        $"{(arenamember.Dead ? 0 : 1)}.{arenamember.Session.Character.CharacterId}.{(byte)arenamember.Session.Character.Class}.{(byte)arenamember.Session.Character.Gender}.{(byte)arenamember.Session.Character.Morph} ";
                else
                    groups += "-1.-1.-1.-1.-1 ";

            for (var i = 0; i < 3; i++)
                if (EnemyTeam.Where(predicate: s => s.Order == i).FirstOrDefault() is ArenaTeamMember arenamember &&
                    showOponent)
                    groups +=
                        $"{(arenamember.Dead ? 0 : 1)}.{arenamember.Session.Character.CharacterId}.{(byte)arenamember.Session.Character.Class}.{(byte)arenamember.Session.Character.Gender}.{(byte)arenamember.Session.Character.Morph} ";
                else
                    groups += "-1.-1.-1.-1.-1 ";

            return
                $"ta_p {tatype} {(byte)type} {5 - arenateam.Where(predicate: s => s.ArenaTeamType == type).Sum(selector: s => s.SummonCount)} {5 - arenateam.Where(predicate: s => s.ArenaTeamType != type).Sum(selector: s => s.SummonCount)} {groups.TrimEnd(' ')}";
        }

        public string GenerateTaPs()
        {
            var arenateam = ServerManager.Instance.ArenaTeams.ToList()
                .FirstOrDefault(predicate: s => s != null && s.Any(predicate: o => o?.Session == Session))
                ?.OrderBy(keySelector: s => s.ArenaTeamType)
                .ToList();
            var groups = "";
            if (arenateam == null) return $"ta_ps {groups.TrimEnd(' ')}";

            var type = arenateam.FirstOrDefault(predicate: s => s.Session == Session)?.ArenaTeamType ??
                       ArenaTeamType.Erenia;

            var MyTeam = arenateam.Where(predicate: s => s.ArenaTeamType == type && s.Order != null).ToList();
            var EnemyTeam = arenateam.Where(predicate: s => s.ArenaTeamType != type && s.Order != null).ToList();

            for (var i = 0; i < 3; i++)
                if (MyTeam.Where(predicate: s => s.Order == i).FirstOrDefault() is ArenaTeamMember arenamember)
                    groups +=
                        $"{arenamember.Session.Character.CharacterId}.{(int)(arenamember.Session.Character.Hp / arenamember.Session.Character.HPLoad() * 100)}.{(int)(arenamember.Session.Character.Mp / arenamember.Session.Character.MPLoad() * 100)}.0 ";
                else
                    groups += "-1.-1.-1.-1.-1 ";

            for (var i = 0; i < 3; i++)
                if (EnemyTeam.Where(predicate: s => s.Order == i).FirstOrDefault() is ArenaTeamMember arenamember)
                    groups +=
                        $"{arenamember.Session.Character.CharacterId}.{(int)(arenamember.Session.Character.Hp / arenamember.Session.Character.HPLoad() * 100)}.{(int)(arenamember.Session.Character.Mp / arenamember.Session.Character.MPLoad() * 100)}.0 ";
                else
                    groups += "-1.-1.-1.-1.-1 ";

            return $"ta_ps {groups.TrimEnd(' ')}";
        }

        public string GenerateTit()
        {
            return
                $"tit {Language.Instance.GetMessageFromKey(key: Class == (byte)ClassType.Adventurer ? nameof(ClassType.Adventurer).ToUpper() : Class == ClassType.Swordsman ? nameof(ClassType.Swordsman).ToUpper() : Class == ClassType.Archer ? nameof(ClassType.Archer).ToUpper() : nameof(ClassType.Magician).ToUpper())} {Name}";
        }

        public string GenerateTp()
        {
            return BattleEntity.GenerateTp();
        }

        public void GetAct4Points(int point)
        {
            //RefreshComplimentRankingIfNeeded();
            Act4Points += point;
        }

        public int GetCP()
        {
            var cpmax = (Class > 0 ? 40 : 0) + JobLevel * 2;
            var cpused = 0;
            foreach (var ski in Skills.GetAllItems()) cpused += ski.Skill.CpCost;
            return cpmax - cpused;
        }

        public int GetDignityIco()
        {
            var icoDignity = 1;

            if (Dignity <= -100) icoDignity = 2;
            if (Dignity <= -200) icoDignity = 3;
            if (Dignity <= -400) icoDignity = 4;
            if (Dignity <= -600) icoDignity = 5;
            if (Dignity <= -800) icoDignity = 6;

            return icoDignity;
        }

        public void GetDir(int pX, int pY, int nX, int nY)
        {
            BeforeDirection = Direction;
            if (pX == nX && pY < nY)
                Direction = 2;
            else if (pX > nX && pY == nY)
                Direction = 3;
            else if (pX == nX && pY > nY)
                Direction = 0;
            else if (pX < nX && pY == nY)
                Direction = 1;
            else if (pX < nX && pY < nY)
                Direction = 6;
            else if (pX > nX && pY < nY)
                Direction = 7;
            else if (pX > nX && pY > nY)
                Direction = 4;
            else if (pX < nX && pY > nY) Direction = 5;
        }

        public List<Portal> GetExtraPortal()
        {
            return new List<Portal>(collection: MapInstancePortalHandler
                .GenerateMinilandEntryPortals(entryMap: MapInstance.Map.MapId,
                    exitMapinstanceId: Miniland.MapInstanceId)
                .Concat(second: Family?.Act4Raid != null
                    ? MapInstancePortalHandler.GenerateAct4EntryPortals(entryMap: MapInstance.Map.MapId)
                    : new List<Portal>()));
        }

        public List<string> GetFamilyHistory()
        {
            //TODO: Fix some bugs(missing history etc)
            if (Family != null)
            {
                const string packetheader = "ghis";
                var packetList = new List<string>();
                var packet = "";
                var i = 0;
                var amount = 0;
                foreach (var log in Family.FamilyLogs
                    .Where(predicate: s => s.FamilyLogType != FamilyLogType.WareHouseAdded &&
                                           s.FamilyLogType != FamilyLogType.WareHouseRemoved)
                    .OrderByDescending(keySelector: s => s.Timestamp)
                    .Take(count: 100))
                {
                    packet +=
                        $" {(byte)log.FamilyLogType}|{log.FamilyLogData}|{(int)(DateTime.Now - log.Timestamp).TotalHours}";
                    i++;
                    if (i == 50)
                    {
                        i = 0;
                        packetList.Add(item: packetheader + (amount == 0 ? " 0 " : "") + packet);
                        amount++;
                    }
                    else if (i + 50 * amount == Family.FamilyLogs.Count)
                    {
                        packetList.Add(item: packetheader + (amount == 0 ? " 0 " : "") + packet);
                    }
                }

                return packetList;
            }

            return new List<string>();
        }

        public IEnumerable<string> GetMinilandEffects()
        {
            return MinilandObjects.Select(selector: mp => mp.GenerateMinilandEffect(removed: false)).ToList();
        }

        public void GetGold(long val, bool isQuest = false)
        {
            Session.Character.Gold += val;
            if (Session.Character.Gold > ServerManager.Instance.Configuration.MaxGold)
            {
                Session.Character.Gold = ServerManager.Instance.Configuration.MaxGold;
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "MAX_GOLD"), type: 0));
            }

            Session.SendPacket(packet: isQuest
                ? GenerateSay(message: $"Quest reward: [ {ServerManager.GetItem(vnum: 1046).Name} x {val} ]", type: 10)
                : Session.Character.GenerateSay(
                    message:
                    $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {ServerManager.GetItem(vnum: 1046).Name} x {val}",
                    type: 10));
            Session.SendPacket(packet: Session.Character.GenerateGold());
        }

        public string GetMinilandObjectList()
        {
            var mlobjstring = "mlobjlst";
            foreach (var item in Inventory.Where(predicate: s => s.Type == InventoryType.Miniland)
                .OrderBy(keySelector: s => s.Slot))
            {
                var mp = MinilandObjects.Find(match: s => s.ItemInstanceId == item.Id);
                var used = mp != null;
                mlobjstring +=
                    $" {item.Slot}.{(used ? 1 : 0)}.{(used ? mp.MapX : 0)}.{(used ? mp.MapY : 0)}.{(item.Item.Width != 0 ? item.Item.Width : 1)}.{(item.Item.Height != 0 ? item.Item.Height : 1)}.{(used ? mp.ItemInstance.DurabilityPoint : 0)}.100.0.1";
            }

            return mlobjstring;
        }

        public void GetReferrerReward()
        {
            var referrerId = Session.Account.ReferrerId;
            if (Level >= 70 && referrerId != 0 && !CharacterId.Equals(obj: referrerId))
            {
                var logs = DaoFactory.GeneralLogDao.LoadByLogType(logType: "ReferralProgram", characterId: null).Where(
                    predicate: g =>
                        g.IpAddress.Equals(value: Session.Account.RegistrationIp.Split(':')[1]
                            .Replace(oldValue: "//", newValue: ""))).ToList();
                if (logs.Count <= 5)
                {
                    var character = DaoFactory.CharacterDao.LoadById(characterId: referrerId);
                    if (character == null || character.Level < 70) return;
                    var referrer = DaoFactory.AccountDao.LoadById(accountId: character.AccountId);
                    if (referrer != null && !AccountId.Equals(obj: character.AccountId))
                    {
                        Logger.LogUserEvent(logEvent: "REFERRERREWARD", caller: Session.GenerateIdentity(),
                            data: $"AccountId: {AccountId} ReferrerId: {referrerId}");
                        DaoFactory.AccountDao.WriteGeneralLog(accountId: AccountId,
                            ipAddress: Session.Account.RegistrationIp, characterId: CharacterId,
                            logType: GeneralLogType.ReferralProgram, logData: $"ReferrerId: {referrerId}");

                        // send gifts like you want
                        //SendGift(CharacterId, 5910, 1, 0, 0, false);
                        //SendGift(referrerId, 5910, 1, 0, 0, false);
                    }
                }
            }
        }

        public int GetReputationIco()
        {
            if (Reputation >= 5000001)
                switch (IsReputationHero())
                {
                    case 1:
                        return 28;

                    case 2:
                        return 29;

                    case 3:
                        return 30;

                    case 4:
                        return 31;

                    case 5:
                        return 32;
                }

            if (Reputation <= 50) return 1;

            if (Reputation <= 150) return 2;

            if (Reputation <= 250) return 3;

            if (Reputation <= 500) return 4;

            if (Reputation <= 750) return 5;

            if (Reputation <= 1000) return 6;

            if (Reputation <= 2250) return 7;

            if (Reputation <= 3500) return 8;

            if (Reputation <= 5000) return 9;

            if (Reputation <= 9500) return 10;

            if (Reputation <= 19000) return 11;

            if (Reputation <= 25000) return 12;

            if (Reputation <= 40000) return 13;

            if (Reputation <= 60000) return 14;

            if (Reputation <= 85000) return 15;

            if (Reputation <= 115000) return 16;

            if (Reputation <= 150000) return 17;

            if (Reputation <= 190000) return 18;

            if (Reputation <= 235000) return 19;

            if (Reputation <= 285000) return 20;

            if (Reputation <= 350000) return 21;

            if (Reputation <= 500000) return 22;

            if (Reputation <= 1500000) return 23;

            if (Reputation <= 2500000) return 24;

            if (Reputation <= 3750000) return 25;

            return Reputation <= 5000000 ? 26 : 27;
        }

        /// <summary>
        ///     Get Stuff Buffs Useful for Stats for example
        /// </summary>
        /// <param name="type"></param>
        /// <param name="subtype"></param>
        /// <returns></returns>
        public int[] GetStuffBuff(CardType type, byte subtype)
        {
            var result = new int[2] { 0, 0 };

            var bcards = new List<BCard>();

            if (Skills != null)
            {
                var passiveSkillBCards =
                    PassiveSkillHelper.Instance.PassiveSkillToBCards(
                        skills: Skills.Where(predicate: s => s?.Skill?.SkillType == 0));

                if (passiveSkillBCards.Any()) bcards.AddRange(collection: passiveSkillBCards);
            }

            var equipmentBCards = EquipmentBCards.ToList();

            if (equipmentBCards.Any()) bcards.AddRange(collection: equipmentBCards);

            foreach (var bcard in bcards.Where(predicate: s =>
                s?.Type == (byte)type && s.SubType == (byte)(subtype / 10) && s.FirstData > 0))
            {
                result[0] += bcard.IsLevelScaled ? bcard.FirstData * Level : bcard.FirstData;
                result[1] += bcard.SecondData;
            }

            return result;
        }

        public void GetXp(long val, bool applyRate = true)
        {
            if (Level >= ServerManager.Instance.Configuration.MaxLevel) return;

            LevelXp += val * (applyRate ? ServerManager.Instance.Configuration.RateXp : 1) *
                       (int)(1 + GetBuff(type: CardType.Item, subtype: (byte)AdditionalTypes.Item.ExpIncreased)[0] /
                           100D);
            GenerateLevelXpLevelUp();
            Session.SendPacket(packet: GenerateLev());
        }

        public void GetJobExp(long val, bool applyRate = true)
        {
            val *= applyRate ? ServerManager.Instance.Configuration.RateXp : 1;
            ItemInstance SpInstance = null;
            if (Inventory != null)
                SpInstance = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);
            if (UseSp && SpInstance != null)
            {
                if (SpInstance.SpLevel >= ServerManager.Instance.Configuration.MaxSpLevel) return;
                var multiplier = SpInstance.SpLevel < 10 ? 10 : SpInstance.SpLevel < 19 ? 5 : 1;
                SpInstance.Xp += (int)(val * (multiplier +
                                               GetBuff(type: CardType.Item,
                                                   subtype: (byte)AdditionalTypes.Item.ExpIncreased)[0] /
                                               100D + GetBuff(type: CardType.Item,
                                                   subtype: (byte)AdditionalTypes.Item.IncreaseSpxp)[
                                                   0] / 100D));
                GenerateSpXpLevelUp(specialist: SpInstance);
                return;
            }

            if (JobLevel >= ServerManager.Instance.Configuration.MaxJobLevel) return;
            JobLevelXp +=
                (int)(val * (1 + GetBuff(type: CardType.Item, subtype: (byte)AdditionalTypes.Item.ExpIncreased)[0] /
                    100D));
            GenerateJobXpLevelUp();
            Session.SendPacket(packet: GenerateLev());
        }

        public void GiftAdd(short itemVNum, short amount, byte rare = 0, byte upgrade = 0, short design = 0,
            bool forceRandom = false, byte minRare = 0)
        {
            if (Inventory != null)
                lock (Inventory)
                {
                    var newItem =
                        Inventory.InstantiateItemInstance(vnum: itemVNum, ownerId: CharacterId, amount: amount);
                    if (newItem.Item == null)
                        Logger.LogEventError(logEvent: "GIFT_ADD_ERROR", data: $"Item VNum {itemVNum} doesn't exist");
                    if (newItem != null)
                    {
                        newItem.Design = design;

                        if (newItem.Item.ItemType == ItemType.Armor || newItem.Item.ItemType == ItemType.Weapon ||
                            newItem.Item.ItemType == ItemType.Shell || forceRandom)
                        {
                            if (rare != 0 && !forceRandom)
                            {
                                newItem.RarifyItem(session: Session, mode: RarifyMode.Drop,
                                    protection: RarifyProtection.None,
                                    forceRare: rare);
                                newItem.Upgrade = (byte)(newItem.Item.BasicUpgrade + upgrade);
                                if (newItem.Upgrade > 10) newItem.Upgrade = 10;
                            }
                            else if (rare == 0 || forceRandom)
                            {
                                do
                                {
                                    try
                                    {
                                        newItem.RarifyItem(session: Session, mode: RarifyMode.Drop,
                                            protection: RarifyProtection.None);
                                        newItem.Upgrade = newItem.Item.BasicUpgrade;
                                        if (newItem.Rare >= minRare) break;
                                    }
                                    catch
                                    {
                                        break;
                                    }
                                } while (forceRandom);
                            }
                        }

                        if (newItem.Item.Type.Equals(obj: InventoryType.Equipment) && rare != 0 && !forceRandom)
                        {
                            newItem.Rare = (sbyte)rare;
                            newItem.SetRarityPoint();
                        }

                        if (newItem.Item.ItemType == ItemType.Shell)
                            newItem.Upgrade = (byte)ServerManager.RandomNumber(min: 50, max: 81);

                        if (newItem.Item.EquipmentSlot == EquipmentType.Gloves ||
                            newItem.Item.EquipmentSlot == EquipmentType.Boots)
                        {
                            newItem.Upgrade = upgrade;
                            newItem.DarkResistance = (short)(newItem.Item.DarkResistance * upgrade);
                            newItem.LightResistance = (short)(newItem.Item.LightResistance * upgrade);
                            newItem.WaterResistance = (short)(newItem.Item.WaterResistance * upgrade);
                            newItem.FireResistance = (short)(newItem.Item.FireResistance * upgrade);
                        }

                        var newInv = Inventory.AddToInventory(newItem: newItem);
                        if (newInv.Count > 0)
                        {
                            Session.SendPacket(packet: GenerateSay(
                                message:
                                $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {newItem.Item.Name} x {amount}",
                                type: 10));
                        }
                        else if (MailList.Count(predicate: s => s.Value.AttachmentVNum != null) < 40)
                        {
                            SendGift(id: CharacterId, vnum: itemVNum, amount: amount, rare: newItem.Rare,
                                upgrade: newItem.Upgrade, design: newItem.Design,
                                isNosmall: false);
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "PACKET_ARRIVED"),
                                    arg0: $"{newItem.Item.Name} x {amount}"), type: 0));
                        }
                    }
                }
        }

        public bool HaveBackpack()
        {
            return StaticBonusList.Any(predicate: s => s.StaticBonusType == StaticBonusType.BackPack);
        }

        public void IncreaseDollars(int amount)
        {
            try
            {
                if (!ServerManager.Instance.MallApi.SendCurrencyAsync(
                    apiKey: ServerManager.Instance.Configuration.MallApiKey,
                    accountId: Session.Account.AccountId, amount: amount).Result)
                {
                    Session.SendPacket(
                        packet: GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "MALL_ACCOUNT_NOT_EXISTING"), type: 10));
                    return;
                }
            }
            catch
            {
                Session.SendPacket(
                    packet: GenerateSay(message: Language.Instance.GetMessageFromKey(key: "MALL_UNKNOWN_ERROR"),
                        type: 10));
                return;
            }

            Session.SendPacket(
                packet: GenerateSay(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MALL_CURRENCY_RECEIVE"),
                        arg0: amount), type: 10));
        }

        public void Initialize()
        {
            _random = new Random();
            ExchangeInfo = null;
            SpCooldown = 30;
            SaveX = 0;
            SaveY = 0;
            LastDefence = DateTime.Now.AddSeconds(value: -21);
            LastDelay = DateTime.Now.AddSeconds(value: -5);
            LastHealth = DateTime.Now;
            LastEffect = DateTime.Now;
            Session = null;
            MailList = new Dictionary<int, MailDto>();
            BattleEntity = new BattleEntity(character: this, skill: null);
            Group = null;
            GmPvtBlock = false;
        }

        public static void InsertOrUpdatePenalty(PenaltyLogDto log)
        {
            DaoFactory.PenaltyLogDao.InsertOrUpdate(log: ref log);
            CommunicationServiceClient.Instance.RefreshPenalty(penaltyId: log.PenaltyLogId);
        }

        public bool IsBlockedByCharacter(long characterId)
        {
            return CharacterRelations.Any(predicate: b =>
                b.RelationType == CharacterRelationType.Blocked && b.CharacterId.Equals(obj: characterId) &&
                characterId != CharacterId);
        }

        public bool IsBlockingCharacter(long characterId)
        {
            return CharacterRelations.Any(predicate: c =>
                c.RelationType == CharacterRelationType.Blocked && c.RelatedCharacterId.Equals(obj: characterId));
        }

        public bool IsCoupleOfCharacter(long characterId)
        {
            return CharacterRelations.Any(predicate: c =>
                characterId != CharacterId && c.RelationType == CharacterRelationType.Spouse &&
                (c.RelatedCharacterId.Equals(obj: characterId) || c.CharacterId.Equals(obj: characterId)));
        }

        public bool IsFriendlistFull()
        {
            return CharacterRelations.Where(predicate: s =>
                    s.RelationType == CharacterRelationType.Friend ||
                    s.RelationType == CharacterRelationType.Spouse)
                .ToList().Count >= 80;
        }

        public bool IsFriendOfCharacter(long characterId)
        {
            return CharacterRelations.Any(predicate: c =>
                characterId != CharacterId &&
                (c.RelationType == CharacterRelationType.Friend || c.RelationType == CharacterRelationType.Spouse) &&
                (c.RelatedCharacterId.Equals(obj: characterId) || c.CharacterId.Equals(obj: characterId)));
        }

        public bool IsMarried
        {
            get => CharacterRelations.Any(predicate: c => c.RelationType == CharacterRelationType.Spouse);
        }

        /// <summary>
        ///     Checks if the current character is in range of the given position
        /// </summary>
        /// <param name="xCoordinate">The x coordinate of the object to check.</param>
        /// <param name="yCoordinate">The y coordinate of the object to check.</param>
        /// <param name="range">The range of the coordinates to be maximal distanced.</param>
        /// <returns>True if the object is in Range, False if not.</returns>
        public bool IsInRange(int xCoordinate, int yCoordinate, int range = 50)
        {
            return Map.GetDistance(p: new MapCell
            {
                X = (short)xCoordinate,
                Y = (short)yCoordinate
            }, q: new MapCell
            {
                X = PositionX,
                Y = PositionY
            }) <= range;
        }

        public bool IsMuted()
        {
            return Session.Account.PenaltyLogs.Any(predicate: s =>
                s.Penalty == PenaltyType.Muted && s.DateEnd > DateTime.Now);
        }

        public int IsReputationHero()
        {
            var i = 0;

            foreach (var character in ServerManager.Instance.TopReputation)
            {
                i++;

                if (character.CharacterId == CharacterId)
                {
                    if (i == 1) return 5;

                    if (i == 2) return 4;

                    if (i == 3) return 3;

                    if (i <= 13) return 2;

                    if (i <= 43) return 1;
                }
            }

            return 0;
        }

        public void LearnAdventurerSkills(bool isCommand = false)
        {
            if (Class == 0)
            {
                var hasLearnedNewSkill = false;

                for (short skillVNum = 200; skillVNum <= 210; skillVNum++)
                {
                    var skill = ServerManager.GetSkill(skillVNum: skillVNum);

                    if (skill?.Class == 0 && JobLevel >= skill.LevelMinimum &&
                        !Skills.Any(predicate: s => s.SkillVNum == skillVNum))
                    {
                        hasLearnedNewSkill = true;

                        Skills[key: skillVNum] = new CharacterSkill
                        {
                            SkillVNum = skillVNum,
                            CharacterId = CharacterId
                        };
                    }
                }

                if (!isCommand && hasLearnedNewSkill)
                {
                    Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "SKILL_LEARNED"), type: 0));
                    Session.SendPacket(packet: GenerateSki());
                    Session.SendPackets(packets: GenerateQuicklist());
                }
            }
        }

        public void LearnSPSkill()
        {
            ItemInstance specialist = null;

            if (Inventory != null)
                specialist = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);

            var SkillSpCount = (byte)SkillsSp.Count;

            SkillsSp = new ThreadSafeSortedList<int, CharacterSkill>();

            foreach (var ski in ServerManager.GetAllSkill())
                if (specialist != null && ski.Class == Morph + 31 && specialist.SpLevel >= ski.LevelMinimum)
                    SkillsSp[key: ski.SkillVNum] = new CharacterSkill
                    { SkillVNum = ski.SkillVNum, CharacterId = CharacterId };

            if (SkillsSp.Count != SkillSpCount)
                Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "SKILL_LEARNED"),
                    type: 0));
        }

        public void LeaveIceBreaker()
        {
            if (IceBreaker.AlreadyFrozenPlayers != null && IceBreaker.AlreadyFrozenPlayers.Contains(item: Session))
                IceBreaker.AlreadyFrozenPlayers.Remove(item: Session);

            if (IceBreaker.FrozenPlayers != null && IceBreaker.FrozenPlayers.Contains(item: Session))
            {
                IceBreaker.FrozenPlayers.Remove(item: Session);
                NoMove = false;
                NoAttack = false;
                Session.SendPacket(packet: GenerateCond());
            }
        }

        public void LeaveTalentArena(bool surrender = false)
        {
            lock (ServerManager.Instance.ArenaTeams)
            {
                var memb = ServerManager.Instance.ArenaMembers.ToList()
                    .FirstOrDefault(predicate: s => s.Session == Session);
                if (memb != null)
                {
                    if (memb.GroupId != null)
                        ServerManager.Instance.ArenaMembers.ToList().Where(predicate: s => s.GroupId == memb.GroupId)
                            .ToList()
                            .ForEach(action: s =>
                            {
                                if (ServerManager.Instance.ArenaMembers.ToList()
                                    .Count(predicate: g => g.GroupId == memb.GroupId) == 2) s.GroupId = null;

                                s.Time = 300;
                                s.Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateBsInfo(mode: 1, title: 2, time: s.Time,
                                        text: 8));
                                s.Session.SendPacket(
                                    packet: s.Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "ARENA_TEAM_LEAVE"),
                                        type: 11));
                            });

                    ServerManager.Instance.ArenaMembers.Remove(item: memb);
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateBsInfo(mode: 2, title: 2, time: 0, text: 0));
                }

                var tm = ServerManager.Instance.ArenaTeams.ToList()
                    .FirstOrDefault(predicate: s => s.Any(predicate: o => o.Session == Session));
                Session.SendPacket(packet: Session.Character.GenerateTaM(type: 1));
                if (tm == null) return;

                var tmem = tm.FirstOrDefault(predicate: s => s.Session == Session);
                if (tmem != null)
                {
                    tmem.Dead = true;
                    if (surrender) Session.Character.TalentSurrender++;

                    Session.SendPacket(packet: Session.Character.GenerateTaP(tatype: 1, showOponent: true));
                    Session.SendPacket(packet: "ta_sv 1");
                    Session.SendPacket(packet: "taw_sv 1");
                }

                if (UseSp)
                    SkillsSp.ForEach(action: c => c.LastUse = DateTime.Now.AddDays(value: -1));
                else
                    Skills.ForEach(action: c => c.LastUse = DateTime.Now.AddDays(value: -1));
                Session.SendPacket(packet: GenerateSki());
                Session.SendPackets(packets: GenerateQuicklist());

                var bufftodisable = new List<BuffType> { BuffType.Bad };
                Session.Character.DisableBuffs(types: bufftodisable);
                Session.Character.RemoveBuff(cardId: 491);

                Session.Character.Hp = (int)Session.Character.HPLoad();
                Session.Character.Mp = (int)Session.Character.MPLoad();
                ServerManager.Instance.ArenaTeams.Remove(item: tm);
                tm.RemoveWhere(predicate: s => s.Session != Session, queueReturned: out tm);
                if (tm.Any()) ServerManager.Instance.ArenaTeams.Add(item: tm);

                tm.ToList().ForEach(action: s =>
                {
                    if (s.ArenaTeamType == tmem.ArenaTeamType)
                        s.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "ARENA_TALENT_LEFT"),
                                arg0: Session.Character.Name), type: 0));

                    s.Session.SendPacket(packet: s.Session.Character.GenerateTaP(tatype: 2, showOponent: true));
                });
            }
        }

        public void LoadInventory()
        {
            IEnumerable<ItemInstanceDto> inventories = DaoFactory.ItemInstanceDao
                .LoadByCharacterId(characterId: CharacterId)
                .Where(predicate: s => s.Type != InventoryType.FamilyWareHouse).ToList();
            var characters = DaoFactory.CharacterDao.LoadAllByAccount(accountId: Session.Account.AccountId);
            IEnumerable<Guid> warehouseInventoryIds = new List<Guid>();
            foreach (var character in characters.Where(predicate: s => s.CharacterId != CharacterId))
            {
                IEnumerable<ItemInstanceDto> characterWarehouseInventory = DaoFactory.ItemInstanceDao
                    .LoadByCharacterId(characterId: character.CharacterId)
                    .Where(predicate: s => s.Type == InventoryType.Warehouse).ToList();
                inventories = inventories.Concat(second: characterWarehouseInventory);
                warehouseInventoryIds =
                    warehouseInventoryIds.Concat(second: characterWarehouseInventory.Select(selector: i => i.Id)
                        .ToList());
            }

            DaoFactory.ItemInstanceDao.DeleteGuidList(guids: warehouseInventoryIds);

            Inventory = new Inventory(Character: this);
            foreach (var inventory in inventories)
            {
                inventory.CharacterId = CharacterId;
                Inventory[key: inventory.Id] = new ItemInstance(input: inventory);
                var iteminstance = inventory as ItemInstance;
                iteminstance?.ShellEffects.Clear();
                iteminstance?.ShellEffects.AddRange(
                    collection: DaoFactory.ShellEffectDao.LoadByEquipmentSerialId(id: iteminstance.EquipmentSerialId));
            }

            var ring = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Ring, type: InventoryType.Wear);
            var bracelet = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Bracelet, type: InventoryType.Wear);
            var necklace = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Necklace, type: InventoryType.Wear);
            CellonOptions.Clear();
            if (ring != null) CellonOptions.AddRange(value: ring.CellonOptions);
            if (bracelet != null) CellonOptions.AddRange(value: bracelet.CellonOptions);
            if (necklace != null) CellonOptions.AddRange(value: necklace.CellonOptions);
        }

        public void LoadQuicklists()
        {
            QuicklistEntries = new List<QuicklistEntryDto>();
            IEnumerable<QuicklistEntryDto> quicklistDTO =
                DaoFactory.QuicklistEntryDao.LoadByCharacterId(characterId: CharacterId).ToList();
            foreach (var qle in quicklistDTO) QuicklistEntries.Add(item: qle);
        }

        public void LoadSentMail()
        {
            foreach (var mail in DaoFactory.MailDao.LoadSentByCharacter(characterId: CharacterId))
            {
                MailList.Add(key: (MailList.Count > 0 ? MailList.OrderBy(keySelector: s => s.Key).Last().Key : 0) + 1,
                    value: mail);

                Session.SendPacket(packet: GeneratePost(mail: mail, type: 2));
            }
        }

        public void LoadSkills()
        {
            Skills = new ThreadSafeSortedList<int, CharacterSkill>();
            IEnumerable<CharacterSkillDto> characterskillDTO =
                DaoFactory.CharacterSkillDao.LoadByCharacterId(characterId: CharacterId).ToList();
            foreach (var characterskill in characterskillDTO.OrderBy(keySelector: s => s.SkillVNum))
                if (!Skills.ContainsKey(key: characterskill.SkillVNum))
                    Skills[key: characterskill.SkillVNum] = new CharacterSkill(input: characterskill);
        }

        public void LoadSpeed()
        {
            lock (SpeedLockObject)
            {
                // only load speed if you dont use custom speed
                if (!IsVehicled && !IsCustomSpeed)
                {
                    Speed = CharacterHelper.SpeedData[(byte)Class];

                    if (UseSp)
                    {
                        var specialist =
                            Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);

                        if (specialist?.Item != null) Speed += specialist.Item.Speed;
                    }

                    var fixSpeed =
                        (byte)GetBuff(type: CardType.Move, subtype: (byte)AdditionalTypes.Move.SetMovement)[0];

                    if (fixSpeed != 0)
                    {
                        Speed = fixSpeed;
                    }
                    else
                    {
                        Speed += (byte)GetBuff(type: CardType.Move,
                            subtype: (byte)AdditionalTypes.Move.MovementSpeedIncreased)[0];
                        Speed = (byte)(Speed * (1 + GetBuff(type: CardType.Move,
                                subtype: (byte)AdditionalTypes.Move.MoveSpeedIncreased)[0] /
                            100D));
                    }
                }

                if (IsShopping)
                {
                    Speed = 0;
                    IsCustomSpeed = false;
                    return;
                }

                // reload vehicle speed after opening an shop for instance
                if (IsVehicled && !IsCustomSpeed)
                {
                    Speed = VehicleSpeed;

                    if (VehicleItem != null)
                    {
                        if (MapInstance?.Map?.MapTypes != null && VehicleItem.MapSpeedBoost != null &&
                            VehicleItem.ActSpeedBoost != null)
                        {
                            Speed += VehicleItem.MapSpeedBoost[MapInstance.Map.MapId];
                            if (MapInstance.Map.MapTypes.Any(predicate: s => new[]
                            {
                                (short) MapTypeEnum.Act1, (short) MapTypeEnum.CometPlain, (short) MapTypeEnum.Mine1,
                                (short) MapTypeEnum.Mine2, (short) MapTypeEnum.MeadowOfMine,
                                (short) MapTypeEnum.SunnyPlain, (short) MapTypeEnum.Fernon, (short) MapTypeEnum.FernonF,
                                (short) MapTypeEnum.Cliff
                            }.Contains(value: s.MapTypeId)))
                                Speed += VehicleItem.ActSpeedBoost[1];
                            else if (MapInstance.Map.MapTypes.Any(predicate: s =>
                                s.MapTypeId == (short)MapTypeEnum.Act2))
                                Speed += VehicleItem.ActSpeedBoost[2];
                            else if (MapInstance.Map.MapTypes.Any(predicate: s =>
                                s.MapTypeId == (short)MapTypeEnum.Act3))
                                Speed += VehicleItem.ActSpeedBoost[3];
                            else if (MapInstance.Map.MapTypes.Any(predicate: s =>
                                s.MapTypeId == (short)MapTypeEnum.Act4))
                                Speed += VehicleItem.ActSpeedBoost[4];
                            else if (MapInstance.Map.MapTypes.Any(predicate: s =>
                                s.MapTypeId == (short)MapTypeEnum.Act51))
                                Speed += VehicleItem.ActSpeedBoost[51];
                            else if (MapInstance.Map.MapTypes.Any(predicate: s =>
                                s.MapTypeId == (short)MapTypeEnum.Act52))
                                Speed += VehicleItem.ActSpeedBoost[52];
                        }

                        if (HasBuff(type: CardType.Move, subtype: (byte)AdditionalTypes.Move.TempMaximized))
                            Speed += VehicleItem.SpeedBoost;
                    }
                }
            }
        }

        public bool MuteMessage()
        {
            var penalty = Session.Account.PenaltyLogs.OrderByDescending(keySelector: s => s.DateEnd).FirstOrDefault();

            if (IsMuted() && penalty != null)
            {
                Session.CurrentMapInstance?.Broadcast(packet: Gender == GenderType.Female
                    ? GenerateSay(message: Language.Instance.GetMessageFromKey(key: "MUTED_FEMALE"), type: 1)
                    : GenerateSay(message: Language.Instance.GetMessageFromKey(key: "MUTED_MALE"), type: 1));
                Session.SendPacket(packet: GenerateSay(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                        arg0: (penalty.DateEnd - DateTime.Now).ToString(format: @"hh\:mm\:ss")), type: 11));
                Session.SendPacket(packet: GenerateSay(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MUTE_TIME"),
                        arg0: (penalty.DateEnd - DateTime.Now).ToString(format: @"hh\:mm\:ss")), type: 12));
                return true;
            }

            return false;
        }

        public string OpenFamilyWarehouse()
        {
            if (Family == null || Family.WarehouseSize == 0)
                return UserInterfaceHelper.GenerateInfo(
                    message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_WAREHOUSE"));
            return GenerateFStashAll();
        }

        public List<string> OpenFamilyWarehouseHist()
        {
            var packetList = new List<string>();
            if (Family == null || !(FamilyCharacter.Authority == FamilyAuthority.Head
                                    || FamilyCharacter.Authority == FamilyAuthority.Familydeputy
                                    || FamilyCharacter.Authority == FamilyAuthority.Member && Family.MemberCanGetHistory
                                    || FamilyCharacter.Authority == FamilyAuthority.Familykeeper &&
                                    Family.ManagerCanGetHistory))
            {
                packetList.Add(
                    item: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_RIGHT")));
                return packetList;
            }

            return GenerateFamilyWarehouseHist();
        }

        public void LoadMail()
        {
            int parcel = 0, letter = 0;
            foreach (var mail in DaoFactory.MailDao.LoadSentToCharacter(characterId: CharacterId))
            {
                MailList.Add(key: (MailList.Count > 0 ? MailList.OrderBy(keySelector: s => s.Key).Last().Key : 0) + 1,
                    value: mail);

                if (mail.AttachmentVNum != null)
                {
                    parcel++;
                    Session.SendPacket(packet: GenerateParcel(mail: mail));
                }
                else
                {
                    if (!mail.IsOpened) letter++;
                    Session.SendPacket(packet: GeneratePost(mail: mail, type: 1));
                }
            }

            if (parcel > 0)
                Session.SendPacket(
                    packet: GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "GIFTED"),
                            arg0: parcel), type: 11));
            if (letter > 0)
                Session.SendPacket(packet: GenerateSay(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "NEW_MAIL"), arg0: letter),
                    type: 10));
        }

        public void RemoveBuffByBCardTypeSubType(List<KeyValuePair<byte, byte>> bcardTypes)
        {
            bcardTypes.ForEach(action: bt =>
                Buff.Where(predicate: b => b.Card.BCards.Any(predicate: s =>
                        s.Type.Equals(obj: bt.Key) && s.SubType.Equals(obj: (byte)(bt.Value / 10)) &&
                        (s.CastType == 0 || b.Start.AddMilliseconds(value: b.Card.Delay * 100 + 1500) < DateTime.Now)))
                    .ToList()
                    .ForEach(action: a => RemoveBuff(cardId: a.Card.CardId)));
        }

        public void RemoveVehicle()
        {
            RemoveBuff(cardId: 336);
            ItemInstance sp = null;
            if (Inventory != null)
                sp = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);
            IsVehicled = false;
            VehicleItem = null;
            LoadSpeed();
            if (UseSp)
            {
                if (sp != null)
                {
                    Morph = sp.Item.Morph;
                    MorphUpgrade = sp.Upgrade;
                    MorphUpgrade2 = sp.Design;
                }
            }
            else
            {
                Morph = 0;
            }

            Session.CurrentMapInstance?.Broadcast(packet: GenerateCMode());
            Session.SendPacket(packet: GenerateCond());
            LastSpeedChange = DateTime.Now;
        }

        public void StandUp()
        {
            if (!IsVehicled && IsSitting)
            {
                IsSitting = false;
                MapInstance?.Broadcast(packet: GenerateRest());
            }
        }

        public void Rest()
        {
            if (LastSkillUse.AddSeconds(value: 4) > DateTime.Now ||
                LastDefence.AddSeconds(value: 4) > DateTime.Now) return;
            if (!IsVehicled)
            {
                IsSitting = !IsSitting;
                Session.CurrentMapInstance?.Broadcast(packet: GenerateRest());
            }
            else
            {
                Session.SendPacket(
                    packet: GenerateSay(message: Language.Instance.GetMessageFromKey(key: "IMPOSSIBLE_TO_USE"),
                        type: 10));
            }
        }

        public void Save()
        {
            Logger.LogUserEvent(logEvent: "CHARACTER_DB_SAVE", caller: Session.GenerateIdentity(), data: "START");
            try
            {
                AccountDto account = Session.Account;
                DaoFactory.AccountDao.InsertOrUpdate(account: ref account);

                CharacterDto character = DeepCopy();
                DaoFactory.CharacterDao.InsertOrUpdate(character: ref character);

                if (Inventory != null)
                    // be sure that noone tries to edit while saving is currently editing
                    lock (Inventory)
                    {
                        // load and concat inventory with equipment
                        var inventories = Inventory.GetAllItems();
                        IEnumerable<Guid> currentlySavedInventoryIds =
                            DaoFactory.ItemInstanceDao.LoadSlotAndTypeByCharacterId(characterId: CharacterId);
                        var characters = DaoFactory.CharacterDao.LoadByAccount(accountId: Session.Account.AccountId);
                        foreach (var characteraccount in characters.Where(predicate: s => s.CharacterId != CharacterId))
                            currentlySavedInventoryIds = currentlySavedInventoryIds.Concat(second: DaoFactory
                                .ItemInstanceDao
                                .LoadByCharacterId(characterId: characteraccount.CharacterId)
                                .Where(predicate: s => s.Type == InventoryType.Warehouse).Select(selector: i => i.Id)
                                .ToList());

                        IEnumerable<MinilandObjectDto> currentlySavedMinilandObjectEntries =
                            DaoFactory.MinilandObjectDao.LoadByCharacterId(characterId: CharacterId).ToList();
                        foreach (var mobjToDelete in currentlySavedMinilandObjectEntries.Except(second: MinilandObjects)
                        )
                            DaoFactory.MinilandObjectDao.DeleteById(id: mobjToDelete.MinilandObjectId);

                        DaoFactory.ItemInstanceDao.DeleteGuidList(
                            guids: currentlySavedInventoryIds.Except(second: inventories.Select(selector: i => i.Id)));

                        // create or update all which are new or do still exist
                        var saveInventory = inventories.Where(predicate: s =>
                            s.Type != InventoryType.Bazaar && s.Type != InventoryType.FamilyWareHouse).ToList();

                        DaoFactory.ItemInstanceDao.InsertOrUpdateFromList(items: saveInventory);

                        foreach (var itemInstance in saveInventory)
                        {
                            if (!(itemInstance is ItemInstance instance)) continue;
                            if (!instance.ShellEffects.Any()) continue;
                            DaoFactory.ShellEffectDao.DeleteByEquipmentSerialId(id: itemInstance.EquipmentSerialId);
                            DaoFactory.ShellEffectDao.InsertOrUpdateFromList(shellEffects: itemInstance.ShellEffects,
                                equipmentSerialId: itemInstance.EquipmentSerialId);
                            DaoFactory.CellonOptionDao.InsertOrUpdateFromList(cellonOption: itemInstance.CellonOptions,
                                equipmentSerialId: itemInstance.EquipmentSerialId);
                            instance.ShellEffects.ForEach(action: s =>
                            {
                                s.EquipmentSerialId = instance.EquipmentSerialId;
                                DaoFactory.ShellEffectDao.InsertOrUpdate(shelleffect: s);
                            });
                            instance.CellonOptions.ForEach(action: s =>
                            {
                                s.EquipmentSerialId = instance.EquipmentSerialId;
                                DaoFactory.CellonOptionDao.InsertOrUpdate(cellonOption: s);
                            });
                        }
                    }

                if (Skills != null)
                {
                    IEnumerable<Guid> currentlySavedCharacterSkills =
                        DaoFactory.CharacterSkillDao.LoadKeysByCharacterId(characterId: CharacterId).ToList();

                    foreach (var characterSkillToDeleteId in
                        currentlySavedCharacterSkills.Except(second: Skills.Select(selector: s => s.Id)))
                        DaoFactory.CharacterSkillDao.Delete(id: characterSkillToDeleteId);

                    foreach (var characterSkill in Skills.GetAllItems())
                        DaoFactory.CharacterSkillDao.InsertOrUpdate(dto: characterSkill);
                }

                var currentlySavedMates = DaoFactory.MateDao.LoadByCharacterId(characterId: CharacterId)
                    .Select(selector: s => s.MateId);

                foreach (var matesToDeleteId in currentlySavedMates.Except(
                    second: Mates.Select(selector: s => s.MateId)))
                    DaoFactory.MateDao.Delete(id: matesToDeleteId);

                foreach (var mate in Mates)
                {
                    MateDto matesave = mate;
                    DaoFactory.MateDao.InsertOrUpdate(mate: ref matesave);
                }

                IEnumerable<QuicklistEntryDto> quickListEntriesToInsertOrUpdate = QuicklistEntries.ToList();

                IEnumerable<Guid> currentlySavedQuicklistEntries =
                    DaoFactory.QuicklistEntryDao.LoadKeysByCharacterId(characterId: CharacterId).ToList();
                foreach (var quicklistEntryToDelete in
                    currentlySavedQuicklistEntries.Except(second: QuicklistEntries.Select(selector: s => s.Id)))
                    DaoFactory.QuicklistEntryDao.Delete(id: quicklistEntryToDelete);
                foreach (var quicklistEntry in quickListEntriesToInsertOrUpdate)
                    DaoFactory.QuicklistEntryDao.InsertOrUpdate(dto: quicklistEntry);

                foreach (var mobjEntry in (IEnumerable<MinilandObjectDto>)MinilandObjects.ToList())
                {
                    var mobj = mobjEntry;
                    DaoFactory.MinilandObjectDao.InsertOrUpdate(obj: ref mobj);
                }

                var currentlySavedBuff = DaoFactory.StaticBuffDao.LoadByTypeCharacterId(characterId: CharacterId);
                foreach (var bonusToDelete in currentlySavedBuff.Except(
                    second: Buff.Select(selector: s => s.Card.CardId)))
                    DaoFactory.StaticBuffDao.Delete(bonusToDelete: bonusToDelete, characterId: CharacterId);
                if (_isStaticBuffListInitial)
                    foreach (var buff in Buff.Where(predicate: s => s.StaticBuff).ToArray())
                    {
                        var bf = new StaticBuffDto
                        {
                            CharacterId = CharacterId,
                            RemainingTime = (int)(buff.RemainingTime - (DateTime.Now - buff.Start).TotalSeconds),
                            CardId = buff.Card.CardId
                        };
                        DaoFactory.StaticBuffDao.InsertOrUpdate(staticBuff: ref bf);
                    }

                //Quest
                foreach (var q in DaoFactory.CharacterQuestDao.LoadByCharacterId(characterId: CharacterId).ToList())
                    DaoFactory.CharacterQuestDao.Delete(characterId: CharacterId, questId: q.QuestId);
                foreach (var qst in Quests.ToList())
                {
                    var qstDTO = new CharacterQuestDto
                    {
                        CharacterId = qst.CharacterId,
                        QuestId = qst.QuestId,
                        FirstObjective = qst.FirstObjective,
                        SecondObjective = qst.SecondObjective,
                        ThirdObjective = qst.ThirdObjective,
                        FourthObjective = qst.FourthObjective,
                        FifthObjective = qst.FifthObjective,
                        IsMainQuest = qst.IsMainQuest
                    };
                    DaoFactory.CharacterQuestDao.InsertOrUpdate(quest: qstDTO);
                }

                foreach (var bonus in StaticBonusList.ToArray())
                {
                    var bonus2 = bonus;
                    DaoFactory.StaticBonusDao.InsertOrUpdate(staticBonus: ref bonus2);
                }

                foreach (var general in GeneralLogs.GetAllItems())
                    if (!DaoFactory.GeneralLogDao.IdAlreadySet(id: general.LogId))
                        DaoFactory.GeneralLogDao.Insert(generalLog: general);
                foreach (var Resp in Respawns)
                {
                    var res = Resp;
                    if (Resp.MapId != 0 && Resp.X != 0 && Resp.Y != 0)
                        DaoFactory.RespawnDao.InsertOrUpdate(respawn: ref res);
                }

                Logger.LogUserEvent(logEvent: "CHARACTER_DB_SAVE", caller: Session.GenerateIdentity(), data: "FINISH");
            }
            catch (Exception e)
            {
                Logger.LogUserEventError(logEvent: "CHARACTER_DB_SAVE", caller: Session.GenerateIdentity(),
                    data: "ERROR", ex: e);
            }
        }

        public void SendGift(long id, short vnum, short amount, sbyte rare, byte upgrade, short design, bool isNosmall)
        {
            var it = ServerManager.GetItem(vnum: vnum);

            if (it != null)
            {
                if (it.ItemType != ItemType.Weapon && it.ItemType != ItemType.Armor &&
                    it.ItemType != ItemType.Specialist && it.EquipmentSlot != EquipmentType.Gloves &&
                    it.EquipmentSlot != EquipmentType.Boots)
                    upgrade = 0;
                else if (it.ItemType != ItemType.Weapon && it.ItemType != ItemType.Armor) rare = 0;
                if (rare > 8 || rare < -2) rare = 0;
                if (upgrade > 10 && it.ItemType != ItemType.Specialist)
                    upgrade = 0;
                else if (it.ItemType == ItemType.Specialist && upgrade > 15) upgrade = 0;

                // maximum size of the amount is 999
                if (amount > 32000) amount = 32000;

                var mail = new MailDto
                {
                    AttachmentAmount = it.Type == InventoryType.Etc || it.Type == InventoryType.Main
                        ? amount
                        : (short)1,
                    IsOpened = false,
                    Date = DateTime.Now,
                    ReceiverId = id,
                    SenderId = CharacterId,
                    AttachmentRarity = (byte)rare,
                    AttachmentUpgrade = upgrade,
                    AttachmentDesign = design,
                    IsSenderCopy = false,
                    Title = isNosmall ? "NOSMALL" : Name,
                    AttachmentVNum = vnum,
                    SenderClass = Class,
                    SenderGender = Gender,
                    SenderHairColor = HairColor,
                    SenderHairStyle = HairStyle,
                    EqPacket = GenerateEqListForPacket(),
                    SenderMorphId = Morph == 0 ? (short)-1 : (short)(Morph > short.MaxValue ? 0 : Morph)
                };
                MailServiceClient.Instance.SendMail(mail: mail);
            }
        }

        public void GetReputation(int amount, bool applyRate = true)
        {
            amount *= amount > 0 && applyRate ? ServerManager.Instance.Configuration.RateReputation : 1;
            var beforeReputIco = GetReputationIco();
            Reputation += amount;
            Session.SendPacket(packet: GenerateFd());
            if (beforeReputIco != GetReputationIco())
                Session.CurrentMapInstance?.Broadcast(client: Session,
                    content: Session.Character.GenerateIn(InEffect: 1),
                    receiver: ReceiverType.AllExceptMe);
            Session.CurrentMapInstance?.Broadcast(client: Session, content: Session.Character.GenerateGidx(),
                receiver: ReceiverType.AllExceptMe);
            if (amount > 0)
                Session.SendPacket(
                    packet: GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "REPUT_INCREASE"),
                            arg0: amount), type: 12));
            else if (amount < 0)
                Session.SendPacket(
                    packet: GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "REPUT_DECREASE"),
                            arg0: amount), type: 11));
        }

        public void SetRespawnPoint(short mapId, short mapX, short mapY)
        {
            if (Session.HasCurrentMapInstance && Session.CurrentMapInstance.Map.MapTypes.Count > 0)
            {
                var respawnmaptype = Session.CurrentMapInstance.Map.MapTypes[index: 0].RespawnMapTypeId;
                if (respawnmaptype != null)
                {
                    var resp = Respawns.Find(match: s => s.RespawnMapTypeId == respawnmaptype);
                    if (resp == null)
                    {
                        resp = new RespawnDto
                        {
                            CharacterId = CharacterId,
                            MapId = mapId,
                            X = mapX,
                            Y = mapY,
                            RespawnMapTypeId = (long)respawnmaptype
                        };
                        Respawns.Add(item: resp);
                    }
                    else
                    {
                        resp.X = mapX;
                        resp.Y = mapY;
                        resp.MapId = mapId;
                    }
                }
            }
        }

        public void SetReturnPoint(short mapId, short mapX, short mapY)
        {
            if (Session.HasCurrentMapInstance && Session.CurrentMapInstance.Map.MapTypes.Count > 0)
            {
                var respawnmaptype = Session.CurrentMapInstance.Map.MapTypes[index: 0].ReturnMapTypeId;
                if (respawnmaptype != null)
                {
                    var resp = Respawns.Find(match: s => s.RespawnMapTypeId == respawnmaptype);
                    if (resp == null)
                    {
                        resp = new RespawnDto
                        {
                            CharacterId = CharacterId,
                            MapId = mapId,
                            X = mapX,
                            Y = mapY,
                            RespawnMapTypeId = (long)respawnmaptype
                        };
                        Respawns.Add(item: resp);
                    }
                    else
                    {
                        resp.X = mapX;
                        resp.Y = mapY;
                        resp.MapId = mapId;
                    }
                }
            }
            else if (Session.HasCurrentMapInstance &&
                     Session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
            {
                var resp = Respawns.Find(match: s => s.RespawnMapTypeId == 1);
                if (resp == null)
                {
                    resp = new RespawnDto
                    { CharacterId = CharacterId, MapId = mapId, X = mapX, Y = mapY, RespawnMapTypeId = 1 };
                    Respawns.Add(item: resp);
                }
                else
                {
                    resp.X = mapX;
                    resp.Y = mapY;
                    resp.MapId = mapId;
                }
            }
        }

        public void UpdateBushFire()
        {
            BattleEntity.UpdateBushFire();
        }

        public bool WeaponLoaded(CharacterSkill ski)
        {
            if (ski != null)
                switch (Class)
                {
                    default:
                        return false;

                    case ClassType.Adventurer:
                        if (ski.Skill.Type == 1 && Inventory != null)
                        {
                            var wearable = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.SecondaryWeapon,
                                type: InventoryType.Wear);
                            if (wearable != null)
                            {
                                if (wearable.Ammo > 0)
                                {
                                    wearable.Ammo--;
                                    return true;
                                }

                                if (Inventory.CountItem(itemVNum: 2081) < 1)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NO_AMMO_ADVENTURER"),
                                        type: 10));
                                    return false;
                                }

                                Inventory.RemoveItemAmount(vnum: 2081);
                                wearable.Ammo = 100;
                                Session.SendPacket(
                                    packet: GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "AMMO_LOADED_ADVENTURER"),
                                        type: 10));
                                return true;
                            }

                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_WEAPON"), type: 10));
                            return false;
                        }

                        return true;

                    case ClassType.Swordsman:
                        if (ski.Skill.Type == 1 && Inventory != null)
                        {
                            var inv = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.SecondaryWeapon,
                                type: InventoryType.Wear);
                            if (inv != null)
                            {
                                if (inv.Ammo > 0)
                                {
                                    inv.Ammo--;
                                    return true;
                                }

                                if (Inventory.CountItem(itemVNum: 2082) < 1)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NO_AMMO_SWORDSMAN"),
                                        type: 10));
                                    return false;
                                }

                                Inventory.RemoveItemAmount(vnum: 2082);
                                inv.Ammo = 100;
                                Session.SendPacket(
                                    packet: GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "AMMO_LOADED_SWORDSMAN"),
                                        type: 10));
                                return true;
                            }

                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_WEAPON"), type: 10));
                            return false;
                        }

                        return true;

                    case ClassType.Archer:
                        if (ski.Skill.Type == 1 && Inventory != null)
                        {
                            var inv = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon,
                                type: InventoryType.Wear);
                            if (inv != null)
                            {
                                if (inv.Ammo > 0)
                                {
                                    inv.Ammo--;
                                    return true;
                                }

                                if (Inventory.CountItem(itemVNum: 2083) < 1)
                                {
                                    Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "NO_AMMO_ARCHER"),
                                            type: 10));
                                    return false;
                                }

                                Inventory.RemoveItemAmount(vnum: 2083);
                                inv.Ammo = 100;
                                Session.SendPacket(
                                    packet: GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "AMMO_LOADED_ARCHER"),
                                        type: 10));
                                return true;
                            }

                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_WEAPON"), type: 10));
                            return false;
                        }

                        return true;

                    case ClassType.Magician:
                        if (ski.Skill.Type == 1 && Inventory != null)
                        {
                            var inv = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.SecondaryWeapon,
                                type: InventoryType.Wear);
                            if (inv == null)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NO_WEAPON"),
                                        type: 10));
                                return false;
                            }
                        }

                        return true;


                    case ClassType.MartialArtist:
                        return true;
                }

            return false;
        }

        internal void RefreshValidity()
        {
            if (StaticBonusList.RemoveAll(
                match: s => s.StaticBonusType == StaticBonusType.BackPack && s.DateEnd < DateTime.Now) > 0)
            {
                Session.SendPacket(
                    packet: GenerateSay(message: Language.Instance.GetMessageFromKey(key: "ITEM_TIMEOUT"), type: 10));
                Session.SendPacket(packet: GenerateExts());
            }

            if (StaticBonusList.RemoveAll(match: s => s.DateEnd < DateTime.Now) > 0)
                Session.SendPacket(
                    packet: GenerateSay(message: Language.Instance.GetMessageFromKey(key: "ITEM_TIMEOUT"), type: 10));

            if (Inventory != null)
                foreach (var suit in Enum.GetValues(enumType: typeof(EquipmentType)))
                {
                    var item = Inventory.LoadBySlotAndType(slot: (byte)suit, type: InventoryType.Wear);
                    if (item?.DurabilityPoint > 0 && item.Item.EquipmentSlot != EquipmentType.Amulet)
                    {
                        item.DurabilityPoint--;
                        if (item.DurabilityPoint == 0)
                        {
                            Inventory.DeleteById(id: item.Id);
                            Session.SendPackets(packets: GenerateStatChar());
                            Session.CurrentMapInstance?.Broadcast(packet: GenerateEq());
                            Session.SendPacket(packet: GenerateEquipment());
                            Session.SendPacket(packet: GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "ITEM_TIMEOUT"), type: 10));
                        }
                    }
                }
        }

        internal void SetSession(ClientSession clientSession)
        {
            Session = clientSession;
        }

        void GenerateXp(MapMonster monster, Dictionary<BattleEntity, long> damageList = null,
            bool isGroupMember = false)
        {
            if (monster?.DamageList == null) return;

            var isKiller = false;

            if (damageList == null)
            {
                damageList = new Dictionary<BattleEntity, long>();

                lock (monster.DamageList)
                {
                    // Deep copy monster.DamageList to damageList.

                    foreach (var keyValuePair in monster.DamageList)
                        damageList.Add(key: keyValuePair.Key, value: keyValuePair.Value);
                }

                isKiller = true;
            }

            Group grp = null;

            if (Group?.GroupType == GroupType.Group) grp = Group;

            bool checkMonsterOwner(long entityId, Group group)
            {
                if (damageList.FirstOrDefault(predicate: s => s.Value > 0).Key is BattleEntity monsterOwner)
                    return monsterOwner.MapEntityId == entityId || monsterOwner.Mate?.Owner?.CharacterId == entityId ||
                           monsterOwner.MapMonster?.Owner?.MapEntityId == entityId ||
                           group != null && group.IsMemberOfGroup(entityId: monsterOwner.MapEntityId);

                return false;
            }

            var isMonsterOwner = checkMonsterOwner(entityId: CharacterId, group: grp);

            lock (monster.DamageList)
            {
                if (monster.DamageList.Any())
                {
                    monster.DamageList.Where(predicate: s => s.Key.MapEntityId == CharacterId).ToList()
                        .ForEach(action: s => monster.DamageList.Remove(item: s));

                    // Call GenerateXp() for group members.

                    if (grp?.Sessions != null && !isGroupMember)
                        foreach (var groupMember in grp.Sessions.GetAllItems().Where(predicate: g =>
                            g.Character != null && g.Character.CharacterId != CharacterId &&
                            g.Character.MapInstanceId == MapInstanceId).ToList())
                            try
                            {
                                groupMember.Character?.GenerateXp(monster: monster, damageList: damageList,
                                    isGroupMember: true);
                            }
                            catch (Exception e)
                            {
                                Logger.Error(ex: e);
                            }
                }

                // Call GenerateXp() for others.

                if (monster.DamageList.Any() && isKiller)
                    try
                    {
                        monster.DamageList.Where(predicate: s =>
                                s.Value > 0 && s.Key.MapEntityId != BattleEntity.MapEntityId)
                            .ToList().ForEach(action: s =>
                                s.Key.Character?.GenerateXp(monster: monster, damageList: damageList));
                    }
                    catch (Exception e)
                    {
                        Logger.Error(ex: e);
                    }
            }

            // Exp percent regarding the damge
            double totalDamage = damageList.Sum(selector: s => s.Value);
            double damageByCharacterOrGroup = damageList.Where(predicate: s =>
                s.Key != null && s.Key.MapEntityId == CharacterId ||
                Mates.Any(predicate: m => m.MateTransportId == s.Key.MapEntityId) ||
                grp != null && grp.IsMemberOfGroup(entityId: s.Key.MapEntityId)).Sum(selector: s => s.Value);
            var expDamageRate = damageByCharacterOrGroup / totalDamage *
                                (isMonsterOwner && damageList.Any(predicate: s =>
                                    s.Key != null && s.Value > 0 && s.Key.MapEntityId != CharacterId &&
                                    (grp == null || !grp.IsMemberOfGroup(entityId: s.Key.MapEntityId)))
                                    ? 1.2f
                                    : 1);

            if (double.IsNaN(d: expDamageRate)) expDamageRate = 0;

            var monsterInfo = monster.Monster;

            if (!Session.Account.PenaltyLogs.Any(predicate: s =>
                s.Penalty == PenaltyType.BlockExp && s.DateEnd > DateTime.Now))
            {
                if (Hp <= 0) return;

                if ((int)(LevelXp / (XpLoad() / 10)) <
                    (int)((LevelXp + monsterInfo.Xp * expDamageRate) / (XpLoad() / 10)))
                {
                    Hp = (int)HPLoad();
                    Mp = (int)MPLoad();
                    Session.SendPacket(packet: GenerateStat());
                    Session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                        callerId: CharacterId, effectId: 5));
                }

                ItemInstance specialist = null;

                if (Inventory != null)
                    specialist = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);

                var xp = (int)(GetXP(mapMonster: monster, group: grp) * expDamageRate * (isMonsterOwner ? 1 : 0.8f) *
                                (1 + GetBuff(type: CardType.Item,
                                    subtype: (byte)AdditionalTypes.Item.ExpIncreased)[0] / 100D) *
                                (1 + GetBuff(type: CardType.MartialArts,
                                        subtype: (byte)AdditionalTypes.MartialArts.IncreaseBattleAndJobExperience)[0] /
                                    100));

                if (Level < ServerManager.Instance.Configuration.MaxLevel) LevelXp += xp;

                foreach (var mate in Mates.Where(predicate: x => x.IsTeamMember && x.IsAlive))
                {
                    mate.GenerateXp(xp: xp);

                    if (mate.IsUsingSp)
                    {
                        mate.Sp.AddXp(amount: xp);
                        mate.Owner?.Session?.SendPacket(packet: mate.GenerateScPacket());
                    }
                }

                if (Class == 0 && JobLevel < 20 ||
                    Class != 0 && JobLevel < ServerManager.Instance.Configuration.MaxJobLevel)
                {
                    if (specialist != null && UseSp &&
                        specialist.SpLevel < ServerManager.Instance.Configuration.MaxSpLevel && specialist.SpLevel > 19)
                        JobLevelXp += (int)(GetJXP(mapMonster: monster, group: grp) * expDamageRate *
                                             (isMonsterOwner ? 1 : 0.8f) / 2D *
                                             (1 + GetBuff(type: CardType.Item,
                                                     subtype: (byte)AdditionalTypes.Item.ExpIncreased)[0] /
                                                 100D) * (1 + GetBuff(type: CardType.MartialArts,
                                                 subtype: (byte)AdditionalTypes.MartialArts
                                                     .IncreaseBattleAndJobExperience)[0] / 100));
                    else
                        JobLevelXp += (int)(GetJXP(mapMonster: monster, group: grp) * expDamageRate *
                                             (isMonsterOwner ? 1 : 0.8f) *
                                             (1 + GetBuff(type: CardType.Item,
                                                     subtype: (byte)AdditionalTypes.Item.ExpIncreased)[0] /
                                                 100D) * (1 + GetBuff(type: CardType.MartialArts,
                                                 subtype: (byte)AdditionalTypes.MartialArts
                                                     .IncreaseBattleAndJobExperience)[0] / 100));
                }

                if (specialist != null && UseSp && specialist.SpLevel < ServerManager.Instance.Configuration.MaxSpLevel)
                {
                    var multiplier = specialist.SpLevel < 10 ? 10 : specialist.SpLevel < 19 ? 5 : 1;

                    specialist.Xp += (int)(GetJXP(mapMonster: monster, group: grp) * expDamageRate *
                                            (multiplier +
                                             (GetBuff(type: CardType.Item,
                                                     subtype: (byte)AdditionalTypes.Item.ExpIncreased)[0] /
                                                 100D + GetBuff(type: CardType.Item,
                                                     subtype: (byte)AdditionalTypes.Item.IncreaseSpxp)[0] / 100D)));
                }

                if (HeroLevel > 0 && HeroLevel < ServerManager.Instance.Configuration.MaxHeroLevel)
                    HeroXp += (int)(GetHXP(mapMonster: monster, group: grp) * expDamageRate / 50 *
                                     (isMonsterOwner ? 1 : 0.8f) *
                                     (1 + GetBuff(type: CardType.Item,
                                         subtype: (byte)AdditionalTypes.Item.ExpIncreased)[0] / 100D));

                var fairy = Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Fairy, type: InventoryType.Wear);

                if (fairy != null)
                {
                    if (fairy.ElementRate + fairy.Item.ElementRate < fairy.Item.MaxElementRate
                        && Level <= monsterInfo.Level + 15 && Level >= monsterInfo.Level - 15)
                        fairy.Xp += ServerManager.Instance.Configuration.RateFairyXp;

                    double experience =
                        CharacterHelper.LoadFairyXPData(elementRate: fairy.ElementRate + fairy.Item.ElementRate);

                    while (fairy.Xp >= experience)
                    {
                        fairy.Xp -= (int)experience;
                        fairy.ElementRate++;

                        if (fairy.ElementRate + fairy.Item.ElementRate == fairy.Item.MaxElementRate)
                        {
                            fairy.Xp = 0;

                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "FAIRYMAX"),
                                    arg0: fairy.Item.Name), type: 10));
                        }
                        else
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "FAIRY_LEVELUP"),
                                    arg0: fairy.Item.Name),
                                type: 10));
                        }

                        Session.SendPacket(packet: GeneratePairy());
                    }
                }

                GenerateLevelXpLevelUp();
                GenerateJobXpLevelUp();

                if (specialist != null) GenerateSpXpLevelUp(specialist: specialist);

                GenerateHeroXpLevelUp();

                Session.SendPacket(packet: GenerateLev());
            }
        }

        int GetGold(MapMonster mapMonster)
        {
            if (MapId == 2006 || MapId == 150) return 0;
            var lowBaseGold =
                ServerManager.RandomNumber(min: 6 * mapMonster.Monster?.Level ?? 1,
                    max: 12 * mapMonster.Monster?.Level ?? 1);
            var actMultiplier =
                Session?.CurrentMapInstance?.Map.MapTypes?.Any(predicate: s =>
                    s.MapTypeId == (short)MapTypeEnum.Act52) ?? false
                    ? 10
                    : 1;
            if (Session?.CurrentMapInstance?.Map.MapTypes?.Any(predicate: s =>
                s.MapTypeId == (short)MapTypeEnum.Act61 || s.MapTypeId == (short)MapTypeEnum.Act61A ||
                s.MapTypeId == (short)MapTypeEnum.Act61d) == true) actMultiplier = 5;
            return lowBaseGold * actMultiplier;
        }

        int GetHXP(MapMonster mapMonster, Group group)
        {
            if (HeroLevel >= ServerManager.Instance.Configuration.MaxHeroLevel) return 0;

            var npcMonster = mapMonster.Monster;

            var partySize = group?.GroupType == GroupType.Group
                ? group.Sessions.ToList().Count(predicate: s =>
                    s?.Character != null && s.Character.MapInstance == mapMonster.MapInstance &&
                    s.Character.HeroLevel > 0 &&
                    s.Character.HeroLevel < ServerManager.Instance.Configuration.MaxHeroLevel)
                : 1;

            if (partySize < 1) partySize = 1;

            double sharedHXp = npcMonster.HeroXp / partySize;

            var memberHXp =
                sharedHXp * CharacterHelper.ExperiencePenalty(playerLevel: Level, monsterLevel: npcMonster.Level) *
                ServerManager.Instance.Configuration.RateHeroicXp;

            return (int)memberHXp;
        }

        int GetJXP(MapMonster mapMonster, Group group)
        {
            var npcMonster = mapMonster.Monster;

            var partySize = group?.GroupType != GroupType.Group
                ? 1
                : group.Sessions.ToList().Count(predicate: s =>
                {
                    if (s?.Character == null
                        || s.Character.MapInstance != mapMonster.MapInstance)
                        return false;

                    if (!s.Character.UseSp)
                        return s.Character.JobLevel <
                               (s.Character.Class == 0 ? 20 : ServerManager.Instance.Configuration.MaxJobLevel);

                    var sp = s.Character.Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                        type: InventoryType.Wear);

                    if (sp != null) return sp.SpLevel < ServerManager.Instance.Configuration.MaxSpLevel;

                    return false;
                });

            if (partySize < 1) partySize = 1;

            var sharedJXp = (double)npcMonster.JobXp / partySize;

            var memberJxp =
                sharedJXp * CharacterHelper.ExperiencePenalty(playerLevel: Level, monsterLevel: npcMonster.Level) *
                (ServerManager.Instance.Configuration.RateXp + MapInstance.XpRate);

            return (int)memberJxp;
        }

        int GetXP(MapMonster mapMonster, Group group)
        {
            var npcMonster = mapMonster.Monster;

            var partySize = group?.GroupType == GroupType.Group
                ? group.Sessions.ToList().Count(predicate: s =>
                    s?.Character != null && s.Character.MapInstance == mapMonster.MapInstance &&
                    s.Character.Level < ServerManager.Instance.Configuration.MaxLevel)
                : 1;

            if (partySize < 1) partySize = 1;

            var sharedXp = (double)npcMonster.Xp / partySize;

            if (npcMonster.Level >= 80)
                sharedXp *= 3;
            else if (npcMonster.Level >= 75) sharedXp *= 2;

            var lvlDifference = Level - npcMonster.Level;

            var memberXp = (lvlDifference < 5 ? sharedXp : sharedXp / 3 * 2) *
                           CharacterHelper.ExperiencePenalty(playerLevel: Level, monsterLevel: npcMonster.Level) *
                           (ServerManager.Instance.Configuration.RateXp + MapInstance.XpRate);

            if (Level <= 5 && lvlDifference < -4) memberXp *= 1.5;

            return (int)memberXp;
        }

        int GetShellMainWeaponEffectValue(ShellWeaponEffectType effectType)
        {
            return ShellEffectMain.Where(predicate: s => s.Effect == (byte)effectType).FirstOrDefault()?.Value ??
                   0;
        }

        int GetShellSecondaryWeaponEffectValue(ShellWeaponEffectType effectType)
        {
            return ShellEffectSecondary.Where(predicate: s => s.Effect == (byte)effectType).FirstOrDefault()?.Value ??
                   0;
        }

        int GetShellArmorEffectValue(ShellArmorEffectType effectType)
        {
            return ShellEffectArmor.Where(predicate: s => s.Effect == (byte)effectType).FirstOrDefault()?.Value ??
                   0;
        }

        int HealthHPLoad()
        {
            var naturalRecovery = 1;
            if (Skills != null)
                naturalRecovery += Skills.Where(predicate: s => s.Skill.SkillType == 0 && s.Skill.CastId == 10)
                    .Sum(selector: s => s.Skill.UpgradeSkill);
            if (IsSitting)
            {
                var regen = GetBuff(type: CardType.Recovery,
                    subtype: (byte)AdditionalTypes.Recovery.HpRecoveryIncreased)[0];
                return (int)((regen + CharacterHelper.HPHealth[(byte)Class] +
                               CellonOptions.Where(predicate: s => s.Type == CellonOptionType.HpRestore)
                                   .Sum(selector: s => s.Value)) *
                              (1 + GetShellArmorEffectValue(effectType: ShellArmorEffectType.RecoveryHpOnRest) / 100D));
            }

            return (DateTime.Now - LastDefence).TotalSeconds > 4
                ? (int)(CharacterHelper.HPHealthStand[(byte)Class] *
                         (1 + GetShellArmorEffectValue(effectType: ShellArmorEffectType.RecoveryHp) / 100D) *
                         naturalRecovery)
                : 0;
        }

        int HealthMPLoad()
        {
            var naturalRecovery = 1;
            if (Skills != null)
                naturalRecovery += Skills.Where(predicate: s => s.Skill.SkillType == 0 && s.Skill.CastId == 10)
                    .Sum(selector: s => s.Skill.UpgradeSkill);
            if (IsSitting)
            {
                var regen = GetBuff(type: CardType.Recovery,
                    subtype: (byte)AdditionalTypes.Recovery.MpRecoveryIncreased)[0];
                return (int)((regen + CharacterHelper.MPHealth[(byte)Class] +
                               CellonOptions.Where(predicate: s => s.Type == CellonOptionType.MpRestore)
                                   .Sum(selector: s => s.Value)) *
                              (1 + GetShellArmorEffectValue(effectType: ShellArmorEffectType.RecoveryMpOnRest) / 100D));
            }

            return (DateTime.Now - LastDefence).TotalSeconds > 4
                ? (int)(CharacterHelper.MPHealthStand[(byte)Class] *
                         (1 + GetShellArmorEffectValue(effectType: ShellArmorEffectType.RecoveryMp) / 100D) *
                         naturalRecovery)
                : 0;
        }

        double HeroXPLoad()
        {
            return HeroLevel == 0 ? 1 : CharacterHelper.HeroXpData[HeroLevel - 1];
        }

        double JobXPLoad()
        {
            return Class == (byte)ClassType.Adventurer
                ? CharacterHelper.FirstJobXpData[JobLevel - 1]
                : CharacterHelper.SecondJobXPData[JobLevel - 1];
        }

        double SpXpLoad()
        {
            ItemInstance specialist = null;
            if (Inventory != null)
                specialist = Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);
            return specialist != null
                ? CharacterHelper.SPXPData[specialist.SpLevel == 0 ? 0 : specialist.SpLevel - 1]
                : 0;
        }

        double XpLoad()
        {
            return CharacterHelper.XPData[Level - 1];
        }

        public void TeleportToDir(int Dir, int Distance)
        {
            WalkDisposable?.Dispose();
            var NewX = PositionX;
            var NewY = PositionY;
            var BlockedZone = false;
            for (short i = 1;
                Map.GetDistance(p: new MapCell { X = PositionX, Y = PositionY }, q: new MapCell { X = NewX, Y = NewY }) <
                Math.Abs(value: Distance) && i < +Math.Abs(value: Distance) + 5 && !BlockedZone;
                i++)
                switch (Dir)
                {
                    case 0:
                        if (!MapInstance.Map.IsBlockedZone(x: NewX, y: NewY - i))
                        {
                            NewX = PositionX;
                            NewY = (short)(PositionY - i);
                        }
                        else
                        {
                            BlockedZone = true;
                        }

                        break;
                    case 1:
                        if (!MapInstance.Map.IsBlockedZone(x: NewX + i, y: NewY))
                        {
                            NewX = (short)(PositionX + i);
                            NewY = PositionY;
                        }
                        else
                        {
                            BlockedZone = true;
                        }

                        break;
                    case 2:
                        if (!MapInstance.Map.IsBlockedZone(x: NewX, y: NewY + i))
                        {
                            NewX = PositionX;
                            NewY = (short)(PositionY + i);
                        }
                        else
                        {
                            BlockedZone = true;
                        }

                        break;
                    case 3:
                        if (!MapInstance.Map.IsBlockedZone(x: NewX - i, y: NewY))
                        {
                            NewX = (short)(PositionX - i);
                            NewY = PositionY;
                        }
                        else
                        {
                            BlockedZone = true;
                        }

                        break;
                    case 4:
                        if (!MapInstance.Map.IsBlockedZone(x: NewX - i, y: NewY - i))
                        {
                            NewX = (short)(PositionX - i);
                            NewY = (short)(PositionY - i);
                        }
                        else
                        {
                            BlockedZone = true;
                        }

                        break;
                    case 5:
                        if (!MapInstance.Map.IsBlockedZone(x: NewX + i, y: NewY - i))
                        {
                            NewX = (short)(PositionX + i);
                            NewY = (short)(PositionY - i);
                        }
                        else
                        {
                            BlockedZone = true;
                        }

                        break;
                    case 6:
                        if (!MapInstance.Map.IsBlockedZone(x: NewX + i, y: NewY + i))
                        {
                            NewX = (short)(PositionX + i);
                            NewY = (short)(PositionY + i);
                        }
                        else
                        {
                            BlockedZone = true;
                        }

                        break;
                    case 7:
                        if (!MapInstance.Map.IsBlockedZone(x: NewX - i, y: NewY + i))
                        {
                            NewX = (short)(PositionX - i);
                            NewY = (short)(PositionY + i);
                        }
                        else
                        {
                            BlockedZone = true;
                        }

                        break;
                }

            PositionX = NewX;
            PositionY = NewY;
            MapInstance.Broadcast(packet: GenerateTp());
        }

        public void EnterInstance(ScriptedInstance input)
        {
            var instance = input.Copy();
            instance.LoadScript(mapinstancetype: MapInstanceType.TimeSpaceInstance, creator: this);
            if (instance.FirstMap == null) return;

            if (Session.Character.Level < instance.LevelMinimum)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "TOO_LOW_LVL"), type: 0));
                return;
            }

            if (Session.Character.Level > instance.LevelMaximum)
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "TOO_HIGH_LVL"), type: 0));
                return;
            }

            var entries = instance.DailyEntries - Session.Character.GeneralLogs.CountLinq(predicate: s =>
                s.LogType == "InstanceEntry" && short.Parse(s: s.LogData) == instance.Id &&
                s.Timestamp.Date == DateTime.Today);
            if (instance.DailyEntries == 0 || entries > 0)
            {
                foreach (var requiredItem in instance.RequiredItems)
                {
                    if (Session.Character.Inventory.CountItem(itemVNum: requiredItem.VNum) < requiredItem.Amount)
                    {
                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "NO_ITEM_REQUIRED"),
                                arg0: ServerManager.GetItem(vnum: requiredItem.VNum).Name), type: 0));
                        return;
                    }

                    Session.Character.Inventory.RemoveItemAmount(vnum: requiredItem.VNum, amount: requiredItem.Amount);
                }

                Session?.SendPackets(packets: instance.GenerateMinimap());
                Session?.SendPacket(packet: instance.GenerateMainInfo());
                Session?.SendPacket(packet: instance.FirstMap.InstanceBag.GenerateScore());
                if (instance.StartX != 0 || instance.StartY != 0)
                    ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                        mapInstanceId: instance.FirstMap.MapInstanceId, mapX: instance.StartX, mapY: instance.StartY);
                else
                    ServerManager.Instance.TeleportOnRandomPlaceInMap(session: Session,
                        guid: instance.FirstMap.MapInstanceId);
                instance.InstanceBag.CreatorId = Session.Character.CharacterId;
                Session.Character.Timespace = instance;
            }
            else
            {
                Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "INSTANCE_NO_MORE_ENTRIES"),
                        type: 0));
                Session.SendPacket(
                    packet: Session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "INSTANCE_NO_MORE_ENTRIES"), type: 10));
            }
        }

        public bool RemoveSp(short vnum, bool forced)
        {
            if (Session?.HasSession == true && (!IsVehicled || forced))
            {
                if (Buff.Any(predicate: s => s.Card.BuffType == BuffType.Bad) && !forced)
                {
                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_UNTRASFORM_WITH_DEBUFFS"),
                        type: 0));
                    return false;
                }

                LastTransform = DateTime.Now;
                DisableBuffs(type: BuffType.All);

                EquipmentBCards.RemoveAll(match: s => s.ItemVNum.Equals(other: vnum));

                UseSp = false;
                LoadSpeed();
                Session.SendPacket(packet: GenerateCond());
                Session.SendPacket(packet: GenerateLev());
                SpCooldown = 30;
                if (SkillsSp != null)
                    foreach (var ski in SkillsSp.Where(predicate: s => !s.CanBeUsed()))
                    {
                        var time = ski.Skill.Cooldown;
                        var temp = (ski.LastUse - DateTime.Now).TotalMilliseconds + time * 100;
                        temp /= 1000;
                        SpCooldown = temp > SpCooldown
                            ? (int)temp
                            : SpCooldown;
                    }

                if (Authority >= AuthorityType.Mod || forced) SpCooldown = 0;
                if (SpCooldown > 0)
                {
                    Session.SendPacket(packet: GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "STAY_TIME"),
                            arg0: SpCooldown), type: 11));
                    Session.SendPacket(packet: $"sd {SpCooldown}");
                }

                Session.CurrentMapInstance?.Broadcast(packet: GenerateCMode());
                Session.CurrentMapInstance?.Broadcast(
                    packet: UserInterfaceHelper.GenerateGuri(type: 6, argument: 1, callerId: CharacterId),
                    xRangeCoordinate: PositionX,
                    yRangeCoordinate: PositionY);
                // ms_c
                Session.SendPacket(packet: GenerateSki());
                Session.SendPackets(packets: GenerateQuicklist());
                Session.SendPacket(packet: GenerateStat());
                Session.SendPackets(packets: GenerateStatChar());
                BattleEntity.RemoveOwnedMonsters();
                Logger.LogUserEvent(logEvent: "CHARACTER_SPECIALIST_RETURN", caller: Session.GenerateIdentity(),
                    data: $"SpCooldown: {SpCooldown}");
                if (SpCooldown > 0)
                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: SpCooldown * 1000)).Subscribe(
                        onNext: o =>
                        {
                            Session.SendPacket(
                                packet: GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "TRANSFORM_DISAPPEAR"),
                                    type: 11));
                            Session.SendPacket(packet: "sd 0");
                        });
            }

            return true;
        }

        #endregion
    }
}