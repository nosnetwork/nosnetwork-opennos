﻿using System;
using OpenNos.Data;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class PartnerSkill
    {
        #region Members

        Skill _skill;

        #endregion

        #region Instantiation

        public PartnerSkill(PartnerSkillDto input)
        {
            PartnerSkillId = input.PartnerSkillId;
            EquipmentSerialId = input.EquipmentSerialId;
            SkillVNum = input.SkillVNum;
            Level = input.Level;
        }

        #endregion

        #region Methods

        public bool CanBeUsed()
        {
            return Skill != null && LastUse.AddMilliseconds(value: Skill.Cooldown * 100) < DateTime.Now;
        }

        #endregion

        #region Properties

        public long PartnerSkillId { get; set; }

        public Guid EquipmentSerialId { get; set; }

        public short SkillVNum { get; set; }

        public byte Level { get; set; }

        public Skill Skill
        {
            get => _skill ?? (_skill = ServerManager.GetSkill(skillVNum: SkillVNum));
        }

        public DateTime LastUse { get; set; }

        #endregion
    }
}