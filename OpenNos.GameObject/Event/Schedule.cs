﻿using System;
using OpenNos.Domain;

namespace OpenNos.GameObject.Event
{
    public class Schedule
    {
        #region Properties

        public EventType Event { get; set; }

        public TimeSpan Time { get; set; }

        public int LvlBracket { get; set; }

        public string DayOfWeek { get; set; }

        #endregion
    }
}