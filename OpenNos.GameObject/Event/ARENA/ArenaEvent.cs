﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;

namespace OpenNos.GameObject.Event.ARENA
{
    internal class ArenaEvent
    {
        #region Methods

        internal static void GenerateTalentArena()
        {
            long groupid = 0;
            var seconds = 0;
            var obs = Observable.Interval(period: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: start2 =>
            {
                lock (ServerManager.Instance.ArenaMembers)
                {
                    ServerManager.Instance.ArenaMembers.ToList()
                        .Where(predicate: s => s.ArenaType == EventType.Talentarena)
                        .ToList().ForEach(action: s =>
                        {
                            s.Time -= 1;
                            var groupids = new List<long>();
                            ServerManager.Instance.ArenaMembers.ToList().Where(predicate: o => o.GroupId != null)
                                .ToList().ForEach(
                                    action: o =>
                                    {
                                        if (ServerManager.Instance.ArenaMembers.ToList()
                                            .Count(predicate: g => g.GroupId == o.GroupId) != 3) return;
                                        if (o.GroupId != null) groupids.Add(item: o.GroupId.Value);
                                    });

                            if (s.Time > 0)
                            {
                                if (s.GroupId == null)
                                {
                                    var members = ServerManager.Instance.ArenaMembers.ToList()
                                        .Where(predicate: e =>
                                            e.Session != s.Session && e.ArenaType == EventType.Talentarena &&
                                            e.Session.Character.Level <= s.Session.Character.Level + 5 &&
                                            e.Session.Character.Level >= s.Session.Character.Level - 5)
                                        .ToList();
                                    members.RemoveAll(match: o =>
                                        o.GroupId != null && groupids.Contains(item: o.GroupId.Value));
                                    var member = members.FirstOrDefault();
                                    if (member == null)
                                    {
                                    }
                                    else
                                    {
                                        if (member.GroupId == null)
                                        {
                                            groupid++;
                                            member.GroupId = groupid;
                                        }

                                        s.GroupId = member.GroupId;
                                        ServerManager.Instance.ArenaMembers.ToList().Where(predicate: e =>
                                                e.ArenaType == EventType.Talentarena && e.GroupId == member.GroupId)
                                            .ToList().ForEach(action: o =>
                                            {
                                                o.Session.SendPacket(packet: UserInterfaceHelper.GenerateBsInfo(mode: 1,
                                                    title: 2, time: -1, text: 6));
                                                o.Session.SendPacket(packet: o.Session.Character.GenerateSay(
                                                    message: Language.Instance.GetMessageFromKey(
                                                        key: "ARENA_TEAM_FOUND"), type: 10));
                                                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1)).Subscribe(
                                                    onNext: time =>
                                                    {
                                                        s.Time = 300;
                                                        if (ServerManager.Instance.ArenaMembers.ToList()
                                                            .Count(predicate: g => g.GroupId == s.GroupId) < 3)
                                                        {
                                                            o.Session.SendPacket(
                                                                packet: UserInterfaceHelper.GenerateBsInfo(mode: 0,
                                                                    title: 2, time: s.Time, text: 8));
                                                            o.Session.SendPacket(
                                                                packet: o.Session.Character.GenerateSay(
                                                                    message: Language.Instance.GetMessageFromKey(
                                                                        key: "SEARCH_ARENA_TEAM"),
                                                                    type: 10));
                                                        }
                                                        else
                                                        {
                                                            o.Session.SendPacket(
                                                                packet: UserInterfaceHelper.GenerateBsInfo(mode: 0,
                                                                    title: 2, time: s.Time, text: 1));
                                                            o.Session.SendPacket(
                                                                packet: o.Session.Character.GenerateSay(
                                                                    message: Language.Instance.GetMessageFromKey(
                                                                        key: "SEARCH_RIVAL_ARENA_TEAM"),
                                                                    type: 10));
                                                        }
                                                    });
                                            });
                                    }
                                }
                                else
                                {
                                    if (ServerManager.Instance.ArenaMembers.ToList()
                                        .Count(predicate: g => g.GroupId == s.GroupId) != 3) return;

                                    var member =
                                        ServerManager.Instance.ArenaMembers.ToList().FirstOrDefault(predicate: o =>
                                            o.GroupId != null && o.GroupId != s.GroupId &&
                                            groupids.Contains(item: o.GroupId.Value) &&
                                            o.Session.Character.Level <= s.Session.Character.Level + 5 &&
                                            o.Session.Character.Level >= s.Session.Character.Level - 5);
                                    if (member == null) return;

                                    var map = ServerManager.GenerateMapInstance(mapId: 2015,
                                        type: MapInstanceType.TalentArenaMapInstance, mapclock: new InstanceBag());
                                    map.IsPvp = true;
                                    var arenaTeam = new ConcurrentBag<ArenaTeamMember>();
                                    ServerManager.Instance.ArenaTeams.Add(item: arenaTeam);

                                    var arenamembers = ServerManager.Instance.ArenaMembers.ToList()
                                        .Where(predicate: o => o.GroupId == member.GroupId || o.GroupId == s.GroupId)
                                        .OrderBy(keySelector: o => o.GroupId)
                                        .ToArray();
                                    for (var i = 0; i < 6; i++)
                                    {
                                        var item = Inventory.InstantiateItemInstance(
                                            vnum: (short)(4433 + (i > 2 ? 5 - i : i)),
                                            ownerId: member.Session.Character.CharacterId);
                                        item.Design = (short)(4433 + (i > 2 ? 5 - i : i));
                                        map.MapDesignObjects.Add(item: new MapDesignObject
                                        {
                                            ItemInstance = item,
                                            ItemInstanceId = item.Id,
                                            CharacterId = member.Session.Character.CharacterId,
                                            MapX = (short)(i > 2 ? 120 : 19),
                                            MapY = (short)(i > 2 ? 35 + i % 3 * 4 : 36 + i % 3 * 4)
                                        });
                                    }

                                    map.InstanceBag.Clock.TotalSecondsAmount = 60;
                                    map.InstanceBag.Clock.SecondsRemaining = 600;
                                    map.InstanceBag.Clock.StartClock();
                                    IDisposable obs4 = null;
                                    IDisposable obs2 = null;
                                    IDisposable obs3 = null;
                                    IDisposable obs6 = null;
                                    var WinLoseMsgSent = false;
                                    Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 5)).Subscribe(onNext: time2 =>
                                    {
                                        obs3 = Observable.Interval(period: TimeSpan.FromSeconds(value: 5)).Subscribe(
                                            onNext: effect =>
                                            {
                                                arenamembers.Where(predicate: c => !c.Session.Character.Invisible)
                                                    .ToList()
                                                    .ForEach(action: o =>
                                                        map.Broadcast(
                                                            packet: o.Session.Character.GenerateEff(
                                                                effectid: o.GroupId == s.GroupId
                                                                    ? 3012
                                                                    : 3013)));
                                            });
                                    });
                                    var obs5 = Observable.Interval(period: TimeSpan.FromMilliseconds(value: 500))
                                        .Subscribe(onNext: start3 =>
                                        {
                                            map.Broadcast(packet: arenamembers
                                                .FirstOrDefault(predicate: o => o.Session != null)?.Session
                                                .Character.GenerateTaPs());
                                            var erenia = arenaTeam
                                                .Replace(predicate: team => team.ArenaTeamType == ArenaTeamType.Erenia)
                                                .ToList();
                                            var zenas = arenaTeam.Replace(predicate: team =>
                                                    team.ArenaTeamType == ArenaTeamType.Zenas)
                                                .ToList();
                                            BuffTeam(team: erenia);
                                            BuffTeam(team: zenas);
                                        });
                                    arenamembers.ToList().ForEach(action: o =>
                                    {
                                        o.Session.SendPacket(packet: UserInterfaceHelper.GenerateBsInfo(mode: 0,
                                            title: 2, time: s.Time, text: 2));
                                        o.Session.SendPacket(packet: o.Session.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(key: "RIVAL_ARENA_TEAM_FOUND"),
                                            type: 10));

                                        Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1)).Subscribe(
                                            onNext: time =>
                                            {
                                                o.Session.SendPacket(packet: "ta_close");
                                                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 5)).Subscribe(
                                                    onNext: time2 =>
                                                    {
                                                        if (o.Session.Character.IsVehicled)
                                                            o.Session.Character.RemoveVehicle();
                                                        var grp = o.Session.Character.Group;
                                                        if (grp != null) grp.LeaveGroup(session: o.Session);
                                                        o.Session.Character.Mates.Where(predicate: m => m.IsTeamMember)
                                                            .ToList()
                                                            .ForEach(action: m => m.BackToMiniland());
                                                        o.Session.Character.GeneralLogs.Add(value: new GeneralLogDto
                                                        {
                                                            AccountId = o.Session.Account.AccountId,
                                                            CharacterId = o.Session.Character.CharacterId,
                                                            IpAddress = o.Session.IpAddress,
                                                            LogData = "Entry",
                                                            LogType = "TalentArena",
                                                            Timestamp = DateTime.Now
                                                        });
                                                        var bufftodisable = new List<BuffType>
                                                            {BuffType.Bad, BuffType.Good, BuffType.Neutral};
                                                        o.Session.Character.DisableBuffs(types: bufftodisable);
                                                        var i = Array.IndexOf(array: arenamembers, value: o) + 1;
                                                        o.Session.Character.Hp = (int)o.Session.Character.HPLoad();
                                                        o.Session.Character.Mp = (int)o.Session.Character.MPLoad();
                                                        ServerManager.Instance.ChangeMapInstance(
                                                            characterId: o.Session.Character.CharacterId,
                                                            mapInstanceId: map.MapInstanceId,
                                                            mapX: o.GroupId == member.GroupId ? 125 : 14,
                                                            mapY: (o.GroupId == member.GroupId ? 37 : 38) + i % 3 * 2);
                                                        o.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                            message: Language.Instance.GetMessageFromKey(
                                                                key: "SELECT_ORDER_ARENA_TIME"), type: 0));
                                                        o.Session.SendPacket(packet: o.Session.Character.GenerateSay(
                                                            message: Language.Instance.GetMessageFromKey(
                                                                key: "SELECT_ORDER_ARENA_TIME"),
                                                            type: 10));
                                                        o.Session.SendPacket(packet: o.Session.Character.GenerateSay(
                                                            message: Language.Instance.GetMessageFromKey(
                                                                key: "SELECT_ORDER_ARENA"), type: 10));
                                                        o.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                            message: Language.Instance.GetMessageFromKey(
                                                                key: "SELECT_ORDER_ARENA"), type: 0));
                                                        o.Session.SendPacket(
                                                            packet: o.Session.Character.GenerateTaM(type: 0));
                                                        o.Session.SendPacket(packet: "ta_sv 0");
                                                        o.Session.SendPacket(
                                                            packet: UserInterfaceHelper.Instance.GenerateTaSt(
                                                                watch: TalentArenaOptionType
                                                                    .Watch));
                                                        o.Session.SendPacket(
                                                            packet: o.Session.Character.GenerateTaM(type: 3));

                                                        o.Session.SendPacket(packet: o.Session.Character.GenerateSay(
                                                            message: Language.Instance.GetMessageFromKey(
                                                                key: o.GroupId == s.GroupId
                                                                    ? "ZENAS"
                                                                    : "ERENIA"), type: 10));
                                                        arenaTeam.Add(item: new ArenaTeamMember(session: o.Session,
                                                            arenaTeamType: o.GroupId == s.GroupId
                                                                ? ArenaTeamType.Zenas
                                                                : ArenaTeamType.Erenia,
                                                            order: null));
                                                        o.Session.SendPacket(
                                                            packet: o.Session.Character.GenerateTaP(tatype: 0,
                                                                showOponent: false));

                                                        obs2 = Observable
                                                            .Interval(period: TimeSpan.FromMilliseconds(value: 100))
                                                            .Subscribe(
                                                                onNext: start3 =>
                                                                {
                                                                    var resettap = false;
                                                                    map.MapDesignObjects.ToList().ForEach(action: e =>
                                                                    {
                                                                        if (e.ItemInstance.Design >= 4433 &&
                                                                            e.ItemInstance.Design <= 4435)
                                                                        {
                                                                            var chara = map
                                                                                .GetCharactersInRange(mapX: e.MapX,
                                                                                    mapY: e.MapY, distance: 0)
                                                                                .FirstOrDefault();
                                                                            if (chara != null)
                                                                            {
                                                                                resettap = true;
                                                                                var teammember =
                                                                                    arenaTeam.FirstOrDefault(
                                                                                        predicate: at =>
                                                                                            at.Session ==
                                                                                            chara.Session);
                                                                                if (teammember != null &&
                                                                                    !arenaTeam.Any(predicate: at =>
                                                                                        at.Order == e.ItemInstance
                                                                                            .ItemVNum -
                                                                                        4433 &&
                                                                                        at.ArenaTeamType ==
                                                                                        (e.MapX == 120
                                                                                            ? ArenaTeamType.Erenia
                                                                                            : ArenaTeamType.Zenas)))
                                                                                {
                                                                                    if (teammember.Order != null)
                                                                                    {
                                                                                        var obj =
                                                                                            map.MapDesignObjects
                                                                                                .FirstOrDefault(
                                                                                                    predicate: mapobj =>
                                                                                                        mapobj
                                                                                                            .ItemInstance
                                                                                                            .ItemVNum ==
                                                                                                        e.ItemInstance
                                                                                                            .ItemVNum &&
                                                                                                        e.MapX ==
                                                                                                        (teammember
                                                                                                             .ArenaTeamType ==
                                                                                                         ArenaTeamType
                                                                                                             .Erenia
                                                                                                            ? 120
                                                                                                            : 19));
                                                                                        if (obj != null)
                                                                                            obj.ItemInstance.Design =
                                                                                                obj.ItemInstance
                                                                                                    .ItemVNum;
                                                                                    }

                                                                                    teammember.Order =
                                                                                        (byte)(e.ItemInstance
                                                                                            .ItemVNum - 4433);
                                                                                }
                                                                            }
                                                                        }
                                                                        else if (e.ItemInstance.Design == 4436)
                                                                        {
                                                                            if (!map.GetCharactersInRange(mapX: e.MapX,
                                                                                mapY: e.MapY, distance: 0).Any())
                                                                            {
                                                                                resettap = true;
                                                                                var teammember =
                                                                                    arenaTeam.FirstOrDefault(
                                                                                        predicate: at =>
                                                                                            at.Order == e.ItemInstance
                                                                                                .ItemVNum - 4433 &&
                                                                                            at.ArenaTeamType ==
                                                                                            (e.MapX == 120
                                                                                                ? ArenaTeamType.Erenia
                                                                                                : ArenaTeamType.Zenas));
                                                                                if (teammember != null)
                                                                                    teammember.Order = null;
                                                                            }
                                                                        }

                                                                        if (!arenaTeam.Any(predicate: at =>
                                                                            at.Order == e.ItemInstance.ItemVNum -
                                                                            4433 &&
                                                                            at.ArenaTeamType == (e.MapX == 120
                                                                                ? ArenaTeamType.Erenia
                                                                                : ArenaTeamType.Zenas)))
                                                                        {
                                                                            if (e.ItemInstance.Design != 4436) return;
                                                                            e.ItemInstance.Design =
                                                                                e.ItemInstance.ItemVNum;
                                                                            map.Broadcast(
                                                                                packet: e.GenerateEffect(
                                                                                    removed: false));
                                                                        }
                                                                        else if (e.ItemInstance.Design != 4436)
                                                                        {
                                                                            e.ItemInstance.Design = 4436;
                                                                            map.Broadcast(
                                                                                packet: e.GenerateEffect(
                                                                                    removed: false));
                                                                        }
                                                                    });

                                                                    if (resettap)
                                                                        arenaTeam.ToList().ForEach(action: arenauser =>
                                                                        {
                                                                            arenauser.Session.SendPacket(
                                                                                packet: arenauser.Session.Character
                                                                                    .GenerateTaP(tatype: 2,
                                                                                        showOponent: false));
                                                                        });
                                                                });
                                                    });
                                            });
                                    });
                                    Observable.Timer(
                                            dueTime: TimeSpan.FromSeconds(value: map.InstanceBag.Clock
                                                .TotalSecondsAmount))
                                        .Subscribe(onNext: start =>
                                        {
                                            obs2.Dispose();
                                            arenaTeam.ToList().ForEach(action: arenauser =>
                                            {
                                                if (arenauser.Order == null)
                                                    for (byte x = 0; x < 3; x++)
                                                        if (!arenaTeam.Any(predicate: at =>
                                                            at.ArenaTeamType == arenauser.ArenaTeamType &&
                                                            at.Order == x))
                                                            arenauser.Order = x;
                                                arenauser.Session.SendPacket(packet: $"ta_pn {arenauser.Order + 1}");
                                            });
                                            map.MapDesignObjects.ToList().ForEach(action: md =>
                                                map.Broadcast(packet: md.GenerateEffect(removed: true)));
                                            map.MapDesignObjects.Clear();

                                            arenaTeam.ToList().ForEach(action: arenauser =>
                                                arenauser.Session.SendPacket(
                                                    packet: arenauser.Session.Character.GenerateTaP(tatype: 2,
                                                        showOponent: true)));

                                            var newround1 = true;
                                            var newround2 = true;
                                            var count1 = 0;
                                            var count2 = 0;
                                            var obs7 = obs4;

                                            ArenaTeamMember beforetm = null;
                                            ArenaTeamMember beforetm2 = null;

                                            obs4 = Observable.Interval(period: TimeSpan.FromMilliseconds(value: 500))
                                                .Subscribe(
                                                    onNext: start3 =>
                                                    {
                                                        var ereniacount = arenaTeam.Count(predicate: at =>
                                                            at.Dead && at.ArenaTeamType == ArenaTeamType.Erenia);
                                                        var zenascount = arenaTeam.Count(predicate: at =>
                                                            at.Dead && at.ArenaTeamType == ArenaTeamType.Zenas);
                                                        if (ServerManager.Instance.ArenaTeams.ToList()
                                                                .Find(match: a =>
                                                                    a.Any(predicate: b =>
                                                                        arenaTeam.Contains(value: b))) is
                                                            ConcurrentBag<ArenaTeamMember> updatedTeam)
                                                        {
                                                            arenaTeam = updatedTeam;
                                                        }
                                                        else
                                                        {
                                                            arenaTeam.Clear();
                                                            obs2.Dispose();
                                                            obs3.Dispose();
                                                            obs5.Dispose();
                                                            obs7?.Dispose();
                                                            map.Dispose();
                                                            ServerManager.Instance.ArenaMembers.RemoveAll(match: o =>
                                                                o.GroupId == member.GroupId || o.GroupId == s.GroupId);
                                                            obs4.Dispose();
                                                            return;
                                                        }

                                                        if (count1 != ereniacount || count2 != zenascount)
                                                        {
                                                            if (count1 != ereniacount) newround1 = true;
                                                            if (count2 != zenascount) newround2 = true;
                                                            count1 = ereniacount;
                                                            count2 = zenascount;
                                                        }

                                                        var tm = arenaTeam.OrderBy(keySelector: tm3 => tm3.Order)
                                                            .FirstOrDefault(predicate: tm3 =>
                                                                tm3.ArenaTeamType == ArenaTeamType.Erenia && !tm3.Dead);
                                                        var tm2 = arenaTeam.OrderBy(keySelector: tm3 => tm3.Order)
                                                            .FirstOrDefault(predicate: tm3 =>
                                                                tm3.ArenaTeamType == ArenaTeamType.Zenas && !tm3.Dead);

                                                        if (tm == beforetm && tm2 == beforetm2) return;

                                                        if (tm != null) beforetm = tm;
                                                        if (tm2 != null) beforetm2 = tm2;

                                                        if (!newround1 && !newround2 && tm != null && tm2 != null)
                                                            return;

                                                        var timeToWait = 0;

                                                        try
                                                        {
                                                            timeToWait =
                                                                tm?.Order == arenaTeam
                                                                    .OrderBy(keySelector: tm3 => tm3.Order)
                                                                    .FirstOrDefault(predicate: tm3 =>
                                                                        tm3.ArenaTeamType == ArenaTeamType.Erenia)
                                                                    .Order &&
                                                                tm2?.Order == arenaTeam
                                                                    .OrderBy(keySelector: tm3 => tm3.Order)
                                                                    .FirstOrDefault(predicate: tm3 =>
                                                                        tm3.ArenaTeamType == ArenaTeamType.Zenas).Order
                                                                    ? 0
                                                                    : 3;
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Logger.Error(data: $"Talent Arena error: {ex}");
                                                            return;
                                                        }

                                                        map.InstanceBag.Clock.TotalSecondsAmount = 300 + timeToWait;
                                                        map.InstanceBag.Clock.SecondsRemaining = 3000 + timeToWait * 10;

                                                        arenaTeam.ToList().ForEach(action: friends =>
                                                        {
                                                            friends.Session.SendPacket(
                                                                packet: friends.Session.Character.GenerateTaM(type: 2));
                                                            friends.Session.SendPacket(
                                                                packet: friends.Session.Character.GenerateTaM(type: 3));
                                                        });
                                                        map.Sessions
                                                            .Except(
                                                                second: arenaTeam.Select(selector: ss => ss.Session))
                                                            .ToList()
                                                            .ForEach(action: o =>
                                                            {
                                                                o.SendPacket(
                                                                    packet: tm2?.Session.Character
                                                                        .GenerateTaM(type: 2));
                                                                o.SendPacket(
                                                                    packet: tm2?.Session.Character
                                                                        .GenerateTaM(type: 3));
                                                            });

                                                        obs6?.Dispose();
                                                        obs6 = Observable
                                                            .Timer(dueTime: TimeSpan.FromSeconds(value: map.InstanceBag
                                                                .Clock
                                                                .TotalSecondsAmount)).Subscribe(onNext: start4 =>
                                                            {
                                                                if (tm2 != null && tm != null)
                                                                {
                                                                    tm.Dead = true;
                                                                    tm2.Dead = true;
                                                                    tm.Session.Character.PositionX = 120;
                                                                    tm.Session.Character.PositionY = 39;
                                                                    tm2.Session.Character.PositionX = 19;
                                                                    tm2.Session.Character.PositionY = 40;
                                                                    map.Broadcast(client: tm2.Session,
                                                                        content: tm.Session.Character.GenerateTp());
                                                                    map.Broadcast(client: tm2.Session,
                                                                        content: tm2.Session.Character.GenerateTp());
                                                                    tm.Session.SendPacket(
                                                                        packet: UserInterfaceHelper.Instance
                                                                            .GenerateTaSt(
                                                                                watch: TalentArenaOptionType.Watch));
                                                                    tm2.Session.SendPacket(
                                                                        packet: UserInterfaceHelper.Instance
                                                                            .GenerateTaSt(
                                                                                watch: TalentArenaOptionType.Watch));

                                                                    var bufftodisable = new List<BuffType>
                                                                        {BuffType.Bad};

                                                                    tm.Session.Character.DisableBuffs(
                                                                        types: bufftodisable);
                                                                    tm.Session.Character.Hp =
                                                                        (int)tm.Session.Character.HPLoad();
                                                                    tm.Session.Character.Mp =
                                                                        (int)tm.Session.Character.MPLoad();
                                                                    tm.Session.SendPacket(
                                                                        packet: tm.Session.Character.GenerateStat());

                                                                    tm2.Session.Character.DisableBuffs(
                                                                        types: bufftodisable);
                                                                    tm2.Session.Character.Hp =
                                                                        (int)tm2.Session.Character.HPLoad();
                                                                    tm2.Session.Character.Mp =
                                                                        (int)tm2.Session.Character.MPLoad();
                                                                    tm2.Session.SendPacket(packet: tm2.Session.Character
                                                                        .GenerateStat());

                                                                    arenaTeam
                                                                        .Replace(predicate: friends =>
                                                                            friends.ArenaTeamType == tm.ArenaTeamType)
                                                                        .ToList().ForEach(action: friends =>
                                                                        {
                                                                            friends.Session.SendPacket(packet: friends
                                                                                .Session
                                                                                .Character.GenerateTaFc(type: 0));
                                                                        });
                                                                }

                                                                newround1 = true;
                                                                newround2 = true;
                                                                arenaTeam.ToList().ForEach(action: arenauser =>
                                                                {
                                                                    arenauser.Session.SendPacket(
                                                                        packet: arenauser.Session.Character
                                                                            .GenerateTaP(tatype: 2, showOponent: true));
                                                                });
                                                            });

                                                        if (tm != null && tm2 != null)
                                                        {
                                                            map.IsPvp = false;

                                                            arenaTeam.ToList().ForEach(action: friends =>
                                                            {
                                                                friends.Session.SendPacket(
                                                                    packet: friends.ArenaTeamType ==
                                                                            ArenaTeamType.Erenia
                                                                        ? tm.Session.Character.GenerateTaFc(type: 0)
                                                                        : tm2.Session.Character.GenerateTaFc(type: 0));
                                                            });

                                                            map.Sessions
                                                                .Except(second: arenaTeam.Select(selector: ss =>
                                                                    ss.Session)).ToList()
                                                                .ForEach(action: ss =>
                                                                {
                                                                    ss.SendPacket(
                                                                        packet: tm.Session.Character.GenerateTaFc(
                                                                            type: 0));
                                                                    ss.SendPacket(
                                                                        packet: tm2.Session.Character.GenerateTaFc(
                                                                            type: 1));
                                                                });

                                                            if (newround1)
                                                                Observable.Timer(
                                                                        dueTime: TimeSpan.FromSeconds(
                                                                            value: timeToWait))
                                                                    .Subscribe(onNext: start5 =>
                                                                    {
                                                                        map.Broadcast(
                                                                            packet: tm.Session.Character.GenerateTaFc(
                                                                                type: 1));
                                                                        tm.Session.Character.PositionX = 87;
                                                                        tm.Session.Character.PositionY = 39;
                                                                        map.Broadcast(client: tm.Session,
                                                                            content: tm.Session.Character.GenerateTp());
                                                                    });
                                                            if (newround2)
                                                                Observable.Timer(
                                                                        dueTime: TimeSpan.FromSeconds(
                                                                            value: timeToWait))
                                                                    .Subscribe(onNext: start5 =>
                                                                    {
                                                                        tm2.Session.Character.PositionX = 56;
                                                                        tm2.Session.Character.PositionY = 40;
                                                                        map.Broadcast(client: tm2.Session,
                                                                            content: tm2.Session.Character
                                                                                .GenerateTp());
                                                                    });
                                                            Observable.Timer(
                                                                    dueTime: TimeSpan.FromSeconds(value: timeToWait))
                                                                .Subscribe(
                                                                    onNext: start5 =>
                                                                    {
                                                                        arenaTeam.Replace(predicate: at =>
                                                                                at.LastSummoned != null)
                                                                            .ToList().ForEach(action: at =>
                                                                            {
                                                                                at.LastSummoned = null;
                                                                                at.Session.Character.PositionX =
                                                                                    at.ArenaTeamType ==
                                                                                    ArenaTeamType.Erenia
                                                                                        ? (short)120
                                                                                        : (short)19;
                                                                                at.Session.Character.PositionY =
                                                                                    at.ArenaTeamType ==
                                                                                    ArenaTeamType.Erenia
                                                                                        ? (short)39
                                                                                        : (short)40;
                                                                                at.Session.CurrentMapInstance.Broadcast(
                                                                                    packet: at.Session.Character
                                                                                        .GenerateTp());
                                                                                at.Session.SendPacket(
                                                                                    packet: UserInterfaceHelper.Instance
                                                                                        .GenerateTaSt(
                                                                                            watch: TalentArenaOptionType
                                                                                                .Watch));

                                                                                var bufftodisable = new List<BuffType>
                                                                                    {BuffType.Bad};
                                                                                at.Session.Character
                                                                                    .DisableBuffs(types: bufftodisable);
                                                                                at.Session.Character.Hp =
                                                                                    (int)at.Session.Character.HPLoad();
                                                                                at.Session.Character.Mp =
                                                                                    (int)at.Session.Character.MPLoad();
                                                                            });

                                                                        tm.Session.SendPacket(
                                                                            packet: UserInterfaceHelper.Instance
                                                                                .GenerateTaSt(
                                                                                    watch: TalentArenaOptionType.Call));
                                                                        tm2.Session.SendPacket(
                                                                            packet: UserInterfaceHelper.Instance
                                                                                .GenerateTaSt(
                                                                                    watch: TalentArenaOptionType.Call));

                                                                        map.Broadcast(packet: "ta_s");
                                                                        Observable.Timer(
                                                                                dueTime: TimeSpan.FromSeconds(value: 5))
                                                                            .Subscribe(onNext: start4 =>
                                                                            {
                                                                                map.IsPvp = true;
                                                                            });
                                                                    });
                                                        }
                                                        else
                                                        {
                                                            switch (tm)
                                                            {
                                                                case null when tm2 == null:
                                                                    if (!WinLoseMsgSent)
                                                                    {
                                                                        map.Broadcast(
                                                                            packet: UserInterfaceHelper.GenerateMsg(
                                                                                message: Language.Instance
                                                                                    .GetMessageFromKey(
                                                                                        key: "EQUALITY"), type: 0));
                                                                        arenaTeam.ToList().ForEach(action: arenauser =>
                                                                        {
                                                                            arenauser.Session.SendPacket(
                                                                                packet: arenauser.Session.Character
                                                                                    .GenerateSay(
                                                                                        message: Language.Instance
                                                                                            .GetMessageFromKey(
                                                                                                key: "EQUALITY"),
                                                                                        type: 10));
                                                                            arenauser.Session.SendPacket(
                                                                                packet: arenauser.Session
                                                                                    .Character.GenerateTaF(
                                                                                        victoriousteam: 3));
                                                                        });
                                                                        map.Sessions
                                                                            .Except(second: arenamembers.Select(
                                                                                selector: x => x.Session))
                                                                            .ToList().ForEach(
                                                                                action: x =>
                                                                                {
                                                                                    var arenauser =
                                                                                        arenaTeam.FirstOrDefault(
                                                                                            predicate: se =>
                                                                                                se.Session != null);
                                                                                    if (arenauser == null) return;
                                                                                    x.SendPacket(
                                                                                        packet: arenauser.Session
                                                                                            .Character
                                                                                            .GenerateSay(
                                                                                                message: Language
                                                                                                    .Instance
                                                                                                    .GetMessageFromKey(
                                                                                                        key:
                                                                                                        "EQUALITY"),
                                                                                                type: 10));
                                                                                    x.SendPacket(packet: arenauser
                                                                                        .Session.Character
                                                                                        .GenerateTaF(
                                                                                            victoriousteam: 0));
                                                                                }
                                                                            );
                                                                        WinLoseMsgSent = true;
                                                                    }

                                                                    break;

                                                                case null:
                                                                    if (!WinLoseMsgSent)
                                                                    {
                                                                        map.Broadcast(
                                                                            packet: UserInterfaceHelper.GenerateMsg(
                                                                                message: Language.Instance
                                                                                    .GetMessageFromKey(
                                                                                        key: "VICTORIOUS_ZENAS"),
                                                                                type: 0));

                                                                        arenaTeam.ToList().ForEach(action: arenauser =>
                                                                        {
                                                                            Observable.Timer(
                                                                                    dueTime: TimeSpan.FromSeconds(
                                                                                        value: 2))
                                                                                .Subscribe(onNext: WinZenas =>
                                                                                {
                                                                                    SendRewards(member: arenauser,
                                                                                        win: arenauser.ArenaTeamType ==
                                                                                             ArenaTeamType.Zenas);
                                                                                    arenauser.Session.SendPacket(
                                                                                        packet: arenauser.Session
                                                                                            .Character
                                                                                            .GenerateSay(
                                                                                                message: Language
                                                                                                    .Instance
                                                                                                    .GetMessageFromKey(
                                                                                                        key:
                                                                                                        "VICTORIOUS_ZENAS"),
                                                                                                type: 10));
                                                                                    arenauser.Session.SendPacket(
                                                                                        packet: arenauser.Session
                                                                                            .Character
                                                                                            .GenerateTaF(
                                                                                                victoriousteam: 1));
                                                                                });
                                                                        });
                                                                        WinLoseMsgSent = true;
                                                                    }

                                                                    break;

                                                                default:
                                                                    if (!WinLoseMsgSent)
                                                                    {
                                                                        map.Broadcast(
                                                                            packet: UserInterfaceHelper.GenerateMsg(
                                                                                message: Language.Instance
                                                                                    .GetMessageFromKey(
                                                                                        key: "VICTORIOUS_ERENIA"),
                                                                                type: 0));
                                                                        arenaTeam.ToList().ForEach(action: arenauser =>
                                                                        {
                                                                            SendRewards(member: arenauser,
                                                                                win: arenauser.ArenaTeamType ==
                                                                                     ArenaTeamType.Erenia);
                                                                            arenauser.Session.SendPacket(
                                                                                packet: arenauser.Session.Character
                                                                                    .GenerateSay(
                                                                                        message: Language.Instance
                                                                                            .GetMessageFromKey(
                                                                                                key:
                                                                                                "VICTORIOUS_ERENIA"),
                                                                                        type: 10));
                                                                            arenauser.Session.SendPacket(
                                                                                packet: arenauser.Session
                                                                                    .Character.GenerateTaF(
                                                                                        victoriousteam: 2));
                                                                        });
                                                                        WinLoseMsgSent = true;
                                                                    }

                                                                    break;
                                                            }

                                                            map.IsPvp = false;
                                                            obs3.Dispose();
                                                            obs2.Dispose();
                                                            obs7?.Dispose();
                                                            obs5.Dispose();
                                                            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 5))
                                                                .Subscribe(
                                                                    onNext:
                                                                    start4 => //Observable.Timer(TimeSpan.FromSeconds(30)).Subscribe(start4 =>
                                                                    {
                                                                        arenaTeam.ToList().ForEach(action: o =>
                                                                        {
                                                                            if (o.Session?.CurrentMapInstance
                                                                                    ?.MapInstanceType ==
                                                                                MapInstanceType.TalentArenaMapInstance)
                                                                            {
                                                                                o.Session.Character.GetSkills()
                                                                                    .ForEach(action: c =>
                                                                                        c.LastUse =
                                                                                            DateTime.Now.AddDays(
                                                                                                value: -1));

                                                                                o.Session.SendPacket(packet: o.Session
                                                                                    .Character
                                                                                    .GenerateSki());
                                                                                o.Session.SendPackets(packets: o.Session
                                                                                    .Character
                                                                                    .GenerateQuicklist());

                                                                                var bufftodisable = new List<BuffType>
                                                                                    {BuffType.Bad};
                                                                                o.Session.Character.DisableBuffs(
                                                                                    types: bufftodisable);
                                                                                o.Session.Character.RemoveBuff(
                                                                                    cardId: 491);
                                                                                ServerManager.Instance
                                                                                    .TeleportOnRandomPlaceInMap(
                                                                                        session: o.Session,
                                                                                        guid: ServerManager.Instance
                                                                                            .ArenaInstance
                                                                                            .MapInstanceId);
                                                                            }
                                                                        });
                                                                        map.Sessions.ToList().ForEach(action: sess =>
                                                                        {
                                                                            if (sess.CurrentMapInstance
                                                                                    ?.MapInstanceType ==
                                                                                MapInstanceType.TalentArenaMapInstance)
                                                                            {
                                                                                var bufftodisable = new List<BuffType>
                                                                                    {BuffType.Bad};
                                                                                sess.Character.DisableBuffs(
                                                                                    types: bufftodisable);
                                                                                sess.Character.RemoveBuff(cardId: 491);
                                                                                ServerManager.Instance
                                                                                    .TeleportOnRandomPlaceInMap(
                                                                                        session: sess,
                                                                                        guid: ServerManager.Instance
                                                                                            .ArenaInstance
                                                                                            .MapInstanceId);
                                                                            }
                                                                        });
                                                                        map.Dispose();
                                                                    });
                                                        }

                                                        newround1 = false;
                                                        newround2 = false;
                                                    });
                                        });
                                    ServerManager.Instance.ArenaMembers.ToList()
                                        .Where(predicate: o => o.GroupId == member.GroupId || o.GroupId == s.GroupId)
                                        .ToList()
                                        .ForEach(action: se =>
                                        {
                                            se.Session.SendPacket(
                                                packet: UserInterfaceHelper.GenerateBsInfo(mode: 2, title: 2,
                                                    time: 0, text: 0));
                                        });

                                    ServerManager.Instance.ArenaMembers.RemoveAll(match: o =>
                                        o.GroupId == member.GroupId || o.GroupId == s.GroupId);
                                }
                            }
                            else
                            {
                                if (s.GroupId == null)
                                {
                                    if (s.Time != -1)
                                    {
                                        s.Session.SendPacket(packet: UserInterfaceHelper.GenerateBsInfo(mode: 1,
                                            title: 2, time: s.Time, text: 7));
                                        s.Session.SendPacket(
                                            packet: s.Session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(key: "NO_TEAM_ARENA"),
                                                type: 10));
                                    }

                                    s.Time = 300;
                                    s.Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateBsInfo(mode: 1, title: 2, time: s.Time,
                                            text: 5));
                                    s.Session.SendPacket(
                                        packet: s.Session.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(key: "SEARCH_ARENA_TEAM"),
                                            type: 10));
                                }
                                else if (ServerManager.Instance.ArenaMembers.ToList()
                                    .Count(predicate: g => g.GroupId == s.GroupId) < 3)
                                {
                                    s.Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateBsInfo(mode: 1, title: 2, time: -1,
                                            text: 4));
                                    Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: time =>
                                    {
                                        s.Time = 300;
                                        s.Session.SendPacket(packet: UserInterfaceHelper.GenerateBsInfo(mode: 1,
                                            title: 2, time: s.Time, text: 8));
                                        s.Session.SendPacket(packet: s.Session.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(
                                                key: "RETRY_SEARCH_ARENA_TEAM"), type: 10));
                                    });
                                }
                                else
                                {
                                    s.Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateBsInfo(mode: 0, title: 2, time: -1,
                                            text: 3));
                                    Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: time =>
                                    {
                                        s.Time = 300;
                                        s.Session.SendPacket(packet: UserInterfaceHelper.GenerateBsInfo(mode: 0,
                                            title: 2, time: s.Time, text: 1));
                                        s.Session.SendPacket(packet: s.Session.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(
                                                key: "SEARCH_RIVAL_ARENA_TEAM"), type: 10));
                                    });
                                }
                            }
                        });
                    seconds++;
                }
            });

            CommunicationServiceClient.Instance.SendMessageToCharacter(message: new ScsCharacterMessage
            {
                SourceWorldId = ServerManager.Instance.WorldId,
                Message = UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "TALENT_ARENA_OPENED"),
                        arg0: ServerManager.Instance.ChannelId), type: 0),
                Type = MessageType.Broadcast
            });

            Observable.Timer(dueTime: TimeSpan.FromHours(value: 7)).Subscribe(onNext: start2 =>
            {
                ServerManager.Instance.StartedEvents.Remove(item: EventType.Talentarena);
                obs.Dispose();
            });
        }

        static void BuffTeam(List<ArenaTeamMember> team)
        {
            if (team.Any(predicate: ch => ch.Session?.Character.Class == ClassType.Archer) &&
                team.Any(predicate: ch => ch.Session?.Character.Class == ClassType.Magician) &&
                team.Any(predicate: ch => ch.Session?.Character.Class == ClassType.Swordsman))
                //buff team
                team.ForEach(action: sess =>
                {
                    if (sess.Session?.Character.Buff.Any(predicate: bf => bf.Card.CardId == 491) == false)
                        sess.Session?.Character.AddBuff(indicator: new Buff.Buff(id: 491, level: 1),
                            sender: sess.Session?.Character.BattleEntity);
                });

            if (team.Count(predicate: ch => ch.Session?.Character.Class == ClassType.Archer) == 3 ||
                team.Count(predicate: ch => ch.Session?.Character.Class == ClassType.Magician) == 3 ||
                team.Count(predicate: ch => ch.Session?.Character.Class == ClassType.Swordsman) == 3)
                //debuff team
                team.ForEach(action: sess =>
                {
                    if (sess.Session?.Character.Buff.Any(predicate: bf => bf.Card.CardId == 490) == false)
                        sess.Session?.Character.AddBuff(indicator: new Buff.Buff(id: 490, level: 1),
                            sender: sess.Session?.Character.BattleEntity);
                });
        }

        static void SendRewards(ArenaTeamMember member, bool win)
        {
            ItemInstance SpInstance = null;
            if (member.Session.Character.Inventory != null && member.Session.Character.UseSp)
                SpInstance =
                    member.Session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                        type: InventoryType.Wear);
            if (win)
            {
                member.Session.Character.GetXp(
                    val: RewardsHelper.ArenaXpReward(characterLevel: member.Session.Character.Level), applyRate: false);
                member.Session.Character.GetJobExp(
                    val: RewardsHelper.ArenaXpReward(characterLevel: SpInstance != null
                        ? SpInstance.SpLevel
                        : member.Session.Character.JobLevel), applyRate: false);
                member.Session.Character.GetReputation(amount: 500);
                member.Session.Character.GiftAdd(itemVNum: 2800, amount: 1);
                member.Session.Character.GetGold(val: member.Session.Character.Level * 1000);
                member.Session.Character.TalentWin++;
            }
            else
            {
                member.Session.Character.GetXp(
                    val: RewardsHelper.ArenaXpReward(characterLevel: member.Session.Character.Level) / 2,
                    applyRate: false);
                member.Session.Character.GetJobExp(
                    val: RewardsHelper.ArenaXpReward(characterLevel: SpInstance != null
                        ? SpInstance.SpLevel
                        : member.Session.Character.JobLevel) / 2, applyRate: false);
                member.Session.Character.GetReputation(amount: 200);
                member.Session.Character.GiftAdd(itemVNum: 2801, amount: 3);
                member.Session.Character.GetGold(val: member.Session.Character.Level * 500);
                member.Session.Character.TalentLose++;
            }
        }

        #endregion
    }
}