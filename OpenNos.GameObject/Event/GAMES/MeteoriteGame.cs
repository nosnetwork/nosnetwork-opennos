﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event.GAMES
{
    public static class MeteoriteGame
    {
        #region Methods

        public static void GenerateMeteoriteGame()
        {
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "METEORITE_MINUTES"),
                        arg0: 5), type: 0));
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "METEORITE_MINUTES"),
                        arg0: 5), type: 1));
            Thread.Sleep(millisecondsTimeout: 4 * 60 * 1000);
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "METEORITE_MINUTES"),
                        arg0: 1), type: 0));
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "METEORITE_MINUTES"),
                        arg0: 1), type: 1));
            Thread.Sleep(millisecondsTimeout: 30 * 1000);
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "METEORITE_SECONDS"),
                        arg0: 30), type: 0));
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "METEORITE_SECONDS"),
                        arg0: 30), type: 1));
            Thread.Sleep(millisecondsTimeout: 20 * 1000);
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "METEORITE_SECONDS"),
                        arg0: 10), type: 0));
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "METEORITE_SECONDS"),
                        arg0: 10), type: 1));
            Thread.Sleep(millisecondsTimeout: 10 * 1000);
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "METEORITE_STARTED"), type: 1));
            ServerManager.Instance.Broadcast(packet: "qnaml 100 #guri^506 The Meteorite Game is starting! Join now!");
            ServerManager.Instance.EventInWaiting = true;
            Thread.Sleep(millisecondsTimeout: 30 * 1000);
            ServerManager.Instance.Sessions.Where(predicate: s => s.Character?.IsWaitingForEvent == false).ToList()
                .ForEach(action: s => s.SendPacket(packet: "esf"));
            ServerManager.Instance.EventInWaiting = false;
            var sessions = ServerManager.Instance.Sessions.Where(predicate: s =>
                s.Character?.IsWaitingForEvent == true &&
                s.Character.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance);

            var map = ServerManager.GenerateMapInstance(mapId: 2004, type: MapInstanceType.EventGameInstance,
                mapclock: new InstanceBag());
            if (map != null)
            {
                foreach (var sess in sessions)
                    ServerManager.Instance.TeleportOnRandomPlaceInMap(session: sess, guid: map.MapInstanceId);

                ServerManager.Instance.Sessions.Where(predicate: s => s.Character != null).ToList()
                    .ForEach(action: s => s.Character.IsWaitingForEvent = false);
                ServerManager.Instance.StartedEvents.Remove(item: EventType.Meteoritegame);

                var task = new MeteoriteGameThread();
                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 10)).Subscribe(onNext: x => task.Run(map: map));
            }
        }

        #endregion

        #region Classes

        public class MeteoriteGameThread
        {
            #region Members

            MapInstance _map;

            #endregion

            #region Methods

            public void Run(MapInstance map)
            {
                _map = map;

                foreach (var session in _map.Sessions)
                {
                    ServerManager.Instance.TeleportOnRandomPlaceInMap(session: session, guid: map.MapInstanceId);
                    if (session.Character.IsVehicled) session.Character.RemoveVehicle();
                    if (session.Character.UseSp)
                    {
                        session.Character.LastSp =
                            (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(value: -50)).TotalSeconds;
                        var specialist =
                            session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                                type: InventoryType.Wear);
                        if (specialist != null) session.Character.RemoveSp(vnum: specialist.ItemVNum, forced: true);
                    }

                    session.Character.Speed = 12;
                    session.Character.IsVehicled = true;
                    session.Character.IsCustomSpeed = true;
                    session.Character.Morph = 1156;
                    session.Character.ArenaWinner = 0;
                    session.Character.MorphUpgrade = 0;
                    session.Character.MorphUpgrade2 = 0;
                    session.SendPacket(packet: session.Character.GenerateCond());
                    session.Character.LastSpeedChange = DateTime.Now;
                    session.CurrentMapInstance?.Broadcast(packet: session.Character.GenerateCMode());
                }

                var i = 0;

                while (_map?.Sessions?.Any() == true) RunRound(number: i++);

                //ended
            }

            static IEnumerable<Tuple<short, int, short, short>> GenerateDrop(Map map, short vnum,
                int amountofdrop, int amount)
            {
                var dropParameters = new List<Tuple<short, int, short, short>>();
                for (var i = 0; i < amountofdrop; i++)
                {
                    var cell = map.GetRandomPosition();
                    dropParameters.Add(item: new Tuple<short, int, short, short>(item1: vnum, item2: amount,
                        item3: cell.X, item4: cell.Y));
                }

                return dropParameters;
            }

            void RunRound(int number)
            {
                var amount = 120 + 60 * number;

                var i = amount;
                while (i != 0)
                {
                    SpawnCircle();
                    Thread.Sleep(millisecondsTimeout: 60000 / amount);
                    i--;
                }

                Thread.Sleep(millisecondsTimeout: 5000);
                _map.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "METEORITE_ROUND"),
                        arg0: number + 1), type: 0));
                Thread.Sleep(millisecondsTimeout: 5000);

                // Your dropped reward
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 1046, amountofdrop: 20,
                    amount: 200 * (number + 1 > 10 ? 10 : number + 1)).ToList());
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 1030, amountofdrop: 10,
                    amount: 3 * (number + 1 > 10 ? 10 : number + 1)).ToList());
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 2282, amountofdrop: 10,
                    amount: 3 * (number + 1 > 10 ? 10 : number + 1)).ToList());
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 2514, amountofdrop: 5,
                    amount: 1 * (number + 1 > 10 ? 10 : number + 1)).ToList());
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 2515, amountofdrop: 5,
                    amount: 1 * (number + 1 > 10 ? 10 : number + 1)).ToList());
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 2516, amountofdrop: 5,
                    amount: 1 * (number + 1 > 10 ? 10 : number + 1)).ToList());
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 2517, amountofdrop: 5,
                    amount: 1 * (number + 1 > 10 ? 10 : number + 1)).ToList());
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 2518, amountofdrop: 5,
                    amount: 1 * (number + 1 > 10 ? 10 : number + 1)).ToList());
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 2519, amountofdrop: 5,
                    amount: 1 * (number + 1 > 10 ? 10 : number + 1)).ToList());
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 2520, amountofdrop: 5,
                    amount: 1 * (number + 1 > 10 ? 10 : number + 1)).ToList());
                _map.DropItems(list: GenerateDrop(map: _map.Map, vnum: 2521, amountofdrop: 5,
                    amount: 1 * (number + 1 > 10 ? 10 : number + 1)).ToList());

                Thread.Sleep(millisecondsTimeout: 30000);
            }

            void SpawnCircle()
            {
                if (_map != null)
                {
                    var cell = _map.Map.GetRandomPosition();

                    var circleId = _map.GetNextMonsterId();

                    var circle = new MapMonster
                    {
                        MonsterVNum = 2018,
                        MapX = cell.X,
                        MapY = cell.Y,
                        MapMonsterId = circleId,
                        IsHostile = false,
                        IsMoving = false,
                        ShouldRespawn = false
                    };
                    circle.Initialize(currentMapInstance: _map);
                    circle.NoAggresiveIcon = true;
                    _map.AddMonster(monster: circle);
                    _map.Broadcast(packet: circle.GenerateIn());
                    _map.Broadcast(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Monster,
                        callerId: circleId, effectId: 4660));
                    Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 3)).Subscribe(onNext: observer =>
                    {
                        if (_map != null)
                        {
                            _map.Broadcast(packet: StaticPacketHelper.SkillUsed(type: UserType.Monster,
                                callerId: circleId, secondaryType: 3, targetId: circleId, skillVNum: 1220,
                                cooldown: 220, attackAnimation: 0, skillEffect: 4983, x: cell.X, y: cell.Y,
                                isAlive: true, health: 0, damage: 65535, hitmode: 0, skillType: 0));
                            foreach (var character in _map.GetCharactersInRange(mapX: cell.X, mapY: cell.Y, distance: 2)
                            )
                            {
                                if (!_map.Sessions.Skip(count: 3).Any())
                                {
                                    // Your reward for the last three living players
                                }

                                character.IsCustomSpeed = false;
                                character.RemoveVehicle();
                                character.GetDamage(damage: 655350, damager: character.BattleEntity);
                                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000)).Subscribe(onNext: o =>
                                    ServerManager.Instance.AskRevive(characterId: character.CharacterId));
                            }

                            _map.RemoveMonster(monsterToRemove: circle);
                            _map.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                                callerId: circle.MapMonsterId));
                        }
                    });
                }
            }

            #endregion
        }

        #endregion
    }
}