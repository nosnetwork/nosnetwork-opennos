﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event
{
    public class Act4RaidThread
    {
        #region Members

        readonly List<long> _wonFamilies = new List<long>();

        short _bossMapId = 136;

        bool _bossMove;

        short _bossVNum = 563;

        short _bossX = 55;

        short _bossY = 11;

        short _destPortalX = 55;

        short _destPortalY = 80;

        byte _faction;

        short _mapId = 135;

        MapInstanceType _raidType;

        short _sourcePortalX = 146;

        short _sourcePortalY = 43;

        int _raidTime = 3600;

        const int _interval = 60;

        #endregion

        #region Methods

        public void Run(MapInstanceType raidType, byte faction)
        {
            _raidType = raidType;
            _faction = faction;
            switch (raidType)
            {
                // Morcos is default
                case MapInstanceType.Act4Hatus:
                    _mapId = 137;
                    _bossMapId = 138;
                    _bossVNum = 577;
                    _bossX = 36;
                    _bossY = 18;
                    _sourcePortalX = 37;
                    _sourcePortalY = 156;
                    _destPortalX = 36;
                    _destPortalY = 58;
                    _bossMove = false;
                    break;

                case MapInstanceType.Act4Calvina:
                    _mapId = 139;
                    _bossMapId = 140;
                    _bossVNum = 629;
                    _bossX = 26;
                    _bossY = 26;
                    _sourcePortalX = 194;
                    _sourcePortalY = 17;
                    _destPortalX = 9;
                    _destPortalY = 41;
                    _bossMove = true;
                    break;

                case MapInstanceType.Act4Berios:
                    _mapId = 141;
                    _bossMapId = 142;
                    _bossVNum = 624;
                    _bossX = 29;
                    _bossY = 29;
                    _sourcePortalX = 188;
                    _sourcePortalY = 96;
                    _destPortalX = 29;
                    _destPortalY = 54;
                    _bossMove = true;
                    break;
            }

#if DEBUG
            _raidTime = 1800;
#endif

            //Run once to load everything in place
            refreshRaid(remaining: _raidTime);

            ServerManager.Instance.Act4RaidStart = DateTime.Now;

            while (_raidTime > 0)
            {
                _raidTime -= _interval;
                Thread.Sleep(millisecondsTimeout: _interval * 1000);
                refreshRaid(remaining: _raidTime);
            }

            endRaid();
        }

        void endRaid()
        {
            foreach (var fam in ServerManager.Instance.FamilyList.GetAllItems())
            {
                if (fam.Act4Raid != null)
                {
                    EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.Act4Raid,
                        eventActionType: EventActionType.DISPOSEMAP, param: null));
                    fam.Act4Raid = null;
                }

                if (fam.Act4RaidBossMap != null)
                {
                    EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.Act4RaidBossMap,
                        eventActionType: EventActionType.DISPOSEMAP,
                        param: null));
                    fam.Act4RaidBossMap = null;
                }
            }

            var bitoren = ServerManager.GetMapInstance(id: ServerManager.GetBaseMapInstanceIdByMapId(mapId: 134));

            foreach (var portal in bitoren.Portals.ToList()
                .Where(predicate: s => s.Type.Equals(obj: 10) || s.Type.Equals(obj: 11)))
            {
                portal.IsDisabled = true;
                bitoren.Broadcast(packet: portal.GenerateGp());
                bitoren.Portals.Remove(item: portal);
            }

            bitoren.Portals.RemoveAll(match: s => s.Type.Equals(obj: 10));
            bitoren.Portals.RemoveAll(match: s => s.Type.Equals(obj: 11));
            switch (_faction)
            {
                case 1:
                    ServerManager.Instance.Act4AngelStat.Mode = 0;
                    ServerManager.Instance.Act4AngelStat.IsMorcos = false;
                    ServerManager.Instance.Act4AngelStat.IsHatus = false;
                    ServerManager.Instance.Act4AngelStat.IsCalvina = false;
                    ServerManager.Instance.Act4AngelStat.IsBerios = false;
                    break;

                case 2:
                    ServerManager.Instance.Act4DemonStat.Mode = 0;
                    ServerManager.Instance.Act4DemonStat.IsMorcos = false;
                    ServerManager.Instance.Act4DemonStat.IsHatus = false;
                    ServerManager.Instance.Act4DemonStat.IsCalvina = false;
                    ServerManager.Instance.Act4DemonStat.IsBerios = false;
                    break;
            }

            ServerManager.Instance.StartedEvents.Remove(item: EventType.Act4Raid);

            foreach (var monster in Act4Raid.Guardians)
            {
                bitoren.Broadcast(
                    packet: StaticPacketHelper.Out(type: UserType.Monster, callerId: monster.MapMonsterId));
                bitoren.RemoveMonster(monsterToRemove: monster);
            }

            Act4Raid.Guardians.Clear();
        }

        void openRaid(Family fam)
        {
            fam.Act4RaidBossMap.OnCharacterDiscoveringMapEvents.Add(item: new Tuple<EventContainer, List<long>>(
                item1: new EventContainer(mapInstance: fam.Act4RaidBossMap,
                    eventActionType: EventActionType.STARTACT4RAIDWAVES, param: new List<long>()),
                item2: new List<long>()));
            var onDeathEvents = new List<EventContainer>
            {
                new EventContainer(mapInstance: fam.Act4RaidBossMap, eventActionType: EventActionType.THROWITEMS,
                    param: new Tuple<int, short, byte, int, int, short>(item1: _bossVNum, item2: 1046, item3: 10,
                        item4: 20000, item5: 20001, item6: 0)),
                new EventContainer(mapInstance: fam.Act4RaidBossMap, eventActionType: EventActionType.THROWITEMS,
                    param: new Tuple<int, short, byte, int, int, short>(item1: _bossVNum, item2: 1244, item3: 10,
                        item4: 5, item5: 6, item6: 0))
            };
            if (_raidType.Equals(obj: MapInstanceType.Act4Berios))
            {
                onDeathEvents.Add(item: new EventContainer(mapInstance: fam.Act4RaidBossMap,
                    eventActionType: EventActionType.THROWITEMS,
                    param: new Tuple<int, short, byte, int, int, short>(item1: _bossVNum, item2: 2395, item3: 3,
                        item4: 1, item5: 2, item6: 0)));
                onDeathEvents.Add(item: new EventContainer(mapInstance: fam.Act4RaidBossMap,
                    eventActionType: EventActionType.THROWITEMS,
                    param: new Tuple<int, short, byte, int, int, short>(item1: _bossVNum, item2: 2396, item3: 5,
                        item4: 1, item5: 2, item6: 0)));
                onDeathEvents.Add(item: new EventContainer(mapInstance: fam.Act4RaidBossMap,
                    eventActionType: EventActionType.THROWITEMS,
                    param: new Tuple<int, short, byte, int, int, short>(item1: _bossVNum, item2: 2397, item3: 10,
                        item4: 1, item5: 2, item6: 0)));

                fam.Act4RaidBossMap.OnCharacterDiscoveringMapEvents.Add(item: new Tuple<EventContainer, List<long>>(
                    item1: new EventContainer(mapInstance: fam.Act4RaidBossMap,
                        eventActionType: EventActionType.SPAWNMONSTER,
                        param: new MonsterToSummon(vnum: 621, spawnCell: fam.Act4RaidBossMap.Map.GetRandomPosition(),
                            target: null, move: true,
                            hasDelay: 30)), item2: new List<long>()));
                fam.Act4RaidBossMap.OnCharacterDiscoveringMapEvents.Add(item: new Tuple<EventContainer, List<long>>(
                    item1: new EventContainer(mapInstance: fam.Act4RaidBossMap,
                        eventActionType: EventActionType.SPAWNMONSTER,
                        param: new MonsterToSummon(vnum: 622, spawnCell: fam.Act4RaidBossMap.Map.GetRandomPosition(),
                            target: null, move: true,
                            hasDelay: 205)), item2: new List<long>()));
                fam.Act4RaidBossMap.OnCharacterDiscoveringMapEvents.Add(item: new Tuple<EventContainer, List<long>>(
                    item1: new EventContainer(mapInstance: fam.Act4RaidBossMap,
                        eventActionType: EventActionType.SPAWNMONSTER,
                        param: new MonsterToSummon(vnum: 623, spawnCell: fam.Act4RaidBossMap.Map.GetRandomPosition(),
                            target: null, move: true,
                            hasDelay: 380)), item2: new List<long>()));
            }

            onDeathEvents.Add(item: new EventContainer(mapInstance: fam.Act4RaidBossMap,
                eventActionType: EventActionType.SCRIPTEND, param: (byte) 1));
            onDeathEvents.Add(item: new EventContainer(mapInstance: fam.Act4Raid,
                eventActionType: EventActionType.CHANGEPORTALTYPE,
                param: new Tuple<int, PortalType>(
                    item1: fam.Act4Raid.Portals
                        .Find(match: s => s.SourceX == _sourcePortalX && s.SourceY == _sourcePortalY && !s.IsDisabled)
                        .PortalId, item2: PortalType.Closed)));
            var bossMob = new MonsterToSummon(vnum: _bossVNum, spawnCell: new MapCell {X = _bossX, Y = _bossY},
                target: null, move: _bossMove)
            {
                DeathEvents = onDeathEvents
            };
            EventHelper.Instance.RunEvent(
                evt: new EventContainer(mapInstance: fam.Act4RaidBossMap, eventActionType: EventActionType.SPAWNMONSTER,
                    param: bossMob));
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.Act4Raid,
                eventActionType: EventActionType.SENDPACKET,
                param: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "ACT4RAID_OPEN"), type: 0)));

            //Observable.Timer(TimeSpan.FromSeconds(90)).Subscribe(o =>
            //{
            //TODO: Summon Monsters
            //});
        }

        void refreshRaid(int remaining)
        {
            Parallel.ForEach(source: ServerManager.Instance.FamilyList.GetAllItems(), body: fam =>
            {
                if (fam.Act4Raid == null)
                    fam.Act4Raid = ServerManager.GenerateMapInstance(mapId: _mapId, type: _raidType,
                        mapclock: new InstanceBag(), dropAllowed: true);
                if (fam.Act4RaidBossMap == null)
                    fam.Act4RaidBossMap = ServerManager.GenerateMapInstance(mapId: _bossMapId, type: _raidType,
                        mapclock: new InstanceBag());
                if (remaining <= 1800 && !fam.Act4Raid.Portals.Any(predicate: s =>
                    s.DestinationMapInstanceId.Equals(g: fam.Act4RaidBossMap.MapInstanceId)))
                {
                    fam.Act4Raid.CreatePortal(portal: new Portal
                    {
                        DestinationMapInstanceId = fam.Act4RaidBossMap.MapInstanceId,
                        DestinationX = _destPortalX,
                        DestinationY = _destPortalY,
                        SourceX = _sourcePortalX,
                        SourceY = _sourcePortalY
                    });
                    openRaid(fam: fam);
                }

                if (fam.Act4RaidBossMap.Monsters.Find(match: s =>
                        s.MonsterVNum == _bossVNum && s.CurrentHp / s.MaxHp < 0.5) !=
                    null
                    && fam.Act4Raid.Portals.Find(match: s =>
                        s.SourceX == _sourcePortalX && s.SourceY == _sourcePortalY && !s.IsDisabled) is Portal portal)
                {
                    EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.Act4Raid,
                        eventActionType: EventActionType.CHANGEPORTALTYPE,
                        param: new Tuple<int, PortalType>(
                            item1: fam.Act4Raid.Portals.Find(match: s =>
                                s.SourceX == _sourcePortalX && s.SourceY == _sourcePortalY && !s.IsDisabled).PortalId,
                            item2: PortalType.Closed)));
                    fam.Act4Raid.Broadcast(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "PORTAL_CLOSED"), type: 0));
                    fam.Act4RaidBossMap.Broadcast(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "PORTAL_CLOSED"), type: 0));
                }
            });
        }

        #endregion
    }
}