using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event.ACT4
{
    public static class CaligorRaid
    {
        #region Methods

        public static void Run()
        {
            var raidThread = new CaligorRaidThread();
            Observable.Timer(dueTime: TimeSpan.FromMinutes(value: 0)).Subscribe(onNext: x => raidThread.Run());
        }

        #endregion

        #region Properties

        public static int AngelDamage { get; set; }

        public static MapInstance CaligorMapInstance { get; set; }

        public static int DemonDamage { get; set; }

        public static bool IsLocked { get; set; }

        public static bool IsRunning { get; set; }

        public static int RemainingTime { get; set; }

        public static MapInstance UnknownLandMapInstance { get; set; }

        #endregion
    }

    public class CaligorRaidThread
    {
        #region Methods

        public void Run()
        {
            CaligorRaid.RemainingTime = 3600;
            const int interval = 3;

            CaligorRaid.CaligorMapInstance =
                ServerManager.GenerateMapInstance(mapId: 154, type: MapInstanceType.CaligorInstance,
                    mapclock: new InstanceBag());
            CaligorRaid.UnknownLandMapInstance =
                ServerManager.GetMapInstance(id: ServerManager.GetBaseMapInstanceIdByMapId(mapId: 153));

            CaligorRaid.CaligorMapInstance.CreatePortal(portal: new Portal
            {
                SourceMapId = 154,
                SourceX = 89,
                SourceY = 23,
                DestinationMapId = 153,
                DestinationX = 91,
                DestinationY = 35,
                Type = -1
            });
            CaligorRaid.CaligorMapInstance.CreatePortal(portal: new Portal
            {
                SourceMapId = 154,
                SourceX = 128,
                SourceY = 174,
                DestinationMapId = 153,
                DestinationX = 110,
                DestinationY = 159,
                Type = -1
            });
            CaligorRaid.CaligorMapInstance.CreatePortal(portal: new Portal
            {
                SourceMapId = 154,
                SourceX = 50,
                SourceY = 174,
                DestinationMapId = 153,
                DestinationX = 70,
                DestinationY = 159,
                Type = -1
            });

            CaligorRaid.UnknownLandMapInstance.CreatePortal(portal: new Portal
            {
                SourceMapId = 153,
                SourceX = 70,
                SourceY = 159,
                DestinationMapInstanceId = CaligorRaid.CaligorMapInstance.MapInstanceId,
                Type = -1
            });
            CaligorRaid.UnknownLandMapInstance.CreatePortal(portal: new Portal
            {
                SourceMapId = 153,
                SourceX = 110,
                SourceY = 159,
                DestinationMapInstanceId = CaligorRaid.CaligorMapInstance.MapInstanceId,
                Type = -1
            });
            CaligorRaid.UnknownLandMapInstance.CreatePortal(portal: new Portal
            {
                SourceMapId = 153,
                SourceX = 91,
                SourceY = 36,
                DestinationMapInstanceId = CaligorRaid.CaligorMapInstance.MapInstanceId,
                DestinationX = 89,
                DestinationY = 23,
                Type = -1
            });

            var onDeathEvents = new List<EventContainer>
            {
                new EventContainer(mapInstance: CaligorRaid.CaligorMapInstance,
                    eventActionType: EventActionType.Scriptend, param: (byte) 1)
            };

            var caligorMonster = new MapMonster
            {
                MonsterVNum = 2305,
                MapY = 90,
                MapX = 70,
                MapId = CaligorRaid.CaligorMapInstance.Map.MapId,
                Position = 2,
                IsMoving = true,
                MapMonsterId = CaligorRaid.CaligorMapInstance.GetNextMonsterId(),
                ShouldRespawn = false
            };
            caligorMonster.Initialize(currentMapInstance: CaligorRaid.CaligorMapInstance);
            CaligorRaid.CaligorMapInstance.AddMonster(monster: caligorMonster);

            var caligor = CaligorRaid.CaligorMapInstance.Monsters.Find(match: s => s.Monster.NpcMonsterVNum == 2305);

            if (caligor != null)
            {
                caligor.BattleEntity.OnDeathEvents = onDeathEvents;
                caligor.IsBoss = true;
            }

            ServerManager.Shout(message: Language.Instance.GetMessageFromKey(key: "CALIGOR_OPEN"), noAdminTag: true);

            RefreshRaid();

            ServerManager.Instance.Act4RaidStart = DateTime.Now;

            while (CaligorRaid.RemainingTime > 0)
            {
                CaligorRaid.RemainingTime -= interval;
                Thread.Sleep(millisecondsTimeout: interval * 1000);
                RefreshRaid();
            }

            EndRaid();
        }

        void EndRaid()
        {
            ServerManager.Shout(message: Language.Instance.GetMessageFromKey(key: "CALIGOR_END"), noAdminTag: true);

            foreach (var p in CaligorRaid.UnknownLandMapInstance.Portals
                .Where(predicate: s => s.DestinationMapInstanceId == CaligorRaid.CaligorMapInstance.MapInstanceId)
                .ToList())
            {
                p.IsDisabled = true;
                CaligorRaid.UnknownLandMapInstance.Broadcast(packet: p.GenerateGp());
                CaligorRaid.UnknownLandMapInstance.Portals.Remove(item: p);
            }

            foreach (var sess in CaligorRaid.CaligorMapInstance.Sessions.ToList())
            {
                ServerManager.Instance.ChangeMapInstance(characterId: sess.Character.CharacterId,
                    mapInstanceId: CaligorRaid.UnknownLandMapInstance.MapInstanceId, mapX: sess.Character.MapX,
                    mapY: sess.Character.MapY);
                Thread.Sleep(millisecondsTimeout: 100);
            }

            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: CaligorRaid.CaligorMapInstance,
                eventActionType: EventActionType.Disposemap,
                param: null));
            CaligorRaid.IsRunning = false;
            CaligorRaid.AngelDamage = 0;
            CaligorRaid.DemonDamage = 0;
            ServerManager.Instance.StartedEvents.Remove(item: EventType.Caligor);
        }

        void LockRaid()
        {
            foreach (var p in CaligorRaid.UnknownLandMapInstance.Portals
                .Where(predicate: s => s.DestinationMapInstanceId == CaligorRaid.CaligorMapInstance.MapInstanceId)
                .ToList())
            {
                p.IsDisabled = true;
                CaligorRaid.UnknownLandMapInstance.Broadcast(packet: p.GenerateGp());
                p.IsDisabled = false;
                p.Type = (byte)PortalType.Closed;
                CaligorRaid.UnknownLandMapInstance.Broadcast(packet: p.GenerateGp());
            }

            ServerManager.Shout(message: Language.Instance.GetMessageFromKey(key: "CALIGOR_LOCKED"), noAdminTag: true);
            CaligorRaid.IsLocked = true;
        }

        void RefreshRaid()
        {
            var maxHp = ServerManager.GetNpcMonster(npcVNum: 2305).MaxHp;
            CaligorRaid.CaligorMapInstance.Broadcast(packet: UserInterfaceHelper.GenerateChdm(maxhp: maxHp,
                angeldmg: CaligorRaid.AngelDamage,
                demondmg: CaligorRaid.DemonDamage, time: CaligorRaid.RemainingTime));

            if (maxHp / 10 * 8 < CaligorRaid.AngelDamage + CaligorRaid.DemonDamage && !CaligorRaid.IsLocked) LockRaid();
        }

        #endregion
    }
}