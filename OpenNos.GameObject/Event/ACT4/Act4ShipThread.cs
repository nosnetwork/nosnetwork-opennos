﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event
{
    public static class Act4ShipThread
    {
        #region Methods

        public static void Run(byte faction)
        {
            var map = ServerManager.GenerateMapInstance(mapId: 149,
                type: faction == 1 ? MapInstanceType.Act4ShipAngel : MapInstanceType.Act4ShipDemon,
                mapclock: new InstanceBag());
            var mapNpc1 = new MapNpc
            {
                NpcVNum = 613,
                MapNpcId = map.GetNextNpcId(),
                Dialog = 434,
                MapId = 149,
                MapX = 8,
                MapY = 28,
                IsMoving = false,
                Position = 1,
                IsSitting = false
            };
            mapNpc1.Initialize(currentMapInstance: map);
            map.AddNPC(npc: mapNpc1);
            var mapNpc2 = new MapNpc
            {
                NpcVNum = 540,
                MapNpcId = map.GetNextNpcId(),
                Dialog = 433,
                MapId = 149,
                MapX = 31,
                MapY = 28,
                IsMoving = false,
                Position = 3,
                IsSitting = false
            };
            mapNpc2.Initialize(currentMapInstance: map);
            map.AddNPC(npc: mapNpc2);
            while (true)
            {
                openShip();
                Thread.Sleep(millisecondsTimeout: 60 * 1000);
                map.Broadcast(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHIP_MINUTES"),
                            arg0: 4), type: 0));
                Thread.Sleep(millisecondsTimeout: 60 * 1000);
                map.Broadcast(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHIP_MINUTES"),
                            arg0: 3), type: 0));
                Thread.Sleep(millisecondsTimeout: 60 * 1000);
                map.Broadcast(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHIP_MINUTES"),
                            arg0: 2), type: 0));
                Thread.Sleep(millisecondsTimeout: 60 * 1000);
                map.Broadcast(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "SHIP_MINUTE"), type: 0));
                lockShip();
                Thread.Sleep(millisecondsTimeout: 30 * 1000);
                map.Broadcast(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHIP_SECONDS"),
                            arg0: 30), type: 0));
                Thread.Sleep(millisecondsTimeout: 20 * 1000);
                map.Broadcast(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHIP_SECONDS"),
                            arg0: 10), type: 0));
                Thread.Sleep(millisecondsTimeout: 10 * 1000);
                map.Broadcast(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "SHIP_SETOFF"), type: 0));
                var sessions = map.Sessions.Where(predicate: s => s?.Character != null).ToList();
                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 0))
                    .Subscribe(onNext: X => teleportPlayers(sessions: sessions));
            }
        }

        static void lockShip()
        {
            EventHelper.Instance.RunEvent(evt: new EventContainer(
                mapInstance: ServerManager.GetMapInstance(id: ServerManager.GetBaseMapInstanceIdByMapId(mapId: 145)),
                eventActionType: EventActionType.NPCSEFFECTCHANGESTATE, param: true));
        }

        static void openShip()
        {
            EventHelper.Instance.RunEvent(evt: new EventContainer(
                mapInstance: ServerManager.GetMapInstance(id: ServerManager.GetBaseMapInstanceIdByMapId(mapId: 145)),
                eventActionType: EventActionType.NPCSEFFECTCHANGESTATE, param: false));
        }

        static void teleportPlayers(List<ClientSession> sessions)
        {
            foreach (var session in sessions)
                if (ServerManager.Instance.IsAct4Online())
                {
                    switch (session.Character.Faction)
                    {
                        case FactionType.None:
                            ServerManager.Instance.ChangeMap(id: session.Character.CharacterId, mapId: 145, mapX: 51,
                                mapY: 41);
                            session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: "You need to be part of a faction to join Act 4"));
                            return;

                        case FactionType.Angel:
                            session.Character.MapId = 130;
                            session.Character.MapX = 12;
                            session.Character.MapY = 40;
                            break;

                        case FactionType.Demon:
                            session.Character.MapId = 131;
                            session.Character.MapX = 12;
                            session.Character.MapY = 40;
                            break;
                    }

                    session.Character.ChangeChannel(ip: ServerManager.Instance.Configuration.Act4IP,
                        port: ServerManager.Instance.Configuration.Act4Port, mode: 1);
                }
                else
                {
                    ServerManager.Instance.ChangeMap(id: session.Character.CharacterId, mapId: 145, mapX: 51, mapY: 41);
                    session.SendPacket(
                        packet: UserInterfaceHelper.GenerateInfo(
                            message: Language.Instance.GetMessageFromKey(key: "ACT4_OFFLINE")));
                }
        }

        #endregion
    }
}