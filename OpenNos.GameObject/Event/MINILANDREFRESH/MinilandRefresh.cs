﻿using System;
using System.Linq;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;

namespace OpenNos.GameObject.Event.MINILANDREFRESH
{
    public static class MinilandRefresh
    {
        #region Methods

        public static void GenerateMinilandEvent()
        {
            ServerManager.Instance.SaveAll();
            foreach (var chara in DaoFactory.CharacterDao.LoadAll())
            {
                var gen = DaoFactory.GeneralLogDao.LoadByAccount(accountId: null).LastOrDefault(predicate: s =>
                    s.LogData == nameof(MinilandRefresh) && s.LogType == "World" &&
                    s.Timestamp.Day == DateTime.Now.Day);
                var count = DaoFactory.GeneralLogDao.LoadByAccount(accountId: chara.AccountId).Count(predicate: s =>
                    s.LogData == "MINILAND" && s.Timestamp > DateTime.Now.AddDays(value: -1) &&
                    s.CharacterId == chara.CharacterId);

                var session = ServerManager.Instance.GetSessionByCharacterId(characterId: chara.CharacterId);
                if (session != null)
                {
                    session.Character.GetReputation(amount: 2 * count);
                    session.Character.MinilandPoint = 2000;
                }
                else if (CommunicationServiceClient.Instance.IsCharacterConnected(
                    worldGroup: ServerManager.Instance.ServerGroup,
                    characterId: chara.CharacterId))
                {
                    if (gen == null) chara.Reputation += 2 * count;
                    chara.MinilandPoint = 2000;
                    var chara2 = chara;
                    DaoFactory.CharacterDao.InsertOrUpdate(character: ref chara2);
                }
            }

            DaoFactory.GeneralLogDao.Insert(generalLog: new GeneralLogDto
            { LogData = nameof(MinilandRefresh), LogType = "World", Timestamp = DateTime.Now });
            ServerManager.Instance.StartedEvents.Remove(item: EventType.Minilandrefreshevent);
        }

        #endregion
    }
}