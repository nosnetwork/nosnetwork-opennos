﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event.INSTANTBATTLE
{
    public static class InstantBattle
    {
        #region Methods

        public static void GenerateInstantBattle()
        {
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES"),
                        arg0: 5), type: 0));
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES"),
                        arg0: 5), type: 1));
            Thread.Sleep(millisecondsTimeout: 4 * 60 * 1000);
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES"),
                        arg0: 1), type: 0));
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES"),
                        arg0: 1), type: 1));
            Thread.Sleep(millisecondsTimeout: 30 * 1000);
            ServerManager.Instance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_SECONDS"),
                    arg0: 30), type: 0));
            ServerManager.Instance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_SECONDS"),
                    arg0: 30), type: 1));
            Thread.Sleep(millisecondsTimeout: 20 * 1000);
            ServerManager.Instance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_SECONDS"),
                    arg0: 10), type: 0));
            ServerManager.Instance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_SECONDS"),
                    arg0: 10), type: 1));
            Thread.Sleep(millisecondsTimeout: 10 * 1000);
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_STARTED"), type: 1));
            ServerManager.Instance.Sessions
                .Where(predicate: s => s.Character?.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                .ToList()
                .ForEach(action: s =>
                    s.SendPacket(
                        packet:
                        $"qnaml 1 #guri^506 {Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_QUESTION")}"));
            ServerManager.Instance.EventInWaiting = true;
            Thread.Sleep(millisecondsTimeout: 30 * 1000);
            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_STARTED"), type: 1));
            ServerManager.Instance.Sessions.Where(predicate: s => s.Character?.IsWaitingForEvent == false).ToList()
                .ForEach(action: s => s.SendPacket(packet: "esf"));
            ServerManager.Instance.EventInWaiting = false;
            var sessions = ServerManager.Instance.Sessions.Where(predicate: s =>
                s.Character?.IsWaitingForEvent == true &&
                s.Character.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance);
            var maps = new List<Tuple<MapInstance, byte>>();
            MapInstance map = null;
            var i = -1;
            var level = 0;
            byte instancelevel = 1;
            foreach (var s in sessions.OrderBy(keySelector: s => s.Character?.Level))
            {
                i++;
                if (s.Character.Level > 79 && level <= 79)
                {
                    i = 0;
                    instancelevel = 80;
                }
                else if (s.Character.Level > 69 && level <= 69)
                {
                    i = 0;
                    instancelevel = 70;
                }
                else if (s.Character.Level > 59 && level <= 59)
                {
                    i = 0;
                    instancelevel = 60;
                }
                else if (s.Character.Level > 49 && level <= 49)
                {
                    i = 0;
                    instancelevel = 50;
                }
                else if (s.Character.Level > 39 && level <= 39)
                {
                    i = 0;
                    instancelevel = 40;
                }

                if (i % 50 == 0)
                {
                    map = ServerManager.GenerateMapInstance(mapId: 2004, type: MapInstanceType.NormalInstance,
                        mapclock: new InstanceBag());
                    maps.Add(item: new Tuple<MapInstance, byte>(item1: map, item2: instancelevel));
                }

                if (map != null) ServerManager.Instance.TeleportOnRandomPlaceInMap(session: s, guid: map.MapInstanceId);

                level = s.Character.Level;
            }

            ServerManager.Instance.Sessions.Where(predicate: s => s.Character != null).ToList()
                .ForEach(action: s => s.Character.IsWaitingForEvent = false);
            ServerManager.Instance.StartedEvents.Remove(item: EventType.Instantbattle);
            foreach (var mapinstance in maps)
                Observable.Timer(dueTime: TimeSpan.FromMinutes(value: 0))
                    .Subscribe(onNext: x => InstantBattleTask.Run(mapinstance: mapinstance));
        }

        #endregion

        #region Classes

        public class InstantBattleTask
        {
            #region Methods

            public static void Run(Tuple<MapInstance, byte> mapinstance)
            {
                var maxGold = ServerManager.Instance.Configuration.MaxGold;
                Thread.Sleep(millisecondsTimeout: 10 * 1000);
                if (!mapinstance.Item1.Sessions.Skip(count: 1 - 1).Any())
                    mapinstance.Item1.Sessions.Where(predicate: s => s.Character != null).ToList().ForEach(action: s =>
                    {
                        s.Character.RemoveBuffByBCardTypeSubType(bcardTypes: new List<KeyValuePair<byte, byte>>
                        {
                            new KeyValuePair<byte, byte>(key: (byte) BCardType.CardType.SpecialActions,
                                value: (byte) AdditionalTypes.SpecialActions.Hide),
                            new KeyValuePair<byte, byte>(key: (byte) BCardType.CardType.FalconSkill,
                                value: (byte) AdditionalTypes.FalconSkill.Hide),
                            new KeyValuePair<byte, byte>(key: (byte) BCardType.CardType.FalconSkill,
                                value: (byte) AdditionalTypes.FalconSkill.Ambush)
                        });
                        ServerManager.Instance.ChangeMap(id: s.Character.CharacterId, mapId: s.Character.MapId,
                            mapX: s.Character.MapX,
                            mapY: s.Character.MapY);
                    });
                Observable.Timer(dueTime: TimeSpan.FromMinutes(value: 12)).Subscribe(onNext: x =>
                {
                    for (var d = 0; d < 180; d++)
                    {
                        if (!mapinstance.Item1.Monsters.Any(predicate: s => s.CurrentHp > 0))
                        {
                            EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 0),
                                evt: new EventContainer(mapInstance: mapinstance.Item1,
                                    eventActionType: EventActionType.Spawnportal,
                                    param: new Portal { SourceX = 47, SourceY = 33, DestinationMapId = 1 }));
                            mapinstance.Item1.Broadcast(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_SUCCEEDED"),
                                    type: 0));
                            foreach (var cli in mapinstance.Item1.Sessions.Where(predicate: s => s.Character != null)
                                .ToList())
                            {
                                cli.Character.GenerateFamilyXp(FXP: cli.Character.Level * 4);
                                cli.Character.GetReputation(amount: cli.Character.Level * 50);
                                cli.Character.Gold += cli.Character.Level * 1000;
                                cli.Character.Gold = cli.Character.Gold > maxGold ? maxGold : cli.Character.Gold;
                                cli.Character.SpAdditionPoint += cli.Character.Level * 100;

                                if (cli.Character.SpAdditionPoint > 1000000) cli.Character.SpAdditionPoint = 1000000;

                                cli.SendPacket(packet: cli.Character.GenerateSpPoint());
                                cli.SendPacket(packet: cli.Character.GenerateGold());
                                cli.SendPacket(packet: cli.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "WIN_MONEY"),
                                        arg0: cli.Character.Level * 1000), type: 10));
                                cli.SendPacket(packet: cli.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "WIN_REPUT"),
                                        arg0: cli.Character.Level * 50), type: 10));
                                if (cli.Character.Family != null)
                                    cli.SendPacket(packet: cli.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "WIN_FXP"),
                                            arg0: cli.Character.Level * 4), type: 10));
                                cli.SendPacket(packet: cli.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "WIN_SP_POINT"),
                                        arg0: cli.Character.Level * 100), type: 10));
                            }

                            break;
                        }

                        Thread.Sleep(millisecondsTimeout: 1000);
                    }
                });

                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 15),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Disposemap,
                        param: null));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 3),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES_REMAINING"),
                                arg0: 12),
                            type: 0)));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 5),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES_REMAINING"),
                                arg0: 10),
                            type: 0)));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 10),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES_REMAINING"),
                                arg0: 5),
                            type: 0)));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 11),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES_REMAINING"),
                                arg0: 4),
                            type: 0)));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 12),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES_REMAINING"),
                                arg0: 3),
                            type: 0)));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 13),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES_REMAINING"),
                                arg0: 2),
                            type: 0)));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 14),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MINUTES_REMAINING"),
                                arg0: 1),
                            type: 0)));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 14.5),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_SECONDS_REMAINING"),
                                arg0: 30),
                            type: 0)));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 14.5),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_SECONDS_REMAINING"),
                                arg0: 30),
                            type: 0)));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromMinutes(value: 0),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MONSTERS_INCOMING"),
                            type: 0)));
                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromSeconds(value: 10),
                    evt: new EventContainer(mapInstance: mapinstance.Item1, eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MONSTERS_HERE"),
                            type: 0)));

                for (var wave = 0; wave < 4; wave++)
                {
                    EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromSeconds(value: 130 + wave * 160),
                        evt: new EventContainer(mapInstance: mapinstance.Item1,
                            eventActionType: EventActionType.Sendpacket,
                            param: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MONSTERS_WAVE"),
                                type: 0)));
                    EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromSeconds(value: 160 + wave * 160),
                        evt: new EventContainer(mapInstance: mapinstance.Item1,
                            eventActionType: EventActionType.Sendpacket,
                            param: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MONSTERS_INCOMING"),
                                type: 0)));
                    EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromSeconds(value: 170 + wave * 160),
                        evt: new EventContainer(mapInstance: mapinstance.Item1,
                            eventActionType: EventActionType.Sendpacket,
                            param: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "INSTANTBATTLE_MONSTERS_HERE"),
                                type: 0)));
                    EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromSeconds(value: 10 + wave * 160),
                        evt: new EventContainer(mapInstance: mapinstance.Item1,
                            eventActionType: EventActionType.Spawnmonsters,
                            param: GetInstantBattleMonster(map: mapinstance.Item1.Map,
                                instantbattletype: mapinstance.Item2, wave: wave)));
                    EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromSeconds(value: 140 + wave * 160),
                        evt: new EventContainer(mapInstance: mapinstance.Item1,
                            eventActionType: EventActionType.Dropitems,
                            param: GetInstantBattleDrop(map: mapinstance.Item1.Map,
                                instantbattletype: mapinstance.Item2, wave: wave)));
                }

                EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromSeconds(value: 650),
                    evt: new EventContainer(mapInstance: mapinstance.Item1,
                        eventActionType: EventActionType.Spawnmonsters,
                        param: GetInstantBattleMonster(map: mapinstance.Item1.Map, instantbattletype: mapinstance.Item2,
                            wave: 4)));
            }

            static IEnumerable<Tuple<short, int, short, short>> GenerateDrop(Map map, short vnum,
                int amountofdrop, int amount)
            {
                var dropParameters = new List<Tuple<short, int, short, short>>();
                for (var i = 0; i < amountofdrop; i++)
                {
                    var cell = map.GetRandomPosition();
                    dropParameters.Add(item: new Tuple<short, int, short, short>(item1: vnum, item2: amount,
                        item3: cell.X, item4: cell.Y));
                }

                return dropParameters;
            }

            static List<Tuple<short, int, short, short>> GetInstantBattleDrop(Map map, short instantbattletype,
                int wave)
            {
                var dropParameters = new List<Tuple<short, int, short, short>>();
                switch (instantbattletype)
                {
                    case 1:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 500));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 2027, amountofdrop: 8,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 2018, amountofdrop: 5,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 180, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 1:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 1000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1002, amountofdrop: 8,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1005, amountofdrop: 16,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 181, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 2:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 1500));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1002, amountofdrop: 10,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1005, amountofdrop: 10,
                                    amount: 5));
                                break;

                            case 3:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 2000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1003, amountofdrop: 10,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1006, amountofdrop: 10,
                                    amount: 5));
                                break;
                        }

                        break;

                    case 40:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 1500));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1008, amountofdrop: 5,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 180, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 1:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 2000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1008, amountofdrop: 8,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 181, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 2:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 2500));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1009, amountofdrop: 10,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1246, amountofdrop: 5,
                                    amount: 1));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1247, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 3:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 3000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1009, amountofdrop: 10,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1248, amountofdrop: 5,
                                    amount: 1));
                                break;
                        }

                        break;

                    case 50:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 1500));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1008, amountofdrop: 5,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 180, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 1:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 2000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1008, amountofdrop: 8,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 181, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 2:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 2500));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1009, amountofdrop: 10,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1246, amountofdrop: 5,
                                    amount: 1));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1247, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 3:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 3000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1009, amountofdrop: 10,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1248, amountofdrop: 5,
                                    amount: 1));
                                break;
                        }

                        break;

                    case 60:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 3000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1010, amountofdrop: 8,
                                    amount: 4));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1246, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 1:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 4000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1010, amountofdrop: 10,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1247, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 2:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 5000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1010, amountofdrop: 10,
                                    amount: 13));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1246, amountofdrop: 8,
                                    amount: 1));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1247, amountofdrop: 8,
                                    amount: 1));
                                break;

                            case 3:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 7000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1011, amountofdrop: 13,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1029, amountofdrop: 5,
                                    amount: 1));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1248, amountofdrop: 13,
                                    amount: 1));
                                break;
                        }

                        break;

                    case 70:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 3000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1010, amountofdrop: 8,
                                    amount: 3));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1246, amountofdrop: 5,
                                    amount: 1));
                                break;

                            case 1:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 4000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1010, amountofdrop: 15,
                                    amount: 4));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1247, amountofdrop: 10,
                                    amount: 1));
                                break;

                            case 2:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 5000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1010, amountofdrop: 13,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1246, amountofdrop: 13,
                                    amount: 1));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1247, amountofdrop: 13,
                                    amount: 1));
                                break;

                            case 3:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 7000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1011, amountofdrop: 13,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1248, amountofdrop: 13,
                                    amount: 1));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1029, amountofdrop: 5,
                                    amount: 1));
                                break;
                        }

                        break;

                    case 80:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 10000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1011, amountofdrop: 15,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1246, amountofdrop: 15,
                                    amount: 1));
                                break;

                            case 1:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 12000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1011, amountofdrop: 15,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1247, amountofdrop: 15,
                                    amount: 1));
                                break;

                            case 2:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 15,
                                    amount: 15000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1011, amountofdrop: 20,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1246, amountofdrop: 15,
                                    amount: 1));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1247, amountofdrop: 15,
                                    amount: 1));
                                break;

                            case 3:
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1046, amountofdrop: 30,
                                    amount: 20000));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1011, amountofdrop: 30,
                                    amount: 5));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 1030, amountofdrop: 30,
                                    amount: 1));
                                dropParameters.AddRange(collection: GenerateDrop(map: map, vnum: 2282, amountofdrop: 12,
                                    amount: 3));
                                break;
                        }

                        break;
                }

                return dropParameters;
            }

            static List<MonsterToSummon> GetInstantBattleMonster(Map map, short instantbattletype, int wave)
            {
                var summonParameters = new List<MonsterToSummon>();

                switch (instantbattletype)
                {
                    case 1:
                        switch (wave)
                        {
                            case 0:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1, amount: 16,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 58, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 105, amount: 16,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 107, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 108, amount: 8, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 111, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 136, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 1:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 194, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 114, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 99, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 39, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 2, amount: 16,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 2:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 140, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 100, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 81, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 12, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 4, amount: 16,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 3:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 115, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 112, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 110, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 14, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 5, amount: 16,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 4:
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 979, amount: 1, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 167, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 137, amount: 10,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 22, amount: 15,
                                    move: false,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 17, amount: 8,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 16, amount: 16, move: true,
                                        deathEvents: new List<EventContainer>()));
                                break;
                        }

                        break;

                    case 40:
                        switch (wave)
                        {
                            case 0:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 120, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 151, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 149, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 139, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 73, amount: 16, move: true,
                                        deathEvents: new List<EventContainer>()));
                                break;

                            case 1:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 152, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 147, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 104, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 62, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 8, amount: 16,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 2:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 153, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 132, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 86, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 76, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 68, amount: 16, move: true,
                                        deathEvents: new List<EventContainer>()));
                                break;

                            case 3:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 134, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 91, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 133, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 70, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 89, amount: 16, move: true,
                                        deathEvents: new List<EventContainer>()));
                                break;

                            case 4:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 154, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 200, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 77, amount: 8,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 217, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 724, amount: 1, move: true,
                                        deathEvents: new List<EventContainer>()));
                                break;
                        }

                        break;

                    case 50:
                        switch (wave)
                        {
                            case 0:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 134, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 91, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 89, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 77, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 71, amount: 16, move: true,
                                        deathEvents: new List<EventContainer>()));
                                break;

                            case 1:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 217, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 200, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 154, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 92, amount: 15, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 79, amount: 16, move: true,
                                        deathEvents: new List<EventContainer>()));
                                break;

                            case 2:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 235, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 226, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 214, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 204, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 201, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 3:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 249, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 236, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 227, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 218, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 202, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 4:
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 583, amount: 1, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 400, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 255, amount: 8, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 253, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 251, amount: 10,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 205, amount: 14,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;
                        }

                        break;

                    case 60:
                        switch (wave)
                        {
                            case 0:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 242, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 234, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 215, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 207, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 202, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 1:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 402, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 253, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 237, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 216, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 205, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 2:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 402, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 243, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 228, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 255, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 205, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 3:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 268, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 255, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 254, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 174, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 172, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 4:
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 725, amount: 1, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 407, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 272, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 261, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 256, amount: 12,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 275, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;
                        }

                        break;

                    case 70:
                        switch (wave)
                        {
                            case 0:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 402, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 253, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 237, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 216, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 205, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 1:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 402, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 243, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 228, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 225, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 205, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 2:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 255, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 254, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 251, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 174, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 172, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 3:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 407, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 272, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 261, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 257, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 256, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 4:
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 748, amount: 1, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 444, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 439, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 275, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 274, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 273, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 163, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;
                        }

                        break;

                    case 80:
                        switch (wave)
                        {
                            case 0:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1007, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1003, amount: 15,
                                    move: false,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1002, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1001, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1000, amount: 16,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 1:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1199, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1198, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1197, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1196, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1123, amount: 16,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 2:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1305, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1304, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1303, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1302, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1194, amount: 16,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 3:
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1902, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1901, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1900, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1045, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1043, amount: 15,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1042, amount: 16,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;

                            case 4:
                                summonParameters.AddRange(
                                    collection: map.GenerateMonsters(vnum: 637, amount: 1, move: true,
                                        deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1903, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1053, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1051, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1049, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1048, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                summonParameters.AddRange(collection: map.GenerateMonsters(vnum: 1047, amount: 13,
                                    move: true,
                                    deathEvents: new List<EventContainer>()));
                                break;
                        }

                        break;
                }

                return summonParameters;
            }

            #endregion
        }

        #endregion
    }
}