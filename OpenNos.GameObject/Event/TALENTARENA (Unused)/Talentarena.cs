﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.Core.Threading;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event
{
    public static class TalentArena
    {
        public static bool IsRunning { get; set; }

        public static ThreadSafeSortedList<long, ClientSession> RegisteredParticipants { get; set; }

        public static ThreadSafeSortedList<long, Group> RegisteredGroups { get; set; }

        public static ThreadSafeSortedList<long, List<Group>> PlayingGroups { get; set; }

        #region Methods

        public static void Run()
        {
            RegisteredParticipants = new ThreadSafeSortedList<long, ClientSession>();
            RegisteredGroups = new ThreadSafeSortedList<long, Group>();
            PlayingGroups = new ThreadSafeSortedList<long, List<Group>>();

            ServerManager.Shout(message: Language.Instance.GetMessageFromKey(key: "TALENTARENA_OPEN"),
                noAdminTag: true);

            var groupingThread = new GroupingThread();
            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 0))
                .Subscribe(onNext: observer => groupingThread.Run());

            var matchmakingThread = new MatchmakingThread();
            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 3))
                .Subscribe(onNext: observer => matchmakingThread.Run());

            IsRunning = true;

            Observable.Timer(dueTime: TimeSpan.FromMinutes(value: 30)).Subscribe(onNext: observer =>
            {
                groupingThread.RequestStop();
                matchmakingThread.RequestStop();
                RegisteredParticipants.ClearAll();
                RegisteredGroups.ClearAll();
                IsRunning = false;
                ServerManager.Instance.StartedEvents.Remove(item: EventType.Talentarena);
            });
        }

        class GroupingThread
        {
            bool _shouldStop;

            public void Run()
            {
                byte[] levelCaps = { 40, 50, 60, 70, 80, 85, 90, 95, 100, 120, 150, 180, 255 };
                while (!_shouldStop)
                {
                    var groups = from sess in RegisteredParticipants.GetAllItems()
                                 group sess by Array.Find(array: levelCaps, match: s => s > sess.Character.Level)
                        into grouping
                                 select grouping;
                    foreach (var group in groups)
                        foreach (var grp in group.ToList().Split(nSize: 3).Where(predicate: s => s.Count == 3))
                        {
                            var g = new Group
                            {
                                GroupType = GroupType.TalentArena,
                                TalentArenaBattle = new TalentArenaBattle
                                {
                                    GroupLevel = group.Key
                                }
                            };

                            foreach (var sess in grp)
                            {
                                RegisteredParticipants.Remove(value: sess);
                                g.JoinGroup(session: sess);
                                sess.SendPacket(
                                    packet: UserInterfaceHelper.GenerateBsInfo(mode: 1, title: 3, time: -1, text: 6));
                                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: observer =>
                                    sess?.SendPacket(
                                        packet: UserInterfaceHelper.GenerateBsInfo(mode: 1, title: 3, time: 300, text: 1)));
                            }

                            RegisteredGroups[key: g.GroupId] = g;
                        }

                    Thread.Sleep(millisecondsTimeout: 5000);
                }
            }

            public void RequestStop()
            {
                _shouldStop = true;
            }
        }

        class MatchmakingThread
        {
            bool _shouldStop;

            public void Run()
            {
                while (!_shouldStop)
                {
                    var groups = from grp in RegisteredGroups.GetAllItems()
                                 where grp.TalentArenaBattle != null
                                 group grp by grp.TalentArenaBattle.GroupLevel
                        into grouping
                                 select grouping;

                    foreach (var group in groups)
                    {
                        Group prevGroup = null;

                        foreach (var g in group)
                            if (prevGroup == null)
                            {
                                prevGroup = g;
                            }
                            else
                            {
                                RegisteredGroups.Remove(value: g);
                                RegisteredGroups.Remove(value: prevGroup);

                                var mapInstance = ServerManager.GenerateMapInstance(mapId: 2015,
                                    type: MapInstanceType.NormalInstance, mapclock: new InstanceBag());
                                mapInstance.IsPvp = true;

                                g.TalentArenaBattle.MapInstance = mapInstance;
                                prevGroup.TalentArenaBattle.MapInstance = mapInstance;

                                g.TalentArenaBattle.Side = 0;
                                prevGroup.TalentArenaBattle.Side = 1;

                                g.TalentArenaBattle.Calls = 5;
                                prevGroup.TalentArenaBattle.Calls = 5;

                                var gs = g.Sessions.GetAllItems().Concat(second: prevGroup.Sessions.GetAllItems());
                                foreach (var sess in gs)
                                    sess.SendPacket(
                                        packet: UserInterfaceHelper.GenerateBsInfo(mode: 1, title: 3, time: -1,
                                            text: 2));
                                Thread.Sleep(millisecondsTimeout: 1000);
                                foreach (var sess in gs)
                                {
                                    sess.SendPacket(
                                        packet: UserInterfaceHelper.GenerateBsInfo(mode: 2, title: 3, time: 0,
                                            text: 0));
                                    sess.SendPacket(packet: UserInterfaceHelper.GenerateTeamArenaClose());
                                }

                                Thread.Sleep(millisecondsTimeout: 5000);
                                foreach (var sess in gs)
                                {
                                    sess.SendPacket(packet: UserInterfaceHelper.GenerateTeamArenaMenu(mode: 0,
                                        zenasScore: 0, ereniaScore: 0, time: 0, arenaType: 0));
                                    short x = 125;
                                    if (sess.Character.Group.TalentArenaBattle.Side == 0) x = 15;
                                    ServerManager.Instance.ChangeMapInstance(characterId: sess.Character.CharacterId,
                                        mapInstanceId: mapInstance.MapInstanceId, mapX: x, mapY: 39);
                                    sess.SendPacketAfter(
                                        packet: UserInterfaceHelper.GenerateTeamArenaMenu(mode: 3, zenasScore: 0,
                                            ereniaScore: 0, time: 60, arenaType: 0),
                                        milliseconds: 5000);
                                }

#pragma warning disable CS1030 // #warning: "TODO: Other Setup stuff"
#warning TODO: Other Setup stuff

                                PlayingGroups[key: g.GroupId] = new List<Group> { g, prevGroup };
#pragma warning restore CS1030 // #warning: "TODO: Other Setup stuff"

                                var battleThread = new BattleThread();
                                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 0)).Subscribe(onNext: observer =>
                                    battleThread.Run(groups: PlayingGroups[key: g.GroupId]));

                                prevGroup = null;
                            }
                    }

                    Thread.Sleep(millisecondsTimeout: 5000);
                }
            }

            public void RequestStop()
            {
                _shouldStop = true;
            }
        }

        class BattleThread
        {
            List<ClientSession> Characters { get; set; }

            public void Run(List<Group> groups)
            {
                Characters = groups[index: 0].Sessions.GetAllItems()
                    .Concat(second: groups[index: 1].Sessions.GetAllItems()).ToList();
            }
        }

        #endregion
    }
}