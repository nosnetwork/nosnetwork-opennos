﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using OpenNos.Domain;

namespace OpenNos.GameObject.Event
{
    public class EventSchedule : IConfigurationSectionHandler
    {
        #region Methods

        public object Create(object parent, object configContext, XmlNode section)
        {
            var list = new List<Schedule>();
            foreach (XmlNode aSchedule in section.ChildNodes) list.Add(item: GetSchedule(str: aSchedule));
            return list;
        }

        static Schedule GetSchedule(XmlNode str)
        {
            if (str.Attributes != null)
            {
                var lvlBracket = 0;
                if (str.Attributes[name: "lvlbracket"] != null)
                    lvlBracket = int.Parse(s: str.Attributes[name: "lvlbracket"].Value);
                var dayOfWeek = "";
                if (str.Attributes[name: "dayOfWeek"] != null) dayOfWeek = str.Attributes[name: "dayOfWeek"].Value;
                return new Schedule
                {
                    Event = (EventType)Enum.Parse(enumType: typeof(EventType),
                        value: str.Attributes[name: "event"].Value),
                    Time = TimeSpan.Parse(s: str.Attributes[name: "time"].Value),
                    LvlBracket = lvlBracket,
                    DayOfWeek = dayOfWeek
                };
            }

            return null;
        }

        #endregion
    }
}