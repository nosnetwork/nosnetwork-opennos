﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event.LOD
{
    public static class Lod
    {
        #region Methods

        public static void GenerateLod()
        {
            const int lodTime = 90;
            const int hornTime = 60;
            const int hornStayTime = 1;
            const int hornRespawnTime = 4;

            EventHelper.Instance.RunEvent(evt: new EventContainer(
                mapInstance: ServerManager.GetMapInstance(id: ServerManager.GetBaseMapInstanceIdByMapId(mapId: 98)),
                eventActionType: EventActionType.Npcseffectchangestate, param: true));
            var lodThread = new LodThread();
            Observable.Timer(dueTime: TimeSpan.FromMinutes(value: 0)).Subscribe(onNext: s =>
                lodThread.Run(lodTime: lodTime * 60, hornTime: (hornTime + 1) * 60,
                    hornRespawn: (hornRespawnTime + hornStayTime) * 60, hornStay: hornStayTime * 60));
        }

        #endregion
    }

    public class LodThread
    {
        #region Members

        public bool IsOpen { get; set; } = true;

        #endregion

        #region Methods

        public void Run(int lodTime, int hornTime, int hornRespawn, int hornStay)
        {
            ChangePortalEffect(effectId: 855);

            const int interval = 30;
            var dhspawns = 0;

            while (lodTime > 0)
            {
                refreshLOD(remaining: lodTime);

                if (lodTime == hornTime || lodTime == hornTime - hornRespawn * dhspawns)
                {
                    foreach (var fam in ServerManager.Instance.FamilyList.GetAllItems())
                        if (fam.LandOfDeath != null)
                        {
                            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.LandOfDeath,
                                eventActionType: EventActionType.Changexprate, param: 3));
                            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.LandOfDeath,
                                eventActionType: EventActionType.Changedroprate, param: 3));
                            spawnDH(landOfDeath: fam.LandOfDeath);
                        }
                }
                else if (lodTime == hornTime - hornRespawn * dhspawns - hornStay)
                {
                    foreach (var fam in ServerManager.Instance.FamilyList.GetAllItems())
                        if (fam.LandOfDeath != null)
                            DespawnDh(landOfDeath: fam.LandOfDeath);

                    dhspawns++;
                }

                lodTime -= interval;
                Thread.Sleep(millisecondsTimeout: interval * 1000);
            }

            endLOD();
        }

        void ChangePortalEffect(short effectId)
        {
            ServerManager.Instance.GetMapNpcsByVNum(npcVNum: 453).ForEach(action: mapNpc => mapNpc.Effect = effectId);
        }

        void DespawnDh(MapInstance landOfDeath)
        {
            EventHelper.Instance.RunEvent(evt: new EventContainer(
                mapInstance: ServerManager.GetMapInstance(id: ServerManager.GetBaseMapInstanceIdByMapId(mapId: 98)),
                eventActionType: EventActionType.Npcseffectchangestate, param: false));
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: landOfDeath,
                eventActionType: EventActionType.Sendpacket,
                param: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "HORN_DISAPEAR"), type: 0)));
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: landOfDeath,
                eventActionType: EventActionType.Unspawnmonsters, param: 443));

            if (IsOpen)
            {
                IsOpen = false;

                ChangePortalEffect(effectId: 0);
            }
        }

        void endLOD()
        {
            foreach (var fam in ServerManager.Instance.FamilyList.GetAllItems())
                if (fam.LandOfDeath != null)
                {
                    EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.LandOfDeath,
                        eventActionType: EventActionType.Disposemap,
                        param: null));
                    fam.LandOfDeath = null;
                }

            ServerManager.Instance.StartedEvents.Remove(item: EventType.Lod);
        }

        void refreshLOD(int remaining)
        {
            foreach (var fam in ServerManager.Instance.FamilyList.GetAllItems())
            {
                if (fam.LandOfDeath == null)
                    fam.LandOfDeath =
                        ServerManager.GenerateMapInstance(mapId: 150, type: MapInstanceType.LodInstance,
                            mapclock: new InstanceBag());

                EventHelper.Instance.RunEvent(
                    evt: new EventContainer(mapInstance: fam.LandOfDeath, eventActionType: EventActionType.Clock,
                        param: remaining * 10));
                EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.LandOfDeath,
                    eventActionType: EventActionType.Startclock,
                    param: new Tuple<List<EventContainer>, List<EventContainer>>(item1: new List<EventContainer>(),
                        item2: new List<EventContainer>())));
            }
        }

        void spawnDH(MapInstance landOfDeath)
        {
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: landOfDeath,
                eventActionType: EventActionType.Spawnonlastentry, param: 443));
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: landOfDeath,
                eventActionType: EventActionType.Sendpacket, param: "df 2"));
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: landOfDeath,
                eventActionType: EventActionType.Sendpacket,
                param: UserInterfaceHelper.GenerateMsg(message: Language.Instance.GetMessageFromKey(key: "HORN_APPEAR"),
                    type: 0)));
        }

        #endregion
    }
}