﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event
{
    public class LODThread
    {
        #region Members

        public bool IsOpen { get; set; } = true;

        #endregion

        #region Methods

        public void Run(int lodTime, int hornTime, int hornRespawn, int hornStay)
        {
            ChangePortalEffect(effectId: 855);

            const int interval = 30;
            var dhspawns = 0;

            while (lodTime > 0)
            {
                refreshLOD(remaining: lodTime);

                if (lodTime == hornTime || lodTime == hornTime - hornRespawn * dhspawns)
                {
                    foreach (var fam in ServerManager.Instance.FamilyList.GetAllItems())
                        if (fam.LandOfDeath != null)
                        {
                            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.LandOfDeath,
                                eventActionType: EventActionType.CHANGEXPRATE, param: 3));
                            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.LandOfDeath,
                                eventActionType: EventActionType.CHANGEDROPRATE, param: 3));
                            spawnDH(LandOfDeath: fam.LandOfDeath);
                        }
                }
                else if (lodTime == hornTime - hornRespawn * dhspawns - hornStay)
                {
                    foreach (var fam in ServerManager.Instance.FamilyList.GetAllItems())
                        if (fam.LandOfDeath != null)
                            despawnDH(LandOfDeath: fam.LandOfDeath);

                    dhspawns++;
                }

                lodTime -= interval;
                Thread.Sleep(millisecondsTimeout: interval * 1000);
            }

            endLOD();
        }

        void ChangePortalEffect(short effectId)
        {
            ServerManager.Instance.GetMapNpcsByVNum(npcVNum: 453).ForEach(action: mapNpc => mapNpc.Effect = effectId);
        }

        void despawnDH(MapInstance LandOfDeath)
        {
            EventHelper.Instance.RunEvent(evt: new EventContainer(
                mapInstance: ServerManager.GetMapInstance(id: ServerManager.GetBaseMapInstanceIdByMapId(mapId: 98)),
                eventActionType: EventActionType.NPCSEFFECTCHANGESTATE, param: false));
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: LandOfDeath,
                eventActionType: EventActionType.SENDPACKET,
                param: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "HORN_DISAPEAR"), type: 0)));
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: LandOfDeath,
                eventActionType: EventActionType.UNSPAWNMONSTERS, param: 443));

            if (IsOpen)
            {
                IsOpen = false;

                ChangePortalEffect(effectId: 0);
            }
        }

        void endLOD()
        {
            foreach (var fam in ServerManager.Instance.FamilyList.GetAllItems())
                if (fam.LandOfDeath != null)
                {
                    EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.LandOfDeath,
                        eventActionType: EventActionType.DISPOSEMAP,
                        param: null));
                    fam.LandOfDeath = null;
                }

            ServerManager.Instance.StartedEvents.Remove(item: EventType.LOD);
        }

        void refreshLOD(int remaining)
        {
            foreach (var fam in ServerManager.Instance.FamilyList.GetAllItems())
            {
                if (fam.LandOfDeath == null)
                    fam.LandOfDeath =
                        ServerManager.GenerateMapInstance(mapId: 150, type: MapInstanceType.LodInstance,
                            mapclock: new InstanceBag());

                EventHelper.Instance.RunEvent(
                    evt: new EventContainer(mapInstance: fam.LandOfDeath, eventActionType: EventActionType.CLOCK,
                        param: remaining * 10));
                EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: fam.LandOfDeath,
                    eventActionType: EventActionType.STARTCLOCK,
                    param: new Tuple<List<EventContainer>, List<EventContainer>>(item1: new List<EventContainer>(),
                        item2: new List<EventContainer>())));
            }
        }

        void spawnDH(MapInstance LandOfDeath)
        {
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: LandOfDeath,
                eventActionType: EventActionType.SPAWNONLASTENTRY, param: 443));
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: LandOfDeath,
                eventActionType: EventActionType.SENDPACKET, param: "df 2"));
            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: LandOfDeath,
                eventActionType: EventActionType.SENDPACKET,
                param: UserInterfaceHelper.GenerateMsg(message: Language.Instance.GetMessageFromKey(key: "HORN_APPEAR"),
                    type: 0)));
        }

        #endregion
    }
}