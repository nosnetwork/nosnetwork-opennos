﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event.ICEBREAKER
{
    public class IceBreaker
    {
        #region Methods

        public static void GenerateIceBreaker(int bracket)
        {
            if (bracket < 0) return;
            _currentBracket = bracket;
            AlreadyFrozenPlayers = new List<ClientSession>();
            Map = ServerManager.GenerateMapInstance(mapId: 2005, type: MapInstanceType.IceBreakerInstance,
                mapclock: new InstanceBag());

            ServerManager.Instance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_MINUTES"), arg0: 5,
                    arg1: LevelBrackets[_currentBracket].Item1, arg2: LevelBrackets[_currentBracket].Item2), type: 1));
            Thread.Sleep(millisecondsTimeout: 5 * 60 * 1000);
            ServerManager.Instance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_MINUTES"), arg0: 1,
                    arg1: LevelBrackets[_currentBracket].Item1, arg2: LevelBrackets[_currentBracket].Item2), type: 1));
            Thread.Sleep(millisecondsTimeout: 1 * 60 * 1000);
            ServerManager.Instance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_SECONDS"), arg0: 30,
                    arg1: LevelBrackets[_currentBracket].Item1, arg2: LevelBrackets[_currentBracket].Item2), type: 1));
            Thread.Sleep(millisecondsTimeout: 30 * 1000);
            ServerManager.Instance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_SECONDS"), arg0: 10,
                    arg1: LevelBrackets[_currentBracket].Item1, arg2: LevelBrackets[_currentBracket].Item2), type: 1));
            Thread.Sleep(millisecondsTimeout: 10 * 1000);

            ServerManager.Instance.Broadcast(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_STARTED"), type: 1));
            ServerManager.Instance.IceBreakerInWaiting = true;
            ServerManager.Instance.Sessions
                .Where(predicate: x => x.Character.Level >= LevelBrackets[_currentBracket].Item1 &&
                                       x.Character.Level <= LevelBrackets[_currentBracket].Item2 &&
                                       x.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance).ToList()
                .ForEach(action: x =>
                    x.SendPacket(
                        packet:
                        $"qnaml 2 #guri^501 {string.Format(format: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_ASK"), arg0: 500)}"));
            /*currentBracket++;
            if (currentBracket > 5)
            {
                currentBracket = 0;
            }*/

            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 10)).Subscribe(onNext: c =>
            {
                ServerManager.Instance.StartedEvents.Remove(item: EventType.Icebreaker);
                ServerManager.Instance.IceBreakerInWaiting = false;
                if (Map.Sessions.Count() <= 1)
                {
                    Map.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_WIN"),
                        type: 0));
                    Map.Sessions.ToList().ForEach(action: x =>
                    {
                        x.Character.GetReputation(amount: x.Character.Level * 10);
                        if (x.Character.Dignity < 100) x.Character.Dignity = 100;

                        x.Character.Gold += GoldRewards[_currentBracket];
                        x.Character.Gold = x.Character.Gold > ServerManager.Instance.Configuration.MaxGold
                            ? ServerManager.Instance.Configuration.MaxGold
                            : x.Character.Gold;
                        x.SendPacket(packet: x.Character.GenerateFd());
                        x.CurrentMapInstance?.Broadcast(client: x, content: x.Character.GenerateIn(InEffect: 1),
                            receiver: ReceiverType.AllExceptMe);
                        x.CurrentMapInstance?.Broadcast(client: x, content: x.Character.GenerateGidx(),
                            receiver: ReceiverType.AllExceptMe);
                        x.SendPacket(packet: x.Character.GenerateGold());
                        x.SendPacket(packet: x.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "WIN_MONEY"),
                                arg0: GoldRewards[_currentBracket]), type: 10));
                        x.SendPacket(packet: x.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "WIN_REPUT"),
                                arg0: x.Character.Level * 10),
                            type: 10));
                        x.SendPacket(packet: x.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "DIGNITY_RESTORED"),
                                arg0: 100), type: 10));
                    });
                    Thread.Sleep(millisecondsTimeout: 5000);
                    IceBreakerTeams.Clear();
                    AlreadyFrozenPlayers.Clear();
                    FrozenPlayers.Clear();
                    EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromSeconds(value: 10),
                        evt: new EventContainer(mapInstance: Map, eventActionType: EventActionType.Disposemap,
                            param: null));
                }
                else
                {
                    Map.Broadcast(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_FIGHT_WARN"),
                            type: 0));
                    Thread.Sleep(millisecondsTimeout: 6000);
                    Map.Broadcast(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_FIGHT_WARN"),
                            type: 0));
                    Thread.Sleep(millisecondsTimeout: 7000);
                    Map.Broadcast(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_FIGHT_WARN"),
                            type: 0));
                    Thread.Sleep(millisecondsTimeout: 1000);
                    Map.Broadcast(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_FIGHT_START"),
                            type: 0));
                    Map.IsPvp = true;

                    ConcurrentBag<ClientSession> winnerTeam = null;
                    for (var d = 0; d < 1800; d++)
                    {
                        foreach (var cli in Map.Sessions.Where(predicate: s => !FrozenPlayers.Contains(item: s))
                            .ToList())
                            if (winnerTeam == null)
                            {
                                winnerTeam = IceBreakerTeams.FirstOrDefault(predicate: t => t.Contains(value: cli));
                            }
                            else if (!winnerTeam.Contains(value: cli))
                            {
                                winnerTeam = null;
                                break;
                            }

                        if (Map.Sessions.Count(predicate: s => !FrozenPlayers.Contains(item: s)) == 1)
                        {
                            winnerTeam = new ConcurrentBag<ClientSession>();
                            winnerTeam.Add(
                                item: Map.Sessions.FirstOrDefault(predicate: s => !FrozenPlayers.Contains(item: s)));
                        }

                        if (winnerTeam != null)
                        {
                            winnerTeam.ToList().ForEach(action: x =>
                            {
                                x.Character.GetReputation(amount: x.Character.Level * 10);
                                if (x.Character.Dignity < 100) x.Character.Dignity = 100;

                                x.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "ICEBREAKER_WIN"), type: 0));
                                x.Character.Gold += GoldRewards[_currentBracket];
                                x.Character.Gold = x.Character.Gold > ServerManager.Instance.Configuration.MaxGold
                                    ? ServerManager.Instance.Configuration.MaxGold
                                    : x.Character.Gold;
                                x.SendPacket(packet: x.Character.GenerateFd());
                                x.CurrentMapInstance?.Broadcast(client: x, content: x.Character.GenerateIn(InEffect: 1),
                                    receiver: ReceiverType.AllExceptMe);
                                x.CurrentMapInstance?.Broadcast(client: x, content: x.Character.GenerateGidx(),
                                    receiver: ReceiverType.AllExceptMe);
                                x.SendPacket(packet: x.Character.GenerateGold());
                                x.SendPacket(packet: x.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "WIN_MONEY"),
                                        arg0: GoldRewards[_currentBracket]), type: 10));
                                x.SendPacket(packet: x.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "WIN_REPUT"),
                                        arg0: x.Character.Level * 10), type: 10));
                                x.SendPacket(packet: x.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "DIGNITY_RESTORED"),
                                        arg0: x.Character.Level * 10), type: 10));
                            });
                            break;
                        }

                        Thread.Sleep(millisecondsTimeout: 1000);
                    }

                    EventHelper.Instance.ScheduleEvent(timeSpan: TimeSpan.FromSeconds(value: 10),
                        evt: new EventContainer(mapInstance: Map, eventActionType: EventActionType.Disposemap,
                            param: null));
                    Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 20)).Subscribe(onNext: x =>
                    {
                        IceBreakerTeams.Clear();
                        AlreadyFrozenPlayers.Clear();
                        FrozenPlayers.Clear();
                    });
                }
            });
        }

        #endregion

        #region Members

        public const int MaxAllowedPlayers = 50;

        static readonly int[] GoldRewards =
        {
            100,
            1000,
            3000,
            5000,
            10000,
            20000
        };

        static readonly Tuple<int, int>[] LevelBrackets =
        {
            new Tuple<int, int>(item1: 1, item2: 25),
            new Tuple<int, int>(item1: 20, item2: 40),
            new Tuple<int, int>(item1: 35, item2: 55),
            new Tuple<int, int>(item1: 50, item2: 70),
            new Tuple<int, int>(item1: 65, item2: 85),
            new Tuple<int, int>(item1: 80, item2: 99)
        };

        static int _currentBracket;

        #endregion

        #region Properties

        public static List<ClientSession> AlreadyFrozenPlayers { get; set; }

        public static List<ClientSession> FrozenPlayers = new List<ClientSession>();

        public static List<ConcurrentBag<ClientSession>> IceBreakerTeams = new List<ConcurrentBag<ClientSession>>();

        public static MapInstance Map { get; private set; }

        #endregion
    }
}