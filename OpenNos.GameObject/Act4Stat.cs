﻿using System;

namespace OpenNos.GameObject
{
    public class Act4Stat
    {
        #region Instantiation

        public Act4Stat()
        {
            var olddate = DateTime.Now.AddMonths(months: 1);
            _nextMonth = new DateTime(year: olddate.Year, month: olddate.Month, day: 1, hour: 0, minute: 0, second: 0,
                kind: olddate.Kind);
            _latestUpdate = DateTime.Now;
        }

        #endregion

        #region Members

        DateTime _latestUpdate;
        readonly DateTime _nextMonth;

        int _percentage;

        short _totalTime;

        #endregion

        #region Properties

        public short CurrentTime
        {
            get =>
                Mode == 0
                    ? (short)0
                    : (short)(_latestUpdate.AddSeconds(value: _totalTime) - DateTime.Now).TotalSeconds;
        }

        public bool IsBerios { get; set; }

        public bool IsCalvina { get; set; }

        public bool IsHatus { get; set; }

        public bool IsMorcos { get; set; }

        public int MinutesUntilReset
        {
            get => (int)(_nextMonth - DateTime.Now).TotalMinutes;
        }

        public byte Mode { get; set; }

        public int Percentage
        {
            get => Mode == 0 ? _percentage : 0;
            set => _percentage = value;
        }

        public short TotalTime
        {
            get => Mode == 0 ? (short)0 : _totalTime;
            set
            {
                _latestUpdate = DateTime.Now;
                _totalTime = value;
            }
        }

        #endregion
    }
}