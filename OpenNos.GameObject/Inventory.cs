﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.Core.Threading;
using OpenNos.DAL;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Log.Networking;
using OpenNos.Log.Shared;

namespace OpenNos.GameObject
{
    public class Inventory : ThreadSafeSortedList<Guid, ItemInstance>
    {
        #region Instantiation

        public Inventory(Character Character)
        {
            Owner = Character;
        }

        #endregion

        #region Properties

        Character Owner { get; }

        #endregion

        #region Members

        const short DEFAULT_BACKPACK_SIZE = 48;

        const short MAX_ITEM_AMOUNT = 32000;

        readonly object _lockObject = new object();

        #endregion

        #region Methods

        public T LoadBySlotAndType<T>(short slot, InventoryType type) where T : ItemInstance
        {
            T retItem = null;
            try
            {
                lock (_lockObject)
                {
                    retItem = (T)SingleOrDefault(predicate: i =>
                       i?.GetType().Equals(o: typeof(T)) == true && i.Slot == slot && i.Type == type);
                }
            }
            catch (InvalidOperationException ioEx)
            {
                Logger.LogUserEventError(logEvent: nameof(LoadBySlotAndType),
                    caller: Owner?.Session?.GenerateIdentity(),
                    data: "Multiple items in slot, Splitting...", ex: ioEx);
                var isFirstItem = true;
                foreach (var item in Where(predicate: i =>
                    i?.GetType().Equals(o: typeof(T)) == true && i.Slot == slot && i.Type == type))
                {
                    if (isFirstItem)
                    {
                        retItem = (T)item;
                        isFirstItem = false;
                        continue;
                    }

                    var itemInstance = FirstOrDefault(predicate: i =>
                        i?.GetType().Equals(o: typeof(T)) == true && i.Slot == slot && i.Type == type);
                    if (itemInstance != null)
                    {
                        var freeSlot = getFreeSlot(type: type);
                        if (freeSlot.HasValue)
                            itemInstance.Slot = freeSlot.Value;
                        else
                            Remove(key: itemInstance.Id);
                    }
                }
            }

            return retItem;
        }

        public static ItemInstance InstantiateItemInstance(short vnum, long ownerId, short amount = 1)
        {
            var newItem = new ItemInstance { ItemVNum = vnum, Amount = amount, CharacterId = ownerId };
            if (newItem.Item != null)
                switch (newItem.Item.Type)
                {
                    case InventoryType.Miniland:
                        newItem.DurabilityPoint = newItem.Item.MinilandObjectPoint / 2;
                        break;

                    case InventoryType.Equipment:
                        newItem = newItem.Item.ItemType == ItemType.Specialist
                            ? new ItemInstance
                            {
                                ItemVNum = vnum,
                                SpLevel = 1,
                                Amount = 1
                            }
                            : new ItemInstance
                            {
                                ItemVNum = vnum,
                                Amount = 1,
                                DurabilityPoint =
                                    newItem.Item.Effect != 790 &&
                                    (newItem.Item.EffectValue < 863 || newItem.Item.EffectValue > 872) &&
                                    !new[] { 3951, 3952, 3953, 3954, 3955, 7427 }.Contains(
                                        value: newItem.Item.EffectValue)
                                        ? newItem.Item.EffectValue
                                        : 0
                            };
                        break;
                }

            // set default itemType
            if (newItem.Item != null) newItem.Type = newItem.Item.Type;

            return newItem;
        }

        public ItemInstance AddIntoBazaarInventory(InventoryType inventory, byte slot, short amount)
        {
            var inv = LoadBySlotAndType(slot: slot, type: inventory);
            if (inv == null || amount > inv.Amount) return null;

            var invcopy = inv.DeepCopy();
            invcopy.Id = Guid.NewGuid();
            if (inv.Item.Type == InventoryType.Equipment)
            {
                for (short i = 0; i < 32000; i++)
                    if (LoadBySlotAndType(slot: i, type: InventoryType.Bazaar) == null)
                    {
                        invcopy.Type = InventoryType.Bazaar;
                        invcopy.Slot = i;
                        invcopy.CharacterId = Owner.CharacterId;
                        DeleteFromSlotAndType(slot: inv.Slot, type: inv.Type);
                        putItem(itemInstance: invcopy);
                        break;
                    }

                Owner.Session.SendPacket(
                    packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: inventory, slot: slot));
                return invcopy;
            }

            if (amount >= inv.Amount)
            {
                for (short i = 0; i < 32000; i++)
                    if (LoadBySlotAndType(slot: i, type: InventoryType.Bazaar) == null)
                    {
                        invcopy.Type = InventoryType.Bazaar;
                        invcopy.Slot = i;
                        invcopy.CharacterId = Owner.CharacterId;
                        DeleteFromSlotAndType(slot: inv.Slot, type: inv.Type);
                        putItem(itemInstance: invcopy);
                        break;
                    }

                Owner.Session.SendPacket(
                    packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: inventory, slot: slot));
                return invcopy;
            }

            invcopy.Amount = amount;
            inv.Amount -= amount;

            for (short i = 0; i < 32000; i++)
                if (LoadBySlotAndType(slot: i, type: InventoryType.Bazaar) == null)
                {
                    invcopy.Type = InventoryType.Bazaar;
                    invcopy.Slot = i;
                    invcopy.CharacterId = Owner.CharacterId;
                    putItem(itemInstance: invcopy);
                    break;
                }

            Owner.Session.SendPacket(packet: inv.GenerateInventoryAdd());
            return invcopy;
        }

        public List<ItemInstance> AddNewToInventory(short vnum, short amount = 1, InventoryType? type = null,
            sbyte Rare = 0, byte Upgrade = 0, short Design = 0)
        {
            if (Owner != null)
            {
                var newItem = InstantiateItemInstance(vnum: vnum, ownerId: Owner.CharacterId, amount: amount);
                newItem.Rare = Rare;
                newItem.Upgrade = Upgrade == 0
                    ? newItem.Item.ItemType == ItemType.Shell ? (byte)ServerManager.RandomNumber(min: 50, max: 80) :
                    Upgrade
                    : Upgrade;
                newItem.Design = Design;
                return AddToInventory(newItem: newItem, type: type);
            }

            return new List<ItemInstance>();
        }

        public List<ItemInstance> AddToInventory(ItemInstance newItem, InventoryType? type = null)
        {
            var invlist = new List<ItemInstance>();
            if (Owner != null)
            {
                ItemInstance inv = null;

                // override type if necessary
                if (type.HasValue) newItem.Type = type.Value;

                if (newItem.Item.Effect == 420 && newItem.Item.EffectValue == 911)
                {
                    newItem.BoundCharacterId = Owner.CharacterId;
                    newItem.DurabilityPoint = (int)newItem.Item.ItemValidTime;
                }

                // check if item can be stapled
                if (newItem.Type != InventoryType.Bazaar &&
                    (newItem.Item.Type == InventoryType.Etc || newItem.Item.Type == InventoryType.Main))
                {
                    var slotNotFull = Where(predicate: i =>
                        i.Type != InventoryType.Bazaar && i.Type != InventoryType.PetWarehouse &&
                        i.Type != InventoryType.Warehouse && i.Type != InventoryType.FamilyWareHouse &&
                        i.ItemVNum.Equals(obj: newItem.ItemVNum) && i.Amount < MAX_ITEM_AMOUNT);
                    var freeslot = BackpackSize() - CountLinq(predicate: s => s.Type == newItem.Type);
                    if (freeslot < 0) freeslot = 0;
                    if (newItem.Amount <= freeslot * MAX_ITEM_AMOUNT +
                        slotNotFull.Sum(selector: s => MAX_ITEM_AMOUNT - s.Amount))
                        foreach (var slot in slotNotFull)
                        {
                            var max = slot.Amount + newItem.Amount;
                            max = max > MAX_ITEM_AMOUNT ? MAX_ITEM_AMOUNT : max;
                            newItem.Amount = (short)(slot.Amount + newItem.Amount - max);
                            newItem.Amount = (short)(newItem.Amount < 0 ? 0 : newItem.Amount);
                            Logger.LogUserEvent(logEvent: "ITEM_CREATE", caller: Owner.GenerateIdentity(),
                                data:
                                $"IIId: {slot.Id} ItemVNum: {slot.ItemVNum} Amount: {max - slot.Amount} MapId: {Owner.MapInstance?.Map.MapId} MapX: {Owner.PositionX} MapY: {Owner.PositionY}");

                            if (ServerManager.Instance.Configuration.UseLogService)
                                LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                                {
                                    Sender = Owner.Name,
                                    SenderId = Owner.CharacterId,
                                    PacketType = LogType.ItemCreate,
                                    Packet =
                                        $"[ITEM_CREATE]IIId: {slot.Id} ItemVNum: {slot.ItemVNum} Amount: {max - slot.Amount} MapId: {Owner.MapInstance?.Map.MapId} MapX: {Owner.PositionX} MapY: {Owner.PositionY}"
                                });

                            slot.Amount = (short)max;
                            invlist.Add(item: slot);
                            Owner.Session?.SendPacket(packet: slot.GenerateInventoryAdd());
                        }
                }

                if (newItem.Amount > 0)
                {
                    // create new item
                    var freeSlot = newItem.Type == InventoryType.Wear
                        ? LoadBySlotAndType(slot: (short)newItem.Item.EquipmentSlot, type: InventoryType.Wear) == null
                            ? (short?)newItem.Item.EquipmentSlot
                            : null
                        : getFreeSlot(type: newItem.Type);
                    if (freeSlot.HasValue)
                    {
                        inv = AddToInventoryWithSlotAndType(itemInstance: newItem, type: newItem.Type,
                            slot: freeSlot.Value);
                        invlist.Add(item: inv);
                    }
                }
            }

            return invlist;
        }

        /// <summary>
        ///     Add iteminstance to inventory with specified slot and type, iteminstance will be overridden.
        /// </summary>
        /// <param name="itemInstance"></param>
        /// <param name="type"></param>
        /// <param name="slot"></param>
        /// <returns></returns>
        public ItemInstance AddToInventoryWithSlotAndType(ItemInstance itemInstance, InventoryType type, short slot)
        {
            if (Owner != null)
            {
                Logger.LogUserEvent(logEvent: "ITEM_CREATE", caller: Owner.GenerateIdentity(),
                    data:
                    $"IIId: {itemInstance.Id} ItemVNum: {itemInstance.ItemVNum} Amount: {itemInstance.Amount} MapId: {Owner.MapInstance?.Map.MapId} MapX: {Owner.PositionX} MapY: {Owner.PositionY}");

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = Owner.Name,
                        SenderId = Owner.CharacterId,
                        PacketType = LogType.ItemCreate,
                        Packet =
                            $"[ITEM_CREATE]IIId: {itemInstance.Id} ItemVNum: {itemInstance.ItemVNum} Amount: {itemInstance.Amount} MapId: {Owner.MapInstance?.Map.MapId} MapX: {Owner.PositionX} MapY: {Owner.PositionY}"
                    });

                itemInstance.Slot = slot;
                itemInstance.Type = type;
                itemInstance.CharacterId = Owner.CharacterId;

                if (ContainsKey(key: itemInstance.Id))
                {
                    Logger.Error(
                        ex: new InvalidOperationException(
                            message: "Cannot add the same ItemInstance twice to inventory."));
                    return null;
                }

                var inventoryPacket = itemInstance.GenerateInventoryAdd();
                if (!string.IsNullOrEmpty(value: inventoryPacket)) Owner.Session?.SendPacket(packet: inventoryPacket);

                if (Any(predicate: s => s.Slot == slot && s.Type == type)) return null;
                this[key: itemInstance.Id] = itemInstance;
                return itemInstance;
            }

            return null;
        }

        public int BackpackSize()
        {
            return DEFAULT_BACKPACK_SIZE + (Owner.HaveBackpack() ? 1 : 0) * 12;
        }

        public bool CanAddItem(short itemVnum)
        {
            return canAddItem(type: ServerManager.GetItem(vnum: itemVnum).Type);
        }

        public int CountItem(int itemVNum)
        {
            return Where(predicate: s =>
                s.ItemVNum == itemVNum && s.Type != InventoryType.Wear && s.Type != InventoryType.FamilyWareHouse &&
                s.Type != InventoryType.Bazaar && s.Type != InventoryType.Warehouse &&
                s.Type != InventoryType.PetWarehouse).Sum(selector: i => i.Amount);
        }

        public int CountItemInAnInventory(InventoryType inv)
        {
            return CountLinq(predicate: s => s.Type == inv);
        }

        public int CountBazaarItems()
        {
            var BazaarList = ServerManager.Instance.BazaarList.GetAllItems();
            return CountLinq(predicate: s =>
                s.Type == InventoryType.Bazaar &&
                BazaarList.FirstOrDefault(predicate: b => b.BazaarItem.ItemInstanceId == s.Id) is BazaarItemLink bz &&
                (bz.BazaarItem.DateStart.AddHours(value: bz.BazaarItem.Duration)
                     .AddDays(value: bz.BazaarItem.MedalUsed ? 30 : 7) -
                 DateTime.Now).TotalMinutes > 0);
        }

        public Tuple<short, InventoryType> DeleteById(Guid id)
        {
            if (Owner != null)
            {
                Tuple<short, InventoryType> removedPlace;
                var inv = this[key: id];

                if (inv != null)
                {
                    removedPlace = new Tuple<short, InventoryType>(item1: inv.Slot, item2: inv.Type);
                    Remove(key: inv.Id);
                }
                else
                {
                    Logger.Error(
                        ex: new InvalidOperationException(
                            message: "Expected item wasn't deleted, Type or Slot did not match!"));
                    return null;
                }

                return removedPlace;
            }

            return null;
        }

        public void DeleteFromSlotAndType(short slot, InventoryType type)
        {
            if (Owner != null)
            {
                var inv = FirstOrDefault(predicate: i => i.Slot.Equals(obj: slot) && i.Type.Equals(obj: type));

                if (inv != null)
                {
                    if (Owner.Session.Character.MinilandObjects.Any(predicate: s => s.ItemInstanceId == inv.Id)) return;

                    Remove(key: inv.Id);
                }
                else
                {
                    Logger.Error(
                        ex: new InvalidOperationException(
                            message: "Expected item wasn't deleted, Type or Slot did not match!"));
                }
            }
        }

        public void DepositItem(InventoryType inventory, byte slot, short amount, byte NewSlot, ref ItemInstance item,
            ref ItemInstance itemdest, bool PartnerBackpack)
        {
            if (item != null && amount <= item.Amount && amount > 0)
            {
                MoveItem(sourcetype: inventory,
                    desttype: PartnerBackpack ? InventoryType.PetWarehouse : InventoryType.Warehouse, sourceSlot: slot,
                    amount: amount, destinationSlot: NewSlot, sourceInventory: out item,
                    destinationInventory: out itemdest);
                Owner.Session.SendPacket(packet: item != null
                    ? item.GenerateInventoryAdd()
                    : UserInterfaceHelper.Instance.GenerateInventoryRemove(type: inventory, slot: slot));

                if (itemdest != null)
                    Owner.Session.SendPacket(packet: PartnerBackpack
                        ? itemdest.GeneratePStash()
                        : itemdest.GenerateStash());
            }
        }

        public bool EnoughPlace(List<ItemInstance> itemInstances)
        {
            var place = new Dictionary<InventoryType, int>();
            foreach (var itemgroup in itemInstances.GroupBy(keySelector: s => s.ItemVNum))
                if (itemgroup.FirstOrDefault()?.Type is InventoryType type)
                {
                    var listitem = Where(predicate: i => i.Type == type);
                    if (!place.ContainsKey(key: type))
                        place.Add(key: type,
                            value: (type != InventoryType.Miniland ? BackpackSize() : 50) - listitem.Count);

                    var amount = itemgroup.Sum(selector: s => s.Amount);
                    var rest = amount % (type == InventoryType.Equipment ? 1 : 32000);
                    var needanotherslot =
                        listitem.Where(predicate: s => s.ItemVNum == itemgroup.Key)
                            .Sum(selector: s => MAX_ITEM_AMOUNT - s.Amount) <= rest;
                    place[key: type] -= amount / (type == InventoryType.Equipment ? 1 : 32000) +
                                        (needanotherslot ? 1 : 0);

                    if (place[key: type] < 0) return false;
                }
                else
                {
                    return false;
                }

            return true;
        }

        public void FDepositItem(InventoryType inventory, byte slot, short amount, byte newSlot, ref ItemInstance item,
            ref ItemInstance itemdest)
        {
            if (item != null && amount <= item.Amount && amount > 0 && item.Item.IsTradable && !item.IsBound)
            {
                var fhead = Owner.Family?.FamilyCharacters.Find(match: s => s.Authority == FamilyAuthority.Head);
                if (fhead == null) return;
                MoveItem(sourcetype: inventory, desttype: InventoryType.FamilyWareHouse, sourceSlot: slot,
                    amount: amount, destinationSlot: newSlot, sourceInventory: out item,
                    destinationInventory: out itemdest);
                itemdest.CharacterId = fhead.CharacterId;
                DaoFactory.ItemInstanceDao.InsertOrUpdate(dto: itemdest);
                Owner.Family.SendPacket(packet: item != null
                    ? item.GenerateInventoryAdd()
                    : UserInterfaceHelper.Instance.GenerateInventoryRemove(type: inventory, slot: slot));
                if (itemdest != null)
                {
                    Owner.Family.SendPacket(packet: itemdest.GenerateFStash());
                    Owner.Family?.InsertFamilyLog(logtype: FamilyLogType.WareHouseAdded, characterName: Owner.Name,
                        message: $"{itemdest.ItemVNum}|{amount}");
                    DeleteById(id: itemdest.Id);
                }
            }
        }

        public ItemInstance GetItemInstanceById(Guid id)
        {
            return this[key: id];
        }

        public ItemInstance LoadBySlotAndType(short slot, InventoryType type)
        {
            ItemInstance retItem = null;

            try
            {
                lock (_lockObject)
                {
                    retItem = SingleOrDefault(predicate: i => i.Slot.Equals(obj: slot) && i.Type.Equals(obj: type));
                }
            }
            catch (InvalidOperationException ioEx)
            {
                Logger.LogUserEventError(logEvent: nameof(LoadBySlotAndType),
                    caller: Owner?.Session?.GenerateIdentity(),
                    data: "Multiple items in slot, Splitting...", ex: ioEx);

                var isFirstItem = true;

                foreach (var item in Where(predicate: i => i.Slot.Equals(obj: slot) && i.Type.Equals(obj: type)))
                {
                    if (isFirstItem)
                    {
                        retItem = item;
                        isFirstItem = false;
                        continue;
                    }

                    var itemInstance =
                        FirstOrDefault(predicate: i => i.Slot.Equals(obj: slot) && i.Type.Equals(obj: type));

                    if (itemInstance != null)
                    {
                        var freeSlot = getFreeSlot(type: type);

                        if (freeSlot.HasValue)
                            itemInstance.Slot = freeSlot.Value;
                        else
                            Remove(key: itemInstance.Id);
                    }
                }
            }

            return retItem;
        }

        public T LoadByVNum<T>(short vNum) where T : ItemInstance
        {
            return (T)FirstOrDefault(predicate: i => i.ItemVNum.Equals(obj: vNum));
        }

        /// <summary>
        ///     Moves one item from one <see cref="Inventory" /> to another. Example: Equipment &lt;-&gt; Wear,
        ///     Equipment &lt;-&gt; Costume, Equipment &lt;-&gt; Specialist
        /// </summary>
        /// <param name="sourceSlot"></param>
        /// <param name="sourceType"></param>
        /// <param name="targetType"></param>
        /// <param name="targetSlot"></param>
        /// <param name="wear"></param>
        public ItemInstance MoveInInventory(short sourceSlot, InventoryType sourceType, InventoryType targetType,
            short? targetSlot = null, bool wear = true)
        {
            var sourceInstance = LoadBySlotAndType(slot: sourceSlot, type: sourceType);

            if (sourceInstance == null && wear)
            {
                Logger.Error(ex: new InvalidOperationException(message: "SourceInstance to move does not exist."));
                return null;
            }

            if (Owner != null && sourceInstance != null)
            {
                if (targetSlot.HasValue)
                {
                    if (wear)
                    {
                        // swap
                        var targetInstance = LoadBySlotAndType(slot: targetSlot.Value, type: targetType);

                        sourceInstance.Slot = targetSlot.Value;
                        sourceInstance.Type = targetType;

                        targetInstance.Slot = sourceSlot;
                        targetInstance.Type = sourceType;
                    }
                    else
                    {
                        // move source to target
                        var freeTargetSlot = getFreeSlot(type: targetType);
                        if (freeTargetSlot.HasValue)
                        {
                            sourceInstance.Slot = freeTargetSlot.Value;
                            sourceInstance.Type = targetType;
                        }
                    }

                    return sourceInstance;
                }

                // check for free target slot
                short? nextFreeSlot;
                switch (targetType)
                {
                    case InventoryType.FirstPartnerInventory:
                    case InventoryType.SecondPartnerInventory:
                    case InventoryType.ThirdPartnerInventory:
                    case InventoryType.FourthPartnerInventory:
                    case InventoryType.FifthPartnerInventory:
                    case InventoryType.SixthPartnerInventory:
                    case InventoryType.SeventhPartnerInventory:
                    case InventoryType.EighthPartnerInventory:
                    case InventoryType.NinthPartnerInventory:
                    case InventoryType.TenthPartnerInventory:
                    case InventoryType.Wear:
                        nextFreeSlot =
                            LoadBySlotAndType(slot: (short)sourceInstance.Item.EquipmentSlot, type: targetType) == null
                                ? (short)sourceInstance.Item.EquipmentSlot
                                : (short)-1;
                        break;

                    default:
                        nextFreeSlot = getFreeSlot(type: targetType);
                        break;
                }

                if (nextFreeSlot.HasValue)
                {
                    sourceInstance.Type = targetType;
                    sourceInstance.Slot = nextFreeSlot.Value;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

            return sourceInstance;
        }

        public void MoveItem(InventoryType sourcetype, InventoryType desttype, short sourceSlot, short amount,
            short destinationSlot, out ItemInstance sourceInventory, out ItemInstance destinationInventory)
        {
            Logger.LogUserEvent(logEvent: "ITEM_MOVE", caller: Owner.GenerateIdentity(),
                data:
                $"SourceType: {sourcetype.ToString()} DestType: {desttype.ToString()} SourceSlot: {sourceSlot} Amount: {amount} DestSlot: {destinationSlot}");

            // Load source and destination slots
            sourceInventory = LoadBySlotAndType(slot: sourceSlot, type: sourcetype);
            destinationInventory = LoadBySlotAndType(slot: destinationSlot, type: desttype);
            if (sourceInventory != null && amount <= sourceInventory.Amount)
            {
                if (destinationInventory == null)
                {
                    if (sourceInventory.Amount == amount)
                    {
                        sourceInventory.Slot = destinationSlot;
                        sourceInventory.Type = desttype;
                    }
                    else
                    {
                        var itemDest = sourceInventory.DeepCopy();
                        sourceInventory.Amount -= amount;
                        itemDest.Amount = amount;
                        itemDest.Type = desttype;
                        itemDest.Id = Guid.NewGuid();
                        AddToInventoryWithSlotAndType(itemInstance: itemDest, type: desttype, slot: destinationSlot);
                    }
                }
                else
                {
                    if (destinationInventory.ItemVNum == sourceInventory.ItemVNum &&
                        (byte)sourceInventory.Item.Type != 0)
                    {
                        if (destinationInventory.Amount + amount > MAX_ITEM_AMOUNT)
                        {
                            int saveItemCount = destinationInventory.Amount;
                            destinationInventory.Amount = MAX_ITEM_AMOUNT;
                            sourceInventory.Amount = (short)(saveItemCount + sourceInventory.Amount - MAX_ITEM_AMOUNT);
                        }
                        else
                        {
                            destinationInventory.Amount += amount;
                            sourceInventory.Amount -= amount;

                            // item with amount of 0 should be removed
                            if (sourceInventory.Amount == 0)
                                DeleteFromSlotAndType(slot: sourceInventory.Slot, type: sourceInventory.Type);
                        }
                    }
                    else
                    {
                        if (destinationInventory.Type == sourceInventory.Type ||
                            destinationInventory.Item.Type == sourcetype)
                        {
                            // add and remove save inventory
                            destinationInventory = takeItem(slot: destinationInventory.Slot,
                                type: destinationInventory.Type);
                            if (destinationInventory == null) return;

                            destinationInventory.Slot = sourceSlot;
                            destinationInventory.Type = sourcetype;
                        }
                        else
                        {
                            if (getFreeSlot(type: destinationInventory.Item.Type) is short freeSlot)
                            {
                                destinationInventory.Slot = freeSlot;
                                destinationInventory.Type = destinationInventory.Item.Type;
                                Owner.Session.SendPacket(packet: destinationInventory.GenerateInventoryAdd());

                                // add and remove save inventory
                                destinationInventory = takeItem(slot: destinationInventory.Slot,
                                    type: destinationInventory.Type);
                                if (destinationInventory == null) return;
                            }
                            else
                            {
                                Owner.Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                        type: 0));
                                return;
                            }
                        }

                        sourceInventory = takeItem(slot: sourceInventory.Slot, type: sourceInventory.Type);
                        if (sourceInventory == null) return;

                        sourceInventory.Slot = destinationSlot;
                        sourceInventory.Type = desttype;
                        putItem(itemInstance: destinationInventory);
                        putItem(itemInstance: sourceInventory);
                    }
                }
            }

            sourceInventory = LoadBySlotAndType(slot: sourceSlot, type: sourcetype);
            destinationInventory = LoadBySlotAndType(slot: destinationSlot, type: desttype);
        }

        public void RemoveItemAmount(int vnum, int amount = 1)
        {
            if (Owner != null)
            {
                var remainingAmount = amount;

                foreach (var inventory in Where(predicate: s =>
                    s.ItemVNum == vnum && s.Type != InventoryType.Wear && s.Type != InventoryType.Bazaar &&
                    s.Type != InventoryType.Warehouse && s.Type != InventoryType.PetWarehouse &&
                    s.Type != InventoryType.FamilyWareHouse).OrderBy(keySelector: i => i.Slot))
                    if (remainingAmount > 0)
                    {
                        if (inventory.Amount > remainingAmount)
                        {
                            // Amount completely removed
                            inventory.Amount -= (short)remainingAmount;
                            remainingAmount = 0;
                            Owner.Session.SendPacket(packet: inventory.GenerateInventoryAdd());
                        }
                        else
                        {
                            // Amount partly removed
                            remainingAmount -= inventory.Amount;
                            DeleteById(id: inventory.Id);
                            Owner.Session.SendPacket(
                                packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: inventory.Type,
                                    slot: inventory.Slot));
                        }
                    }
                    else
                    {
                        // Amount to remove reached
                        break;
                    }
            }
        }

        public void RemoveItemFromInventory(Guid id, short amount = 1)
        {
            if (Owner != null)
            {
                var inv = FirstOrDefault(predicate: i => i.Id.Equals(g: id));
                if (inv != null)
                {
                    inv.Amount -= amount;
                    if (inv.Amount <= 0)
                    {
                        Owner.Session.SendPacket(
                            packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: inv.Type,
                                slot: inv.Slot));
                        Remove(key: inv.Id);
                        return;
                    }

                    Owner.Session.SendPacket(packet: inv.GenerateInventoryAdd());
                }
            }
        }

        /// <summary>
        ///     Reorders item in given inventorytype
        /// </summary>
        /// <param name="session"></param>
        /// <param name="inventoryType"></param>
        public void Reorder(ClientSession session, InventoryType inventoryType)
        {
            var itemsByInventoryType = new List<ItemInstance>();
            switch (inventoryType)
            {
                case InventoryType.Costume:
                    itemsByInventoryType =
                        Where(predicate: s => s.Type == InventoryType.Costume).OrderBy(keySelector: s => s.ItemVNum)
                            .ToList();
                    break;

                case InventoryType.Specialist:
                    itemsByInventoryType = Where(predicate: s => s.Type == InventoryType.Specialist)
                        .OrderBy(keySelector: s => s.Item.LevelJobMinimum).ToList();
                    break;

                default:
                    itemsByInventoryType = Where(predicate: s => s.Type == inventoryType)
                        .OrderBy(keySelector: s => s.Item.Price).ToList();
                    break;
            }

            generateClearInventory(type: inventoryType);
            for (short i = 0; i < itemsByInventoryType.Count; i++)
            {
                var item = itemsByInventoryType[index: i];
                item.Slot = i;
                this[key: item.Id].Slot = i;
                session.SendPacket(packet: item.GenerateInventoryAdd());
            }
        }

        bool canAddItem(InventoryType type)
        {
            return Owner != null && getFreeSlot(type: type).HasValue;
        }

        void generateClearInventory(InventoryType type)
        {
            if (Owner != null)
                for (short i = 0; i < DEFAULT_BACKPACK_SIZE; i++)
                    if (LoadBySlotAndType(slot: i, type: type) != null)
                        Owner.Session.SendPacket(
                            packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: type, slot: i));
        }

        /// <summary>
        ///     Gets free slots in given inventory type
        /// </summary>
        /// <param name="type"></param>
        /// <returns>short?; based on given inventory type</returns>
        short? getFreeSlot(InventoryType type)
        {
            var itemInstanceSlotsByType = Where(predicate: i => i.Type == type).OrderBy(keySelector: i => i.Slot)
                .Select(selector: i => (int)i.Slot);
            IEnumerable<int> instanceSlotsByType =
                itemInstanceSlotsByType as int[] ?? itemInstanceSlotsByType.ToArray();
            var backpackSize = BackpackSize();
            var maxRange = (type != InventoryType.Miniland ? backpackSize : 50) + 1;
            var nextFreeSlot = instanceSlotsByType.Any()
                ? Enumerable.Range(start: 0, count: maxRange).Except(second: instanceSlotsByType).Cast<int?>()
                    .FirstOrDefault()
                : 0;
            return (short?)nextFreeSlot < (type != InventoryType.Miniland ? backpackSize : 50)
                ? (short?)nextFreeSlot
                : null;
        }

        /// <summary>
        ///     Puts a Single ItemInstance to the Inventory
        /// </summary>
        /// <param name="itemInstance"></param>
        void putItem(ItemInstance itemInstance)
        {
            this[key: itemInstance.Id] = itemInstance;
        }

        /// <summary>
        ///     Takes a Single Inventory including ItemInstance from the List and removes it.
        /// </summary>
        /// <param name="slot"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        ItemInstance takeItem(short slot, InventoryType type)
        {
            var itemInstance = SingleOrDefault(predicate: i => i.Slot == slot && i.Type == type);
            if (itemInstance != null)
            {
                Remove(key: itemInstance.Id);
                return itemInstance;
            }

            return null;
        }

        #endregion
    }
}