﻿using System.Collections.Generic;

namespace OpenNos.GameObject.Helpers
{
    public static class MappingHelper
    {
        #region Instantiation

        static MappingHelper()
        {
            // intialize hardcode in waiting for better solution
            GuriItemEffects = new Dictionary<int, int>
            {
                [key: 859] = 1343,
                [key: 860] = 1344,
                [key: 861] = 1344,
                [key: 875] = 1558,
                [key: 876] = 1559,
                [key: 877] = 1560,
                [key: 878] = 1560,
                [key: 879] = 1561,
                [key: 880] = 1561
            };
        }

        #endregion

        // effect items aka. fireworks

        #region Properties

        public static Dictionary<int, int> GuriItemEffects { get; }

        #endregion
    }
}