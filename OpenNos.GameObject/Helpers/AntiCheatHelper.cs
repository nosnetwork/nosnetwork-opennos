﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Helpers
{
    public static class AntiCheatHelper
    {
        #region Members

        static readonly string _table = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        #endregion

        #region Properties

        public static bool IsAntiCheatEnabled
        {
            get => ServerManager.Instance.Configuration.IsAntiCheatEnabled;
        }

        public static string ClientKey
        {
            get => ServerManager.Instance.Configuration.AntiCheatClientKey;
        }

        public static string ServerKey
        {
            get => ServerManager.Instance.Configuration.AntiCheatServerKey;
        }

        #endregion

        #region Methods

        public static string Encrypt(byte[] data, byte[] encryptedKey)
        {
            var plainTextKey = encryptedKey;

            for (var i = 0; i < data.Length; i++) plainTextKey[i] ^= 0x0F;

            for (var i = 0; i < data.Length; i++) data[i] ^= plainTextKey[i];

            return Encoding.UTF8.GetString(bytes: data);
        }

        public static string GenerateData(int length)
        {
            var result = "";

            while (length-- > 0) result += _table[index: ServerManager.RandomNumber(min: 0, max: _table.Length)];

            return result;
        }

        public static bool IsValidSignature(string signature, byte[] data, byte[] encryptedKey, string crc32)
        {
            var buffer = data.Concat(second: encryptedKey).Concat(second: Encoding.ASCII.GetBytes(s: crc32))
                .Concat(second: Encoding.ASCII.GetBytes(s: ServerKey)).ToArray();

            return signature == Sha512(buffer: buffer);
        }

        public static string Sha512(string input)
        {
            return Sha512(buffer: Encoding.ASCII.GetBytes(s: input));
        }

        public static string Sha512(byte[] buffer)
        {
            using (var sha512 = SHA512.Create())
            {
                return string.Concat(values: sha512.ComputeHash(buffer: buffer)
                    .Select(selector: s => s.ToString(format: "X2")));
            }
        }

        #endregion
    }
}