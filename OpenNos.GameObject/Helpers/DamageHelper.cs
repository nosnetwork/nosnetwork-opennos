using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using OpenNos.Domain;
using OpenNos.GameObject.Networking;
using static OpenNos.Domain.BCardType;

namespace OpenNos.GameObject.Helpers
{
    public class DamageHelper
    {
        #region Members

        static DamageHelper _instance;

        #endregion

        #region Properties

        public static DamageHelper Instance
        {
            get => _instance ??= new DamageHelper();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Calculates the damage attacker inflicts defender
        /// </summary>
        /// <param name="attacker">The attacking Entity</param>
        /// <param name="defender">The defending Entity</param>
        /// <param name="skill">The used Skill</param>
        /// <param name="hitMode">reference to HitMode</param>
        /// <param name="onyxWings"></param>
        /// <returns>Damage</returns>
        public int CalculateDamage(BattleEntity attacker, BattleEntity defender, Skill skill, ref int hitMode,
            ref bool onyxWings, bool attackGreaterDistance = false)
        {
            if (!attacker.CanAttackEntity(receiver: defender))
            {
                hitMode = 2;
                return 0;
            }

            if (attacker.Character?.Timespace != null &&
                attacker.Character.Timespace.SpNeeded?[(byte)attacker.Character.Class] != 0)
            {
                var specialist =
                    attacker.Character.Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                        type: InventoryType.Wear);
                if (specialist == null || specialist.ItemVNum !=
                    attacker.Character?.Timespace.SpNeeded?[(byte)attacker.Character.Class])
                {
                    hitMode = 2;
                    return 0;
                }
            }

            if (skill != null && SkillHelper.IsSelfAttack(skillVNum: skill.SkillVNum))
            {
                hitMode = 0;
                return 0;
            }

            var maxRange = skill != null ? skill.Range > 0 ? skill.Range :
                skill.TargetRange :
                attacker.Mate != null ? attacker.Mate.Monster.BasicRange > 0 ? attacker.Mate.Monster.BasicRange : 10 :
                attacker.MapMonster != null ? attacker.MapMonster.Monster.BasicRange > 0
                    ? attacker.MapMonster.Monster.BasicRange
                    : 10 :
                attacker.MapNpc != null ? attacker.MapNpc.Npc.BasicRange > 0 ? attacker.MapNpc.Npc.BasicRange : 10 : 0;

            if (skill != null && skill.HitType == 1 && skill.TargetType == 1 && skill.TargetRange > 0)
                maxRange = skill.TargetRange;

            if (skill != null && skill.HitType == 2 && skill.TargetType == 1 && skill.TargetRange > 0)
                maxRange = skill.TargetRange;

            if (skill != null && (skill.CastEffect == 4657 || skill.CastEffect == 4940)) maxRange = 3;

            if (attacker.EntityType == defender.EntityType && attacker.MapEntityId == defender.MapEntityId
                || attacker.Character == null && attacker.Mate == null &&
                Map.GetDistance(p: new MapCell { X = attacker.PositionX, Y = attacker.PositionY },
                    q: new MapCell { X = defender.PositionX, Y = defender.PositionY }) > maxRange)
                if (skill == null || skill.TargetRange != 0 || skill.Range != 0 && !attackGreaterDistance)
                {
                    hitMode = 2;
                    return 0;
                }

            if (skill != null && skill.BCards.Any(predicate: s =>
                s.Type == (byte)CardType.DrainAndSteal &&
                s.SubType == (byte)AdditionalTypes.DrainAndSteal.ConvertEnemyHpToMp / 10)) return 0;

            if (attacker.Character != null
                && (attacker.Character.UseSp
                    && attacker.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                            type: InventoryType.Wear) is
                        ItemInstance attackerSp
                    && skill?.Element == 0
                    && attackerSp.ItemVNum != 900
                    && attackerSp.ItemVNum != 907
                    && attackerSp.ItemVNum != 908
                    && attackerSp.ItemVNum != 4099
                    && attackerSp.ItemVNum != 4100 &&
                    (skill == null || defender.MapMonster?.Monster.Race != 5 || !skill.BCards.Any(predicate: s =>
                        s.Type == (byte)CardType.LightAndShadow && s.SubType ==
                        (byte)AdditionalTypes.LightAndShadow.InflictDamageOnUndead / 10)) &&
                    skill?.SkillVNum != 1065 && skill?.SkillVNum != 1248
                    || defender.MapMonster?.Owner?.MapEntityId == attacker.MapEntityId &&
                    !defender.IsMateTrainer(vnum: defender.MapMonster.MonsterVNum))
                || skill?.SkillVNum >= 235 && skill?.SkillVNum <= 237 || skill?.SkillVNum == 274 ||
                skill?.SkillVNum == 276 || skill?.SkillVNum == 892 || skill?.SkillVNum == 916
                || skill?.SkillVNum == 1129 || skill?.SkillVNum == 1133 || skill?.SkillVNum == 1137 ||
                skill?.SkillVNum == 1138 || skill?.SkillVNum == 1329
                || attacker.MapMonster?.MonsterVNum == 1438 || attacker.MapMonster?.MonsterVNum == 1439)
            {
                hitMode = 0;
                return 0;
            }

            if (defender.Character != null && defender.Character.HasGodMode)
            {
                hitMode = 0;
                return 0;
            }

            if (defender.MapMonster != null && skill?.SkillVNum != 888 &&
                MonsterHelper.IsNamaju(monsterVNum: defender.MapMonster.MonsterVNum))
            {
                hitMode = 0;
                return 0;
            }

            if (attacker.MapMonster?.MonsterVNum == 1436)
            {
                hitMode = 0;
                return 0;
            }

            if (attacker.HasBuff(type: CardType.NoDefeatAndNoDamage,
                    subtype: (byte)AdditionalTypes.NoDefeatAndNoDamage.NeverCauseDamage)
                || defender.HasBuff(type: CardType.NoDefeatAndNoDamage,
                    subtype: (byte)AdditionalTypes.NoDefeatAndNoDamage.NeverReceiveDamage))
            {
                hitMode = 0;
                return 0;
            }

            var totalDamage = 0;
            var percentDamage = false;

            var realAttacker = attacker;

            if (attacker.MapMonster?.Owner?.Character != null &&
                !attacker.IsMateTrainer(vnum: attacker.MapMonster.MonsterVNum))
                if (attacker.DamageMinimum == 0 ||
                    MonsterHelper.UseOwnerEntity(monsterVNum: attacker.MapMonster.MonsterVNum))
                    attacker = new BattleEntity(character: attacker.MapMonster.Owner.Character, skill: skill);

            var attackerBCards = attacker.BCards.ToList();
            var defenderBCards = defender.BCards.ToList();

            if (attacker.Character != null)
            {
                var skills = attacker.Character.GetSkills();
                //Upgrade Skills
                if (skill != null &&
                    skills.FirstOrDefault(predicate: s => s.SkillVNum == skill.SkillVNum) is CharacterSkill charSkill)
                {
                    attackerBCards.AddRange(collection: charSkill.GetSkillBCards());
                }
                else // Passive Skills are getted on GetSkillBCards()
                {
                    if (skill?.BCards != null) attackerBCards.AddRange(collection: skill.BCards);
                    //Passive Skills
                    attackerBCards.AddRange(
                        collection: PassiveSkillHelper.Instance.PassiveSkillToBCards(
                            skills: attacker.Character.Skills?.Where(predicate: s => s.Skill.SkillType == 0)));
                }
            }
            else
            {
                if (skill?.BCards != null) attackerBCards.AddRange(collection: skill.BCards);
            }

            int[] GetAttackerBenefitingBuffs(CardType type, byte subtype, bool castTypeNotZero = false)
            {
                var value1 = 0;
                var value2 = 0;
                var value3 = 0;
                var temp = 0;

                var tmp = GetBuff(level: attacker.Level, buffs: attacker.Buffs.GetAllItems(), bcards: attackerBCards,
                    type: type, subtype: subtype,
                    btype: BuffType.Good,
                    count: ref temp, castTypeNotZero: castTypeNotZero);
                value1 += tmp[0];
                value2 += tmp[1];
                value3 += tmp[2];
                tmp = GetBuff(level: attacker.Level, buffs: attacker.Buffs.GetAllItems(), bcards: attackerBCards,
                    type: type, subtype: subtype,
                    btype: BuffType.Neutral,
                    count: ref temp, castTypeNotZero: castTypeNotZero);
                value1 += tmp[0];
                value2 += tmp[1];
                value3 += tmp[2];
                tmp = GetBuff(level: defender.Level, buffs: defender.Buffs.GetAllItems(), bcards: defenderBCards,
                    type: type, subtype: subtype, btype: BuffType.Bad,
                    count: ref temp, castTypeNotZero: castTypeNotZero);
                value1 += tmp[0];
                value2 += tmp[1];
                value3 += tmp[2];

                return new[] { value1, value2, value3, temp };
            }

            int[] GetDefenderBenefitingBuffs(CardType type, byte subtype)
            {
                var value1 = 0;
                var value2 = 0;
                var value3 = 0;
                var temp = 0;

                var tmp = GetBuff(level: defender.Level, buffs: defender.Buffs.GetAllItems(), bcards: defenderBCards,
                    type: type, subtype: subtype,
                    btype: BuffType.Good,
                    count: ref temp);
                value1 += tmp[0];
                value2 += tmp[1];
                value3 += tmp[2];
                tmp = GetBuff(level: defender.Level, buffs: defender.Buffs.GetAllItems(), bcards: defenderBCards,
                    type: type, subtype: subtype,
                    btype: BuffType.Neutral,
                    count: ref temp);
                value1 += tmp[0];
                value2 += tmp[1];
                value3 += tmp[2];
                tmp = GetBuff(level: attacker.Level, buffs: attacker.Buffs.GetAllItems(), bcards: attackerBCards,
                    type: type, subtype: subtype, btype: BuffType.Bad,
                    count: ref temp);
                value1 += tmp[0];
                value2 += tmp[1];
                value3 += tmp[2];

                return new[] { value1, value2, value3, temp };
            }

            int GetShellWeaponEffectValue(ShellWeaponEffectType effectType)
            {
                return attacker.ShellWeaponEffects?.Where(predicate: s => s.Effect == (byte)effectType)
                           .FirstOrDefault()?.Value ??
                       0;
            }

            int GetShellArmorEffectValue(ShellArmorEffectType effectType)
            {
                return defender.ShellArmorEffects?.Where(predicate: s => s.Effect == (byte)effectType).FirstOrDefault()
                           ?.Value ??
                       0;
            }

            #region Basic Buff Initialisation

            attacker.Morale +=
                GetAttackerBenefitingBuffs(type: CardType.Morale,
                    subtype: (byte)AdditionalTypes.Morale.MoraleIncreased)[0];
            attacker.Morale +=
                GetDefenderBenefitingBuffs(type: CardType.Morale,
                    subtype: (byte)AdditionalTypes.Morale.MoraleDecreased)[0];
            defender.Morale +=
                GetDefenderBenefitingBuffs(type: CardType.Morale,
                    subtype: (byte)AdditionalTypes.Morale.MoraleIncreased)[0];
            defender.Morale +=
                GetAttackerBenefitingBuffs(type: CardType.Morale,
                    subtype: (byte)AdditionalTypes.Morale.MoraleDecreased)[0];

            attacker.AttackUpgrade += (short)GetAttackerBenefitingBuffs(type: CardType.AttackPower,
                subtype: (byte)AdditionalTypes.AttackPower.AttackLevelIncreased)[0];
            attacker.AttackUpgrade += (short)GetDefenderBenefitingBuffs(type: CardType.AttackPower,
                subtype: (byte)AdditionalTypes.AttackPower.AttackLevelDecreased)[0];
            defender.DefenseUpgrade += (short)GetDefenderBenefitingBuffs(type: CardType.Defence,
                subtype: (byte)AdditionalTypes.Defence.DefenceLevelIncreased)[0];
            defender.DefenseUpgrade += (short)GetAttackerBenefitingBuffs(type: CardType.Defence,
                subtype: (byte)AdditionalTypes.Defence.DefenceLevelDecreased)[0];

            if (attacker.AttackUpgrade > 10) attacker.AttackUpgrade = 10;
            if (defender.DefenseUpgrade > 10) defender.DefenseUpgrade = 10;

            var attackerpercentdamage = GetAttackerBenefitingBuffs(type: CardType.RecoveryAndDamagePercent,
                subtype: (byte)AdditionalTypes.RecoveryAndDamagePercent.HpRecovered, castTypeNotZero: true);
            var attackerpercentdamage2 = GetAttackerBenefitingBuffs(type: CardType.RecoveryAndDamagePercent,
                subtype: (byte)AdditionalTypes.RecoveryAndDamagePercent.DecreaseEnemyHp);
            var defenderpercentdefense = GetDefenderBenefitingBuffs(type: CardType.RecoveryAndDamagePercent,
                subtype: (byte)AdditionalTypes.RecoveryAndDamagePercent.DecreaseSelfHp);

            if (attackerpercentdamage[3] != 0)
            {
                totalDamage = defender.HpMax / 100 * attackerpercentdamage[2];
                percentDamage = true;
            }

            if (attackerpercentdamage2[3] != 0)
            {
                totalDamage = defender.HpMax / 100 * Math.Abs(value: attackerpercentdamage2[0]);
                percentDamage = true;
            }

            if (defenderpercentdefense[3] != 0)
            {
                totalDamage = defender.HpMax / 100 * Math.Abs(value: defenderpercentdefense[0]);
                percentDamage = true;
            }

            if (defender.MapMonster != null && defender.MapMonster.MonsterVNum == 1381 && // Jack O'Lantern
                (attacker.Character != null && attacker.Character.Group?.Raid?.Id == 10
                 || attacker.Mate != null && attacker.Mate.Owner.Group?.Raid?.Id == 10
                 || attacker.MapMonster != null && (attacker.MapMonster.Owner?.Character?.Group?.Raid?.Id == 10 ||
                                                    attacker.MapMonster.Owner?.Mate?.Owner.Group?.Raid?.Id == 10)))
            {
                if (attacker.Character != null && attacker.Character.IsMorphed)
                    totalDamage = 600;
                else
                    totalDamage = 150;
                percentDamage = true;
            }

            if (defender.MapMonster?.MonsterVNum == 533)
            {
                totalDamage = 63;
                percentDamage = true;
            }

            if (skill?.SkillVNum == 529 &&
                (defender.Character?.PyjamaDead == true || defender.Mate?.Owner.PyjamaDead == true))
            {
                totalDamage = (int)(defender.HpMax * 0.8D);
                percentDamage = true;
            }

            /*
             *
             * Percentage Boost categories:
             *  1.: Adds to Total Damage
             *  2.: Adds to Normal Damage
             *  3.: Adds to Base Damage
             *  4.: Adds to Defense
             *  5.: Adds to Element
             *
             * Buff Effects get added, whereas
             * Shell Effects get multiplied afterwards.
             *
             * Simplified Example on Defense (Same for Attack):
             *  - 1k Defense
             *  - Costume(+5% Defense)
             *  - Defense Potion(+20% Defense)
             *  - S-Defense Shell with 20% Boost
             *
             * Calculation:
             *  1000 * 1.25 * 1.2 = 1500
             *  Def    Buff   Shell Total
             *
             * Keep in Mind that after each step, one has
             * to round the current value down if necessary
             *
             * Static Boost categories:
             *  1.: Adds to Total Damage
             *  2.: Adds to Normal Damage
             *  3.: Adds to Base Damage
             *  4.: Adds to Defense
             *  5.: Adds to Element
             *
             */

            attacker.Morale -= defender.Morale;

            var hitrate = attacker.Hitrate + attacker.Morale;

            #region Definitions

            double boostCategory1 = 1;
            double boostCategory2 = 1;
            double boostCategory3 = 1;
            double boostCategory4 = 1;
            double boostCategory5 = 1;
            double shellBoostCategory1 = 1;
            double shellBoostCategory2 = 1;
            double shellBoostCategory3 = 1;
            double shellBoostCategory4 = 1;
            double shellBoostCategory5 = 1;
            var staticBoostCategory1 = 0;
            var staticBoostCategory2 = 0;
            var staticBoostCategory3 = 0;
            var staticBoostCategory4 = 0;
            var staticBoostCategory5 = 0;

            #endregion

            #region Type 1

            #region Static

            // None for now

            #endregion

            #region Boost

            shellBoostCategory1 += GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.PercentageTotalDamage) /
                                   100D;

            if ((attacker.EntityType == EntityType.Player || attacker.EntityType == EntityType.Mate)
                && (defender.EntityType == EntityType.Player || defender.EntityType == EntityType.Mate))
                shellBoostCategory1 +=
                    GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.PercentageDamageInPvp) / 100D;

            #endregion

            #endregion

            #region Type 2

            #region Static

            if (attacker.Character != null && attacker.Character.Invisible)
                staticBoostCategory2 +=
                    GetAttackerBenefitingBuffs(type: CardType.LightAndShadow,
                        subtype: (byte)AdditionalTypes.LightAndShadow.AdditionalDamageWhenHidden)[0];

            #endregion

            #region Boost

            boostCategory2 +=
                GetAttackerBenefitingBuffs(type: CardType.Damage,
                        subtype: (byte)AdditionalTypes.Damage.DamageIncreased)
                    [0] / 100D;
            boostCategory2 +=
                GetDefenderBenefitingBuffs(type: CardType.Damage,
                        subtype: (byte)AdditionalTypes.Damage.DamageDecreased)
                    [0] / 100D;
            boostCategory2 +=
                GetAttackerBenefitingBuffs(type: CardType.Item, subtype: (byte)AdditionalTypes.Item.AttackIncreased)[0]
                / 100D;
            boostCategory2 +=
                GetDefenderBenefitingBuffs(type: CardType.Item,
                    subtype: (byte)AdditionalTypes.Item.DefenceIncreased)[0]
                / 100D;

            if ((attacker.EntityType == EntityType.Player || attacker.EntityType == EntityType.Mate)
                && (defender.EntityType == EntityType.Player || defender.EntityType == EntityType.Mate))
            {
                boostCategory2 += GetAttackerBenefitingBuffs(type: CardType.SpecialisationBuffResistance,
                                      subtype: (byte)AdditionalTypes.SpecialisationBuffResistance.IncreaseDamageInPvp)[
                                      0]
                                  / 100D;
                boostCategory2 += GetDefenderBenefitingBuffs(type: CardType.SpecialisationBuffResistance,
                                      subtype: (byte)AdditionalTypes.SpecialisationBuffResistance.DecreaseDamageInPvp)[
                                      0]
                                  / 100D;
                boostCategory2 += GetAttackerBenefitingBuffs(type: CardType.LeonaPassiveSkill,
                    subtype: (byte)AdditionalTypes.LeonaPassiveSkill.AttackIncreasedInPvp)[0] / 100D;
                boostCategory2 += GetDefenderBenefitingBuffs(type: CardType.LeonaPassiveSkill,
                    subtype: (byte)AdditionalTypes.LeonaPassiveSkill.AttackDecreasedInPvp)[0] / 100D;
            }

            if (defender.MapMonster != null)
                if (GetAttackerBenefitingBuffs(type: CardType.LeonaPassiveSkill,
                            subtype: (byte)AdditionalTypes.LeonaPassiveSkill.IncreaseDamageAgainst) is int[]
                        IncreaseDamageAgainst
                    && IncreaseDamageAgainst[1] > 0 && defender.MapMonster.Monster.RaceType == IncreaseDamageAgainst[0])
                    boostCategory2 += IncreaseDamageAgainst[1] / 100D;

            #endregion

            #endregion

            #region Type 3

            #region Static

            staticBoostCategory3 += GetAttackerBenefitingBuffs(type: CardType.AttackPower,
                subtype: (byte)AdditionalTypes.AttackPower.AllAttacksIncreased)[0];
            staticBoostCategory3 += GetDefenderBenefitingBuffs(type: CardType.AttackPower,
                subtype: (byte)AdditionalTypes.AttackPower.AllAttacksDecreased)[0];
            staticBoostCategory3 += GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.DamageImproved);

            #endregion

            #region Soft-Damage

            var soft = GetAttackerBenefitingBuffs(type: CardType.IncreaseDamage,
                subtype: (byte)AdditionalTypes.IncreaseDamage.IncreasingPropability);
            var skin = GetAttackerBenefitingBuffs(type: CardType.EffectSummon,
                subtype: (byte)AdditionalTypes.EffectSummon.DamageBoostOnHigherLvl);
            if (attacker.Level < defender.Level)
            {
                soft[0] += skin[0];
                soft[1] += skin[1];
            }

            if (attacker == realAttacker && ServerManager.RandomNumber() < soft[0])
            {
                boostCategory3 += soft[1] / 100D;
                attacker.MapInstance?.Broadcast(
                    packet: StaticPacketHelper.GenerateEff(effectType: realAttacker.UserType,
                        callerId: realAttacker.MapEntityId, effectId: 15));
            }

            #endregion

            #endregion

            #region Type 4

            #region Static

            staticBoostCategory4 +=
                GetDefenderBenefitingBuffs(type: CardType.Defence,
                    subtype: (byte)AdditionalTypes.Defence.AllIncreased)[0];
            staticBoostCategory4 +=
                GetAttackerBenefitingBuffs(type: CardType.Defence,
                    subtype: (byte)AdditionalTypes.Defence.AllDecreased)[0];

            var temp2 = 0;
            staticBoostCategory4 +=
                GetBuff(level: defender.Level, buffs: defender.Buffs.GetAllItems(), bcards: defenderBCards,
                    type: CardType.Defence,
                    subtype: (byte)AdditionalTypes.Defence.AllDecreased, btype: BuffType.Good, count: ref temp2)[0];

            #endregion

            #region Boost

            boostCategory4 += GetDefenderBenefitingBuffs(type: CardType.DodgeAndDefencePercent,
                subtype: (byte)AdditionalTypes.DodgeAndDefencePercent.DefenceIncreased)[0] / 100D;
            boostCategory4 += GetAttackerBenefitingBuffs(type: CardType.DodgeAndDefencePercent,
                subtype: (byte)AdditionalTypes.DodgeAndDefencePercent.DefenceReduced)[0] / 100D;
            shellBoostCategory4 +=
                GetShellArmorEffectValue(effectType: ShellArmorEffectType.PercentageTotalDefence) / 100D;

            if ((attacker.EntityType == EntityType.Player || attacker.EntityType == EntityType.Mate)
                && (defender.EntityType == EntityType.Player || defender.EntityType == EntityType.Mate))
            {
                boostCategory4 += GetDefenderBenefitingBuffs(type: CardType.LeonaPassiveSkill,
                                      subtype: (byte)AdditionalTypes.LeonaPassiveSkill.DefenceIncreasedInPvp)[0] /
                                  100D;
                boostCategory4 += GetAttackerBenefitingBuffs(type: CardType.LeonaPassiveSkill,
                                      subtype: (byte)AdditionalTypes.LeonaPassiveSkill.DefenceDecreasedInPvp)[0] /
                                  100D;
                shellBoostCategory4 -=
                    GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.ReducesPercentageEnemyDefenceInPvp) /
                    100D;
                shellBoostCategory4 +=
                    GetShellArmorEffectValue(effectType: ShellArmorEffectType.PercentageAllPvpDefence) / 100D;
            }

            var chanceAllIncreased =
                GetAttackerBenefitingBuffs(type: CardType.Block,
                    subtype: (byte)AdditionalTypes.Block.ChanceAllIncreased);
            var chanceAllDecreased =
                GetDefenderBenefitingBuffs(type: CardType.Block,
                    subtype: (byte)AdditionalTypes.Block.ChanceAllDecreased);

            if (ServerManager.RandomNumber() < chanceAllIncreased[0]) boostCategory1 += chanceAllIncreased[1] / 100D;

            if (ServerManager.RandomNumber() < -chanceAllDecreased[0]) boostCategory1 -= chanceAllDecreased[1] / 100D;

            #endregion

            #endregion

            #region Type 5

            #region Static

            staticBoostCategory5 +=
                GetAttackerBenefitingBuffs(type: CardType.Element,
                    subtype: (byte)AdditionalTypes.Element.AllIncreased)[0];
            staticBoostCategory5 +=
                GetDefenderBenefitingBuffs(type: CardType.Element,
                    subtype: (byte)AdditionalTypes.Element.AllDecreased)[0];
            staticBoostCategory5 +=
                GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.IncreasedElementalProperties);

            #endregion

            #region Boost

            #endregion

            #endregion

            #region All Type Class Dependant

            int[] chanceIncreased = null;
            int[] chanceDecreased = null;

            switch (attacker.AttackType)
            {
                case AttackType.Melee:
                    chanceIncreased = GetAttackerBenefitingBuffs(type: CardType.Block,
                        subtype: (byte)AdditionalTypes.Block.ChanceMeleeIncreased);
                    chanceDecreased = GetDefenderBenefitingBuffs(type: CardType.Block,
                        subtype: (byte)AdditionalTypes.Block.ChanceMeleeDecreased);
                    boostCategory2 +=
                        GetAttackerBenefitingBuffs(type: CardType.Damage,
                            subtype: (byte)AdditionalTypes.Damage.MeleeIncreased)[0] /
                        100D;
                    boostCategory2 +=
                        GetDefenderBenefitingBuffs(type: CardType.Damage,
                            subtype: (byte)AdditionalTypes.Damage.MeleeDecreased)[0] /
                        100D;
                    staticBoostCategory3 += GetAttackerBenefitingBuffs(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MeleeAttacksIncreased)[0];
                    staticBoostCategory3 += GetDefenderBenefitingBuffs(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MeleeAttacksDecreased)[0];
                    staticBoostCategory4 += GetShellArmorEffectValue(effectType: ShellArmorEffectType.CloseDefence);
                    staticBoostCategory4 +=
                        GetDefenderBenefitingBuffs(type: CardType.Defence,
                            subtype: (byte)AdditionalTypes.Defence.MeleeIncreased)[0];
                    staticBoostCategory4 +=
                        GetAttackerBenefitingBuffs(type: CardType.Defence,
                            subtype: (byte)AdditionalTypes.Defence.MeleeDecreased)[0];
                    break;

                case AttackType.Range:
                    chanceIncreased = GetAttackerBenefitingBuffs(type: CardType.Block,
                        subtype: (byte)AdditionalTypes.Block.ChanceRangedIncreased);
                    chanceDecreased = GetDefenderBenefitingBuffs(type: CardType.Block,
                        subtype: (byte)AdditionalTypes.Block.ChanceRangedDecreased);
                    boostCategory2 +=
                        GetAttackerBenefitingBuffs(type: CardType.Damage,
                            subtype: (byte)AdditionalTypes.Damage.RangedIncreased)[0] /
                        100D;
                    boostCategory2 +=
                        GetDefenderBenefitingBuffs(type: CardType.Damage,
                            subtype: (byte)AdditionalTypes.Damage.RangedDecreased)[0] /
                        100D;
                    staticBoostCategory3 += GetAttackerBenefitingBuffs(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MeleeAttacksIncreased)[0];
                    staticBoostCategory3 += GetDefenderBenefitingBuffs(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MeleeAttacksDecreased)[0];
                    staticBoostCategory4 += GetShellArmorEffectValue(effectType: ShellArmorEffectType.DistanceDefence);
                    staticBoostCategory4 +=
                        GetDefenderBenefitingBuffs(type: CardType.Defence,
                            subtype: (byte)AdditionalTypes.Defence.RangedIncreased)[0];
                    staticBoostCategory4 +=
                        GetAttackerBenefitingBuffs(type: CardType.Defence,
                            subtype: (byte)AdditionalTypes.Defence.RangedDecreased)[0];
                    break;

                case AttackType.Magical:
                    chanceIncreased = GetAttackerBenefitingBuffs(type: CardType.Block,
                        subtype: (byte)AdditionalTypes.Block.ChanceMagicalIncreased);
                    chanceDecreased = GetDefenderBenefitingBuffs(type: CardType.Block,
                        subtype: (byte)AdditionalTypes.Block.ChanceMagicalDecreased);
                    boostCategory2 +=
                        GetAttackerBenefitingBuffs(type: CardType.Damage,
                            subtype: (byte)AdditionalTypes.Damage.MagicalIncreased)[0] /
                        100D;
                    boostCategory2 +=
                        GetDefenderBenefitingBuffs(type: CardType.Damage,
                            subtype: (byte)AdditionalTypes.Damage.MagicalDecreased)[0] /
                        100D;
                    staticBoostCategory3 += GetAttackerBenefitingBuffs(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MeleeAttacksIncreased)[0];
                    staticBoostCategory3 += GetDefenderBenefitingBuffs(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MeleeAttacksDecreased)[0];
                    staticBoostCategory4 += GetShellArmorEffectValue(effectType: ShellArmorEffectType.MagicDefence);
                    staticBoostCategory4 += GetDefenderBenefitingBuffs(type: CardType.Defence,
                        subtype: (byte)AdditionalTypes.Defence.MagicalIncreased)[0];
                    staticBoostCategory4 += GetAttackerBenefitingBuffs(type: CardType.Defence,
                        subtype: (byte)AdditionalTypes.Defence.MagicalDecreased)[0];
                    break;
            }

            if (ServerManager.RandomNumber() < chanceIncreased[0]) boostCategory1 += chanceIncreased[1] / 100D;

            if (ServerManager.RandomNumber() < -chanceDecreased[0]) boostCategory1 -= chanceDecreased[1] / 100D;

            #endregion

            #region Element Dependant

            switch (realAttacker.Element)
            {
                case 1:
                    defender.FireResistance += GetDefenderBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
                    defender.FireResistance += GetAttackerBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.AllDecreased)[0];
                    defender.FireResistance += GetDefenderBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.FireIncreased)[0];
                    defender.FireResistance += GetAttackerBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.FireDecreased)[0];
                    defender.FireResistance += GetDefenderBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.AllIncreased)[0];
                    defender.FireResistance += GetAttackerBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.AllDecreased)[0];
                    defender.FireResistance += GetDefenderBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.FireIncreased)[0];
                    defender.FireResistance += GetAttackerBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.FireDecreased)[0];
                    if ((attacker.EntityType == EntityType.Player || attacker.EntityType == EntityType.Mate)
                        && (defender.EntityType == EntityType.Player || defender.EntityType == EntityType.Mate))
                    {
                        defender.FireResistance -=
                            GetShellWeaponEffectValue(effectType: ShellWeaponEffectType
                                .ReducesEnemyFireResistanceInPvp);
                        defender.FireResistance -=
                            GetShellWeaponEffectValue(effectType: ShellWeaponEffectType
                                .ReducesEnemyAllResistancesInPvp);
                    }

                    defender.FireResistance +=
                        GetShellArmorEffectValue(effectType: ShellArmorEffectType.IncreasedFireResistence);
                    defender.FireResistance +=
                        GetShellArmorEffectValue(effectType: ShellArmorEffectType.IncreasedAllResistence);
                    staticBoostCategory5 +=
                        GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.IncreasedFireProperties);
                    boostCategory5 += GetAttackerBenefitingBuffs(type: CardType.IncreaseDamage,
                        subtype: (byte)AdditionalTypes.IncreaseDamage.FireIncreased)[0] / 100D;
                    staticBoostCategory5 += GetAttackerBenefitingBuffs(type: CardType.Element,
                        subtype: (byte)AdditionalTypes.Element.FireIncreased)[0];
                    staticBoostCategory5 += GetDefenderBenefitingBuffs(type: CardType.Element,
                        subtype: (byte)AdditionalTypes.Element.FireDecreased)[0];
                    break;

                case 2:
                    defender.WaterResistance += GetDefenderBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
                    defender.WaterResistance += GetAttackerBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.AllDecreased)[0];
                    defender.WaterResistance += GetDefenderBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.WaterIncreased)[0];
                    defender.WaterResistance += GetAttackerBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.WaterDecreased)[0];
                    defender.WaterResistance += GetDefenderBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.AllIncreased)[0];
                    defender.WaterResistance += GetAttackerBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.AllDecreased)[0];
                    defender.WaterResistance += GetDefenderBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.WaterIncreased)[0];
                    defender.WaterResistance += GetAttackerBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.WaterDecreased)[0];
                    if ((attacker.EntityType == EntityType.Player || attacker.EntityType == EntityType.Mate)
                        && (defender.EntityType == EntityType.Player || defender.EntityType == EntityType.Mate))
                    {
                        defender.FireResistance -=
                            GetShellWeaponEffectValue(
                                effectType: ShellWeaponEffectType.ReducesEnemyWaterResistanceInPvp);
                        defender.FireResistance -=
                            GetShellWeaponEffectValue(effectType: ShellWeaponEffectType
                                .ReducesEnemyAllResistancesInPvp);
                    }

                    defender.FireResistance +=
                        GetShellArmorEffectValue(effectType: ShellArmorEffectType.IncreasedWaterResistence);
                    defender.FireResistance +=
                        GetShellArmorEffectValue(effectType: ShellArmorEffectType.IncreasedAllResistence);
                    staticBoostCategory5 +=
                        GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.IncreasedWaterProperties);
                    boostCategory5 += GetAttackerBenefitingBuffs(type: CardType.IncreaseDamage,
                        subtype: (byte)AdditionalTypes.IncreaseDamage.WaterIncreased)[0] / 100D;
                    staticBoostCategory5 += GetAttackerBenefitingBuffs(type: CardType.Element,
                        subtype: (byte)AdditionalTypes.Element.WaterIncreased)[0];
                    staticBoostCategory5 += GetDefenderBenefitingBuffs(type: CardType.Element,
                        subtype: (byte)AdditionalTypes.Element.WaterDecreased)[0];
                    break;

                case 3:
                    defender.LightResistance += GetDefenderBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
                    defender.LightResistance += GetAttackerBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.AllDecreased)[0];
                    defender.LightResistance += GetDefenderBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.LightIncreased)[0];
                    defender.LightResistance += GetAttackerBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.LightDecreased)[0];
                    defender.LightResistance += GetDefenderBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.AllIncreased)[0];
                    defender.LightResistance += GetAttackerBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.AllDecreased)[0];
                    defender.LightResistance += GetDefenderBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.LightIncreased)[0];
                    defender.LightResistance += GetAttackerBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.LightDecreased)[0];
                    if ((attacker.EntityType == EntityType.Player || attacker.EntityType == EntityType.Mate)
                        && (defender.EntityType == EntityType.Player || defender.EntityType == EntityType.Mate))
                    {
                        defender.FireResistance -=
                            GetShellWeaponEffectValue(
                                effectType: ShellWeaponEffectType.ReducesEnemyLightResistanceInPvp);
                        defender.FireResistance -=
                            GetShellWeaponEffectValue(effectType: ShellWeaponEffectType
                                .ReducesEnemyAllResistancesInPvp);
                    }

                    defender.FireResistance +=
                        GetShellArmorEffectValue(effectType: ShellArmorEffectType.IncreasedLightResistence);
                    defender.FireResistance +=
                        GetShellArmorEffectValue(effectType: ShellArmorEffectType.IncreasedAllResistence);
                    staticBoostCategory5 +=
                        GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.IncreasedLightProperties);
                    boostCategory5 += GetAttackerBenefitingBuffs(type: CardType.IncreaseDamage,
                        subtype: (byte)AdditionalTypes.IncreaseDamage.LightIncreased)[0] / 100D;
                    staticBoostCategory5 += GetAttackerBenefitingBuffs(type: CardType.Element,
                        subtype: (byte)AdditionalTypes.Element.LightIncreased)[0];
                    staticBoostCategory5 += GetDefenderBenefitingBuffs(type: CardType.Element,
                        subtype: (byte)AdditionalTypes.Element.Light5Decreased)[0];
                    break;

                case 4:
                    defender.ShadowResistance += GetDefenderBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
                    defender.ShadowResistance += GetAttackerBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.AllDecreased)[0];
                    defender.ShadowResistance += GetDefenderBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.DarkIncreased)[0];
                    defender.ShadowResistance += GetAttackerBenefitingBuffs(type: CardType.ElementResistance,
                        subtype: (byte)AdditionalTypes.ElementResistance.DarkDecreased)[0];
                    defender.ShadowResistance += GetDefenderBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.AllIncreased)[0];
                    defender.ShadowResistance += GetAttackerBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.AllDecreased)[0];
                    defender.ShadowResistance += GetDefenderBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.DarkIncreased)[0];
                    defender.ShadowResistance += GetAttackerBenefitingBuffs(type: CardType.EnemyElementResistance,
                        subtype: (byte)AdditionalTypes.EnemyElementResistance.DarkDecreased)[0];
                    if ((attacker.EntityType == EntityType.Player || attacker.EntityType == EntityType.Mate)
                        && (defender.EntityType == EntityType.Player || defender.EntityType == EntityType.Mate))
                    {
                        defender.FireResistance -=
                            GetShellWeaponEffectValue(effectType: ShellWeaponEffectType
                                .ReducesEnemyDarkResistanceInPvp);
                        defender.FireResistance -=
                            GetShellWeaponEffectValue(effectType: ShellWeaponEffectType
                                .ReducesEnemyAllResistancesInPvp);
                    }

                    defender.FireResistance +=
                        GetShellArmorEffectValue(effectType: ShellArmorEffectType.IncreasedDarkResistence);
                    defender.FireResistance +=
                        GetShellArmorEffectValue(effectType: ShellArmorEffectType.IncreasedAllResistence);
                    staticBoostCategory5 +=
                        GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.IncreasedDarkProperties);

                    boostCategory5 += GetAttackerBenefitingBuffs(type: CardType.IncreaseDamage,
                        subtype: (byte)AdditionalTypes.IncreaseDamage.DarkIncreased)[0] / 100D;

                    var darkElementDamageIncreaseChance = GetDefenderBenefitingBuffs(type: CardType.DarkCloneSummon,
                        subtype: (byte)AdditionalTypes.DarkCloneSummon.DarkElementDamageIncreaseChance);

                    if (ServerManager.RandomNumber() < darkElementDamageIncreaseChance[0])
                        boostCategory5 += darkElementDamageIncreaseChance[1] / 100D;

                    staticBoostCategory5 += GetAttackerBenefitingBuffs(type: CardType.Element,
                        subtype: (byte)AdditionalTypes.Element.DarkIncreased)[0];
                    staticBoostCategory5 += GetDefenderBenefitingBuffs(type: CardType.Element,
                        subtype: (byte)AdditionalTypes.Element.DarkDecreased)[0];
                    break;
            }

            #endregion

            #endregion

            #region Attack Type Related Variables

            switch (attacker.AttackType)
            {
                case AttackType.Melee:
                    defender.Defense = defender.MeleeDefense;
                    defender.ArmorDefense = defender.ArmorMeleeDefense;
                    defender.Dodge = defender.MeleeDefenseDodge;
                    staticBoostCategory3 += GetAttackerBenefitingBuffs(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MeleeAttacksIncreased)[0];
                    if (GetDefenderBenefitingBuffs(type: CardType.Target,
                            subtype: (byte)AdditionalTypes.Target.MeleeHitRateIncreased)
                        [0] is int MeleeHitRateIncreased)
                        if (MeleeHitRateIncreased != 0)
                            hitrate += MeleeHitRateIncreased;
                    break;

                case AttackType.Range:
                    defender.Defense = defender.RangeDefense;
                    defender.ArmorDefense = defender.ArmorRangeDefense;
                    defender.Dodge = defender.RangeDefenseDodge;
                    staticBoostCategory3 += GetAttackerBenefitingBuffs(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.RangedAttacksIncreased)[0];
                    if (GetDefenderBenefitingBuffs(type: CardType.Target,
                        subtype: (byte)AdditionalTypes.Target.RangedHitRateIncreased)[0] is int RangedHitRateIncreased)
                        if (RangedHitRateIncreased != 0)
                            hitrate += RangedHitRateIncreased;
                    break;

                case AttackType.Magical:
                    defender.Defense = defender.MagicalDefense;
                    defender.ArmorDefense = defender.ArmorMagicalDefense;
                    staticBoostCategory3 += GetAttackerBenefitingBuffs(type: CardType.AttackPower,
                        subtype: (byte)AdditionalTypes.AttackPower.MagicalAttacksIncreased)[0];
                    if (GetDefenderBenefitingBuffs(type: CardType.Target,
                            subtype: (byte)AdditionalTypes.Target.MagicalConcentrationIncreased)[0] is int
                        MagicalConcentrationIncreased)
                        if (MagicalConcentrationIncreased != 0)
                            hitrate += MagicalConcentrationIncreased;
                    break;
            }

            #endregion

            #region Attack Type Attack Disabled

            var AttackDisabled = false;

            switch (attacker.AttackType)
            {
                case AttackType.Melee:
                    if (attacker.HasBuff(type: CardType.SpecialAttack,
                        subtype: (byte)AdditionalTypes.SpecialAttack.MeleeDisabled))
                        AttackDisabled = true;
                    break;

                case AttackType.Range:
                    if (attacker.HasBuff(type: CardType.SpecialAttack,
                        subtype: (byte)AdditionalTypes.SpecialAttack.RangedDisabled))
                        AttackDisabled = true;
                    break;

                case AttackType.Magical:
                    if (attacker.HasBuff(type: CardType.SpecialAttack,
                        subtype: (byte)AdditionalTypes.SpecialAttack.MagicDisabled))
                        AttackDisabled = true;
                    break;
            }

            if (AttackDisabled)
            {
                hitMode = 2;

                if (skill != null && attacker.Character != null)
                    Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: o =>
                    {
                        var ski = attacker.Character.GetSkills()?.Find(match: s =>
                            s.Skill?.CastId == skill.CastId && s.Skill?.UpgradeSkill == 0);

                        if (ski?.Skill != null)
                        {
                            ski.LastUse = DateTime.Now.AddMilliseconds(value: ski.Skill.Cooldown * 100 * -1);
                            attacker.Character.Session?.SendPacket(
                                packet: StaticPacketHelper.SkillReset(castId: skill.CastId));
                        }
                    });
                return 0;
            }

            #endregion

            #region Too Near Range Attack Penalty (boostCategory2)

            if (!attacker.HasBuff(type: CardType.GuarantedDodgeRangedAttack,
                subtype: (byte)AdditionalTypes.GuarantedDodgeRangedAttack.NoPenatly))
                if (attacker.AttackType == AttackType.Range && Map.GetDistance(
                    p: new MapCell { X = attacker.PositionX, Y = attacker.PositionY },
                    q: new MapCell { X = defender.PositionX, Y = defender.PositionY }) < 4)
                    boostCategory2 -= 0.3;

            if (attacker.AttackType == AttackType.Range && attacker.HasBuff(type: CardType.GuarantedDodgeRangedAttack,
                subtype: (byte)AdditionalTypes.GuarantedDodgeRangedAttack.DistanceDamageIncreasing))
            {
                double distance = Map.GetDistance(
                    p: new MapCell { X = attacker.PositionX, Y = attacker.PositionY },
                    q: new MapCell { X = defender.PositionX, Y = defender.PositionY });

                boostCategory2 += distance * 0.015;
            }

            #endregion

            #region Morale and Dodge

            defender.Dodge += GetDefenderBenefitingBuffs(type: CardType.DodgeAndDefencePercent,
                                  subtype: (byte)AdditionalTypes.DodgeAndDefencePercent.DodgeIncreased)[0]
                              - GetDefenderBenefitingBuffs(type: CardType.DodgeAndDefencePercent,
                                  subtype: (byte)AdditionalTypes.DodgeAndDefencePercent.DodgeDecreased)[0];

            double chance = 0;
            if (attacker.AttackType != AttackType.Magical)
            {
                if (GetAttackerBenefitingBuffs(type: CardType.Target,
                        subtype: (byte)AdditionalTypes.Target.AllHitRateIncreased)[0] is
                    int AllHitRateIncreased)
                    if (AllHitRateIncreased != 0)
                        hitrate += AllHitRateIncreased;

                double multiplier = defender.Dodge / (hitrate > 1 ? hitrate : 1);

                if (multiplier > 5) multiplier = 5;

                chance = -0.25 * Math.Pow(x: multiplier, y: 3) - 0.57 * Math.Pow(x: multiplier, y: 2) +
                         25.3 * multiplier
                         - 1.41;
                if (chance <= 1) chance = 1;

                if (GetAttackerBenefitingBuffs(type: CardType.GuarantedDodgeRangedAttack,
                        subtype: (byte)AdditionalTypes.GuarantedDodgeRangedAttack.AttackHitChance)[0] is int
                    AttackHitChance)
                    if (AttackHitChance != 0 && chance > 100 - AttackHitChance)
                        chance = 100 - AttackHitChance;

                if (GetDefenderBenefitingBuffs(type: CardType.GuarantedDodgeRangedAttack,
                        subtype: (byte)AdditionalTypes.GuarantedDodgeRangedAttack.AttackHitChance)[0] is int
                    AttackHitChanceNegated)
                    if (AttackHitChanceNegated != 0 && chance < 100 - AttackHitChanceNegated)
                        chance = 100 - AttackHitChanceNegated;

                if (attacker.HasBuff(type: CardType.GuarantedDodgeRangedAttack,
                    subtype: (byte)AdditionalTypes.GuarantedDodgeRangedAttack.AttackHitChance100)) chance = 0;
            }

            var bonus = 0;
            if ((attacker.EntityType == EntityType.Player || attacker.EntityType == EntityType.Mate)
                && (defender.EntityType == EntityType.Player || defender.EntityType == EntityType.Mate))
            {
                switch (attacker.AttackType)
                {
                    case AttackType.Melee:
                        bonus += GetShellArmorEffectValue(effectType: ShellArmorEffectType.CloseDefenceDodgeInPvp);
                        break;

                    case AttackType.Range:
                        bonus += GetShellArmorEffectValue(effectType: ShellArmorEffectType.DistanceDefenceDodgeInPvp);
                        break;

                    case AttackType.Magical:
                        bonus += GetShellArmorEffectValue(effectType: ShellArmorEffectType.IgnoreMagicDamage);
                        break;
                }

                bonus += GetShellArmorEffectValue(effectType: ShellArmorEffectType.DodgeAllAttacksInPvp);
            }

            if (!defender.Invincible && ServerManager.RandomNumber() - bonus < chance)
            {
                if (attacker.Character != null)
                    if (attacker.Character.SkillComboCount > 0)
                    {
                        attacker.Character.SkillComboCount = 0;
                        attacker.Character.Session.SendPackets(packets: attacker.Character.GenerateQuicklist());
                        attacker.Character.Session.SendPacket(packet: "ms_c 1");
                    }

                hitMode = 4;
                return 0;
            }

            #endregion

            #region Base Damage

            var baseDamage = ServerManager.RandomNumber(
                min: attacker.DamageMinimum < attacker.DamageMaximum ? attacker.DamageMinimum : attacker.DamageMaximum,
                max: attacker.DamageMaximum + 1);
            var weaponDamage =
                ServerManager.RandomNumber(
                    min: attacker.WeaponDamageMinimum < attacker.WeaponDamageMaximum
                        ? attacker.WeaponDamageMinimum
                        : attacker.WeaponDamageMaximum, max: attacker.WeaponDamageMaximum + 1);

            // Adventurer Boost
            if (attacker.Character?.Class == ClassType.Adventurer && attacker.Character?.Level <= 20)
                baseDamage *= 300 / 100;

            #region Attack Level Calculation

            var atklvlfix = GetDefenderBenefitingBuffs(type: CardType.CalculatingLevel,
                subtype: (byte)AdditionalTypes.CalculatingLevel.CalculatedAttackLevel);
            var deflvlfix = GetAttackerBenefitingBuffs(type: CardType.CalculatingLevel,
                subtype: (byte)AdditionalTypes.CalculatingLevel.CalculatedDefenceLevel);

            if (atklvlfix[3] != 0) attacker.AttackUpgrade = (short)atklvlfix[0];

            if (deflvlfix[3] != 0) attacker.DefenseUpgrade = (short)deflvlfix[0];

            attacker.AttackUpgrade -= defender.DefenseUpgrade;

            if (attacker.AttackUpgrade < -10)
                attacker.AttackUpgrade = -10;
            else if (attacker.AttackUpgrade > ServerManager.Instance.Configuration.MaxUpgrade)
                attacker.AttackUpgrade = ServerManager.Instance.Configuration.MaxUpgrade;

            if (attacker.Mate?.MateType == MateType.Pet)
                switch (attacker.AttackUpgrade)
                {
                    case 0:
                        baseDamage += 0;
                        break;

                    case 1:
                        baseDamage += (int)(baseDamage * 0.1);
                        break;

                    case 2:
                        baseDamage += (int)(baseDamage * 0.15);
                        break;

                    case 3:
                        baseDamage += (int)(baseDamage * 0.22);
                        break;

                    case 4:
                        baseDamage += (int)(baseDamage * 0.32);
                        break;

                    case 5:
                        baseDamage += (int)(baseDamage * 0.43);
                        break;

                    case 6:
                        baseDamage += (int)(baseDamage * 0.54);
                        break;

                    case 7:
                        baseDamage += (int)(baseDamage * 0.65);
                        break;

                    case 8:
                        baseDamage += (int)(baseDamage * 0.9);
                        break;

                    case 9:
                        baseDamage += (int)(baseDamage * 1.2);
                        break;

                    case 10:
                        baseDamage += baseDamage * 2;
                        break;

                        //default:
                        //    if (attacker.AttackUpgrade > 0)
                        //    {
                        //        weaponDamage *= attacker.AttackUpgrade / 5;
                        //    }

                        //    break;
                }
            else
                switch (attacker.AttackUpgrade)
                {
                    case 0:
                        weaponDamage += 0;
                        break;

                    case 1:
                        weaponDamage += (int)(weaponDamage * 0.1);
                        break;

                    case 2:
                        weaponDamage += (int)(weaponDamage * 0.15);
                        break;

                    case 3:
                        weaponDamage += (int)(weaponDamage * 0.22);
                        break;

                    case 4:
                        weaponDamage += (int)(weaponDamage * 0.32);
                        break;

                    case 5:
                        weaponDamage += (int)(weaponDamage * 0.43);
                        break;

                    case 6:
                        weaponDamage += (int)(weaponDamage * 0.54);
                        break;

                    case 7:
                        weaponDamage += (int)(weaponDamage * 0.65);
                        break;

                    case 8:
                        weaponDamage += (int)(weaponDamage * 0.9);
                        break;

                    case 9:
                        weaponDamage += (int)(weaponDamage * 1.2);
                        break;

                    case 10:
                        weaponDamage += weaponDamage * 2;
                        break;

                        //default:
                        //    if (attacker.AttackUpgrade > 0)
                        //    {
                        //        weaponDamage *= attacker.AttackUpgrade / 5;
                        //    }

                        //    break;
                }

            #endregion

            baseDamage = (int)((int)((baseDamage + staticBoostCategory3 + weaponDamage + 15) * boostCategory3)
                                * shellBoostCategory3);

            if (attacker.Character?.ChargeValue > 0)
            {
                baseDamage += attacker.Character.ChargeValue;
                attacker.Character.ChargeValue = 0;
                attacker.RemoveBuff(id: 0);
            }

            #endregion

            #region Defense

            switch (attacker.AttackUpgrade)
            {
                //default:
                //    if (attacker.AttackUpgrade < 0)
                //    {
                //        defender.ArmorDefense += defender.ArmorDefense / 5;
                //    }

                //break;

                case -10:
                    defender.ArmorDefense += defender.ArmorDefense * 2;
                    break;

                case -9:
                    defender.ArmorDefense += (int)(defender.ArmorDefense * 1.2);
                    break;

                case -8:
                    defender.ArmorDefense += (int)(defender.ArmorDefense * 0.9);
                    break;

                case -7:
                    defender.ArmorDefense += (int)(defender.ArmorDefense * 0.65);
                    break;

                case -6:
                    defender.ArmorDefense += (int)(defender.ArmorDefense * 0.54);
                    break;

                case -5:
                    defender.ArmorDefense += (int)(defender.ArmorDefense * 0.43);
                    break;

                case -4:
                    defender.ArmorDefense += (int)(defender.ArmorDefense * 0.32);
                    break;

                case -3:
                    defender.ArmorDefense += (int)(defender.ArmorDefense * 0.22);
                    break;

                case -2:
                    defender.ArmorDefense += (int)(defender.ArmorDefense * 0.15);
                    break;

                case -1:
                    defender.ArmorDefense += (int)(defender.ArmorDefense * 0.1);
                    break;

                case 0:
                    defender.ArmorDefense += 0;
                    break;
            }

            var defense =
                (int)((int)((defender.Defense + defender.ArmorDefense + staticBoostCategory4) * boostCategory4) *
                       shellBoostCategory4);

            if (defender.HasBuff(type: CardType.SpecialDefence,
                    subtype: (byte)AdditionalTypes.SpecialDefence.NoDefence)
                || GetAttackerBenefitingBuffs(type: CardType.SpecialDefence,
                    subtype: (byte)AdditionalTypes.SpecialDefence.AllDefenceNullified)[3] != 0
                || GetAttackerBenefitingBuffs(type: CardType.SpecialDefence,
                    subtype: (byte)AdditionalTypes.SpecialDefence.MeleeDefenceNullified)[3] != 0
                && attacker.AttackType.Equals(obj: AttackType.Melee)
                || GetAttackerBenefitingBuffs(type: CardType.SpecialDefence,
                    subtype: (byte)AdditionalTypes.SpecialDefence.RangedDefenceNullified)[3] != 0
                && attacker.AttackType.Equals(obj: AttackType.Range)
                || GetAttackerBenefitingBuffs(type: CardType.SpecialDefence,
                    subtype: (byte)AdditionalTypes.SpecialDefence.MagicDefenceNullified)[3] != 0
                && attacker.AttackType.Equals(obj: AttackType.Magical))
                defense = 0;

            if (GetAttackerBenefitingBuffs(type: CardType.StealBuff,
                    subtype: (byte)AdditionalTypes.StealBuff.IgnoreDefenceChance) is
                int[] IgnoreDefenceChance)
                if (ServerManager.RandomNumber() < IgnoreDefenceChance[0])
                    defense -= (int)(defense * IgnoreDefenceChance[1] / 100D);

            #endregion

            #region Normal Damage

            var normalDamage = (int)((int)((baseDamage + staticBoostCategory2 - defense) * boostCategory2)
                                      * shellBoostCategory2);

            if (normalDamage < 0) normalDamage = 0;

            #endregion

            #region Crit Damage

            attacker.CritChance += GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.CriticalChance);
            attacker.CritChance -= GetShellArmorEffectValue(effectType: ShellArmorEffectType.ReducedCritChanceRecive);
            attacker.CritChance +=
                GetAttackerBenefitingBuffs(type: CardType.Critical,
                    subtype: (byte)AdditionalTypes.Critical.InflictingIncreased)[0];
            attacker.CritChance +=
                GetDefenderBenefitingBuffs(type: CardType.Critical,
                    subtype: (byte)AdditionalTypes.Critical.ReceivingIncreased)[0];

            attacker.CritRate += GetShellWeaponEffectValue(effectType: ShellWeaponEffectType.CriticalDamage);
            attacker.CritRate +=
                GetAttackerBenefitingBuffs(type: CardType.Critical,
                    subtype: (byte)AdditionalTypes.Critical.DamageIncreased)[0];
            attacker.CritRate += GetDefenderBenefitingBuffs(type: CardType.Critical,
                subtype: (byte)AdditionalTypes.Critical.DamageFromCriticalIncreased)[0];

            if (defender.CellonOptions != null)
                attacker.CritRate -= defender.CellonOptions.Where(predicate: s => s.Type == CellonOptionType.CritReduce)
                    .Sum(selector: s => s.Value);

            if (GetDefenderBenefitingBuffs(type: CardType.StealBuff,
                    subtype: (byte)AdditionalTypes.StealBuff.ReduceCriticalReceivedChance) is int[]
                ReduceCriticalReceivedChance)
                if (ServerManager.RandomNumber() < ReduceCriticalReceivedChance[0])
                    attacker.CritRate -= (int)(attacker.CritRate * ReduceCriticalReceivedChance[1] / 100D);

            if (defender.GetBuff(type: CardType.SpecialCritical,
                subtype: (byte)AdditionalTypes.SpecialCritical.ReceivingChancePercent)[0] is int Rate)
                if (Rate < 0) // If > 0 is benefit defender buff
                    if (attacker.CritChance < -Rate)
                        attacker.CritChance = -Rate;

            if (defender.HasBuff(type: CardType.SpecialCritical,
                subtype: (byte)AdditionalTypes.SpecialCritical.AlwaysReceives))
                attacker.CritChance = 100;

            if (defender.HasBuff(type: CardType.SpecialCritical,
                subtype: (byte)AdditionalTypes.SpecialCritical.NeverReceives))
                attacker.CritChance = 0;

            if (skill?.SkillVNum == 1124 &&
                GetAttackerBenefitingBuffs(type: CardType.SniperAttack,
                        subtype: (byte)AdditionalTypes.SniperAttack.ReceiveCriticalFromSniper)[0] is int
                    ReceiveCriticalFromSniper)
                if (ReceiveCriticalFromSniper > 0)
                    attacker.CritChance = ReceiveCriticalFromSniper;

            if (skill?.SkillVNum == 1248
                || ServerManager.RandomNumber() < attacker.CritChance && attacker.AttackType != AttackType.Magical &&
                !attacker.HasBuff(type: CardType.SpecialCritical,
                    subtype: (byte)AdditionalTypes.SpecialCritical.NeverInflict))
            {
                var multiplier = attacker.CritRate / 100D;

                if (multiplier > 3)
                {
#pragma warning disable CS1030 // #warning: "Disabled Critical Rate limit"
#warning Disabled Critical Rate limit
                    //  multiplier = 3;
                }
#pragma warning restore CS1030 // #warning: "Disabled Critical Rate limit"

                normalDamage += (int)(normalDamage * multiplier);

                if (GetDefenderBenefitingBuffs(type: CardType.VulcanoElementBuff,
                    subtype: (byte)AdditionalTypes.VulcanoElementBuff.CriticalDefence)[0] is int CriticalDefence)
                    if (CriticalDefence > 0 && normalDamage > CriticalDefence)
                        normalDamage = CriticalDefence;

                hitMode = 3;
            }

            #endregion

            #region Fairy Damage

            var fairyDamage = (int)((baseDamage + 100) * realAttacker.ElementRate / 100D);

            #endregion

            #region Elemental Damage Advantage

            double elementalBoost = 0;

            switch (realAttacker.Element)
            {
                case 0:
                    break;

                case 1:
                    defender.Resistance = defender.FireResistance;
                    switch (defender.Element)
                    {
                        case 0:
                            elementalBoost = 1.3; // Damage vs no element
                            break;

                        case 1:
                            elementalBoost = 1; // Damage vs fire
                            break;

                        case 2:
                            elementalBoost = 2; // Damage vs water
                            break;

                        case 3:
                            elementalBoost = 1; // Damage vs light
                            break;

                        case 4:
                            elementalBoost = 1.5; // Damage vs darkness
                            break;
                    }

                    break;

                case 2:
                    defender.Resistance = defender.WaterResistance;
                    switch (defender.Element)
                    {
                        case 0:
                            elementalBoost = 1.3;
                            break;

                        case 1:
                            elementalBoost = 2;
                            break;

                        case 2:
                            elementalBoost = 1;
                            break;

                        case 3:
                            elementalBoost = 1.5;
                            break;

                        case 4:
                            elementalBoost = 1;
                            break;
                    }

                    break;

                case 3:
                    defender.Resistance = defender.LightResistance;
                    switch (defender.Element)
                    {
                        case 0:
                            elementalBoost = 1.3;
                            break;

                        case 1:
                            elementalBoost = 1.5;
                            break;

                        case 2:
                        case 3:
                            elementalBoost = 1;
                            break;

                        case 4:
                            elementalBoost = 3;
                            break;
                    }

                    break;

                case 4:
                    defender.Resistance = defender.ShadowResistance;
                    switch (defender.Element)
                    {
                        case 0:
                            elementalBoost = 1.3;
                            break;

                        case 1:
                            elementalBoost = 1;
                            break;

                        case 2:
                            elementalBoost = 1.5;
                            break;

                        case 3:
                            elementalBoost = 3;
                            break;

                        case 4:
                            elementalBoost = 1;
                            break;
                    }

                    break;
            }

            if ( /*skill?.Element == 0 || */skill?.Element != 0 && skill?.Element != realAttacker.Element &&
                                            realAttacker.EntityType == EntityType.Player)
            {
                //elementalBoost = 0;
            }

            #endregion

            #region Elemental Damage

            var elementalDamage =
                (int)((int)((int)((int)((staticBoostCategory5 + fairyDamage) * elementalBoost)
                                     * (1 - defender.Resistance / 100D)) * boostCategory5) * shellBoostCategory5);

            if (elementalDamage < 0) elementalDamage = 0;

            #endregion

            #region Total Damage

            if (!percentDamage)
            {
                totalDamage =
                    (int)((int)((normalDamage + elementalDamage + attacker.Morale + staticBoostCategory1)
                                  * boostCategory1) * shellBoostCategory1);

                if ((attacker.EntityType == EntityType.Player || attacker.EntityType == EntityType.Mate)
                    && (defender.EntityType == EntityType.Player || defender.EntityType == EntityType.Mate))
                    totalDamage /= 2;

                if (defender.EntityType == EntityType.Monster || defender.EntityType == EntityType.Npc)
                {
                    //totalDamage -= GetMonsterDamageBonus(defender.Level);
                }

                if (totalDamage < 5 && boostCategory1 > 0 && shellBoostCategory1 > 0)
                    totalDamage = ServerManager.RandomNumber(min: 1, max: 6);

                if (attacker.EntityType == EntityType.Monster || attacker.EntityType == EntityType.Npc)
                    if (totalDamage < GetMonsterDamageBonus(level: attacker.Level) && boostCategory1 > 0 &&
                        shellBoostCategory1 > 0)
                        totalDamage = GetMonsterDamageBonus(level: attacker.Level);

                if (realAttacker != attacker) totalDamage /= 2;
            }

            if (totalDamage <= 0) totalDamage = 1;

            #endregion

            #region Onyx Wings

            var onyxBuff = GetAttackerBenefitingBuffs(type: CardType.StealBuff,
                subtype: (byte)AdditionalTypes.StealBuff.ChanceSummonOnyxDragon);
            if (onyxBuff[0] > ServerManager.RandomNumber()) onyxWings = true;

            #endregion

            if (defender.Character != null && defender.HasBuff(type: CardType.NoDefeatAndNoDamage,
                subtype: (byte)AdditionalTypes.NoDefeatAndNoDamage.TransferAttackPower))
            {
                if (!percentDamage)
                {
                    defender.Character.RemoveBuffByBCardTypeSubType(bcardTypes: new List<KeyValuePair<byte, byte>>
                    {
                        new KeyValuePair<byte, byte>(key: (byte) CardType.NoDefeatAndNoDamage,
                            value: (byte) AdditionalTypes.NoDefeatAndNoDamage.TransferAttackPower)
                    });
                    defender.Character.ChargeValue = totalDamage;
                    if (defender.Character.ChargeValue > 7000) defender.Character.ChargeValue = 7000;
                    defender.AddBuff(indicator: new Buff.Buff(id: 0, level: defender.Level), sender: defender);
                }

                hitMode = 0;
                return 0;
            }

            #region AbsorptionAndPowerSkill

            var addDamageToHp = defender.GetBuff(type: CardType.AbsorptionAndPowerSkill,
                subtype: (byte)AdditionalTypes.AbsorptionAndPowerSkill.AddDamageToHp);

            if (addDamageToHp[0] > 0)
            {
                var damageToHp = (int)(totalDamage / 100D * addDamageToHp[0]);

                if (defender.Hp + damageToHp > defender.HpMax) damageToHp = defender.HpMax - defender.Hp;

                if (damageToHp > 0)
                {
                    defender.MapInstance?.Broadcast(packet: defender.GenerateRc(characterHealth: damageToHp));

                    defender.Hp = Math.Min(val1: defender.Hp + damageToHp, val2: defender.HpMax);

                    if (defender.Character != null)
                        defender.Character.Session?.SendPacket(packet: defender.Character.GenerateStat());
                }

                hitMode = 0;
                return 0;
            }

            #endregion

            if (defender.GetBuff(type: CardType.SecondSpCard,
                    subtype: (byte)AdditionalTypes.SecondSpCard.HitAttacker) is int[]
                CounterDebuff)
                if (ServerManager.RandomNumber() < CounterDebuff[0])
                    realAttacker.AddBuff(indicator: new Buff.Buff(id: (short)CounterDebuff[1], level: defender.Level),
                        sender: defender);

            #region ReflectMaximumDamageFrom

            if (defender.GetBuff(type: CardType.TauntSkill,
                    subtype: (byte)AdditionalTypes.TauntSkill.ReflectMaximumDamageFrom) is int
                [] ReflectsMaximumDamageFrom)
                if (ReflectsMaximumDamageFrom[0] < 0)
                {
                    var maxReflectDamage = -ReflectsMaximumDamageFrom[0];

                    var reflectedDamage = Math.Min(val1: totalDamage, val2: maxReflectDamage);
                    totalDamage -= reflectedDamage;

                    if (!percentDamage)
                    {
                        reflectedDamage =
                            realAttacker.GetDamage(damage: reflectedDamage, damager: defender, dontKill: true);

                        defender.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: realAttacker.UserType,
                            callerId: realAttacker.MapEntityId, secondaryType: (byte)realAttacker.UserType,
                            targetId: realAttacker.MapEntityId,
                            skillVNum: -1, cooldown: 0, attackAnimation: 0, skillEffect: 0, x: 0, y: 0,
                            isAlive: realAttacker.Hp > 0,
                            health: (int)(realAttacker.Hp / realAttacker.HPLoad() * 100), damage: reflectedDamage,
                            hitmode: 0, skillType: 1));

                        defender.Character?.Session?.SendPacket(packet: defender.Character.GenerateStat());
                    }
                }

            #endregion

            #region ReflectMaximumReceivedDamage

            if (defender.GetBuff(type: CardType.DamageConvertingSkill,
                    subtype: (byte)AdditionalTypes.DamageConvertingSkill.ReflectMaximumReceivedDamage) is int[]
                ReflectMaximumReceivedDamage)
                if (ReflectMaximumReceivedDamage[0] > 0)
                {
                    var maxReflectDamage = ReflectMaximumReceivedDamage[0];

                    var reflectedDamage = Math.Min(val1: totalDamage, val2: maxReflectDamage);
                    totalDamage -= reflectedDamage;

                    if (!percentDamage)
                    {
                        reflectedDamage =
                            realAttacker.GetDamage(damage: reflectedDamage, damager: defender, dontKill: true);

                        defender.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: realAttacker.UserType,
                            callerId: realAttacker.MapEntityId, secondaryType: (byte)realAttacker.UserType,
                            targetId: realAttacker.MapEntityId,
                            skillVNum: -1, cooldown: 0, attackAnimation: 0, skillEffect: 0, x: 0, y: 0,
                            isAlive: realAttacker.Hp > 0,
                            health: (int)(realAttacker.Hp / realAttacker.HPLoad() * 100), damage: reflectedDamage,
                            hitmode: 0, skillType: 1));

                        defender.Character?.Session?.SendPacket(packet: defender.Character.GenerateStat());
                    }
                }

            #endregion

            if (defender.Buffs.FirstOrDefault(predicate: s => s.Card.BCards.Any(predicate: b =>
                    b.Type.Equals(obj: (byte)CardType.DamageConvertingSkill) &&
                    b.SubType.Equals(obj: (byte)AdditionalTypes.DamageConvertingSkill.TransferInflictedDamage / 10)))
                ?.Sender is BattleEntity TransferInflictedDamageSender)
                if (defender.GetBuff(type: CardType.DamageConvertingSkill,
                        subtype: (byte)AdditionalTypes.DamageConvertingSkill.TransferInflictedDamage) is int[]
                    TransferInflictedDamage)
                    if (TransferInflictedDamage[0] > 0)
                    {
                        var transferedDamage = (int)(totalDamage * TransferInflictedDamage[0] / 100d);
                        totalDamage -= transferedDamage;
                        TransferInflictedDamageSender.GetDamage(damage: transferedDamage, damager: defender,
                            dontKill: true);
                        if (TransferInflictedDamageSender.Hp - transferedDamage <= 0)
                            transferedDamage = TransferInflictedDamageSender.Hp - 1;
                        defender.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: realAttacker.UserType,
                            callerId: realAttacker.MapEntityId,
                            secondaryType: (byte)TransferInflictedDamageSender.UserType,
                            targetId: TransferInflictedDamageSender.MapEntityId,
                            skillVNum: skill?.SkillVNum ?? 0, cooldown: skill?.Cooldown ?? 0,
                            attackAnimation: 0,
                            skillEffect: skill?.Effect ?? attacker.Mate?.Monster.BasicSkill ??
                            attacker.MapMonster?.Monster.BasicSkill ??
                            attacker.MapNpc?.Npc.BasicSkill ?? 0,
                            x: defender.PositionX, y: defender.PositionY,
                            isAlive: TransferInflictedDamageSender.Hp > 0,
                            health: (int)(TransferInflictedDamageSender.Hp / TransferInflictedDamageSender.HPLoad() *
                                           100),
                            damage: transferedDamage,
                            hitmode: 0, skillType: 1));
                        if (TransferInflictedDamageSender.Character != null)
                            TransferInflictedDamageSender.Character.Session.SendPacket(
                                packet: TransferInflictedDamageSender
                                    .Character.GenerateStat());
                    }

            totalDamage = Math.Max(val1: 0, val2: totalDamage);

            // TODO: Find a better way because hardcoded
            // There is no clue about this in DB¿

            // Convert && Corruption

            if (skill?.SkillVNum == 1348 && defender.HasBuff(cardId: 628))
            {
                var bonusDamage = totalDamage / 2;

                if (defender.Character != null)
                {
                    bonusDamage /= 2;

                    defender.GetDamage(damage: bonusDamage, damager: realAttacker, dontKill: true);

                    defender.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: realAttacker.UserType,
                        callerId: realAttacker.MapEntityId, secondaryType: (byte)defender.UserType,
                        targetId: defender.MapEntityId,
                        skillVNum: skill.SkillVNum, cooldown: skill.Cooldown, attackAnimation: 0,
                        skillEffect: skill.Effect, x: defender.PositionX, y: defender.PositionY,
                        isAlive: defender.Hp > 0, health: (int)(defender.Hp / defender.HPLoad() * 100),
                        damage: bonusDamage, hitmode: 0, skillType: 1));

                    defender.Character.Session.SendPacket(packet: defender.Character.GenerateStat());
                }
                else
                {
                    defender.GetDamage(damage: bonusDamage, damager: realAttacker, dontKill: true);

                    defender.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: realAttacker.UserType,
                        callerId: realAttacker.MapEntityId, secondaryType: (byte)defender.UserType,
                        targetId: defender.MapEntityId,
                        skillVNum: skill.SkillVNum, cooldown: skill.Cooldown, attackAnimation: 0,
                        skillEffect: skill.Effect, x: defender.PositionX, y: defender.PositionY,
                        isAlive: defender.Hp > 0, health: (int)(defender.Hp / defender.HPLoad() * 100),
                        damage: bonusDamage, hitmode: 0, skillType: 1));
                }

                defender.RemoveBuff(id: 628);
            }

            // Spirit Splitter && Mark of Death

            else if (skill?.SkillVNum == 1178 && defender.HasBuff(cardId: 597))
            {
                totalDamage *= 2;

                defender.RemoveBuff(id: 597);
            }

            // Holy Explosion && Illuminating Powder

            else if (skill?.SkillVNum == 1326 && defender.HasBuff(cardId: 619))
            {
                defender.GetDamage(damage: totalDamage, damager: realAttacker, dontKill: true);

                defender.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: realAttacker.UserType,
                    callerId: realAttacker.MapEntityId, secondaryType: (byte)defender.UserType,
                    targetId: defender.MapEntityId,
                    skillVNum: skill.SkillVNum, cooldown: skill.Cooldown, attackAnimation: 0, skillEffect: skill.Effect,
                    x: defender.PositionX, y: defender.PositionY,
                    isAlive: defender.Hp > 0, health: (int)(defender.Hp / defender.HPLoad() * 100),
                    damage: totalDamage, hitmode: 0, skillType: 1));

                defender.RemoveBuff(id: 619);
            }

            return totalDamage;
        }

        static int[] GetBuff(byte level, List<Buff.Buff> buffs, List<BCard> bcards, CardType type,
            byte subtype, BuffType btype, ref int count, bool castTypeNotZero = false)
        {
            var value1 = 0;
            var value2 = 0;
            var value3 = 0;

            IEnumerable<BCard> cards;

            if (bcards != null && btype.Equals(obj: BuffType.Good))
            {
                cards = subtype % 10 == 1
                    ? bcards.ToList().Where(predicate: s =>
                        (!castTypeNotZero || s.CastType != 0) && s.Type.Equals(obj: (byte)type) &&
                        s.SubType.Equals(obj: (byte)(subtype / 10)) && s.FirstData >= 0)
                    : bcards.ToList().Where(predicate: s =>
                        (!castTypeNotZero || s.CastType != 0) && s.Type.Equals(obj: (byte)type) &&
                        s.SubType.Equals(obj: (byte)(subtype / 10))
                        && (s.FirstData <= 0 || s.ThirdData < 0));

                foreach (var entry in cards.ToList())
                {
                    if (entry.IsLevelScaled)
                    {
                        if (entry.IsLevelDivided)
                            value1 += level / entry.FirstData;
                        else
                            value1 += entry.FirstData * level;
                    }
                    else
                    {
                        value1 += entry.FirstData;
                    }

                    value2 += entry.SecondData;
                    value3 += entry.ThirdData;
                    count++;
                }
            }

            if (buffs != null)
                foreach (var buff in buffs.ToList().Where(predicate: b => b.Card.BuffType.Equals(obj: btype)))
                {
                    cards = subtype % 10 == 1
                        ? buff.Card.BCards.Where(predicate: s =>
                            (!castTypeNotZero || s.CastType != 0) && s.Type.Equals(obj: (byte)type) &&
                            s.SubType.Equals(obj: (byte)(subtype / 10))
                            && (s.CastType != 1 || s.CastType == 1
                                && buff.Start.AddMilliseconds(value: buff.Card.Delay * 100) < DateTime.Now)
                            && s.FirstData >= 0).ToList()
                        : buff.Card.BCards.Where(predicate: s =>
                            (!castTypeNotZero || s.CastType != 0) && s.Type.Equals(obj: (byte)type) &&
                            s.SubType.Equals(obj: (byte)(subtype / 10))
                            && (s.CastType != 1 || s.CastType == 1
                                && buff.Start.AddMilliseconds(value: buff.Card.Delay * 100) < DateTime.Now)
                            && s.FirstData <= 0).ToList();

                    foreach (var entry in cards)
                    {
                        if (entry.IsLevelScaled)
                        {
                            if (entry.IsLevelDivided)
                                value1 += buff.Level / entry.FirstData;
                            else
                                value1 += entry.FirstData * buff.Level;
                        }
                        else
                        {
                            value1 += entry.FirstData;
                        }

                        value2 += entry.SecondData;
                        value3 += entry.ThirdData;
                        count++;
                    }
                }

            return new[] { value1, value2, value3 };
        }

        static int GetMonsterDamageBonus(byte level)
        {
            if (level < 45)
                return 0;
            if (level < 55)
                return level;
            if (level < 60)
                return level * 2;
            if (level < 65)
                return level * 3;
            if (level < 70)
                return level * 4;
            return level * 5;
        }

        #endregion
    }
}