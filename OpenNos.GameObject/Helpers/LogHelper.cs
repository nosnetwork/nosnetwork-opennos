﻿using System;
using OpenNos.DAL;
using OpenNos.Data;

namespace OpenNos.GameObject.Helpers
{
    public class LogHelper
    {
        public void InsertQuestLog(long characterId, string ipAddress, long questId, DateTime lastDaily)
        {
            var log = new QuestLogDto
            {
                CharacterId = characterId,
                IpAddress = ipAddress,
                QuestId = questId,
                LastDaily = lastDaily
            };
            DaoFactory.QuestLogDao.InsertOrUpdate(questLog: ref log);
        }

        #region Singleton

        static LogHelper _instance;

        public static LogHelper Instance
        {
            get => _instance ?? (_instance = new LogHelper());
        }

        #endregion
    }
}