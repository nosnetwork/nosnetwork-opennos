﻿using System.Collections.Generic;
using OpenNos.Domain;

namespace OpenNos.GameObject.Helpers
{
    public class PassiveSkillHelper
    {
        public List<BCard> PassiveSkillToBCards(IEnumerable<CharacterSkill> skills)
        {
            var bcards = new List<BCard>();

            if (skills != null)
                foreach (var skill in skills)
                    switch (skill.Skill.CastId)
                    {
                        case 0:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.AttackPower,
                                SubType = (byte)AdditionalTypes.AttackPower.MeleeAttacksIncreased / 10
                            });
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.Defence,
                                SubType = (byte)AdditionalTypes.Defence.MeleeIncreased / 10
                            });
                            break;
                        case 1:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.Target,
                                SubType = (byte)AdditionalTypes.Target.AllHitRateIncreased / 10
                            });
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.DodgeAndDefencePercent,
                                SubType = (byte)AdditionalTypes.DodgeAndDefencePercent.DodgeIncreased / 10
                            });
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.Defence,
                                SubType = (byte)AdditionalTypes.Defence.RangedIncreased / 10
                            });
                            break;
                        case 2:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.AttackPower,
                                SubType = (byte)AdditionalTypes.AttackPower.MagicalAttacksIncreased / 10
                            });
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.Defence,
                                SubType = (byte)AdditionalTypes.Defence.MagicalIncreased / 10
                            });
                            break;
                        case 4:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.MaxHpmp,
                                SubType = (byte)AdditionalTypes.MaxHpmp.MaximumHpIncreased / 10
                            });
                            break;
                        case 5:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.MaxHpmp,
                                SubType = (byte)AdditionalTypes.MaxHpmp.MaximumMpIncreased / 10
                            });
                            break;
                        case 6:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.AttackPower,
                                SubType = (byte)AdditionalTypes.AttackPower.AllAttacksIncreased / 10
                            });
                            break;
                        case 7:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.Defence,
                                SubType = (byte)AdditionalTypes.Defence.AllIncreased / 10
                            });
                            break;
                        case 8:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.Recovery,
                                SubType = (byte)AdditionalTypes.Recovery.HpRecoveryIncreased / 10
                            });
                            break;
                        case 9:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeSkill,
                                Type = (byte)BCardType.CardType.Recovery,
                                SubType = (byte)AdditionalTypes.Recovery.MpRecoveryIncreased / 10
                            });
                            break;
                        case 19:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.SpecialisationBuffResistance,
                                SubType = (byte)AdditionalTypes.SpecialisationBuffResistance.IncreaseDamageInPvp / 10
                            });
                            break;
                        case 20:
                            bcards.Add(item: new BCard
                            {
                                FirstData = -skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.SpecialisationBuffResistance,
                                SubType = (byte)AdditionalTypes.SpecialisationBuffResistance.DecreaseDamageInPvp / 10
                            });
                            break;
                        case 21:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.AttackPower,
                                SubType = (byte)AdditionalTypes.AttackPower.MeleeAttacksIncreased / 10
                            });
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.Defence,
                                SubType = (byte)AdditionalTypes.Defence.MeleeIncreased / 10
                            });
                            break;
                        case 22:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.Target,
                                SubType = (byte)AdditionalTypes.Target.AllHitRateIncreased / 10
                            });
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.DodgeAndDefencePercent,
                                SubType = (byte)AdditionalTypes.DodgeAndDefencePercent.DodgeIncreased / 10
                            });
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.Defence,
                                SubType = (byte)AdditionalTypes.Defence.RangedIncreased / 10
                            });
                            break;
                        case 23:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.AttackPower,
                                SubType = (byte)AdditionalTypes.AttackPower.MagicalAttacksIncreased / 10
                            });
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.Defence,
                                SubType = (byte)AdditionalTypes.Defence.MagicalIncreased / 10
                            });
                            break;
                        case 24:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.MaxHpmp,
                                SubType = (byte)AdditionalTypes.MaxHpmp.MaximumHpIncreased / 10
                            });
                            break;
                        case 25:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.MaxHpmp,
                                SubType = (byte)AdditionalTypes.MaxHpmp.MaximumMpIncreased / 10
                            });
                            break;
                        case 26:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.Defence,
                                SubType = (byte)AdditionalTypes.Defence.AllIncreased / 10
                            });
                            break;
                        case 27:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.AttackPower,
                                SubType = (byte)AdditionalTypes.AttackPower.AllAttacksIncreased / 10
                            });
                            break;
                        case 28:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.ElementResistance,
                                SubType = (byte)AdditionalTypes.ElementResistance.AllIncreased / 10
                            });
                            break;
                        case 29:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.Item,
                                SubType = (byte)AdditionalTypes.Item.ExpIncreased / 10
                            });
                            break;
                        case 30:
                            bcards.Add(item: new BCard
                            {
                                FirstData = skill.Skill.UpgradeType,
                                Type = (byte)BCardType.CardType.Item,
                                SubType = (byte)AdditionalTypes.Item.IncreaseEarnedGold / 10
                            });
                            break;
                    }

            return bcards;
        }

        #region Singleton

        static PassiveSkillHelper _instance;

        public static PassiveSkillHelper Instance
        {
            get => _instance ?? (_instance = new PassiveSkillHelper());
        }

        #endregion
    }
}