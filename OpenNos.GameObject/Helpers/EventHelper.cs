﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Event.ACT4;
using OpenNos.GameObject.Event.ARENA;
using OpenNos.GameObject.Event.GAMES;
using OpenNos.GameObject.Event.ICEBREAKER;
using OpenNos.GameObject.Event.INSTANTBATTLE;
using OpenNos.GameObject.Event.LOD;
using OpenNos.GameObject.Event.MINILANDREFRESH;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;
using OpenNos.PathFinder;

namespace OpenNos.GameObject.Helpers
{
    public class EventHelper
    {
        #region Members

        static EventHelper _instance;

        #endregion

        #region Properties

        public static EventHelper Instance
        {
            get => _instance ?? (_instance = new EventHelper());
        }

        #endregion

        #region Methods

        public static int CalculateComboPoint(int n)
        {
            var a = 4;
            var b = 7;
            for (var i = 0; i < n; i++)
            {
                var temp = a;
                a = b;
                b = temp + b;
            }

            return a;
        }

        public static void GenerateEvent(EventType type, int lvlBracket = -1)
        {
            if (type == EventType.Icebreaker && lvlBracket < 0) return;

            if (!ServerManager.Instance.StartedEvents.Contains(item: type))
                Task.Factory.StartNew(action: () =>
                {
                    ServerManager.Instance.StartedEvents.Add(item: type);
                    switch (type)
                    {
                        case EventType.Rankingrefresh:
                            ServerManager.Instance.RefreshRanking();
                            ServerManager.Instance.StartedEvents.Remove(item: type);
                            break;

                        case EventType.Lod:
                            if (ServerManager.Instance.ChannelId != 51)
                                Lod.GenerateLod();
                            else
                                ServerManager.Instance.StartedEvents.Remove(item: type);

                            break;

                        case EventType.Minilandrefreshevent:
                            MinilandRefresh.GenerateMinilandEvent();
                            break;

                        case EventType.Instantbattle:
                            if (ServerManager.Instance.ChannelId != 51)
                                InstantBattle.GenerateInstantBattle();
                            else
                                ServerManager.Instance.StartedEvents.Remove(item: type);

                            break;

                        case EventType.Meteoritegame:
                            if (ServerManager.Instance.ChannelId != 51)
                                MeteoriteGame.GenerateMeteoriteGame();
                            else
                                ServerManager.Instance.StartedEvents.Remove(item: type);

                            break;

                        case EventType.Act4Ship:
                            Act4Ship.GenerateAct4Ship(faction: 1);
                            Act4Ship.GenerateAct4Ship(faction: 2);
                            break;

                        case EventType.Talentarena:
                            if (ServerManager.Instance.ChannelId != 51)
                                ArenaEvent.GenerateTalentArena();
                            else
                                ServerManager.Instance.StartedEvents.Remove(item: type);

                            break;

                        case EventType.Caligor:
                            if (ServerManager.Instance.ChannelId == 51)
                                CaligorRaid.Run();
                            else
                                ServerManager.Instance.StartedEvents.Remove(item: type);

                            break;

                        case EventType.Icebreaker:
                            if (ServerManager.Instance.ChannelId != 51)
                                IceBreaker.GenerateIceBreaker(bracket: lvlBracket);
                            else
                                ServerManager.Instance.StartedEvents.Remove(item: type);

                            break;
                    }
                });
        }

        public static TimeSpan GetMilisecondsBeforeTime(TimeSpan time)
        {
            var now = TimeSpan.Parse(s: DateTime.Now.ToString(format: "HH:mm"));
            var timeLeftUntilFirstRun = time - now;
            if (timeLeftUntilFirstRun.TotalHours < 0)
                timeLeftUntilFirstRun += new TimeSpan(hours: 24, minutes: 0, seconds: 0);
            return timeLeftUntilFirstRun;
        }

        public void RunEvent(EventContainer evt, ClientSession session = null, MapMonster monster = null,
            MapNpc npc = null)
        {
            if (evt != null)
            {
                if (session != null)
                {
                    evt.MapInstance = session.CurrentMapInstance;
                    switch (evt.EventActionType)
                    {
                        #region EventForUser

                        case EventActionType.Npcdialog:
                            session.SendPacket(packet: session.Character.GenerateNpcDialog(value: (int)evt.Parameter));
                            break;

                        case EventActionType.Sendpacket:
                            session.SendPacket(packet: (string)evt.Parameter);
                            break;

                            #endregion
                    }
                }

                if (evt.MapInstance != null)
                    switch (evt.EventActionType)
                    {
                        #region EventForUser

                        case EventActionType.Npcdialog:
                        case EventActionType.Sendpacket:
                            if (session == null)
                                evt.MapInstance.Sessions.ToList().ForEach(action: e => RunEvent(evt: evt, session: e));
                            break;

                        #endregion

                        #region MapInstanceEvent

                        case EventActionType.Registerevent:
                            var even = (Tuple<string, List<EventContainer>>)evt.Parameter;
                            switch (even.Item1)
                            {
                                case "OnCharacterDiscoveringMap":
                                    even.Item2.ForEach(action: s =>
                                        evt.MapInstance.OnCharacterDiscoveringMapEvents.Add(
                                            item: new Tuple<EventContainer, List<long>>(item1: s,
                                                item2: new List<long>())));
                                    break;

                                case "OnMoveOnMap":
                                    evt.MapInstance.OnMoveOnMapEvents.AddRange(value: even.Item2);
                                    break;

                                case "OnMapClean":
                                    evt.MapInstance.OnMapClean.AddRange(collection: even.Item2);
                                    break;

                                case "OnLockerOpen":
                                    evt.MapInstance.UnlockEvents.AddRange(collection: even.Item2);
                                    break;
                            }

                            break;

                        case EventActionType.Registerwave:
                            evt.MapInstance.WaveEvents.Add(item: (EventWave)evt.Parameter);
                            break;

                        case EventActionType.Setareaentry:
                            var even2 = (ZoneEvent)evt.Parameter;
                            evt.MapInstance.OnAreaEntryEvents.Add(value: even2);
                            break;

                        case EventActionType.Removemonsterlocker:
                            var evt2 = (EventContainer)evt.Parameter;
                            if (evt2 == null) throw new ArgumentNullException(paramName: nameof(evt2));
                            if (evt.MapInstance.InstanceBag.MonsterLocker.Current > 0)
                                evt.MapInstance.InstanceBag.MonsterLocker.Current--;
                            if (evt.MapInstance.InstanceBag.MonsterLocker.Current == 0 &&
                                evt.MapInstance.InstanceBag.ButtonLocker.Current == 0)
                            {
                                var unlockEventsCopy = evt.MapInstance.UnlockEvents.ToList();
                                unlockEventsCopy.ForEach(action: e => RunEvent(evt: e));
                                evt.MapInstance.UnlockEvents.RemoveAll(match: s =>
                                    s != null && unlockEventsCopy.Contains(item: s));
                            }

                            break;

                        case EventActionType.Removebuttonlocker:
                            if (evt.MapInstance.InstanceBag.ButtonLocker.Current > 0)
                                evt.MapInstance.InstanceBag.ButtonLocker.Current--;
                            if (evt.MapInstance.InstanceBag.MonsterLocker.Current == 0 &&
                                evt.MapInstance.InstanceBag.ButtonLocker.Current == 0)
                            {
                                var unlockEventsCopy = evt.MapInstance.UnlockEvents.ToList();
                                unlockEventsCopy.ForEach(action: e => RunEvent(evt: e));
                                evt.MapInstance.UnlockEvents.RemoveAll(match: s =>
                                    s != null && unlockEventsCopy.Contains(item: s));
                            }

                            break;

                        case EventActionType.Effect:
                            {
                                var effectTuple = (Tuple<short, int>)evt.Parameter;

                                var effectId = effectTuple.Item1;
                                var delay = effectTuple.Item2;

                                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: delay)).Subscribe(onNext: obs =>
                                {
                                    if (monster != null)
                                    {
                                        monster.LastEffect = DateTime.Now;
                                        evt.MapInstance.Broadcast(packet: StaticPacketHelper.GenerateEff(
                                            effectType: UserType.Monster,
                                            callerId: monster.MapMonsterId, effectId: effectId));
                                    }
                                    else
                                    {
                                        evt.MapInstance.Sessions.Where(predicate: s => s?.Character != null).ToList()
                                            .ForEach(action: s =>
                                                s.SendPacket(packet: StaticPacketHelper.GenerateEff(
                                                    effectType: UserType.Player,
                                                    callerId: s.Character.CharacterId, effectId: effectId)));
                                    }
                                });
                            }
                            break;

                        case EventActionType.Controlemonsterinrange:
                            if (monster != null)
                            {
                                var evnt = (Tuple<short, byte, List<EventContainer>>)evt.Parameter;
                                var mapMonsters =
                                    evt.MapInstance.GetMonsterInRangeList(mapX: monster.MapX, mapY: monster.MapY,
                                        distance: evnt.Item2);
                                if (evnt.Item1 != 0) mapMonsters.RemoveAll(match: s => s.MonsterVNum != evnt.Item1);
                                mapMonsters.ForEach(action: s =>
                                    evnt.Item3.ForEach(action: e => RunEvent(evt: e, monster: s)));
                            }

                            break;

                        case EventActionType.Ontarget:
                            if (monster.MoveEvent?.InZone(positionX: monster.MapX, positionY: monster.MapY) == true)
                            {
                                monster.MoveEvent = null;
                                monster.Path = new List<Node>();
                                ((List<EventContainer>)evt.Parameter).ForEach(action: s =>
                                   RunEvent(evt: s, monster: monster));
                            }

                            break;

                        case EventActionType.Move:
                            var evt4 = (ZoneEvent)evt.Parameter;
                            if (monster != null)
                            {
                                monster.MoveEvent = evt4;
                                monster.Path = BestFirstSearch.FindPathJagged(
                                    start: new Node { X = monster.MapX, Y = monster.MapY },
                                    end: new Node { X = evt4.X, Y = evt4.Y },
                                    Grid: evt.MapInstance?.Map.JaggedGrid);
                                monster.RunToX = evt4.X;
                                monster.RunToY = evt4.Y;
                            }
                            else if (npc != null)
                            {
                                //npc.MoveEvent = evt4;
                                npc.Path = BestFirstSearch.FindPathJagged(start: new Node { X = npc.MapX, Y = npc.MapY },
                                    end: new Node { X = evt4.X, Y = evt4.Y }, Grid: evt.MapInstance?.Map.JaggedGrid);
                                npc.RunToX = evt4.X;
                                npc.RunToY = evt4.Y;
                            }

                            break;

                        case EventActionType.Startact4Raidwaves:
                            IDisposable spawnsDisposable = null;
                            spawnsDisposable = Observable.Interval(period: TimeSpan.FromSeconds(value: 60)).Subscribe(
                                onNext: s =>
                                {
                                    var count = evt.MapInstance.Sessions.Count();

                                    if (count <= 0)
                                    {
                                        if (spawnsDisposable != null) spawnsDisposable.Dispose();
                                        return;
                                    }

                                    if (count > 5) count = 5;
                                    var mobWave = new List<MonsterToSummon>();
                                    for (var i = 0; i < count; i++)
                                        switch (evt.MapInstance.MapInstanceType)
                                        {
                                            case MapInstanceType.Act4Morcos:
                                                mobWave.Add(item: new MonsterToSummon(vnum: 561,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 561,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 561,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 562,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 562,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 562,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 851,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: false));
                                                break;

                                            case MapInstanceType.Act4Hatus:
                                                mobWave.Add(item: new MonsterToSummon(vnum: 574,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 574,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 575,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 575,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 576,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 576,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                break;

                                            case MapInstanceType.Act4Calvina:
                                                mobWave.Add(item: new MonsterToSummon(vnum: 770,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 770,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 770,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 771,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 771,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 771,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                break;

                                            case MapInstanceType.Act4Berios:
                                                mobWave.Add(item: new MonsterToSummon(vnum: 780,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 781,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 782,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 782,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 783,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                mobWave.Add(item: new MonsterToSummon(vnum: 783,
                                                    spawnCell: evt.MapInstance.Map.GetRandomPosition(), target: null,
                                                    move: true));
                                                break;
                                        }

                                    evt.MapInstance.SummonMonsters(monstersToSummon: mobWave);
                                });
                            break;

                        case EventActionType.Setmonsterlockers:
                            evt.MapInstance.InstanceBag.MonsterLocker.Current = Convert.ToByte(value: evt.Parameter);
                            evt.MapInstance.InstanceBag.MonsterLocker.Initial = Convert.ToByte(value: evt.Parameter);
                            break;

                        case EventActionType.Setbuttonlockers:
                            evt.MapInstance.InstanceBag.ButtonLocker.Current = Convert.ToByte(value: evt.Parameter);
                            evt.MapInstance.InstanceBag.ButtonLocker.Initial = Convert.ToByte(value: evt.Parameter);
                            break;

                        case EventActionType.Scriptend:
                            switch (evt.MapInstance.MapInstanceType)
                            {
                                case MapInstanceType.TimeSpaceInstance:
                                    evt.MapInstance.InstanceBag.Clock.StopClock();
                                    evt.MapInstance.Clock.StopClock();
                                    evt.MapInstance.InstanceBag.EndState = (byte)evt.Parameter;
                                    var client = evt.MapInstance.Sessions.ToList()
                                        .Where(predicate: s => s.Character?.Timespace != null).FirstOrDefault();
                                    if (client != null && client.Character?.Timespace != null &&
                                        evt.MapInstance.InstanceBag.EndState != 10)
                                    {
#pragma warning disable CS1030 // #warning: "Check EndState and monsters to kill"
#warning Check EndState and monsters to kill
                                        ServerManager.GetBaseMapInstanceIdByMapId(mapId: client.Character.MapId);
                                        var si = ServerManager.Instance.TimeSpaces.FirstOrDefault(predicate: s =>
                                            s.Id == client.Character.Timespace.Id);
                                        if (si == null) return;
                                        byte penalty = 0;
                                        if (penalty > (client.Character.Level - si.LevelMinimum) * 2)
                                        {
                                            penalty = penalty > 100 ? (byte)100 : penalty;
                                            client.SendPacket(packet: client.Character.GenerateSay(
                                                message: string.Format(
                                                    format: Language.Instance.GetMessageFromKey(key: "TS_PENALTY"),
                                                    arg0: penalty), type: 10));
                                        }

                                        var point = evt.MapInstance.InstanceBag.Point * (100 - penalty) / 100;
                                        var perfection = "";
                                        perfection += evt.MapInstance.InstanceBag.MonstersKilled >= si.MonsterAmount
                                            ? 1
                                            : 0;
                                        perfection += evt.MapInstance.InstanceBag.NpcsKilled == 0 ? 1 : 0;
                                        perfection += evt.MapInstance.InstanceBag.RoomsVisited >= si.RoomAmount ? 1 : 0;
                                        foreach (var mapInstance in
                                            client.Character.Timespace._mapInstanceDictionary.Values)
                                            mapInstance.Broadcast(
                                                packet:
                                                $"score  {evt.MapInstance.InstanceBag.EndState} {point} 27 47 18 {si.DrawItems?.Count ?? 0} {evt.MapInstance.InstanceBag.MonstersKilled} {si.NpcAmount - evt.MapInstance.InstanceBag.NpcsKilled} {evt.MapInstance.InstanceBag.RoomsVisited} {perfection} 1 1");

                                        if (evt.MapInstance.InstanceBag.EndState == 5)
                                        {
                                            if (client.Character.Inventory.GetAllItems().FirstOrDefault(predicate: s =>
                                                s.Item.ItemType == ItemType.Special && s.Item.Effect == 140 &&
                                                s.Item.EffectValue == si.Id) is ItemInstance tsStone)
                                                client.Character.Inventory.RemoveItemFromInventory(id: tsStone.Id);
                                            var tsmembers = new ClientSession[40];
                                            client.Character.Timespace._mapInstanceDictionary
                                                .SelectMany(selector: s => s.Value?.Sessions).ToList()
                                                .CopyTo(array: tsmembers);
                                            foreach (var targetSession in tsmembers)
                                                if (targetSession != null)
                                                    targetSession.Character.IncrementQuests(type: QuestType.TimesSpace,
                                                        firstData: si.QuestTimeSpaceId);
                                        }

                                        var ClientTimeSpace = client.Character.Timespace;
                                        Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 30)).Subscribe(
                                            onNext: o =>
                                            {
                                                var tsmembers = new ClientSession[40];
                                                ClientTimeSpace._mapInstanceDictionary
                                                    .SelectMany(selector: s => s.Value?.Sessions)
                                                    .ToList().CopyTo(array: tsmembers);
                                                foreach (var targetSession in tsmembers)
                                                    if (targetSession != null)
                                                        if (targetSession.Character.Hp <= 0)
                                                        {
                                                            targetSession.Character.Hp = 1;
                                                            targetSession.Character.Mp = 1;
                                                        }

                                                ClientTimeSpace._mapInstanceDictionary.Values.ToList()
                                                    .ForEach(action: m => m.Dispose());
                                            });
                                    }

                                    break;

                                case MapInstanceType.RaidInstance:
                                    {
                                        evt.MapInstance.InstanceBag.EndState = (byte)evt.Parameter;

                                        var owner = evt.MapInstance.Sessions.FirstOrDefault(predicate: s =>
                                                s.Character.Group?.Raid?.InstanceBag.CreatorId == s.Character.CharacterId)
                                            ?.Character;
                                        if (owner == null)
                                            owner = evt.MapInstance.Sessions
                                                .FirstOrDefault(predicate: s => s.Character.Group?.Raid != null)?.Character;

                                        var group = owner?.Group;

                                        if (group?.Raid == null) break;

                                        var teamSize = group.Raid.InstanceBag.Lives;

                                        if (evt.MapInstance.InstanceBag.EndState == 1 &&
                                            evt.MapInstance.Monsters.Any(predicate: s => s.IsBoss))
                                        {
                                            Parallel.ForEach(
                                                source: group.Sessions.Where(predicate: s =>
                                                    s?.Character?.MapInstance?.Monsters.Any(predicate: e => e.IsBoss) ==
                                                    true), body: s =>
                                                {
                                                    foreach (var gift in group.Raid.GiftItems)
                                                    {
                                                        byte rare = 0;

                                                        for (int i = ItemHelper.RareRate.Length - 1,
                                                            t = ServerManager.RandomNumber();
                                                            i >= 0;
                                                            i--)
                                                            if (t < ItemHelper.RareRate[i])
                                                            {
                                                                rare = (byte)i;
                                                                break;
                                                            }

                                                        if (rare < 1) rare = 1;

                                                        if (s.Character.Level >= group.Raid.LevelMinimum)
                                                            if (gift.MinTeamSize == 0 && gift.MaxTeamSize == 0 ||
                                                                teamSize >= gift.MinTeamSize &&
                                                                teamSize <= gift.MaxTeamSize)
                                                                s.Character.GiftAdd(itemVNum: gift.VNum,
                                                                    amount: gift.Amount, rare: rare, upgrade: 0,
                                                                    design: gift.Design, forceRandom: gift.IsRandomRare);
                                                    }

                                                    s.Character.GetReputation(amount: group.Raid.Reputation);

                                                    if (s.Character.GenerateFamilyXp(FXP: group.Raid.FamExp,
                                                        InstanceId: group.Raid.Id))
                                                        s.SendPacket(packet: s.Character.GenerateSay(
                                                            message: string.Format(
                                                                format: Language.Instance.GetMessageFromKey(key: "WIN_FXP"),
                                                                arg0: group.Raid.FamExp), type: 10));

                                                    s.Character.IncrementQuests(type: QuestType.WinRaid,
                                                        firstData: group.Raid.Id);
                                                });

                                            foreach (var mapMonster in evt.MapInstance.Monsters)
                                                if (mapMonster != null)
                                                {
                                                    mapMonster.SetDeathStatement();
                                                    evt.MapInstance.Broadcast(packet: StaticPacketHelper.Out(
                                                        type: UserType.Monster,
                                                        callerId: mapMonster.MapMonsterId));
                                                    evt.MapInstance.RemoveMonster(monsterToRemove: mapMonster);
                                                }

                                            Logger.LogUserEvent(logEvent: "RAID_SUCCESS", caller: owner.Name,
                                                data: $"RaidId: {group.GroupId}");

                                            ServerManager.Instance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                                                message: string.Format(
                                                    format: Language.Instance.GetMessageFromKey(key: "RAID_SUCCEED"),
                                                    arg0: group.Raid.Label, arg1: owner.Name), type: 0));

                                            Parallel.ForEach(source: group.Sessions.GetAllItems(), body: s =>
                                            {
                                                if (s.Account != null && s.Character?.Group?.Raid != null)
                                                    s.Character.GeneralLogs?.Add(value: new GeneralLogDto
                                                    {
                                                        AccountId = s.Account.AccountId,
                                                        CharacterId = s.Character.CharacterId,
                                                        IpAddress = s.IpAddress,
                                                        LogData = $"{s.Character.Group.Raid.Id}",
                                                        LogType = "InstanceEntry",
                                                        Timestamp = DateTime.Now
                                                    });
                                            });
                                        }

                                        var dueTime =
                                            TimeSpan.FromSeconds(value: evt.MapInstance.InstanceBag.EndState == 1 ? 15 : 0);

                                        evt.MapInstance.Broadcast(
                                            packet: Character.GenerateRaidBf(type: evt.MapInstance.InstanceBag.EndState));

                                        Observable.Timer(dueTime: dueTime).Subscribe(onNext: o =>
                                        {
                                            evt.MapInstance.Sessions.Where(predicate: s =>
                                                    s.Character != null &&
                                                    s.Character.HasBuff(type: BCardType.CardType.FrozenDebuff,
                                                        subtype: (byte)AdditionalTypes.FrozenDebuff.EternalIce))
                                                .Select(selector: s => s.Character).ToList().ForEach(action: c =>
                                                {
                                                    c.RemoveBuff(cardId: 569);
                                                });

                                            var groupMembers = new ClientSession[group.SessionCount];
                                            group.Sessions.CopyTo(grpmembers: groupMembers);

                                            foreach (var groupMember in groupMembers)
                                            {
                                                if (groupMember.Character.Hp < 1)
                                                {
                                                    groupMember.Character.Hp = 1;
                                                    groupMember.Character.Mp = 1;
                                                }

                                                groupMember.SendPacket(
                                                    packet: groupMember.Character.GenerateRaid(Type: 1, exit: true));
                                                groupMember.SendPacket(
                                                    packet: groupMember.Character.GenerateRaid(Type: 2, exit: true));
                                                group.LeaveGroup(session: groupMember);
                                            }

                                            ServerManager.Instance.GroupList.RemoveAll(match: s =>
                                                s.GroupId == group.GroupId);
                                            ServerManager.Instance.ThreadSafeGroupList.Remove(key: group.GroupId);

                                            group.Raid.Dispose();
                                        });
                                    }
                                    break;


                                case MapInstanceType.Act4Morcos:
                                case MapInstanceType.Act4Hatus:
                                case MapInstanceType.Act4Calvina:
                                case MapInstanceType.Act4Berios:
                                    client = evt.MapInstance.Sessions.FirstOrDefault(predicate: s =>
                                        s.Character?.Family?.Act4RaidBossMap == evt.MapInstance);
                                    if (client != null)
                                    {
                                        var fam = client.Character.Family;
                                        if (fam != null)
                                        {
                                            short destX = 38;
                                            short destY = 179;
                                            short rewardVNum = 882;
                                            switch (evt.MapInstance.MapInstanceType)
                                            {
                                                //Morcos is default
                                                case MapInstanceType.Act4Hatus:
                                                    destX = 18;
                                                    destY = 10;
                                                    rewardVNum = 185;
                                                    break;

                                                case MapInstanceType.Act4Calvina:
                                                    destX = 25;
                                                    destY = 7;
                                                    rewardVNum = 942;
                                                    break;

                                                case MapInstanceType.Act4Berios:
                                                    destX = 16;
                                                    destY = 25;
                                                    rewardVNum = 999;
                                                    break;
                                            }

                                            var count = evt.MapInstance.Sessions.Count(predicate: s =>
                                                s?.Character != null);
                                            foreach (var sess in evt.MapInstance.Sessions)
                                                if (sess?.Character != null)
                                                {
                                                    sess.Character.GiftAdd(itemVNum: rewardVNum, amount: 1,
                                                        forceRandom: true, minRare: 1,
                                                        design: 255);
                                                    if (sess.Character.GenerateFamilyXp(FXP: 10000 / count))
                                                        sess.SendPacket(packet: sess.Character.GenerateSay(
                                                            message: string.Format(
                                                                format: Language.Instance.GetMessageFromKey(
                                                                    key: "WIN_FXP"),
                                                                arg0: 10000 / count), type: 10));
                                                }

                                            evt.MapInstance.Broadcast(packet: "dance 2");

                                            Logger.LogEvent(logEvent: "FAMILYRAID_SUCCESS",
                                                data:
                                                $"[fam.Name]FamilyRaidId: {evt.MapInstance.MapInstanceType.ToString()}");

                                            CommunicationServiceClient.Instance.SendMessageToCharacter(
                                                message: new ScsCharacterMessage
                                                {
                                                    DestinationCharacterId = fam.FamilyId,
                                                    SourceCharacterId = client.Character.CharacterId,
                                                    SourceWorldId = ServerManager.Instance.WorldId,
                                                    Message = UserInterfaceHelper.GenerateMsg(
                                                        message: Language.Instance.GetMessageFromKey(
                                                            key: "FAMILYRAID_SUCCESS"), type: 0),
                                                    Type = MessageType.Family
                                                });
                                            //ServerManager.Instance.Broadcast(UserInterfaceHelper.Instance.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("FAMILYRAID_SUCCESS"), grp?.Raid?.Label, grp.Characters.ElementAt(0).Character.Name), 0));

                                            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 30)).Subscribe(
                                                onNext: o =>
                                                {
                                                    foreach (var targetSession in evt.MapInstance.Sessions.ToArray())
                                                        if (targetSession != null)
                                                        {
                                                            if (targetSession.Character.Hp <= 0)
                                                            {
                                                                targetSession.Character.Hp = 1;
                                                                targetSession.Character.Mp = 1;
                                                            }

                                                            ServerManager.Instance.ChangeMapInstance(
                                                                characterId: targetSession.Character.CharacterId,
                                                                mapInstanceId: fam.Act4Raid.MapInstanceId, mapX: destX,
                                                                mapY: destY);

                                                            targetSession.SendPacket(packet: "dance");
                                                        }

                                                    evt.MapInstance.Dispose();
                                                });

                                            fam.InsertFamilyLog(logtype: FamilyLogType.RaidWon,
                                                raidType: (int)evt.MapInstance.MapInstanceType - 7);
                                        }
                                    }

                                    break;
                                case MapInstanceType.CaligorInstance:

                                    var winningFaction = CaligorRaid.AngelDamage > CaligorRaid.DemonDamage
                                        ? FactionType.Angel
                                        : FactionType.Demon;

                                    foreach (var sess in evt.MapInstance.Sessions)
                                        if (sess?.Character != null)
                                        {
                                            if (CaligorRaid.RemainingTime > 2400)
                                            {
                                                if (sess.Character.Faction == winningFaction)
                                                    sess.Character.GiftAdd(itemVNum: 5960, amount: 1);
                                                else
                                                    sess.Character.GiftAdd(itemVNum: 5961, amount: 1);
                                            }
                                            else
                                            {
                                                if (sess.Character.Faction == winningFaction)
                                                    sess.Character.GiftAdd(itemVNum: 5961, amount: 1);
                                                else
                                                    sess.Character.GiftAdd(itemVNum: 5958, amount: 1);
                                            }

                                            sess.Character.GiftAdd(itemVNum: 5959, amount: 1);
                                            sess.Character.GenerateFamilyXp(FXP: 500);
                                        }

                                    evt.MapInstance.Broadcast(packet: UserInterfaceHelper.GenerateChdm(
                                        maxhp: ServerManager.GetNpcMonster(npcVNum: 2305).MaxHp,
                                        angeldmg: CaligorRaid.AngelDamage,
                                        demondmg: CaligorRaid.DemonDamage, time: CaligorRaid.RemainingTime));
                                    break;
                            }

                            break;

                        case EventActionType.Clock:
                            evt.MapInstance.InstanceBag.Clock.TotalSecondsAmount =
                                Convert.ToInt32(value: evt.Parameter);
                            evt.MapInstance.InstanceBag.Clock.SecondsRemaining = Convert.ToInt32(value: evt.Parameter);
                            break;

                        case EventActionType.Mapclock:
                            evt.MapInstance.Clock.TotalSecondsAmount = Convert.ToInt32(value: evt.Parameter);
                            evt.MapInstance.Clock.SecondsRemaining = Convert.ToInt32(value: evt.Parameter);
                            break;

                        case EventActionType.Startclock:
                            var eve = (Tuple<List<EventContainer>, List<EventContainer>>)evt.Parameter;
                            evt.MapInstance.InstanceBag.Clock.StopEvents = eve.Item1;
                            evt.MapInstance.InstanceBag.Clock.TimeoutEvents = eve.Item2;
                            evt.MapInstance.InstanceBag.Clock.StartClock();
                            evt.MapInstance.Broadcast(packet: evt.MapInstance.InstanceBag.Clock.GetClock());
                            break;

                        case EventActionType.Startmapclock:
                            eve = (Tuple<List<EventContainer>, List<EventContainer>>)evt.Parameter;
                            evt.MapInstance.Clock.StopEvents = eve.Item1;
                            evt.MapInstance.Clock.TimeoutEvents = eve.Item2;
                            evt.MapInstance.Clock.StartClock();
                            evt.MapInstance.Broadcast(packet: evt.MapInstance.Clock.GetClock());
                            break;

                        case EventActionType.Stopclock:
                            evt.MapInstance.InstanceBag.Clock.StopClock();
                            evt.MapInstance.Broadcast(packet: evt.MapInstance.InstanceBag.Clock.GetClock());
                            break;

                        case EventActionType.Stopmapclock:
                            evt.MapInstance.Clock.StopClock();
                            evt.MapInstance.Broadcast(packet: evt.MapInstance.Clock.GetClock());
                            break;

                        case EventActionType.Addclocktime:
                            evt.MapInstance.InstanceBag.Clock.AddTime(seconds: (int)evt.Parameter);
                            evt.MapInstance.Broadcast(packet: evt.MapInstance.InstanceBag.Clock.GetClock());
                            break;

                        case EventActionType.Addmapclocktime:
                            evt.MapInstance.Clock.AddTime(seconds: (int)evt.Parameter);
                            evt.MapInstance.Broadcast(packet: evt.MapInstance.Clock.GetClock());
                            break;

                        case EventActionType.Teleport:
                            var tp = (Tuple<short, short, short, short>)evt.Parameter;
                            var characters = evt.MapInstance
                                .GetCharactersInRange(mapX: tp.Item1, mapY: tp.Item2, distance: 5).ToList();
                            characters.ForEach(action: s =>
                            {
                                s.PositionX = tp.Item3;
                                s.PositionY = tp.Item4;
                                evt.MapInstance?.Broadcast(client: s.Session, content: s.GenerateTp());
                                foreach (var mate in s.Mates.Where(predicate: m => m.IsTeamMember && m.IsAlive))
                                {
                                    mate.PositionX = tp.Item3;
                                    mate.PositionY = tp.Item4;
                                    evt.MapInstance?.Broadcast(client: s.Session, content: mate.GenerateTp());
                                }
                            });
                            break;

                        case EventActionType.Spawnportal:
                            evt.MapInstance.CreatePortal(portal: (Portal)evt.Parameter);
                            break;

                        case EventActionType.Refreshmapitems:
                            evt.MapInstance.MapClear();
                            break;

                        case EventActionType.Stopmapwaves:
                            evt.MapInstance.WaveEvents.Clear();
                            break;

                        case EventActionType.Npcseffectchangestate:
                            evt.MapInstance.Npcs.ForEach(action: s => s.EffectActivated = (bool)evt.Parameter);
                            break;

                        case EventActionType.Changeportaltype:
                            var param = (Tuple<int, PortalType>)evt.Parameter;
                            var portal = evt.MapInstance.Portals.Find(match: s => s.PortalId == param.Item1);
                            if (portal != null)
                            {
                                portal.IsDisabled = true;
                                evt.MapInstance.Broadcast(packet: portal.GenerateGp());
                                portal.IsDisabled = false;

                                portal.Type = (short)param.Item2;
                                if ((PortalType)portal.Type == PortalType.Closed
                                    && (evt.MapInstance.MapInstanceType.Equals(obj: MapInstanceType.Act4Berios)
                                        || evt.MapInstance.MapInstanceType.Equals(obj: MapInstanceType.Act4Calvina)
                                        || evt.MapInstance.MapInstanceType.Equals(obj: MapInstanceType.Act4Hatus)
                                        || evt.MapInstance.MapInstanceType.Equals(obj: MapInstanceType.Act4Morcos)))
                                    portal.IsDisabled = true;
                                evt.MapInstance.Broadcast(packet: portal.GenerateGp());
                            }

                            break;

                        case EventActionType.Changedroprate:
                            break;

                        case EventActionType.Changexprate:
                            evt.MapInstance.XpRate = (int)evt.Parameter;
                            break;

                        case EventActionType.Clearmapmonsters:
                            Parallel.ForEach(
                                source: evt.MapInstance.Monsters.ToList()
                                    .Where(predicate: s => s.Owner?.Character == null && s.Owner?.Mate == null),
                                body: mapMonster =>
                                {
                                    evt.MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                                        callerId: mapMonster.MapMonsterId));
                                    mapMonster.SetDeathStatement();
                                    evt.MapInstance.RemoveMonster(monsterToRemove: mapMonster);
                                });
                            break;

                        case EventActionType.Disposemap:
                            evt.MapInstance.Dispose();
                            break;

                        case EventActionType.Spawnbutton:
                            evt.MapInstance.SpawnButton(parameter: (MapButton)evt.Parameter);
                            break;

                        case EventActionType.Unspawnmonsters:
                            evt.MapInstance.DespawnMonster(monsterVnum: (int)evt.Parameter);
                            break;

                        case EventActionType.Spawnmonster:
                            evt.MapInstance.SummonMonster(summon: (MonsterToSummon)evt.Parameter);
                            break;

                        case EventActionType.Spawnmonsters:
                            evt.MapInstance.SummonMonsters(monstersToSummon: (List<MonsterToSummon>)evt.Parameter);
                            break;

                        case EventActionType.Refreshraidgoal:
                            var cl = evt.MapInstance.Sessions.FirstOrDefault();
                            if (cl?.Character != null)
                            {
                                ServerManager.Instance.Broadcast(client: cl,
                                    content: cl.Character?.Group?.GeneraterRaidmbf(session: cl),
                                    receiver: ReceiverType.Group);
                                ServerManager.Instance.Broadcast(client: cl,
                                    content: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NEW_MISSION"),
                                        type: 0), receiver: ReceiverType.Group);
                            }

                            break;

                        case EventActionType.Spawnnpc:
                            evt.MapInstance.SummonNpc(npcToSummon: (NpcToSummon)evt.Parameter);
                            break;

                        case EventActionType.Spawnnpcs:
                            evt.MapInstance.SummonNpcs(npcsToSummon: (List<NpcToSummon>)evt.Parameter);
                            break;

                        case EventActionType.Dropitems:
                            evt.MapInstance.DropItems(list: (List<Tuple<short, int, short, short>>)evt.Parameter);
                            break;

                        case EventActionType.Throwitems:
                            var parameters = (Tuple<int, short, byte, int, int, short>)evt.Parameter;
                            if (monster != null)
                                parameters = new Tuple<int, short, byte, int, int, short>(item1: monster.MapMonsterId,
                                    item2: parameters.Item2, item3: parameters.Item3, item4: parameters.Item4,
                                    item5: parameters.Item5,
                                    item6: parameters.Item6);
                            evt.MapInstance.ThrowItems(parameter: parameters);
                            break;

                        case EventActionType.Spawnonlastentry:
                            var lastincharacter = evt.MapInstance.Sessions
                                .OrderByDescending(keySelector: s => s.RegisterTime)
                                .FirstOrDefault()?.Character;
                            var summonParameters = new List<MonsterToSummon>();
                            var hornSpawn = new MapCell
                            {
                                X = lastincharacter?.PositionX ?? 154,
                                Y = lastincharacter?.PositionY ?? 140
                            };
                            var hornTarget = lastincharacter?.BattleEntity;
                            summonParameters.Add(item: new MonsterToSummon(vnum: Convert.ToInt16(value: evt.Parameter),
                                spawnCell: hornSpawn,
                                target: hornTarget, move: true));
                            evt.MapInstance.SummonMonsters(monstersToSummon: summonParameters);
                            break;

                        case EventActionType.Removeafter:
                            {
                                Observable.Timer(
                                        dueTime: TimeSpan.FromSeconds(value: Convert.ToInt16(value: evt.Parameter)))
                                    .Subscribe(onNext: o =>
                                    {
                                        if (monster != null)
                                        {
                                            monster.SetDeathStatement();
                                            evt.MapInstance.RemoveMonster(monsterToRemove: monster);
                                            evt.MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                                                callerId: monster.MapMonsterId));
                                        }
                                    });
                            }
                            break;

                        case EventActionType.Removelaurenabuff:
                            {
                                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1))
                                    .Subscribe(onNext: observer =>
                                    {
                                        if (evt.Parameter is BattleEntity battleEntity
                                            && evt.MapInstance?.Monsters != null
                                            && !evt.MapInstance.Monsters.ToList()
                                                .Any(predicate: s => s.MonsterVNum == 2327))
                                            battleEntity.RemoveBuff(id: 475);
                                    });
                            }
                            break;

                            #endregion
                    }
            }
        }

        public void ScheduleEvent(TimeSpan timeSpan, EventContainer evt)
        {
            Observable.Timer(dueTime: timeSpan).Subscribe(onNext: x => RunEvent(evt: evt));
        }

        #endregion
    }
}