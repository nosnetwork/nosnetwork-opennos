﻿using OpenNos.Domain;

namespace OpenNos.GameObject
{
    public class MonsterMapItem : MapItem
    {
        #region Instantiation

        public MonsterMapItem(short x, short y, short itemVNum, int amount = 1, long ownerId = -1,
            bool isQuest = false) : base(x: x, y: y)
        {
            ItemVNum = itemVNum;
            if (amount < 1000) Amount = (short)amount;
            GoldAmount = amount;
            OwnerId = ownerId;
            IsQuest = isQuest;
        }

        #endregion

        #region Properties

        public sealed override short Amount { get; set; }

        public int GoldAmount { get; }

        public sealed override short ItemVNum { get; set; }

        public long? OwnerId { get; }

        #endregion

        #region Methods

        public override ItemInstance GetItemInstance()
        {
            if (_itemInstance == null && OwnerId != null)
                _itemInstance =
                    Inventory.InstantiateItemInstance(vnum: ItemVNum, ownerId: OwnerId.Value, amount: Amount);
            return _itemInstance;
        }

        public void Rarify(ClientSession session)
        {
            var instance = GetItemInstance();
            if (instance?.Item?.Type == InventoryType.Equipment &&
                (instance?.Item?.ItemType == ItemType.Weapon || instance?.Item?.ItemType == ItemType.Armor))
                instance?.RarifyItem(session: session, mode: RarifyMode.Drop, protection: RarifyProtection.None);
        }

        #endregion
    }
}