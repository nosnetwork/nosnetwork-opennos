﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using static OpenNos.Domain.BCardType;

namespace OpenNos.GameObject
{
    public class CharacterSkill : CharacterSkillDto
    {
        #region Members

        short? _firstCastId;

        Skill _skill;

        #endregion

        #region Instantiation

        public CharacterSkill()
        {
            LastUse = DateTime.Now.AddHours(value: -1);
            Hit = 0;
        }

        public CharacterSkill(CharacterSkillDto input) : this()
        {
            CharacterId = input.CharacterId;
            Id = input.Id;
            SkillVNum = input.SkillVNum;
        }

        #endregion

        #region Properties

        public short? FirstCastId
        {
            get => _firstCastId ?? (_firstCastId = Skill.CastId);
            set => _firstCastId = value;
        }

        public short Hit { get; set; }

        public DateTime LastUse { get; set; }

        public Skill Skill
        {
            get => _skill ?? (_skill = ServerManager.GetSkill(skillVNum: SkillVNum));
        }

        #endregion

        #region Methods

        public bool CanBeUsed()
        {
            return Skill != null && LastUse.AddMilliseconds(value: Skill.Cooldown * 100) < DateTime.Now;
        }

        public List<BCard> GetSkillBCards()
        {
            var SkillBCards = new List<BCard>();
            SkillBCards.AddRange(collection: Skill.BCards);
            if (ServerManager.Instance.GetSessionByCharacterId(characterId: CharacterId) is ClientSession Session)
            {
                var skills = Session.Character.GetSkills();

                //Upgrade Skills
                var upgradeSkills = skills.FindAll(match: s => s.Skill?.UpgradeSkill == SkillVNum);
                if (upgradeSkills?.Count > 0)
                {
                    foreach (var upgradeSkill in upgradeSkills)
                        SkillBCards.AddRange(collection: upgradeSkill.Skill.BCards);
                    if (upgradeSkills.OrderByDescending(keySelector: s => s.SkillVNum).FirstOrDefault() is
                        CharacterSkill
                        LastUpgradeSkill)
                        if (LastUpgradeSkill.Skill.BCards.Any(predicate: s => s.Type == 25 && s.SubType == 1))
                            SkillBCards.Where(predicate: s =>
                                    s.Type == 25 && s.SubType == 1 && s.SkillVNum != LastUpgradeSkill.SkillVNum)
                                .ToList()
                                .ForEach(action: s => SkillBCards.Remove(item: s)); // Only buffs of last upgrade skill
                }

                //Passive Skills
                SkillBCards.AddRange(
                    collection: PassiveSkillHelper.Instance.PassiveSkillToBCards(
                        skills: Session.Character.Skills?.Where(predicate: s => s.Skill.SkillType == 0)));

                if (Skill.SkillVNum == 1123)
                    foreach (var ambushBCard in Session.Character.Buff.GetAllItems().SelectMany(selector: s =>
                        s.Card.BCards.Where(predicate: b =>
                            b.Type == (byte)CardType.FearSkill &&
                            b.SubType == (byte)AdditionalTypes.FearSkill.ProduceWhenAmbushe / 10)))
                        SkillBCards.Add(item: ambushBCard);
                else if (Skill.SkillVNum == 1124)
                    foreach (var sniperAttackBCard in Session.Character.Buff.GetAllItems().SelectMany(selector: s =>
                        s.Card.BCards.Where(predicate: b =>
                            b.Type == (byte)CardType.SniperAttack &&
                            b.SubType == (byte)AdditionalTypes.SniperAttack.ChanceCausing / 10)))
                        SkillBCards.Add(item: sniperAttackBCard);
                foreach (var ambushAttackBCard in Session.Character.Buff.GetAllItems().SelectMany(selector: s =>
                    s.Card.BCards.Where(predicate: b =>
                        b.Type == (byte)CardType.SniperAttack &&
                        b.SubType == (byte)AdditionalTypes.SniperAttack.ProduceChance / 10)))
                    SkillBCards.Add(item: ambushAttackBCard);
            }

            return SkillBCards.ToList();
        }

        public int GetSkillRange()
        {
            int skillRange = Skill.Range;
            if (ServerManager.Instance.GetSessionByCharacterId(characterId: CharacterId) is ClientSession Session)
                skillRange += Session.Character.GetBuff(type: CardType.FearSkill,
                    subtype: (byte)AdditionalTypes.FearSkill.AttackRangedIncreased)[0];
            return skillRange;
        }

        public short MpCost()
        {
            var mpCost = Skill.MpCost;
            if (ServerManager.Instance.GetSessionByCharacterId(characterId: CharacterId) is ClientSession Session)
            {
                var skills = Session.Character.GetSkills();

                //Upgrade Skills
                var upgradeSkills = skills.FindAll(match: s => s.Skill?.UpgradeSkill == SkillVNum);
                if (upgradeSkills?.Count > 0)
                    foreach (var upgradeSkill in upgradeSkills)
                        mpCost += upgradeSkill.Skill.MpCost;
            }

            return mpCost;
        }

        public byte TargetRange()
        {
            var targetRange = Skill.TargetRange;

            if (Skill.HitType != 0)
                if (ServerManager.Instance.GetSessionByCharacterId(characterId: CharacterId) is ClientSession Session)
                    Session.Character.Buff.GetAllItems().SelectMany(selector: s => s.Card.BCards).Where(predicate: s =>
                            s.Type == (byte)CardType.FireCannoneerRangeBuff
                            && s.SubType == (byte)AdditionalTypes.FireCannoneerRangeBuff.AoeIncreased / 10).ToList()
                        .ForEach(action: s => targetRange += (byte)s.FirstData);

            return targetRange;
        }

        #endregion
    }
}