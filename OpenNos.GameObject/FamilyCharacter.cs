﻿using OpenNos.DAL;
using OpenNos.Data;

namespace OpenNos.GameObject
{
    public class FamilyCharacter : FamilyCharacterDto
    {
        #region Members

        CharacterDto _character;

        #endregion

        #region Properties

        public CharacterDto Character
        {
            get => _character ?? (_character = DaoFactory.CharacterDao.LoadById(characterId: CharacterId));
        }

        #endregion

        #region Instantiation

        public FamilyCharacter()
        {
        }

        public FamilyCharacter(FamilyCharacterDto input)
        {
            Authority = input.Authority;
            CharacterId = input.CharacterId;
            DailyMessage = input.DailyMessage;
            Experience = input.Experience;
            FamilyCharacterId = input.FamilyCharacterId;
            FamilyId = input.FamilyId;
            Rank = input.Rank;
        }

        #endregion
    }
}