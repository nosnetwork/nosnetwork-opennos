﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;

namespace OpenNos.GameObject
{
    public class Clock
    {
        #region Instantiation

        public Clock(byte type)
        {
            StopEvents = new List<EventContainer>();
            TimeoutEvents = new List<EventContainer>();
            Type = type;
            SecondsRemaining = 1;
            Observable.Interval(period: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: x => tick());
        }

        #endregion

        #region Properties

        public int TotalSecondsAmount { get; set; }

        public int SecondsRemaining { get; set; }

        public bool Enabled { get; private set; }

        public List<EventContainer> StopEvents { get; set; }

        public List<EventContainer> TimeoutEvents { get; set; }

        public byte Type { get; set; }

        #endregion

        #region Methods

        public string GetClock()
        {
            return $"evnt {Type} {(Enabled ? 0 : Type != 3 ? -1 : 1)} {SecondsRemaining} {TotalSecondsAmount}";
        }

        public void StartClock()
        {
            Enabled = true;
        }

        public void StopClock()
        {
            Enabled = false;
            StopEvents.ForEach(action: e => EventHelper.Instance.RunEvent(evt: e));
            StopEvents.RemoveAll(match: s => s != null);
        }

        public void AddTime(int seconds)
        {
            SecondsRemaining += seconds * 10;
            TotalSecondsAmount += seconds * 10;
        }

        void tick()
        {
            if (Enabled)
            {
                if (SecondsRemaining > 0)
                {
                    SecondsRemaining -= 10;
                }
                else
                {
                    TimeoutEvents.ForEach(action: ev => EventHelper.Instance.RunEvent(evt: ev));
                    TimeoutEvents.RemoveAll(match: s => s != null);
                }
            }
        }

        #endregion
    }
}