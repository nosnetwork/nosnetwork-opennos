﻿using System.Collections.Generic;
using System.Linq;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class CharacterQuest : CharacterQuestDto
    {
        #region Members

        Quest _quest;
#pragma warning disable CS0169 // Das Feld "CharacterQuest.qst" wird nie verwendet.
        QuestDto qst;
#pragma warning restore CS0169 // Das Feld "CharacterQuest.qst" wird nie verwendet.

        #endregion

        #region Instantiation

        public CharacterQuest(CharacterQuestDto characterQuest)
        {
            CharacterId = characterQuest.CharacterId;
            QuestId = characterQuest.QuestId;
            FirstObjective = characterQuest.FirstObjective;
            SecondObjective = characterQuest.SecondObjective;
            ThirdObjective = characterQuest.ThirdObjective;
            FourthObjective = characterQuest.FourthObjective;
            FifthObjective = characterQuest.FifthObjective;
            IsMainQuest = characterQuest.IsMainQuest;
        }

        public CharacterQuest(long questId, long characterId)
        {
            QuestId = questId;
            CharacterId = characterId;
        }

        #endregion

        #region Properties

        public Quest Quest
        {
            get => _quest ?? (_quest = ServerManager.Instance.GetQuest(questId: QuestId));
        }

        public bool RewardInWaiting { get; set; }

        public List<QuestRewardDto> QuestRewards { get; set; }

        public short QuestNumber { get; set; }

        #endregion

        #region Methods

        public string GetProgressMessage(byte index, int amount)
        {
            string type = null;
            string objectiveName = null;
            switch ((QuestType)Quest.QuestType)
            {
                case QuestType.Capture1:
                case QuestType.Capture2:
                    type = "capturing";
                    objectiveName = ServerManager.GetNpcMonster(npcVNum: (short)GetObjectiveByIndex(index: index).Data)
                        .Name;
                    break;
                case QuestType.Collect1:
                case QuestType.Collect2:
                case QuestType.Collect3:
                case QuestType.Collect4:
                    objectiveName = ServerManager.GetItem(vnum: (short)GetObjectiveByIndex(index: index).Data).Name;
                    type = "collecting";
                    break;
                case QuestType.Hunt:
                    type = "hunting";
                    objectiveName = ServerManager.GetNpcMonster(npcVNum: (short)GetObjectiveByIndex(index: index).Data)
                        .Name;
                    break;
            }

            if (!string.IsNullOrEmpty(value: type) && !string.IsNullOrEmpty(value: objectiveName) &&
                GetObjectives()[index - 1] < GetObjectiveByIndex(index: index).Objective)
                return
                    $"say 2 {CharacterId} 11 [{objectiveName}] {type}: {GetObjectives()[index - 1] + amount}/{GetObjectiveByIndex(index: index).Objective}";
            return "";
        }

        public string GetInfoPacket(bool sendMsg)
        {
            return
                $"{QuestNumber}.{Quest.InfoId}.{((IsMainQuest || Quest.IsDaily) && Quest.QuestId < 5000 ? Quest.InfoId : 0)}.{Quest.QuestType}.{FirstObjective}.{GetObjectiveByIndex(index: 1)?.Objective ?? 0}.{(RewardInWaiting ? 1 : 0)}.{SecondObjective}.{GetObjectiveByIndex(index: 2)?.Objective ?? 0}.{ThirdObjective}.{GetObjectiveByIndex(index: 3)?.Objective ?? 0}.{FourthObjective}.{GetObjectiveByIndex(index: 4)?.Objective ?? 0}.{FifthObjective}.{GetObjectiveByIndex(index: 5)?.Objective ?? 0}.{(sendMsg ? 1 : 0)}";
        }

        public QuestObjectiveDto GetObjectiveByIndex(byte index)
        {
            return Quest.QuestObjectives.FirstOrDefault(predicate: q => q.ObjectiveIndex.Equals(obj: index));
        }

        public int[] GetObjectives()
        {
            return new[] { FirstObjective, SecondObjective, ThirdObjective, FourthObjective, FifthObjective };
        }

        public void Incerment(byte index, int amount)
        {
            switch (index)
            {
                case 1:
                    FirstObjective += FirstObjective >= GetObjectiveByIndex(index: index)?.Objective ? 0 : amount;
                    break;

                case 2:
                    SecondObjective += SecondObjective >= GetObjectiveByIndex(index: index)?.Objective ? 0 : amount;
                    break;

                case 3:
                    ThirdObjective += ThirdObjective >= GetObjectiveByIndex(index: index)?.Objective ? 0 : amount;
                    break;

                case 4:
                    FourthObjective += FourthObjective >= GetObjectiveByIndex(index: index)?.Objective ? 0 : amount;
                    break;

                case 5:
                    FifthObjective += FifthObjective >= GetObjectiveByIndex(index: index)?.Objective ? 0 : amount;
                    break;
            }
        }

        #endregion
    }
}