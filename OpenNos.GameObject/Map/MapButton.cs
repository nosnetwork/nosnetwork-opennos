﻿using System.Collections.Generic;
using OpenNos.Domain;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;

namespace OpenNos.GameObject
{
    public class MapButton
    {
        #region Instantiation

        public MapButton(int id, short positionX, short positionY, short enabledVNum, short disabledVNum,
            List<EventContainer> disableEvents, List<EventContainer> enableEvents,
            List<EventContainer> firstEnableEvents)
        {
            MapButtonId = id;
            PositionX = positionX;
            PositionY = positionY;
            EnabledVNum = enabledVNum;
            DisabledVNum = disabledVNum;
            DisableEvents = disableEvents;
            EnableEvents = enableEvents;
            FirstEnableEvents = firstEnableEvents;
        }

        #endregion

        #region Properties

        public short DisabledVNum { get; set; }

        public List<EventContainer> DisableEvents { get; set; }

        public short EnabledVNum { get; set; }

        public List<EventContainer> EnableEvents { get; set; }

        public List<EventContainer> FirstEnableEvents { get; set; }

        public int MapButtonId { get; set; }

        public short PositionX { get; set; }

        public short PositionY { get; set; }

        public bool State { get; set; }

        #endregion

        #region Methods

        public string GenerateIn()
        {
            return StaticPacketHelper.In(type: UserType.Object, callerVNum: State ? EnabledVNum : DisabledVNum,
                callerId: MapButtonId,
                mapX: PositionX, mapY: PositionY, direction: 1, currentHp: 0, currentMp: 0, dialog: 0, respawnType: 0,
                isSitting: false, name: "-", invisible: false);
        }

        public void RunAction()
        {
            State = !State;
            if (State)
            {
                EnableEvents.ForEach(action: e => EventHelper.Instance.RunEvent(evt: e));
                FirstEnableEvents.ForEach(action: e => EventHelper.Instance.RunEvent(evt: e));
                FirstEnableEvents.RemoveAll(match: s => s != null);
            }
            else
            {
                DisableEvents.ForEach(action: e => EventHelper.Instance.RunEvent(evt: e));
            }
        }

        #endregion
    }
}