﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Serializing;
using OpenNos.Core.Threading;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.PathFinder;
using static OpenNos.Domain.BCardType;

namespace OpenNos.GameObject
{
    public class MapNpc : MapNpcDto
    {
        public MapNpc(bool invisible)
        {
            Invisible = invisible;
            PveLockObject = new object();
            OnSpawnEvents = new List<EventContainer>();
        }

        public MapNpc(MapNpcDto input, bool invisible) : this(invisible: invisible)
        {
            Dialog = input.Dialog;
            Effect = input.Effect;
            EffectDelay = input.EffectDelay;
            IsDisabled = input.IsDisabled;
            IsMoving = input.IsMoving;
            IsSitting = input.IsSitting;
            MapId = input.MapId;
            MapNpcId = input.MapNpcId;
            MapX = input.MapX;
            MapY = input.MapY;
            Name = input.Name;
            NpcVNum = input.NpcVNum;
            Position = input.Position;
        }

        #region Members

        public NpcMonster Npc;

        int _movetime;

        Random _random;
        MapNpcDto input;

        public MapNpc()
        {
            throw new NotImplementedException();
        }

        public MapNpc(MapNpcDto input)
        {
            this.input = input;
        }

        #endregion

        #region Properties

        public int AliveTime { get; set; }

        public Node[][] BrushFireJagged { get; set; }

        public ThreadSafeSortedList<short, Buff.Buff> Buff
        {
            get => BattleEntity.Buffs;
        }

        public ThreadSafeSortedList<short, IDisposable> BuffObservables
        {
            get => BattleEntity.BuffObservables;
        }

        public double CurrentHp { get; set; }

        public double CurrentMp { get; set; }

        public DateTime Death { get; set; }

        public bool EffectActivated { get; set; }

        public short FirstX { get; set; }

        public short FirstY { get; set; }

        public bool IsAlive { get; set; }

        public bool IsHostile { get; set; }

        public bool IsMate { get; set; }

        public bool IsTsReward { get; set; }

        public bool IsProtected { get; set; }

        public DateTime LastDefence { get; set; }

        public DateTime LastEffect { get; set; }

        public DateTime LastProtectedEffect { get; private set; }

        public DateTime LastSkill { get; private set; }

        public DateTime LastMonsterAggro { get; set; }

        public DateTime LastMove { get; private set; }

        public IDisposable LifeEvent { get; set; }

        public bool IsOut { get; set; }

        public MapInstance MapInstance { get; set; }

        public double MaxHp { get; set; }

        public double MaxMp { get; set; }

        public List<EventContainer> OnDeathEvents
        {
            get => BattleEntity.OnDeathEvents;
        }

        public List<EventContainer> OnSpawnEvents { get; set; }

        public BattleEntity Owner { get; set; }

        public List<Node> Path { get; set; }

        public object PveLockObject { get; set; }

        public List<Recipe> Recipes { get; set; }

        public Shop Shop { get; set; }

        public bool? ShouldRespawn { get; set; }

        public bool Started { get; internal set; }

        public List<NpcMonsterSkill> Skills { get; set; }

        public long Target { get; set; }

        public short RunToX { get; set; }

        public short RunToY { get; set; }

        public List<TeleporterDto> Teleporters { get; set; }
        public bool Invisible { get; }

        #endregion

        #region BattleEntityProperties

        public BattleEntity BattleEntity { get; set; }

        public void AddBuff(Buff.Buff indicator, BattleEntity battleEntity)
        {
            BattleEntity.AddBuff(indicator: indicator, sender: battleEntity);
        }

        public void RemoveBuff(short cardId)
        {
            BattleEntity.RemoveBuff(id: cardId);
        }

        public int[] GetBuff(CardType type, byte subtype)
        {
            return BattleEntity.GetBuff(type: type, subtype: subtype);
        }

        public bool HasBuff(CardType type, byte subtype)
        {
            return BattleEntity.HasBuff(type: type, subtype: subtype);
        }

        public void DisableBuffs(BuffType type, int level = 100)
        {
            BattleEntity.DisableBuffs(type: type, level: level);
        }

        public void DisableBuffs(List<BuffType> types, int level = 100)
        {
            BattleEntity.DisableBuffs(types: types, level: level);
        }

        #endregion

        #region Methods

        public string GenerateSay(string message, int type)
        {
            return $"say 2 {MapNpcId} 2 {message}";
        }

        public string GenerateIn(InRespawnType respawnType = InRespawnType.NoEffect)
        {
            var npcinfo = ServerManager.GetNpcMonster(npcVNum: NpcVNum);
            if (npcinfo == null || IsDisabled || !IsAlive) return "";
            IsOut = false;
            return StaticPacketHelper.In(type: UserType.Npc,
                callerVNum: Npc.OriginalNpcMonsterVNum > 0 ? Npc.OriginalNpcMonsterVNum : NpcVNum, callerId: MapNpcId,
                mapX: MapX, mapY: MapY, direction: Position,
                currentHp: 100, currentMp: 100, dialog: Dialog, respawnType: respawnType, isSitting: IsSitting,
                name: string.IsNullOrEmpty(value: Name) ? "-" : Name, invisible: Invisible);
        }

        public string GenerateOut()
        {
            var npcinfo = ServerManager.GetNpcMonster(npcVNum: NpcVNum);
            if (npcinfo == null || IsDisabled) return "";
            IsOut = true;
            return $"out 2 {MapNpcId}";
        }

        public string GetNpcDialog()
        {
            return $"npc_req 2 {MapNpcId} {Dialog}";
        }

        public void Initialize(MapInstance currentMapInstance)
        {
            MapInstance = currentMapInstance;
            Initialize();
        }

        public void Initialize()
        {
            if (MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance &&
                ServerManager.Instance.MapBossVNums.Contains(item: NpcVNum))
            {
                var randomCell = MapInstance.Map.GetRandomPosition();
                if (randomCell != null)
                {
                    if (MapInstance.Portals.Any(predicate: s =>
                        Map.GetDistance(p: new MapCell { X = s.SourceX, Y = s.SourceY },
                            q: new MapCell { X = randomCell.X, Y = randomCell.Y }) < 5))
                        randomCell = MapInstance.Map.GetRandomPosition();
                    MapX = randomCell.X;
                    MapY = randomCell.Y;
                }
            }

            _random = new Random(Seed: MapNpcId);
            Npc = ServerManager.GetNpcMonster(npcVNum: NpcVNum);
            MaxHp = Npc.MaxHp;
            MaxMp = Npc.MaxMp;

            if (MapInstance?.MapInstanceType == MapInstanceType.TimeSpaceInstance)
                if (IsProtected)
                {
                    MaxHp *= 8;
                    MaxMp *= 8;
                }

            IsAlive = true;
            CurrentHp = MaxHp;
            CurrentMp = MaxMp;
            LastEffect = DateTime.Now;
            LastProtectedEffect = DateTime.Now;
            LastMove = DateTime.Now;
            LastSkill = DateTime.Now;
            IsHostile = Npc.IsHostile;
            ShouldRespawn = ShouldRespawn ?? true;
            FirstX = MapX;
            FirstY = MapY;
            EffectActivated = true;
            _movetime = ServerManager.RandomNumber(min: 500, max: 3000);
            Path = new List<Node>();
            Recipes = ServerManager.Instance.GetRecipesByMapNpcId(mapNpcId: MapNpcId);
            Target = -1;
            Teleporters = ServerManager.Instance.GetTeleportersByNpcVNum(npcMonsterVNum: MapNpcId);
            var shop = ServerManager.Instance.GetShopByMapNpcId(mapNpcId: MapNpcId);
            if (shop != null)
            {
                shop.Initialize();
                Shop = shop;
            }

            Skills = new List<NpcMonsterSkill>();
            foreach (var ski in Npc.Skills)
                Skills.Add(item: new NpcMonsterSkill { SkillVNum = ski.SkillVNum, Rate = ski.Rate });
            BattleEntity = new BattleEntity(npc: this);

            if (AliveTime > 0)
            {
                var aliveTimeThread = new Thread(start: () => AliveTimeCheck());
                aliveTimeThread.Start();
            }

            if (NpcVNum == 1408)
                OnDeathEvents.Add(item: new EventContainer(mapInstance: MapInstance,
                    eventActionType: EventActionType.Spawnmonster,
                    param: new MonsterToSummon(vnum: 621, spawnCell: new MapCell { X = MapX, Y = MapY }, target: null,
                        move: true)));
            if (NpcVNum == 1409)
                OnDeathEvents.Add(item: new EventContainer(mapInstance: MapInstance,
                    eventActionType: EventActionType.Spawnmonster,
                    param: new MonsterToSummon(vnum: 622, spawnCell: new MapCell { X = MapX, Y = MapY }, target: null,
                        move: true)));
            if (NpcVNum == 1410)
                OnDeathEvents.Add(item: new EventContainer(mapInstance: MapInstance,
                    eventActionType: EventActionType.Spawnmonster,
                    param: new MonsterToSummon(vnum: 623, spawnCell: new MapCell { X = MapX, Y = MapY }, target: null,
                        move: true)));

            if (OnSpawnEvents.Any())
            {
                OnSpawnEvents.ToList().ForEach(action: e => { EventHelper.Instance.RunEvent(evt: e, npc: this); });
                OnSpawnEvents.Clear();
            }
        }

        void AliveTimeCheck()
        {
            var percentPerSecond = 100 / (double)AliveTime;
            for (var i = 0; i < AliveTime; i++)
            {
                if (!IsAlive || CurrentHp <= 0) return;
                CurrentHp -= MaxHp * percentPerSecond / 100;
                if (CurrentHp <= 0) break;
                Thread.Sleep(millisecondsTimeout: 1000);
            }

            MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Npc, callerId: MapNpcId));
            MapInstance.RemoveNpc(npcToRemove: this);
        }

        /// <summary>
        ///     Check if the Monster is in the given Range.
        /// </summary>
        /// <param name="mapX">The X coordinate on the Map of the object to check.</param>
        /// <param name="mapY">The Y coordinate on the Map of the object to check.</param>
        /// <param name="distance">The maximum distance of the object to check.</param>
        /// <returns>True if the Monster is in range, False if not.</returns>
        public bool IsInRange(short mapX, short mapY, byte distance)
        {
            return Map.GetDistance(p: new MapCell
            {
                X = mapX,
                Y = mapY
            }, q: new MapCell
            {
                X = MapX,
                Y = MapY
            }) <= distance;
        }

        public void RunDeathEvent()
        {
            MapInstance.InstanceBag.NpcsKilled++;
            OnDeathEvents.ForEach(action: e =>
            {
                if (e.EventActionType == EventActionType.Throwitems)
                {
                    var evt = (Tuple<int, short, byte, int, int>)e.Parameter;
                    e.Parameter =
                        new Tuple<int, short, byte, int, int>(item1: MapNpcId, item2: evt.Item2, item3: evt.Item3,
                            item4: evt.Item4, item5: evt.Item5);
                }

                EventHelper.Instance.RunEvent(evt: e);
            });

            if (OnDeathEvents.Any(predicate: s => s.EventActionType == EventActionType.Spawnmonsters)
                && (List<MonsterToSummon>)OnDeathEvents
                    .FirstOrDefault(predicate: e => e.EventActionType == EventActionType.Spawnmonsters)
                    ?.Parameter is { } summonParameters)
                Parallel.ForEach(source: summonParameters, body: npcMonster =>
                {
                    npcMonster.SpawnCell.X = MapX;
                    npcMonster.SpawnCell.Y = MapY;
                });
            if (OnDeathEvents.Any(predicate: s => s.EventActionType == EventActionType.Spawnnpc)
                && (NpcToSummon)OnDeathEvents
                    .FirstOrDefault(predicate: e => e.EventActionType == EventActionType.Spawnnpc)
                    ?.Parameter is { } npcMonsterToSummon)
            {
                npcMonsterToSummon.SpawnCell.X = MapX;
                npcMonsterToSummon.SpawnCell.Y = MapY;
            }

            OnDeathEvents.RemoveAll(match: s => s != null);
        }

        public void SetDeathStatement()
        {
            if (Npc.BCards.Any(predicate: s =>
                s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoDeath / 10 && s.FirstData == -1))
            {
                CurrentHp = MaxHp;
                return;
            }

            IsAlive = false;
            CurrentHp = 0;
            CurrentMp = 0;
            Death = DateTime.Now;
            LastMove = DateTime.Now;
            DisableBuffs(type: BuffType.All);
            // Respawn
            if (ShouldRespawn != null && !ShouldRespawn.Value)
            {
                MapInstance.RemoveNpc(npcToRemove: this);
                MapInstance.Broadcast(packet: GenerateOut());
            }
        }

        /// <summary>
        ///     Remove the current Target from Npc.
        /// </summary>
        internal void RemoveTarget()
        {
            if (Target != -1)
            {
                Path.Clear();
                Target = -1;

                //return to origin
                Path = BestFirstSearch.FindPathJagged(start: new Node { X = MapX, Y = MapY },
                    end: new Node { X = FirstX, Y = FirstY },
                    Grid: MapInstance.Map.JaggedGrid);
            }
        }

        internal void StartLife()
        {
            try
            {
                if (!MapInstance.IsSleeping) NpcLife();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        void Respawn()
        {
            if (Npc != null)
            {
                DisableBuffs(type: BuffType.All);
                IsAlive = true;
                Target = -1;
                MaxHp = Npc.MaxHp;
                MaxMp = Npc.MaxMp;
                CurrentHp = MaxHp;
                CurrentMp = MaxMp;
                MapX = FirstX;
                MapY = FirstY;
                if (MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance &&
                    ServerManager.Instance.MapBossVNums.Contains(item: NpcVNum))
                {
                    var randomCell = MapInstance.Map.GetRandomPosition();
                    if (randomCell != null)
                    {
                        MapX = randomCell.X;
                        MapY = randomCell.Y;
                    }
                }

                Path = new List<Node>();
                MapInstance.Broadcast(packet: GenerateIn());
                Npc.BCards.Where(predicate: s => s.Type != 25).ToList()
                    .ForEach(action: s => s.ApplyBCards(session: BattleEntity, sender: BattleEntity));
            }
        }

        void NpcLife()
        {
            // Respawn
            if (CurrentHp <= 0 && ShouldRespawn != null && !ShouldRespawn.Value)
            {
                MapInstance.RemoveNpc(npcToRemove: this);
                MapInstance.Broadcast(packet: GenerateOut());
            }

            if (!IsAlive && ShouldRespawn != null && ShouldRespawn.Value)
            {
                var timeDeath = (DateTime.Now - Death).TotalSeconds;
                if (timeDeath >= Npc.RespawnTime / 10d) Respawn();
            }

            if (LastProtectedEffect.AddMilliseconds(value: 6000) <= DateTime.Now)
            {
                LastProtectedEffect = DateTime.Now;
                if (IsMate || IsProtected)
                    MapInstance.Broadcast(
                        packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc, callerId: MapNpcId,
                            effectId: 825), xRangeCoordinate: MapX, yRangeCoordinate: MapY);
            }

            var time = (DateTime.Now - LastEffect).TotalMilliseconds;

            if (EffectDelay > 0)
                if (time > EffectDelay)
                {
                    if (Effect > 0 && EffectActivated)
                        MapInstance.Broadcast(
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc, callerId: MapNpcId,
                                effectId: Effect), xRangeCoordinate: MapX,
                            yRangeCoordinate: MapY);

                    LastEffect = DateTime.Now;
                }

            time = (DateTime.Now - LastMove).TotalMilliseconds;
            if (Target == -1 && IsMoving && Npc.Speed > 0 && time > _movetime &&
                !HasBuff(type: CardType.Move, subtype: (byte)AdditionalTypes.Move.MovementImpossible))
            {
                _movetime = ServerManager.RandomNumber(min: 500, max: 3000);
                var maxindex = Path.Count > Npc.Speed / 2 && Npc.Speed > 1 ? Npc.Speed / 2 : Path.Count;
                if (maxindex < 1) maxindex = 1;
                if (Path.Count == 0 || Path.Count >= maxindex && maxindex > 0 && Path[index: maxindex - 1] == null)
                {
                    var unused = (short)ServerManager.RandomNumber(min: -1, max: 1);
                    var unused1 = (short)ServerManager.RandomNumber(min: -1, max: 1);

                    var moveToPosition = new MapCell { X = FirstX, Y = FirstY };
                    if (RunToX != 0 || RunToY != 0)
                    {
                        moveToPosition = new MapCell { X = RunToX, Y = RunToY };
                        _movetime = ServerManager.RandomNumber(min: 300, max: 1200);
                    }

                    Path = BestFirstSearch.FindPathJagged(start: new GridPos { X = MapX, Y = MapY },
                        end: new GridPos
                        {
                            X = (short)ServerManager.RandomNumber(min: moveToPosition.X - 3,
                                max: moveToPosition.X + 3),
                            Y = (short)ServerManager.RandomNumber(min: moveToPosition.Y - 3, max: moveToPosition.Y + 3)
                        }, Grid: MapInstance.Map.JaggedGrid);
                    /*
                                        maxindex = Path.Count > Npc.Speed / 2 && Npc.Speed > 1 ? Npc.Speed / 2 : Path.Count;
                    */
                }

                if (DateTime.Now > LastMove && Npc.Speed > 0 && Path.Count > 0)
                {
                    var speedIndex = (byte)(Npc.Speed / 2.5 < 1 ? 1 : Npc.Speed / 2.5);
                    maxindex = Path.Count > speedIndex ? speedIndex : Path.Count;
                    var mapX = (short)ServerManager.RandomNumber(min: Path[index: maxindex - 1].X - 1,
                        max: Path[index: maxindex - 1].X + 1);
                    var mapY = (short)_random.Next(minValue: Path[index: maxindex - 1].Y - 1,
                        maxValue: Path[index: maxindex - 1].Y + 1);

                    //short mapX = Path[maxindex - 1].X;
                    //short mapY = Path[maxindex - 1].Y;
                    var waitingtime =
                        Map.GetDistance(p: new MapCell { X = mapX, Y = mapY }, q: new MapCell { X = MapX, Y = MapY }) /
                        (double)Npc.Speed;
                    MapInstance.Broadcast(packet: new BroadcastPacket(session: null,
                        packet: PacketFactory.Serialize(packet: StaticPacketHelper.Move(type: UserType.Npc,
                            callerId: MapNpcId, positionX: mapX, positionY: mapY, speed: Npc.Speed)),
                        receiver: ReceiverType.All, xCoordinate: mapX, yCoordinate: mapY));
                    LastMove = DateTime.Now.AddSeconds(value: waitingtime > 1 ? 1 : waitingtime);

                    Observable.Timer(
                            dueTime: TimeSpan.FromMilliseconds(
                                value: (int)((waitingtime > 1 ? 1 : waitingtime) * 1000)))
                        .Subscribe(onNext: x =>
                        {
                            MapX = mapX;
                            MapY = mapY;
                        });

                    Path.RemoveRange(index: 0, count: maxindex);
                }
            }

            if (Target == -1)
            {
                if (IsHostile && Shop == null)
                {
                    var monster = MapInstance
                        .GetMonsterInRangeList(mapX: MapX, mapY: MapY,
                            distance: (byte)(Npc.NoticeRange > 5 ? Npc.NoticeRange / 2 : Npc.NoticeRange))
                        .Where(predicate: s => BattleEntity.CanAttackEntity(receiver: s.BattleEntity)).FirstOrDefault();
                    var session = MapInstance.Sessions.FirstOrDefault(predicate: s =>
                        BattleEntity.CanAttackEntity(receiver: s.Character.BattleEntity) &&
                        MapInstance == s.Character.MapInstance && Map.GetDistance(p: new MapCell { X = MapX, Y = MapY },
                            q: new MapCell { X = s.Character.PositionX, Y = s.Character.PositionY }) < Npc.NoticeRange);

                    if (monster != null) Target = monster.MapMonsterId;
                    if (session?.Character != null) Target = session.Character.CharacterId;
                }
            }
            else if (Target != -1)
            {
                var monster = MapInstance.Monsters.Find(match: s => s.MapMonsterId == Target);
                if (monster == null || monster.CurrentHp < 1)
                {
                    Target = -1;
                    return;
                }

                NpcMonsterSkill npcMonsterSkill = null;
                if (ServerManager.RandomNumber(min: 0, max: 10) > 8)
                    npcMonsterSkill = Skills
                        .Where(predicate: s =>
                            (DateTime.Now - s.LastSkillUse).TotalMilliseconds >= 100 * s.Skill.Cooldown)
                        .OrderBy(keySelector: rnd => _random.Next()).FirstOrDefault();
                var hitmode = 0;
                var onyxWings = false;
                var damage = DamageHelper.Instance.CalculateDamage(attacker: new BattleEntity(npc: this),
                    defender: new BattleEntity(monster: monster),
                    skill: npcMonsterSkill?.Skill, hitMode: ref hitmode, onyxWings: ref onyxWings);
                if (monster.Monster.BCards.Find(match: s =>
                    s.Type == (byte)CardType.LightAndShadow &&
                    s.SubType == (byte)AdditionalTypes.LightAndShadow.InflictDamageToMp) is { } card)
                {
                    var reduce = damage / 100 * card.FirstData;
                    if (monster.CurrentMp < reduce)
                    {
                        reduce = (int)monster.CurrentMp;
                        monster.CurrentMp = 0;
                    }
                    else
                    {
                        monster.DecreaseMp(amount: reduce);
                    }

                    damage -= reduce;
                }

                var distance = Map.GetDistance(p: new MapCell { X = MapX, Y = MapY },
                    q: new MapCell { X = monster.MapX, Y = monster.MapY });
                if (monster.CurrentHp > 0 &&
                    (npcMonsterSkill != null && distance < npcMonsterSkill.Skill.Range || distance <= Npc.BasicRange) &&
                    !HasBuff(type: CardType.SpecialAttack, subtype: (byte)AdditionalTypes.SpecialAttack.NoAttack))
                {
                    if ((DateTime.Now - LastSkill).TotalMilliseconds >= 1000 + Npc.BasicCooldown * 200 ||
                        npcMonsterSkill != null)
                    {
                        if (npcMonsterSkill != null)
                        {
                            npcMonsterSkill.LastSkillUse = DateTime.Now;
                            MapInstance.Broadcast(packet: StaticPacketHelper.CastOnTarget(attackerType: UserType.Npc,
                                attackerId: MapNpcId,
                                defenderType: UserType.Monster, defenderId: Target,
                                castAnimation: npcMonsterSkill.Skill.CastAnimation,
                                castEffect: npcMonsterSkill.Skill.CastEffect,
                                skillVNum: npcMonsterSkill.Skill.SkillVNum));
                        }

                        if (npcMonsterSkill != null && npcMonsterSkill.Skill.CastEffect != 0)
                            MapInstance.Broadcast(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                callerId: MapNpcId, effectId: Effect));
                        monster.BattleEntity.GetDamage(damage: damage, damager: BattleEntity);
                        lock (monster.DamageList)
                        {
                            if (!monster.DamageList.Any(predicate: s => s.Key.MapEntityId == MapNpcId))
                                monster.AddToAggroList(aggroEntity: BattleEntity);
                        }

                        MapInstance.Broadcast(packet: npcMonsterSkill != null
                            ? StaticPacketHelper.SkillUsed(type: UserType.Npc, callerId: MapNpcId, secondaryType: 3,
                                targetId: Target, skillVNum: npcMonsterSkill.SkillVNum,
                                cooldown: npcMonsterSkill.Skill.Cooldown,
                                attackAnimation: npcMonsterSkill.Skill.AttackAnimation,
                                skillEffect: npcMonsterSkill.Skill.Effect, x: 0, y: 0, isAlive: monster.CurrentHp > 0,
                                health: (int)((float)monster.CurrentHp / (float)monster.MaxHp * 100), damage: damage,
                                hitmode: hitmode, skillType: 0)
                            : StaticPacketHelper.SkillUsed(type: UserType.Npc, callerId: MapNpcId, secondaryType: 3,
                                targetId: Target, skillVNum: 0, cooldown: Npc.BasicCooldown, attackAnimation: 11,
                                skillEffect: Npc.BasicSkill, x: 0, y: 0, isAlive: monster.CurrentHp > 0,
                                health: (int)((float)monster.CurrentHp / (float)monster.MaxHp * 100), damage: damage,
                                hitmode: hitmode, skillType: 0));
                        LastSkill = DateTime.Now;

                        if (npcMonsterSkill?.Skill.TargetType == 1 && npcMonsterSkill?.Skill.HitType == 2)
                        {
                            var clientSessions =
                                MapInstance.Sessions?.Where(predicate: s =>
                                    s.Character.IsInRange(xCoordinate: MapX,
                                        yCoordinate: MapY, range: npcMonsterSkill.Skill.TargetRange));
                            IEnumerable<Mate> mates =
                                MapInstance.GetListMateInRange(mapX: MapX, mapY: MapY,
                                    distance: npcMonsterSkill.Skill.TargetRange);

                            foreach (var skillBcard in npcMonsterSkill.Skill.BCards)
                                if (skillBcard.Type == 25 && skillBcard.SubType == 1 &&
                                    new Buff.Buff(id: (short)skillBcard.SecondData, level: Npc.Level).Card?.BuffType ==
                                    BuffType.Good)
                                {
                                    if (clientSessions != null)
                                        foreach (var clientSession in clientSessions)
                                            if (clientSession.Character != null)
                                                if (!BattleEntity.CanAttackEntity(
                                                    receiver: clientSession.Character.BattleEntity))
                                                    skillBcard.ApplyBCards(
                                                        session: clientSession.Character.BattleEntity,
                                                        sender: BattleEntity);
                                    if (mates != null)
                                        foreach (var mate in mates)
                                            if (!BattleEntity.CanAttackEntity(receiver: mate.BattleEntity))
                                                skillBcard.ApplyBCards(session: mate.BattleEntity,
                                                    sender: BattleEntity);
                                }
                        }

                        if (monster.CurrentHp < 1 && monster.SetDeathStatement())
                        {
                            monster.RunDeathEvent();
                            RemoveTarget();
                        }
                    }
                }
                else
                {
                    var unused = Npc.NoticeRange > 5 ? Npc.NoticeRange / 2 : Npc.NoticeRange;
                    if (IsMoving && !HasBuff(type: CardType.Move,
                        subtype: (byte)AdditionalTypes.Move.MovementImpossible))
                    {
                        const short maxDistance = 5;
                        var maxindex = Path.Count > Npc.Speed / 2 && Npc.Speed > 1 ? Npc.Speed / 2 : Path.Count;
                        if (maxindex < 1) maxindex = 1;
                        if (Path.Count == 0 && distance >= 1 && distance < maxDistance ||
                            Path.Count >= maxindex && maxindex > 0 && Path[index: maxindex - 1] == null)
                        {
                            var xoffset = (short)ServerManager.RandomNumber(min: -1, max: 1);
                            var yoffset = (short)ServerManager.RandomNumber(min: -1, max: 1);

                            //go to monster
                            Path = BestFirstSearch.FindPathJagged(start: new GridPos { X = MapX, Y = MapY },
                                end: new GridPos
                                {
                                    X = (short)(monster.MapX + xoffset),
                                    Y = (short)(monster.MapY + yoffset)
                                }, Grid: MapInstance.Map.JaggedGrid);
                            /*
                                                        maxindex = Path.Count > Npc.Speed / 2 && Npc.Speed > 1 ? Npc.Speed / 2 : Path.Count;
                            */
                        }

                        if (DateTime.Now > LastMove && Npc.Speed > 0 && Path.Count > 0)
                        {
                            var speedIndex = (byte)(Npc.Speed / 2.5 < 1 ? 1 : Npc.Speed / 2.5);
                            maxindex = Path.Count > speedIndex ? speedIndex : Path.Count;
                            //short mapX = (short)ServerManager.RandomNumber(Path[maxindex - 1].X - 1, Path[maxindex - 1].X + 1);
                            //short mapY = (short)_random.Next(Path[maxindex - 1].Y - 1, Path[maxindex - 1].Y + 1);

                            var mapX = Path[index: maxindex - 1].X;
                            var mapY = Path[index: maxindex - 1].Y;
                            var waitingtime =
                                Map.GetDistance(p: new MapCell { X = mapX, Y = mapY },
                                    q: new MapCell { X = MapX, Y = MapY }) /
                                (double)Npc.Speed;
                            MapInstance.Broadcast(packet: new BroadcastPacket(session: null,
                                packet: PacketFactory.Serialize(packet: StaticPacketHelper.Move(type: UserType.Npc,
                                    callerId: MapNpcId, positionX: mapX, positionY: mapY,
                                    speed: Npc.Speed)), receiver: ReceiverType.All, xCoordinate: mapX,
                                yCoordinate: mapY));
                            LastMove = DateTime.Now.AddSeconds(value: waitingtime > 1 ? 1 : waitingtime);

                            Observable.Timer(
                                    dueTime: TimeSpan.FromMilliseconds(
                                        value: (int)((waitingtime > 1 ? 1 : waitingtime) * 1000)))
                                .Subscribe(onNext: x =>
                                {
                                    MapX = mapX;
                                    MapY = mapY;
                                });

                            Path.RemoveRange(index: 0, count: maxindex);
                        }

                        if (Target != -1 && (MapId != monster.MapId || distance > maxDistance)) RemoveTarget();
                    }
                }
            }
        }

        #endregion
    }
}