using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Threading;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Battle;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Event.ACT4;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.PathFinder;
using static OpenNos.Domain.BCardType;

namespace OpenNos.GameObject
{
    public class MapMonster : MapMonsterDto
    {
        #region Members

        public object OnHitLockObject = new object();

        const int MaxDistance = 20;

        const int TimeAgroLoss = 8000;

        byte _speed;

        short _previousSkillVNum;

        #endregion

        #region Instantiation

        public MapMonster()
        {
            HitQueue = new ConcurrentQueue<HitRequest>();
            OnNoticeEvents = new List<EventContainer>();
            OnSpawnEvents = new List<EventContainer>();
            PVELockObject = new object();
        }

        public MapMonster(MapMonsterDto input) : this()
        {
            IsDisabled = input.IsDisabled;
            IsMoving = input.IsMoving;
            MapId = input.MapId;
            MapMonsterId = input.MapMonsterId;
            MapX = input.MapX;
            MapY = input.MapY;
            MonsterVNum = input.MonsterVNum;
            Name = input.Name;
            Position = input.Position;
        }

        #endregion

        #region Properties

        public bool IsSelfAttack
        {
            get => MonsterHelper.IsSelfAttack(monsterVNum: MonsterVNum);
        }

        public bool IsPendingDelete { get; set; }

        public List<UseSkillOnDamage> UseSkillOnDamage { get; set; }

        public Node[][] BrushFireJagged { get; set; }

        public ThreadSafeSortedList<short, Buff.Buff> Buff
        {
            get => BattleEntity.Buffs;
        }

        public ThreadSafeSortedList<short, IDisposable> BuffObservables
        {
            get => BattleEntity.BuffObservables;
        }

        public double CurrentHp { get; set; }

        public double CurrentMp { get; set; }

        public IDictionary<BattleEntity, long> DamageList { get; private set; }

        public List<BattleEntity> AggroList { get; set; }

        public DateTime Death { get; set; }

        public ConcurrentQueue<HitRequest> HitQueue { get; }

        public bool IsAlive { get; set; }

        public bool IsBonus { get; set; }

        public bool IsBoss { get; set; }

        public bool IsHostile { get; set; }

        public bool IsTarget { get; set; }

        public bool IsJumping { get; set; }

        public DateTime LastDefence { get; set; }

        public DateTime LastEffect { get; set; }

        public DateTime LastHealth { get; set; }

        public DateTime LastMonsterAggro { get; set; }

        public DateTime LastMove { get; set; }

        public DateTime LastSkill { get; set; }

        public IDisposable LifeEvent { get; set; }

        public MapInstance MapInstance { get; set; }

        public int BaseMaxHp { get; set; }

        public int BaseMaxMp { get; set; }

        public double MaxHp { get; set; }

        public double MaxMp { get; set; }

        public NpcMonster Monster { get; private set; }

        public ZoneEvent MoveEvent { get; set; }

        public bool NoAggresiveIcon { get; internal set; }

        public byte NoticeRange { get; set; }

        public List<EventContainer> OnDeathEvents
        {
            get => BattleEntity.OnDeathEvents;
        }

        public List<EventContainer> OnNoticeEvents { get; set; }

        public List<EventContainer> OnSpawnEvents { get; set; }

        public List<Node> Path { get; set; }

        public bool? ShouldRespawn { get; set; }

        public List<NpcMonsterSkill> Skills { get; set; }

        public bool Started { get; internal set; }

        public byte Speed
        {
            get
            {
                if (_speed > 59) return 59;
                return _speed;
            }

            set => _speed = value > 59 ? (byte)59 : value;
        }

        public BattleEntity Target { get; set; }

        public short RunToX { get; set; }

        public short RunToY { get; set; }

        public short FirstX { get; set; }

        public short FirstY { get; set; }

        public BattleEntity Owner { get; set; }

        public object PVELockObject { get; set; }

        public bool Invisible { get; set; }

        public int AliveTime { get; set; }

        public int AliveTimeMp { get; set; }

        public bool CanSeeHiddenThings
        {
            get =>
                GetBuff(type: CardType.SpecialActions,
                    subtype: (byte)AdditionalTypes.SpecialActions.SeeHiddenThings)[0] < 0;
        }

        public FactionType Faction { get; set; }

        #endregion

        #region BattleEntityProperties

        public BattleEntity BattleEntity { get; set; }

        public void AddBuff(Buff.Buff indicator, BattleEntity battleEntity, bool forced = false)
        {
            BattleEntity.AddBuff(indicator: indicator, sender: battleEntity, forced: forced);
        }

        public void RemoveBuff(short cardId)
        {
            BattleEntity.RemoveBuff(id: cardId);
        }

        public int[] GetBuff(CardType type, byte subtype)
        {
            return BattleEntity.GetBuff(type: type, subtype: subtype);
        }

        public bool HasBuff(CardType type, byte subtype)
        {
            return BattleEntity.HasBuff(type: type, subtype: subtype);
        }

        public void DisableBuffs(BuffType type, int level = 100)
        {
            BattleEntity.DisableBuffs(type: type, level: level);
        }

        public void DisableBuffs(List<BuffType> types, int level = 100)
        {
            BattleEntity.DisableBuffs(types: types, level: level);
        }

        public MapCell GetPos()
        {
            return BattleEntity.GetPos();
        }

        public void DecreaseMp(int amount)
        {
            BattleEntity.DecreaseMp(amount: amount);
        }

        #endregion

        #region Methods

        public int HpPercent()
        {
            return BattleEntity.HpPercent();
        }

        public int MpPercent()
        {
            return BattleEntity.MpPercent();
        }

        public string GenerateBoss()
        {
            return $"rboss 3 {MapMonsterId} {CurrentHp} {MaxHp}";
        }

        public string GenerateIn()
        {
            if (IsAlive && !IsDisabled && !IsJumping)
                return StaticPacketHelper.In(type: UserType.Monster,
                    callerVNum: Monster.OriginalNpcMonsterVNum > 0 ? Monster.OriginalNpcMonsterVNum : MonsterVNum,
                    callerId: MapMonsterId,
                    mapX: MapX, mapY: MapY, direction: Position,
                    currentHp: (int)(CurrentHp / MaxHp * 100), currentMp: (int)(CurrentMp / MaxMp * 100), dialog: 0,
                    respawnType: NoAggresiveIcon ? InRespawnType.NoEffect : InRespawnType.TeleportationEffect,
                    isSitting: false,
                    name: string.IsNullOrEmpty(value: Name) ? "-" : Name, invisible: Invisible);

            return "";
        }

        public void Initialize(MapInstance currentMapInstance)
        {
            MapInstance = currentMapInstance;
            Initialize();
        }

        public void Initialize()
        {
            if (MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance &&
                ServerManager.Instance.MapBossVNums.Contains(item: MonsterVNum))
            {
                var randomCell = MapInstance.Map.GetRandomPosition();
                if (randomCell != null)
                {
                    if (MapInstance.Portals.Any(predicate: s =>
                        Map.GetDistance(p: new MapCell { X = s.SourceX, Y = s.SourceY },
                            q: new MapCell { X = randomCell.X, Y = randomCell.Y }) < 5))
                        randomCell = MapInstance.Map.GetRandomPosition();
                    MapX = randomCell.X;
                    MapY = randomCell.Y;
                }
            }

            FirstX = MapX;
            FirstY = MapY;
            LastSkill = LastMove = LastEffect = new DateTime();
            Path = new List<Node>();
            IsAlive = true;
            ShouldRespawn ??= true;
            Monster = ServerManager.GetNpcMonster(npcVNum: MonsterVNum);
            Faction = MonsterHelper.GetFaction(monsterVNum: MonsterVNum);

            if (BaseMaxHp <= 0) BaseMaxHp = Monster.MaxHp > 0 ? Monster.MaxHp : -Monster.MaxHp;
            if (BaseMaxMp <= 0) BaseMaxMp = Monster.MaxMp;
            MaxHp = BaseMaxHp;
            MaxMp = BaseMaxMp;

            if (MapInstance?.MapInstanceType == MapInstanceType.RaidInstance)
            {
                if (IsBoss)
                {
                    MaxHp *= 5;
                    MaxMp *= 5;
                }
                else
                {
                    MaxHp *= 3;
                    MaxMp *= 3;

                    if (IsTarget)
                    {
                        MaxHp *= 4;
                        MaxMp *= 4;
                    }
                }

                // Huge Snowman Head
                if (MonsterVNum == 533)
                    if (ServerManager.Instance.GetSessionByCharacterId(characterId: MapInstance.InstanceBag.CreatorId)
                        ?.Character
                        .Group is { } raidGroup)
                    {
                        var entitiesCount = raidGroup.Sessions.Count;
                        BaseMaxHp = 63 * 300 * entitiesCount;
                        MaxHp = BaseMaxHp;
                    }
            }

            // Irrelevant for now(Act4)
            //if (MapInstance?.MapInstanceType == MapInstanceType.Act4Morcos || MapInstance?.MapInstanceType == MapInstanceType.Act4Hatus || MapInstance?.MapInstanceType == MapInstanceType.Act4Calvina || MapInstance?.MapInstanceType == MapInstanceType.Act4Berios)
            //{
            //    if (MonsterVNum == 563 || MonsterVNum == 577 || MonsterVNum == 629 || MonsterVNum == 624)
            //    {
            //        MaxHp *= 5;
            //        MaxMp *= 5;
            //    }
            //}

            NoAggresiveIcon = Monster.NoAggresiveIcon;

            IsHostile = Monster.IsHostile;

            CurrentHp = MaxHp;
            CurrentMp = MaxMp;

            Skills = new List<NpcMonsterSkill>();

            foreach (var ski in Monster.Skills)
                Skills.Add(item: new NpcMonsterSkill { SkillVNum = ski.SkillVNum, Rate = ski.Rate });

            DamageList = new Dictionary<BattleEntity, long>();
            AggroList = new List<BattleEntity>();
            ServerManager.RandomNumber(min: 400, max: 3200);

            BattleEntity = new BattleEntity(monster: this);

            // Test damage on arena spawned by command mobs
            if (Owner == null && MapInstance.Map.MapId == 2006)
            {
                MaxHp = 500000;
                CurrentHp = MaxHp;
                BattleEntity.BCards.AddRange(collection: new Buff.Buff(id: 196, level: 99).Card.BCards);
            }

            Monster.BCards.Where(predicate: s => s.Type != 25).ToList()
                .ForEach(action: s => s.ApplyBCards(session: BattleEntity, sender: BattleEntity));

            if (MonsterVNum == 1382) AliveTime = 20;
            if (MonsterVNum == 390) // Bird's Egg
            {
                AliveTime = 0;
                AliveTimeMp = 0;
                var transformCountDownThread = new Thread(start: () => TransformCountDown(seconds: 40));
                transformCountDownThread.Start();
            }

            if (AliveTime > 0)
            {
                var aliveTimeThread = new Thread(start: () => AliveTimeCheck());
                aliveTimeThread.Start();
            }

            if (AliveTimeMp > 0)
            {
                var aliveTimeMpThread = new Thread(start: () => AliveTimeMpCheck());
                aliveTimeMpThread.Start();
            }

            if (BattleEntity.HasBuff(type: CardType.Count, subtype: (byte)AdditionalTypes.Count.Summon))
            {
                var disminMpPerSecThread = new Thread(start: () => DisminPercentMpPerSec(percentPerSecond: 1));
                if (MonsterVNum == 2013 || MonsterVNum == 2016)
                    disminMpPerSecThread = new Thread(start: () => DisminPercentMpPerSec(percentPerSecond: 8));
                disminMpPerSecThread.Start();
            }

            if (MonsterVNum == 621)
                OnDeathEvents.Add(item: new EventContainer(mapInstance: MapInstance,
                    eventActionType: EventActionType.Spawnnpc,
                    param: new NpcToSummon(vnum: 1408, spawnCell: new MapCell { X = MapX, Y = MapY }, target: -1,
                        move: true)));
            if (MonsterVNum == 622)
                OnDeathEvents.Add(item: new EventContainer(mapInstance: MapInstance,
                    eventActionType: EventActionType.Spawnnpc,
                    param: new NpcToSummon(vnum: 1409, spawnCell: new MapCell { X = MapX, Y = MapY }, target: -1,
                        move: true)));
            if (MonsterVNum == 623)
                OnDeathEvents.Add(item: new EventContainer(mapInstance: MapInstance,
                    eventActionType: EventActionType.Spawnnpc,
                    param: new NpcToSummon(vnum: 1410, spawnCell: new MapCell { X = MapX, Y = MapY }, target: -1,
                        move: true)));

            if (OnSpawnEvents.Any())
            {
                OnSpawnEvents.ToList().ForEach(action: e => { EventHelper.Instance.RunEvent(evt: e, monster: this); });
                OnSpawnEvents.Clear();
            }
        }

        void AliveTimeCheck()
        {
            var percentPerSecond = 100 / (double)AliveTime;
            for (var i = 0; i < AliveTime; i++)
            {
                if (!IsAlive || CurrentHp <= 0) return;
                CurrentHp -= MaxHp * percentPerSecond / 100;
                if (CurrentHp <= 0) break;
                Thread.Sleep(millisecondsTimeout: 1000);
            }

            RunDeathEvent();
            MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster, callerId: MapMonsterId));
            MapInstance.RemoveMonster(monsterToRemove: this);
        }

        void AliveTimeMpCheck()
        {
            var percentPerSecond = 100 / (double)AliveTimeMp;
            for (var i = 0; i < AliveTimeMp; i++)
            {
                if (!IsAlive || CurrentHp <= 0) return;
                CurrentMp -= MaxMp * percentPerSecond / 100;
                if (CurrentMp <= 0) break;
                Thread.Sleep(millisecondsTimeout: 1000);
            }

            RunDeathEvent();
            MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster, callerId: MapMonsterId));
            MapInstance.RemoveMonster(monsterToRemove: this);
        }

        void DisminPercentMpPerSec(double percentPerSecond)
        {
            for (var i = 0; i < 100 / percentPerSecond + 1; i++)
            {
                if (!IsAlive || CurrentHp <= 0) return;
                CurrentMp -= MaxMp * percentPerSecond / 100;
                if (CurrentMp <= 0)
                {
                    if (BattleEntity.GetBuff(type: CardType.Count, subtype: (byte)AdditionalTypes.Count.Summon) is { }
                        countSummon)
                        MapInstance.SummonMonster(summon: new MonsterToSummon(vnum: (short)countSummon[1],
                            spawnCell: new MapCell { X = MapX, Y = MapY }, target: null, move: true));
                    break;
                }

                Thread.Sleep(millisecondsTimeout: 1000);
            }

            RunDeathEvent();
            MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster, callerId: MapMonsterId));
            MapInstance.RemoveMonster(monsterToRemove: this);
        }

        void TransformCountDown(double seconds)
        {
            for (var i = 0; i < seconds; i++)
            {
                if (!IsAlive || CurrentHp <= 0) return;
                Thread.Sleep(millisecondsTimeout: 1000);
            }

            if (MonsterVNum == 390) // Bird's Egg
                MapInstance.SummonMonster(summon: new MonsterToSummon(vnum: 383,
                    spawnCell: new MapCell { X = MapX, Y = MapY }, target: null, move: true));
            MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster, callerId: MapMonsterId));
            MapInstance.RemoveMonster(monsterToRemove: this);
        }

        public void AddToDamageList(BattleEntity damagerEntity, long damage)
        {
            lock (DamageList)
            {
                if (DamageList.FirstOrDefault(predicate: s =>
                        s.Key.MapEntityId == damagerEntity.MapEntityId && s.Key.EntityType == damagerEntity.EntityType)
                    .Key is { } existingEntity)
                {
                    if (damagerEntity == existingEntity)
                    {
                        DamageList[key: existingEntity] += damage;
                    }
                    else
                    {
                        damage += DamageList[key: existingEntity];
                        DamageList.Remove(key: existingEntity);
                        DamageList.Add(key: damagerEntity, value: damage);
                    }
                }
                else
                {
                    DamageList.Add(key: damagerEntity, value: damage);
                }
            }

            AddToAggroList(aggroEntity: damagerEntity);
        }

        public void AddToAggroList(BattleEntity aggroEntity)
        {
            lock (AggroList)
            {
                if (AggroList.ToList().FirstOrDefault(predicate: s =>
                        s.MapEntityId == aggroEntity.MapEntityId && s.EntityType == aggroEntity.EntityType) is { }
                    existingEntity)
                {
                    if (existingEntity != aggroEntity)
                    {
                        AggroList.Remove(item: existingEntity);
                        AggroList.Add(item: aggroEntity);
                    }
                }
                else
                {
                    AggroList.Add(item: aggroEntity);
                }
            }
        }

        public void RemoveFromAggroList(BattleEntity aggroEntity)
        {
            lock (AggroList)
            {
                AggroList.RemoveAll(match: s =>
                    s.MapEntityId == aggroEntity.MapEntityId && s.EntityType == aggroEntity.EntityType);
            }
        }

        /// <summary>
        ///     Check if the Monster is in the given Range.
        /// </summary>
        /// <param name="mapX">The X coordinate on the Map of the object to check.</param>
        /// <param name="mapY">The Y coordinate on the Map of the object to check.</param>
        /// <param name="distance">The maximum distance of the object to check.</param>
        /// <returns>True if the Monster is in range, False if not.</returns>
        public bool IsInRange(short mapX, short mapY, byte distance)
        {
            return Map.GetDistance(p: new MapCell
            {
                X = mapX,
                Y = mapY
            }, q: new MapCell
            {
                X = MapX,
                Y = MapY
            }) <= distance;
        }

        public void RunDeathEvent()
        {
            Buff.ClearAll();
            if (IsBonus)
            {
                MapInstance.InstanceBag.Combo++;
                MapInstance.InstanceBag.Point += EventHelper.CalculateComboPoint(n: MapInstance.InstanceBag.Combo + 1);
            }
            else
            {
                MapInstance.InstanceBag.Combo = 0;
                MapInstance.InstanceBag.Point += EventHelper.CalculateComboPoint(n: MapInstance.InstanceBag.Combo);
            }

            MapInstance.InstanceBag.MonstersKilled++;

            if (Owner != null && Monster.BCards.FirstOrDefault(predicate: s =>
                s.CastType == 0 && s.Type == (byte)CardType.SummonAndRecoverHp &&
                s.SubType == (byte)AdditionalTypes.SummonAndRecoverHp.RestoreHp / 10) is { } restoreHp)
            {
                double recoverHp = Owner.HpMax * restoreHp.FirstData / 100;
                if (Owner.Hp + recoverHp > Owner.HpMax) recoverHp = Owner.HpMax - Owner.Hp;
                Owner.Hp += (int)recoverHp;
                MapInstance.Broadcast(packet: Owner.GenerateRc(characterHealth: (int)recoverHp));
            }

            if (OnDeathEvents.Any(predicate: s => s.EventActionType == EventActionType.Spawnmonsters)
                && (List<MonsterToSummon>)OnDeathEvents
                    .FirstOrDefault(predicate: e => e.EventActionType == EventActionType.Spawnmonsters)
                    ?.Parameter is { } summonParameters)
                Parallel.ForEach(source: summonParameters, body: npcMonster =>
                {
                    npcMonster.SpawnCell.X = MapX;
                    npcMonster.SpawnCell.Y = MapY;
                });
            if (OnDeathEvents.Any(predicate: s => s.EventActionType == EventActionType.Spawnnpc)
                && (NpcToSummon)OnDeathEvents
                    .FirstOrDefault(predicate: e => e.EventActionType == EventActionType.Spawnnpc)
                    ?.Parameter is { } npcMonsterToSummon)
            {
                npcMonsterToSummon.SpawnCell.X = MapX;
                npcMonsterToSummon.SpawnCell.Y = MapY;
            }

            OnDeathEvents.ForEach(action: e => EventHelper.Instance.RunEvent(evt: e, monster: this));

            BattleEntity.ClearEnemyFalcon();
        }

        public bool SetDeathStatement()
        {
            if (Monster.BCards.Any(predicate: s =>
                s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoDeath / 10 && s.FirstData == -1))
            {
                CurrentHp = MaxHp;
                return false;
            }

            IsAlive = false;
            CurrentHp = 0;
            CurrentMp = 0;
            Death = DateTime.Now;
            LastMove = DateTime.Now;
            DisableBuffs(type: BuffType.All);
            return true;
        }

        public void StartLife()
        {
            try
            {
                if (!MapInstance.IsSleeping) MonsterLife();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        internal void GetNearestOponent()
        {
            lock (AggroList)
            {
                var entitiesList = AggroList.ToList();

                if (entitiesList.Any())
                {
                    entitiesList.AddRange(collection: entitiesList.Where(predicate: s => s.Mate != null)
                        .Select(selector: s => s.Mate.Owner.BattleEntity)
                        .ToList());
                    entitiesList.AddRange(collection: entitiesList.Where(predicate: s => s.Character != null)
                        .SelectMany(selector: s =>
                            s.Character.Mates
                                .Where(predicate: m =>
                                    (m.IsTeamMember || m.IsTemporalMate) &&
                                    !entitiesList.Contains(item: m.BattleEntity))
                                .Select(selector: m2 => m2.BattleEntity)).ToList());
                    entitiesList.AddRange(collection: entitiesList.Where(predicate: s => s.Character != null)
                        .SelectMany(selector: s =>
                            s.Character.BattleEntity.GetOwnedMonsters().Select(selector: m => m.BattleEntity))
                        .ToList());
                    entitiesList.AddRange(collection: entitiesList.Where(predicate: s => s.MapNpc != null).ToList());
                    var newTarget = entitiesList.Where(predicate: s =>
                            s.Character == null || !s.Character.InvisibleGm &&
                            (!s.Character.Invisible || CanSeeHiddenThings))
                        .OrderBy(keySelector: e => Map.GetDistance(p: GetPos(), q: e.GetPos()))
                        .FirstOrDefault(predicate: e => BattleEntity.CanAttackEntity(receiver: e));
                    if (newTarget == null && Target != null)
                        RemoveTarget();
                    else
                        Target = newTarget;
                }
            }
        }

        internal void HostilityTarget()
        {
            if (!IsHostile || Target != null) return;

            var target = MapInstance.BattleEntities
                .OrderBy(keySelector: e => Map.GetDistance(p: GetPos(), q: e.GetPos()))
                .FirstOrDefault(predicate: e => BattleEntity.CanAttackEntity(receiver: e) && e.CanBeTargetted &&
                                                (e.Mate == null || !BattleEntity.IsMateTrainer(vnum: MonsterVNum) ||
                                                 e.MapInstance != e.Mate?.Owner.Miniland ||
                                                 e.TargettedByMonstersList(teamCheck: false).Count() < 9) &&
                                                (e.Character == null
                                                 || !e.Character.InvisibleGm &&
                                                 (!e.Character.Invisible ||
                                                  CanSeeHiddenThings)) &&
                                                Map.GetDistance(p: GetPos(), q: e.GetPos()) <
                                                (NoticeRange == 0 ? Monster.NoticeRange : NoticeRange));

            if (target == null /* || MoveEvent != null*/) return;

            if (OnNoticeEvents.Any())
            {
                OnNoticeEvents.ToList().ForEach(action: e => { EventHelper.Instance.RunEvent(evt: e, monster: this); });
                OnNoticeEvents.Clear();
                return;
            }

            LastMonsterAggro = DateTime.Now;

            Target = target;
            AddToAggroList(aggroEntity: target);
            if (!Monster.NoAggresiveIcon && target.Character != null)
                Target.Character.Session.SendPacket(
                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Monster, callerId: MapMonsterId,
                        effectId: 5000));
        }

        /// <summary>
        ///     Remove the current Target from Monster.
        /// </summary>
        internal void RemoveTarget()
        {
            if (Target != null)
            {
                RemoveFromAggroList(aggroEntity: Target);
                Target = null;

                //(Path ?? (Path = new List<Node>())).Clear();
                //return to origin
                //Path = BestFirstSearch.FindPathJagged(new Node {X = MapX, Y = MapY}, new Node {X = FirstX, Y = FirstY}, MapInstance.Map.JaggedGrid);
            }
        }

        void RunAway()
        {
            if (Target == null || Monster == null || CurrentHp < 1 || !IsMoving || Monster.Speed < 1) return;

            var time = (DateTime.Now - LastMove).TotalMilliseconds;

            var timeToWalk = 2000 / Monster.Speed;

            if (time > timeToWalk)
            {
                var tempX = MapX;
                var tempY = MapY;

                var cells = (short)ServerManager.RandomNumber(min: 1, max: 3);

                if (SkillHelper.CalculateNewPosition(mapInstance: MapInstance, x: Target.PositionX, y: Target.PositionY,
                    cells: cells, mapX: ref tempX,
                    mapY: ref tempY))
                {
                    MapX = tempX;
                    MapY = tempY;

                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: timeToWalk))
                        .Subscribe(onNext: x =>
                        {
                            MapX = tempX;
                            MapY = tempY;

                            MoveEvent?.Events.ForEach(action: e =>
                                EventHelper.Instance.RunEvent(evt: e, monster: this));
                        });

                    MapInstance.Broadcast(packet: StaticPacketHelper.Move(type: UserType.Monster,
                        callerId: MapMonsterId, positionX: tempX, positionY: tempY,
                        speed: Monster.Speed));
                    MapInstance.Broadcast(packet: StaticPacketHelper.Say(type: 3, callerId: MapMonsterId,
                        secondaryType: 0, message: "!!!!"));
                }
            }
        }

        /// <summary>
        ///     Follow the Monsters target to it's position.
        /// </summary>
        /// <param name="target">The TargetSession to follow</param>
        void FollowTarget(BattleEntity target)
        {
            if (IsMoving && !HasBuff(type: CardType.Move, subtype: (byte)AdditionalTypes.Move.MovementImpossible))
            {
                if (Map.GetDistance(p: new MapCell
                {
                    X = MapX,
                    Y = MapY
                },
                    q: new MapCell
                    {
                        X = target.PositionX,
                        Y = target.PositionY
                    }) <= Monster.BasicRange && Monster.BasicRange > 0)
                    return;

                if (target.LastMonsterAggro.AddSeconds(value: 5) < DateTime.Now || target.BrushFireJagged == null)
                    target.UpdateBushFire();

                Path.Clear();

                target.LastMonsterAggro = DateTime.Now;
                if (Path.Count == 0)
                {
                    var xoffset = (short)ServerManager.RandomNumber(min: -1, max: 1);
                    var yoffset = (short)ServerManager.RandomNumber(min: -1, max: 1);
                    try
                    {
                        Path = BestFirstSearch.TracePathJagged(node: new Node { X = MapX, Y = MapY },
                            grid: target.BrushFireJagged,
                            mapGrid: target.MapInstance.Map.JaggedGrid);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(
                            data:
                            $"Pathfinding using Pathfinder failed. Map: {MapId} StartX: {MapX} StartY: {MapY} TargetX: {(short)(target.PositionX + xoffset)} TargetY: {(short)(target.PositionY + yoffset)}",
                            ex: ex);
                        RemoveTarget();
                    }
                }

                Move();
            }
        }

        public void OnReceiveHit(HitRequest hitRequest)
        {
            if (IsAlive && hitRequest.Session.Character.Hp > 0 &&
                (hitRequest.Mate == null || hitRequest.Mate.Hp > 0))
            {
                double cooldownReduction = hitRequest.Session.Character.GetBuff(type: CardType.Morale,
                    subtype: (byte)AdditionalTypes.Morale.SkillCooldownDecreased)[0];

                var increaseEnemyCooldownChance = hitRequest.Session.Character.GetBuff(type: CardType.DarkCloneSummon,
                    subtype: (byte)AdditionalTypes.DarkCloneSummon.IncreaseEnemyCooldownChance);

                if (ServerManager.RandomNumber() < increaseEnemyCooldownChance[0])
                    cooldownReduction -= increaseEnemyCooldownChance[1];

                var hitmode = 0;
                var isCaptureSkill =
                    hitRequest.SkillBCards.Any(predicate: s => s.Type.Equals(obj: (byte)CardType.Capture));

                // calculate damage
                var onyxWings = false;
                var attackerBattleEntity = hitRequest.Mate == null
                    ? new BattleEntity(character: hitRequest.Session.Character, skill: hitRequest.Skill)
                    : new BattleEntity(mate: hitRequest.Mate);
                var damage = DamageHelper.Instance.CalculateDamage(attacker: attackerBattleEntity,
                    defender: new BattleEntity(monster: this),
                    skill: hitRequest.Skill, hitMode: ref hitmode, onyxWings: ref onyxWings);

                if (Monster.BCards.Find(match: s =>
                    s.Type == (byte)CardType.LightAndShadow &&
                    s.SubType == (byte)AdditionalTypes.LightAndShadow.InflictDamageToMp) is { } card)
                {
                    var reduce = damage / 100 * card.FirstData;
                    if (CurrentMp < reduce)
                    {
                        reduce = (int)CurrentMp;
                        CurrentMp = 0;
                    }
                    else
                    {
                        DecreaseMp(amount: reduce);
                    }

                    damage -= reduce;
                }

                if (damage >= CurrentHp &&
                    Monster.BCards.Any(predicate: s =>
                        s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                        s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoDeath / 10 &&
                        s.FirstData == -1))
                {
                    damage = (int)CurrentHp - 1;
                }
                else if (onyxWings)
                {
                    var onyxX = (short)(hitRequest.Session.Character.PositionX + 2);
                    var onyxY = (short)(hitRequest.Session.Character.PositionY + 2);
                    var onyxId = MapInstance.GetNextMonsterId();
                    var onyx = new MapMonster
                    {
                        MonsterVNum = 2371,
                        MapX = onyxX,
                        MapY = onyxY,
                        MapMonsterId = onyxId,
                        IsHostile = false,
                        IsMoving = false,
                        ShouldRespawn = false
                    };
                    MapInstance.Broadcast(packet: UserInterfaceHelper.GenerateGuri(type: 31, argument: 1,
                        callerId: hitRequest.Session.Character.CharacterId, value: onyxX, value2: onyxY));
                    onyx.Initialize(currentMapInstance: MapInstance);
                    MapInstance.AddMonster(monster: onyx);
                    MapInstance.Broadcast(packet: onyx.GenerateIn());
                    BattleEntity.GetDamage(damage: damage / 2, damager: attackerBattleEntity);
                    var request = hitRequest;
                    var damage1 = damage;
                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 350)).Subscribe(onNext: o =>
                    {
                        MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: UserType.Monster,
                            callerId: onyxId, secondaryType: 3,
                            targetId: MapMonsterId, skillVNum: -1, cooldown: 0, attackAnimation: -1,
                            skillEffect: request.Skill?.Effect ?? 0, x: -1, y: -1, isAlive: IsAlive,
                            health: (int)(CurrentHp / MaxHp * 100), damage: damage1 / 2, hitmode: 0,
                            skillType: 0));
                        MapInstance.RemoveMonster(monsterToRemove: onyx);
                        MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                            callerId: onyx.MapMonsterId));
                    });
                }

                var firstHit = false;

                lock (DamageList)
                {
                    if (!DamageList.Any(predicate: s => s.Value > 0)) firstHit = true;
                    if (attackerBattleEntity.MapMonster?.Owner != null)
                        AddToDamageList(damagerEntity: attackerBattleEntity.MapMonster.Owner, damage: damage);
                    else if (attackerBattleEntity.Mate?.Owner != null)
                        AddToDamageList(damagerEntity: attackerBattleEntity.Mate.Owner.BattleEntity, damage: damage);
                    else if (attackerBattleEntity.Character != null)
                        AddToDamageList(damagerEntity: attackerBattleEntity, damage: damage);
                }

                attackerBattleEntity.BCards.Where(predicate: s => s.CastType == 1).ForEach(action: s =>
                {
                    if (s.Type != (byte)CardType.Buff)
                        s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity);
                });

                hitRequest.SkillBCards.Where(predicate: s =>
                        !s.Type.Equals(obj: (byte)CardType.Buff) && !s.Type.Equals(obj: (byte)CardType.Capture) &&
                        s.CardId == null).ToList()
                    .ForEach(action: s => s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity));

                if (hitmode != 4 && hitmode != 2)
                {
                    if (damage > 0)
                    {
                        RemoveBuff(cardId: 36);
                        RemoveBuff(cardId: 548);
                    }

                    attackerBattleEntity.BCards.Where(predicate: s => s.CastType == 1).ForEach(action: s =>
                    {
                        if (s.Type == (byte)CardType.Buff)
                        {
                            var b = new Buff.Buff(id: (short)s.SecondData, level: Monster.Level);
                            if (b.Card != null)
                                switch (b.Card?.BuffType)
                                {
                                    case BuffType.Bad:
                                        s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity);
                                        break;

                                    case BuffType.Good:
                                    case BuffType.Neutral:
                                        s.ApplyBCards(session: attackerBattleEntity, sender: attackerBattleEntity);
                                        break;
                                }
                        }
                    });

                    BattleEntity.BCards.Where(predicate: s => s.CastType == 0).ForEach(action: s =>
                    {
                        if (s.Type == (byte)CardType.Buff)
                        {
                            var b = new Buff.Buff(id: (short)s.SecondData, level: BattleEntity.Level);
                            if (b.Card != null)
                                switch (b.Card?.BuffType)
                                {
                                    case BuffType.Bad:
                                        s.ApplyBCards(session: attackerBattleEntity, sender: BattleEntity);
                                        break;

                                    case BuffType.Good:
                                    case BuffType.Neutral:
                                        if (s.NpcMonsterVNum == null || firstHit)
                                            s.ApplyBCards(session: BattleEntity, sender: BattleEntity);
                                        break;
                                }
                        }
                    });

                    hitRequest.SkillBCards.Where(predicate: s =>
                            s.Type.Equals(obj: (byte)CardType.Buff) &&
                            new Buff.Buff(id: (short)s.SecondData, level: hitRequest.Session.Character.Level).Card
                                ?.BuffType ==
                            BuffType.Bad).ToList()
                        .ForEach(action: s => s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity));

                    hitRequest.SkillBCards.Where(predicate: s => s.Type.Equals(obj: (byte)CardType.Capture)).ToList()
                        .ForEach(action: s => s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity));

                    hitRequest.SkillBCards.Where(predicate: s => s.Type.Equals(obj: (byte)CardType.SniperAttack))
                        .ToList()
                        .ForEach(action: s => s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity));

                    if (attackerBattleEntity.ShellWeaponEffects != null)
                        foreach (var shell in attackerBattleEntity.ShellWeaponEffects)
                            switch (shell.Effect)
                            {
                                case (byte)ShellWeaponEffectType.Blackout:
                                    {
                                        var buff = new Buff.Buff(id: 7, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.DeadlyBlackout:
                                    {
                                        var buff = new Buff.Buff(id: 66, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.MinorBleeding:
                                    {
                                        var buff = new Buff.Buff(id: 1, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.Bleeding:
                                    {
                                        var buff = new Buff.Buff(id: 21, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.HeavyBleeding:
                                    {
                                        var buff = new Buff.Buff(id: 42, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.Freeze:
                                    {
                                        var buff = new Buff.Buff(id: 27, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                            }

                    if (IsBoss && MapInstance == CaligorRaid.CaligorMapInstance)
                        switch (hitRequest.Session.Character.Faction)
                        {
                            case FactionType.Angel:
                                CaligorRaid.AngelDamage += damage;
                                if (onyxWings) CaligorRaid.AngelDamage += damage / 2;

                                break;

                            case FactionType.Demon:
                                CaligorRaid.DemonDamage += damage;
                                if (onyxWings) CaligorRaid.DemonDamage += damage / 2;

                                break;
                        }
                }

                if (isCaptureSkill) damage = 0;

                if (CurrentHp <= damage)
                    SetDeathStatement();
                else
                    BattleEntity.GetDamage(damage: damage, damager: attackerBattleEntity);

                // only set the hit delay if we become the monsters target with this hit
                if (Target == null) LastSkill = DateTime.Now;
                if (hitmode != 2)
                {
                    if (hitRequest.Skill != null)
                        switch (hitRequest.TargetHitType)
                        {
                            case TargetHitType.SingleTargetHit:
                                //if (!isCaptureSkill)
                                {
                                    BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                        type: attackerBattleEntity.UserType,
                                        callerId: attackerBattleEntity.MapEntityId,
                                        secondaryType: (byte)BattleEntity.UserType,
                                        targetId: BattleEntity.MapEntityId,
                                        skillVNum: hitRequest.Skill.SkillVNum,
                                        cooldown: (short)(hitRequest.Skill.Cooldown +
                                                           hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                        attackAnimation: hitRequest.Skill.AttackAnimation,
                                        skillEffect: hitRequest.SkillEffect,
                                        x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                        isAlive: IsAlive, health: (int)(CurrentHp / MaxHp * 100), damage: damage,
                                        hitmode: hitmode,
                                        skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                }

                                break;

                            case TargetHitType.SingleTargetHitCombo:
                                BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: attackerBattleEntity.UserType,
                                    callerId: attackerBattleEntity.MapEntityId,
                                    secondaryType: (byte)BattleEntity.UserType,
                                    targetId: BattleEntity.MapEntityId,
                                    skillVNum: hitRequest.Skill.SkillVNum,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.SkillCombo.Animation,
                                    skillEffect: hitRequest.SkillCombo.Effect,
                                    x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                    isAlive: IsAlive, health: (int)(CurrentHp / MaxHp * 100), damage: damage,
                                    hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                break;

                            case TargetHitType.SingleAoeTargetHit:
                                if (hitRequest.ShowTargetHitAnimation)
                                {
                                    if (hitRequest.Session.Character != null)
                                        if (hitRequest.Skill.SkillVNum == 1085 || hitRequest.Skill.SkillVNum == 1091 ||
                                            hitRequest.Skill.SkillVNum == 1060)
                                        {
                                            attackerBattleEntity.PositionX = MapX;
                                            attackerBattleEntity.PositionY = MapY;
                                            attackerBattleEntity.MapInstance.Broadcast(packet: hitRequest.Session
                                                .Character
                                                .GenerateTp());
                                        }

                                    BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                        type: attackerBattleEntity.UserType,
                                        callerId: attackerBattleEntity.MapEntityId,
                                        secondaryType: (byte)BattleEntity.UserType,
                                        targetId: BattleEntity.MapEntityId,
                                        skillVNum: hitRequest.Skill.SkillVNum,
                                        cooldown: (short)(hitRequest.Skill.Cooldown +
                                                           hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                        attackAnimation: hitRequest.Skill.AttackAnimation,
                                        skillEffect: hitRequest.SkillEffect,
                                        x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                        isAlive: IsAlive, health: (int)(CurrentHp / MaxHp * 100), damage: damage,
                                        hitmode: hitmode,
                                        skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                }
                                else
                                {
                                    switch (hitmode)
                                    {
                                        case 1:
                                        case 4:
                                            hitmode = 7;
                                            break;

                                        case 2:
                                            hitmode = 2;
                                            break;

                                        case 3:
                                            hitmode = 6;
                                            break;

                                        default:
                                            hitmode = 5;
                                            break;
                                    }

                                    BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                        type: attackerBattleEntity.UserType,
                                        callerId: attackerBattleEntity.MapEntityId,
                                        secondaryType: (byte)BattleEntity.UserType,
                                        targetId: BattleEntity.MapEntityId,
                                        skillVNum: -1,
                                        cooldown: (short)(hitRequest.Skill.Cooldown +
                                                           hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                        attackAnimation: hitRequest.Skill.AttackAnimation,
                                        skillEffect: hitRequest.SkillEffect,
                                        x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                        isAlive: IsAlive, health: (int)(CurrentHp / MaxHp * 100), damage: damage,
                                        hitmode: hitmode,
                                        skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                }

                                break;

                            case TargetHitType.AoeTargetHit:
                                switch (hitmode)
                                {
                                    case 1:
                                    case 4:
                                        hitmode = 7;
                                        break;

                                    case 2:
                                        hitmode = 2;
                                        break;

                                    case 3:
                                        hitmode = 6;
                                        break;

                                    default:
                                        hitmode = 5;
                                        break;
                                }

                                BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: attackerBattleEntity.UserType,
                                    callerId: attackerBattleEntity.MapEntityId,
                                    secondaryType: (byte)BattleEntity.UserType,
                                    targetId: BattleEntity.MapEntityId,
                                    skillVNum: hitRequest.Skill.SkillVNum,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.Skill.AttackAnimation,
                                    skillEffect: hitRequest.SkillEffect,
                                    x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                    isAlive: IsAlive, health: (int)(CurrentHp / MaxHp * 100), damage: damage,
                                    hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                break;

                            case TargetHitType.ZoneHit:
                                BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: attackerBattleEntity.UserType,
                                    callerId: attackerBattleEntity.MapEntityId,
                                    secondaryType: (byte)BattleEntity.UserType,
                                    targetId: BattleEntity.MapEntityId,
                                    skillVNum: hitRequest.Skill.SkillVNum,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.Skill.AttackAnimation,
                                    skillEffect: hitRequest.SkillEffect, x: hitRequest.MapX,
                                    y: hitRequest.MapY, isAlive: IsAlive, health: (int)(CurrentHp / MaxHp * 100),
                                    damage: damage, hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                break;

                            case TargetHitType.SpecialZoneHit:
                                BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: attackerBattleEntity.UserType,
                                    callerId: attackerBattleEntity.MapEntityId,
                                    secondaryType: (byte)BattleEntity.UserType,
                                    targetId: BattleEntity.MapEntityId,
                                    skillVNum: hitRequest.Skill.SkillVNum,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.Skill.AttackAnimation,
                                    skillEffect: hitRequest.SkillEffect,
                                    x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                    isAlive: IsAlive, health: (int)(CurrentHp / MaxHp * 100), damage: damage,
                                    hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                break;
                        }
                    else
                        MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(type: attackerBattleEntity.UserType,
                            callerId: attackerBattleEntity.MapEntityId, secondaryType: (byte)BattleEntity.UserType,
                            targetId: BattleEntity.MapEntityId,
                            skillVNum: 0,
                            cooldown: (short)(hitRequest.Mate != null ? hitRequest.Mate.Monster.BasicCooldown : 12),
                            attackAnimation: 11,
                            skillEffect: (short)(hitRequest.Mate != null ? hitRequest.Mate.Monster.BasicSkill : 12),
                            x: 0, y: 0, isAlive: IsAlive,
                            health: (int)(CurrentHp / MaxHp * 100), damage: damage, hitmode: hitmode, skillType: 0));
                }
                else
                {
                    hitRequest.Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: MapMonsterId));
                }

                if (attackerBattleEntity.Character != null)
                {
                    if (hitmode != 4 && hitmode != 2 && damage > 0)
                        attackerBattleEntity.Character.RemoveBuffByBCardTypeSubType(
                            bcardTypes: new List<KeyValuePair<byte, byte>>
                            {
                                new KeyValuePair<byte, byte>(key: (byte) CardType.SpecialActions,
                                    value: (byte) AdditionalTypes.SpecialActions.Hide)
                            });

                    if (attackerBattleEntity.HasBuff(type: CardType.FalconSkill,
                        subtype: (byte)AdditionalTypes.FalconSkill.Hide))
                    {
                        attackerBattleEntity.Character.RemoveBuffByBCardTypeSubType(
                            bcardTypes: new List<KeyValuePair<byte, byte>>
                            {
                                new KeyValuePair<byte, byte>(key: (byte) CardType.FalconSkill,
                                    value: (byte) AdditionalTypes.FalconSkill.Hide)
                            });
                        attackerBattleEntity.AddBuff(
                            indicator: new Buff.Buff(id: 560, level: attackerBattleEntity.Level),
                            sender: attackerBattleEntity);
                    }
                }

                if (CurrentHp <= 0 && !isCaptureSkill)
                {
                    // generate the kill bonus
                    hitRequest.Session.Character.GenerateKillBonus(monsterToAttack: this, Killer: attackerBattleEntity);

                    if (attackerBattleEntity.Character != null && hitRequest.SkillBCards.FirstOrDefault(predicate: s =>
                        s.Type == (byte)CardType.TauntSkill &&
                        s.SubType == (byte)AdditionalTypes.TauntSkill.EffectOnKill / 10) is { } effectOnKill)
                        if (ServerManager.RandomNumber() < effectOnKill.FirstData)
                            attackerBattleEntity.AddBuff(
                                indicator: new Buff.Buff(id: (short)effectOnKill.SecondData,
                                    level: attackerBattleEntity.Level),
                                sender: attackerBattleEntity);
                }
            }
            else
            {
                // monster already has been killed, send cancel
                hitRequest.Session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: MapMonsterId));
            }

            if (IsBoss) MapInstance.Broadcast(packet: GenerateBoss());
        }

        /// <summary>
        ///     Handle any kind of Monster interaction
        /// </summary>
        void MonsterLife()
        {
            lock (OnHitLockObject)
            {
                if (Monster == null) return;

                if (MonsterVNum == 2305 && MapInstance.MapInstanceType == MapInstanceType.CaligorInstance
                                        && RunToX == 0 && RunToY == 0
                                        && Map.GetDistance(p: new MapCell { X = FirstX, Y = FirstY },
                                            q: new MapCell { X = MapX, Y = MapY }) > 30)
                {
                    RunToX = FirstX;
                    RunToY = FirstY;
                }

                if ((DateTime.Now - LastEffect).TotalSeconds >= 5)
                {
                    LastEffect = DateTime.Now;
                    if (IsTarget)
                        MapInstance.Broadcast(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Monster,
                            callerId: MapMonsterId, effectId: 824));

                    if (IsBonus)
                        MapInstance.Broadcast(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Monster,
                            callerId: MapMonsterId, effectId: 826));
                }

                if (IsBoss && IsAlive) MapInstance.Broadcast(packet: GenerateBoss());

                // handle hit queue
                while (HitQueue.TryDequeue(result: out var hitRequest)) OnReceiveHit(hitRequest: hitRequest);

                lock (DamageList)
                {
                    // Check DamageList members on map and entity (Character or Mate or Mob or Npc not null)
                    var newDamageList = new Dictionary<BattleEntity, long>();
                    DamageList.Where(predicate: s => s.Key.HasEntity && s.Key.MapInstance == MapInstance).ToList()
                        .ForEach(action: s => newDamageList.Add(key: s.Key, value: s.Value));
                    DamageList = newDamageList;
                }

                lock (AggroList)
                {
                    var newAggroList = new List<BattleEntity>();
                    AggroList.ToList().Where(predicate: s => s.HasEntity && s.MapInstance == MapInstance).ToList()
                        .ForEach(action: s => newAggroList.Add(item: s));
                    AggroList = newAggroList;
                }

                GetNearestOponent();

                // Respawn
                if (!IsAlive && ShouldRespawn != null && !ShouldRespawn.Value)
                    MapInstance.RemoveMonster(monsterToRemove: this);

                if (!IsAlive && ShouldRespawn != null && ShouldRespawn.Value)
                {
                    var timeDeath = (DateTime.Now - Death).TotalSeconds;
                    if (timeDeath >= Monster.RespawnTime / 10d) Respawn();
                }

                else if (IsSelfAttack)
                {
                    if (!IsPendingDelete && MapInstance != null && Skills != null)
                    {
                        TargetHit(target: BattleEntity, npcMonsterSkill: Skills.FirstOrDefault());

                        IsPendingDelete = true;

                        Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1))
                            .Subscribe(onNext: observable =>
                            {
                                RunDeathEvent();
                                MapInstance?.RemoveMonster(monsterToRemove: this);
                                MapInstance?.Broadcast(packet: StaticPacketHelper.Die(callerType: UserType.Monster,
                                    callerId: MapMonsterId,
                                    targetType: UserType.Monster, targetId: MapMonsterId));
                            });
                    }
                }

                // normal movement
                else if (Target == null)
                {
                    Move();
                }

                // target following
                else if (MapInstance != null)
                {
                    GetNearestOponent(); //MobbAll Activate
                    HostilityTarget();
                    NpcMonsterSkill npcMonsterSkill = null;
                    BattleEntity targetEntity = null;

                    switch (Target?.EntityType)
                    {
                        case EntityType.Player:
                            {
                                if (MapInstance.GetSessionByCharacterId(characterId: Target.MapEntityId) is { }
                                    targetSession)
                                    targetEntity = targetSession.Character?.BattleEntity;
                            }
                            break;

                        case EntityType.Mate:
                            {
                                var mate = MapInstance.Sessions.SelectMany(selector: x => x.Character.Mates)
                                    .FirstOrDefault(predicate: s =>
                                        (s.IsTeamMember || s.IsTemporalMate) &&
                                        s.BattleEntity.MapEntityId == Target.MapEntityId);

                                if (mate != null) targetEntity = mate.BattleEntity;
                            }
                            break;

                        case EntityType.Monster:
                            {
                                if (MapInstance.GetMonsterById(mapMonsterId: Target.MapEntityId) is { } mapMonster)
                                    targetEntity = mapMonster.BattleEntity;
                            }
                            break;

                        case EntityType.Npc:
                            {
                                if (MapInstance.GetNpc(mapNpcId: Target.MapEntityId) is { } mapNpc)
                                    targetEntity = mapNpc.BattleEntity;
                            }
                            break;
                    }

                    // remove target in some situations
                    if (targetEntity == null
                        || targetEntity.Hp <= 0
                        || CurrentHp <= 0
                        || targetEntity.MapInstance != MapInstance
                        || targetEntity.Character != null
                        && (targetEntity.Character.Invisible || targetEntity.Character.InvisibleGm)
                        && !CanSeeHiddenThings)
                    {
                        RemoveTarget();
                        return;
                    }

                    if (HasBuff(type: CardType.SpecialActions, subtype: (byte)AdditionalTypes.SpecialActions.RunAway))
                    {
                        RunAway();
                        return;
                    }

                    var instantAttack = MonsterVNum == 1439 || MonsterVNum == 1436 || MonsterVNum == 946 ||
                                        MonsterVNum == 1382;

                    if ((DateTime.Now - LastSkill).TotalMilliseconds >=
                        1100 + (instantAttack ? 0 : Monster.BasicCooldown * 200))
                    {
                        if (Skills != null)
                        {
                            var specialSkillsRate = ServerManager.RandomNumber(min: 0, max: 12);

                            if (ServerManager.Instance.BossVNums.Contains(item: MonsterVNum))
                                specialSkillsRate = ServerManager.RandomNumber(min: 0, max: 30);

                            #region UseSkillOnDamage

                            if (UseSkillOnDamage != null)
                            {
                                var useSkillOnDamage =
                                    UseSkillOnDamage.FirstOrDefault(predicate: u => HpPercent() <= u.HpPercent);

                                if (useSkillOnDamage != null)
                                {
                                    UseSkillOnDamage.Remove(item: useSkillOnDamage);

                                    npcMonsterSkill =
                                        Skills.FirstOrDefault(predicate: s =>
                                            s.SkillVNum == useSkillOnDamage.SkillVNum);

                                    if (npcMonsterSkill != null)
                                        TargetHit(target: targetEntity, npcMonsterSkill: npcMonsterSkill);

                                    return;
                                }
                            }

                            #endregion

                            var possibleSkills = Skills.Where(predicate: s =>
                                !SkillHelper.IsManagedSkill(skillVNum: s.SkillVNum) &&
                                ((DateTime.Now - s.LastSkillUse).TotalMilliseconds >= 100 * s.Skill.Cooldown ||
                                 s.Rate == 0 || instantAttack)).ToList();

                            foreach (var skill in possibleSkills.OrderBy(keySelector: rnd =>
                                ServerManager.RandomNumber()))
                                if (skill.Rate == 0 || instantAttack)
                                {
                                    if (skill.SkillVNum == 1226 && _previousSkillVNum != 1224 ||
                                        skill.SkillVNum == 1256 && _previousSkillVNum != 1255) continue;

                                    npcMonsterSkill = skill;
                                }
                                else if (ServerManager.RandomNumber() < skill.Rate && specialSkillsRate > 8)
                                {
                                    if (skill.SkillVNum != 1226 && _previousSkillVNum == 1224 ||
                                        skill.SkillVNum != 1256 && _previousSkillVNum == 1255) continue;

                                    npcMonsterSkill = skill;

                                    break;
                                }
                        }

                        if (_previousSkillVNum == 1255 && LastSkill.AddSeconds(value: 10) > DateTime.Now
                        ) //Glacerus wait after storm skill)
                            return;

                        if ((DateTime.Now - LastMove).TotalMilliseconds >= Speed * 100)
                        {
                            if (npcMonsterSkill?.Skill.TargetType == 1 && npcMonsterSkill?.Skill.HitType == 0)
                            {
                                TargetHit(target: targetEntity, npcMonsterSkill: npcMonsterSkill);
                                return;
                            }

                            if (npcMonsterSkill?.Skill.TargetType == 1 && npcMonsterSkill?.Skill.HitType == 2)
                            {
                                TargetHit(target: BattleEntity, npcMonsterSkill: npcMonsterSkill);
                                return;
                            }

                            if ((targetEntity.Character == null || !targetEntity.Character.InvisibleGm ||
                                 !targetEntity.Character.Invisible || CanSeeHiddenThings) && targetEntity.Hp > 0)
                            {
                                if (npcMonsterSkill?.Skill.TargetType == 1 && npcMonsterSkill?.Skill.HitType == 1 &&
                                    npcMonsterSkill?.Skill.TargetRange > 0
                                    && CurrentMp >= npcMonsterSkill.Skill.MpCost && Map.GetDistance(
                                        p: new MapCell
                                        {
                                            X = MapX,
                                            Y = MapY
                                        },
                                        q: new MapCell
                                        {
                                            X = targetEntity.PositionX,
                                            Y = targetEntity.PositionY
                                        }) <= npcMonsterSkill.Skill.TargetRange)
                                {
                                    TargetHit(target: targetEntity, npcMonsterSkill: npcMonsterSkill);
                                    return;
                                }

                                if (npcMonsterSkill != null && CurrentMp >= npcMonsterSkill.Skill.MpCost &&
                                    Map.GetDistance(
                                        p: new MapCell
                                        {
                                            X = MapX,
                                            Y = MapY
                                        },
                                        q: new MapCell
                                        {
                                            X = targetEntity.PositionX,
                                            Y = targetEntity.PositionY
                                        }) <= (npcMonsterSkill.Skill.Range > 0
                                        ? npcMonsterSkill.Skill.Range
                                        : Monster.BasicRange))
                                {
                                    TargetHit(target: targetEntity, npcMonsterSkill: npcMonsterSkill);
                                    return;
                                }

                                if (Map.GetDistance(p: new MapCell
                                {
                                    X = MapX,
                                    Y = MapY
                                },
                                    q: new MapCell
                                    {
                                        X = targetEntity.PositionX,
                                        Y = targetEntity.PositionY
                                    }) <= Monster.BasicRange && Monster.BasicRange > 0)
                                {
                                    TargetHit(target: targetEntity, npcMonsterSkill: null);
                                    return;
                                }
                            }
                        }
                    }

                    FollowTarget(target: targetEntity);
                }
            }
        }

        public void LoadSpeed()
        {
            Speed = Monster.Speed;

            var fixSpeed = (byte)GetBuff(type: CardType.Move, subtype: (byte)AdditionalTypes.Move.SetMovement)[0];
            if (fixSpeed != 0)
            {
                Speed = fixSpeed;
            }
            else
            {
                Speed += (byte)GetBuff(type: CardType.Move,
                    subtype: (byte)AdditionalTypes.Move.MovementSpeedIncreased)[0];
                if (Speed == 59) Speed = 1;
                Speed = (byte)(Speed * (1 + GetBuff(type: CardType.Move,
                        subtype: (byte)AdditionalTypes.Move.MoveSpeedIncreased)[0] /
                    100D));
            }
        }

        void Move()
        {
            MoveTest();

            /*
            if (Monster != null && IsAlive && !HasBuff(CardType.Move, (byte)AdditionalTypes.Move.MovementImpossible) && IsMoving && Monster.Speed > 0)
            {
                if (!Path.Any() && (DateTime.Now - LastMove).TotalMilliseconds > _movetime && Target == null) // Basic Move
                {
                    Path = BestFirstSearch.FindPathJagged(new Node { X = MapX, Y = MapY }, new Node { X = (short)(FirstX + ServerManager.RandomNumber(-2, 2)), Y = (short)(FirstY + ServerManager.RandomNumber(-2, 2)) }, MapInstance.Map.JaggedGrid);
                }
                else if (DateTime.Now > LastMove && Path.Any()) // Follow target || move back to original pos
                {
                    LoadSpeed();
                    byte speedIndex = (byte)(Speed / 2.5 < 1 ? 1 : Speed / 2.5);
                    int maxindex = Path.Count > speedIndex ? speedIndex : Path.Count;
                    short smapX = (short)ServerManager.RandomNumber(Path[maxindex - 1].X - 1, Path[maxindex - 1].X + 1);
                    short smapY = (short)_random.Next(Path[maxindex - 1].Y - 1, Path[maxindex - 1].Y + 1);
                    if (MapInstance.Map.IsBlockedZone(smapX, smapY))
                    {
                        smapX = Path[maxindex - 1].X;
                        smapY = Path[maxindex - 1].Y;
                    }

                    if (Target == null || Map.GetDistance(new MapCell { X = smapX, Y = smapY }, new MapCell { X = Target.PositionX, Y = Target.PositionY }) >= Monster.BasicRange - 1)
                    {
                        double waitingtime = Map.GetDistance(new MapCell { X = smapX, Y = smapY }, GetPos()) / (double)Speed;
                        MapInstance.Broadcast(new BroadcastPacket(null, $"mv 3 {MapMonsterId} {smapX} {smapY} {Speed}", ReceiverType.All, xCoordinate: smapX, yCoordinate: smapY));
                        LastMove = DateTime.Now.AddSeconds(waitingtime > 1 ? 1 : waitingtime);
                        Observable.Timer(TimeSpan.FromMilliseconds((int)((waitingtime > 1 ? 1 : waitingtime) * 1000))).Subscribe(x =>
                        {
                            MapX = smapX;
                            MapY = smapY;
                        });
                    }

                    if (Target != null && (int)Path[0].F > _maxDistance) // Remove Target if distance between target & monster is > max Distance
                    {
                        RemoveTarget();
                        return;
                    }
                    Path.RemoveRange(0, maxindex);
                }
            }
            HostilityTarget();
            */
        }

        public void MoveTest()
        {
            var walkWaitTime =
                (Target == null && RunToX == 0 && RunToY == 0 ? ServerManager.RandomNumber(min: 400, max: 3200) : 0) +
                Speed / 1.5f * 100 - (DateTime.Now - LastMove).TotalMilliseconds;
            double skillWaitTime = 0 /*800 - (DateTime.Now - LastSkill).TotalMilliseconds*/;
            if (Monster != null && !IsDisabled && !IsJumping && IsAlive &&
                !HasBuff(type: CardType.Move, subtype: (byte)AdditionalTypes.Move.MovementImpossible) && IsMoving &&
                Monster.Speed > 0 && walkWaitTime <= 0 && skillWaitTime <= 0)
            {
                LoadSpeed();

                LastMove = DateTime.Now;

                var moveToPosition = new MapCell { X = FirstX, Y = FirstY };

                if (RunToX != 0 || RunToY != 0) moveToPosition = new MapCell { X = RunToX, Y = RunToY };
                if (Target != null && (MonsterVNum != 2305 || RunToX == 0 && RunToY == 0))
                    moveToPosition = new MapCell { X = Target.PositionX, Y = Target.PositionY };

                var nextStep = Map.GetNextStep(start: new MapCell { X = MapX, Y = MapY },
                    end: new MapCell
                    {
                        X = (short)ServerManager.RandomNumber(min: moveToPosition.X - 3, max: moveToPosition.X + 3),
                        Y = (short)ServerManager.RandomNumber(min: moveToPosition.Y - 3, max: moveToPosition.Y + 3)
                    }, steps: Speed / 2.5f);

                short tries = 0;
                short maxTries = 15;

                while (MapInstance.Map.IsBlockedZone(firstX: MapX, firstY: MapY, mapX: nextStep.X, mapY: nextStep.Y) &&
                       tries < maxTries)
                {
                    nextStep = Map.GetNextStep(start: new MapCell { X = MapX, Y = MapY },
                        end: new MapCell
                        {
                            X = (short)ServerManager.RandomNumber(min: moveToPosition.X - 3,
                                max: moveToPosition.X + 3),
                            Y = (short)ServerManager.RandomNumber(min: moveToPosition.Y - 3, max: moveToPosition.Y + 3)
                        }, steps: Speed / 2.5f);
                    tries++;
                }

                if (tries < maxTries)
                {
                    MapInstance.Broadcast(packet: new BroadcastPacket(session: null,
                        packet: $"mv 3 {MapMonsterId} {nextStep.X} {nextStep.Y} {Math.Round(a: Speed * 1.5f)}",
                        receiver: ReceiverType.All,
                        xCoordinate: MapX, yCoordinate: MapY));
                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: Math.Round(a: Speed * 1.5f) * 10))
                        .Subscribe(onNext: s =>
                        {
                            MapX = nextStep.X;
                            MapY = nextStep.Y;
                        });
                }
                else
                {
                    //Generate Coords 
                    bool notwalkable;
                    short genX, genY;
                    tries = 0;
                    do
                    {
                        genX = MapX;
                        genY = MapY;

                        var xMin = ServerManager.RandomNumber(min: 0, max: 10);
                        var yMin = ServerManager.RandomNumber(min: 0, max: 10);

                        if (xMin > 6)
                            genX -= 3;
                        else if (xMin < 4) genX += 3;
                        if (yMin > 6)
                            genY -= 3;
                        else if (yMin < 4) genY += 3;

                        notwalkable =
                            MapInstance.Map.IsBlockedZone(firstX: MapX, firstY: MapY, mapX: genX, mapY: genY);

                        if (Map.GetDistance(p: new MapCell { X = MapX, Y = MapY },
                            q: new MapCell { X = genX, Y = genY }) > 3)
                            notwalkable = true;
                        if (MapX == genX && MapY == genY) notwalkable = true;
                        tries++;
                    } while (notwalkable && tries < maxTries);

                    if (!notwalkable)
                    {
                        MapInstance.Broadcast(packet: new BroadcastPacket(session: null,
                            packet: $"mv 3 {MapMonsterId} {genX} {genY} {Math.Round(a: Speed * 1.3f)}",
                            receiver: ReceiverType.All,
                            xCoordinate: MapX, yCoordinate: MapY));
                        Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: Math.Round(a: Speed * 1.3f) * 10))
                            .Subscribe(onNext: s =>
                            {
                                MapX = genX;
                                MapY = genY;
                            });
                    }
                }

                if (Map.GetDistance(p: new MapCell { X = MapX, Y = MapY }, q: new MapCell { X = RunToX, Y = RunToY }) < 2)
                {
                    RunToX = 0;
                    RunToY = 0;
                }

                if (Map.GetDistance(p: new MapCell { X = MapX, Y = MapY },
                        q: new MapCell { X = moveToPosition.X, Y = moveToPosition.Y }) > MaxDistance &&
                    LastSkill.AddMilliseconds(value: TimeAgroLoss) <= DateTime.Now
                    || MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance &&
                    LastSkill.AddMilliseconds(value: 20000) <= DateTime.Now &&
                    LastMonsterAggro.AddMilliseconds(value: 20000) <= DateTime.Now)
                    RemoveTarget();
            }

            HostilityTarget();
        }

        void Respawn()
        {
            if (Monster != null)
            {
                DisableBuffs(type: BuffType.All);

                lock (DamageList)
                {
                    DamageList = new Dictionary<BattleEntity, long>();
                }

                lock (AggroList)
                {
                    AggroList = new List<BattleEntity>();
                }

                IsAlive = true;
                Target = null;
                MaxHp = BaseMaxHp;
                MaxMp = BaseMaxMp;
                CurrentHp = MaxHp;
                CurrentMp = MaxMp;
                MapX = FirstX;
                MapY = FirstY;
                if (MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance &&
                    ServerManager.Instance.MapBossVNums.Contains(item: MonsterVNum))
                {
                    var randomCell = MapInstance.Map.GetRandomPosition();
                    if (randomCell != null)
                    {
                        MapX = randomCell.X;
                        MapY = randomCell.Y;
                    }
                }

                Path = new List<Node>();
                MapInstance.Broadcast(packet: GenerateIn());
                Monster.BCards.Where(predicate: s => s.Type != 25).ToList()
                    .ForEach(action: s => s.ApplyBCards(session: BattleEntity, sender: BattleEntity));
            }
        }

        /// <summary>
        ///     Hit the Target.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="npcMonsterSkill"></param>
        void TargetHit(BattleEntity target, NpcMonsterSkill npcMonsterSkill)
        {
            if (Monster != null && !HasBuff(type: CardType.SpecialAttack,
                subtype: (byte)AdditionalTypes.SpecialAttack.NoAttack))
            {
                var castTime = 0;
                if (npcMonsterSkill != null)
                {
                    if (CurrentMp < npcMonsterSkill.Skill.MpCost)
                    {
                        FollowTarget(target: target);
                        return;
                    }

                    _previousSkillVNum = npcMonsterSkill.SkillVNum;

                    npcMonsterSkill.LastSkillUse = DateTime.Now;
                    DecreaseMp(amount: npcMonsterSkill.Skill.MpCost);
                    MapInstance.Broadcast(packet: StaticPacketHelper.CastOnTarget(attackerType: UserType.Monster,
                        attackerId: MapMonsterId,
                        defenderType: target.UserType, defenderId: target.MapEntityId,
                        castAnimation: npcMonsterSkill.Skill.CastAnimation,
                        castEffect: npcMonsterSkill.Skill.CastEffect,
                        skillVNum: npcMonsterSkill.Skill.SkillVNum));
                    if (npcMonsterSkill.Skill.CastEffect != 0)
                    {
                        MapInstance.Broadcast(
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Monster, callerId: MapMonsterId,
                                effectId: npcMonsterSkill.Skill.CastEffect), xRangeCoordinate: MapX,
                            yRangeCoordinate: MapY);
                        castTime = npcMonsterSkill.Skill.CastTime * 100;
                    }

                    if (npcMonsterSkill.Skill.CastEffect == 4657 || npcMonsterSkill.Skill.CastEffect == 4940)
                    {
                        var possibleTargets = MapInstance.BattleEntities
                            .OrderBy(keySelector: e => Map.GetDistance(p: GetPos(), q: e.GetPos()))
                            .Where(predicate: e => e.UserType == UserType.Player && e.Character != null &&
                                                   BattleEntity.CanAttackEntity(receiver: e) &&
                                                   !e.Character.InvisibleGm &&
                                                   (!e.Character.Invisible || CanSeeHiddenThings) &&
                                                   Map.GetDistance(p: GetPos(), q: e.GetPos()) <
                                                   npcMonsterSkill.Skill.Range)
                            .ToList();
                        Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: castTime > 200 ? 200 : 0))
                            .Subscribe(onNext: obs =>
                            {
                                MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                                    callerId: MapMonsterId));
                            });
                        IsJumping = true;
                        if (possibleTargets.Count > 0)
                        {
                            var chosenTarget = possibleTargets.OrderBy(keySelector: s => ServerManager.RandomNumber())
                                .FirstOrDefault();
                            if (chosenTarget != null)
                            {
                                MapX = chosenTarget.PositionX;
                                MapY = chosenTarget.PositionY;
                                MapInstance.Broadcast(packet: StaticPacketHelper.GenerateEff(
                                    effectType: chosenTarget.UserType,
                                    callerId: chosenTarget.MapEntityId, effectId: 4660));
                                possibleTargets.Except(second: new List<BattleEntity> { chosenTarget }).ToList().ForEach(
                                    action: s =>
                                        MapInstance.Broadcast(packet: StaticPacketHelper.GenerateEff(
                                            effectType: s.UserType,
                                            callerId: s.MapEntityId, effectId: 4660)));
                            }
                        }

                        Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: castTime > 100 ? castTime - 100 : 0))
                            .Subscribe(
                                onNext: obs =>
                                {
                                    IsJumping = false;
                                    MapInstance.Broadcast(packet: GenerateIn());
                                });
                    }

                    if (npcMonsterSkill.Skill.CastEffect == 4426)
                    {
                        var possibleTargets = MapInstance.BattleEntities
                            .OrderBy(keySelector: e => Map.GetDistance(p: GetPos(), q: e.GetPos()))
                            .Where(predicate: e => e.UserType == UserType.Player && e.Character != null &&
                                                   BattleEntity.CanAttackEntity(receiver: e) &&
                                                   !e.Character.InvisibleGm &&
                                                   (!e.Character.Invisible || CanSeeHiddenThings) &&
                                                   Map.GetDistance(p: GetPos(), q: e.GetPos()) <
                                                   npcMonsterSkill.Skill.Range)
                            .ToList();
                        Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: castTime > 200 ? 200 : 0))
                            .Subscribe(onNext: obs =>
                            {
                                MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                                    callerId: MapMonsterId));
                            });
                        IsJumping = true;
                        if (possibleTargets.Count > 0)
                        {
                            var chosenTarget = possibleTargets.OrderBy(keySelector: s => ServerManager.RandomNumber())
                                .FirstOrDefault();

                            Observable.Timer(
                                    dueTime: TimeSpan.FromMilliseconds(value: castTime > 2000 ? castTime - 2000 : 0))
                                .Subscribe(onNext: obs =>
                                {
                                    if (chosenTarget != null)
                                    {
                                        MapX = chosenTarget.PositionX;
                                        MapY = chosenTarget.PositionY;
                                    }

                                    MapInstance.Broadcast(packet: $"eff_g  4431 0 {MapX} {MapY} 0");
                                });

                            possibleTargets.Except(second: new List<BattleEntity> { chosenTarget }).ToList().ForEach(
                                action: s =>
                                {
                                    MapInstance.SummonMonster(summon: new MonsterToSummon(vnum: 2048,
                                        spawnCell: new MapCell { X = s.PositionX, Y = s.PositionY }, target: null,
                                        move: false));
                                });
                        }

                        Observable.Timer(
                                dueTime: TimeSpan.FromMilliseconds(
                                    value: (castTime > 2000 ? castTime - 2000 : 0) + 1900))
                            .Subscribe(onNext: obs2 =>
                            {
                                IsJumping = false;
                                MapInstance.Broadcast(packet: GenerateIn());
                                MapInstance.Broadcast(packet: $"eff_g  4425 0 {MapX} {MapY} 0");
                            });
                    }

                    if (npcMonsterSkill.SkillVNum == 1226)
                    {
                        AddBuff(indicator: new Buff.Buff(id: 529, level: BattleEntity.Level),
                            battleEntity: BattleEntity,
                            forced: true);
                        AddBuff(indicator: new Buff.Buff(id: 530, level: BattleEntity.Level),
                            battleEntity: BattleEntity,
                            forced: true);
                    }
                }

                LastMove = DateTime.Now;

                LastSkill = DateTime.Now;

                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: castTime)).Subscribe(onNext: obs =>
                {
                    var hitmode = 0;
                    var onyxWings = false;
                    BattleEntity targetEntity = null;
                    switch (target.EntityType)
                    {
                        case EntityType.Player:
                            targetEntity = new BattleEntity(character: target.Character, skill: null);
                            break;
                        case EntityType.Mate:
                            targetEntity = new BattleEntity(mate: target.Mate);
                            break;
                        case EntityType.Monster:
                            targetEntity = new BattleEntity(monster: target.MapMonster);
                            break;
                        case EntityType.Npc:
                            targetEntity = new BattleEntity(npc: target.MapNpc);
                            break;
                    }

                    var damage = DamageHelper.Instance.CalculateDamage(attacker: new BattleEntity(monster: this),
                        defender: targetEntity, skill: npcMonsterSkill?.Skill, hitMode: ref hitmode,
                        onyxWings: ref onyxWings);

                    // deal 0 damage to GM with GodMode
                    if (target.Character != null && target.Character.HasGodMode ||
                        target.Mate != null && target.Mate.Owner.HasGodMode) damage = 0;

                    if (target.Character != null)
                        if (ServerManager.RandomNumber() < target.Character.GetBuff(type: CardType.DarkCloneSummon,
                            subtype: (byte)AdditionalTypes.DarkCloneSummon.ConvertDamageToHpChance)[0])
                        {
                            var amount = damage;

                            target.Character.ConvertedDamageToHP += amount;
                            target.Character.MapInstance?.Broadcast(
                                packet: target.Character.GenerateRc(characterHealth: amount));
                            target.Character.Hp += amount;

                            if (target.Character.Hp > target.Character.HPLoad())
                                target.Character.Hp = (int)target.Character.HPLoad();

                            target.Character.Session?.SendPacket(packet: target.Character.GenerateStat());

                            damage = 0;
                        }

                    var manaShield = target.GetBuff(type: CardType.LightAndShadow,
                        subtype: (byte)AdditionalTypes.LightAndShadow.InflictDamageToMp);
                    if (manaShield[0] != 0 && hitmode != 4)
                    {
                        var reduce = damage / 100 * manaShield[0];
                        if (target.Mp < reduce)
                        {
                            reduce = target.Mp;
                            target.Mp = 0;
                        }
                        else
                        {
                            target.DecreaseMp(amount: reduce);
                        }

                        damage -= reduce;
                    }

                    if (target.Character != null && target.Character.IsSitting)
                    {
                        target.Character.IsSitting = false;
                        MapInstance.Broadcast(packet: target.Character.GenerateRest());
                    }

                    TargetHit2(target: target, npcMonsterSkill: npcMonsterSkill, damage: damage, hitmode: hitmode);

                    if (Owner?.Mate != null || MonsterHelper.IsKamikaze(monsterVNum: MonsterVNum))
                    {
                        double ms = MonsterVNum >= 2112 && MonsterVNum <= 2115 ? 1000 : 250;

                        Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: ms)).Subscribe(onNext: t =>
                        {
                            RunDeathEvent();
                            MapInstance.RemoveMonster(monsterToRemove: this);
                            MapInstance.Broadcast(packet: StaticPacketHelper.Die(callerType: UserType.Monster,
                                callerId: MapMonsterId,
                                targetType: UserType.Monster, targetId: MapMonsterId));
                        });
                    }
                });
            }
        }

        public void TargetHit2(BattleEntity target, NpcMonsterSkill npcMonsterSkill, int damage, int hitmode)
        {
            var attackGreaterDistance = false;
            var bCards = new List<BCard>();
            bCards.AddRange(collection: Monster.BCards.ToList());
            var skillRange = Monster.BasicRange;
            var skillTargetRange = Monster.BasicRange;
            if (npcMonsterSkill != null)
            {
                if (npcMonsterSkill.SkillVNum == 1321) hitmode = 0;

                skillRange = npcMonsterSkill.Skill.Range;
                skillTargetRange = npcMonsterSkill.Skill.TargetRange;
                bCards.AddRange(collection: npcMonsterSkill.Skill.BCards.ToList());
                if (npcMonsterSkill.Skill.TargetType == 1 && npcMonsterSkill.Skill.HitType == 1 &&
                    npcMonsterSkill.Skill.TargetRange == 0 && npcMonsterSkill.Skill.Range > 0)
                    if (npcMonsterSkill.SkillVNum != 1207)
                        attackGreaterDistance = true;
                if (npcMonsterSkill.Skill.CastEffect == 4657 || npcMonsterSkill.Skill.CastEffect == 4940)
                    skillRange = 3;
                if (npcMonsterSkill.Skill.TargetType == 0 && npcMonsterSkill.Skill.HitType == 0)
                {
                    skillRange = npcMonsterSkill.Skill.TargetRange;
                    skillTargetRange = 0;
                }

                if (npcMonsterSkill.Skill.TargetType == 1 && npcMonsterSkill.Skill.HitType == 1)
                {
                    skillRange = npcMonsterSkill.Skill.TargetRange;
                    skillTargetRange = 0;
                }

                if (attackGreaterDistance ||
                    npcMonsterSkill.Skill.TargetType == 0 && npcMonsterSkill.Skill.HitType == 1)
                {
                    skillRange = npcMonsterSkill.Skill.TargetRange;
                    skillTargetRange = npcMonsterSkill.Skill.Range;
                }

                if (npcMonsterSkill.Skill.TargetType == 1 && npcMonsterSkill.Skill.HitType == 2) // Area Buff
                {
                    MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: UserType.Monster,
                        callerId: MapMonsterId,
                        secondaryType: (byte)BattleEntity.UserType, targetId: MapMonsterId,
                        skillVNum: npcMonsterSkill.SkillVNum, cooldown: npcMonsterSkill.Skill.Cooldown,
                        attackAnimation: npcMonsterSkill.Skill.AttackAnimation,
                        skillEffect: npcMonsterSkill.Skill.Effect, x: MapX, y: MapY,
                        isAlive: CurrentHp > 0,
                        health: (int)(CurrentHp / MaxHp * 100), damage: 0,
                        hitmode: 0, skillType: 0));

                    MapInstance.GetBattleEntitiesInRange(pos: new MapCell { X = MapX, Y = MapY },
                            distance: skillTargetRange)
                        .Where(predicate: s => !BattleEntity.CanAttackEntity(receiver: s))
                        .ToList().ForEach(action: s =>
                            npcMonsterSkill.Skill.BCards.ForEach(action: b =>
                                b.ApplyBCards(session: s, sender: BattleEntity, x: MapX, y: MapY)));
                    return;
                }

                if (npcMonsterSkill.SkillVNum == 1215)
                {
                    var recoverHp = 0;
                    MapInstance.BattleEntities
                        .Where(predicate: s =>
                            !BattleEntity.CanAttackEntity(receiver: s) && s.MapMonster?.MonsterVNum == 2017)
                        .ToList().ForEach(action: s =>
                        {
                            recoverHp += s.Hp;
                            s.MapMonster.RunDeathEvent();
                            MapInstance.RemoveMonster(monsterToRemove: s.MapMonster);
                            MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                                callerId: s.MapMonster.MapMonsterId));
                        });
                    if (CurrentHp + recoverHp > MaxHp) recoverHp = (int)(MaxHp - CurrentHp);
                    if (recoverHp > 0)
                    {
                        MapInstance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(message: string.Format(
                            format: Language.Instance.GetMessageFromKey(key: "EARNED_VITALLITY"), arg0: Monster.Name,
                            arg1: ServerManager.GetNpcMonster(npcVNum: 2017).Name), type: 0));
                        CurrentHp += recoverHp;
                        MapInstance.Broadcast(packet: BattleEntity.GenerateRc(characterHealth: recoverHp));
                    }
                }
            }

            lock (target.PVELockObject)
            {
                if (target.Hp > 0 && target.MapInstance == MapInstance && !attackGreaterDistance
                    && (skillTargetRange == 0 || Map.GetDistance(p: new MapCell { X = MapX, Y = MapY },
                        q: new MapCell { X = target.PositionX, Y = target.PositionY }) <= skillTargetRange))
                {
                    if (damage >= target.Hp &&
                        Monster.BCards.Any(predicate: s =>
                            s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                            s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoKill / 10 &&
                            s.FirstData == 1))
                        damage = target.Hp - 1;
                    if (damage >= target.Hp &&
                        target.BCards.Any(predicate: s =>
                            s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                            s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoDeath / 10 &&
                            s.FirstData == -1))
                        damage = target.Hp - 1;

                    if (Owner != null && MonsterVNum == 945) hitmode = 0;

                    var firstHit = false;

                    target.GetDamage(damage: damage, damager: BattleEntity);

                    if (target.MapNpc != null)
                        if (target.MapNpc.Target == -1)
                            target.MapNpc.Target = MapMonsterId;

                    if (target.Character != null)
                    {
                        MapInstance.Broadcast(client: null, content: target.Character.GenerateStat(),
                            receiver: ReceiverType.OnlySomeone,
                            characterName: "", characterId: target.MapEntityId);

                        // Magical Fetters

                        if (damage > 0)
                            if (target.Character.HasMagicalFetters)
                            {
                                // Magic Spell

                                target.Character.AddBuff(
                                    indicator: new Buff.Buff(id: 617, level: target.Character.Level),
                                    sender: target.Character.BattleEntity);

                                var castId = 10 + Monster.Element;

                                if (castId == 10) castId += 5; // No element

                                target.Character.LastComboCastId = castId;
                                target.Character.Session?.SendPacket(packet: $"mslot {castId} -1");
                            }
                    }

                    if (target.Mate != null)
                    {
                        target.Mate.Owner.Session.SendPacket(packet: target.Mate.Owner.GeneratePst()
                            .FirstOrDefault(predicate: s => s.Contains(value: target.Mate.MateTransportId.ToString())));
                        if (target.Mate.IsSitting)
                            target.Mate.Owner.MapInstance.Broadcast(packet: target.Mate.GenerateRest(ownerSit: false));
                    }

                    if (target.MapMonster != null && Owner != null)
                    {
                        if (target.MapMonster.Target == null) target.MapMonster.Target = BattleEntity;
                        lock (target.MapMonster.DamageList)
                        {
                            if (!target.MapMonster.DamageList.Any(predicate: s => s.Value > 0)) firstHit = true;
                            target.MapMonster.AddToDamageList(damagerEntity: Owner, damage: damage);
                        }
                    }

                    MapInstance.Broadcast(packet: npcMonsterSkill != null
                        ? StaticPacketHelper.SkillUsed(type: UserType.Monster, callerId: MapMonsterId,
                            secondaryType: (byte)target.UserType,
                            targetId: target.MapEntityId,
                            skillVNum: npcMonsterSkill.SkillVNum, cooldown: npcMonsterSkill.Skill.Cooldown,
                            attackAnimation: npcMonsterSkill.Skill.AttackAnimation,
                            skillEffect: npcMonsterSkill.Skill.Effect, x: MapX, y: MapY,
                            isAlive: target.Hp > 0,
                            health: (int)(target.Hp / target.HPLoad() * 100), damage: damage,
                            hitmode: hitmode, skillType: 0)
                        : StaticPacketHelper.SkillUsed(type: UserType.Monster, callerId: MapMonsterId,
                            secondaryType: (byte)target.UserType,
                            targetId: target.MapEntityId, skillVNum: 0,
                            cooldown: Monster.BasicCooldown, attackAnimation: 11, skillEffect: Monster.BasicSkill, x: 0,
                            y: 0, isAlive: target.Hp > 0,
                            health: (int)(target.Hp / target.HPLoad() * 100), damage: damage,
                            hitmode: hitmode, skillType: 0));

                    if (hitmode != 4 && hitmode != 2)
                    {
                        // Maybe must be out (hitmode != 4 && hitmode != 2) condition
                        bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                            action: s =>
                            {
                                if (s.Type != (byte)CardType.Buff)
                                    s.ApplyBCards(session: target, sender: BattleEntity);
                            });

                        bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                            action: s =>
                            {
                                if (s.Type == (byte)CardType.Buff)
                                {
                                    var b = new Buff.Buff(id: (short)s.SecondData, level: Monster.Level);
                                    if (b.Card != null)
                                        switch (b.Card?.BuffType)
                                        {
                                            case BuffType.Bad:
                                                s.ApplyBCards(session: target, sender: BattleEntity);
                                                break;

                                            case BuffType.Good:
                                            case BuffType.Neutral:
                                                s.ApplyBCards(session: BattleEntity, sender: BattleEntity);
                                                break;
                                        }
                                }
                            });

                        target.BCards.Where(predicate: s => s.CastType == 0).ForEach(action: s =>
                        {
                            if (s.Type == (byte)CardType.Buff)
                            {
                                var b = new Buff.Buff(id: (short)s.SecondData, level: BattleEntity.Level);
                                if (b.Card != null)
                                    switch (b.Card?.BuffType)
                                    {
                                        case BuffType.Bad:
                                            s.ApplyBCards(session: BattleEntity, sender: target);
                                            break;

                                        case BuffType.Good:
                                        case BuffType.Neutral:
                                            if (s.NpcMonsterVNum == null || firstHit)
                                                s.ApplyBCards(session: target, sender: target);
                                            break;
                                    }
                            }
                        });

                        if (damage > 0)
                        {
                            target.Character?.RemoveBuffByBCardTypeSubType(
                                bcardTypes: new List<KeyValuePair<byte, byte>>
                                {
                                    new KeyValuePair<byte, byte>(key: (byte) CardType.SpecialActions,
                                        value: (byte) AdditionalTypes.SpecialActions.Hide)
                                });
                            target.RemoveBuff(id: 36);
                            target.RemoveBuff(id: 548);
                        }
                    }

                    if (target.Hp <= 0)
                    {
                        if (target.Character != null)
                        {
                            if (target.Character.IsVehicled) target.Character.RemoveVehicle();
                            if (Owner?.Character != null)
                            {
                                Owner.MapInstance?.Broadcast(packet: Owner.Character.GenerateSay(
                                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "PVP_KILL"),
                                        arg0: Owner.Character.Name, arg1: target.Character.Name), type: 10));
                                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000)).Subscribe(onNext: o =>
                                {
                                    if (target.Character?.CharacterId != null)
                                        ServerManager.Instance.AskPvpRevive(
                                            characterId: (long)target.Character?.CharacterId);
                                });
                            }
                            else
                            {
                                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000)).Subscribe(onNext: o =>
                                {
                                    if (target.Character?.CharacterId != null)
                                        ServerManager.Instance.AskRevive(
                                            characterId: (long)target.Character?.CharacterId);
                                });
                            }
                        }
                        else if (target.Mate != null)
                        {
                            if (target.Mate.IsTsProtected)
                            {
                                target.Mate.Owner.Session.SendPacket(packet: target.Mate.Owner.GenerateSay(
                                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "PET_DIED"),
                                        arg0: target.Mate.Name),
                                    type: 11));
                                target.Mate.Owner.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "PET_DIED"),
                                        arg0: target.Mate.Name),
                                    type: 0));
                            }
                        }
                        else if (target.MapMonster != null)
                        {
                            if (target.MapMonster.SetDeathStatement())
                            {
                                if (Owner?.Character != null)
                                    Owner.Character.GenerateKillBonus(monsterToAttack: target.MapMonster,
                                        Killer: BattleEntity);
                                else
                                    target.MapMonster.RunDeathEvent();
                            }
                        }
                        else if (target.MapNpc != null)
                        {
                            target.MapNpc.SetDeathStatement();
                            target.MapNpc.RunDeathEvent();
                        }

                        RemoveTarget();
                    }
                }
                else
                {
                    if (npcMonsterSkill != null && npcMonsterSkill.Skill.TargetType == 1 &&
                        npcMonsterSkill.Skill.HitType == 1)
                    {
                        MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: UserType.Monster,
                            callerId: MapMonsterId,
                            secondaryType: (byte)BattleEntity.UserType, targetId: BattleEntity.MapEntityId,
                            skillVNum: npcMonsterSkill.SkillVNum, cooldown: npcMonsterSkill.Skill.Cooldown,
                            attackAnimation: npcMonsterSkill.Skill.AttackAnimation,
                            skillEffect: npcMonsterSkill.Skill.Effect, x: MapX, y: MapY,
                            isAlive: BattleEntity.Hp > 0,
                            health: (int)(BattleEntity.Hp / BattleEntity.HPLoad() * 100), damage: 0,
                            hitmode: 0, skillType: 0));

                        if (npcMonsterSkill.Skill.Range > 0)
                            MapInstance.GetBattleEntitiesInRange(pos: new MapCell { X = MapX, Y = MapY },
                                    distance: npcMonsterSkill.Skill.Range).ToList()
                                .ForEach(action: e =>
                                {
                                    npcMonsterSkill.Skill.BCards.ForEach(action: bc =>
                                        bc.ApplyBCards(session: e, sender: BattleEntity));
                                });
                    }
                }
            }

            if (npcMonsterSkill != null
                && SkillHelper.IsSelfAttack(skillVNum: npcMonsterSkill.SkillVNum))
                return;

            // In range entities

            var rangeBaseX = target.PositionX;
            var rangeBaseY = target.PositionY;

            if (npcMonsterSkill != null && npcMonsterSkill.Skill.HitType == 1 && npcMonsterSkill.Skill.TargetType == 1)
            {
                rangeBaseX = MapX;
                rangeBaseY = MapY;
            }

            if (npcMonsterSkill != null && (npcMonsterSkill.Skill.Range > 0 || npcMonsterSkill.Skill.TargetRange > 0))
            {
                var onyxWings = false;
                foreach (var characterInRange in MapInstance
                    .GetCharactersInRange(
                        mapX: npcMonsterSkill.Skill.TargetRange == 0 ? MapX : rangeBaseX,
                        mapY: npcMonsterSkill.Skill.TargetRange == 0 ? MapY : rangeBaseY,
                        distance: skillRange,
                        attackGreaterDistance: attackGreaterDistance)
                    .Where(predicate: s => s.CharacterId != target.MapEntityId))
                    if (!BattleEntity.CanAttackEntity(receiver: characterInRange.BattleEntity))
                    {
                        npcMonsterSkill.Skill.BCards.Where(predicate: s => s.Type == (byte)CardType.Buff).ToList()
                            .ForEach(action: s =>
                            {
                                if (new Buff.Buff(id: (short)s.SecondData, level: Monster.Level) is { } b)
                                    switch (b.Card?.BuffType)
                                    {
                                        case BuffType.Good:
                                        case BuffType.Neutral:
                                            s.ApplyBCards(session: characterInRange.BattleEntity, sender: BattleEntity);
                                            break;
                                    }
                            });
                    }
                    else
                    {
                        if (characterInRange.IsSitting)
                        {
                            characterInRange.IsSitting = false;
                            MapInstance.Broadcast(packet: characterInRange.GenerateRest());
                        }

                        if (characterInRange.HasGodMode) hitmode = 4;

                        if (characterInRange.Hp > 0)
                        {
                            var dmg = DamageHelper.Instance.CalculateDamage(attacker: new BattleEntity(monster: this),
                                defender: new BattleEntity(character: characterInRange, skill: null),
                                skill: npcMonsterSkill.Skill, hitMode: ref hitmode,
                                onyxWings: ref onyxWings, attackGreaterDistance: attackGreaterDistance);
                            if (dmg >= characterInRange.Hp &&
                                Monster.BCards.Any(predicate: s =>
                                    s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                                    s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoKill / 10 &&
                                    s.FirstData == 1))
                                dmg = characterInRange.Hp - 1;

                            if (hitmode != 4 && hitmode != 2)
                            {
                                bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                                    action: s =>
                                    {
                                        if (s.Type != (byte)CardType.Buff && s.Type != (byte)CardType.Summons &&
                                            s.Type != (byte)CardType.SummonSkill)
                                            s.ApplyBCards(session: characterInRange.BattleEntity, sender: BattleEntity);
                                    });

                                bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                                    action: s =>
                                    {
                                        if (s.Type == (byte)CardType.Buff)
                                        {
                                            var b = new Buff.Buff(id: (short)s.SecondData, level: Monster.Level);
                                            if (b.Card != null)
                                                switch (b.Card?.BuffType)
                                                {
                                                    case BuffType.Bad:
                                                        s.ApplyBCards(session: characterInRange.BattleEntity,
                                                            sender: BattleEntity);
                                                        break;

                                                    case BuffType.Good:
                                                    case BuffType.Neutral:
                                                        s.ApplyBCards(session: BattleEntity, sender: BattleEntity);
                                                        break;
                                                }
                                        }
                                    });

                                characterInRange.BattleEntity.BCards.Where(predicate: s => s.CastType == 0).ForEach(
                                    action: s =>
                                    {
                                        if (s.Type == (byte)CardType.Buff)
                                        {
                                            var b = new Buff.Buff(id: (short)s.SecondData, level: BattleEntity.Level);
                                            if (b.Card != null)
                                                switch (b.Card?.BuffType)
                                                {
                                                    case BuffType.Bad:
                                                        s.ApplyBCards(session: BattleEntity,
                                                            sender: characterInRange.BattleEntity);
                                                        break;

                                                    case BuffType.Good:
                                                    case BuffType.Neutral:
                                                        s.ApplyBCards(session: characterInRange.BattleEntity,
                                                            sender: characterInRange.BattleEntity);
                                                        break;
                                                }
                                        }
                                    });
                            }

                            characterInRange.GetDamage(damage: dmg, damager: BattleEntity);
                            lock (DamageList)
                            {
                                if (!DamageList.Any(predicate: s => s.Key.MapEntityId == characterInRange.CharacterId))
                                    AddToAggroList(aggroEntity: characterInRange.BattleEntity);
                            }

                            MapInstance.Broadcast(client: null, content: characterInRange.GenerateStat(),
                                receiver: ReceiverType.OnlySomeone,
                                characterName: "", characterId: characterInRange.CharacterId);

                            switch (hitmode)
                            {
                                case 1:
                                case 4:
                                    hitmode = 7;
                                    break;

                                case 2:
                                    hitmode = 2;
                                    break;

                                case 3:
                                    hitmode = 6;
                                    break;

                                default:
                                    hitmode = 5;
                                    break;
                            }

                            MapInstance.Broadcast(packet: npcMonsterSkill != null
                                ? StaticPacketHelper.SkillUsed(type: UserType.Monster, callerId: MapMonsterId,
                                    secondaryType: (byte)UserType.Player,
                                    targetId: characterInRange.CharacterId,
                                    skillVNum: npcMonsterSkill.SkillVNum, cooldown: npcMonsterSkill.Skill.Cooldown,
                                    attackAnimation: npcMonsterSkill.Skill.AttackAnimation,
                                    skillEffect: npcMonsterSkill.Skill.Effect, x: MapX, y: MapY,
                                    isAlive: characterInRange.Hp > 0,
                                    health: (int)(characterInRange.Hp / characterInRange.HPLoad() * 100), damage: dmg,
                                    hitmode: hitmode, skillType: 0)
                                : StaticPacketHelper.SkillUsed(type: UserType.Monster, callerId: MapMonsterId,
                                    secondaryType: (byte)UserType.Player,
                                    targetId: characterInRange.CharacterId, skillVNum: 0,
                                    cooldown: Monster.BasicCooldown, attackAnimation: 11,
                                    skillEffect: Monster.BasicSkill, x: 0, y: 0, isAlive: characterInRange.Hp > 0,
                                    health: (int)(characterInRange.Hp / characterInRange.HPLoad() * 100), damage: dmg,
                                    hitmode: hitmode, skillType: 0));

                            if (hitmode != 4 && hitmode != 2 && dmg > 0)
                            {
                                characterInRange.RemoveBuffByBCardTypeSubType(
                                    bcardTypes: new List<KeyValuePair<byte, byte>>
                                    {
                                        new KeyValuePair<byte, byte>(key: (byte) CardType.SpecialActions,
                                            value: (byte) AdditionalTypes.SpecialActions.Hide)
                                    });
                                characterInRange.RemoveBuff(cardId: 36);
                                characterInRange.RemoveBuff(cardId: 548);
                            }

                            if (characterInRange.Hp <= 0)
                            {
                                if (characterInRange.IsVehicled) characterInRange.RemoveVehicle();
                                if (Owner?.Character != null)
                                {
                                    Owner.MapInstance?.Broadcast(packet: Owner.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "PVP_KILL"),
                                            arg0: Owner.Character.Name, arg1: characterInRange?.Name), type: 10));
                                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000))
                                        .Subscribe(onNext: o =>
                                        {
                                            if (characterInRange?.CharacterId != null)
                                                ServerManager.Instance.AskPvpRevive(
                                                    characterId: (long)characterInRange?.CharacterId);
                                        });
                                }
                                else
                                {
                                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000))
                                        .Subscribe(onNext: o =>
                                        {
                                            if (characterInRange?.CharacterId != null)
                                                ServerManager.Instance.AskRevive(
                                                    characterId: (long)characterInRange?.CharacterId);
                                        });
                                }
                            }
                        }
                    }

                foreach (var mateInRange in BattleEntity.MapInstance
                    .GetListMateInRange(
                        mapX: npcMonsterSkill.Skill.TargetRange == 0 ? BattleEntity.PositionX : rangeBaseX,
                        mapY: npcMonsterSkill.Skill.TargetRange == 0 ? BattleEntity.PositionY : rangeBaseY,
                        distance: skillRange,
                        attackGreaterDistance: attackGreaterDistance)
                    .Where(predicate: s => s.MateTransportId != target.MapEntityId))
                    if (!BattleEntity.CanAttackEntity(receiver: mateInRange.BattleEntity))
                        npcMonsterSkill.Skill.BCards.Where(predicate: s => s.Type == (byte)CardType.Buff).ToList()
                            .ForEach(action: s =>
                            {
                                if (new Buff.Buff(id: (short)s.SecondData, level: Monster.Level) is { } b)
                                    switch (b.Card?.BuffType)
                                    {
                                        case BuffType.Good:
                                        case BuffType.Neutral:
                                            s.ApplyBCards(session: mateInRange.BattleEntity, sender: BattleEntity);
                                            break;
                                    }
                            });
                    else
                        mateInRange.HitRequest(hitRequest: new HitRequest(targetHitType: TargetHitType.AoeTargetHit,
                            monster: this, skill: npcMonsterSkill));

                foreach (var monsterInRange in MapInstance
                    .GetMonsterInRangeList(
                        mapX: npcMonsterSkill.Skill.TargetRange == 0 ? BattleEntity.PositionX : rangeBaseX,
                        mapY: npcMonsterSkill.Skill.TargetRange == 0 ? BattleEntity.PositionY : rangeBaseY,
                        distance: skillRange,
                        attackGreaterDistance: attackGreaterDistance)
                    .Where(predicate: s => s.MapMonsterId != target.MapEntityId))
                    if (!BattleEntity.CanAttackEntity(receiver: monsterInRange.BattleEntity))
                    {
                        npcMonsterSkill.Skill.BCards.Where(predicate: s => s.Type == (byte)CardType.Buff).ToList()
                            .ForEach(action: s =>
                            {
                                if (new Buff.Buff(id: (short)s.SecondData, level: Monster.Level) is { } b)
                                    switch (b.Card?.BuffType)
                                    {
                                        case BuffType.Good:
                                        case BuffType.Neutral:
                                            s.ApplyBCards(session: monsterInRange.BattleEntity, sender: BattleEntity);
                                            break;
                                    }
                            });
                    }
                    else
                    {
                        if (monsterInRange.CurrentHp > 0 && monsterInRange.Owner?.Character == null &&
                            monsterInRange.Owner?.Mate == null)
                        {
                            var dmg = DamageHelper.Instance.CalculateDamage(attacker: new BattleEntity(monster: this),
                                defender: new BattleEntity(monster: monsterInRange), skill: npcMonsterSkill.Skill,
                                hitMode: ref hitmode, onyxWings: ref onyxWings,
                                attackGreaterDistance: attackGreaterDistance);
                            if (dmg >= monsterInRange.CurrentHp &&
                                Monster.BCards.Any(predicate: s =>
                                    s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                                    s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoKill / 10 &&
                                    s.FirstData == 1))
                                dmg = (int)monsterInRange.CurrentHp - 1;
                            if (dmg >= monsterInRange.CurrentHp &&
                                monsterInRange.Monster.BCards.Any(predicate: s =>
                                    s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                                    s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoDeath / 10 &&
                                    s.FirstData == -1))
                                dmg = (int)monsterInRange.CurrentHp - 1;

                            var firstHit = false;
                            lock (monsterInRange.DamageList)
                            {
                                if (!monsterInRange.DamageList.Any(predicate: s => s.Value > 0)) firstHit = true;
                            }

                            if (monsterInRange.Target == null) monsterInRange.Target = BattleEntity;

                            if (hitmode != 4 && hitmode != 2)
                            {
                                bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                                    action: s =>
                                    {
                                        if (s.Type != (byte)CardType.Buff && s.Type != (byte)CardType.Summons &&
                                            s.Type != (byte)CardType.SummonSkill)
                                            s.ApplyBCards(session: monsterInRange.BattleEntity, sender: BattleEntity);
                                    });

                                if (dmg > 0)
                                {
                                    monsterInRange.RemoveBuff(cardId: 36);
                                    monsterInRange.RemoveBuff(cardId: 548);
                                }

                                bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                                    action: s =>
                                    {
                                        if (s.Type == (byte)CardType.Buff)
                                        {
                                            var b = new Buff.Buff(id: (short)s.SecondData, level: Monster.Level);
                                            if (b.Card != null)
                                                switch (b.Card?.BuffType)
                                                {
                                                    case BuffType.Bad:
                                                        s.ApplyBCards(session: monsterInRange.BattleEntity,
                                                            sender: BattleEntity);
                                                        break;

                                                    case BuffType.Good:
                                                    case BuffType.Neutral:
                                                        s.ApplyBCards(session: BattleEntity, sender: BattleEntity);
                                                        break;
                                                }
                                        }
                                    });

                                monsterInRange.BattleEntity.BCards.Where(predicate: s => s.CastType == 0).ForEach(
                                    action: s =>
                                    {
                                        if (s.Type == (byte)CardType.Buff)
                                        {
                                            var b = new Buff.Buff(id: (short)s.SecondData, level: BattleEntity.Level);
                                            if (b.Card != null)
                                                switch (b.Card?.BuffType)
                                                {
                                                    case BuffType.Bad:
                                                        s.ApplyBCards(session: BattleEntity,
                                                            sender: monsterInRange.BattleEntity);
                                                        break;

                                                    case BuffType.Good:
                                                    case BuffType.Neutral:
                                                        if (s.NpcMonsterVNum == null || firstHit)
                                                            s.ApplyBCards(session: monsterInRange.BattleEntity,
                                                                sender: monsterInRange.BattleEntity);
                                                        break;
                                                }
                                        }
                                    });
                            }

                            monsterInRange.BattleEntity.GetDamage(damage: dmg, damager: BattleEntity);
                            lock (DamageList)
                            {
                                if (!DamageList.Any(predicate: s => s.Key.MapEntityId == monsterInRange.MapMonsterId))
                                    AddToAggroList(aggroEntity: monsterInRange.BattleEntity);
                            }

                            if (Owner != null)
                                lock (monsterInRange.DamageList)
                                {
                                    monsterInRange.AddToDamageList(damagerEntity: Owner, damage: dmg);
                                }

                            switch (hitmode)
                            {
                                case 1:
                                case 4:
                                    hitmode = 7;
                                    break;

                                case 2:
                                    hitmode = 2;
                                    break;

                                case 3:
                                    hitmode = 6;
                                    break;

                                default:
                                    hitmode = 5;
                                    break;
                            }

                            MapInstance.Broadcast(packet: npcMonsterSkill != null
                                ? StaticPacketHelper.SkillUsed(type: UserType.Monster, callerId: MapMonsterId,
                                    secondaryType: (byte)UserType.Monster,
                                    targetId: monsterInRange.MapMonsterId,
                                    skillVNum: npcMonsterSkill.SkillVNum, cooldown: npcMonsterSkill.Skill.Cooldown,
                                    attackAnimation: npcMonsterSkill.Skill.AttackAnimation,
                                    skillEffect: npcMonsterSkill.Skill.Effect, x: MapX, y: MapY,
                                    isAlive: monsterInRange.CurrentHp > 0,
                                    health: (int)(monsterInRange.CurrentHp / monsterInRange.MaxHp * 100), damage: dmg,
                                    hitmode: hitmode, skillType: 0)
                                : StaticPacketHelper.SkillUsed(type: UserType.Monster, callerId: MapMonsterId,
                                    secondaryType: (byte)UserType.Monster,
                                    targetId: monsterInRange.MapMonsterId, skillVNum: 0,
                                    cooldown: Monster.BasicCooldown, attackAnimation: 11,
                                    skillEffect: Monster.BasicSkill, x: 0, y: 0, isAlive: monsterInRange.CurrentHp > 0,
                                    health: (int)(monsterInRange.CurrentHp / monsterInRange.MaxHp * 100), damage: dmg,
                                    hitmode: hitmode, skillType: 0));

                            if (monsterInRange.CurrentHp <= 0)
                                if (monsterInRange.SetDeathStatement())
                                {
                                    if (Owner?.Character != null)
                                        Owner.Character.GenerateKillBonus(monsterToAttack: monsterInRange,
                                            Killer: BattleEntity);
                                    else
                                        monsterInRange.RunDeathEvent();
                                }
                        }
                    }

                foreach (var npcInRange in MapInstance
                    .GetListNpcInRange(
                        mapX: npcMonsterSkill.Skill.TargetRange == 0 ? BattleEntity.PositionX : rangeBaseX,
                        mapY: npcMonsterSkill.Skill.TargetRange == 0 ? BattleEntity.PositionY : rangeBaseY,
                        distance: skillRange,
                        attackGreaterDistance: attackGreaterDistance)
                    .Where(predicate: s => s.MapNpcId != target.MapEntityId))
                    if (!BattleEntity.CanAttackEntity(receiver: npcInRange.BattleEntity))
                    {
                        npcMonsterSkill.Skill.BCards.Where(predicate: s => s.Type == (byte)CardType.Buff).ToList()
                            .ForEach(action: s =>
                            {
                                if (new Buff.Buff(id: (short)s.SecondData, level: Monster.Level) is { } b)
                                    switch (b.Card?.BuffType)
                                    {
                                        case BuffType.Good:
                                        case BuffType.Neutral:
                                            s.ApplyBCards(session: npcInRange.BattleEntity, sender: BattleEntity);
                                            break;
                                    }
                            });
                    }
                    else
                    {
                        if (npcInRange.CurrentHp > 0)
                        {
                            var dmg = DamageHelper.Instance.CalculateDamage(attacker: new BattleEntity(monster: this),
                                defender: new BattleEntity(npc: npcInRange), skill: npcMonsterSkill.Skill,
                                hitMode: ref hitmode, onyxWings: ref onyxWings,
                                attackGreaterDistance: attackGreaterDistance);
                            if (dmg >= npcInRange.CurrentHp &&
                                Monster.BCards.Any(predicate: s =>
                                    s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                                    s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoKill / 10 &&
                                    s.FirstData == 1))
                                dmg = (int)npcInRange.CurrentHp - 1;

                            if (hitmode != 4 && hitmode != 2)
                            {
                                bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                                    action: s =>
                                    {
                                        if (s.Type != (byte)CardType.Buff && s.Type != (byte)CardType.Summons &&
                                            s.Type != (byte)CardType.SummonSkill)
                                            s.ApplyBCards(session: npcInRange.BattleEntity, sender: BattleEntity);
                                    });

                                if (dmg > 0)
                                {
                                    npcInRange.RemoveBuff(cardId: 36);
                                    npcInRange.RemoveBuff(cardId: 548);
                                }

                                bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                                    action: s =>
                                    {
                                        if (s.Type == (byte)CardType.Buff)
                                        {
                                            var b = new Buff.Buff(id: (short)s.SecondData, level: Monster.Level);
                                            if (b.Card != null)
                                                switch (b.Card?.BuffType)
                                                {
                                                    case BuffType.Bad:
                                                        s.ApplyBCards(session: npcInRange.BattleEntity,
                                                            sender: BattleEntity);
                                                        break;

                                                    case BuffType.Good:
                                                    case BuffType.Neutral:
                                                        s.ApplyBCards(session: BattleEntity, sender: BattleEntity);
                                                        break;
                                                }
                                        }
                                    });

                                npcInRange.BattleEntity.BCards.Where(predicate: s => s.CastType == 0).ForEach(
                                    action: s =>
                                    {
                                        if (s.Type == (byte)CardType.Buff)
                                        {
                                            var b = new Buff.Buff(id: (short)s.SecondData, level: BattleEntity.Level);
                                            if (b.Card != null)
                                                switch (b.Card?.BuffType)
                                                {
                                                    case BuffType.Bad:
                                                        s.ApplyBCards(session: BattleEntity,
                                                            sender: npcInRange.BattleEntity);
                                                        break;

                                                    case BuffType.Good:
                                                    case BuffType.Neutral:
                                                        s.ApplyBCards(session: npcInRange.BattleEntity,
                                                            sender: npcInRange.BattleEntity);
                                                        break;
                                                }
                                        }
                                    });
                            }

                            npcInRange.BattleEntity.GetDamage(damage: dmg, damager: BattleEntity);

                            if (npcInRange.Target == -1) npcInRange.Target = MapMonsterId;
                            lock (DamageList)
                            {
                                if (!DamageList.Any(predicate: s => s.Key.MapEntityId == npcInRange.MapNpcId))
                                    AddToAggroList(aggroEntity: npcInRange.BattleEntity);
                            }

                            switch (hitmode)
                            {
                                case 1:
                                case 4:
                                    hitmode = 7;
                                    break;

                                case 2:
                                    hitmode = 2;
                                    break;

                                case 3:
                                    hitmode = 6;
                                    break;

                                default:
                                    hitmode = 5;
                                    break;
                            }

                            MapInstance.Broadcast(packet: npcMonsterSkill != null
                                ? StaticPacketHelper.SkillUsed(type: UserType.Monster, callerId: MapMonsterId,
                                    secondaryType: (byte)UserType.Npc,
                                    targetId: npcInRange.MapNpcId,
                                    skillVNum: npcMonsterSkill.SkillVNum, cooldown: npcMonsterSkill.Skill.Cooldown,
                                    attackAnimation: npcMonsterSkill.Skill.AttackAnimation,
                                    skillEffect: npcMonsterSkill.Skill.Effect, x: MapX, y: MapY,
                                    isAlive: npcInRange.CurrentHp > 0,
                                    health: (int)(npcInRange.CurrentHp / npcInRange.MaxHp * 100), damage: dmg,
                                    hitmode: hitmode, skillType: 0)
                                : StaticPacketHelper.SkillUsed(type: UserType.Monster, callerId: MapMonsterId,
                                    secondaryType: (byte)UserType.Npc,
                                    targetId: npcInRange.MapNpcId, skillVNum: 0,
                                    cooldown: Monster.BasicCooldown, attackAnimation: 11,
                                    skillEffect: Monster.BasicSkill, x: 0, y: 0, isAlive: npcInRange.CurrentHp > 0,
                                    health: (int)(npcInRange.CurrentHp / npcInRange.MaxHp * 100), damage: dmg,
                                    hitmode: hitmode, skillType: 0));

                            if (npcInRange.CurrentHp <= 0)
                            {
                                npcInRange.SetDeathStatement();
                                npcInRange.RunDeathEvent();
                            }
                        }
                    }
            }
        }

        #endregion
    }
}