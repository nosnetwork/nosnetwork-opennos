﻿using System;

namespace OpenNos.GameObject
{
    public class CharacterMapItem : MapItem
    {
        #region Instantiation

        public CharacterMapItem(short x, short y, ItemInstance itemInstance) : base(x: x, y: y)
        {
            ItemInstance = itemInstance;
        }

        #endregion

        #region Methods

        public override ItemInstance GetItemInstance()
        {
            return ItemInstance;
        }

        #endregion

        #region Properties

        public override short Amount
        {
            get => ItemInstance.Amount;
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException(paramName: nameof(value));
                ItemInstance.Amount = Amount;
            }
        }

        public ItemInstance ItemInstance { get; set; }

        public override short ItemVNum
        {
            get => ItemInstance.ItemVNum;
            set => ItemInstance.ItemVNum = value;
        }

        #endregion
    }
}