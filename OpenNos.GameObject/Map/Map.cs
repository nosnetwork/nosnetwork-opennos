﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using OpenNos.Core.ArrayExtensions;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Data.Interfaces;
using OpenNos.GameObject.Event;
using OpenNos.PathFinder;

namespace OpenNos.GameObject
{
    public class Map : IMapDto
    {
        #region Instantiation

        public Map(short mapId, short gridMapId, byte[] data)
        {
            MapId = mapId;
            GridMapId = gridMapId;
            Data = data;
            LoadZone();
            MapTypes = new List<MapTypeDto>();
            foreach (var maptypemap in DaoFactory.MapTypeMapDao.LoadByMapId(mapId: mapId).ToList())
            {
                var maptype = DaoFactory.MapTypeDao.LoadById(maptypeId: maptypemap.MapTypeId);
                MapTypes.Add(item: maptype);
            }

            if (MapTypes.Count > 0 && MapTypes[index: 0].RespawnMapTypeId != null)
            {
                var respawnMapTypeId = MapTypes[index: 0].RespawnMapTypeId;
                var returnMapTypeId = MapTypes[index: 0].ReturnMapTypeId;
                if (respawnMapTypeId != null)
                    DefaultRespawn = DaoFactory.RespawnMapTypeDao.LoadById(respawnMapTypeId: (long)respawnMapTypeId);
                if (returnMapTypeId != null)
                    DefaultReturn = DaoFactory.RespawnMapTypeDao.LoadById(respawnMapTypeId: (long)returnMapTypeId);
            }
        }

        #endregion

        #region Members

        //private readonly Random _random;

        //Function to get a random number 
        static readonly Random Random = new Random();
        static readonly object SyncLock = new object();

        public static int RandomNumber(int min, int max)
        {
            lock (SyncLock)
            {
                // synchronize
                return Random.Next(minValue: min, maxValue: max);
            }
        }

        #endregion

        #region Properties

        public byte[] Data { get; set; }

        public RespawnMapTypeDto DefaultRespawn { get; }

        public RespawnMapTypeDto DefaultReturn { get; }

        public GridPos[][] JaggedGrid { get; set; }

        public short MapId { get; set; }

        public short GridMapId { get; set; }

        public List<MapTypeDto> MapTypes { get; }

        public int Music { get; set; }

        public string Name { get; set; }

        public bool ShopAllowed { get; set; }

        ConcurrentBag<MapCell> Cells { get; set; }

        internal int XLength { get; set; }

        internal int YLength { get; set; }

        public byte XpRate { get; set; }

        #endregion

        #region Methods

        public static MapCell GetNextStep(MapCell start, MapCell end, double steps)
        {
            MapCell futurPoint;
            double newX = start.X;
            double newY = start.Y;

            if (start.X < end.X)
            {
                newX = start.X + steps;
                if (newX > end.X)
                    newX = end.X;
            }
            else if (start.X > end.X)
            {
                newX = start.X - steps;
                if (newX < end.X)
                    newX = end.X;
            }

            if (start.Y < end.Y)
            {
                newY = start.Y + steps;
                if (newY > end.Y)
                    newY = end.Y;
            }
            else if (start.Y > end.Y)
            {
                newY = start.Y - steps;
                if (newY < end.Y)
                    newY = end.Y;
            }

            futurPoint = new MapCell { X = (short)newX, Y = (short)newY };
            return futurPoint;
        }

        public static int GetDistance(Character character1, Character character2)
        {
            return GetDistance(p: new MapCell { X = character1.PositionX, Y = character1.PositionY },
                q: new MapCell { X = character2.PositionX, Y = character2.PositionY });
        }

        public static int GetDistance(MapCell p, MapCell q)
        {
            return (int)Heuristic.Octile(iDx: Math.Abs(value: p.X - q.X), iDy: Math.Abs(value: p.Y - q.Y));
        }

        public IEnumerable<MonsterToSummon> GenerateMonsters(short vnum, short amount, bool move,
            List<EventContainer> deathEvents, bool isBonus = false, bool isHostile = true, bool isBoss = false)
        {
            var summonParameters = new List<MonsterToSummon>();
            for (var i = 0; i < amount; i++)
            {
                var cell = GetRandomPosition();
                summonParameters.Add(
                    item: new MonsterToSummon(vnum: vnum, spawnCell: cell, target: null, move: move, isBonus: isBonus,
                            isHostile: isHostile, isBoss: isBoss)
                    { DeathEvents = deathEvents });
            }

            return summonParameters;
        }

        public List<NpcToSummon> GenerateNpcs(short vnum, short amount, List<EventContainer> deathEvents, bool isMate,
            bool isProtected, bool move, bool isHostile)
        {
            var summonParameters = new List<NpcToSummon>();
            for (var i = 0; i < amount; i++)
            {
                var cell = GetRandomPosition();
                summonParameters.Add(item: new NpcToSummon(vnum: vnum, spawnCell: cell, target: -1,
                        isProtected: isProtected, isMate: isMate, move: move, isHostile: isHostile)
                { DeathEvents = deathEvents });
            }

            return summonParameters;
        }

        public MapCell GetRandomPosition()
        {
            if (Cells == null)
            {
                Cells = new ConcurrentBag<MapCell>();
                Parallel.For(fromInclusive: 0, toExclusive: YLength, body: y => Parallel.For(fromInclusive: 0,
                    toExclusive: XLength, body: x =>
                    {
                        if (!IsBlockedZone(x: x, y: y) && CanWalkAround(x: x, y: y))
                            Cells.Add(item: new MapCell { X = (short)x, Y = (short)y });
                    }));
            }

            return Cells.OrderBy(keySelector: s => RandomNumber(min: 0, max: int.MaxValue)).FirstOrDefault();
        }

        public bool CanWalkAround(int x, int y)
        {
            for (var dX = -1; dX <= 1; dX++)
                for (var dY = -1; dY <= 1; dY++)
                {
                    if (dX == 0 && dY == 0) continue;

                    if (!IsBlockedZone(x: x + dX, y: y + dY)) return true;
                }

            return false;
        }

        public MapCell GetRandomPositionByDistance(short xPos, short yPos, short distance, bool randomInRange = false)
        {
            if (Cells == null)
            {
                Cells = new ConcurrentBag<MapCell>();
                Parallel.For(fromInclusive: 0, toExclusive: YLength, body: y => Parallel.For(fromInclusive: 0,
                    toExclusive: XLength, body: x =>
                    {
                        if (!IsBlockedZone(x: x, y: y)) Cells.Add(item: new MapCell { X = (short)x, Y = (short)y });
                    }));
            }

            if (randomInRange)
                return Cells
                    .Where(predicate: s =>
                        GetDistance(p: new MapCell { X = xPos, Y = yPos }, q: new MapCell { X = s.X, Y = s.Y }) <=
                        distance && !IsBlockedZone(firstX: xPos, firstY: yPos, mapX: s.X, mapY: s.Y))
                    .OrderBy(keySelector: s => RandomNumber(min: 0, max: int.MaxValue)).FirstOrDefault();
            return Cells
                .Where(predicate: s =>
                    GetDistance(p: new MapCell { X = xPos, Y = yPos }, q: new MapCell { X = s.X, Y = s.Y }) <=
                    distance && !IsBlockedZone(firstX: xPos, firstY: yPos, mapX: s.X, mapY: s.Y))
                .OrderBy(keySelector: s => RandomNumber(min: 0, max: int.MaxValue)).ThenByDescending(keySelector: s =>
                    GetDistance(p: new MapCell { X = xPos, Y = yPos }, q: new MapCell { X = s.X, Y = s.Y }))
                .FirstOrDefault();
        }

        public bool IsBlockedZone(int x, int y)
        {
            try
            {
                if (JaggedGrid == null
                    || MapId == 2552 && y > 38
                    || x < 0
                    || y < 0
                    || x >= JaggedGrid.Length
                    || JaggedGrid[x] == null
                    || y >= JaggedGrid[x].Length
                    || JaggedGrid[x][y] == null
                )
                    return true;

                return !JaggedGrid[x][y].IsWalkable();
            }
            catch
            {
                return true;
            }
        }

        internal bool GetFreePosition(ref short firstX, ref short firstY, byte xpoint, byte ypoint)
        {
            var minX = (short)(-xpoint + firstX);
            var maxX = (short)(xpoint + firstX);

            var minY = (short)(-ypoint + firstY);
            var maxY = (short)(ypoint + firstY);

            var cells = new List<MapCell>();
            for (var y = minY; y <= maxY; y++)
                for (var x = minX; x <= maxX; x++)
                    if (x != firstX || y != firstY)
                        cells.Add(item: new MapCell { X = x, Y = y });
            foreach (var cell in cells.OrderBy(keySelector: s => RandomNumber(min: 0, max: int.MaxValue)))
                if (!IsBlockedZone(firstX: firstX, firstY: firstY, mapX: cell.X, mapY: cell.Y))
                {
                    firstX = cell.X;
                    firstY = cell.Y;
                    return true;
                }

            return false;
        }

        public bool IsBlockedZone(int firstX, int firstY, int mapX, int mapY)
        {
            if (IsBlockedZone(x: mapX, y: mapY) || !CanWalkAround(x: mapX, y: mapY)) return true;
            for (var i = 1; i <= Math.Abs(value: mapX - firstX); i++)
                if (IsBlockedZone(x: firstX + Math.Sign(value: mapX - firstX) * i, y: firstY))
                    return true;
            for (var i = 1; i <= Math.Abs(value: mapY - firstY); i++)
                if (IsBlockedZone(x: firstX, y: firstY + Math.Sign(value: mapY - firstY) * i))
                    return true;
            return false;
        }

        void LoadZone()
        {
            using (var reader = new BinaryReader(input: new MemoryStream(buffer: Data)))
            {
                XLength = reader.ReadInt16();
                YLength = reader.ReadInt16();

                JaggedGrid = JaggedArrayExtensions.CreateJaggedArray<GridPos>(xLenght: XLength, yLength: YLength);
                for (short i = 0; i < YLength; ++i)
                    for (short t = 0; t < XLength; ++t)
                        JaggedGrid[t][i] = new GridPos
                        {
                            Value = reader.ReadByte(),
                            X = t,
                            Y = i
                        };
            }
        }

        #endregion
    }
}