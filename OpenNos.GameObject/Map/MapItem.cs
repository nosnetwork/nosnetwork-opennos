﻿using System;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;

namespace OpenNos.GameObject
{
    public abstract class MapItem
    {
        #region Instantiation

        protected MapItem(short x, short y)
        {
            PositionX = x;
            PositionY = y;
            CreatedDate = DateTime.Now;
            TransportId = 0;
        }

        #endregion

        #region Members

        protected ItemInstance _itemInstance;

        readonly object _lockObject = new object();
        long _transportId;

        #endregion

        #region Properties

        public abstract short Amount { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsQuest { get; set; }

        public abstract short ItemVNum { get; set; }

        public short PositionX { get; set; }

        public short PositionY { get; set; }

        public long TransportId
        {
            get
            {
                lock (_lockObject)
                {
                    if (_transportId == 0) _transportId = TransportFactory.Instance.GenerateTransportId();
                    return _transportId;
                }
            }

            private set
            {
                // ReSharper disable once RedundantCheckBeforeAssignment
                if (value != _transportId) _transportId = value;
            }
        }

        #endregion

        #region Methods

        public string GenerateIn()
        {
            return StaticPacketHelper.In(type: UserType.Object, callerVNum: ItemVNum, callerId: TransportId,
                mapX: PositionX, mapY: PositionY,
                direction: this is MonsterMapItem monsterMapItem && monsterMapItem.GoldAmount > 1
                    ? monsterMapItem.GoldAmount
                    : Amount, currentHp: 0, currentMp: 0, dialog: 0, respawnType: 0, isSitting: false, name: "-",
                invisible: false);
        }

        public abstract ItemInstance GetItemInstance();

        #endregion
    }
}