﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Threading;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Log.Networking;
using OpenNos.Log.Shared;
using OpenNos.PathFinder;

namespace OpenNos.GameObject
{
    public class MapInstance : BroadcastableBase
    {
        #region Instantiation

        public MapInstance(Map map, Guid guid, bool shopAllowed, MapInstanceType type, InstanceBag instanceBag,
            bool dropAllowed = false)
        {
            OnSpawnEvents = new List<EventContainer>();
            Buttons = new List<MapButton>();
            XpRate = 0;

            if (type == MapInstanceType.BaseMapInstance) XpRate = map.XpRate;

            MinLevel = 1;
            MaxLevel = 99;

            if (type != MapInstanceType.TimeSpaceInstance && type != MapInstanceType.RaidInstance)
                switch (map.MapId)
                {
                    case 154: // Caligor's Realm
                        MinLevel = 80;
                        break;
                    case 205: // Kertos' Jaws
                    case 206: // Eastern Path
                    case 208: // Valakus' Claws
                    case 209: // Phoenix Wings
                    case 210: // Left Wing
                    case 211: // Right Wing
                        MinLevel = 88;
                        break;
                }

            DropAllowed = dropAllowed;
            ShopAllowed = shopAllowed;
            MapInstanceType = type;
            _isSleeping = true;
            LastUserShopId = 0;
            InstanceBag = instanceBag;
            Clock = new Clock(type: 3);
            _random = new Random();
            Map = map;
            MapInstanceId = guid;
            ScriptedInstances = new List<ScriptedInstance>();
            OnCharacterDiscoveringMapEvents = new List<Tuple<EventContainer, List<long>>>();
            OnMoveOnMapEvents = new ThreadSafeGenericList<EventContainer>();
            OnAreaEntryEvents = new ThreadSafeGenericList<ZoneEvent>();
            WaveEvents = new List<EventWave>();
            OnMapClean = new List<EventContainer>();
            _monsters = new ThreadSafeSortedList<long, MapMonster>();
            _delayedMonsters = new ThreadSafeSortedList<long, MapMonster>();
            _npcs = new ThreadSafeSortedList<long, MapNpc>();
            _mapMonsterIds = new ThreadSafeSortedList<int, int>();
            _mapNpcIds = new ThreadSafeSortedList<int, int>();
            DroppedList = new ThreadSafeSortedList<long, MapItem>();
            Portals = new List<Portal>();
            UnlockEvents = new List<EventContainer>();
            UserShops = new Dictionary<long, MapShop>();
            RemovedMobNpcList = new List<object>();
            StartLife();
        }

        #endregion

        #region Members

        public ConcurrentBag<MapDesignObject> MapDesignObjects = new ConcurrentBag<MapDesignObject>();

        readonly ThreadSafeSortedList<int, int> _mapMonsterIds;

        readonly ThreadSafeSortedList<int, int> _mapNpcIds;

        readonly ThreadSafeSortedList<long, MapMonster> _monsters;

        readonly ThreadSafeSortedList<long, MapMonster> _delayedMonsters;

        readonly ThreadSafeSortedList<long, MapNpc> _npcs;

        readonly Random _random;

        bool _isSleeping;

        bool _isSleepingRequest;

        #endregion

        #region Properties

        public List<EventContainer> OnSpawnEvents { get; set; }

        public List<MapButton> Buttons { get; set; }

        public Clock Clock { get; set; }

        public ThreadSafeSortedList<long, MapItem> DroppedList { get; }

        public bool DropAllowed { get; set; }

        public InstanceBag InstanceBag { get; set; }

        public bool IsDancing { get; set; }

        public bool IsPvp { get; set; }

        public bool IsScriptedInstance
        {
            get =>
                MapInstanceType == MapInstanceType.TimeSpaceInstance ||
                MapInstanceType == MapInstanceType.RaidInstance;
        }

        public bool IsReputationMap
        {
            get
            {
                if (!IsScriptedInstance)
                    switch (Map.MapId)
                    {
                        case 134:
                        case 153:
                            return true;
                    }

                return false;
            }
        }

        public bool IsSleeping
        {
            get
            {
                if (_isSleepingRequest && !_isSleeping && LastUnregister.AddSeconds(value: 30) < DateTime.Now)
                {
                    _isSleeping = true;
                    _isSleepingRequest = false;
                    return true;
                }

                return _isSleeping;
            }
            set
            {
                if (value)
                {
                    _isSleepingRequest = true;
                }
                else
                {
                    _isSleeping = false;
                    _isSleepingRequest = false;
                }
            }
        }

        public long LastUserShopId { get; set; }

        public Map Map { get; set; }

        public byte MapIndexX { get; set; }

        public byte MapIndexY { get; set; }

        public Guid MapInstanceId { get; set; }

        public MapInstanceType MapInstanceType { get; set; }

        public byte MinLevel { get; set; }

        public byte MaxLevel { get; set; }

        public List<MapMonster> Monsters
        {
            get => _monsters.GetAllItems();
        }

        public List<MapMonster> DelayedMonsters
        {
            get => _delayedMonsters.GetAllItems();
        }

        public List<MapNpc> Npcs
        {
            get => _npcs.GetAllItems();
        }

        public ThreadSafeGenericList<ZoneEvent> OnAreaEntryEvents { get; set; }

        public List<Tuple<EventContainer, List<long>>> OnCharacterDiscoveringMapEvents { get; set; }

        public List<EventContainer> OnMapClean { get; set; }

        public ThreadSafeGenericList<EventContainer> OnMoveOnMapEvents { get; set; }

        public List<Portal> Portals { get; }

        public List<object> RemovedMobNpcList { get; set; }

        public List<ScriptedInstance> ScriptedInstances { get; set; }

        public bool ShopAllowed { get; set; }

        public List<EventContainer> UnlockEvents { get; set; }

        public Dictionary<long, MapShop> UserShops { get; }

        public List<EventWave> WaveEvents { get; set; }

        public int XpRate { get; set; }

        #endregion

        #region Methods

        public IEnumerable<BattleEntity> BattleEntities
        {
            get
            {
                return Sessions.Select(selector: s => s.Character?.BattleEntity)
                    .Concat(second: Mates.Select(selector: s => s.BattleEntity))
                    .Concat(second: Monsters.Select(selector: s => s.BattleEntity))
                    .Concat(second: Npcs.Select(selector: s => s.BattleEntity));
            }
        }

        public int DropRate { get; internal set; }
        public int InstanceMusic { get; internal set; }

        public void AddMonster(MapMonster monster)
        {
            _monsters[key: monster.MapMonsterId] = monster;
        }

        public void AddDelayedMonster(MapMonster monster)
        {
            _delayedMonsters[key: monster.MapMonsterId] = monster;
        }

        public void AddNpc(MapNpc npc)
        {
            _npcs[key: npc.MapNpcId] = npc;
        }

        public void DespawnMonster(int monsterVnum)
        {
            Parallel.ForEach(source: _monsters.Where(predicate: s => s.MonsterVNum == monsterVnum), body: monster =>
            {
                monster.SetDeathStatement();
                Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster, callerId: monster.MapMonsterId));
            });
        }

        public void DespawnMonster(MapMonster monster)
        {
            monster.SetDeathStatement();
            Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster, callerId: monster.MapMonsterId));
        }

        public void DropItemByMonster(long? owner, DropDto drop, short mapX, short mapY, bool isQuest = false)
        {
            try
            {
                var localMapX = mapX;
                var localMapY = mapY;
                var possibilities = new List<MapCell>();

                for (short x = -1; x < 2; x++)
                    for (short y = -1; y < 2; y++)
                        possibilities.Add(item: new MapCell { X = x, Y = y });

                foreach (var possibility in possibilities.OrderBy(keySelector: s => ServerManager.RandomNumber()))
                {
                    localMapX = (short)(mapX + possibility.X);
                    localMapY = (short)(mapY + possibility.Y);
                    if (!Map.IsBlockedZone(x: localMapX, y: localMapY)) break;
                }

                var droppedItem = new MonsterMapItem(x: localMapX, y: localMapY, itemVNum: drop.ItemVNum,
                    amount: drop.Amount, ownerId: owner ?? -1,
                    isQuest: isQuest);
                DroppedList[key: droppedItem.TransportId] = droppedItem;
                Broadcast(
                    packet:
                    $"drop {droppedItem.ItemVNum} {droppedItem.TransportId} {droppedItem.PositionX} {droppedItem.PositionY} {(droppedItem.GoldAmount > 1 ? droppedItem.GoldAmount : droppedItem.Amount)} {(isQuest ? 1 : 0)} {owner}");
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public void DropItems(List<Tuple<short, int, short, short>> list)
        {
            foreach (var drop in list)
            {
                var droppedItem = new MonsterMapItem(x: drop.Item3, y: drop.Item4, itemVNum: drop.Item1,
                    amount: drop.Item2);
                DroppedList[key: droppedItem.TransportId] = droppedItem;
                Broadcast(
                    packet:
                    $"drop {droppedItem.ItemVNum} {droppedItem.TransportId} {droppedItem.PositionX} {droppedItem.PositionY} {(droppedItem.GoldAmount > 1 ? droppedItem.GoldAmount : droppedItem.Amount)} 0 {droppedItem.OwnerId ?? -1}");
            }
        }

        public string GenerateMapDesignObjects()
        {
            var mlobjstring = "mltobj";
            var i = 0;
            foreach (var mp in MapDesignObjects)
            {
                mlobjstring += $" {mp.ItemInstance.ItemVNum}.{i}.{mp.MapX}.{mp.MapY}";
                i++;
            }

            return mlobjstring;
        }

        public IEnumerable<string> GenerateNPCShopOnMap()
        {
            return (from npc in Npcs
                    where npc.Shop != null
                    select
                        $"shop 2 {npc.MapNpcId} {npc.Shop.ShopId} {npc.Shop.MenuType} {npc.Shop.ShopType} {npc.Shop.Name}"
                )
                .ToList();
        }

        public IEnumerable<string> GeneratePlayerShopOnMap()
        {
            return UserShops.Select(selector: shop => $"pflag 1 {shop.Value.OwnerId} {shop.Key + 1}").ToList();
        }

        public string GenerateRsfn(bool isInit = false)
        {
            if (MapInstanceType == MapInstanceType.TimeSpaceInstance)
                return
                    $"rsfn {MapIndexX} {MapIndexY} {(isInit ? 1 : Monsters.Where(predicate: s => s.IsAlive).ToList().Count == 0 ? 0 : 1)}";
            return "";
        }

        public IEnumerable<string> GenerateUserShops()
        {
            return UserShops.Select(selector: shop => $"shop 1 {shop.Value.OwnerId} 1 3 0 {shop.Value.Name}").ToList();
        }

        public List<MapMonster> GetMonsterInRangeList(short mapX, short mapY, byte distance,
            bool attackGreaterDistance = false)
        {
            return _monsters.Where(predicate: s =>
                s.IsAlive && (!attackGreaterDistance
                    ? s.IsInRange(mapX: mapX, mapY: mapY, distance: distance)
                    : !s.IsInRange(mapX: mapX, mapY: mapY, distance: distance))).ToList();
        }

        public List<MapNpc> GetListNpcInRange(short mapX, short mapY, byte distance, bool attackGreaterDistance = false)
        {
            return _npcs.Where(predicate: s =>
                s.CurrentHp > 0 && (!attackGreaterDistance
                    ? s.IsInRange(mapX: mapX, mapY: mapY, distance: distance)
                    : !s.IsInRange(mapX: mapX, mapY: mapY, distance: distance))).ToList();
        }

        public List<Mate> GetListMateInRange(short mapX, short mapY, byte distance, bool attackGreaterDistance = false)
        {
            return Sessions.SelectMany(selector: s => s.Character?.Mates.Where(predicate: m =>
                (m.IsTeamMember || m.IsTemporalMate) && (!attackGreaterDistance
                    ? m.IsInRange(xCoordinate: mapX, yCoordinate: mapY, range: distance)
                    : !m.IsInRange(xCoordinate: mapX, yCoordinate: mapY, range: distance)))).ToList();
        }

        public IEnumerable<string> GetMapDesignObjectEffects()
        {
            return MapDesignObjects.Select(selector: mp => mp.GenerateEffect(removed: false)).ToList();
        }

        public List<string> GetMapItems()
        {
            var packets = new List<string>();
            Sessions.Where(predicate: s => s.Character?.InvisibleGm == false).ToList().ForEach(action: s =>
                s.Character.Mates.Where(predicate: m => m.IsTeamMember).ToList()
                    .ForEach(action: m => packets.Add(item: m.GenerateIn())));
            Portals.ForEach(action: s => packets.Add(item: s.GenerateGp()));
            ScriptedInstances.Where(predicate: s => s.Type == ScriptedInstanceType.TimeSpace).ToList()
                .ForEach(action: s => packets.Add(item: s.GenerateWp()));
            Monsters.ForEach(action: s =>
            {
                packets.Add(item: s.GenerateIn());
                if (s.IsBoss) packets.Add(item: s.GenerateBoss());
            });

            Npcs.ForEach(action: npc =>
            {
                packets.Add(item: npc.GenerateIn());

                if (npc.EffectDelay == 0)
                    if (npc.Effect > 0 && npc.EffectActivated)
                        packets.Add(item: $"eff {(byte)UserType.Npc} {npc.MapNpcId} {npc.Effect}");
            });

            packets.AddRange(collection: GenerateNPCShopOnMap());
            DroppedList.ForEach(action: s => packets.Add(item: s.GenerateIn()));
            Buttons.ForEach(action: s => packets.Add(item: s.GenerateIn()));
            packets.AddRange(collection: GenerateUserShops());
            packets.AddRange(collection: GeneratePlayerShopOnMap());
            return packets;
        }

        public MapMonster GetMonsterById(long mapMonsterId)
        {
            return _monsters[key: mapMonsterId];
        }

        public Mate GetMate(long mateTransportId)
        {
            return Sessions.SelectMany(selector: s => s.Character?.Mates)
                .FirstOrDefault(predicate: m => m.MateTransportId == mateTransportId);
        }

        public int GetNextMonsterId()
        {
            var nextId = _mapMonsterIds.Count > 0 ? _mapMonsterIds.Last() + 1 : 1;
            _mapMonsterIds[key: nextId] = nextId;
            return nextId;
        }

        public int GetNextNpcId()
        {
            var nextId = _mapNpcIds.Count > 0 ? _mapNpcIds.Last() + 1 : 1;
            while (ServerManager.Instance.GetShopByMapNpcId(mapNpcId: nextId) != null) nextId++;
            _mapNpcIds[key: nextId] = nextId;
            return nextId;
        }

        public MapNpc GetNpc(long mapNpcId)
        {
            return _npcs[key: mapNpcId];
        }

        public void LoadMonsters()
        {
            Parallel.ForEach(source: DaoFactory.MapMonsterDao.LoadFromMap(mapId: Map.MapId).ToList(), body: monster =>
            {
                var mapMonster = new MapMonster(input: monster);
                mapMonster.Initialize(currentMapInstance: this);
                var mapMonsterId = mapMonster.MapMonsterId;
                _monsters[key: mapMonsterId] = mapMonster;
                _mapMonsterIds[key: mapMonsterId] = mapMonsterId;
            });
        }

        public void LoadNpcs()
        {
            Parallel.ForEach(source: DaoFactory.MapNpcDao.LoadFromMap(mapId: Map.MapId).ToList(), body: npc =>
            {
                var mapNpc = new MapNpc(input: npc);
                mapNpc.Initialize(currentMapInstance: this);
                var mapNpcId = mapNpc.MapNpcId;
                _npcs[key: mapNpcId] = mapNpc;
                _mapNpcIds[key: mapNpcId] = mapNpcId;
            });
        }

        public void LoadPortals()
        {
            foreach (var portal in DaoFactory.PortalDao.LoadByMap(mapId: Map.MapId))
            {
                var p = new Portal(input: portal)
                {
                    SourceMapInstanceId = MapInstanceId
                };
                Portals.Add(item: p);
            }
        }

        public void MapClear()
        {
            Broadcast(packet: "mapclear");
            Parallel.ForEach(source: GetMapItems(), body: s => Broadcast(packet: s));
        }

        public MapItem PutItem(InventoryType type, short slot, short amount, ref ItemInstance inv,
            ClientSession session)
        {
            Logger.LogUserEventDebug(logEvent: "PUTITEM", caller: session.GenerateIdentity(),
                data: $"type: {type} slot: {slot} amount: {amount}");
            var random2 = Guid.NewGuid();
            MapItem droppedItem = null;
            var possibilities = new List<GridPos>();

            for (short x = -2; x < 3; x++)
                for (short y = -2; y < 3; y++)
                    possibilities.Add(item: new GridPos { X = x, Y = y });

            short mapX = 0;
            short mapY = 0;
            var niceSpot = false;
            foreach (var possibility in possibilities.OrderBy(keySelector: s => _random.Next()))
            {
                mapX = (short)(session.Character.PositionX + possibility.X);
                mapY = (short)(session.Character.PositionY + possibility.Y);
                if (!Map.IsBlockedZone(x: mapX, y: mapY))
                {
                    niceSpot = true;
                    break;
                }
            }

            if (niceSpot && amount > 0 && amount <= inv.Amount)
            {
                var newItemInstance = inv.DeepCopy();
                newItemInstance.Id = random2;
                newItemInstance.Amount = amount;
                droppedItem = new CharacterMapItem(x: mapX, y: mapY, itemInstance: newItemInstance);

                DroppedList[key: droppedItem.TransportId] = droppedItem;
                inv.Amount -= amount;

                if (ServerManager.Instance.Configuration.UseLogService)
                    LogServiceClient.Instance.LogPacket(logEntry: new PacketLogEntry
                    {
                        Sender = session.Character.Name,
                        SenderId = session.Character.CharacterId,
                        PacketType = LogType.Drop,
                        Packet =
                            $"[PUT_DROP]OldIIId: {inv.Id} NewIIId: {newItemInstance.Id} ItemVNum: {inv.ItemVNum} Amount: {amount} Rare: {inv.Rare} Upgrade: {inv.Upgrade}"
                    });
            }

            return droppedItem;
        }

        public void RemoveMapItem()
        {
            // take the data from list to remove it without having enumeration problems (ToList)
            try
            {
                var dropsToRemove =
                    DroppedList.Where(predicate: dl => dl.CreatedDate.AddMinutes(value: 3) < DateTime.Now);
                Parallel.ForEach(source: dropsToRemove, body: drop =>
                {
                    Broadcast(packet: StaticPacketHelper.Out(type: UserType.Object, callerId: drop.TransportId));
                    DroppedList.Remove(key: drop.TransportId);
                });
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public void RemoveMonster(MapMonster monsterToRemove)
        {
            _monsters.Remove(key: monsterToRemove.MapMonsterId);
        }

        public void RemoveDelayedMonster(MapMonster monsterToRemove)
        {
            _delayedMonsters.Remove(key: monsterToRemove.MapMonsterId);
        }

        public void RemoveNpc(MapNpc npcToRemove)
        {
            _npcs.Remove(key: npcToRemove.MapNpcId);
        }

        public void SpawnButton(MapButton parameter)
        {
            Buttons.Add(item: parameter);
            Broadcast(packet: parameter.GenerateIn());
        }

        public void ThrowItems(Tuple<int, short, byte, int, int, short> parameter)
        {
            var mon = Monsters.Find(match: s => s.MapMonsterId == parameter.Item1) ??
                      Monsters.Find(match: s => s.MonsterVNum == parameter.Item1);
            if (mon == null) return;
            var originX = mon.MapX;
            var originY = mon.MapY;
            short destX;
            short destY;
            int amount;
            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: parameter.Item6)).Subscribe(onNext: s =>
            {
                for (var i = 0; i < parameter.Item3; i++)
                {
                    amount = ServerManager.RandomNumber(min: parameter.Item4, max: parameter.Item5);
                    destX = (short)(originX + ServerManager.RandomNumber(min: -10, max: 10));
                    destY = (short)(originY + ServerManager.RandomNumber(min: -10, max: 10));
                    if (Map.IsBlockedZone(x: destX, y: destY))
                    {
                        destX = originX;
                        destY = originY;
                    }

                    var droppedItem = new MonsterMapItem(x: destX, y: destY, itemVNum: parameter.Item2, amount: amount);
                    DroppedList[key: droppedItem.TransportId] = droppedItem;
                    Broadcast(
                        packet:
                        $"throw {droppedItem.ItemVNum} {droppedItem.TransportId} {originX} {originY} {droppedItem.PositionX} {droppedItem.PositionY} {(droppedItem.GoldAmount > 1 ? droppedItem.GoldAmount : droppedItem.Amount)}");
                }
            });
        }

        internal void CreatePortal(Portal portal)
        {
            portal.SourceMapInstanceId = MapInstanceId;
            Portals.Add(item: portal);
            Broadcast(packet: portal.GenerateGp());
        }

        internal IEnumerable<Character> GetCharactersInRange(short mapX, short mapY, byte distance,
            bool attackGreaterDistance = false)
        {
            var characters = new List<Character>();
            var cl = Sessions.Where(predicate: s => s.HasSelectedCharacter && s.Character.Hp > 0);
            IEnumerable<ClientSession> clientSessions = cl as IList<ClientSession> ?? cl.ToList();
            for (var i = clientSessions.Count() - 1; i >= 0; i--)
                if (!attackGreaterDistance)
                {
                    if (Map.GetDistance(p: new MapCell { X = mapX, Y = mapY },
                        q: new MapCell
                        {
                            X = clientSessions.ElementAt(index: i).Character.PositionX,
                            Y = clientSessions.ElementAt(index: i).Character.PositionY
                        }) <= distance + 1) characters.Add(item: clientSessions.ElementAt(index: i).Character);
                }
                else
                {
                    if (Map.GetDistance(p: new MapCell { X = mapX, Y = mapY },
                        q: new MapCell
                        {
                            X = clientSessions.ElementAt(index: i).Character.PositionX,
                            Y = clientSessions.ElementAt(index: i).Character.PositionY
                        }) > distance) characters.Add(item: clientSessions.ElementAt(index: i).Character);
                }

            return characters;
        }

        public IEnumerable<BattleEntity> GetBattleEntitiesInRange(MapCell pos, byte distance)
        {
            return BattleEntities.Where(predicate: b => Map.GetDistance(p: b.GetPos(), q: pos) <= distance);
        }

        internal void RemoveMonstersTarget(long characterId)
        {
            Parallel.ForEach(source: Monsters.Where(predicate: m => m.Target?.MapEntityId == characterId),
                body: monster => monster.RemoveTarget());
        }

        internal void StartLife()
        {
            Observable.Interval(period: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: x =>
            {
                if (InstanceBag?.EndState == 0)
                {
                    Parallel.ForEach(source: WaveEvents, body: waveEvent =>
                    {
                        if (waveEvent?.LastStart.AddSeconds(value: waveEvent.Delay) <= DateTime.Now)
                        {
                            if (waveEvent.Offset == 0 && waveEvent.RunTimes > 0)
                            {
                                waveEvent.Events.ForEach(action: e => EventHelper.Instance.RunEvent(evt: e));
                                waveEvent.RunTimes--;
                            }

                            waveEvent.Offset = waveEvent.Offset > 0 ? (byte)(waveEvent.Offset - 1) : (byte)0;
                            waveEvent.LastStart = DateTime.Now;
                        }
                    });
                    try
                    {
                        if (!Monsters.Any(predicate: s =>
                                s.IsAlive && s.Owner?.Character == null && s.Owner?.Mate == null) &&
                            DelayedMonsters.Count == 0)
                        {
                            var onMapCleanCopy = OnMapClean.ToList();
                            onMapCleanCopy.ForEach(action: e => EventHelper.Instance.RunEvent(evt: e));
                            OnMapClean.RemoveAll(match: s => s != null && onMapCleanCopy.Contains(item: s));
                        }

                        if (!IsSleeping) RemoveMapItem();
                    }
                    catch (Exception e)
                    {
                        Logger.Error(ex: e);
                    }
                }
            });
        }

        internal int SummonMonster(MonsterToSummon summon)
        {
            var npcMonster = ServerManager.GetNpcMonster(npcVNum: summon.VNum);

            if (npcMonster != null)
            {
                var mapX = summon.SpawnCell.X;
                var mapY = summon.SpawnCell.Y;

                if (mapX == 0 && mapY == 0)
                {
                    var cell = Map.GetRandomPosition();

                    if (cell != null)
                    {
                        mapX = cell.X;
                        mapY = cell.Y;
                    }
                }

                var mapMonster = new MapMonster
                {
                    MonsterVNum = npcMonster.NpcMonsterVNum,
                    MapX = mapX,
                    MapY = mapY,
                    Position = 2,
                    MapId = Map.MapId,
                    IsMoving = summon.IsMoving,
                    MapMonsterId = GetNextMonsterId(),
                    ShouldRespawn = false,
                    OnNoticeEvents = summon.NoticingEvents,
                    UseSkillOnDamage = summon.UseSkillOnDamage,
                    OnSpawnEvents = summon.SpawnEvents,
                    IsTarget = summon.IsTarget,
                    Target = summon.Target,
                    IsBonus = summon.IsBonus,
                    IsBoss = summon.IsBoss,
                    NoticeRange = summon.NoticeRange,
                    Owner = summon.Owner,
                    AliveTime = summon.AliveTime,
                    AliveTimeMp = summon.AliveTimeMp,
                    BaseMaxHp = summon.MaxHp,
                    BaseMaxMp = summon.MaxMp
                };

                if (summon.HasDelay > 0) AddDelayedMonster(monster: mapMonster);

                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: summon.HasDelay))
                    .Subscribe(onNext: o =>
                    {
                        mapMonster.Initialize(currentMapInstance: this);
                        mapMonster.BattleEntity.OnDeathEvents.AddRange(collection: summon.DeathEvents);
                        mapMonster.IsHostile = summon.IsHostile;

                        AddMonster(monster: mapMonster);
                        Broadcast(packet: mapMonster.GenerateIn());
                        RemoveDelayedMonster(monsterToRemove: mapMonster);

                        if (summon.AfterSpawnEvents.Any())
                            summon.AfterSpawnEvents.ForEach(action: e =>
                                EventHelper.Instance.RunEvent(evt: e, monster: mapMonster));

                        if (summon.IsMeteorite) OnMeteoriteEvents(monsterToSummon: summon, mapMonster: mapMonster);
                    });

                return mapMonster.MapMonsterId;
            }

            return default;
        }

        internal void SummonMonsters(List<MonsterToSummon> monstersToSummon)
        {
            foreach (var monsterToSummon in monstersToSummon) SummonMonster(summon: monsterToSummon);
        }

        void OnMeteoriteEvents(MonsterToSummon monsterToSummon, MapMonster mapMonster)
        {
            var ski = mapMonster.Skills?.FirstOrDefault();

            if (ski?.Skill == null) return;

            Broadcast(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Monster,
                callerId: mapMonster.MapMonsterId, effectId: ski.Skill.CastEffect));

            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 2))
                .Subscribe(onNext: a =>
                {
                    Broadcast(packet: StaticPacketHelper.SkillUsed(type: UserType.Monster,
                        callerId: mapMonster.MapMonsterId, secondaryType: 3,
                        targetId: mapMonster.MapMonsterId,
                        skillVNum: ski.SkillVNum, cooldown: ski.Skill.Cooldown,
                        attackAnimation: ski.Skill.AttackAnimation, skillEffect: ski.Skill.Effect, x: mapMonster.MapX,
                        y: mapMonster.MapY,
                        isAlive: true, health: 0, damage: 0, hitmode: 0, skillType: ski.Skill.SkillType));

                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 500))
                        .Subscribe(onNext: b =>
                        {
                            Parallel.ForEach(
                                source: GetBattleEntitiesInRange(
                                    pos: new MapCell { X = mapMonster.MapX, Y = mapMonster.MapY },
                                    distance: ski.Skill.Range),
                                body: x =>
                                {
                                    if (x.Mate != null || x.MapNpc != null || x.MapMonster?.IsBoss == true
                                        || x.Character != null &&
                                        x.Character.CharacterId == mapMonster.Owner?.MapEntityId
                                        || x.MapMonster != null && monsterToSummon.Owner == null)
                                        return;

                                    int damage;

                                    if (x.MapMonster?.Owner is { } owner)
                                    {
                                        var hitMode = 0;
                                        var onyxWings = false;

                                        damage = DamageHelper.Instance.CalculateDamage(attacker: owner, defender: x,
                                            skill: ski.Skill, hitMode: ref hitMode,
                                            onyxWings: ref onyxWings);
                                    }
                                    else
                                    {
                                        damage = monsterToSummon.Damage;
                                    }

                                    x.GetDamage(damage: damage, damager: mapMonster.BattleEntity);

                                    if (x.Character != null)
                                        x.Character.Session?.SendPacket(packet: x.Character.GenerateStat());

                                    x.MapInstance.Broadcast(packet: x.GenerateDm(dmg: damage));

                                    if (x.Hp < 1)
                                    {
                                        x.MapInstance.Broadcast(packet: StaticPacketHelper.Die(callerType: x.UserType,
                                            callerId: x.MapEntityId,
                                            targetType: x.UserType, targetId: x.MapEntityId));

                                        if (x.Character != null)
                                            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1)).Subscribe(
                                                onNext: c =>
                                                    ServerManager.Instance.AskRevive(
                                                        characterId: x.Character.CharacterId));
                                    }
                                });
                        });

                    RemoveMonster(monsterToRemove: mapMonster);
                    Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                        callerId: mapMonster.MapMonsterId));
                });
        }

        internal int SummonNpc(NpcToSummon npcToSummon)
        {
            var npcMonster = ServerManager.GetNpcMonster(npcVNum: npcToSummon.VNum);
            if (npcMonster != null)
            {
                var mapNpc = new MapNpc
                {
                    NpcVNum = npcMonster.NpcMonsterVNum,
                    MapX = npcToSummon.SpawnCell.X,
                    MapY = npcToSummon.SpawnCell.Y,
                    Position = npcToSummon.Dir,
                    MapId = Map.MapId,
                    ShouldRespawn = false,
                    IsMoving = npcToSummon.Move,
                    MapNpcId = GetNextNpcId(),
                    Target = npcToSummon.Target,
                    IsMate = npcToSummon.IsMate,
                    IsTsReward = npcToSummon.IsTsReward,
                    IsProtected = npcToSummon.IsProtected
                };

                mapNpc.OnSpawnEvents = npcToSummon.SpawnEvents.ToList();
                mapNpc.Initialize(currentMapInstance: this);
                mapNpc.IsHostile = npcToSummon.IsHostile;
                mapNpc.BattleEntity.OnDeathEvents.AddRange(collection: npcToSummon.DeathEvents);
                AddNpc(npc: mapNpc);
                Broadcast(packet: mapNpc.GenerateIn());
                return mapNpc.MapNpcId;
            }

            return default;
        }

        internal void SummonNpcs(List<NpcToSummon> npcsToSummon)
        {
            foreach (var npcToSummon in npcsToSummon) SummonNpc(npcToSummon: npcToSummon);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _npcs.Dispose();
                _monsters.Dispose();
                _delayedMonsters.Dispose();
                _mapNpcIds.Dispose();
                _mapMonsterIds.Dispose();
                DroppedList.Dispose();
                foreach (var session in ServerManager.Instance.Sessions.Where(predicate: s =>
                    s.Character != null && s.Character.MapInstanceId == MapInstanceId))
                    ServerManager.Instance.ChangeMap(id: session.Character.CharacterId, mapId: session.Character.MapId,
                        mapX: session.Character.MapX, mapY: session.Character.MapY);
            }
        }

        #endregion
    }
}