﻿using OpenNos.Domain;

namespace OpenNos.GameObject.Battle
{
    public class MtListHitTarget
    {
        #region Instantiation

        public MtListHitTarget(UserType entityType, long targetId, TargetHitType targetHitType)
        {
            EntityType = entityType;
            TargetId = targetId;
            TargetHitType = targetHitType;
        }

        #endregion

        #region Properties

        public UserType EntityType { get; set; }

        public long TargetId { get; set; }

        public TargetHitType TargetHitType { get; set; }

        #endregion
    }
}