﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using OpenNos.Core.Threading;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.XMLModel.Events;
using OpenNos.XMLModel.Models.ScriptedInstance;
using OpenNos.XMLModel.Objects;

namespace OpenNos.GameObject
{
    public class ScriptedInstance : ScriptedInstanceDto
    {
        #region Members

        public readonly Dictionary<int, MapInstance> _mapInstanceDictionary = new Dictionary<int, MapInstance>();

        IDisposable _disposable;

        #endregion

        #region Instantiation

        public ScriptedInstance()
        {
        }

        public ScriptedInstance(ScriptedInstanceDto input)
        {
            MapId = input.MapId;
            PositionX = input.PositionX;
            PositionY = input.PositionY;
            Script = input.Script;
            ScriptedInstanceId = input.ScriptedInstanceId;
            Type = input.Type;
            QuestTimeSpaceId = input.QuestTimeSpaceId;
        }

        #endregion

        #region Properties

        public List<Gift> DrawItems { get; set; }

        public int FamExp { get; set; }

        public MapInstance FirstMap { get; set; }

        public List<Gift> GiftItems { get; set; }

        public long Gold { get; set; }

        public short Id { get; set; }

        public bool IsIndividual { get; set; }

        public short DailyEntries { get; set; }

        public InstanceBag InstanceBag { get; set; } = new InstanceBag();

#pragma warning disable CS0108 // '"ScriptedInstance.Label" blendet den vererbten Member "ScriptedInstanceDTO.Label" aus. Verwenden Sie das new-Schlüsselwort, wenn das Ausblenden vorgesehen war.
        public string Label { get; set; }
#pragma warning restore CS0108 // '"ScriptedInstance.Label" blendet den vererbten Member "ScriptedInstanceDTO.Label" aus. Verwenden Sie das new-Schlüsselwort, wenn das Ausblenden vorgesehen war.

        public bool IsGiantTeam { get; set; }

        public byte LevelMaximum { get; set; }

        public byte LevelMinimum { get; set; }

        public byte Lives { get; set; }

        public ScriptedInstanceModel Model { get; set; }

        public int MonsterAmount { get; internal set; }

        public string Name { get; set; }

        public int NpcAmount { get; internal set; }

        public int Reputation { get; set; }

        public List<Gift> RequiredItems { get; set; }

        public int RoomAmount { get; internal set; }

        public List<Gift> SpecialItems { get; set; }

        public short StartX { get; set; }

        public short StartY { get; set; }

        public int[] SpNeeded { get; set; }

        #endregion

        #region Methods

        public void Dispose()
        {
            Thread.Sleep(millisecondsTimeout: 10000);
            _mapInstanceDictionary.Values.ToList().ForEach(action: m => m.Dispose());
        }

        public string GenerateMainInfo()
        {
            return $"minfo 0 1 -1.0/0 -1.0/0 -1/0 -1.0/0 1 {InstanceBag.Lives + 1} 0";
        }

        public List<string> GenerateMinimap()
        {
            var lst = new List<string> { "rsfm 0 0 4 12" };
            _mapInstanceDictionary.Values.ToList().ForEach(action: s => lst.Add(item: s.GenerateRsfn(isInit: true)));
            return lst;
        }

        public string GenerateRbr()
        {
            var drawgift = "";
#pragma warning disable CS0219 // Die Variable "requireditem" ist zugewiesen, ihr Wert wird aber nie verwendet.
            var requireditem = "";
#pragma warning restore CS0219 // Die Variable "requireditem" ist zugewiesen, ihr Wert wird aber nie verwendet.
            var bonusitems = "";
            var specialitems = "";

            for (var i = 0; i < 5; i++)
            {
                var gift = DrawItems?.ElementAtOrDefault(index: i);
                drawgift += $" {(gift == null ? "-1.0" : $"{gift.VNum}.{gift.Amount}")}";
            }

            for (var i = 0; i < 2; i++)
            {
                var gift = SpecialItems?.ElementAtOrDefault(index: i);
                specialitems += $" {(gift == null ? "-1.0" : $"{gift.VNum}.{gift.Amount}")}";
            }

            for (var i = 0; i < 3; i++)
            {
                var gift = GiftItems?.ElementAtOrDefault(index: i);
                bonusitems += (i == 0 ? "" : " ") + (gift == null ? "-1.0" : $"{gift.VNum}.{gift.Amount}");
            }

            const int WinnerScore = 0;
            const string Winner = "";
            return
                $"rbr 0.0.0 4 15 {LevelMinimum}.{LevelMaximum} {RequiredItems?.Sum(selector: s => s.Amount)} {drawgift} {specialitems} {bonusitems} {WinnerScore}.{(WinnerScore > 0 ? Winner : "")} 0 0 {Name}\n{Label}";
        }

        public string GenerateWp()
        {
            return $"wp {PositionX} {PositionY} {ScriptedInstanceId} 0 {LevelMinimum} {LevelMaximum}";
        }

        public void LoadGlobals()
        {
            if (Script != null)
            {
                var serializer = new XmlSerializer(type: typeof(ScriptedInstanceModel));
                using (var textReader = new StringReader(s: Script))
                {
                    Model = (ScriptedInstanceModel)serializer.Deserialize(textReader: textReader);
                }

                if (Model?.Globals != null)
                {
                    RequiredItems = new List<Gift>();
                    DrawItems = new List<Gift>();
                    SpecialItems = new List<Gift>();
                    GiftItems = new List<Gift>();
                    SpNeeded = new[] { 0, 0, 0, 0, 0 };

                    // set the values
                    Id = Model.Globals.Id?.Value ?? 0;
                    IsIndividual = Model.Globals.IsIndividual?.Value ?? false;
                    DailyEntries = Model.Globals.DailyEntries?.Value ?? 0;
                    Gold = Model.Globals.Gold?.Value ?? 0;
                    Reputation = Model.Globals.Reputation?.Value ?? 0;
                    FamExp = Model.Globals.FamExp?.Value ?? 0;
                    StartX = Model.Globals.StartX?.Value ?? 0;
                    StartY = Model.Globals.StartY?.Value ?? 0;
                    Lives = Model.Globals.Lives?.Value ?? 0;
                    LevelMinimum = Model.Globals.LevelMinimum?.Value ?? 1;
                    LevelMaximum = Model.Globals.LevelMaximum?.Value ?? 99;
                    Name = Model.Globals.Name?.Value ?? "No Name";
                    Label = Model.Globals.Label?.Value ?? "No Description";
                    IsGiantTeam = Model.Globals.GiantTeam != null;
                    if (Model.Globals.RequiredItems != null)
                        foreach (var item in Model.Globals.RequiredItems)
                            RequiredItems.Add(item: new Gift(vnum: item.VNum, amount: item.Amount, design: item.Design,
                                isRareRandom: item.IsRandomRare));
                    if (Model.Globals.DrawItems != null)
                        foreach (var item in Model.Globals.DrawItems)
                            DrawItems.Add(item: new Gift(vnum: item.VNum, amount: item.Amount, design: item.Design,
                                isRareRandom: item.IsRandomRare));
                    if (Model.Globals.SpecialItems != null)
                        foreach (var item in Model.Globals.SpecialItems)
                            SpecialItems.Add(item: new Gift(vnum: item.VNum, amount: item.Amount, design: item.Design,
                                isRareRandom: item.IsRandomRare));
                    if (Model.Globals.GiftItems != null)
                        foreach (var item in Model.Globals.GiftItems)
                            GiftItems.Add(item: new Gift(vnum: item.VNum, amount: item.Amount, design: item.Design,
                                isRareRandom: item.IsRandomRare)
                            {
                                MinTeamSize = item.MinTeamSize,
                                MaxTeamSize = item.MaxTeamSize
                            });
                    SpNeeded[0] = Model.Globals.SpNeeded?.Adventurer ?? 0;
                    SpNeeded[1] = Model.Globals.SpNeeded?.Swordman ?? 0;
                    SpNeeded[2] = Model.Globals.SpNeeded?.Archer ?? 0;
                    SpNeeded[3] = Model.Globals.SpNeeded?.Magician ?? 0;
                    SpNeeded[4] = Model.Globals.SpNeeded?.MartialArtist ?? 0;
                }
            }
        }

        public void LoadScript(MapInstanceType mapinstancetype, Character creator)
        {
            if (Model != null)
            {
                InstanceBag = new InstanceBag();
                InstanceBag.Lives = Lives;
                if (Model.InstanceEvents?.CreateMap != null)
                    foreach (var createMap in Model.InstanceEvents.CreateMap)
                    {
                        var mapInstance = ServerManager.GenerateMapInstance(mapId: createMap.VNum,
                            type: mapinstancetype,
                            mapclock: InstanceBag, dropAllowed: createMap.DropAllowed, isScriptedInstance: true);
                        mapInstance.XpRate = createMap.XpRate;
                        mapInstance.MapIndexX = createMap.IndexX;
                        mapInstance.MapIndexY = createMap.IndexY;
                        mapInstance.InstanceBag.CreatorId = creator.CharacterId;
                        if (!_mapInstanceDictionary.ContainsKey(key: createMap.Map))
                            _mapInstanceDictionary.Add(key: createMap.Map, value: mapInstance);
                    }

                FirstMap = _mapInstanceDictionary.Values.FirstOrDefault();
                Observable.Timer(dueTime: TimeSpan.FromMinutes(value: 3)).Subscribe(onNext: x =>
                {
                    if (!InstanceBag.Lock)
                    {
                        _mapInstanceDictionary.Values.ToList().ForEach(action: m =>
                            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: m,
                                eventActionType: EventActionType.Scriptend, param: (byte)1)));
                        Dispose();
                    }
                });
                _disposable = Observable.Interval(period: TimeSpan.FromMilliseconds(value: 100)).Subscribe(onNext: x =>
                {
                    if (mapinstancetype == MapInstanceType.TimeSpaceInstance)
                    {
                        if (InstanceBag.DeadList.ToList()
                            .Any(predicate: s =>
                                InstanceBag.DeadList.Count(predicate: e => e == s) > InstanceBag.Lives))
                        {
                            _mapInstanceDictionary.Values.ToList().ForEach(action: m =>
                                EventHelper.Instance.RunEvent(
                                    evt: new EventContainer(mapInstance: m, eventActionType: EventActionType.Scriptend,
                                        param: (byte)3)));
                            Dispose();
                            _disposable.Dispose();
                        }

                        if (_mapInstanceDictionary.SelectMany(selector: s => s.Value?.Sessions).ToList()
                            .SelectMany(selector: s => s.Character.Mates)
                            .Any(predicate: s => s.IsTemporalMate && s.IsTsProtected && !s.IsAlive))
                        {
                            _mapInstanceDictionary.Values.ToList().ForEach(action: m =>
                                EventHelper.Instance.RunEvent(
                                    evt: new EventContainer(mapInstance: m, eventActionType: EventActionType.Scriptend,
                                        param: (byte)2)));
                            Dispose();
                            _disposable.Dispose();
                        }
                    }

                    if (InstanceBag.Clock.SecondsRemaining <= 0)
                    {
                        _mapInstanceDictionary.Values.ToList().ForEach(action: m =>
                            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: m,
                                eventActionType: EventActionType.Scriptend, param: (byte)2)));
                        Dispose();
                        _disposable.Dispose();
                    }
                });

                GenerateEvent(parentMapInstance: FirstMap).ForEach(action: e => EventHelper.Instance.RunEvent(evt: e));
            }
        }

        ThreadSafeGenericList<EventContainer> GenerateEvent(MapInstance parentMapInstance)
        {
            // Needs Optimization, look into it.
            var evts = new ThreadSafeGenericList<EventContainer>();

            if (Model.InstanceEvents.CreateMap != null)
                Parallel.ForEach(source: Model.InstanceEvents.CreateMap, body: createMap =>
                {
                    var mapInstance =
                        _mapInstanceDictionary.FirstOrDefault(predicate: s => s.Key == createMap.Map).Value ??
                        parentMapInstance;

                    if (mapInstance == null) return;

                    // SummonMonster
                    evts.AddRange(
                        value: SummonMonster(mapInstance: mapInstance, summonMonster: createMap.SummonMonster));

                    // SummonNpc
                    evts.AddRange(value: SummonNpc(mapInstance: mapInstance, summonNpc: createMap.SummonNpc));

                    // SpawnPortal
                    evts.AddRange(value: SpawnPortal(mapInstance: mapInstance, spawnPortal: createMap.SpawnPortal));

                    // SpawnButton
                    evts.AddRange(value: SpawnButton(mapInstance: mapInstance, spawnButton: createMap.SpawnButton));

                    // OnCharacterDiscoveringMap
                    evts.AddRange(value: OnCharacterDiscoveringMap(mapInstance: mapInstance, createMap: createMap));

                    // GenerateClock
                    if (createMap.GenerateClock != null)
                        evts.Add(value: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Clock, param: createMap.GenerateClock.Value));

                    // StartClock
                    if (createMap.StartClock != null)
                        evts.AddRange(value: StartClock(mapInstance: mapInstance, StartClock: createMap.StartClock));

                    // OnMoveOnMap
                    if (createMap.OnMoveOnMap != null)
                        Parallel.ForEach(source: createMap.OnMoveOnMap,
                            body: onMoveOnMap =>
                                evts.AddRange(value: OnMoveOnMap(mapInstance: mapInstance, onMoveOnMap: onMoveOnMap)));

                    // OnLockerOpen
                    if (createMap.OnLockerOpen != null)
                        evts.AddRange(value: OnLockerOpen(mapInstance: mapInstance,
                            onLockerOpen: createMap.OnLockerOpen));

                    // OnAreaEntry
                    if (createMap.OnAreaEntry != null)
                        foreach (var onAreaEntry in createMap.OnAreaEntry)
                        {
                            var onAreaEntryEvents = new List<EventContainer>();
                            if (onAreaEntry.SummonMonster != null)
                                onAreaEntryEvents.AddRange(collection: SummonMonster(mapInstance: mapInstance,
                                    summonMonster: onAreaEntry.SummonMonster));
                            evts.Add(value: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Setareaentry,
                                param: new ZoneEvent
                                {
                                    X = onAreaEntry.PositionX,
                                    Y = onAreaEntry.PositionY,
                                    Range = onAreaEntry.Range,
                                    Events = onAreaEntryEvents
                                }));
                        }

                    // SetButtonLockers
                    if (createMap.SetButtonLockers != null)
                        evts.Add(value: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Setbuttonlockers,
                            param: createMap.SetButtonLockers.Value));

                    // SetMonsterLockers
                    if (createMap.SetMonsterLockers != null)
                        evts.Add(value: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Setmonsterlockers,
                            param: createMap.SetMonsterLockers.Value));
                });

            return evts;
        }

        List<EventContainer> OnLockerOpen(MapInstance mapInstance, OnLockerOpen onLockerOpen)
        {
            var evts = new List<EventContainer>();

            if (onLockerOpen != null)
            {
                var onLockerOpenEvents = new List<EventContainer>();

                // GenerateMapClock
                if (onLockerOpen.GenerateMapClock != null)
                    onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Mapclock,
                        param: onLockerOpen.GenerateMapClock.Value));

                // GenerateClock
                if (onLockerOpen.GenerateClock != null)
                    onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Clock,
                        param: onLockerOpen.GenerateClock.Value));

                // StartClock
                if (onLockerOpen.StartClock != null)
                    onLockerOpenEvents.AddRange(collection: StartClock(mapInstance: mapInstance,
                        StartClock: onLockerOpen.StartClock));

                // StartMapClock
                if (onLockerOpen.StartMapClock != null)
                    onLockerOpenEvents.AddRange(collection: StartMapClock(mapInstance: mapInstance,
                        StartMapClock: onLockerOpen.StartMapClock));

                // StopClock
                if (onLockerOpen.StopClock != null)
                    onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Stopclock, param: null));

                // StopMapClock
                if (onLockerOpen.StopMapClock != null)
                    onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Stopmapclock, param: null));

                // ChangePortalType
                if (onLockerOpen.ChangePortalType != null)
                    onLockerOpenEvents.AddRange(collection: ChangePortalType(mapInstance: mapInstance,
                        ChangePortalType: new[] { onLockerOpen.ChangePortalType }));

                // SendMessage
                if (onLockerOpen.SendMessage != null)
                    foreach (var SendMessage in onLockerOpen.SendMessage)
                        onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Sendpacket,
                            param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value,
                                type: SendMessage.Type)));

                // RefreshMapItems
                if (onLockerOpen.RefreshMapItems != null)
                    onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Refreshmapitems, param: null));

                // Set Monster Lockers
                if (onLockerOpen.SetMonsterLockers != null)
                    onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Setmonsterlockers,
                        param: onLockerOpen.SetMonsterLockers.Value));
                // Set Button Lockers
                if (onLockerOpen.SetButtonLockers != null)
                    onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Setbuttonlockers,
                        param: onLockerOpen.SetButtonLockers.Value));

                // SummonMonster
                onLockerOpenEvents.AddRange(collection: SummonMonster(mapInstance: mapInstance,
                    summonMonster: onLockerOpen.SummonMonster, isChildMonster: true));

                // ClearMapMonsters
                if (onLockerOpen.ClearMapMonsters != null)
                    onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Clearmapmonsters, param: null));

                // StopMapWaves
                if (onLockerOpen.StopMapWaves != null)
                    onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Stopmapwaves, param: null));

                // End
                if (onLockerOpen.End != null)
                    onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Scriptend,
                        param: onLockerOpen.End.Type));

                // NpcDialog
                if (onLockerOpen.NpcDialog != null)
                    foreach (var npcdialog in onLockerOpen.NpcDialog)
                        onLockerOpenEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Npcdialog,
                            param: npcdialog.Value));

                // RefreshOnLockerOpen
                onLockerOpenEvents.AddRange(collection: OnLockerOpen(mapInstance: mapInstance,
                    onLockerOpen: onLockerOpen.RefreshOnLockerOpen));

                evts.Add(item: new EventContainer(mapInstance: mapInstance,
                    eventActionType: EventActionType.Registerevent,
                    param: new Tuple<string, List<EventContainer>>(item1: nameof(XMLModel.Events.OnLockerOpen),
                        item2: onLockerOpenEvents)));
            }

            return evts;
        }

        List<EventContainer> OnCharacterDiscoveringMap(MapInstance mapInstance,
            CreateMap createMap)
        {
            var evts = new List<EventContainer>();

            // OnCharacterDiscoveringMap
            if (createMap.OnCharacterDiscoveringMap != null)
            {
                var onDiscoverEvents = new List<EventContainer>();

                // GenerateMapClock
                if (createMap.OnCharacterDiscoveringMap.GenerateMapClock != null)
                    onDiscoverEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Mapclock,
                        param: createMap.OnCharacterDiscoveringMap.GenerateMapClock.Value));

                // GenerateClock
                if (createMap.OnCharacterDiscoveringMap.GenerateClock != null)
                    onDiscoverEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Clock,
                        param: createMap.OnCharacterDiscoveringMap.GenerateClock.Value));

                // StartClock
                if (createMap.OnCharacterDiscoveringMap.StartClock != null)
                    onDiscoverEvents.AddRange(collection: StartClock(mapInstance: mapInstance,
                        StartClock: createMap.OnCharacterDiscoveringMap.StartClock));

                // StartMapClock
                if (createMap.OnCharacterDiscoveringMap.StartMapClock != null)
                    onDiscoverEvents.AddRange(collection: StartMapClock(mapInstance: mapInstance,
                        StartMapClock: createMap.OnCharacterDiscoveringMap.StartMapClock));

                // StopClock
                if (createMap.OnCharacterDiscoveringMap.StopClock != null)
                    onDiscoverEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Stopclock, param: null));

                // StopMapClock
                if (createMap.OnCharacterDiscoveringMap.StopMapClock != null)
                    onDiscoverEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Stopmapclock, param: null));

                // NpcDialog
                if (createMap.OnCharacterDiscoveringMap.NpcDialog != null)
                    foreach (var npcdialog in createMap.OnCharacterDiscoveringMap.NpcDialog)
                        onDiscoverEvents.Add(
                            item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Npcdialog, param: npcdialog.Value));

                // SendMessage
                if (createMap.OnCharacterDiscoveringMap.SendMessage != null)
                    foreach (var SendMessage in createMap.OnCharacterDiscoveringMap.SendMessage)
                        onDiscoverEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Sendpacket,
                            param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value,
                                type: SendMessage.Type)));

                // SendPacket
                if (createMap.OnCharacterDiscoveringMap.SendPacket != null)
                    foreach (var sendpacket in createMap.OnCharacterDiscoveringMap.SendPacket)
                        onDiscoverEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Sendpacket,
                            param: sendpacket.Value));

                // Effect
                if (createMap.OnCharacterDiscoveringMap.Effect != null)
                    mapInstance.OnSpawnEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Effect,
                        param: new Tuple<short, int>(item1: createMap.OnCharacterDiscoveringMap.Effect.Value,
                            item2: createMap.OnCharacterDiscoveringMap.Effect.Delay)));

                // SummonMonster
                onDiscoverEvents.AddRange(collection: SummonMonster(mapInstance: mapInstance,
                    summonMonster: createMap.OnCharacterDiscoveringMap.SummonMonster));

                // SummonNpc
                onDiscoverEvents.AddRange(collection: SummonNpc(mapInstance: mapInstance,
                    summonNpc: createMap.OnCharacterDiscoveringMap.SummonNpc));

                // SpawnPortal
                onDiscoverEvents.AddRange(collection: SpawnPortal(mapInstance: mapInstance,
                    spawnPortal: createMap.OnCharacterDiscoveringMap.SpawnPortal));

                // OnMoveOnMap
                if (createMap.OnCharacterDiscoveringMap.OnMoveOnMap != null)
                    onDiscoverEvents.AddRange(collection: OnMoveOnMap(mapInstance: mapInstance,
                        onMoveOnMap: createMap.OnCharacterDiscoveringMap.OnMoveOnMap));

                // Set Monster Lockers
                if (createMap.OnCharacterDiscoveringMap.SetMonsterLockers != null)
                    onDiscoverEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Setmonsterlockers,
                        param: createMap.OnCharacterDiscoveringMap.SetMonsterLockers.Value));
                // Set Button Lockers
                if (createMap.OnCharacterDiscoveringMap.SetButtonLockers != null)
                    onDiscoverEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Setbuttonlockers,
                        param: createMap.OnCharacterDiscoveringMap.SetButtonLockers.Value));

                // RefreshRaidGoals
                if (createMap.OnCharacterDiscoveringMap.RefreshRaidGoals != null)
                    onDiscoverEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Refreshraidgoal, param: null));

                // OnMapClean
                onDiscoverEvents.AddRange(collection: OnMapClean(mapInstance: mapInstance,
                    onMapClean: createMap.OnCharacterDiscoveringMap.OnMapClean));

                // Wave
                if (createMap.OnCharacterDiscoveringMap.Wave != null)
                    onDiscoverEvents.AddRange(collection: Wave(mapInstance: mapInstance,
                        Wave: createMap.OnCharacterDiscoveringMap.Wave));

                evts.Add(item: new EventContainer(mapInstance: mapInstance,
                    eventActionType: EventActionType.Registerevent,
                    param: new Tuple<string, List<EventContainer>>(
                        item1: nameof(XMLModel.Events.OnCharacterDiscoveringMap),
                        item2: onDiscoverEvents)));
            }

            return evts;
        }

        List<EventContainer> OnMapClean(MapInstance mapInstance, OnMapClean onMapClean)
        {
            var evts = new List<EventContainer>();

            // OnMapClean
            if (onMapClean != null)
            {
                var onMapCleanEvents = new List<EventContainer>();

                // AddClockTime
                if (onMapClean.AddClockTime != null)
                    onMapCleanEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Addclocktime,
                        param: onMapClean.AddClockTime.Seconds));

                // AddMapClockTime
                if (onMapClean.AddMapClockTime != null)
                    onMapCleanEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Addmapclocktime,
                        param: onMapClean.AddMapClockTime.Seconds));

                // StopMapClock
                if (onMapClean.StopMapClock != null)
                    onMapCleanEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Stopmapclock, param: null));

                // StopMapWaves
                if (onMapClean.StopMapWaves != null)
                    onMapCleanEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Stopmapwaves, param: null));

                // ChangePortalType
                if (onMapClean.ChangePortalType != null)
                    onMapCleanEvents.AddRange(collection: ChangePortalType(mapInstance: mapInstance,
                        ChangePortalType: onMapClean.ChangePortalType));

                // RefreshMapItems
                if (onMapClean.RefreshMapItems != null)
                    onMapCleanEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Refreshmapitems, param: null));

                // SendMessage
                if (onMapClean.SendMessage != null)
                    foreach (var SendMessage in onMapClean.SendMessage)
                        onMapCleanEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Sendpacket,
                            param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value,
                                type: SendMessage.Type)));

                // SendPacket
                if (onMapClean.SendPacket != null)
                    foreach (var sendpacket in onMapClean.SendPacket)
                        onMapCleanEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Sendpacket,
                            param: sendpacket.Value));

                // NpcDialog
                if (onMapClean.NpcDialog != null)
                    foreach (var npcdialog in onMapClean.NpcDialog)
                        onMapCleanEvents.Add(
                            item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Npcdialog, param: npcdialog.Value));

                // SummonMonster
                onMapCleanEvents.AddRange(collection: SummonMonster(mapInstance: mapInstance,
                    summonMonster: onMapClean.SummonMonster));

                // RefreshOnMapClean
                onMapCleanEvents.AddRange(collection: OnMapClean(mapInstance: mapInstance,
                    onMapClean: onMapClean.RefreshOnMapClean));

                evts.Add(item: new EventContainer(mapInstance: mapInstance,
                    eventActionType: EventActionType.Registerevent,
                    param: new Tuple<string, List<EventContainer>>(item1: nameof(XMLModel.Events.OnMapClean),
                        item2: onMapCleanEvents)));
            }

            return evts;
        }


        List<EventContainer> OnMoveOnMap(MapInstance mapInstance, OnMoveOnMap onMoveOnMap)
        {
            var evts = new List<EventContainer>();

            // OnMoveOnMap
            if (onMoveOnMap != null)
            {
                var onMoveOnMapEvents = new List<EventContainer>();

                // GenerateMapClock
                if (onMoveOnMap.GenerateMapClock != null)
                    onMoveOnMapEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Mapclock,
                        param: onMoveOnMap.GenerateMapClock.Value));

                // GenerateClock
                if (onMoveOnMap.GenerateClock != null)
                    onMoveOnMapEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Clock,
                        param: onMoveOnMap.GenerateClock.Value));

                // StartClock
                if (onMoveOnMap.StartClock != null)
                    onMoveOnMapEvents.AddRange(collection: StartClock(mapInstance: mapInstance,
                        StartClock: onMoveOnMap.StartClock));

                // StartMapClock
                if (onMoveOnMap.StartMapClock != null)
                    onMoveOnMapEvents.AddRange(collection: StartMapClock(mapInstance: mapInstance,
                        StartMapClock: onMoveOnMap.StartMapClock));

                // StopClock
                if (onMoveOnMap.StopClock != null)
                    onMoveOnMapEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Stopclock, param: null));

                // StopMapClock
                if (onMoveOnMap.StopMapClock != null)
                    onMoveOnMapEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Stopmapclock, param: null));

                // SendMessage
                if (onMoveOnMap.SendMessage != null)
                    foreach (var SendMessage in onMoveOnMap.SendMessage)
                        onMoveOnMapEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Sendpacket,
                            param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value,
                                type: SendMessage.Type)));

                // SendPacket
                if (onMoveOnMap.SendPacket != null)
                    foreach (var sendpacket in onMoveOnMap.SendPacket)
                        onMoveOnMapEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Sendpacket,
                            param: sendpacket.Value));

                // Wave
                if (onMoveOnMap.Wave != null)
                    onMoveOnMapEvents.AddRange(collection: Wave(mapInstance: mapInstance, Wave: onMoveOnMap.Wave));

                // SummonMonster
                onMoveOnMapEvents.AddRange(collection: SummonMonster(mapInstance: mapInstance,
                    summonMonster: onMoveOnMap.SummonMonster));

                // SummonNpc
                onMoveOnMapEvents.AddRange(collection: SummonNpc(mapInstance: mapInstance,
                    summonNpc: onMoveOnMap.SummonNpc));

                // OnMapClean
                onMoveOnMapEvents.AddRange(collection: OnMapClean(mapInstance: mapInstance,
                    onMapClean: onMoveOnMap.OnMapClean));

                // Set Monster Lockers
                if (onMoveOnMap.SetMonsterLockers != null)
                    onMoveOnMapEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Setmonsterlockers,
                        param: onMoveOnMap.SetMonsterLockers.Value));
                // Set Button Lockers
                if (onMoveOnMap.SetButtonLockers != null)
                    onMoveOnMapEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Setbuttonlockers,
                        param: onMoveOnMap.SetButtonLockers.Value));

                // RefreshRaidGoals
                if (onMoveOnMap.RefreshRaidGoals != null)
                    onMoveOnMapEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Refreshraidgoal, param: null));

                evts.Add(item: new EventContainer(mapInstance: mapInstance,
                    eventActionType: EventActionType.Registerevent,
                    param: new Tuple<string, List<EventContainer>>(item1: nameof(XMLModel.Events.OnMoveOnMap),
                        item2: onMoveOnMapEvents)));
            }

            return evts;
        }

        List<EventContainer> Wave(MapInstance mapInstance, Wave[] Wave)
        {
            var evts = new List<EventContainer>();

            foreach (var wave in Wave)
            {
                var waveEvent = new List<EventContainer>();

                // SummonMonster
                waveEvent.AddRange(collection: SummonMonster(mapInstance: mapInstance,
                    summonMonster: wave.SummonMonster));

                // SummonNpc
                waveEvent.AddRange(collection: SummonNpc(mapInstance: mapInstance, summonNpc: wave.SummonNpc));

                // SendMessage
                if (wave.SendMessage != null)
                    foreach (var SendMessage in wave.SendMessage)
                        waveEvent.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Sendpacket,
                            param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value,
                                type: SendMessage.Type)));

                evts.Add(item: new EventContainer(mapInstance: mapInstance,
                    eventActionType: EventActionType.Registerwave,
                    param: new EventWave(delay: wave.Delay, events: waveEvent, offset: wave.Offset,
                        runtimes: wave.RunTimes > 0 ? wave.RunTimes : short.MaxValue)));
            }

            return evts;
        }

        List<EventContainer> StartClock(MapInstance mapInstance, StartClock StartClock)
        {
            var evts = new List<EventContainer>();

            var onStop = new List<EventContainer>();
            var onTimeout = new List<EventContainer>();

            // OnStop
            if (StartClock.OnStop != null)
                onStop.AddRange(collection: OnStop(mapInstance: mapInstance, OnStop: StartClock.OnStop));

            // OnTimeout
            if (StartClock.OnTimeout != null)
                onTimeout.AddRange(collection: OnTimeout(mapInstance: mapInstance, OnTimeout: StartClock.OnTimeout));

            evts.Add(item: new EventContainer(mapInstance: mapInstance, eventActionType: EventActionType.Startclock,
                param: new Tuple<List<EventContainer>, List<EventContainer>>(item1: onStop, item2: onTimeout)));

            return evts;
        }

        List<EventContainer> StartMapClock(MapInstance mapInstance, StartClock StartMapClock)
        {
            var evts = new List<EventContainer>();

            var onStop = new List<EventContainer>();
            var onTimeout = new List<EventContainer>();

            // OnStop
            if (StartMapClock.OnStop != null)
                onStop.AddRange(collection: OnStop(mapInstance: mapInstance, OnStop: StartMapClock.OnStop));

            // OnTimeout
            if (StartMapClock.OnTimeout != null)
                onTimeout.AddRange(collection: OnTimeout(mapInstance: mapInstance, OnTimeout: StartMapClock.OnTimeout));

            evts.Add(item: new EventContainer(mapInstance: mapInstance, eventActionType: EventActionType.Startmapclock,
                param: new Tuple<List<EventContainer>, List<EventContainer>>(item1: onStop, item2: onTimeout)));

            return evts;
        }

        List<EventContainer> OnStop(MapInstance mapInstance, OnStop OnStop)
        {
            var evts = new List<EventContainer>();

            if (OnStop.ChangePortalType != null)
                evts.AddRange(collection: ChangePortalType(mapInstance: mapInstance,
                    ChangePortalType: OnStop.ChangePortalType));
            if (OnStop.SendMessage != null)
                foreach (var SendMessage in OnStop.SendMessage)
                    evts.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value, type: SendMessage.Type)));
            if (OnStop.SendPacket != null)
                foreach (var sendpacket in OnStop.SendPacket)
                    evts.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Sendpacket, param: sendpacket.Value));
            if (OnStop.RefreshMapItems != null)
                evts.Add(item: new EventContainer(mapInstance: mapInstance,
                    eventActionType: EventActionType.Refreshmapitems, param: null));

            return evts;
        }

        List<EventContainer> OnTimeout(MapInstance mapInstance, OnTimeout OnTimeout)
        {
            var evts = new List<EventContainer>();

            if (OnTimeout.StopMapWaves != null)
                evts.Add(item: new EventContainer(mapInstance: mapInstance,
                    eventActionType: EventActionType.Stopmapwaves, param: null));
            if (OnTimeout.ChangePortalType != null)
                evts.AddRange(collection: ChangePortalType(mapInstance: mapInstance,
                    ChangePortalType: OnTimeout.ChangePortalType));
            if (OnTimeout.SendMessage != null)
                foreach (var SendMessage in OnTimeout.SendMessage)
                    evts.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Sendpacket,
                        param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value, type: SendMessage.Type)));
            if (OnTimeout.NpcDialog != null)
                foreach (var npcdialog in OnTimeout.NpcDialog)
                    evts.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Npcdialog, param: npcdialog.Value));
            if (OnTimeout.SendPacket != null)
                foreach (var sendpacket in OnTimeout.SendPacket)
                    evts.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Sendpacket, param: sendpacket.Value));
            if (OnTimeout.RefreshMapItems != null)
                evts.Add(item: new EventContainer(mapInstance: mapInstance,
                    eventActionType: EventActionType.Refreshmapitems, param: null));
            if (OnTimeout.End != null)
                evts.Add(item: new EventContainer(mapInstance: mapInstance, eventActionType: EventActionType.Scriptend,
                    param: OnTimeout.End.Type));
            // ClearMapMonsters
            if (OnTimeout.ClearMapMonsters != null)
                evts.Add(item: new EventContainer(mapInstance: mapInstance,
                    eventActionType: EventActionType.Clearmapmonsters, param: null));

            return evts;
        }

        List<EventContainer> SpawnButton(MapInstance mapInstance, SpawnButton[] spawnButton)
        {
            var evts = new List<EventContainer>();

            // SpawnButton
            if (spawnButton != null)
                foreach (var spawn in spawnButton)
                {
                    var positionX = spawn.PositionX;
                    var positionY = spawn.PositionY;

                    if (positionX == 0 || positionY == 0)
                    {
                        var cell = mapInstance?.Map?.GetRandomPosition();
                        if (cell != null)
                        {
                            positionX = cell.X;
                            positionY = cell.Y;
                        }
                    }

                    var button = new MapButton(id: spawn.Id, positionX: positionX, positionY: positionY,
                        enabledVNum: spawn.VNumEnabled, disabledVNum: spawn.VNumDisabled,
                        disableEvents: new List<EventContainer>(), enableEvents: new List<EventContainer>(),
                        firstEnableEvents: new List<EventContainer>());

                    // OnFirstEnable
                    if (spawn.OnFirstEnable != null)
                    {
                        var onFirst = new List<EventContainer>();

                        // SummonMonster
                        onFirst.AddRange(collection: SummonMonster(mapInstance: mapInstance,
                            summonMonster: spawn.OnFirstEnable.SummonMonster));

                        // Teleport
                        if (spawn.OnFirstEnable.Teleport != null)
                            onFirst.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Teleport,
                                param: new Tuple<short, short, short, short>(
                                    item1: spawn.OnFirstEnable.Teleport.PositionX,
                                    item2: spawn.OnFirstEnable.Teleport.PositionY,
                                    item3: spawn.OnFirstEnable.Teleport.DestinationX,
                                    item4: spawn.OnFirstEnable.Teleport.DestinationY)));

                        // RemoveButtonLocker
                        if (spawn.OnFirstEnable.RemoveButtonLocker != null)
                            onFirst.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Removebuttonlocker, param: null));

                        // RefreshRaidGoals
                        if (spawn.OnFirstEnable.RefreshRaidGoals != null)
                            onFirst.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Refreshraidgoal, param: null));

                        // SendMessage
                        if (spawn.OnFirstEnable.SendMessage != null)
                            foreach (var SendMessage in spawn.OnFirstEnable.SendMessage)
                                onFirst.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Sendpacket,
                                    param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value,
                                        type: SendMessage.Type)));

                        // OnMapClean
                        if (spawn.OnFirstEnable.OnMapClean != null)
                            onFirst.AddRange(collection: OnMapClean(mapInstance: mapInstance,
                                onMapClean: spawn.OnFirstEnable.OnMapClean));

                        // ChangePortalType
                        if (spawn.OnFirstEnable.ChangePortalType != null)
                            onFirst.AddRange(collection: ChangePortalType(mapInstance: mapInstance,
                                ChangePortalType: spawn.OnFirstEnable.ChangePortalType));

                        // RefreshMapItems
                        if (spawn.OnFirstEnable.RefreshMapItems != null)
                            onFirst.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Refreshmapitems, param: null));

                        // StopMapWaves
                        if (spawn.OnFirstEnable.StopMapWaves != null)
                            onFirst.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Stopmapwaves, param: null));

                        if (spawn.OnFirstEnable.NpcDialog != null)
                            foreach (var npcdialog in spawn.OnFirstEnable.NpcDialog)
                                onFirst.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Npcdialog,
                                    param: npcdialog.Value));

                        button.FirstEnableEvents.AddRange(collection: onFirst);
                    }

                    // OnEnable & Teleport
                    if (spawn.OnEnable?.Teleport != null)
                        button.EnableEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Teleport,
                            param: new Tuple<short, short, short, short>(item1: spawn.OnEnable.Teleport.PositionX,
                                item2: spawn.OnEnable.Teleport.PositionY, item3: spawn.OnEnable.Teleport.DestinationX,
                                item4: spawn.OnEnable.Teleport.DestinationY)));

                    if (spawn.OnEnable?.NpcDialog != null)
                        foreach (var npcdialog in spawn.OnEnable?.NpcDialog)
                            button.EnableEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Npcdialog,
                                param: npcdialog.Value));

                    // OnDisable & Teleport
                    if (spawn.OnDisable?.Teleport != null)
                        button.DisableEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Teleport,
                            param: new Tuple<short, short, short, short>(item1: spawn.OnDisable.Teleport.PositionX,
                                item2: spawn.OnDisable.Teleport.PositionY, item3: spawn.OnDisable.Teleport.DestinationX,
                                item4: spawn.OnDisable.Teleport.DestinationY)));

                    if (spawn.OnDisable?.NpcDialog != null)
                        foreach (var npcdialog in spawn.OnDisable?.NpcDialog)
                            button.DisableEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Npcdialog,
                                param: npcdialog.Value));

                    evts.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Spawnbutton, param: button));
                }

            return evts;
        }

        List<EventContainer> SpawnPortal(MapInstance mapInstance, SpawnPortal[] spawnPortal)
        {
            var evts = new List<EventContainer>();

            // SpawnPortal
            if (spawnPortal != null)
                foreach (var portalEvent in spawnPortal)
                {
                    _mapInstanceDictionary.TryGetValue(key: portalEvent.ToMap, value: out var destinationMap);
                    var portal = new Portal
                    {
                        PortalId = portalEvent.IdOnMap,
                        SourceX = portalEvent.PositionX,
                        SourceY = portalEvent.PositionY,
                        Type = portalEvent.Type,
                        DestinationX = portalEvent.ToX,
                        DestinationY = portalEvent.ToY,
                        DestinationMapId = (short)(destinationMap?.MapInstanceId == default ? -1 : 0),
                        SourceMapInstanceId = mapInstance.MapInstanceId,
                        DestinationMapInstanceId = destinationMap?.MapInstanceId ?? Guid.Empty
                    };

                    // OnTraversal
                    if (portalEvent.OnTraversal?.End != null)
                        portal.OnTraversalEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Scriptend,
                            param: portalEvent.OnTraversal.End.Type));

                    if (portalEvent.OnTraversal?.NpcDialog != null)
                        foreach (var npcdialog in portalEvent.OnTraversal.NpcDialog)
                            portal.OnTraversalEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Npcdialog,
                                param: npcdialog.Value));

                    evts.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Spawnportal, param: portal));
                }

            return evts;
        }

        List<EventContainer> SummonMonster(MapInstance mapInstance,
            SummonMonster[] summonMonster, bool isChildMonster = false)
        {
            var evts = new List<EventContainer>();

            // SummonMonster
            if (summonMonster != null)
                foreach (var summon in summonMonster)
                {
                    MonsterAmount++;
                    var monster = new MonsterToSummon(vnum: summon.VNum,
                        spawnCell: new MapCell { X = summon.PositionX, Y = summon.PositionY }, target: null,
                        move: summon.Move, isTarget: summon.IsTarget,
                        isBonus: summon.IsBonus, isHostile: summon.IsHostile, isBoss: summon.IsBoss);

                    //NoticeRange
                    monster.NoticeRange = summon.NoticeRange;

                    //HasDelay
                    monster.HasDelay = summon.HasDelay;

                    // Meteorite
                    monster.IsMeteorite = summon.IsMeteorite;
                    monster.Damage = summon.Damage;

                    // UseSkillOnDamage
                    summon.UseSkillOnDamage?.ToList()
                        .ForEach(action: s =>
                        {
                            monster.UseSkillOnDamage.Add(item: new UseSkillOnDamage
                            {
                                SkillVNum = s.SkillVNum,
                                HpPercent = s.HpPercent
                            });
                        });

                    // OnDeath
                    if (summon.OnDeath != null)
                    {
                        // ThrowItem
                        if (summon.OnDeath.ThrowItem != null)
                            foreach (var throwItem in summon.OnDeath.ThrowItem)
                                monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Throwitems,
                                    param: new Tuple<int, short, byte, int, int, short>(item1: -1,
                                        item2: throwItem.VNum,
                                        item3: throwItem.PackAmount == 0 ? (byte)1 : throwItem.PackAmount,
                                        item4: throwItem.MinAmount == 0 ? 1 : throwItem.MinAmount,
                                        item5: throwItem.MaxAmount == 0 ? 1 : throwItem.MaxAmount,
                                        item6: throwItem.Delay)));

                        // AddClockTime
                        if (summon.OnDeath.AddClockTime != null)
                            monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Addclocktime,
                                param: summon.OnDeath.AddClockTime.Seconds));

                        // AddMapClockTime
                        if (summon.OnDeath.AddMapClockTime != null)
                            monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Addmapclocktime,
                                param: summon.OnDeath.AddMapClockTime.Seconds));

                        // RemoveButtonLocker
                        if (summon.OnDeath.RemoveButtonLocker != null)
                            monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Removebuttonlocker,
                                param: null));

                        // RemoveMonsterLocker
                        if (summon.OnDeath.RemoveMonsterLocker != null)
                            monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Removemonsterlocker,
                                param: null));

                        // ChangePortalType
                        if (summon.OnDeath.ChangePortalType != null)
                            monster.DeathEvents.AddRange(collection: ChangePortalType(mapInstance: mapInstance,
                                ChangePortalType: summon.OnDeath.ChangePortalType));

                        // SendMessage
                        if (summon.OnDeath.SendMessage != null)
                            foreach (var SendMessage in summon.OnDeath.SendMessage)
                                monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Sendpacket,
                                    param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value,
                                        type: SendMessage.Type)));

                        // NpcDialog
                        if (summon.OnDeath.NpcDialog != null)
                            foreach (var npcdialog in summon.OnDeath.NpcDialog)
                                monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Npcdialog,
                                    param: npcdialog.Value));

                        // SendPacket
                        if (summon.OnDeath.SendPacket != null)
                            foreach (var sendpacket in summon.OnDeath.SendPacket)
                                monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Sendpacket,
                                    param: sendpacket.Value));

                        // RefreshRaidGoals
                        if (summon.OnDeath.RefreshRaidGoals != null)
                            monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Refreshraidgoal,
                                param: null));

                        // End
                        if (summon.OnDeath.End != null)
                            monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Scriptend,
                                param: summon.OnDeath.End.Type));

                        // StopMapClock
                        if (summon.OnDeath.StopMapClock != null)
                            monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Stopmapclock,
                                param: null));

                        // RefreshMapItems
                        if (summon.OnDeath.RefreshMapItems != null)
                            monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Refreshmapitems,
                                param: null));

                        // Wave
                        if (summon.OnDeath.Wave != null)
                            monster.DeathEvents.AddRange(collection: Wave(mapInstance: mapInstance,
                                Wave: summon.OnDeath.Wave));

                        // StopMapWaves
                        if (summon.OnDeath.StopMapWaves != null)
                            monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Stopmapwaves,
                                param: null));

                        // RemoveButtonLocker
                        if (summon.OnDeath.ClearMapMonsters != null)
                            monster.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Clearmapmonsters,
                                param: null));

                        // SummonMonster Child
                        if (!isChildMonster)
                            monster.DeathEvents.AddRange(
                                collection: SummonMonster(mapInstance: mapInstance,
                                    summonMonster: summon.OnDeath?.SummonMonster, isChildMonster: true));
                    }

                    // OnNoticing
                    if (summon.OnNoticing != null)
                    {
                        // Effect
                        if (summon.OnNoticing.Effect != null)
                            monster.NoticingEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Effect,
                                param: new Tuple<short, int>(item1: summon.OnNoticing.Effect.Value,
                                    item2: summon.OnNoticing.Effect.Delay)));

                        // Move
                        if (summon.OnNoticing.Move != null)
                        {
                            var events = new List<EventContainer>();

                            // Effect
                            if (summon.OnNoticing.Move.Effect != null)
                                events.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Effect,
                                    param: new Tuple<short, int>(item1: summon.OnNoticing.Move.Effect.Value,
                                        item2: summon.OnNoticing.Move.Effect.Delay)));

                            monster.NoticingEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Move,
                                param: new ZoneEvent
                                {
                                    X = summon.OnNoticing.Move.PositionX,
                                    Y = summon.OnNoticing.Move.PositionY,
                                    Events = events
                                }));
                        }

                        // SummonMonster Child
                        if (!isChildMonster)
                            monster.NoticingEvents.AddRange(collection: SummonMonster(mapInstance: mapInstance,
                                summonMonster: summon.OnDeath?.SummonMonster,
                                isChildMonster: true));
                    }

                    // SendMessage
                    if (summon.SendMessage != null)
                        foreach (var SendMessage in summon.SendMessage)
                            monster.SpawnEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Sendpacket,
                                param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value,
                                    type: SendMessage.Type)));

                    // Effect
                    if (summon.Effect != null)
                        monster.SpawnEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Effect,
                            param: new Tuple<short, int>(item1: summon.Effect.Value, item2: summon.Effect.Delay)));

                    // RemoveAfter
                    if (summon.RemoveAfter != null)
                        monster.AfterSpawnEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Removeafter,
                            param: summon.RemoveAfter.Value));

                    // Move to coords on spawn
                    if (summon.Roam != null)
                        monster.SpawnEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Move,
                            param: new ZoneEvent { X = summon.Roam.FirstX, Y = summon.Roam.FirstY }));

                    evts.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Spawnmonster, param: monster));
                }

            return evts;
        }

        List<EventContainer> SummonNpc(MapInstance mapInstance, SummonNpc[] summonNpc,
            bool isChildNpc = false)
        {
            var evts = new List<EventContainer>();

            if (summonNpc != null)
                foreach (var summon in summonNpc)
                {
                    var positionX = summon.PositionX;
                    var positionY = summon.PositionY;

                    if (positionX == 0 || positionY == 0)
                    {
                        var cell = mapInstance?.Map?.GetRandomPosition();
                        if (cell != null)
                        {
                            positionX = cell.X;
                            positionY = cell.Y;
                        }
                    }

                    NpcAmount++;
                    var npcToSummon = new NpcToSummon(vnum: summon.VNum,
                        spawnCell: new MapCell { X = positionX, Y = positionY }, target: -1,
                        isProtected: summon.IsProtected, isMate: summon.IsMate, move: summon.Move,
                        isHostile: summon.IsHostile, isTsReward: summon.IsTsReward)
                    {
                        Dir = summon.Dir
                    };

                    // OnDeath
                    if (summon.OnDeath != null)
                    {
                        // ThrowItem
                        if (summon.OnDeath.ThrowItem != null)
                            foreach (var throwItem in summon.OnDeath.ThrowItem)
                                npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Throwitems,
                                    param: new Tuple<int, short, byte, int, int, short>(item1: -1,
                                        item2: throwItem.VNum,
                                        item3: throwItem.PackAmount == 0 ? (byte)1 : throwItem.PackAmount,
                                        item4: throwItem.MinAmount == 0 ? 1 : throwItem.MinAmount,
                                        item5: throwItem.MaxAmount == 0 ? 1 : throwItem.MaxAmount,
                                        item6: throwItem.Delay)));

                        // AddClockTime
                        if (summon.OnDeath.AddClockTime != null)
                            npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Addclocktime,
                                param: summon.OnDeath.AddClockTime.Seconds));

                        // AddMapClockTime
                        if (summon.OnDeath.AddMapClockTime != null)
                            npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Addmapclocktime,
                                param: summon.OnDeath.AddMapClockTime.Seconds));

                        // RemoveButtonLocker
                        if (summon.OnDeath.RemoveButtonLocker != null)
                            npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Removebuttonlocker, param: null));

                        // RemoveMonsterLocker
                        if (summon.OnDeath.RemoveMonsterLocker != null)
                            npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Removemonsterlocker, param: null));

                        // ChangePortalType
                        if (summon.OnDeath.ChangePortalType != null)
                            npcToSummon.DeathEvents.AddRange(collection: ChangePortalType(mapInstance: mapInstance,
                                ChangePortalType: summon.OnDeath.ChangePortalType));

                        // SendMessage
                        if (summon.OnDeath.SendMessage != null)
                            foreach (var SendMessage in summon.OnDeath.SendMessage)
                                npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Sendpacket,
                                    param: UserInterfaceHelper.GenerateMsg(message: SendMessage.Value,
                                        type: SendMessage.Type)));

                        // NpcDialog
                        if (summon.OnDeath.NpcDialog != null)
                            foreach (var npcdialog in summon.OnDeath.NpcDialog)
                                npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Npcdialog,
                                    param: npcdialog.Value));

                        // SendPacket
                        if (summon.OnDeath.SendPacket != null)
                            foreach (var sendpacket in summon.OnDeath.SendPacket)
                                npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                    eventActionType: EventActionType.Sendpacket,
                                    param: sendpacket.Value));

                        // RefreshRaidGoals
                        if (summon.OnDeath.RefreshRaidGoals != null)
                            npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Refreshraidgoal,
                                param: null));

                        // End
                        if (summon.OnDeath.End != null)
                            npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Scriptend,
                                param: summon.OnDeath.End.Type));

                        // StopMapClock
                        if (summon.OnDeath.StopMapClock != null)
                            npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Stopmapclock,
                                param: null));

                        // RefreshMapItems
                        if (summon.OnDeath.RefreshMapItems != null)
                            npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Refreshmapitems,
                                param: null));

                        // Wave
                        if (summon.OnDeath.Wave != null)
                            npcToSummon.DeathEvents.AddRange(collection: Wave(mapInstance: mapInstance,
                                Wave: summon.OnDeath.Wave));

                        // StopMapWaves
                        if (summon.OnDeath.StopMapWaves != null)
                            npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Stopmapwaves,
                                param: null));

                        // RemoveButtonLocker
                        if (summon.OnDeath.ClearMapMonsters != null)
                            npcToSummon.DeathEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                                eventActionType: EventActionType.Clearmapmonsters, param: null));

                        // SummonNpc Child
                        if (!isChildNpc)
                            npcToSummon.DeathEvents.AddRange(collection: SummonNpc(mapInstance: mapInstance,
                                summonNpc: summon.OnDeath?.SummonNpc, isChildNpc: true));
                    }

                    // Effect
                    if (summon.Effect != null)
                        npcToSummon.SpawnEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Effect,
                            param: new Tuple<short, int>(item1: summon.Effect.Value, item2: summon.Effect.Delay)));

                    // Move to coords on spawn
                    if (summon.Roam != null)
                        npcToSummon.SpawnEvents.Add(item: new EventContainer(mapInstance: mapInstance,
                            eventActionType: EventActionType.Move,
                            param: new ZoneEvent { X = summon.Roam.FirstX, Y = summon.Roam.FirstY }));

                    evts.Add(item: new EventContainer(mapInstance: mapInstance,
                        eventActionType: EventActionType.Spawnnpc, param: npcToSummon));
                }

            return evts;
        }

        List<EventContainer> ChangePortalType(MapInstance mapInstance,
            ChangePortalType[] ChangePortalType)
        {
            var evts = new List<EventContainer>();

            foreach (var changePortalType in ChangePortalType)
            {
                if (changePortalType.Map != 0)
                {
                    _mapInstanceDictionary.TryGetValue(key: changePortalType.Map, value: out var destinationMap);
                    if (destinationMap != null) mapInstance = destinationMap;
                }

                evts.Add(item: new EventContainer(mapInstance: mapInstance,
                    eventActionType: EventActionType.Changeportaltype,
                    param: new Tuple<int, PortalType>(item1: changePortalType.IdOnMap,
                        item2: (PortalType)changePortalType.Type)));
            }

            return evts;
        }

        public void End()
        {
            if (InstanceBag.Lives - InstanceBag.DeadList.Count < 0)
            {
                foreach (var m in _mapInstanceDictionary.Values)
                    EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: m,
                        eventActionType: EventActionType.Scriptend, param: (byte)3));
                Dispose();
                _disposable.Dispose();
                return;
            }

            if (InstanceBag.Clock.SecondsRemaining > 0) return;
            foreach (var m in _mapInstanceDictionary.Values)
                EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: m,
                    eventActionType: EventActionType.Scriptend, param: (byte)1));
            Dispose();
            _disposable.Dispose();
        }

        #endregion
    }
}