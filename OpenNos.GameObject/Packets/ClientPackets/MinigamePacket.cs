﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("mg")]
    public class MinigamePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }
        [PacketIndex(index: 1)] public byte Id { get; set; }
        [PacketIndex(index: 2)] public short MinigameVNum { get; set; }

        [PacketIndex(index: 3)] public int? Point { get; set; }

        #endregion
    }
}