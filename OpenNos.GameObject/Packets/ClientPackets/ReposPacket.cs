﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("repos")]
    public class ReposPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte OldSlot { get; set; }

        [PacketIndex(index: 1)] public short Amount { get; set; }

        [PacketIndex(index: 2)] public byte NewSlot { get; set; }

        [PacketIndex(index: 3)] public bool PartnerBackpack { get; set; }

        #endregion
    }
}