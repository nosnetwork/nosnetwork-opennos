﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("wear")]
    public class WearPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte InventorySlot { get; set; }

        [PacketIndex(index: 1)] public byte Type { get; set; }

        #endregion
    }
}