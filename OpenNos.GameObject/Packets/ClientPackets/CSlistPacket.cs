﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("c_slist")]
    public class CSListPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Index { get; set; }


        [PacketIndex(index: 1)] public byte Filter { get; set; }

        #endregion
    }
}