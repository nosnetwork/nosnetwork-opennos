﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("mkraid")]
    public class MkRaidPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 1)] public short Parameter { get; set; }

        #endregion
    }
}