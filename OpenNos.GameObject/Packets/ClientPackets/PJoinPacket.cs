﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("pjoin")]
    public class PJoinPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public GroupRequestType RequestType { get; set; }

        [PacketIndex(index: 1)] public long CharacterId { get; set; }

        #endregion
    }
}