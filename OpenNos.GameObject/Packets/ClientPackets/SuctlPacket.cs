﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("suctl")]
    public class SuctlPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public int CastId { get; set; }

        [PacketIndex(index: 1)] public int Unknown2 { get; set; }

        [PacketIndex(index: 2)] public int MateTransportId { get; set; }

        [PacketIndex(index: 3)] public UserType TargetType { get; set; }

        [PacketIndex(index: 4)] public long TargetId { get; set; }
    }
}