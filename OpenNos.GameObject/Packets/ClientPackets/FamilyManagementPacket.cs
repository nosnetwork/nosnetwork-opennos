﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("fmg")]
    public class FamilyManagementPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public FamilyAuthority FamilyAuthorityType { get; set; }

        [PacketIndex(index: 1)] public long TargetId { get; set; }

        #endregion
    }
}