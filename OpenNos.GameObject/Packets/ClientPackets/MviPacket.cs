﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("mvi")]
    public class MviPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public InventoryType InventoryType { get; set; }

        [PacketIndex(index: 1)] public short Slot { get; set; }

        [PacketIndex(index: 2)] public short Amount { get; set; }

        [PacketIndex(index: 3)] public byte DestinationSlot { get; set; }

        #endregion
    }
}