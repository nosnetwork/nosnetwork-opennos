﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("up_gr")]
    public class UpgradePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte UpgradeType { get; set; }

        [PacketIndex(index: 1)] public InventoryType InventoryType { get; set; }

        [PacketIndex(index: 2)] public byte Slot { get; set; }

        [PacketIndex(index: 3)] public InventoryType? InventoryType2 { get; set; }

        [PacketIndex(index: 4)] public byte? Slot2 { get; set; }

        #endregion
    }
}