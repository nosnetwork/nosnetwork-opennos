﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("say_p")]
    public class SayPPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int PetId { get; set; }

        [PacketIndex(index: 1, SerializeToEnd = true)]
        public string Message { get; set; }

        #endregion
    }
}