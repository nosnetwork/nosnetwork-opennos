﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("s_carrier")]
    public class SpecialistHolderPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Slot { get; set; }

        [PacketIndex(index: 1)] public byte HolderSlot { get; set; }

        #endregion
    }
}