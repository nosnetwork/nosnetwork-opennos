﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("walk")]
    public class WalkPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short XCoordinate { get; set; }

        [PacketIndex(index: 1)] public short YCoordinate { get; set; }

        [PacketIndex(index: 2)] public short Unknown { get; set; }

        [PacketIndex(index: 3)] public short Speed { get; set; }

        #endregion
    }
}