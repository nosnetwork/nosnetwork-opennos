﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("csp")]
    public class CspPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public long CharacterId { get; set; }

        [PacketIndex(index: 1, serializeToEnd: true)]
        public string Message { get; set; }

        #endregion
    }
}