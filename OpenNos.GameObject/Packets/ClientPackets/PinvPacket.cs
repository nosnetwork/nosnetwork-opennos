﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("$pinv")]
    public class PinvPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        #endregion
    }
}