﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("u_s")]
    public class UseSkillPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int CastId { get; set; }

        [PacketIndex(index: 1)] public UserType UserType { get; set; }

        [PacketIndex(index: 2)] public int MapMonsterId { get; set; }

        [PacketIndex(index: 3)] public short? MapX { get; set; }

        [PacketIndex(index: 4)] public short? MapY { get; set; }

        public override string ToString()
        {
            return $"{CastId} {UserType} {MapMonsterId} {MapX} {MapY}";
        }

        #endregion
    }
}