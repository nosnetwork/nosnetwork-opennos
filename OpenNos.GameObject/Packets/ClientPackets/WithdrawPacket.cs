﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("withdraw")]
    public class WithdrawPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Slot { get; set; }

        [PacketIndex(index: 1)] public short Amount { get; set; }

        [PacketIndex(index: 2)] public bool PetBackpack { get; set; }

        #endregion
    }
}