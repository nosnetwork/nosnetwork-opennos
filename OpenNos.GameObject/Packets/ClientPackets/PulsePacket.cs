﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("pulse")]
    public class PulsePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Tick { get; set; }

        #endregion
    }
}