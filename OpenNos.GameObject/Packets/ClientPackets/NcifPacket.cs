﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("ncif")]
    public class NcifPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 1)] public long TargetId { get; set; }

        #endregion
    }
}