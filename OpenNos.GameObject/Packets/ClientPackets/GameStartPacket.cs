﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("game_start")]
    public class GameStartPacket : PacketDefinition
    {
    }
}