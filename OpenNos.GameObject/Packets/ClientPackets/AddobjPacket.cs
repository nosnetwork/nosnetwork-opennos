﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("addobj")]
    public class AddObjPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Slot { get; set; }
        [PacketIndex(index: 1)] public short PositionX { get; set; }
        [PacketIndex(index: 2)] public short PositionY { get; set; }

        #endregion
    }
}