﻿using OpenNos.Core.Interfaces.Packets.ClientPackets;
using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("Char_NEW")]
    public class CharacterCreatePacket : PacketDefinition, ICharacterCreatePacket
    {
        #region Properties

        [PacketIndex(index: 0)] public string Name { get; set; }

        [PacketIndex(index: 1)] public byte Slot { get; set; }

        [PacketIndex(index: 2)] public GenderType Gender { get; set; }

        [PacketIndex(index: 3)] public HairStyleType HairStyle { get; set; }

        [PacketIndex(index: 4)] public HairColorType HairColor { get; set; }

        public override string ToString()
        {
            return $"Create Character Name: {Name} Slot: {Slot}";
        }

        #endregion
    }
}