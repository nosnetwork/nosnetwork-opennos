﻿using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("mtlist_sub_packet")] // header will be ignored for serializing just sub list packets
    public class MultiTargetListSubPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public UserType TargetType { get; set; }

        [PacketIndex(index: 1)] public int TargetId { get; set; }
    }
}