﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader(":")]
    public class FamilyChatPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0, serializeToEnd: true)]
        public string Message { get; set; }

        #endregion
    }
}