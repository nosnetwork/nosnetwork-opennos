﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader(";")]
    public class GroupSayPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0, SerializeToEnd = true)]
        public string Message { get; set; }

        #endregion
    }
}