﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("qt")]
    public class QtPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Type { get; set; }

        [PacketIndex(index: 1)] public int Data { get; set; }

        #endregion
    }
}