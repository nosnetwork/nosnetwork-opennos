﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("shopping")]
    public class ShoppingPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 3)] public int NpcId { get; set; }

        #endregion
    }
}