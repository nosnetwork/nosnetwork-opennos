﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("preq")]
    public class PreqPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short? Parameter { get; set; }

        #endregion
    }
}