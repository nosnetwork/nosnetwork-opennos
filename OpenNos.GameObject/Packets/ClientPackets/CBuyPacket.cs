﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("c_buy")]
    public class CBuyPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int BazaarId { get; set; }

        [PacketIndex(index: 1)] public short VNum { get; set; }

        [PacketIndex(index: 2)] public short Amount { get; set; }

        [PacketIndex(index: 3)] public long Price { get; set; }

        #endregion
    }
}