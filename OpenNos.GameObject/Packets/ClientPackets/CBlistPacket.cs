﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("c_blist")]
    public class CBListPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Index { get; set; }

        [PacketIndex(index: 1)] public BazaarListType TypeFilter { get; set; }

        [PacketIndex(index: 2)] public byte SubTypeFilter { get; set; }

        [PacketIndex(index: 3)] public byte LevelFilter { get; set; }

        [PacketIndex(index: 4)] public byte RareFilter { get; set; }

        [PacketIndex(index: 5)] public byte UpgradeFilter { get; set; }

        [PacketIndex(index: 6)] public byte OrderFilter { get; set; }

        [PacketIndex(index: 7)] public byte Unknown1 { get; set; }

        [PacketIndex(index: 8, SerializeToEnd = true)]
        public string ItemVNumFilter { get; set; }

        #endregion
    }
}