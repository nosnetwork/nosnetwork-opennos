﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("n_run")]
    public class NRunPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Runner { get; set; }

        [PacketIndex(index: 1)] public short Type { get; set; }

        [PacketIndex(index: 2)] public short Value { get; set; }

        [PacketIndex(index: 3)] public int NpcId { get; set; }

        #endregion
    }
}