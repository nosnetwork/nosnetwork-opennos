﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("req_exc")]
    public class ExchangeRequestPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public RequestExchangeType RequestType { get; set; }

        [PacketIndex(index: 1)] public long CharacterId { get; set; }
    }
}