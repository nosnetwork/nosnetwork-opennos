﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("blins")]
    public class BlInsPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public long CharacterId { get; set; }

        #endregion
    }
}