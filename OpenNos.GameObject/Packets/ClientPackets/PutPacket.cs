﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("put")]
    public class PutPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public InventoryType InventoryType { get; set; }

        [PacketIndex(index: 1)] public byte Slot { get; set; }

        [PacketIndex(index: 2)] public short Amount { get; set; }

        #endregion
    }
}