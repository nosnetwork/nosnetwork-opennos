﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("bldel")]
    public class BlDelPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public long CharacterId { get; set; }

        #endregion
    }
}