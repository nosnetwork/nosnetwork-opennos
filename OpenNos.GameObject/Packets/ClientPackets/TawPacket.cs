﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("taw")]
    public class TawPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string Username { get; set; }

        #endregion
    }
}