﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("rxit")]
    public class RaidExitPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte State { get; set; }

        #endregion
    }
}