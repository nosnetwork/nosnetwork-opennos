﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("NoS0575")]
    public class LoginPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Number { get; set; }

        [PacketIndex(index: 1)] public string Name { get; set; }

        [PacketIndex(index: 2)] public string Password { get; set; }

        [PacketIndex(index: 4)] public string ClientDataOld { get; set; }

        [PacketIndex(index: 5)] public string ClientData { get; set; }

        #endregion
    }
}