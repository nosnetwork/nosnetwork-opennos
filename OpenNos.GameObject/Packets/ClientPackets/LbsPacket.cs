﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("lbs")]
    public class LbsPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Type { get; set; }

        #endregion
    }
}