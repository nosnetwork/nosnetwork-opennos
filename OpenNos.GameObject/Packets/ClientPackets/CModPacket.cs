﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("c_mod")]
    public class CModPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public long BazaarId { get; set; }

        [PacketIndex(index: 1)] public short ItemVNum { get; set; }

        [PacketIndex(index: 2)] public short Amount { get; set; }

        [PacketIndex(index: 3)] public long Price { get; set; }

        #endregion
    }
}