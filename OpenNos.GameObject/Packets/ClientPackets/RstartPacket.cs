﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("rstart")]
    public class RStartPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte? Type { get; set; }

        #endregion
    }
}