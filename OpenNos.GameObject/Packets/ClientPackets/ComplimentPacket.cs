﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("compl")]
    public class ComplimentPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 1)] public long CharacterId { get; set; }

        #endregion
    }
}