﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("exc_list")]
    public class ExchangeListPacket
    {
        [PacketIndex(index: 1)] public long CharacterId { get; set; }

        [PacketIndex(index: 2)] public long Gold { get; set; }
    }
}