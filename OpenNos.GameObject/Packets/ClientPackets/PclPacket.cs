﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("pcl")]
    public class PclPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Type { get; set; }

        [PacketIndex(index: 1)] public int Unknown { get; set; }

        #endregion
    }
}