﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("f_deposit")]
    public class FDepositPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public InventoryType Inventory { get; set; }

        [PacketIndex(index: 1)] public byte Slot { get; set; }

        [PacketIndex(index: 2)] public short Amount { get; set; }

        [PacketIndex(index: 3)] public byte NewSlot { get; set; }

        [PacketIndex(index: 4)] public byte? Unknown { get; set; }

        #endregion
    }
}