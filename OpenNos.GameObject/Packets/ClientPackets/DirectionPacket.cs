﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("dir")]
    public class DirectionPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 1)] public int Option { get; set; }

        [PacketIndex(index: 0)] public byte Direction { get; set; }

        [PacketIndex(index: 2)] public long CharacterId { get; set; }

        #endregion
    }
}