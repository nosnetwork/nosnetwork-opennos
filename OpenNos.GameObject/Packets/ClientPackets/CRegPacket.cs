﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("c_reg")]
    public class CRegPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Type { get; set; }
        [PacketIndex(index: 1)] public byte Inventory { get; set; }
        [PacketIndex(index: 2)] public byte Slot { get; set; }
        [PacketIndex(index: 3)] public int Unknown1 { get; set; }
        [PacketIndex(index: 4)] public int Unknown2 { get; set; }
        [PacketIndex(index: 5)] public byte Durability { get; set; }
        [PacketIndex(index: 6)] public int IsPackage { get; set; }
        [PacketIndex(index: 7)] public short Amount { get; set; }
        [PacketIndex(index: 8)] public long Price { get; set; }
        [PacketIndex(index: 9)] public int Taxes { get; set; }
        [PacketIndex(index: 10)] public byte MedalUsed { get; set; }

        #endregion
    }
}