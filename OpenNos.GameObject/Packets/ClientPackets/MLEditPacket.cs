﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("mledit")]
    public class MLEditPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 1, SerializeToEnd = true)]
        public string Parameters { get; set; }

        #endregion
    }
}