﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("gop")]
    public class CharacterOptionPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public CharacterOption Option { get; set; }

        [PacketIndex(index: 1)] public bool IsActive { get; set; }
    }
}