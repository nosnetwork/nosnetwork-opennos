﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("ptctl")]
    public class PtCtlPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short MapType { get; set; }

        [PacketIndex(index: 1)] public short Amount { get; set; }

        [PacketIndex(index: 2, SerializeToEnd = true)]
        public string PacketEnd { get; set; }

        /*
         
        [PacketIndex(2, RemoveSeparator = true)]
        public List<PtCtlSubPacket> Users { get; set; }

        [PacketIndex(3)]
        public byte Option { get; set; }

         */

        #endregion
    }
}