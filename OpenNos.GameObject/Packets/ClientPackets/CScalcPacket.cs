﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("c_scalc")]
    public class CScalcPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public long BazaarId { get; set; }

        [PacketIndex(index: 1)] public short VNum { get; set; }

        [PacketIndex(index: 2)] public short Amount { get; set; }

        [PacketIndex(index: 3)] public short MaxAmount { get; set; }

        [PacketIndex(index: 4)] public long Price { get; set; }

        #endregion
    }
}