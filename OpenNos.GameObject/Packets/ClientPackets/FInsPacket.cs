﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("fins")]
    public class FInsPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 1)] public long CharacterId { get; set; }

        #endregion
    }
}