﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("pst")]
    public class PstPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Argument { get; set; }

        [PacketIndex(index: 1)] public int Type { get; set; }

        [PacketIndex(index: 2)] public long Id { get; set; }

        [PacketIndex(index: 3)] public int Unknown1 { get; set; }

        [PacketIndex(index: 4)] public int Unknown2 { get; set; }

        [PacketIndex(index: 5)] public string Receiver { get; set; }

        [PacketIndex(index: 6, SerializeToEnd = true)]
        public string Data { get; set; }

        #endregion
    }
}