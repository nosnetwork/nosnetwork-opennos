﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("qset")]
    public class QSetPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Type { get; set; }

        [PacketIndex(index: 1)] public short Q1 { get; set; }

        [PacketIndex(index: 2)] public short Q2 { get; set; }

        [PacketIndex(index: 3)] public short? Data1 { get; set; }

        [PacketIndex(index: 4)] public short? Data2 { get; set; }

        #endregion
    }
}