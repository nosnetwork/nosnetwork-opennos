﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("eqinfo")]
    public class EquipmentInfoPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 1)] public short Slot { get; set; }

        [PacketIndex(index: 2)] public short MateSlot { get; set; }

        [PacketIndex(index: 3)] public long? ShopOwnerId { get; set; }

        #endregion
    }
}