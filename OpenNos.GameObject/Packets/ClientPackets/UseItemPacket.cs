﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("u_i")]
    public class UseItemPacket : PacketDefinition
    {
        [PacketIndex(index: 2)] public InventoryType Type { get; set; }

        [PacketIndex(index: 3)] public short Slot { get; set; }
    }
}