﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("rl")]
    public class RlPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Type { get; set; }

        [PacketIndex(index: 1)] public string CharacterName { get; set; }

        #endregion
    }
}