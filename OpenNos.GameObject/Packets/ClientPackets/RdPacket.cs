﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("rd")]
    public class RdPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Type { get; set; }

        [PacketIndex(index: 1)] public long CharacterId { get; set; }

        [PacketIndex(index: 2)] public short? Parameter { get; set; }

        #endregion
    }
}