﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("revival")]
    public class RevivalPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        #endregion
    }
}