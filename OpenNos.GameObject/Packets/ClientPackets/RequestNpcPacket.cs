﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("npc_req")]
    public class RequestNpcPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Type { get; set; }

        [PacketIndex(index: 1)] public long Owner { get; set; }

        #endregion
    }
}