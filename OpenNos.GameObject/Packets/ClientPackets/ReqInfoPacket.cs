﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("req_info")]
    public class ReqInfoPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 1)] public long TargetVNum { get; set; }

        [PacketIndex(index: 2)] public int? MateVNum { get; set; }

        #endregion
    }
}