﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("Char_REN")]
    public class CharacterRenamePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Slot { get; set; }

        [PacketIndex(index: 1)] public string Name { get; set; }

        #endregion
    }
}