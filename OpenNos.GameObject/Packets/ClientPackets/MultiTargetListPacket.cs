﻿using System.Collections.Generic;
using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("mtlist")]
    public class MultiTargetListPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte TargetsAmount { get; set; }

        [PacketIndex(index: 1, RemoveSeparator = true)]
        public List<MultiTargetListSubPacket> Targets { get; set; }

        #endregion
    }

    [PacketHeader("mtlist_sub_packet")] // header will be ignored for serializing just sub list packets
    public class MultiTargetListSubPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public UserType TargetType { get; set; }

        [PacketIndex(index: 1)] public int TargetId { get; set; }
    }
}