﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("f_repos")]
    public class FReposPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte OldSlot { get; set; }

        [PacketIndex(index: 1)] public short Amount { get; set; }

        [PacketIndex(index: 2)] public byte NewSlot { get; set; }

        [PacketIndex(index: 3)] public byte? Unknown { get; set; }

        #endregion
    }
}