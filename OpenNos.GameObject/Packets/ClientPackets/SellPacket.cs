﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("sell")]
    public class SellPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 2)] public short Data { get; set; }

        [PacketIndex(index: 3)] public byte? Slot { get; set; }

        [PacketIndex(index: 4)] public short? Amount { get; set; }

        #endregion
    }
}