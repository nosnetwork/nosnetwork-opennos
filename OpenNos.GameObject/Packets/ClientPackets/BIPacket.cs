﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("b_i")]
    public class BIPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public InventoryType InventoryType { get; set; }

        [PacketIndex(index: 1)] public byte Slot { get; set; }

        [PacketIndex(index: 2)] public byte? Option { get; set; }

        #endregion
    }
}