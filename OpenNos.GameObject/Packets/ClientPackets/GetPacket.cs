﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("get")]
    public class GetPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte PickerType { get; set; }

        [PacketIndex(index: 1)] public int PickerId { get; set; }

        [PacketIndex(index: 2)] public long TransportId { get; set; }

        #endregion
    }
}