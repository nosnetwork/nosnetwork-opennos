﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("pt_ctl_sub_packet")] // header will be ignored for serializing just sub list packets
    public class PtCtlSubPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public long UserId { get; set; }

        [PacketIndex(index: 1)] public short UserX { get; set; }

        [PacketIndex(index: 2)] public short UserY { get; set; }
    }
}