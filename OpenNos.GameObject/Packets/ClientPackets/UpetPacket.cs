﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("u_pet")]
    public class UpetPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public int MateTransportId { get; set; }

        [PacketIndex(index: 1)] public UserType TargetType { get; set; }

        [PacketIndex(index: 2)] public int TargetId { get; set; }

        [PacketIndex(index: 3)] public int Unknown2 { get; set; }

        public override string ToString()
        {
            return $"{MateTransportId} {TargetType} {TargetId} 0";
        }
    }
}