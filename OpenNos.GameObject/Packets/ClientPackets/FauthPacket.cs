﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("fauth")]
    public class FAuthPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public FamilyAuthority MemberType { get; set; }

        [PacketIndex(index: 1)] public byte AuthorityId { get; set; }

        [PacketIndex(index: 2)] public byte Value { get; set; }

        #endregion
    }
}