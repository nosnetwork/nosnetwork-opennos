﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("u_ps")]
    public class UsePartnerSkillPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public long TransportId { get; set; }

        [PacketIndex(index: 1)] public UserType TargetType { get; set; }

        [PacketIndex(index: 2)] public long TargetId { get; set; }

        [PacketIndex(index: 3)] public byte CastId { get; set; }

        [PacketIndex(index: 4)] public short MapX { get; set; }

        [PacketIndex(index: 5)] public short MapY { get; set; }

        #endregion
    }
}