﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("sl")]
    public class SpTransformPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 3)] public int TransportId { get; set; }

        [PacketIndex(index: 4)] public short SpecialistDamage { get; set; }

        [PacketIndex(index: 5)] public short SpecialistDefense { get; set; }

        [PacketIndex(index: 6)] public short SpecialistElement { get; set; }

        [PacketIndex(index: 7)] public short SpecialistHP { get; set; }

        #endregion
    }
}