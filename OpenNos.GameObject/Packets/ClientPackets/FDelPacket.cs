﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("fdel")]
    public class FDelPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public long CharacterId { get; set; }

        #endregion
    }
}