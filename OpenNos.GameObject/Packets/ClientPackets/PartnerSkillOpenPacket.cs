﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("ps_op", PassNonParseablePacket = false)]
    public class PartnerSkillOpenPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte PetId { get; set; }

        [PacketIndex(index: 1)] public byte CastId { get; set; }

        [PacketIndex(index: 2)] public bool JustDoIt { get; set; }

        #endregion
    }
}