﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("mall")]
    public class MallPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Type { get; set; }

        #endregion
    }
}