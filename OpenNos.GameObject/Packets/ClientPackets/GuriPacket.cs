﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("guri")]
    public class GuriPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Type { get; set; }

        [PacketIndex(index: 1)] public int Argument { get; set; }

        [PacketIndex(index: 2)] public long User { get; set; }

        [PacketIndex(index: 3)] public int? Data { get; set; }

        [PacketIndex(index: 4, serializeToEnd: true)]
        public string Value { get; set; }

        #endregion
    }
}