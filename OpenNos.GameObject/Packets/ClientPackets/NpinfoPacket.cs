﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("npinfo")]
    public class NpinfoPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Page { get; set; }

        #endregion
    }
}