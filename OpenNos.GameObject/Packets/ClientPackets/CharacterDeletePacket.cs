﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("Char_DEL")]
    public class CharacterDeletePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Slot { get; set; }

        [PacketIndex(index: 1)] public string Password { get; set; }

        public override string ToString()
        {
            return $"Delete Character Slot {Slot}";
        }

        #endregion
    }
}