﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("git")]
    public class GitPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int ButtonId { get; set; }

        #endregion
    }
}