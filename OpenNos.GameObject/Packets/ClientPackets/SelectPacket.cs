﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("select")]
    public class SelectPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Slot { get; set; }

        #endregion
    }
}