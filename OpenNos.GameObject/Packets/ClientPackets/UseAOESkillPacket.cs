﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("u_as")]
    public class UseAOESkillPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int CastId { get; set; }

        [PacketIndex(index: 1)] public short MapX { get; set; }

        [PacketIndex(index: 2)] public short MapY { get; set; }

        #endregion
    }
}