﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("mve")]
    public class MvePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public InventoryType InventoryType { get; set; }

        [PacketIndex(index: 1)] public short Slot { get; set; }

        [PacketIndex(index: 2)] public InventoryType DestinationInventoryType { get; set; }

        [PacketIndex(index: 3)] public short DestinationSlot { get; set; }

        #endregion
    }
}