﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ClientPackets
{
    [PacketHeader("glist")]
    public class GListPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 1)] public byte Type { get; set; }

        #endregion
    }
}