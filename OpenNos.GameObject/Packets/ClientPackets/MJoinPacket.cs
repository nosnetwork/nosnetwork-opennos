﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("mjoin")]
    public class MJoinPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }
        [PacketIndex(index: 1)] public long CharacterId { get; set; }

        #endregion
    }
}