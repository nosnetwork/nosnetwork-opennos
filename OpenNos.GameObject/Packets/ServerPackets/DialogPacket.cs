﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("dlg")]
    public class DialogPacket<TAnswerYesPacket, TAnswerNoPacket> : PacketDefinition
        where TAnswerYesPacket : PacketDefinition
        where TAnswerNoPacket : PacketDefinition
    {
        [PacketIndex(index: 0, isReturnPacket: true)]
        public TAnswerYesPacket AnswerYesReturnPacket { get; set; }

        [PacketIndex(index: 1, isReturnPacket: true)]
        public TAnswerNoPacket AnswerNoReturnPacket { get; set; }

        [PacketIndex(index: 2, SerializeToEnd = true)]
        public string DialogText { get; set; }
    }
}