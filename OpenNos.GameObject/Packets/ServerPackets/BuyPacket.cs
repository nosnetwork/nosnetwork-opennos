﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("buy")]
    public class BuyPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public BuyShopType Type { get; set; }

        [PacketIndex(index: 1)] public long OwnerId { get; set; }

        [PacketIndex(index: 2)] public short Slot { get; set; }

        [PacketIndex(index: 3)] public short Amount { get; set; }

        public override string ToString()
        {
            return $"BuyShop {Type} {OwnerId} {Slot} {Amount}";
        }

        #endregion
    }
}