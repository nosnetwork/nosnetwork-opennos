﻿using OpenNos.Core;

namespace OpenNos.GameObject
{
    [PacketHeader("sub_equipment")] // actually no header rendered, avoid error
    public class EquipSubPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Index { get; set; }

        [PacketIndex(index: 1)] public int ItemVNum { get; set; }

        [PacketIndex(index: 2)] public byte Rare { get; set; }

        [PacketIndex(index: 4)] public byte Unknown { get; set; }

        [PacketIndex(index: 3)] public byte Upgrade { get; set; }

        #endregion
    }
}