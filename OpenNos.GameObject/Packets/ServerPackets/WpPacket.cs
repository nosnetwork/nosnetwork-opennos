﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("wp")]
    public class WpPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 2)] public short Id { get; set; }

        [PacketIndex(index: 5)] public short LevelMaximum { get; set; }

        [PacketIndex(index: 4)] public short LevelMinimum { get; set; }

        [PacketIndex(index: 3)] public short Unknown { get; set; }

        [PacketIndex(index: 0)] public short X { get; set; }

        [PacketIndex(index: 1)] public short Y { get; set; }

        #endregion
    }
}