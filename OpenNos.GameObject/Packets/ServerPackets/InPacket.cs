﻿using System.Collections.Generic;
using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("in")]
    public class InPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public InType InType { get; set; }

        [PacketIndex(index: 1)] public string Name { get; set; }

        [PacketIndex(index: 2)] public string UneccesaryNameOffset { get; set; }

        [PacketIndex(index: 3)] public long Id { get; set; }

        [PacketIndex(index: 4)] public short MapX { get; set; }

        [PacketIndex(index: 5)] public short MapY { get; set; }

        [PacketIndex(index: 6)] public byte Direction { get; set; }

        [PacketIndex(index: 7)] public byte Authority { get; set; }

        [PacketIndex(index: 8)] public byte Gender { get; set; }

        [PacketIndex(index: 9)] public byte Hairstyle { get; set; }

        [PacketIndex(index: 10)] public byte Color { get; set; }

        [PacketIndex(index: 11)] public ClassType Class { get; set; }

        [PacketIndex(index: 12)] public List<int?> Equipments { get; set; }

        [PacketIndex(index: 13)] public int CurrentHp { get; set; }

        [PacketIndex(index: 14)] public int CurrentMp { get; set; }

        [PacketIndex(index: 15)] public bool IsSitting { get; set; }

        [PacketIndex(index: 16)] public bool? HasGroup { get; set; }

        [PacketIndex(index: 17)] public byte? FairyMovement { get; set; }

        [PacketIndex(index: 18)] public byte FairyElement { get; set; }

        [PacketIndex(index: 20)] public byte FairyMorph { get; set; }

        [PacketIndex(index: 22)] public byte Morph { get; set; }

        [PacketIndex(index: 23)] public byte? WeaponUpgradeRarity { get; set; }

        [PacketIndex(index: 24)] public byte? ArmourUpgradeRarity { get; set; }

        [PacketIndex(index: 25)] public long? FamilyId { get; set; }

        [PacketIndex(index: 26)] public string FamilyName { get; set; }

        [PacketIndex(index: 27)] public byte Icon { get; set; }

        [PacketIndex(index: 28)] public bool IsInvisible { get; set; }

        [PacketIndex(index: 29)] public byte MorphUpgrade { get; set; }

        [PacketIndex(index: 31)] public byte MorphUpgrade2 { get; set; }

        [PacketIndex(index: 32)] public byte Level { get; set; }

        [PacketIndex(index: 33)] public byte FamilyLevel { get; set; }

        [PacketIndex(index: 34)] public int ArenaWinner { get; set; }

        [PacketIndex(index: 35)] public short Compliment { get; set; }

        [PacketIndex(index: 36)] public int Size { get; set; }

        [PacketIndex(index: 37)] public byte HeroLevel { get; set; }

        #endregion
    }
}