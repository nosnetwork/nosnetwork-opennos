﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.ServerPackets
{
    [PacketHeader("hero")]
    public class HeroPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Type { get; set; }

        [PacketIndex(index: 1)] public string CharacterName { get; set; }

        [PacketIndex(index: 2)] public string Message { get; set; }

        #endregion
    }
}