﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("mvi")]
    public class MviPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte InventoryId { get; set; }

        [PacketIndex(index: 1)] public byte SlotId { get; set; }

        [PacketIndex(index: 2)] public long ItemId { get; set; }

        [PacketIndex(index: 3)] public byte NewSlotId { get; set; }

        #endregion
    }
}