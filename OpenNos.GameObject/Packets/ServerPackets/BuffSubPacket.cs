﻿using OpenNos.Core;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("sub_buff")]
    public class BuffSubPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public int Value { get; set; } // uses left of buff, used for example for rarifying

        [PacketIndex(index: 1)] public int BuffId { get; set; }

        [PacketIndex(index: 2)] public int Duration { get; set; } // divided by 10
    }
}