﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("stat")]
    public class StatPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int CurrentHp { get; set; }

        [PacketIndex(index: 1)] public int MaxHp { get; set; }

        [PacketIndex(index: 2)] public short CurrentMp { get; set; }

        [PacketIndex(index: 3)] public short MaxMp { get; set; }

        [PacketIndex(index: 4)] public byte Unknown { get; set; }

        [PacketIndex(index: 5)] public short Options { get; set; }

        #endregion
    }
}