﻿using System.Collections.Generic;
using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("bf")]
    public class BfPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 1)] public long CharacterId { get; set; }

        [PacketIndex(index: 2)] public List<BuffSubPacket> BuffEntries { get; set; }

        [PacketIndex(index: 3)] public int CharacterLevel { get; set; }
    }

    [PacketHeader("sub_buff")]
    public class BuffSubPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public int Value { get; set; } // uses left of buff, used for example for rarifying

        [PacketIndex(index: 1)] public int BuffId { get; set; }

        [PacketIndex(index: 2)] public int Duration { get; set; } // divided by 10
    }
}