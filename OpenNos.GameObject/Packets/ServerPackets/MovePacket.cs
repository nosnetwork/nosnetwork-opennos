﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("mv")]
    public class MovePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public UserType MoveType { get; set; }

        [PacketIndex(index: 1)] public long CallerId { get; set; }

        [PacketIndex(index: 2)] public short PositionX { get; set; }

        [PacketIndex(index: 3)] public short PositionY { get; set; }

        [PacketIndex(index: 4)] public byte Speed { get; set; }

        #endregion
    }
}