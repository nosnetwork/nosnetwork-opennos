﻿using System.Collections.Generic;
using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("equip")]
    public class EquipPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte WeaponArmourUpgrade { get; set; }

        [PacketIndex(index: 1)] public byte Design { get; set; }

        [PacketIndex(index: 2)] public List<EquipSubPacket> EquipEntries { get; set; }

        #endregion
    }

    [PacketHeader("sub_equipment")] // actually no header rendered, avoid error
    public class EquipSubPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Index { get; set; }

        [PacketIndex(index: 1)] public int ItemVNum { get; set; }

        [PacketIndex(index: 2)] public byte Rare { get; set; }

        [PacketIndex(index: 4)] public byte Unknown { get; set; }

        [PacketIndex(index: 3)] public byte Upgrade { get; set; }

        #endregion
    }
}