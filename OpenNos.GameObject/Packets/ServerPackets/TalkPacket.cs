﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("talk")]
    public class TalkPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public long CharacterId { get; set; }

        [PacketIndex(index: 1)] public string Message { get; set; }

        #endregion
    }
}