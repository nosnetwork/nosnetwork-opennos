﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("vb")]
    public class VbPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte VbType { get; set; }

        [PacketIndex(index: 1)] public byte Unknown { get; set; }

        [PacketIndex(index: 2)] public long VbBuffTime { get; set; }

        #endregion
    }
}