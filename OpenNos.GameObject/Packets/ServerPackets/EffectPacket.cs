﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("eff")]
    public class EffectPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public UserType EffectType { get; set; }

        [PacketIndex(index: 1)] public long CallerId { get; set; }

        [PacketIndex(index: 2)] public int EffectId { get; set; }

        #endregion
    }
}