﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("ncif")]
    public class NcifPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte ObjectType { get; set; }

        [PacketIndex(index: 1)] public long ObjectId { get; set; }

        #endregion
    }
}