﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("remove")]
    public class RemovePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte EquipSlot { get; set; }

        [PacketIndex(index: 1)] public long MateId { get; set; }

        #endregion
    }
}