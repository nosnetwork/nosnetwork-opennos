﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("u_i")]
    public class UseItemPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte ObjectType { get; set; }

        [PacketIndex(index: 1)] public long ObjectId { get; set; }

        [PacketIndex(index: 2)] public byte Inventory { get; set; }

        [PacketIndex(index: 3)] public byte InventorySlot { get; set; }

        [PacketIndex(index: 4)] public byte Unknown { get; set; }

        [PacketIndex(index: 5)] public byte Unknown1 { get; set; }

        #endregion
    }
}