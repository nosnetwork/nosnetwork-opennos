﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("useobj")]
    public class UseObjPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string Name { get; set; }

        [PacketIndex(index: 1)] public long ObjectId { get; set; }

        #endregion
    }
}