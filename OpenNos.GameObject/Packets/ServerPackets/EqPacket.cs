﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("eq")]
    public class EqPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public long Id { get; set; }
    }

    [PacketHeader("eqsub")]
    public class EqSubPacket : PacketDefinition
    {
    }

    [PacketHeader("eqraresub")]
    public class EqRareSubPacket : PacketDefinition
    {
    }
}