﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("withdraw")]
    public class WithDrawPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte ShopSlot { get; set; }

        [PacketIndex(index: 1)] public byte InventoryId { get; set; }

        #endregion
    }
}