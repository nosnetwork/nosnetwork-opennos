﻿using System.Collections.Generic;
using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject
{
    [PacketHeader("clist")]
    public class CListPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Slot { get; set; }

        [PacketIndex(index: 1)] public string Name { get; set; }

        [PacketIndex(index: 2)] public byte Unknown { get; set; }

        [PacketIndex(index: 3)] public byte Gender { get; set; }

        [PacketIndex(index: 4)] public byte HairStyle { get; set; }

        [PacketIndex(index: 5)] public byte HairColor { get; set; }

        [PacketIndex(index: 6)] public byte Unknown1 { get; set; }

        [PacketIndex(index: 7)] public ClassType Class { get; set; }

        [PacketIndex(index: 8)] public byte Level { get; set; }

        [PacketIndex(index: 9)] public byte HeroLevel { get; set; }

        [PacketIndex(index: 10)] public List<short?> Equipments { get; set; }

        [PacketIndex(index: 11)] public byte JobLevel { get; set; }

        [PacketIndex(index: 12)] public byte QuestCompletion { get; set; }

        [PacketIndex(index: 13)] public byte QuestPart { get; set; }

        [PacketIndex(index: 14)] public List<short?> Pets { get; set; }

        [PacketIndex(index: 15)] public byte Unknown2 { get; set; }

        [PacketIndex(index: 16)] public byte Unknown3 { get; set; }

        #endregion
    }
}