﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("treq")]
    public class TreqPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 3)] public byte? RecordPress { get; set; }

        [PacketIndex(index: 2)] public byte? StartPress { get; set; }

        [PacketIndex(index: 0)] public int X { get; set; }

        [PacketIndex(index: 1)] public int Y { get; set; }

        #endregion
    }
}