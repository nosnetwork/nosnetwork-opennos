﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Packets.ServerPackets
{
    [PacketHeader("get")]
    public class GetPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 1)] public long ObjectId { get; set; }

        [PacketIndex(index: 2)] public short MapX { get; set; }

        [PacketIndex(index: 3)] public short SlotId { get; set; }

        [PacketIndex(index: 4)] public byte Number { get; set; }

        #endregion
    }
}