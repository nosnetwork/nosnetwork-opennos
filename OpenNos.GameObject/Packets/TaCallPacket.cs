﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("ta_call")]
    public class TaCallPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte CalledIndex { get; set; }

        #endregion
    }
}