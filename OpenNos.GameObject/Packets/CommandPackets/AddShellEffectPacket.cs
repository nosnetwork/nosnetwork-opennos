﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$AddShell", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class AddShellEffectPacket : PacketDefinition
    {
        #region Methods

        public static string ReturnHelp()
        {
            return "$AddShell <Slot> <EffectLevel> <Effect> <Value>";
        }

        #endregion

        #region Properties

        [PacketIndex(index: 0)] public byte Slot { get; set; }

        [PacketIndex(index: 1)] public byte EffectLevel { get; set; }

        [PacketIndex(index: 2)] public byte Effect { get; set; }

        [PacketIndex(index: 3)] public short Value { get; set; }

        #endregion
    }
}