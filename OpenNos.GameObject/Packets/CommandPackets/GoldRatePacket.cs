﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$GoldRate", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class GoldRatePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Value { get; set; }

        public static string ReturnHelp()
        {
            return "$GoldRate <Value>";
        }

        #endregion
    }
}