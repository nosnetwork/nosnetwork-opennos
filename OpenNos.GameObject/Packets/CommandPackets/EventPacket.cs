﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Event", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class EventPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public EventType EventType { get; set; }

        [PacketIndex(index: 1)] public int LvlBracket { get; set; }

        public static string ReturnHelp()
        {
            return "$Event <Event> <LevelBracket>";
        }

        #endregion
    }
}