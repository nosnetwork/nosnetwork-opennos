﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$FamilyFaction", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class FamilyFactionPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public string FamilyName { get; set; }

        public static string ReturnHelp()
        {
            return "$FamilyFaction <Name>";
        }
    }
}