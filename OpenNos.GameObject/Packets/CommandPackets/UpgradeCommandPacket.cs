﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Upgrade", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class UpgradeCommandPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Slot { get; set; }

        [PacketIndex(index: 1)] public UpgradeMode Mode { get; set; }

        [PacketIndex(index: 2)] public UpgradeProtection Protection { get; set; }

        public static string ReturnHelp()
        {
            return "$Upgrade <Slot> <Mode> <Protection>";
        }

        #endregion
    }
}