﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$AddUserLog", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class AddUserLogPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string Username { get; set; }

        #endregion

        #region Methods

        public static string ReturnHelp()
        {
            return "$AddUserLog <Username>";
        }

        #endregion
    }
}