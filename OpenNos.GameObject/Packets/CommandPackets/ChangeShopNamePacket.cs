﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ChangeShopName", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm })]
    public class ChangeShopNamePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string Name { get; set; }

        public static string ReturnHelp()
        {
            return "$ChangeShopName <Value>";
        }

        #endregion
    }
}