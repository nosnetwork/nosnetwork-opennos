﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$XpRate", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class XpRatePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Value { get; set; }

        public static string ReturnHelp()
        {
            return "$XpRate <Value>";
        }

        #endregion
    }
}