﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$PenaltyLog", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class PenaltyLogPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$PenaltyLog <Nickname>";
        }

        #endregion
    }
}