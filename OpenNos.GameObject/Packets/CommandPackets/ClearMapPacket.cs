﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ClearMap", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ClearMapPacket : PacketDefinition
    {
        #region Properties

        public static string ReturnHelp()
        {
            return "$ClearMap";
        }

        #endregion
    }
}