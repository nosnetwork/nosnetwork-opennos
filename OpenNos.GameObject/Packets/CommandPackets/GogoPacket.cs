﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Go", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class GogoPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short X { get; set; }

        [PacketIndex(index: 1)] public short Y { get; set; }

        public static string ReturnHelp()
        {
            return "$Go <ToX> <ToY>";
        }

        #endregion
    }
}