﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Maintenance", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class MaintenancePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Delay { get; set; }

        [PacketIndex(index: 1)] public int Duration { get; set; }

        [PacketIndex(index: 2, SerializeToEnd = true)]
        public string Reason { get; set; }

        public static string ReturnHelp()
        {
            return "$Maintenance <Delay> <Duration> <Reason>";
        }

        #endregion
    }
}