﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Gift", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class GiftPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        [PacketIndex(index: 1)] public short VNum { get; set; }

        [PacketIndex(index: 2)] public short Amount { get; set; }

        [PacketIndex(index: 3)] public sbyte Rare { get; set; }

        [PacketIndex(index: 4)] public byte Upgrade { get; set; }

        [PacketIndex(index: 5)] public short Design { get; set; }

        [PacketIndex(index: 6)] public short ReceiverLevelMin { get; set; }

        [PacketIndex(index: 7)] public short ReceiverLevelMax { get; set; }

        public static string ReturnHelp()
        {
            return "$Gift <Nickname|*> <VNum> <Amount> <Rare> <Upgrade> <Design> <MinimumLevel> <MaximumLevel>";
        }

        #endregion
    }
}