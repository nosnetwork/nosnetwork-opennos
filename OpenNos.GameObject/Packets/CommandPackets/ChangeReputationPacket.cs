﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ChangeReputation", "$Reputation", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm })]
    public class ChangeReputationPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public long Reputation { get; set; }

        public static string ReturnHelp()
        {
            return "$ChangeReputation | $Reputation <Value>";
        }

        #endregion
    }
}