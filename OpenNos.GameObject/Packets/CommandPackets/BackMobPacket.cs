﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$RollBack", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class BackMobPacket : PacketDefinition
    {
        #region Properties

        public static string ReturnHelp()
        {
            return "$RollBack";
        }

        #endregion
    }
}