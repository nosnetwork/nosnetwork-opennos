﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ChannelInfo", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ChannelInfoPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$ChannelInfo";
        }
    }
}