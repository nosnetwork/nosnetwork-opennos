﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$MapPVP", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class MapPVPPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$MapPVP";
        }
    }
}