﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$AddPet", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class AddPetPacket : PacketDefinition
    {
        #region Methods

        public static string ReturnHelp()
        {
            return "$AddPet <VNum> <Level>";
        }

        #endregion

        #region Properties

        [PacketIndex(index: 0)] public short MonsterVNum { get; set; }

        [PacketIndex(index: 1)] public byte Level { get; set; }

        #endregion
    }
}