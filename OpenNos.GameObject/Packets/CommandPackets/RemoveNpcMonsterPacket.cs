﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$RemoveMonster", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm })]
    public class RemoveMobPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$RemoveMonster";
        }
    }
}