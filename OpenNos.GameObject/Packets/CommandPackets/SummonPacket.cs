﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Monster", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class MobPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short NpcMonsterVNum { get; set; }

        [PacketIndex(index: 1)] public short Amount { get; set; }

        [PacketIndex(index: 2)] public bool IsMoving { get; set; }

        public static string ReturnHelp()
        {
            return "$Monster <VNum> <Amount> <IsMoving>";
        }

        #endregion
    }
}