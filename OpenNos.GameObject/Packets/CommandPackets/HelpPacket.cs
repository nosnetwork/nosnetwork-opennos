﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Help", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gs, AuthorityType.Gm, AuthorityType.Mod })]
    public class HelpPacket : PacketDefinition
    {
        [PacketIndex(index: 0, SerializeToEnd = true)]
        public string Contents { get; set; }
    }
}