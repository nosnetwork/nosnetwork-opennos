﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Morph", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class MorphPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int MorphId { get; set; }

        [PacketIndex(index: 1)] public byte Upgrade { get; set; }

        [PacketIndex(index: 2)] public byte MorphDesign { get; set; }

        [PacketIndex(index: 3)] public int ArenaWinner { get; set; }

        public static string ReturnHelp()
        {
            return "$Morph <VNum> <Upgrade> <Wings> <IsArenaWinner>";
        }

        #endregion
    }
}