﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$CreateItem", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class CreateItemPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short VNum { get; set; }

        [PacketIndex(index: 1)] public short? Design { get; set; }

        [PacketIndex(index: 2)] public byte? Upgrade { get; set; }

        public static string ReturnHelp()
        {
            return "$CreateItem <VNum> <Design|Rare|Amount|Wings> <Upgrade>";
        }

        #endregion
    }
}