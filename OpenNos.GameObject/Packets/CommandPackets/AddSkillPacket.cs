﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$AddSkill", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class AddSkillPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short SkillVNum { get; set; }

        public static string ReturnHelp()
        {
            return "$AddSkill <VNum>";
        }

        #endregion
    }
}