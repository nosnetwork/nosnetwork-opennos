﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ChangeSex", "$Sex", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class ChangeSexPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$ChangeSex | $Sex";
        }
    }
}