﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Perfection", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class SetPerfectionPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Slot { get; set; }

        [PacketIndex(index: 1)] public byte Type { get; set; }

        [PacketIndex(index: 2)] public byte Value { get; set; }

        public static string ReturnHelp()
        {
            return "$Perfection <Slot> <Type> <Value>";
        }

        #endregion
    }
}