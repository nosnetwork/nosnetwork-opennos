﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Faction", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class FactionPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$Faction";
        }
    }
}