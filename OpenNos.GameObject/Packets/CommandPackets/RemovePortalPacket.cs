﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$RemovePortal", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm })]
    public class RemovePortalPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$RemovePortal";
        }
    }
}