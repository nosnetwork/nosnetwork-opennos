﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ArenaWinner", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class ArenaWinnerPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$ArenaWinner";
        }
    }
}