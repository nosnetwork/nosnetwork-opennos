﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$CharEdit", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class CharacterEditPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string Property { get; set; }

        [PacketIndex(index: 1, serializeToEnd: true)]
        public string Data { get; set; }

        public static string ReturnHelp()
        {
            return "$CharEdit <Property> <Data>";
        }

        #endregion
    }
}