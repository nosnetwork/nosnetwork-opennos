﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$SPLvl", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ChangeSpecialistLevelPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte SpecialistLevel { get; set; }

        public static string ReturnHelp()
        {
            return "$SPLvl <Value>";
        }

        #endregion
    }
}