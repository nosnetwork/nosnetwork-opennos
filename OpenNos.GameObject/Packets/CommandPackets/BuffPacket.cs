﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Buff", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class BuffPacket : PacketDefinition
    {
        #region Methods

        public static string ReturnHelp()
        {
            return "$Buff <CardId> <?Level>";
        }

        #endregion

        #region Properties

        [PacketIndex(index: 0)] public short CardId { get; set; }

        [PacketIndex(index: 1)] public byte? Level { get; set; }

        #endregion
    }
}