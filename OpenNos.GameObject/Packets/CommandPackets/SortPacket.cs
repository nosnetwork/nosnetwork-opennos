﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Sort", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class SortPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public InventoryType? InventoryType { get; set; }

        public static string ReturnHelp()
        {
            return "$Sort <InventoryType>";
        }

        #endregion
    }
}