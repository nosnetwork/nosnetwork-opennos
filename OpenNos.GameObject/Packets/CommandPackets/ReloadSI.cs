﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ReloadSI", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class ReloadSIPacket : PacketDefinition
    {
        #region Properties

        public static string ReturnHelp()
        {
            return "$ReloadSI";
        }

        #endregion
    }
}