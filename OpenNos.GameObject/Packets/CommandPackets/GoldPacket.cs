﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Gold", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class GoldPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public long Amount { get; set; }

        public static string ReturnHelp()
        {
            return "$Gold <Value>";
        }

        #endregion
    }
}