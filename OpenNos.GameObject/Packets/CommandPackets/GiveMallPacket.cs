﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$GiveMall", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class GiveMallPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Amount { get; set; }

        [PacketIndex(index: 1)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$GiveMall <Amount> <Nickname>";
        }

        #endregion
    }
}