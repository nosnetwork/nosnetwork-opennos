﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Act4", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class Act4Packet : PacketDefinition
    {
        #region Properties

        public static string ReturnHelp()
        {
            return "$Act4";
        }

        #endregion
    }
}