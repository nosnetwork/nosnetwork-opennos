﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Home", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class HomePacket : PacketDefinition
    {
        #region Properties

        public static string ReturnHelp()
        {
            return "$Home";
        }

        #endregion
    }
}