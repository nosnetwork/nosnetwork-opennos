﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ResetMap", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class MapResetPacket : PacketDefinition
    {
        #region Properties

        public static string ReturnHelp()
        {
            return "$ResetMap";
        }

        #endregion
    }
}