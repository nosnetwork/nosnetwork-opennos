﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$MapDance", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class MapDancePacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$MapDance";
        }
    }
}