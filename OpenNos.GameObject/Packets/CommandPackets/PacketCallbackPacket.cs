﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Packet", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class PacketCallbackPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0, SerializeToEnd = true)]
        public string Packet { get; set; }

        public static string ReturnHelp()
        {
            return "$Packet <Value>";
        }

        #endregion
    }
}