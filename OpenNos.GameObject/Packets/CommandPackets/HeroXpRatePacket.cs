﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$HeroXpRate", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class HeroXpRatePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Value { get; set; }

        public static string ReturnHelp()
        {
            return "$HeroXpRate <Value>";
        }

        #endregion
    }
}