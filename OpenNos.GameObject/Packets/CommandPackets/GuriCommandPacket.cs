﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Guri", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class GuriCommandPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 1)] public byte Argument { get; set; }

        [PacketIndex(index: 2)] public int Value { get; set; }

        public static string ReturnHelp()
        {
            return "$Guri <Type> <Argument> <Value>";
        }

        #endregion
    }
}