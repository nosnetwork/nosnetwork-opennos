﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$JLvl", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ChangeJobLevelPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte JobLevel { get; set; }

        public static string ReturnHelp()
        {
            return "$JLvl <Value>";
        }

        #endregion
    }
}