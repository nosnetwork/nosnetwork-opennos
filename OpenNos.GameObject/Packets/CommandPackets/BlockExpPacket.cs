﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$BlockExp", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class BlockExpPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        [PacketIndex(index: 1)] public int Duration { get; set; }

        [PacketIndex(index: 2, SerializeToEnd = true)]
        public string Reason { get; set; }

        public static string ReturnHelp()
        {
            return "$BlockExp <Nickname> <Duration> <Reason>";
        }

        #endregion
    }
}