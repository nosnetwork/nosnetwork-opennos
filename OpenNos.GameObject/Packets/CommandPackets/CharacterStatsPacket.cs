﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$CharStat", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class CharacterStatsPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$CharStat <Nickname>";
        }

        #endregion
    }
}