﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$GoldDropRate", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm })]
    public class GoldDropRatePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Value { get; set; }

        public static string ReturnHelp()
        {
            return "$GoldDropRate <Value>";
        }

        #endregion
    }
}