﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$DropRate", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class DropRatePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Value { get; set; }

        public static string ReturnHelp()
        {
            return "$DropRate <Value>";
        }

        #endregion
    }
}