﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$MapStat", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class MapStatisticsPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short? MapId { get; set; }

        #endregion

        #region Methods

        public static string ReturnHelp()
        {
            return "$MapStat <?MapId>";
        }

        #endregion
    }
}