﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Rarify", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class RarifyPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 1)] public RarifyMode Mode { get; set; }

        [PacketIndex(index: 2)] public RarifyProtection Protection { get; set; }

        [PacketIndex(index: 0)] public short Slot { get; set; }

        public static string ReturnHelp()
        {
            return "$Rarify <Slot> <Mode> <Protection>";
        }

        #endregion
    }
}