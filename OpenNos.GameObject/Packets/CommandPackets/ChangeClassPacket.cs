﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ChangeClass", "$Class", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm })]
    public class ChangeClassPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public ClassType ClassType { get; set; }

        public static string ReturnHelp()
        {
            return
                "$ChangeClass | $Class <ClassType[0 = Adventurer, 1 = Swordsman, 2 = Archer, 3 = Mage, 4 = Martial Artist]>";
        }

        #endregion
    }
}