﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Lvl", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ChangeLevelPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Level { get; set; }

        public static string ReturnHelp()
        {
            return "$Lvl <Value>";
        }

        #endregion
    }
}