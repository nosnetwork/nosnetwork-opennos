﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Act4Stat", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class Act4StatPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Faction { get; set; }

        [PacketIndex(index: 1)] public int Value { get; set; }

        public static string ReturnHelp()
        {
            return "$Act4Stat <Faction> <Value>";
        }

        #endregion
    }
}