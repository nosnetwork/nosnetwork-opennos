﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Shout", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class ShoutPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0, SerializeToEnd = true)]
        public string Message { get; set; }

        public static string ReturnHelp()
        {
            return "$Shout <Message>";
        }

        #endregion
    }
}