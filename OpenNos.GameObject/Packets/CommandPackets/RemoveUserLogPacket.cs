﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$RemoveUserLog", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class RemoveUserLogPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string Username { get; set; }

        #endregion

        #region Methods

        public static string ReturnHelp()
        {
            return "$RemoveUserLog <Username>";
        }

        #endregion
    }
}