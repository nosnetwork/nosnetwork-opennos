﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Unmute", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class UnmutePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$Unmute <Nickname>";
        }

        #endregion
    }
}