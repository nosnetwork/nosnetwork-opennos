﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Undercover", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class UndercoverPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$Undercover";
        }
    }
}