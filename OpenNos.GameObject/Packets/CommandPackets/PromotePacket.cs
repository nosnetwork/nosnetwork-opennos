﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Promote", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class PromotePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$Promote <Nickname>";
        }

        #endregion
    }
}