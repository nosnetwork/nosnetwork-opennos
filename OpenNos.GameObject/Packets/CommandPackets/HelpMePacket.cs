﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$HelpMe", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class HelpMePacket : PacketDefinition
    {
        [PacketIndex(index: 0, SerializeToEnd = true)]
        public string Message { get; set; }

        #region Methods

        public static string ReturnHelp()
        {
            return "$HelpMe <Message>";
        }

        #endregion
    }
}