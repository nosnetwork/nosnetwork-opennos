﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ShutdownAll", PassNonParseablePacket = true,
        Authorities = new[] { AuthorityType.Administrator })]
    public class ShutdownAllPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public string WorldGroup { get; set; }

        public static string ReturnHelp()
        {
            return "$ShutdownAll <WorldGroup|*>";
        }
    }
}