﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$GlobalEvent", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class GlobalEventPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public EventType EventType { get; set; }

        public static string ReturnHelp()
        {
            return "$GlobalEvent <Type>";
        }

        #endregion
    }
}