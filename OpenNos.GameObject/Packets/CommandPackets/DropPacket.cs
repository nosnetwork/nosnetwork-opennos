﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Drop", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class DropPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short VNum { get; set; }

        [PacketIndex(index: 1)] public short Amount { get; set; }

        [PacketIndex(index: 2)] public int Count { get; set; }

        [PacketIndex(index: 3)] public int Time { get; set; }

        public static string ReturnHelp()
        {
            return "$Drop <VNum> <Amount> <Count> <Delay>";
        }

        #endregion
    }
}