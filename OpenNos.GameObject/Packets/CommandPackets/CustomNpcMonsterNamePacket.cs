﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$NpcName", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ChangeNpcMonsterNamePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string Name { get; set; }

        public static string ReturnHelp()
        {
            return "$NpcName <Value>";
        }

        #endregion
    }
}