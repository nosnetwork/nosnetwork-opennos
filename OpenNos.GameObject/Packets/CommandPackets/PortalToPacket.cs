﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Portal", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class PortalToPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short DestinationMapId { get; set; }

        [PacketIndex(index: 1)] public short DestinationX { get; set; }

        [PacketIndex(index: 2)] public short DestinationY { get; set; }

        [PacketIndex(index: 3)] public PortalType? PortalType { get; set; }

        public static string ReturnHelp()
        {
            return "$Portal <ToMapId> <ToX> <ToY> <?PortalType>";
        }

        #endregion
    }
}