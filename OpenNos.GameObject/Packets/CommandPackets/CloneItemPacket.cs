﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$CloneItem", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class CloneItemPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Slot { get; set; }

        #endregion

        public static string ReturnHelp()
        {
            return "$CloneItem <Slot>";
        }
    }
}