﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ServerInfo", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ServerInfoPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public int? ChannelId { get; set; }

        public static string ReturnHelp()
        {
            return "$ServerInfo <?ChannelId>";
        }
    }
}