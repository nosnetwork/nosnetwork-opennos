﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Zoom", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class ZoomPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Value { get; set; }

        public static string ReturnHelp()
        {
            return "$Zoom <Value>";
        }

        #endregion
    }
}