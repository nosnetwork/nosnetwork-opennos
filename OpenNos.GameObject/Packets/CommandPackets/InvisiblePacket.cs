﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Invisible", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class InvisiblePacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$Invisible";
        }
    }
}