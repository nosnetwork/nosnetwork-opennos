﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$RestartAll", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class RestartAllPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public string WorldGroup { get; set; }

        [PacketIndex(index: 1)] public int Time { get; set; }

        public static string ReturnHelp()
        {
            return "$RestartAll <WorldGroup|*> <?Delay>";
        }
    }
}