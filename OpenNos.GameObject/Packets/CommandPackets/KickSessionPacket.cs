﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$KickSession", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class KickSessionPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string AccountName { get; set; }

        [PacketIndex(index: 1)] public int? SessionId { get; set; }

        public static string ReturnHelp()
        {
            return "$KickSession <Username> <?SessionId>";
        }

        #endregion
    }
}