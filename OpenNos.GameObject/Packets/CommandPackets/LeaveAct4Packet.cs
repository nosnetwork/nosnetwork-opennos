﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$LeaveAct4", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class LeaveAct4Packet : PacketDefinition
    {
        #region Properties

        public static string ReturnHelp()
        {
            return "$LeaveAct4";
        }

        #endregion
    }
}