﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$AddMonster", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class AddMonsterPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short MonsterVNum { get; set; }

        [PacketIndex(index: 1)] public bool IsMoving { get; set; }

        public static string ReturnHelp()
        {
            return "$AddMonster <VNum> <IsMoving>";
        }

        #endregion
    }
}