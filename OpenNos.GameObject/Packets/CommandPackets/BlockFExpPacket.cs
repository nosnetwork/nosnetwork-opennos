﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$BlockFExp", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class BlockFExpPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        [PacketIndex(index: 1)] public int Duration { get; set; }

        [PacketIndex(index: 2, SerializeToEnd = true)]
        public string Reason { get; set; }

        public static string ReturnHelp()
        {
            return "$BlockFExp <Nickname> <Duration> <Reason>";
        }

        #endregion
    }
}