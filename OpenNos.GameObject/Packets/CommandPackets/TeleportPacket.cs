﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Teleport", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class TeleportPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string Data { get; set; }

        [PacketIndex(index: 1)] public short X { get; set; }

        [PacketIndex(index: 2)] public short Y { get; set; }

        public static string ReturnHelp()
        {
            return "$Teleport <Nickname|ToMapId> <?ToX> <?ToY>";
        }

        #endregion
    }
}