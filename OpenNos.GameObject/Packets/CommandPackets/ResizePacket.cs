﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Resize", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class ResizePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Value { get; set; }

        public static string ReturnHelp()
        {
            return "$Resize <Value>";
        }

        #endregion
    }
}