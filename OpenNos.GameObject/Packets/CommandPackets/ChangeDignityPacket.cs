﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ChangeDignity", "$Dignity", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class ChangeDignityPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public float Dignity { get; set; }

        public static string ReturnHelp()
        {
            return "$ChangeDignity | $Dignity <Value>";
        }

        #endregion
    }
}