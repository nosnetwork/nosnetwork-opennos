﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$FLvl", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ChangeFairyLevelPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short FairyLevel { get; set; }

        public static string ReturnHelp()
        {
            return "$FLvl <Value>";
        }

        #endregion
    }
}