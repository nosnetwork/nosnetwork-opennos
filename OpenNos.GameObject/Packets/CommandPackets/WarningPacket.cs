﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Warn", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gs, AuthorityType.Gm, AuthorityType.Mod })]
    public class WarningPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        [PacketIndex(index: 1, serializeToEnd: true)]
        public string Reason { get; set; }

        public static string ReturnHelp()
        {
            return "$Warn <Nickname> <Reason>";
        }

        #endregion
    }
}