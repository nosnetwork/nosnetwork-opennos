﻿using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$MobRain", PassNonParseablePacket = false, Authorities = new[] {AuthorityType.GM})]
    public class MobRainPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short NpcMonsterVNum { get; set; }

        [PacketIndex(index: 1)] public short Amount { get; set; }

        [PacketIndex(index: 2)] public bool IsMoving { get; set; }

        public static string ReturnHelp()
        {
            return "$MobRain <VNum> <Amount> <IsMoving>";
        }

        #endregion
    }
}