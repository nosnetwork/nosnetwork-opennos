﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ReputationRate", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm })]
    public class ReputationRatePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Value { get; set; }

        public static string ReturnHelp()
        {
            return "$ReputationRate <Value>";
        }

        #endregion
    }
}