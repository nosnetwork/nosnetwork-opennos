﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$UserLog", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class UserLogPacket : PacketDefinition
    {
        #region Methods

        public static string ReturnHelp()
        {
            return "$UserLog";
        }

        #endregion
    }
}