﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Unban", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class UnbanPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$Unban <Nickname>";
        }

        #endregion
    }
}