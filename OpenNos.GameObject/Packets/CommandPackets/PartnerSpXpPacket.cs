﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$PspXp", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Mod })]
    public class PartnerSpXpPacket : PacketDefinition
    {
        #region Properties

        public static string ReturnHelp()
        {
            return "$PspXp";
        }

        #endregion
    }
}