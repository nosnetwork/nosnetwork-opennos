﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Restart", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class RestartPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public int Time { get; set; }

        public static string ReturnHelp()
        {
            return "$Restart <?Delay>";
        }
    }
}