﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Backpack", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class BackpackPacket : PacketDefinition
    {
        #region Methods

        public static string ReturnHelp()
        {
            return "$Backpack";
        }

        #endregion
    }
}