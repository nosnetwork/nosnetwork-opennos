﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$HeroLvl", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ChangeHeroLevelPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte HeroLevel { get; set; }

        public static string ReturnHelp()
        {
            return "$HeroLvl <Value>";
        }

        #endregion
    }
}