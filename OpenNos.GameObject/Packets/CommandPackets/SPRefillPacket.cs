﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$SPRefill", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class SPRefillPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$SPRefill";
        }
    }
}