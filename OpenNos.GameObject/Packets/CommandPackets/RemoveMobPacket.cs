﻿using OpenNos.Core;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$RemoveMonster", PassNonParseablePacket = false,
        Authorities = new[] {AuthorityType.GM})]
    public class RemoveMobPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$RemoveMonster";
        }
    }
}