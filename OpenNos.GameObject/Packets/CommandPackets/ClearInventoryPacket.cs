﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Clear", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ClearInventoryPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public InventoryType InventoryType { get; set; }

        public static string ReturnHelp()
        {
            return "$Clear <InventoryType>";
        }

        #endregion
    }
}