﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$HairColor", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class HairColorPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public HairColorType HairColor { get; set; }

        public static string ReturnHelp()
        {
            return "$HairColor <Value>";
        }

        #endregion
    }
}