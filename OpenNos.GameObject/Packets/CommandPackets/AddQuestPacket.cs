﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$AddQuest", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class AddQuestPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short QuestId { get; set; }

        public static string ReturnHelp()
        {
            return "$AddQuest <QuestId>";
        }

        #endregion
    }
}