﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Effect", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class EffectCommandPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int EffectId { get; set; }

        public static string ReturnHelp()
        {
            return "$Effect <Value>";
        }

        #endregion
    }
}