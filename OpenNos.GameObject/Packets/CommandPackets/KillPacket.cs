﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Kill", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class KillPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$Kill <Nickname>";
        }

        #endregion
    }
}