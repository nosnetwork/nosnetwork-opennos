﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$SearchMonster", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm })]
    public class SearchMonsterPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0, SerializeToEnd = true)]
        public string Contents { get; set; }

        public static string ReturnHelp()
        {
            return "$SearchMonster <Name>";
        }

        #endregion
    }
}