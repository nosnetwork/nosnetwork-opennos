﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Npc", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class NPCPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short NpcMonsterVNum { get; set; }

        [PacketIndex(index: 1)] public short Amount { get; set; }

        [PacketIndex(index: 2)] public bool IsMoving { get; set; }

        public static string ReturnHelp()
        {
            return "$Npc <VNum> <Amount> <IsMoving>";
        }

        #endregion
    }
}