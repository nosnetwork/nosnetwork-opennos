﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$BlockPM", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class BlockPMPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$BlockPM";
        }
    }
}