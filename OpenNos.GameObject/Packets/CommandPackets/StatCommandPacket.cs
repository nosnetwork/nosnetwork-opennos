﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Stat", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class StatCommandPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$Stat";
        }
    }
}