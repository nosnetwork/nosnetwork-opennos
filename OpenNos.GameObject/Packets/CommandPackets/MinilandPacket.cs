﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Miniland", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class MinilandPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$Miniland <?Nickname>";
        }

        #endregion
    }
}