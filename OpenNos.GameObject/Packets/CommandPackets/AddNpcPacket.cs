﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$AddNpc", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class AddNpcPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short NpcVNum { get; set; }

        [PacketIndex(index: 1)] public bool IsMoving { get; set; }

        public static string ReturnHelp()
        {
            return "$AddNpc <VNum> <IsMoving>";
        }

        #endregion
    }
}