﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$GodMode", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class GodModePacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$GodMode";
        }
    }
}