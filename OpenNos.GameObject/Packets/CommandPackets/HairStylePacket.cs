﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$HairStyle", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class HairStylePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public HairStyleType HairStyle { get; set; }

        public static string ReturnHelp()
        {
            return "$HairStyle <Value>";
        }

        #endregion
    }
}