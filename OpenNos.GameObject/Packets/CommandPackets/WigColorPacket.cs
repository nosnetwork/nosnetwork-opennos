﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$WigColor", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm, AuthorityType.Mod })]
    public class WigColorPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Color { get; set; }

        public static string ReturnHelp()
        {
            return "$WigColor <Value>";
        }

        #endregion
    }
}