﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$FairyXpRate", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class FairyXpRatePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public int Value { get; set; }

        public static string ReturnHelp()
        {
            return "$FairyXpRate <Value>";
        }

        #endregion
    }
}