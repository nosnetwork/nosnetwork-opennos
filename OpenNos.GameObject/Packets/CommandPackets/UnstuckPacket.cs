﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Unstuck", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.User })]
    public class UnstuckPacket : PacketDefinition
    {
        #region Methods

        public static string ReturnHelp()
        {
            return "$Unstuck";
        }

        #endregion
    }
}