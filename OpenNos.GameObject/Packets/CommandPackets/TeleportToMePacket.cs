﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$TeleportToMe", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Gm })]
    public class SummonPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$TeleportToMe <Nickname|*>";
        }

        #endregion
    }
}