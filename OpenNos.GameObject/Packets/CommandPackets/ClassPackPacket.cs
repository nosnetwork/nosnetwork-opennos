﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$ClassPack", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class ClassPackPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public short Class { get; set; }

        public static string ReturnHelp()
        {
            return "$ClassPack <ClassType[1 = Swordsman, 2 = Archer, 3 = Mage]>";
        }

        #endregion
    }
}