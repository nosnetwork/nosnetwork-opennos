﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Kick", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class KickPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$Kick <Nickname>";
        }

        #endregion
    }
}