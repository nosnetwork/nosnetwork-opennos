﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Demote", PassNonParseablePacket = false, Authorities = new[] { AuthorityType.Gm })]
    public class DemotePacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        public static string ReturnHelp()
        {
            return "$Demote <Nickname>";
        }

        #endregion
    }
}