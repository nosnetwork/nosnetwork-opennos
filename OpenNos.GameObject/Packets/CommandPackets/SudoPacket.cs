﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Sudo", PassNonParseablePacket = false,
        Authorities = new[] { AuthorityType.Administrator })]
    public class SudoPacket : PacketDefinition
    {
        [PacketIndex(index: 0)] public string CharacterName { get; set; }

        [PacketIndex(index: 1, serializeToEnd: true)]
        public string CommandContents { get; set; }

        public static string ReturnHelp()
        {
            return "$Sudo <Nickname> <Command>";
        }
    }
}