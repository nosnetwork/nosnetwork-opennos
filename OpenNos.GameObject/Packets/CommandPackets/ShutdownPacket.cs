﻿using OpenNos.Core.Serializing;
using OpenNos.Domain;

namespace OpenNos.GameObject.CommandPackets
{
    [PacketHeader("$Shutdown", PassNonParseablePacket = true,
        Authorities = new[] { AuthorityType.Administrator })]
    public class ShutdownPacket : PacketDefinition
    {
        public static string ReturnHelp()
        {
            return "$Shutdown";
        }
    }
}