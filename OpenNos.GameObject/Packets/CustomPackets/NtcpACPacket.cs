﻿using OpenNos.Core.Serializing;

namespace OpenNos.GameObject
{
    [PacketHeader("ntcp_ac")]
    public class NtcpAcPacket : PacketDefinition
    {
        #region Properties

        [PacketIndex(index: 0)] public byte Type { get; set; }

        [PacketIndex(index: 1)] public byte TrapValue { get; set; }

        [PacketIndex(index: 2)] public string Data { get; set; }

        [PacketIndex(index: 3)] public string EncryptedKey { get; set; }

        [PacketIndex(index: 4)] public string Crc32 { get; set; }

        [PacketIndex(index: 5)] public string Signature { get; set; }

        #endregion
    }
}