﻿using System;
using System.Collections.Concurrent;
using OpenNos.Core;
using OpenNos.Core.Networking;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class SessionManager
    {
        #region Instantiation

        public SessionManager(Type packetHandler, bool isWorldServer)
        {
            _packetHandler = packetHandler;
            IsWorldServer = isWorldServer;
        }

        #endregion

        #region Properties

        public bool IsWorldServer { get; set; }

        #endregion

        #region Members

        protected Type _packetHandler;

        protected ConcurrentDictionary<long, ClientSession> _sessions = new ConcurrentDictionary<long, ClientSession>();

        #endregion

        #region Methods

        public void AddSession(INetworkClient customClient)
        {
            Logger.Info(message: Language.Instance.GetMessageFromKey(key: "NEW_CONNECT") + customClient.ClientId);

            var session = IntializeNewSession(client: customClient);
            customClient.SetClientSession(clientSession: session);

            if (session != null && IsWorldServer && !_sessions.TryAdd(key: customClient.ClientId, value: session))
            {
                Logger.Warn(data: string.Format(format: Language.Instance.GetMessageFromKey(key: "FORCED_DISCONNECT"),
                    arg0: customClient.ClientId));
                customClient.Disconnect();
                _sessions.TryRemove(key: customClient.ClientId, value: out session);
            }
        }

        public virtual void StopServer()
        {
            _sessions.Clear();
            ServerManager.StopServer();
        }

        protected virtual ClientSession IntializeNewSession(INetworkClient client)
        {
            var session = new ClientSession(client: client);
            client.SetClientSession(clientSession: session);
            return session;
        }

        protected void RemoveSession(INetworkClient client)
        {
            _sessions.TryRemove(key: client.ClientId, value: out var session);

            // check if session hasnt been already removed
            if (session != null)
            {
                session.IsDisposing = true;

                session.Destroy();

                if (IsWorldServer && session.HasSelectedCharacter) session.Character.Save();

                client.Disconnect();
                Logger.Info(message: Language.Instance.GetMessageFromKey(key: "DISCONNECT") + client.ClientId);

                // session = null;
            }
        }

        #endregion
    }
}