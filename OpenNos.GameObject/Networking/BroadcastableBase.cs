﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Serializing;
using OpenNos.Core.Threading;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;

namespace OpenNos.GameObject
{
    public abstract class BroadcastableBase : IDisposable
    {
        #region Instantiation

        protected BroadcastableBase()
        {
            LastUnregister = DateTime.Now.AddMinutes(value: -1);
            _sessions = new ThreadSafeSortedList<long, ClientSession>();
        }

        #endregion

        #region Members

        /// <summary>
        ///     List of all connected clients.
        /// </summary>
        readonly ThreadSafeSortedList<long, ClientSession> _sessions;

        bool _disposed;

        #endregion

        #region Properties

        public IEnumerable<ClientSession> Sessions
        {
            get => _sessions.Where(predicate: s => s.HasSelectedCharacter && !s.IsDisposing && s.IsConnected);
        }

        public IEnumerable<Mate> Mates
        {
            get
            {
                var mates = new List<Mate>();
                Sessions.ToList().ForEach(action: s =>
                    mates.AddRange(
                        collection: s.Character?.Mates.Where(predicate: m => m.IsTeamMember || m.IsTemporalMate) ??
                                    throw new InvalidOperationException()));
                return mates;
            }
        }

        protected DateTime LastUnregister { get; private set; }

        #endregion

        #region Methods

        public void Broadcast(string packet, ReceiverType receiver = ReceiverType.All)
        {
            Broadcast(client: null, content: packet, receiver: receiver);
        }

        public void Broadcast(string packet, int xRangeCoordinate, int yRangeCoordinate)
        {
            Broadcast(packet: new BroadcastPacket(session: null, packet: packet, receiver: ReceiverType.AllInRange,
                xCoordinate: xRangeCoordinate,
                yCoordinate: yRangeCoordinate));
        }

        public void Broadcast(PacketDefinition packet, ReceiverType receiver = ReceiverType.All)
        {
            Broadcast(client: null, packet: packet, receiver: receiver);
        }

        public void Broadcast(PacketDefinition packet, int xRangeCoordinate, int yRangeCoordinate)
        {
            Broadcast(packet: new BroadcastPacket(session: null, packet: PacketFactory.Serialize(packet: packet),
                receiver: ReceiverType.AllInRange,
                xCoordinate: xRangeCoordinate, yCoordinate: yRangeCoordinate));
        }

        public void Broadcast(ClientSession client, PacketDefinition packet, ReceiverType receiver = ReceiverType.All,
            string characterName = "", long characterId = -1)
        {
            Broadcast(client: client, content: PacketFactory.Serialize(packet: packet), receiver: receiver,
                characterName: characterName, characterId: characterId);
        }

        public void Broadcast(BroadcastPacket packet)
        {
            try
            {
                SpreadBroadcastpacket(sentPacket: packet);
            }
            catch (Exception ex)
            {
                Logger.Error(ex: ex);
            }
        }

        public void Broadcast(ClientSession client, string content, ReceiverType receiver = ReceiverType.All,
            string characterName = "", long characterId = -1)
        {
            try
            {
                if (client == null || client.Character == null || !client.Character.InvisibleGm ||
                    content.StartsWith(value: "out", comparisonType: StringComparison.CurrentCulture))
                    SpreadBroadcastpacket(sentPacket: new BroadcastPacket(session: client, packet: content,
                        receiver: receiver, someonesCharacterName: characterName, someonesCharacterId: characterId));
            }
            catch (Exception ex)
            {
                Logger.Error(ex: ex);
            }
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                Dispose(disposing: true);
                GC.SuppressFinalize(obj: this);
                _disposed = true;
            }
        }

        public ClientSession GetSessionByCharacterId(long characterId)
        {
            return _sessions.ContainsKey(key: characterId) ? _sessions[key: characterId] : null;
        }

        public Character GetCharacterById(long characterId)
        {
            return GetSessionByCharacterId(characterId: characterId)?.Character;
        }

        public void RegisterSession(ClientSession session)
        {
            if (!session.HasSelectedCharacter) return;
            session.RegisterTime = DateTime.Now;

            // Create a ChatClient and store it in a collection
            _sessions[key: session.Character.CharacterId] = session;
            if (session.HasCurrentMapInstance) session.CurrentMapInstance.IsSleeping = false;
        }

        public void UnregisterSession(long characterId)
        {
            // Get client from client list, if not in list do not continue
            var session = _sessions[key: characterId];
            if (session == null) return;

            // Remove client from online clients list
            _sessions.Remove(key: characterId);
            if (session.HasCurrentMapInstance && _sessions.Count == 0) session.CurrentMapInstance.IsSleeping = true;
            LastUnregister = DateTime.Now;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing) _sessions.Dispose();
        }

        void SpreadBroadcastpacket(BroadcastPacket sentPacket)
        {
            if (Sessions != null && !string.IsNullOrEmpty(value: sentPacket?.Packet))
                switch (sentPacket.Receiver)
                {
                    case ReceiverType.All: // send packet to everyone
                        if (sentPacket.Packet.StartsWith(value: "out", comparisonType: StringComparison.CurrentCulture))
                        {
                            foreach (var session in Sessions.Where(predicate: s =>
                                s.Character == null || !s.Character.IsChangingMapInstance))
                                if (session.HasSelectedCharacter)
                                {
                                    if (sentPacket.Sender != null)
                                    {
                                        if (!sentPacket.Sender.Character.IsBlockedByCharacter(characterId: session
                                            .Character
                                            .CharacterId)) session.SendPacket(packet: sentPacket.Packet);
                                    }
                                    else
                                    {
                                        session.SendPacket(packet: sentPacket.Packet);
                                    }
                                }
                        }
                        else
                        {
                            Parallel.ForEach(source: Sessions, body: session =>
                            {
                                if (session?.HasSelectedCharacter == true)
                                {
                                    if (sentPacket.Sender != null)
                                    {
                                        if (!sentPacket.Sender.Character.IsBlockedByCharacter(characterId: session
                                                .Character
                                                .CharacterId) ||
                                            sentPacket.Packet.StartsWith(value: "revive",
                                                comparisonType: StringComparison.CurrentCulture))
                                            session.SendPacket(packet: sentPacket.Packet);
                                    }
                                    else
                                    {
                                        session.SendPacket(packet: sentPacket.Packet);
                                    }
                                }
                            });
                        }

                        break;

                    case ReceiverType.AllExceptMe: // send to everyone except the sender
                        if (sentPacket.Packet.StartsWith(value: "out", comparisonType: StringComparison.CurrentCulture))
                        {
                            foreach (var session in Sessions.Where(predicate: s =>
                                s != null && s.SessionId != sentPacket.Sender?.SessionId &&
                                (s.Character == null || !s.Character.IsChangingMapInstance)))
                                if (session.HasSelectedCharacter)
                                {
                                    if (sentPacket.Sender != null)
                                        //if (!sentPacket.Sender.Character.IsBlockedByCharacter(session.Character.CharacterId))
                                        session.SendPacket(packet: sentPacket.Packet);
                                    else
                                        session.SendPacket(packet: sentPacket.Packet);
                                }
                        }
                        else
                        {
                            Parallel.ForEach(
                                source: Sessions.Where(predicate: s => s?.SessionId != sentPacket.Sender?.SessionId),
                                body: session =>
                                {
                                    if (session?.HasSelectedCharacter == true)
                                    {
                                        if (sentPacket.Sender != null)
                                        {
                                            if (!sentPacket.Sender.Character.IsBlockedByCharacter(characterId: session
                                                .Character
                                                .CharacterId)) session.SendPacket(packet: sentPacket.Packet);
                                        }
                                        else
                                        {
                                            session.SendPacket(packet: sentPacket.Packet);
                                        }
                                    }
                                });
                        }

                        break;

                    case ReceiverType.AllExceptGroup:
                        if (sentPacket.Packet.StartsWith(value: "out", comparisonType: StringComparison.CurrentCulture))
                        {
                            foreach (var session in Sessions.Where(predicate: s =>
                                s.SessionId != sentPacket.Sender.SessionId &&
                                (s.Character == null || !s.Character.IsChangingMapInstance)))
                                if (session.HasSelectedCharacter)
                                {
                                    if (sentPacket.Sender != null)
                                    {
                                        if (!sentPacket.Sender.Character.IsBlockedByCharacter(characterId: session
                                            .Character
                                            .CharacterId)) session.SendPacket(packet: sentPacket.Packet);
                                    }
                                    else
                                    {
                                        session.SendPacket(packet: sentPacket.Packet);
                                    }
                                }
                        }
                        else
                        {
                            foreach (var session in Sessions.Where(predicate: s =>
                                s.SessionId != sentPacket.Sender.SessionId &&
                                (s.Character?.Group == null || s.Character?.Group?.GroupId !=
                                    sentPacket.Sender?.Character?.Group?.GroupId)))
                                if (session.HasSelectedCharacter &&
                                    !sentPacket.Sender.Character.IsBlockedByCharacter(
                                        characterId: session.Character.CharacterId))
                                    session.SendPacket(packet: sentPacket.Packet);
                        }

                        break;

                    case ReceiverType.AllExceptMeAct4: // send to everyone except the sender(Act4)
                        Parallel.ForEach(
                            source: Sessions.Where(predicate: s =>
                                s.SessionId != sentPacket.Sender?.SessionId &&
                                (s.Character == null || !s.Character.IsChangingMapInstance)), body: session =>
                            {
                                if (session?.HasSelectedCharacter == true)
                                {
                                    if (sentPacket.Sender != null)
                                    {
                                        if (!sentPacket.Sender.Character.IsBlockedByCharacter(characterId: session
                                            .Character
                                            .CharacterId))
                                        {
                                            if (session.Character.Faction == sentPacket.Sender.Character.Faction)
                                            {
                                                session.SendPacket(packet: sentPacket.Packet);
                                            }
                                            else if (session.Account.Authority >=
                                                     AuthorityType
                                                         .Mod /*|| session.Account.Authority == AuthorityType.Moderator*/
                                            )
                                            {
                                                var vals = sentPacket.Packet.Split(' ');
                                                if (vals[0] == "say" && vals[3] == "13")
                                                {
                                                    vals[5] = $"[{sentPacket.Sender.Character.Faction}] {vals[5]}";
                                                    sentPacket.Packet = string.Join(separator: " ", value: vals);
                                                }

                                                session.SendPacket(packet: sentPacket.Packet);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        session.SendPacket(packet: sentPacket.Packet);
                                    }
                                }
                            });
                        break;

                    case ReceiverType.AllInRange: // send to everyone which is in a range of 50x50
                        if (sentPacket.XCoordinate != 0 && sentPacket.YCoordinate != 0)
                            Parallel.ForEach(
                                source: Sessions.Where(predicate: s =>
                                    s?.Character.IsInRange(xCoordinate: sentPacket.XCoordinate,
                                        yCoordinate: sentPacket.YCoordinate) == true &&
                                    (s.Character == null || !s.Character.IsChangingMapInstance)), body: session =>
                                {
                                    if (session?.HasSelectedCharacter == true)
                                    {
                                        if (sentPacket.Sender != null)
                                        {
                                            if (!sentPacket.Sender.Character.IsBlockedByCharacter(characterId: session
                                                .Character
                                                .CharacterId)) session.SendPacket(packet: sentPacket.Packet);
                                        }
                                        else
                                        {
                                            session.SendPacket(packet: sentPacket.Packet);
                                        }
                                    }
                                });
                        break;

                    case ReceiverType.OnlySomeone:
                        if (sentPacket.SomeonesCharacterId > 0 ||
                            !string.IsNullOrEmpty(value: sentPacket.SomeonesCharacterName))
                        {
                            var targetSession = Sessions.SingleOrDefault(predicate: s =>
                                s.Character.CharacterId == sentPacket.SomeonesCharacterId ||
                                s.Character.Name == sentPacket.SomeonesCharacterName);
                            if (targetSession?.HasSelectedCharacter == true)
                            {
                                if (sentPacket.Sender != null)
                                {
                                    if (!sentPacket.Sender.Character.IsBlockedByCharacter(characterId: targetSession
                                        .Character
                                        .CharacterId))
                                        targetSession.SendPacket(packet: sentPacket.Packet);
                                    else
                                        sentPacket.Sender.SendPacket(
                                            packet: UserInterfaceHelper.GenerateInfo(
                                                message: Language.Instance.GetMessageFromKey(
                                                    key: "BLACKLIST_BLOCKED")));
                                }
                                else
                                {
                                    targetSession.SendPacket(packet: sentPacket.Packet);
                                }
                            }
                        }

                        break;

                    case ReceiverType.AllNoEmoBlocked:
                        Parallel.ForEach(source: Sessions.Where(predicate: s => s?.Character.EmoticonsBlocked == false),
                            body: session =>
                            {
                                if (session?.HasSelectedCharacter == true &&
                                    sentPacket.Sender?.Character.IsBlockedByCharacter(
                                        characterId: session.Character.CharacterId) ==
                                    false) session.SendPacket(packet: sentPacket.Packet);
                            });
                        break;

                    case ReceiverType.AllNoHeroBlocked:
                        Parallel.ForEach(source: Sessions.Where(predicate: s => s?.Character.HeroChatBlocked == false),
                            body: session =>
                            {
                                if (session?.HasSelectedCharacter == true &&
                                    sentPacket.Sender?.Character.IsBlockedByCharacter(
                                        characterId: session.Character.CharacterId) ==
                                    false) session.SendPacket(packet: sentPacket.Packet);
                            });
                        break;

                    case ReceiverType.Group:
                        foreach (var session in Sessions.Where(predicate: s =>
                            s.Character?.Group != null && sentPacket.Sender?.Character?.Group != null &&
                            s.Character.Group.GroupId == sentPacket.Sender.Character.Group.GroupId))
                            session.SendPacket(packet: sentPacket.Packet);
                        break;

                    case ReceiverType.Unknown:
                        break;
                }
        }

        #endregion
    }
}