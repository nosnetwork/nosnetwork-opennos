﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.Core.Cryptography;
using OpenNos.Core.Networking;
using OpenNos.Core.Networking.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.Core.Networking.Communication.Scs.Server;
using OpenNos.Domain;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class NetworkManager<TEncryptorT> : SessionManager where TEncryptorT : CryptographyBase
    {
        #region Instantiation

        public NetworkManager(string ipAddress, int port, Type packetHandler, Type fallbackEncryptor,
            bool isWorldServer) : base(packetHandler: packetHandler, isWorldServer: isWorldServer)
        {
            _encryptor = (TEncryptorT)Activator.CreateInstance(type: typeof(TEncryptorT));

            if (fallbackEncryptor != null)
                _fallbackEncryptor = (CryptographyBase)Activator.CreateInstance(type: fallbackEncryptor);

            _server = ScsServerFactory.CreateServer(endPoint: new ScsTcpEndPoint(ipAddress: ipAddress, port: port));

            // Register events of the server to be informed about clients
            _server.ClientConnected += OnServerClientConnected;
            _server.ClientDisconnected += OnServerClientDisconnected;
            _server.WireProtocolFactory = new WireProtocolFactory();

            // Start the server
            _server.Start();

            // ReSharper disable once ExplicitCallerInfoArgument
            Logger.Info(message: Language.Instance.GetMessageFromKey(key: "STARTED"), memberName: "NetworkManager");
        }

        #endregion

        #region Properties

        IDictionary<string, DateTime> ConnectionLog
        {
            get => _connectionLog ?? (_connectionLog = new Dictionary<string, DateTime>());
        }

        #endregion

        #region Members

        readonly TEncryptorT _encryptor;

        readonly CryptographyBase _fallbackEncryptor;

        readonly IScsServer _server;

        IDictionary<string, DateTime> _connectionLog;

        #endregion

        #region Methods

        public override void StopServer()
        {
            _server.Stop();
            _server.ClientConnected -= OnServerClientDisconnected;
            _server.ClientDisconnected -= OnServerClientConnected;
        }

        protected override ClientSession IntializeNewSession(INetworkClient client)
        {
            if (!CheckGeneralLog(client: client))
            {
                Logger.Warn(data: string.Format(format: Language.Instance.GetMessageFromKey(key: "FORCED_DISCONNECT"),
                    arg0: client.ClientId));
                client.Initialize(encryptor: _fallbackEncryptor);
                client.SendPacket(packet: $"failc {LoginFailType.CantConnect}");
                client.Disconnect();
                return null;
            }

            var session = new ClientSession(client: client);
            session.Initialize(encryptor: _encryptor, packetHandler: _packetHandler, isWorldServer: IsWorldServer);

            return session;
        }

        bool CheckGeneralLog(INetworkClient client)
        {
            if (!client.IpAddress.Contains(value: "127.0.0.1") && ServerManager.Instance.ChannelId != 51)
            {
                if (ConnectionLog.Count > 0)
                    foreach (var item in ConnectionLog.Where(predicate: cl =>
                            cl.Key.Contains(value: client.IpAddress.Split(':')[1]) &&
                            (DateTime.Now - cl.Value).TotalSeconds > 3)
                        .ToList())
                        ConnectionLog.Remove(key: item.Key);

                if (ConnectionLog.Any(predicate: c => c.Key.Contains(value: client.IpAddress.Split(':')[1])))
                    return false;
                ConnectionLog.Add(key: client.IpAddress, value: DateTime.Now);
                return true;
            }

            return true;
        }

        void OnServerClientConnected(object sender, ServerClientEventArgs e)
        {
            AddSession(customClient: e.Client as NetworkClient);
        }

        void OnServerClientDisconnected(object sender, ServerClientEventArgs e)
        {
            RemoveSession(client: e.Client as NetworkClient);
        }

        #endregion
    }
}