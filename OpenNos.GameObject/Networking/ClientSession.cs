﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using OpenNos.Core;
using OpenNos.Core.Cryptography;
using OpenNos.Core.Handling;
using OpenNos.Core.Networking;
using OpenNos.Core.Networking.Communication.Scs.Communication.Messages;
using OpenNos.Core.Serializing;
using OpenNos.Core.Threading;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;

namespace OpenNos.GameObject
{
    public class ClientSession
    {
        #region Instantiation

        public ClientSession(INetworkClient client)
        {
            // set the time of last received packet

            // lag mode
            // ReSharper disable once ObjectCreationAsStatement
            new Random(Seed: (int)client.ClientId);

            // initialize network client
            _client = client;

            // absolutely new instantiated Client has no SessionId
            SessionId = 0;

            // register for NetworkClient events
            _client.MessageReceived += OnNetworkClientMessageReceived;

            // start observer for receiving packets
            _receiveQueue = new ConcurrentQueue<byte[]>();
            Observable
                .Interval(period: new TimeSpan(days: 0, hours: 0, minutes: 0, seconds: 0, milliseconds: 10))
                .Subscribe(onNext: x => HandlePackets());

            #region Anti-Cheat Heartbeat

            if (ServerManager.Instance.IsWorldServer) RegisterAntiCheat();

            #endregion
        }

        #endregion

        #region Members

        public bool HealthStop;

        static CryptographyBase _encryptor;

        readonly INetworkClient _client;

        readonly ConcurrentQueue<byte[]> _receiveQueue;

        readonly IList<string> _waitForPacketList = new List<string>();

        Character _character;

        IDictionary<string[], HandlerMethodReference> _handlerMethods;

        int _lastPacketId;

        // private byte countPacketReceived;

        int? _waitForPacketsAmount;

        #endregion

        #region Properties

        public static ThreadSafeGenericLockedList<string> UserLog { get; set; } =
            new ThreadSafeGenericLockedList<string>();

        public bool IsAntiCheatAlive { get; set; }

        public IDisposable AntiCheatHeartbeatDisposable { get; set; }

        public string LastAntiCheatData { get; set; }

        public string Crc32 { get; set; }

        public Account Account { get; private set; }

        public Character Character
        {
            get
            {
                if (_character == null || !HasSelectedCharacter)
                    // cant access an
                    Logger.Warn(data: "An uninitialized character should not be accessed.");

                return _character;
            }
            private set => _character = value;
        }

        public long ClientId
        {
            get => _client.ClientId;
        }

        public MapInstance CurrentMapInstance { get; set; }

        public IDictionary<string[], HandlerMethodReference> HandlerMethods
        {
            get => _handlerMethods ?? (_handlerMethods = new Dictionary<string[], HandlerMethodReference>());
        }

        public bool HasCurrentMapInstance
        {
            get => CurrentMapInstance != null;
        }

        public bool HasSelectedCharacter { get; private set; }

        public bool HasSession
        {
            get => _client != null;
        }

        public string IpAddress
        {
            get => _client.IpAddress;
        }

        public string CleanIpAddress
        {
            get
            {
                var cleanIp = _client.IpAddress.Replace(oldValue: "tcp://", newValue: "");
                return cleanIp.Substring(startIndex: 0,
                    length: cleanIp.LastIndexOf(value: ":", comparisonType: StringComparison.Ordinal) > 0
                        ? cleanIp.LastIndexOf(value: ":", comparisonType: StringComparison.Ordinal)
                        : cleanIp.Length);
            }
            set
            {
                if (value == null) throw new ArgumentNullException(paramName: nameof(value));
            }
        }

        public bool IsAuthenticated { get; private set; }

        public bool IsConnected
        {
            get => _client.IsConnected;
        }

        public bool IsDisposing
        {
            get => _client.IsDisposing;
            internal set => _client.IsDisposing = value;
        }

        public bool IsLocalhost
        {
            get => IpAddress.Contains(value: "127.0.0.1");
        }

        public bool IsOnMap
        {
            get => CurrentMapInstance != null;
        }

        public DateTime RegisterTime { get; internal set; }

        public int SessionId { get; private set; }

        #endregion

        #region Methods

        #region Anti-Cheat

        public void RegisterAntiCheat()
        {
            if (!AntiCheatHelper.IsAntiCheatEnabled) return;

            IsAntiCheatAlive = true;

            AntiCheatHeartbeatDisposable = Observable.Interval(period: TimeSpan.FromMinutes(value: 1))
                .Subscribe(onNext: observer =>
                {
                    if (IsAntiCheatAlive)
                    {
                        LastAntiCheatData = AntiCheatHelper.GenerateData(length: 64);
                        SendPacket(
                            packet:
                            $"ntcp_ac 0 {LastAntiCheatData} {AntiCheatHelper.Sha512(input: LastAntiCheatData + AntiCheatHelper.ClientKey)}");
                        IsAntiCheatAlive = false;
                    }
                    else
                    {
                        if (Account?.Authority != AuthorityType.Administrator) Disconnect();
                    }
                });
        }

        public void BanForCheatUsing(int detectionCode)
        {
            var isFirstTime = !DaoFactory.PenaltyLogDao.LoadByAccount(accountId: Account.AccountId)
                                  .Any(predicate: s => s.AdminName == "Anti-Cheat")
                              && !DaoFactory.PenaltyLogDao.LoadByIp(ip: IpAddress)
                                  .Any(predicate: s => s.AdminName == "Anti-Cheat");

            var penaltyLog = new PenaltyLogDto
            {
                AccountId = Account.AccountId,
                Reason = $"Cheat Using ({detectionCode})",
                Penalty = PenaltyType.Banned,
                DateStart = DateTime.Now,
                DateEnd = isFirstTime ? DateTime.Now.AddHours(value: 24) : DateTime.Now.AddYears(value: 15),
                AdminName = "Anti-Cheat"
            };

            Character.InsertOrUpdatePenalty(log: penaltyLog);
            Disconnect();
        }

        #endregion

        public void ClearLowPriorityQueue()
        {
            _client.ClearLowPriorityQueueAsync();
        }

        public void Destroy()
        {
            #region Anti-Cheat Heartbeat

            AntiCheatHeartbeatDisposable?.Dispose();

            #endregion

            // unregister from WCF events
            CommunicationServiceClient.Instance.CharacterConnectedEvent -= OnOtherCharacterConnected;
            CommunicationServiceClient.Instance.CharacterDisconnectedEvent -= OnOtherCharacterDisconnected;

            // do everything necessary before removing client, DB save, Whatever
            if (HasSelectedCharacter)
            {
                Logger.LogUserEvent(logEvent: "CHARACTER_LOGOUT", caller: GenerateIdentity(), data: "");

                var characterId = Character.CharacterId;

                Character.Dispose();

                // disconnect client
                CommunicationServiceClient.Instance.DisconnectCharacter(worldId: ServerManager.Instance.WorldId,
                    characterId: characterId);

                // unregister from map if registered
                if (CurrentMapInstance != null)
                {
                    CurrentMapInstance.UnregisterSession(characterId: characterId);
                    CurrentMapInstance = null;
                }

                ServerManager.Instance.UnregisterSession(characterId: characterId);
            }

            if (Account != null) CommunicationServiceClient.Instance.DisconnectAccount(accountId: Account.AccountId);

            ClearReceiveQueue();
        }

        public void Disconnect()
        {
            _client.Disconnect();
        }

        public string GenerateIdentity()
        {
            if (Character != null) return $"Character: {Character.Name}";
            return $"Account: {Account.Name}";
        }

        public void Initialize(CryptographyBase encryptor, Type packetHandler, bool isWorldServer)
        {
            _encryptor = encryptor;
            _client.Initialize(encryptor: encryptor);

            // dynamically create packethandler references
            GenerateHandlerReferences(type: packetHandler, isWorldServer: isWorldServer);
        }

        public void InitializeAccount(Account account, bool crossServer = false)
        {
            Account = account;
            if (crossServer)
                CommunicationServiceClient.Instance.ConnectAccountCrossServer(worldId: ServerManager.Instance.WorldId,
                    accountId: account.AccountId, sessionId: SessionId);
            else
                CommunicationServiceClient.Instance.ConnectAccount(worldId: ServerManager.Instance.WorldId,
                    accountId: account.AccountId,
                    sessionId: SessionId);
            IsAuthenticated = true;
        }

        public void ReceivePacket(string packet, bool ignoreAuthority = false)
        {
            var header = packet.Split(' ')[0];
            TriggerHandler(packetHeader: header, packet: $"{_lastPacketId} {packet}", force: false,
                ignoreAuthority: ignoreAuthority);
            _lastPacketId++;
        }

        public void SendPacket(string packet, byte priority = 10)
        {
            if (!IsDisposing)
            {
                _client.SendPacket(packet: packet, priority: priority);
                if (packet != null && _character != null && HasSelectedCharacter &&
                    !packet.StartsWith(value: "cond ") &&
                    !packet.StartsWith(value: "mv ")) SendPacket(packet: Character.GenerateCond());
            }
        }

        public void SendPacket(PacketDefinition packet, byte priority = 10)
        {
            if (!IsDisposing) _client.SendPacket(packet: PacketFactory.Serialize(packet: packet), priority: priority);
        }

        public void SendPacketAfter(string packet, int milliseconds)
        {
            if (!IsDisposing)
                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: milliseconds))
                    .Subscribe(onNext: o => SendPacket(packet: packet));
        }

        public void SendPacketFormat(string packet, params object[] param)
        {
            if (!IsDisposing) _client.SendPacketFormat(packet: packet, param: param);
        }

        public void SendPackets(IEnumerable<string> packets, byte priority = 10)
        {
            if (!IsDisposing)
            {
                _client.SendPackets(packets: packets, priority: priority);
                if (_character != null && HasSelectedCharacter) SendPacket(packet: Character.GenerateCond());
            }
        }

        public void SendPackets(IEnumerable<PacketDefinition> packets, byte priority = 10)
        {
            if (!IsDisposing)
                packets.ToList().ForEach(action: s =>
                    _client.SendPacket(packet: PacketFactory.Serialize(packet: s), priority: priority));
        }

        public void SetCharacter(Character character)
        {
            Character = character;
            HasSelectedCharacter = true;

            Logger.LogUserEvent(logEvent: "CHARACTER_LOGIN", caller: GenerateIdentity(), data: "");

            // register CSC events
            CommunicationServiceClient.Instance.CharacterConnectedEvent += OnOtherCharacterConnected;
            CommunicationServiceClient.Instance.CharacterDisconnectedEvent += OnOtherCharacterDisconnected;

            // register for servermanager
            ServerManager.Instance.RegisterSession(session: this);
            ServerManager.Instance.CharacterScreenSessions.Remove(key: character.AccountId);
            Character.SetSession(clientSession: this);
        }

        void ClearReceiveQueue()
        {
            while (_receiveQueue.TryDequeue(result: out var _))
            {
                // do nothing
            }
        }

        void GenerateHandlerReferences(Type type, bool isWorldServer)
        {
            var handlerTypes = !isWorldServer
                ? type.Assembly.GetTypes()
                    .Where(predicate: t => t.Name.Equals(value: "LoginPacketHandler")) // shitty but it works
                : type.Assembly.GetTypes().Where(predicate: p =>
                {
                    var interfaceType = type.GetInterfaces().FirstOrDefault();
                    return interfaceType != null && !p.IsInterface && interfaceType.IsAssignableFrom(c: p);
                });

            // iterate thru each type in the given assembly
            foreach (var handlerType in handlerTypes)
            {
                var handler = (IPacketHandler)Activator.CreateInstance(type: handlerType, this);

                // include PacketDefinition
                foreach (var methodInfo in handlerType.GetMethods().Where(predicate: x =>
                    x.GetCustomAttributes(inherit: false).OfType<PacketAttribute>().Any() ||
                    x.GetParameters().FirstOrDefault()?.ParameterType.BaseType == typeof(PacketDefinition)))
                {
                    var packetAttributes = methodInfo.GetCustomAttributes(inherit: false).OfType<PacketAttribute>()
                        .ToList();

                    // assume PacketDefinition based handler method
                    if (packetAttributes.Count == 0)
                    {
                        var methodReference = new HandlerMethodReference(
                            handlerMethod: DelegateBuilder.BuildDelegate<Action<object, object>>(method: methodInfo),
                            parentHandler: handler,
                            packetBaseParameterType: methodInfo.GetParameters().FirstOrDefault()?.ParameterType);
                        HandlerMethods.Add(key: methodReference.Identification, value: methodReference);
                    }
                    else
                    {
                        // assume string based handler method
                        foreach (var packetAttribute in packetAttributes)
                        {
                            var methodReference = new HandlerMethodReference(
                                handlerMethod:
                                DelegateBuilder.BuildDelegate<Action<object, object>>(method: methodInfo),
                                parentHandler: handler,
                                handlerMethodAttribute: packetAttribute);
                            HandlerMethods.Add(key: methodReference.Identification, value: methodReference);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Handle the packet received by the Client.
        /// </summary>
        void HandlePackets()
        {
            try
            {
                while (_receiveQueue.TryDequeue(result: out var packetData))
                {
                    // determine first packet
                    if (_encryptor.HasCustomParameter && SessionId == 0)
                    {
                        var sessionPacket = _encryptor.DecryptCustomParameter(data: packetData);

                        var sessionParts = sessionPacket.Split(' ');

                        if (sessionParts.Length == 0) return;

                        if (!int.TryParse(s: sessionParts[0], result: out var packetId)) Disconnect();

                        _lastPacketId = packetId;

                        // set the SessionId if Session Packet arrives
                        if (sessionParts.Length < 2) return;

                        if (int.TryParse(s: sessionParts[1].Split('\\').FirstOrDefault(), result: out var sessid))
                        {
                            SessionId = sessid;
                            Logger.Debug(
                                data: string.Format(format: Language.Instance.GetMessageFromKey(key: "CLIENT_ARRIVED"),
                                    arg0: SessionId));

                            if (!_waitForPacketsAmount.HasValue)
                                TriggerHandler(packetHeader: "OpenNos.EntryPoint", packet: "", force: false);
                        }

                        return;
                    }

                    var packetConcatenated = _encryptor.Decrypt(data: packetData, sessionId: SessionId);

                    foreach (var packet in packetConcatenated.Split(separator: new[] { (char)0xFF },
                        options: StringSplitOptions.RemoveEmptyEntries))
                    {
                        var packetstring = packet.Replace(oldChar: '^', newChar: ' ');
                        var packetsplit = packetstring.Split(' ');

                        if (_encryptor.HasCustomParameter)
                        {
                            var nextRawPacketId = packetsplit[0];

                            if (!int.TryParse(s: nextRawPacketId, result: out var nextPacketId) &&
                                nextPacketId != _lastPacketId + 1)
                            {
                                Logger.Error(data: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "CORRUPTED_KEEPALIVE"),
                                    arg0: _client.ClientId));
                                _client.Disconnect();
                                return;
                            }

                            if (nextPacketId == 0)
                            {
                                if (_lastPacketId == ushort.MaxValue) _lastPacketId = nextPacketId;
                            }
                            else
                            {
                                _lastPacketId = nextPacketId;
                            }

                            if (_waitForPacketsAmount.HasValue)
                            {
                                _waitForPacketList.Add(item: packetstring);

                                var packetssplit = packetstring.Split(' ');

                                if (packetssplit.Length > 3 && packetsplit[1] == "DAC")
                                    _waitForPacketList.Add(item: "0 CrossServerAuthenticate");

                                if (_waitForPacketList.Count == _waitForPacketsAmount)
                                {
                                    _waitForPacketsAmount = null;
                                    var queuedPackets = string.Join(separator: " ",
                                        value: _waitForPacketList.ToArray());
                                    var header = queuedPackets.Split(' ', '^')[1];
                                    TriggerHandler(packetHeader: header, packet: queuedPackets, force: true);
                                    _waitForPacketList.Clear();
                                    return;
                                }
                            }
                            else if (packetsplit.Length > 1)
                            {
                                if (packetsplit[1].Length >= 1 &&
                                    (packetsplit[1][index: 0] == '/' || packetsplit[1][index: 0] == ':' ||
                                     packetsplit[1][index: 0] == ';'))
                                {
                                    packetsplit[1] = packetsplit[1][index: 0].ToString();
                                    packetstring = packet.Insert(startIndex: packet.IndexOf(value: ' ') + 2,
                                        value: " ");
                                }

                                if (packetsplit[1] != "0")
                                    TriggerHandler(packetHeader: packetsplit[1].Replace(oldValue: "#", newValue: ""),
                                        packet: packetstring, force: false);
                            }
                        }
                        else
                        {
                            var packetHeader = packetstring.Split(' ')[0];

                            // simple messaging
                            if (packetHeader[index: 0] == '/' || packetHeader[index: 0] == ':' ||
                                packetHeader[index: 0] == ';')
                            {
                                packetHeader = packetHeader[index: 0].ToString();
                                packetstring = packet.Insert(startIndex: packet.IndexOf(value: ' ') + 2, value: " ");
                            }

                            TriggerHandler(packetHeader: packetHeader.Replace(oldValue: "#", newValue: ""),
                                packet: packetstring, force: false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(data: "Invalid packet (Crash Exploit)", ex: ex);
                Disconnect();
            }
        }

        /// <summary>
        ///     This will be triggered when the underlying NetworkClient receives a packet.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnNetworkClientMessageReceived(object sender, MessageEventArgs e)
        {
            var message = e.Message as ScsRawDataMessage;
            if (message == null) return;
            if (message.MessageData.Length > 0 && message.MessageData.Length > 2)
                _receiveQueue.Enqueue(item: message.MessageData);
        }

        void OnOtherCharacterConnected(object sender, EventArgs e)
        {
            if (Character?.IsDisposed != false) return;

            var loggedInCharacter = (Tuple<long, string>)sender;

            if (Character.IsFriendOfCharacter(characterId: loggedInCharacter.Item1) && Character != null &&
                Character.CharacterId != loggedInCharacter.Item1)
            {
                _client.SendPacket(packet: Character.GenerateSay(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "CHARACTER_LOGGED_IN"),
                        arg0: loggedInCharacter.Item2),
                    type: 10));
                _client.SendPacket(packet: Character.GenerateFinfo(relatedCharacterLoggedId: loggedInCharacter.Item1,
                    isConnected: true));
            }

            var chara = Character.Family?.FamilyCharacters.Find(match: s => s.CharacterId == loggedInCharacter.Item1);

            if (chara != null && loggedInCharacter.Item1 != Character?.CharacterId)
                _client.SendPacket(packet: Character.GenerateSay(
                    message: string.Format(
                        format: Language.Instance.GetMessageFromKey(key: "CHARACTER_FAMILY_LOGGED_IN"),
                        arg0: loggedInCharacter.Item2,
                        arg1: Language.Instance.GetMessageFromKey(key: chara.Authority.ToString().ToUpper())),
                    type: 10));
        }

        void OnOtherCharacterDisconnected(object sender, EventArgs e)
        {
            if (Character?.IsDisposed != false) return;

            var loggedOutCharacter = (Tuple<long, string>)sender;

            if (Character.IsFriendOfCharacter(characterId: loggedOutCharacter.Item1) && Character != null &&
                Character.CharacterId != loggedOutCharacter.Item1)
            {
                _client.SendPacket(packet: Character.GenerateSay(
                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "CHARACTER_LOGGED_OUT"),
                        arg0: loggedOutCharacter.Item2), type: 10));
                _client.SendPacket(packet: Character.GenerateFinfo(relatedCharacterLoggedId: loggedOutCharacter.Item1,
                    isConnected: false));
            }
        }

        void TriggerHandler(string packetHeader, string packet, bool force, bool ignoreAuthority = false)
        {
            if (ServerManager.Instance.InShutdown) return;
            if (!IsDisposing)
            {
                if (Account?.Name != null && UserLog.Contains(item: Account.Name))
                    try
                    {
                        File.AppendAllText(path: $"C:\\{Account.Name.Replace(oldValue: " ", newValue: "")}.txt",
                            contents: packet + "\n");
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex: ex);
                    }

                var key = HandlerMethods.Keys.FirstOrDefault(predicate: s =>
                    s.Any(predicate: m => string.Equals(a: m, b: packetHeader,
                        comparisonType: StringComparison.CurrentCultureIgnoreCase)));
                var methodReference = key != null ? HandlerMethods[key: key] : null;
                if (methodReference != null)
                {
                    if (methodReference.HandlerMethodAttribute != null && !force &&
                        methodReference.HandlerMethodAttribute.Amount > 1 && !_waitForPacketsAmount.HasValue)
                    {
                        // we need to wait for more
                        _waitForPacketsAmount = methodReference.HandlerMethodAttribute.Amount;
                        _waitForPacketList.Add(item: packet != "" ? packet : $"1 {packetHeader} ");
                        return;
                    }

                    try
                    {
                        if (HasSelectedCharacter ||
                            methodReference.ParentHandler.GetType().Name == "CharacterScreenPacketHandler" ||
                            methodReference.ParentHandler.GetType().Name == "LoginPacketHandler")
                        {
                            // call actual handler method
                            if (methodReference.PacketDefinitionParameterType != null)
                            {
                                //check for the correct authority
                                if (Account != null && (!IsAuthenticated
                                                        || Account.Authority.Equals(obj: AuthorityType.Administrator)
                                                        || methodReference.Authorities.Any(predicate: a =>
                                                            a.Equals(obj: Account.Authority))
                                                        || methodReference.Authorities.Any(
                                                            predicate: a => a.Equals(obj: AuthorityType.User)) &&
                                                        Account.Authority >= AuthorityType.User
                                                        || methodReference.Authorities.Any(predicate: a =>
                                                            a.Equals(obj: AuthorityType.Mod)) &&
                                                        Account.Authority >= AuthorityType.Mod &&
                                                        Account.Authority <= AuthorityType.Gm
                                                        || methodReference.Authorities.Any(predicate: a =>
                                                            a.Equals(obj: AuthorityType.Gm)) &&
                                                        Account.Authority >= AuthorityType.Gm
                                                        || methodReference.Authorities.Any(predicate: a =>
                                                            a.Equals(obj: AuthorityType.Administrator)) &&
                                                        methodReference.Authorities.Any(predicate: a =>
                                                            a.Equals(obj: AuthorityType.User))
                                                        || ignoreAuthority))
                                {
                                    var deserializedPacket = PacketFactory.Deserialize(packetContent: packet,
                                        packetType: methodReference.PacketDefinitionParameterType,
                                        includesKeepAliveIdentity: IsAuthenticated);
                                    if (deserializedPacket != null || methodReference.PassNonParseablePacket)
                                        /*if (ServerManager.Instance.Configuration != null && ServerManager.Instance.Configuration.UseLogService && Character != null)
                                            {
                                                try
                                                {
                                                    string message = "";
                                                    string[] valuesplit = deserializedPacket.OriginalContent?.Split(' ');
                                                    if (valuesplit == null)
                                                    {
                                                        return;
                                                    }
    
                                                    if (valuesplit[1] != null && valuesplit[1] != "walk" && valuesplit[1] != "ncif" && valuesplit[1] != "say" && valuesplit[1] != "preq")
                                                    {
                                                        for (int i = 0; i < valuesplit.Length; i++)
                                                        {
                                                            if (i > 0)
                                                            {
                                                                message += valuesplit[i] + " ";
                                                            }
                                                        }
                                                        message = message.Substring(0, message.Length - 1); // Remove the last " "
                                                        try
                                                        {
                                                            LogServiceClient.Instance.LogPacket(new PacketLogEntry()
                                                            {
                                                                Sender = Character.Name,
                                                                SenderId = Character.CharacterId,
                                                                PacketType = LogType.Packet,
                                                                Packet = message
                                                            });
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Logger.Error("PacketLog Error. SessionId: " + SessionId, ex);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logger.Error("PacketLog Error. SessionId: " + SessionId, ex);
                                                }
                                            }*/
                                        methodReference.HandlerMethod(arg1: methodReference.ParentHandler,
                                            arg2: deserializedPacket);
                                    else
                                        Logger.Warn(data: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "CORRUPT_PACKET"),
                                            arg0: packetHeader,
                                            arg1: packet));
                                }
                            }
                            else
                            {
                                methodReference.HandlerMethod(arg1: methodReference.ParentHandler, arg2: packet);
                            }
                        }
                    }
                    catch (DivideByZeroException ex)
                    {
                        // disconnect if something unexpected happens
                        Logger.Error(data: "Handler Error SessionId: " + SessionId, ex: ex);
                        Disconnect();
                    }
                }
                else
                {
                    Logger.Warn(
                        data:
                        $"{string.Format(format: Language.Instance.GetMessageFromKey(key: "HANDLER_NOT_FOUND"), arg0: packetHeader)} From IP: {_client.IpAddress}");
                }
            }
            else
            {
                Logger.Warn(data: string.Format(
                    format: Language.Instance.GetMessageFromKey(key: "CLIENTSESSION_DISPOSING"),
                    arg0: packetHeader));
            }
        }

        #endregion
    }
}