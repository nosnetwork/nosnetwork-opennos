﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.Core.Threading;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Buff;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Event.ACT4;
using OpenNos.GameObject.Helpers;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;
using OpenNos.XMLModel.Models.Quest;

namespace OpenNos.GameObject.Networking
{
    public class ServerManager : BroadcastableBase
    {
        #region Instantiation

        ServerManager()
        {
        }

        #endregion

        #region Members

        public ThreadSafeSortedList<long, Group> ThreadSafeGroupList;

        public bool InShutdown;

        public bool IsReboot { get; set; }

        public bool ShutdownStop;

        static readonly ConcurrentBag<Card> _cards = new ConcurrentBag<Card>();

        static readonly ConcurrentBag<Item> _items = new ConcurrentBag<Item>();

        static readonly ConcurrentDictionary<Guid, MapInstance> _mapinstances =
            new ConcurrentDictionary<Guid, MapInstance>();

        static readonly ConcurrentBag<Map> _maps = new ConcurrentBag<Map>();

        static readonly ConcurrentBag<NpcMonster> _npcmonsters = new ConcurrentBag<NpcMonster>();

        static readonly ConcurrentBag<Skill> _skills = new ConcurrentBag<Skill>();

        static ServerManager _instance;

        List<DropDto> _generalDrops;

        bool _inRelationRefreshMode;

        long _lastGroupId;

        ThreadSafeSortedList<short, List<MapNpc>> _mapNpcs;

        ThreadSafeSortedList<short, List<DropDto>> _monsterDrops;

        ThreadSafeSortedList<short, List<NpcMonsterSkill>> _monsterSkills;

        ThreadSafeSortedList<int, RecipeListDto> _recipeLists;

        ThreadSafeSortedList<short, Recipe> _recipes;

        ThreadSafeSortedList<int, List<ShopItemDto>> _shopItems;

        ThreadSafeSortedList<int, Shop> _shops;

        ThreadSafeSortedList<int, List<ShopSkillDto>> _shopSkills;

        ThreadSafeSortedList<int, List<TeleporterDto>> _teleporters;

        #endregion

        #region Properties

        public static ServerManager Instance
        {
            get => _instance ?? (_instance = new ServerManager());
        }

        public Act4Stat Act4AngelStat { get; set; }

        public Act4Stat Act4DemonStat { get; set; }

        public DateTime Act4RaidStart { get; set; }

        public MapInstance ArenaInstance { get; private set; }

        public List<ArenaMember> ArenaMembers { get; set; } = new List<ArenaMember>();

        public List<ConcurrentBag<ArenaTeamMember>> ArenaTeams { get; set; } =
            new List<ConcurrentBag<ArenaTeamMember>>();

        public List<long> BannedCharacters { get; set; } = new List<long>();

        public ThreadSafeGenericList<BazaarItemLink> BazaarList { get; set; }

        public List<short> BossVNums { get; set; }

        public List<short> MapBossVNums { get; set; }

        public int ChannelId { get; set; }

        public List<CharacterRelationDto> CharacterRelations { get; set; }

        public ConfigurationObject Configuration { get; set; }

        public bool EventInWaiting { get; set; }

        public MapInstance FamilyArenaInstance { get; private set; }

        public ThreadSafeSortedList<long, Family> FamilyList { get; set; }

        public ThreadSafeGenericLockedList<Group> GroupList { get; set; } = new ThreadSafeGenericLockedList<Group>();

        public List<Group> Groups
        {
            get => ThreadSafeGroupList.GetAllItems();
        }

        public bool IceBreakerInWaiting { get; set; }

        public bool InBazaarRefreshMode { get; set; }

        public DateTime LastFCSent { get; set; }

        DateTime LastMaintenanceAdvert { get; set; }

        public MallAPIHelper MallApi { get; set; }

        public List<int> MateIds { get; internal set; } = new List<int>();

        public List<PenaltyLogDto> PenaltyLogs { get; set; }

        public ThreadSafeSortedList<long, QuestModel> QuestList { get; set; }

        public ConcurrentBag<ScriptedInstance> Raids { get; set; }

        public ConcurrentBag<ScriptedInstance> TimeSpaces { get; set; }

        public List<Schedule> Schedules { get; set; }

        public string ServerGroup { get; set; }

        public List<MapInstance> SpecialistGemMapInstances { get; set; } = new List<MapInstance>();

        public List<EventType> StartedEvents { get; set; }

        public Task TaskShutdown { get; set; }

        public List<CharacterDto> TopComplimented { get; set; }

        public List<CharacterDto> TopPoints { get; set; }

        public List<CharacterDto> TopReputation { get; set; }

        public Guid WorldId { get; private set; }

        public ThreadSafeSortedList<long, ClientSession> CharacterScreenSessions { get; set; }

        public List<Quest> Quests { get; set; }

        public long? FlowerQuestId { get; set; }

        public bool IsWorldServer
        {
            get => WorldId != Guid.Empty;
        }

        #endregion

        #region Methods

        public List<MapNpc> GetMapNpcsByVNum(short npcVNum)
        {
            return GetAllMapInstances()
                .Where(predicate: mapInstance => mapInstance != null && !mapInstance.IsScriptedInstance)
                .SelectMany(selector: mapInstance =>
                    mapInstance.Npcs.Where(predicate: mapNpc => mapNpc?.NpcVNum == npcVNum)).ToList();
        }

        public void AddGroup(Group group)
        {
            ThreadSafeGroupList[key: group.GroupId] = group;
        }

        public void AskPvpRevive(long characterId)
        {
            var session = GetSessionByCharacterId(characterId: characterId);
            if (session?.HasSelectedCharacter == true)
            {
                if (session.Character.IsVehicled) session.Character.RemoveVehicle();
                session.Character.DisableBuffs(type: BuffType.All);
                session.Character.BattleEntity.AdditionalHp = 0;
                session.Character.BattleEntity.AdditionalMp = 0;
                session.SendPacket(packet: session.Character.GenerateAdditionalHpMp());
                session.SendPacket(packet: session.Character.GenerateStat());
                session.SendPacket(packet: session.Character.GenerateCond());
                session.SendPackets(packets: UserInterfaceHelper.GenerateVb());

                session.Character.BattleEntity.RemoveOwnedMonsters();

                switch (session.CurrentMapInstance.MapInstanceType)
                {
                    case MapInstanceType.TalentArenaMapInstance:
                        var team = Instance.ArenaTeams.ToList()
                            .FirstOrDefault(predicate: s => s.Any(predicate: o => o.Session == session));
                        var member = team?.FirstOrDefault(predicate: s => s.Session == session);
                        if (member != null)
                        {
                            if (member.LastSummoned == null && team.OrderBy(keySelector: tm3 => tm3.Order)
                                .FirstOrDefault(predicate: tm3 =>
                                    tm3.ArenaTeamType == member.ArenaTeamType && !tm3.Dead)
                                ?.Session == session)
                            {
                                session.CurrentMapInstance.InstanceBag.DeadList.Add(
                                    item: session.Character.CharacterId);
                                member.Dead = true;
                                team.ToList().Where(predicate: s => s.LastSummoned != null).ToList().ForEach(
                                    action: s =>
                                    {
                                        s.LastSummoned = null;
                                        s.Session.Character.PositionX = s.ArenaTeamType == ArenaTeamType.Erenia
                                            ? (short)120
                                            : (short)19;
                                        s.Session.Character.PositionY = s.ArenaTeamType == ArenaTeamType.Erenia
                                            ? (short)39
                                            : (short)40;
                                        session.CurrentMapInstance.Broadcast(packet: s.Session.Character.GenerateTp());
                                        s.Session.SendPacket(
                                            packet: UserInterfaceHelper.Instance.GenerateTaSt(
                                                watch: TalentArenaOptionType.Watch));

                                        var bufftodisable = new List<BuffType> { BuffType.Bad };
                                        s.Session.Character.DisableBuffs(types: bufftodisable);
                                        s.Session.Character.Hp = (int)s.Session.Character.HPLoad();
                                        s.Session.Character.Mp = (int)s.Session.Character.MPLoad();
                                    });
                                var killer = team.OrderBy(keySelector: s => s.Order)
                                    .FirstOrDefault(predicate: s => !s.Dead && s.ArenaTeamType != member.ArenaTeamType);
                                session.CurrentMapInstance.Broadcast(packet: session.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "TEAM_WINNER_ARENA_ROUND"),
                                        arg0: killer?.Session.Character.Name, arg1: killer?.ArenaTeamType), type: 10));
                                session.CurrentMapInstance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "TEAM_WINNER_ARENA_ROUND"),
                                        arg0: killer?.Session.Character.Name, arg1: killer?.ArenaTeamType), type: 0));
                                session.CurrentMapInstance.Sessions
                                    .Except(second: team.Where(predicate: s => s.ArenaTeamType == killer?.ArenaTeamType)
                                        .Select(selector: s => s.Session)).ToList().ForEach(action: o =>
                                    {
                                        if (killer?.ArenaTeamType == ArenaTeamType.Erenia)
                                        {
                                            o.SendPacket(packet: killer.Session.Character.GenerateTaM(type: 2));
                                            o.SendPacket(
                                                packet: killer.Session.Character.GenerateTaP(tatype: 2,
                                                    showOponent: true));
                                        }
                                        else
                                        {
                                            o.SendPacket(packet: member.Session.Character.GenerateTaM(type: 2));
                                            o.SendPacket(
                                                packet: member.Session.Character.GenerateTaP(tatype: 2,
                                                    showOponent: true));
                                        }

                                        o.SendPacket(packet: $"taw_d {member.Session.Character.CharacterId}");
                                        o.SendPacket(packet: member.Session.Character.GenerateSay(
                                            message: string.Format(
                                                format: Language.Instance.GetMessageFromKey(key: "WINNER_ARENA_ROUND"),
                                                arg0: killer?.Session.Character.Name /*, killer?.ArenaTeamType*/,
                                                arg1: member.Session.Character.Name), type: 10));
                                        o.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                            message: string.Format(
                                                format: Language.Instance.GetMessageFromKey(key: "WINNER_ARENA_ROUND"),
                                                arg0: killer?.Session.Character.Name /*, killer?.ArenaTeamType*/,
                                                arg1: member.Session.Character.Name), type: 0));
                                    });
                                team.Replace(predicate: friends => friends.ArenaTeamType == member.ArenaTeamType)
                                    .ToList()
                                    .ForEach(action: friends =>
                                    {
                                        friends.Session.SendPacket(
                                            packet: friends.Session.Character.GenerateTaFc(type: 0));
                                    });
                            }
                            else
                            {
                                member.LastSummoned = null;
                                var tm = team.OrderBy(keySelector: tm3 => tm3.Order).FirstOrDefault(predicate: tm3 =>
                                    tm3.ArenaTeamType == member.ArenaTeamType && !tm3.Dead);
                                team.Replace(predicate: friends => friends.ArenaTeamType == member.ArenaTeamType)
                                    .ToList()
                                    .ForEach(action: friends =>
                                    {
                                        if (tm != null)
                                            friends.Session.SendPacket(
                                                packet: tm.Session.Character.GenerateTaFc(type: 0));
                                    });
                            }

                            team.ToList().ForEach(action: arenauser =>
                            {
                                if (arenauser?.Session?.Character != null)
                                {
                                    arenauser.Session.SendPacket(
                                        packet: arenauser.Session.Character.GenerateTaP(tatype: 2, showOponent: true));
                                    arenauser.Session.SendPacket(
                                        packet: arenauser.Session.Character.GenerateTaM(type: 2));
                                }
                            });

                            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 3)).Subscribe(onNext: s =>
                            {
                                if (member?.Session != null)
                                {
                                    member.Session.Character.PositionX = member.ArenaTeamType == ArenaTeamType.Erenia
                                        ? (short)120
                                        : (short)19;
                                    member.Session.Character.PositionY = member.ArenaTeamType == ArenaTeamType.Erenia
                                        ? (short)39
                                        : (short)40;
                                    member.Session.CurrentMapInstance.Broadcast(client: member.Session,
                                        content: member.Session.Character.GenerateTp());
                                    member.Session.SendPacket(
                                        packet: UserInterfaceHelper.Instance.GenerateTaSt(
                                            watch: TalentArenaOptionType.Watch));
                                }
                            });

                            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 4)).Subscribe(onNext: s =>
                            {
                                if (session != null)
                                {
                                    session.Character.Hp = (int)session.Character.HPLoad();
                                    session.Character.Mp = (int)session.Character.MPLoad();
                                    session.CurrentMapInstance?.Broadcast(client: session,
                                        content: session.Character.GenerateRevive());
                                    session.SendPacket(packet: session.Character.GenerateStat());
                                }
                            });
                        }

                        break;

                    default:
                        if (session.CurrentMapInstance == ArenaInstance ||
                            session.CurrentMapInstance == FamilyArenaInstance)
                        {
                            session.Character.LeaveTalentArena(surrender: true);
                            session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                dialog:
                                $"#revival^2 #revival^1 {Language.Instance.GetMessageFromKey(key: "ASK_REVIVE_PVP")}"));
                            Task.Factory.StartNew(function: async () =>
                            {
                                var revive = true;
                                for (var i = 1; i <= 30; i++)
                                {
                                    await Task.Delay(millisecondsDelay: 1000);
                                    if (session.Character.Hp <= 0) continue;

                                    revive = false;
                                    break;
                                }

                                if (revive) ReviveTask(session: session);
                            });
                        }
                        else
                        {
                            AskRevive(characterId: characterId);
                        }

                        break;
                }
            }
        }

        // PacketHandler -> with Callback?
        public void AskRevive(long characterId)
        {
            var session = GetSessionByCharacterId(characterId: characterId);
            if (session?.HasSelectedCharacter == true && session.HasCurrentMapInstance)
            {
                if (session.Character.IsVehicled) session.Character.RemoveVehicle();

                session.Character.ClearLaurena();

                session.Character.DisableBuffs(type: BuffType.All);
                session.Character.BattleEntity.AdditionalHp = 0;
                session.Character.BattleEntity.AdditionalMp = 0;
                session.SendPacket(packet: session.Character.GenerateAdditionalHpMp());
                session.SendPacket(packet: session.Character.GenerateStat());
                session.SendPacket(packet: session.Character.GenerateCond());
                session.SendPackets(packets: UserInterfaceHelper.GenerateVb());

                switch (session.CurrentMapInstance.MapInstanceType)
                {
                    case MapInstanceType.BaseMapInstance:
                        if (session.Character.Level > 20 && ChannelId != 51)
                        {
                            session.Character.Dignity -=
                                (short)(session.Character.Level < 50 ? session.Character.Level : 50);
                            if (session.Character.Dignity < -1000)
                            {
                                session.Character.Dignity = -1000;
                                session.SendPacket(packet: session.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "LOSE_DIGNITY"),
                                        arg0: (short)(session.Character.Level < 50 ? session.Character.Level : 50)),
                                    type: 11));
                            }

                            session.SendPacket(packet: session.Character.GenerateFd());
                            session.CurrentMapInstance?.Broadcast(client: session,
                                content: session.Character.GenerateIn(InEffect: 1),
                                receiver: ReceiverType.AllExceptMe);
                            session.CurrentMapInstance?.Broadcast(client: session,
                                content: session.Character.GenerateGidx(),
                                receiver: ReceiverType.AllExceptMe);
                        }

                        session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                            dialog:
                            $"#revival^0 #revival^1 {(session.Character.Level > 20 ? Language.Instance.GetMessageFromKey(key: "ASK_REVIVE") : Language.Instance.GetMessageFromKey(key: "ASK_REVIVE_FREE"))}"));
                        ReviveTask(session: session);
                        break;

                    case MapInstanceType.TimeSpaceInstance:
                        lock (session.CurrentMapInstance.InstanceBag.DeadList)
                        {
                            if (session.CurrentMapInstance.InstanceBag.Lives - session.CurrentMapInstance.InstanceBag
                                .DeadList.ToList().Count(predicate: s => s == session.Character.CharacterId) < 0)
                            {
                                session.Character.Hp = 1;
                                session.Character.Mp = 1;
                            }
                            else
                            {
                                session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "YOU_HAVE_LIFE"),
                                        arg0: session.CurrentMapInstance.InstanceBag.Lives -
                                              session.CurrentMapInstance.InstanceBag.DeadList.Count(predicate: e =>
                                                  e == session.Character.CharacterId)), type: 0));
                                session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                    dialog:
                                    $"#revival^1 #revival^1 {Language.Instance.GetMessageFromKey(key: "ASK_REVIVE_TS")}"));
                                ReviveTask(session: session);
                            }
                        }

                        break;

                    case MapInstanceType.RaidInstance:
                        var save = session.CurrentMapInstance.InstanceBag.DeadList.ToList();
                        if (session.CurrentMapInstance.InstanceBag.Lives - save.Count < 0)
                        {
                            session.Character.Hp = 1;
                            session.Character.Mp = 1;
                            session.Character.Group?.Raid.End();
                        }
                        else if (3 - save.Count(predicate: s => s == session.Character.CharacterId) > 0)
                        {
                            session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "YOU_HAVE_LIFE"),
                                    arg0: 2 - session.CurrentMapInstance.InstanceBag.DeadList.Count(predicate: s =>
                                        s == session.Character.CharacterId))));

                            session.Character.Group?.Sessions.ForEach(action: grpSession =>
                            {
                                grpSession?.SendPacket(
                                    packet: grpSession.Character.Group?.GeneraterRaidmbf(session: grpSession));
                                grpSession?.SendPacket(packet: grpSession.Character.Group?.GenerateRdlst());
                            });
                            Task.Factory.StartNew(function: async () =>
                            {
                                await Task.Delay(millisecondsDelay: 20000)
                                    .ConfigureAwait(continueOnCapturedContext: false);
                                Instance.ReviveFirstPosition(characterId: session.Character.CharacterId);
                            });
                        }
                        else
                        {
                            var grp = session.Character?.Group;
                            if (session.Character != null)
                            {
                                session.Character.Hp = 1;
                                session.Character.Mp = 1;
                                ChangeMap(id: session.Character.CharacterId, mapId: session.Character.MapId,
                                    mapX: session.Character.MapX,
                                    mapY: session.Character.MapY);
                                session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "KICK_RAID"), type: 0));
                                if (grp != null)
                                {
                                    grp.LeaveGroup(session: session);
                                    grp.Sessions.ForEach(action: s =>
                                    {
                                        s.SendPacket(packet: grp.GenerateRdlst());
                                        s.SendPacket(packet: s.Character.Group?.GeneraterRaidmbf(session: s));
                                        s.SendPacket(packet: s.Character.GenerateRaid(Type: 0));
                                    });
                                }

                                session.SendPacket(packet: session.Character.GenerateRaid(Type: 1, exit: true));
                                session.SendPacket(packet: session.Character.GenerateRaid(Type: 2, exit: true));
                            }
                        }

                        break;

                    case MapInstanceType.LodInstance:
                        const int saver = 1211;
                        if (session.Character.Inventory.CountItem(itemVNum: saver) >= 1)
                        {
                            session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                dialog:
                                $"#revival^0 #revival^1 {Language.Instance.GetMessageFromKey(key: "ASK_REVIVE_LOD")}"));
                            ReviveTask(session: session);
                        }
                        else
                        {
                            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 10)).Subscribe(onNext: o =>
                                Instance.ReviveFirstPosition(characterId: session.Character.CharacterId));
                        }

                        break;

                    case MapInstanceType.Act4Berios:
                    case MapInstanceType.Act4Calvina:
                    case MapInstanceType.Act4Hatus:
                    case MapInstanceType.Act4Morcos:
                        session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                            dialog:
                            $"#revival^0 #revival^1 {string.Format(format: Language.Instance.GetMessageFromKey(key: "ASK_REVIVE_ACT4RAID"), arg0: session.Character.Level * 10)}"));
                        ReviveTask(session: session);
                        break;

                    case MapInstanceType.CaligorInstance:
                        session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                            dialog:
                            $"#revival^0 #revival^1 {Language.Instance.GetMessageFromKey(key: "ASK_REVIVE_CALIGOR")}"));
                        ReviveTask(session: session);
                        break;

                    default:
                        Instance.ReviveFirstPosition(characterId: session.Character.CharacterId);
                        break;
                }
            }
        }

        public void BazaarRefresh(long bazaarItemId)
        {
            InBazaarRefreshMode = true;
            CommunicationServiceClient.Instance.UpdateBazaar(worldGroup: ServerGroup, bazaarItemId: bazaarItemId);
            SpinWait.SpinUntil(condition: () => !InBazaarRefreshMode);
        }

        public void ChangeMap(long id, short? mapId = null, short? mapX = null, short? mapY = null)
        {
            var session = GetSessionByCharacterId(characterId: id);
            if (session?.Character != null)
            {
                if (mapId != null)
                {
                    var gotoMapInstance = GetMapInstanceByMapId(mapId: mapId.Value);
                    if (session.Character.Level < gotoMapInstance.MinLevel ||
                        session.Character.Level > gotoMapInstance.MaxLevel)
                    {
                        session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(message: string.Format(
                            format: Language.Instance.GetMessageFromKey(key: "LOW_LVL_MAP"),
                            arg0: gotoMapInstance.MinLevel,
                            arg1: gotoMapInstance.MaxLevel)));
                        return;
                    }

                    session.Character.MapInstanceId = GetBaseMapInstanceIdByMapId(mapId: (short)mapId);
                }

                ChangeMapInstance(characterId: id, mapInstanceId: session.Character.MapInstanceId, mapX: mapX,
                    mapY: mapY);
            }
        }

        // Both partly
        public void ChangeMapInstance(long characterId, Guid mapInstanceId, int? mapX = null, int? mapY = null,
            bool noAggroLoss = false)
        {
            var session = GetSessionByCharacterId(characterId: characterId);
            if (session?.Character != null && !session.Character.IsChangingMapInstance)
            {
                session.Character.IsChangingMapInstance = true;

                session.Character.RemoveBuff(cardId: 620);

                session.Character.WalkDisposable?.Dispose();
                SpinWait.SpinUntil(condition: () =>
                    session.Character.LastSkillUse.AddMilliseconds(value: 500) <= DateTime.Now);
                try
                {
                    var gotoMapInstance = GetMapInstance(id: mapInstanceId);
                    if (session.Character.Level < gotoMapInstance.MinLevel ||
                        session.Character.Level > gotoMapInstance.MaxLevel)
                    {
                        session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(message: string.Format(
                            format: Language.Instance.GetMessageFromKey(key: "LOW_LVL_MAP"),
                            arg0: gotoMapInstance.MinLevel,
                            arg1: gotoMapInstance.MaxLevel)));
                        session.Character.IsChangingMapInstance = false;
                        return;
                    }

                    session.SendPacket(packet: StaticPacketHelper.Cancel(type: 2, callerId: characterId));

                    if (session.Character.InExchangeOrTrade) session.Character.CloseExchangeOrTrade();

                    if (session.Character.HasShopOpened) session.Character.CloseShop();

                    session.Character.BattleEntity.ClearOwnFalcon();
                    session.Character.BattleEntity.ClearEnemyFalcon();

                    if (!noAggroLoss)
                        session.CurrentMapInstance.RemoveMonstersTarget(characterId: session.Character.CharacterId);

                    session.Character.BattleEntity.RemoveOwnedMonsters();

                    if (gotoMapInstance == CaligorRaid.CaligorMapInstance &&
                        session.Character.MapInstance != CaligorRaid.CaligorMapInstance)
                    {
                        session.Character.OriginalFaction = (byte)session.Character.Faction;
                        if (CaligorRaid.CaligorMapInstance.Sessions.ToList()
                                .Count(predicate: s => s.Character?.Faction == FactionType.Angel) >
                            CaligorRaid.CaligorMapInstance.Sessions.ToList()
                                .Count(predicate: s => s.Character?.Faction == FactionType.Demon)
                            && session.Character.Faction != FactionType.Demon)
                        {
                            session.Character.Faction = FactionType.Demon;
                            session.SendPacket(packet: session.Character.GenerateFaction());
                        }
                        else if (CaligorRaid.CaligorMapInstance.Sessions.ToList()
                                     .Count(predicate: s => s.Character?.Faction == FactionType.Demon) >
                                 CaligorRaid.CaligorMapInstance.Sessions.ToList()
                                     .Count(predicate: s => s.Character?.Faction == FactionType.Angel)
                                 && session.Character.Faction != FactionType.Angel)
                        {
                            session.Character.Faction = FactionType.Angel;
                            session.SendPacket(packet: session.Character.GenerateFaction());
                        }

                        if (mapX <= 0 && mapY <= 0)
                            switch (session.Character.Faction)
                            {
                                case FactionType.Angel:
                                    mapX = 58;
                                    mapY = 164;
                                    break;
                                case FactionType.Demon:
                                    mapX = 121;
                                    mapY = 164;
                                    break;
                            }
                    }
                    else if (gotoMapInstance != CaligorRaid.CaligorMapInstance &&
                             session.Character.MapInstance == CaligorRaid.CaligorMapInstance)
                    {
                        if (session.Character.OriginalFaction != -1 &&
                            (byte)session.Character.Faction != session.Character.OriginalFaction)
                        {
                            session.Character.Faction = (FactionType)session.Character.OriginalFaction;
                            session.SendPacket(packet: session.Character.GenerateFaction());
                        }
                    }

                    session.CurrentMapInstance.UnregisterSession(characterId: session.Character.CharacterId);
                    LeaveMap(id: session.Character.CharacterId);

                    // cleanup sending queue to avoid sending uneccessary packets to it
                    session.ClearLowPriorityQueue();

                    session.Character.IsSitting = false;
                    session.Character.MapInstanceId = mapInstanceId;
                    session.CurrentMapInstance = session.Character.MapInstance;

                    if (!session.Character.MapInstance.MapInstanceType.Equals(obj: MapInstanceType.TimeSpaceInstance) &&
                        session.Character.Timespace != null)
                    {
                        session.Character.TimespaceRewardGotten = false;
                        session.Character.RemoveTemporalMates();
                        if (session.Character.Timespace.SpNeeded?[(byte)session.Character.Class] != 0)
                        {
                            var specialist =
                                session.Character.Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                                    type: InventoryType.Wear);
                            if (specialist != null && (specialist != null || specialist.ItemVNum ==
                                session.Character.Timespace.SpNeeded?[
                                    (byte)session.Character.Class]))
                                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 300)).Subscribe(onNext: s =>
                                    session.Character.RemoveSp(vnum: specialist.ItemVNum, forced: true));
                        }

                        session.Character.Timespace = null;
                    }

                    if (session.Character.Hp <= 0 && !session.Character.IsSeal)
                    {
                        session.Character.Hp = 1;
                        session.Character.Mp = 1;
                    }

                    session.Character.LeaveTalentArena();

                    if (session.Character.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                    {
                        session.Character.MapId = session.Character.MapInstance.Map.MapId;
                        if (mapX != null && mapY != null)
                        {
                            session.Character.MapX = (short)mapX.Value;
                            session.Character.MapY = (short)mapY.Value;
                        }
                    }

                    if (mapX != null && mapY != null)
                    {
                        session.Character.PositionX = (short)mapX.Value;
                        session.Character.PositionY = (short)mapY.Value;
                    }

                    foreach (var mate in session.Character.Mates.Where(predicate: m =>
                        m.IsTeamMember && !session.Character.IsVehicled || m.IsTemporalMate))
                    {
                        mate.PositionX =
                            (short)(session.Character.PositionX + (mate.MateType == MateType.Partner ? -1 : 1));
                        mate.PositionY = (short)(session.Character.PositionY + 1);
                        if (session.Character.MapInstance.Map.IsBlockedZone(x: mate.PositionX, y: mate.PositionY))
                        {
                            mate.PositionX = session.Character.PositionX;
                            mate.PositionY = session.Character.PositionY;
                        }

                        mate.UpdateBushFire();
                    }

                    session.Character.UpdateBushFire();
                    session.CurrentMapInstance.RegisterSession(session: session);
                    session.Character.LoadSpeed();

                    if (gotoMapInstance.Map?.MapId != 2514) session.Character.ClearLaurena();

                    session.SendPacket(packet: session.Character.GenerateCInfo());
                    session.SendPacket(packet: session.Character.GenerateCMode());
                    session.SendPacket(packet: session.Character.GenerateEq());
                    session.SendPacket(packet: session.Character.GenerateEquipment());
                    session.SendPacket(packet: session.Character.GenerateLev());
                    session.SendPacket(packet: session.Character.GenerateStat());
                    session.SendPacket(packet: session.Character.GenerateAt());
                    session.SendPacket(packet: session.Character.GenerateCond());
                    session.SendPacket(packet: session.Character.GenerateCMap());
                    session.SendPackets(packets: session.Character.GenerateStatChar());
                    session.SendPacket(packet: session.Character.GeneratePairy());
                    session.SendPacket(packet: Character.GenerateAct());
                    session.SendPacket(packet: session.Character.GenerateScpStc());

                    if (session.CurrentMapInstance.OnSpawnEvents.Any())
                        session.CurrentMapInstance.OnSpawnEvents.ForEach(action: e =>
                            EventHelper.Instance.RunEvent(evt: e, session: session));

                    if (ChannelId == 51)
                    {
                        session.SendPacket(packet: session.Character.GenerateFc());

                        if (mapInstanceId == session.Character.Family?.Act4Raid?.MapInstanceId ||
                            mapInstanceId == session.Character.Family?.Act4RaidBossMap?.MapInstanceId)
                            session.SendPacket(packet: session.Character.GenerateDG());
                    }

                    if (session.Character.Group?.Raid?.InstanceBag?.Lock == true)
                    {
                        session.SendPacket(packet: session.Character.Group.GeneraterRaidmbf(session: session));

                        if (session.CurrentMapInstance.Monsters.Any(predicate: s => s.IsBoss))
                            session.Character.Group.Sessions?.Where(predicate: s => s?.Character != null).ForEach(
                                action: s =>
                                {
                                    if (!s.Character.IsChangingMapInstance &&
                                        s.CurrentMapInstance != session.CurrentMapInstance)
                                        ChangeMapInstance(characterId: s.Character.CharacterId,
                                            mapInstanceId: session.CurrentMapInstance.MapInstanceId,
                                            mapX: mapX, mapY: mapY);
                                });
                    }

                    if (session.Character.MapInstance == session.Character.Family?.Act4RaidBossMap)
                        session.Character.Family.Act4Raid.Sessions
                            .Where(predicate: s => !s.Character.IsChangingMapInstance)
                            .ToList().ForEach(action: s =>
                            {
                                ChangeMapInstance(characterId: s.Character.CharacterId,
                                    mapInstanceId: session.CurrentMapInstance.MapInstanceId,
                                    mapX: mapX, mapY: mapY);
                            });

                    Parallel.ForEach(
                        source: session.CurrentMapInstance.Sessions.Where(predicate: s =>
                            s.Character?.InvisibleGm == false &&
                            s.Character.CharacterId != session.Character.CharacterId), body: visibleSession =>
                        {
                            if (ChannelId != 51 || session.Character.Faction == visibleSession.Character.Faction)
                            {
                                session.SendPacket(packet: visibleSession.Character.GenerateIn());
                                session.SendPacket(packet: visibleSession.Character.GenerateGidx());
                                visibleSession.Character.Mates
                                    .Where(predicate: m => (m.IsTeamMember || m.IsTemporalMate) &&
                                                           m.CharacterId != session.Character.CharacterId)
                                    .ToList().ForEach(action: m => session.SendPacket(packet: m.GenerateIn()));
                            }
                            else
                            {
                                session.SendPacket(
                                    packet: visibleSession.Character.GenerateIn(foe: true,
                                        receiverAuthority: session.Account.Authority));
                                visibleSession.Character.Mates
                                    .Where(predicate: m => (m.IsTeamMember || m.IsTemporalMate) &&
                                                           m.CharacterId != session.Character.CharacterId)
                                    .ToList().ForEach(action: m =>
                                        session.SendPacket(packet: m.GenerateIn(hideNickname: true,
                                            isAct4: ChannelId == 51,
                                            receiverAuthority: session.Account.Authority)));
                            }
                        });
                    session.SendPacket(packet: session.CurrentMapInstance.GenerateMapDesignObjects());
                    session.SendPackets(packets: session.CurrentMapInstance.GetMapDesignObjectEffects());

                    session.CurrentMapInstance.Npcs.Where(predicate: npc => npc != null).ToList().ForEach(action: npc =>
                    {
                        if (npc.IsMate)
                        {
                            var npcPartner = new Mate(owner: session.Character, npcMonster: npc.Npc,
                                level: npc.Npc.Level, matetype: MateType.Partner, temporal: true,
                                tsReward: npc.IsTsReward, tsProtected: npc.IsProtected);
                            session.Character.AddPet(mate: npcPartner);
                            session.CurrentMapInstance.RemoveNpc(npcToRemove: npc);
                        }
                    });

                    session.SendPackets(packets: session.CurrentMapInstance.GetMapItems());

                    MapInstancePortalHandler
                        .GenerateMinilandEntryPortals(entryMap: session.CurrentMapInstance.Map.MapId,
                            exitMapinstanceId: session.Character.Miniland.MapInstanceId)
                        .ForEach(action: p => session.SendPacket(packet: p.GenerateGp()));
                    MapInstancePortalHandler
                        .GenerateAct4EntryPortals(entryMap: session.CurrentMapInstance.Map.MapId)
                        .ForEach(action: p => session.SendPacket(packet: p.GenerateGp()));

                    if (session.CurrentMapInstance.InstanceBag?.Clock?.Enabled == true)
                        session.SendPacket(packet: session.CurrentMapInstance.InstanceBag.Clock.GetClock());

                    if (session.CurrentMapInstance.Clock.Enabled)
                        session.SendPacket(packet: session.CurrentMapInstance.Clock.GetClock());

                    // TODO: fix this
                    if (session.Character.MapInstance.Map.MapTypes.Any(predicate: m =>
                        m.MapTypeId == (short)MapTypeEnum.CleftOfDarkness))
                        session.SendPacket(packet: "bc 0 0 0");

                    if (!session.Character.InvisibleGm)
                        Parallel.ForEach(
                            source: session.CurrentMapInstance.Sessions.Where(predicate: s => s.Character != null),
                            body: s =>
                            {
                                if (ChannelId != 51 || session.Character.Faction == s.Character.Faction)
                                {
                                    s.SendPacket(packet: session.Character.GenerateIn());
                                    s.SendPacket(packet: session.Character.GenerateGidx());
                                    session.Character.Mates.Where(predicate: m => m.IsTeamMember || m.IsTemporalMate)
                                        .ToList()
                                        .ForEach(action: m =>
                                            s.SendPacket(packet: m.GenerateIn(hideNickname: false,
                                                isAct4: ChannelId == 51)));
                                }
                                else
                                {
                                    s.SendPacket(packet: session.Character.GenerateIn(foe: true,
                                        receiverAuthority: s.Account.Authority));
                                    session.Character.Mates.Where(predicate: m => m.IsTeamMember || m.IsTemporalMate)
                                        .ToList()
                                        .ForEach(
                                            action: m => s.SendPacket(packet: m.GenerateIn(hideNickname: true,
                                                isAct4: ChannelId == 51, receiverAuthority: s.Account.Authority)));
                                }

                                if (session.Character.GetBuff(type: BCardType.CardType.SpecialEffects,
                                            subtype: (byte)AdditionalTypes.SpecialEffects.ShadowAppears) is int[]
                                        EffectData &&
                                    EffectData[0] != 0 && EffectData[1] != 0)
                                    s.CurrentMapInstance.Broadcast(
                                        packet:
                                        $"guri 0 {(short)UserType.Player} {session.Character.CharacterId} {EffectData[0]} {EffectData[1]}");

                                session.Character.Mates.Where(predicate: m => m.IsTeamMember || m.IsTemporalMate)
                                    .ToList()
                                    .ForEach(action: m =>
                                    {
                                        if (session.Character.IsVehicled)
                                        {
                                            m.PositionX = session.Character.PositionX;
                                            m.PositionY = session.Character.PositionY;
                                        }

                                        if (m.GetBuff(type: BCardType.CardType.SpecialEffects,
                                                    subtype: (byte)AdditionalTypes.SpecialEffects.ShadowAppears) is int
                                                []
                                                MateEffectData && MateEffectData[0] != 0 &&
                                            MateEffectData[1] != 0)
                                            s.CurrentMapInstance.Broadcast(
                                                packet:
                                                $"guri 0 {(short)UserType.Monster} {m.MateTransportId} {MateEffectData[0]} {MateEffectData[1]}");
                                    });
                            });

                    session.SendPacket(packet: session.Character.GeneratePinit());

                    if (session.Character.Mates.FirstOrDefault(predicate: s =>
                            (s.IsTeamMember || s.IsTemporalMate) && s.MateType == MateType.Partner &&
                            s.IsUsingSp) is Mate
                        partner) session.SendPacket(packet: partner.Sp.GeneratePski());

                    session.Character.Mates.ForEach(action: s => session.SendPacket(packet: s.GenerateScPacket()));
                    session.SendPackets(packets: session.Character.GeneratePst());

                    if (session.Character.Size != 10) session.SendPacket(packet: session.Character.GenerateScal());

                    if (session.CurrentMapInstance?.IsDancing == true && !session.Character.IsDancing)
                    {
                        session.CurrentMapInstance?.Broadcast(packet: "dance 2");
                    }
                    else if (session.CurrentMapInstance?.IsDancing == false && session.Character.IsDancing)
                    {
                        session.Character.IsDancing = false;
                        session.CurrentMapInstance?.Broadcast(packet: "dance");
                    }

                    if (Groups != null)
                        Parallel.ForEach(source: Groups, body: group =>
                        {
                            foreach (var groupSession in group.Sessions.GetAllItems())
                            {
                                var groupCharacterSession = Sessions.FirstOrDefault(predicate: s =>
                                    s.Character != null &&
                                    s.Character.CharacterId == groupSession.Character.CharacterId &&
                                    s.CurrentMapInstance == groupSession.CurrentMapInstance);
                                if (groupCharacterSession == null) continue;

                                groupSession.SendPacket(packet: groupSession.Character.GeneratePinit());
                                groupSession.SendPackets(packets: groupSession.Character.GeneratePst());
                            }
                        });

                    if (session.Character.Group?.GroupType == GroupType.Group)
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GeneratePidx(),
                            receiver: ReceiverType.AllExceptMe);

                    if (session.CurrentMapInstance?.Map.MapTypes.All(predicate: s =>
                            s.MapTypeId != (short)MapTypeEnum.Act52) ==
                        true && session.Character.Buff.Any(predicate: s => s.Card.CardId == 339)) //Act5.2 debuff
                    {
                        session.Character.RemoveBuff(cardId: 339);
                    }
                    else if (
                        session.CurrentMapInstance?.Map.MapTypes.Any(predicate: s =>
                            s.MapTypeId == (short)MapTypeEnum.Act52) ==
                        true &&
                        session.Character.Buff.All(predicate: s => s.Card.CardId != 339 && s.Card.CardId != 340))
                    {
                        session.Character.AddStaticBuff(staticBuff: new StaticBuffDto
                        {
                            CardId = 339,
                            CharacterId = session.Character.CharacterId,
                            RemainingTime = -1
                        });
                        session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "ENCASED_BURNING_SWORD")));
                    }

                    session.SendPacket(packet: session.Character.GenerateMinimapPosition());
                    session.CurrentMapInstance.OnCharacterDiscoveringMapEvents.ForEach(action: e =>
                    {
                        if (!e.Item2.Contains(item: session.Character.CharacterId))
                        {
                            e.Item2.Add(item: session.Character.CharacterId);
                            EventHelper.Instance.RunEvent(evt: e.Item1, session: session);
                        }
                    });
                    session.CurrentMapInstance.OnCharacterDiscoveringMapEvents = session.CurrentMapInstance
                        .OnCharacterDiscoveringMapEvents
                        .Where(predicate: s => s.Item1.EventActionType == EventActionType.Sendpacket).ToList();
                    session.Character.LeaveIceBreaker();

                    session.Character.IsChangingMapInstance = false;
                }
                catch (Exception ex)
                {
                    Logger.Warn(data: "Character changed while changing map. Do not abuse Commands.",
                        innerException: ex);
                    session.Character.IsChangingMapInstance = false;
                }
            }
        }

        public void FamilyRefresh(long familyId, bool changeFaction = false)
        {
            CommunicationServiceClient.Instance.UpdateFamily(worldGroup: ServerGroup, familyId: familyId,
                changeFaction: changeFaction);
        }

        public static MapInstance GenerateMapInstance(short mapId, MapInstanceType type, InstanceBag mapclock,
            bool dropAllowed = false, bool isScriptedInstance = false)
        {
            var map = _maps.FirstOrDefault(predicate: m => m.MapId.Equals(obj: mapId));
            if (map != null)
            {
                var guid = Guid.NewGuid();
                var mapInstance = new MapInstance(map: map, guid: guid, shopAllowed: false, type: type,
                    instanceBag: mapclock, dropAllowed: dropAllowed);
                if (!isScriptedInstance)
                {
                    mapInstance.LoadMonsters();
                    mapInstance.LoadNpcs();
                    mapInstance.LoadPortals();
                    Parallel.ForEach(source: mapInstance.Monsters, body: mapMonster =>
                    {
                        mapMonster.MapInstance = mapInstance;
                        mapInstance.AddMonster(monster: mapMonster);
                    });
                    Parallel.ForEach(source: mapInstance.Npcs, body: mapNpc =>
                    {
                        mapNpc.MapInstance = mapInstance;
                        mapInstance.AddNpc(npc: mapNpc);
                    });
                }

                _mapinstances.TryAdd(key: guid, value: mapInstance);
                return mapInstance;
            }

            return null;
        }

        public static MapInstance ResetMapInstance(MapInstance baseMapInstance)
        {
            if (baseMapInstance != null)
            {
                var mapinfo = new Map(mapId: baseMapInstance.Map.MapId, gridMapId: baseMapInstance.Map.GridMapId,
                    data: baseMapInstance.Map.Data)
                {
                    Music = baseMapInstance.Map.Music,
                    Name = baseMapInstance.Map.Name,
                    ShopAllowed = baseMapInstance.Map.ShopAllowed,
                    XpRate = baseMapInstance.Map.XpRate
                };
                var mapInstance = new MapInstance(map: mapinfo, guid: baseMapInstance.MapInstanceId,
                    shopAllowed: baseMapInstance.ShopAllowed,
                    type: baseMapInstance.MapInstanceType, instanceBag: new InstanceBag(),
                    dropAllowed: baseMapInstance.DropAllowed);
                mapInstance.LoadMonsters();
                mapInstance.LoadNpcs();
                mapInstance.LoadPortals();
                foreach (var si in DaoFactory.ScriptedInstanceDao.LoadByMap(mapId: mapInstance.Map.MapId).ToList())
                {
                    var siObj = new ScriptedInstance(input: si);
                    if (siObj.Type == ScriptedInstanceType.TimeSpace)
                    {
                        mapInstance.ScriptedInstances.Add(item: siObj);
                    }
                    else if (siObj.Type == ScriptedInstanceType.Raid)
                    {
                        var port = new Portal
                        {
                            Type = (byte)PortalType.Raid,
                            SourceMapId = siObj.MapId,
                            SourceX = siObj.PositionX,
                            SourceY = siObj.PositionY
                        };
                        mapInstance.Portals.Add(item: port);
                    }
                }

                Parallel.ForEach(source: mapInstance.Monsters, body: mapMonster =>
                {
                    mapMonster.MapInstance = mapInstance;
                    mapInstance.AddMonster(monster: mapMonster);
                });
                Parallel.ForEach(source: mapInstance.Npcs, body: mapNpc =>
                {
                    mapNpc.MapInstance = mapInstance;
                    mapInstance.AddNpc(npc: mapNpc);
                });
                RemoveMapInstance(mapId: baseMapInstance.MapInstanceId);
                _mapinstances.TryAdd(key: baseMapInstance.MapInstanceId, value: mapInstance);
                return mapInstance;
            }

            return null;
        }

        public static IEnumerable<Card> GetAllCard()
        {
            return _cards;
        }

        public Card GetCardByCardId(short cardId)
        {
            return _cards.FirstOrDefault(predicate: s => s.CardId == cardId);
        }

        public static List<MapInstance> GetAllMapInstances()
        {
            return _mapinstances.Values.ToList();
        }

        public List<Recipe> GetAllRecipes()
        {
            return _recipes.GetAllItems();
        }

        public static IEnumerable<Skill> GetAllSkill()
        {
            return _skills;
        }

        public static Guid GetBaseMapInstanceIdByMapId(short mapId)
        {
            return _mapinstances.FirstOrDefault(predicate: s =>
                s.Value?.Map.MapId == mapId && s.Value.MapInstanceType == MapInstanceType.BaseMapInstance).Key;
        }

        public static Card GetCard(short? cardId)
        {
            return _cards.FirstOrDefault(predicate: m => m.CardId.Equals(obj: cardId));
        }

        public List<DropDto> GetDropsByMonsterVNum(short monsterVNum)
        {
            return _monsterDrops.ContainsKey(key: monsterVNum)
                ? _generalDrops.Concat(second: _monsterDrops[key: monsterVNum]).ToList()
                : _generalDrops.ToList();
        }

        public Group GetGroupByCharacterId(long characterId)
        {
            return Groups?.SingleOrDefault(predicate: g => g.IsMemberOfGroup(entityId: characterId));
        }

        public static Item GetItem(short vnum)
        {
            return _items.FirstOrDefault(predicate: m => m.VNum.Equals(obj: vnum));
        }

        public static MapInstance GetMapInstance(Guid id)
        {
            return _mapinstances.ContainsKey(key: id) ? _mapinstances[key: id] : null;
        }

        public static MapInstance GetMapInstanceByMapId(short mapId)
        {
            return _mapinstances.Values.FirstOrDefault(predicate: s => s.Map.MapId == mapId);
        }

        public static List<MapInstance> GetMapInstances(Func<MapInstance, bool> predicate)
        {
            return _mapinstances.Values.Where(predicate: predicate).ToList();
        }

        public long GetNextGroupId()
        {
            return ++_lastGroupId;
        }

        public int GetNextMobId()
        {
            var maxMobId = 0;
            foreach (var map in _mapinstances.Values.ToList())
                if (map.Monsters.Count > 0 && maxMobId < map.Monsters.Max(selector: m => m.MapMonsterId))
                    maxMobId = map.Monsters.Max(selector: m => m.MapMonsterId);
            return ++maxMobId;
        }

        public int GetNextNpcId()
        {
            var mapNpcId = 0;
            foreach (var map in _mapinstances.Values.ToList())
                if (map.Npcs.Count > 0 && mapNpcId < map.Npcs.Max(selector: m => m.MapNpcId))
                    mapNpcId = map.Npcs.Max(selector: m => m.MapNpcId);
            return ++mapNpcId;
        }

        public static NpcMonster GetNpcMonster(short npcVNum)
        {
            return _npcmonsters.FirstOrDefault(predicate: m => m.NpcMonsterVNum.Equals(obj: npcVNum));
        }

        public List<Recipe> GetRecipesByItemVNum(short itemVNum)
        {
            var recipes = new List<Recipe>();
            foreach (var recipeList in _recipeLists.Where(predicate: r => r.ItemVNum == itemVNum))
                recipes.Add(item: _recipes[key: recipeList.RecipeId]);
            return recipes;
        }

        public List<Recipe> GetRecipesByMapNpcId(int mapNpcId)
        {
            var recipes = new List<Recipe>();
            foreach (var recipeList in _recipeLists.Where(predicate: r => r.MapNpcId == mapNpcId))
                recipes.Add(item: _recipes[key: recipeList.RecipeId]);
            return recipes;
        }

        public ClientSession GetSessionByCharacterName(string name)
        {
            return Sessions.SingleOrDefault(predicate: s => s.Character.Name == name);
        }

        public ClientSession GetSessionBySessionId(int sessionId)
        {
            return Sessions.SingleOrDefault(predicate: s => s.SessionId == sessionId);
        }

        public static Skill GetSkill(short skillVNum)
        {
            return _skills.FirstOrDefault(predicate: m => m.SkillVNum.Equals(obj: skillVNum));
        }

        public Quest GetQuest(long questId)
        {
            return Quests.FirstOrDefault(predicate: m => m.QuestId.Equals(obj: questId));
        }

        public void GroupLeave(ClientSession session)
        {
            if (Groups != null)
            {
                var grp = Instance.Groups.Find(match: s => s.IsMemberOfGroup(entityId: session.Character.CharacterId));
                if (grp != null)
                {
                    switch (grp.GroupType)
                    {
                        case GroupType.BigTeam:
                        case GroupType.GiantTeam:
                        case GroupType.Team:
                            if (grp.Raid?.InstanceBag.Lock == true)
                                grp.Raid.InstanceBag.DeadList.Add(item: session.Character.CharacterId);
                            if (grp.Sessions.ElementAt(v: 0) == session && grp.SessionCount > 1)
                                Broadcast(client: session,
                                    content: UserInterfaceHelper.GenerateInfo(
                                        message: Language.Instance.GetMessageFromKey(key: "NEW_LEADER")),
                                    receiver: ReceiverType.OnlySomeone, characterName: "",
                                    characterId: grp.Sessions.ElementAt(v: 1)?.Character.CharacterId ?? 0);
                            grp.LeaveGroup(session: session);
                            session.SendPacket(packet: session.Character.GenerateRaid(Type: 1, exit: true));
                            session.SendPacket(packet: session.Character.GenerateRaid(Type: 2, exit: true));
                            foreach (var groupSession in grp.Sessions.GetAllItems())
                            {
                                groupSession.SendPacket(packet: grp.GenerateRdlst());
                                groupSession.SendPacket(packet: grp.GeneraterRaidmbf(session: groupSession));
                                groupSession.SendPacket(packet: groupSession.Character.GenerateRaid(Type: 0));
                            }

                            if (session.CurrentMapInstance?.MapInstanceType == MapInstanceType.RaidInstance)
                                ChangeMap(id: session.Character.CharacterId, mapId: session.Character.MapId,
                                    mapX: session.Character.MapX, mapY: session.Character.MapY);
                            session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "LEFT_RAID"), type: 0));
                            break;

                        /*case GroupType.GiantTeam:
                            ClientSession[] grpmembers = new ClientSession[40];
                            grp.Sessions.CopyTo(grpmembers);
                            foreach (ClientSession targetSession in grpmembers)
                            {
                                if (targetSession != null)
                                {
                                    targetSession.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("GROUP_CLOSED"), 0));
                                    Broadcast(targetSession.Character.GeneratePidx(true));
                                    grp.LeaveGroup(targetSession);
                                    targetSession.SendPacket(targetSession.Character.GeneratePinit());
                                    targetSession.SendPackets(targetSession.Character.GeneratePst());
                                }
                            }
                            GroupList.RemoveAll(s => s.GroupId == grp.GroupId);
                            ThreadSafeGroupList.Remove(grp.GroupId);
                            break;*/ //Nizar GiantTeam

                        case GroupType.Group:
                            if (grp.Sessions.ElementAt(v: 0) == session && grp.SessionCount > 1)
                                Broadcast(client: session,
                                    content: UserInterfaceHelper.GenerateInfo(
                                        message: Language.Instance.GetMessageFromKey(key: "NEW_LEADER")),
                                    receiver: ReceiverType.OnlySomeone, characterName: "",
                                    characterId: grp.Sessions.ElementAt(v: 1).Character.CharacterId);
                            grp.LeaveGroup(session: session);
                            if (grp.SessionCount == 1)
                            {
                                var targetSession = grp.Sessions.ElementAt(v: 0);
                                if (targetSession != null)
                                {
                                    targetSession.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "GROUP_CLOSED"),
                                            type: 0));
                                    Broadcast(packet: targetSession.Character.GeneratePidx(isLeaveGroup: true));
                                    grp.LeaveGroup(session: targetSession);
                                    targetSession.SendPacket(packet: targetSession.Character.GeneratePinit());
                                    targetSession.SendPackets(packets: targetSession.Character.GeneratePst());
                                }
                            }
                            else
                            {
                                foreach (var groupSession in grp.Sessions.GetAllItems())
                                {
                                    groupSession.SendPacket(packet: groupSession.Character.GeneratePinit());
                                    groupSession.SendPackets(packets: session.Character.GeneratePst());
                                    groupSession.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "LEAVE_GROUP"),
                                            arg0: session.Character.Name), type: 0));
                                }
                            }

                            session.SendPacket(packet: session.Character.GeneratePinit());
                            session.SendPackets(packets: session.Character.GeneratePst());
                            Broadcast(packet: session.Character.GeneratePidx(isLeaveGroup: true));
                            session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "GROUP_LEFT"), type: 0));
                            break;

                        default:
                            return;
                    }

                    session.Character.Group = null;
                }
            }
        }

        public bool IsAct4Online()
        {
            return CommunicationServiceClient.Instance.IsAct4Online(worldGroup: ServerGroup);
        }

        public void Initialize()
        {
            Act4RaidStart = DateTime.Now;
            Act4AngelStat = new Act4Stat();
            Act4DemonStat = new Act4Stat();
            LastFCSent = DateTime.Now;
            LoadBossEntities();

            CharacterScreenSessions = new ThreadSafeSortedList<long, ClientSession>();

            // Load Configuration

            Schedules = ConfigurationManager.GetSection(sectionName: "eventScheduler") as List<Schedule>;

            var itemPartitioner =
                Partitioner.Create(source: DaoFactory.ItemDao.LoadAll(),
                    partitionerOptions: EnumerablePartitionerOptions.NoBuffering);
            Parallel.ForEach(source: itemPartitioner, parallelOptions: new ParallelOptions { MaxDegreeOfParallelism = 4 },
                body: itemDto =>
                {
                    switch (itemDto.ItemType)
                    {
                        case ItemType.Armor:
                        case ItemType.Jewelery:
                        case ItemType.Fashion:
                        case ItemType.Specialist:
                        case ItemType.Weapon:
                            _items.Add(item: new WearableItem(item: itemDto));
                            break;

                        case ItemType.Box:
                            _items.Add(item: new BoxItem(item: itemDto));
                            break;

                        case ItemType.Shell:
                        case ItemType.Magical:
                        case ItemType.Event:
                            _items.Add(item: new MagicalItem(item: itemDto));
                            break;

                        case ItemType.Food:
                            _items.Add(item: new FoodItem(item: itemDto));
                            break;

                        case ItemType.Potion:
                            _items.Add(item: new PotionItem(item: itemDto));
                            break;

                        case ItemType.Production:
                            _items.Add(item: new ProduceItem(item: itemDto));
                            break;

                        case ItemType.Snack:
                            _items.Add(item: new SnackItem(item: itemDto));
                            break;

                        case ItemType.Special:
                            _items.Add(item: new SpecialItem(item: itemDto));
                            break;

                        case ItemType.Teacher:
                            _items.Add(item: new TeacherItem(item: itemDto));
                            break;

                        case ItemType.Upgrade:
                            _items.Add(item: new UpgradeItem(item: itemDto));
                            break;

                        default:
                            _items.Add(item: new NoFunctionItem(item: itemDto));
                            break;
                    }
                });
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ITEMS_LOADED"),
                arg0: _items.Count));

            // intialize monsterdrops
            _monsterDrops = new ThreadSafeSortedList<short, List<DropDto>>();
            Parallel.ForEach(source: DaoFactory.DropDao.LoadAll().GroupBy(keySelector: d => d.MonsterVNum),
                body: monsterDropGrouping =>
                {
                    if (monsterDropGrouping.Key.HasValue)
                        _monsterDrops[key: monsterDropGrouping.Key.Value] =
                            monsterDropGrouping.OrderBy(keySelector: d => d.DropChance).ToList();
                    else
                        _generalDrops = monsterDropGrouping.ToList();
                });
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "DROPS_LOADED"),
                arg0: _monsterDrops.Sum(selector: i => i.Count)));

            // initialize monsterskills
            _monsterSkills = new ThreadSafeSortedList<short, List<NpcMonsterSkill>>();
            Parallel.ForEach(
                source: DaoFactory.NpcMonsterSkillDao.LoadAll().GroupBy(keySelector: n => n.NpcMonsterVNum),
                body: monsterSkillGrouping => _monsterSkills[key: monsterSkillGrouping.Key] =
                    monsterSkillGrouping.Select(selector: n => new NpcMonsterSkill(input: n)).ToList());
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MONSTERSKILLS_LOADED"),
                arg0: _monsterSkills.Sum(selector: i => i.Count)));

            // initialize bazaar
            BazaarList = new ThreadSafeGenericList<BazaarItemLink>();
            var bazaarPartitioner = Partitioner.Create(source: DaoFactory.BazaarItemDao.LoadAll(),
                partitionerOptions: EnumerablePartitionerOptions.NoBuffering);
            Parallel.ForEach(source: bazaarPartitioner,
                parallelOptions: new ParallelOptions { MaxDegreeOfParallelism = 8 }, body: bazaarItem =>
                  {
                      var item = new BazaarItemLink
                      {
                          BazaarItem = bazaarItem
                      };
                      var chara = DaoFactory.CharacterDao.LoadById(characterId: bazaarItem.SellerId);
                      if (chara != null)
                      {
                          item.Owner = chara.Name;
                          item.Item = new ItemInstance(
                              input: DaoFactory.ItemInstanceDao.LoadById(id: bazaarItem.ItemInstanceId));
                      }

                      BazaarList.Add(value: item);
                  });
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "BAZAAR_LOADED"),
                arg0: BazaarList.Count));

            // initialize npcmonsters
            Parallel.ForEach(source: DaoFactory.NpcMonsterDao.LoadAll(), body: npcMonster =>
            {
                var npcMonsterObj = new NpcMonster(input: npcMonster);
                npcMonsterObj.Initialize();
                npcMonsterObj.BCards = new List<BCard>();
                DaoFactory.BCardDao
                    .LoadByNpcMonsterVNum(vNum: npcMonster.OriginalNpcMonsterVNum > 0
                        ? npcMonster.OriginalNpcMonsterVNum
                        : npcMonster.NpcMonsterVNum).ToList()
                    .ForEach(action: s => npcMonsterObj.BCards.Add(item: new BCard(input: s)));
                _npcmonsters.Add(item: npcMonsterObj);
            });
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "NPCMONSTERS_LOADED"),
                arg0: _npcmonsters.Count));

            // intialize recipes
            _recipes = new ThreadSafeSortedList<short, Recipe>();
            Parallel.ForEach(source: DaoFactory.RecipeDao.LoadAll(), body: recipeGrouping =>
            {
                var recipe = new Recipe(input: recipeGrouping);
                _recipes[key: recipeGrouping.RecipeId] = recipe;
                recipe.Initialize();
            });
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "RECIPES_LOADED"),
                arg0: _recipes.Count));

            // initialize recipelist
            _recipeLists = new ThreadSafeSortedList<int, RecipeListDto>();
            Parallel.ForEach(source: DaoFactory.RecipeListDao.LoadAll(),
                body: recipeListGrouping => _recipeLists[key: recipeListGrouping.RecipeListId] = recipeListGrouping);
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "RECIPELISTS_LOADED"),
                arg0: _recipeLists.Count));

            // initialize shopitems
            _shopItems = new ThreadSafeSortedList<int, List<ShopItemDto>>();
            Parallel.ForEach(source: DaoFactory.ShopItemDao.LoadAll().GroupBy(keySelector: s => s.ShopId),
                body: shopItemGrouping => _shopItems[key: shopItemGrouping.Key] = shopItemGrouping.ToList());
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHOPITEMS_LOADED"),
                arg0: _shopItems.Sum(selector: i => i.Count)));

            // initialize shopskills
            _shopSkills = new ThreadSafeSortedList<int, List<ShopSkillDto>>();
            Parallel.ForEach(source: DaoFactory.ShopSkillDao.LoadAll().GroupBy(keySelector: s => s.ShopId),
                body: shopSkillGrouping => _shopSkills[key: shopSkillGrouping.Key] = shopSkillGrouping.ToList());
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHOPSKILLS_LOADED"),
                arg0: _shopSkills.Sum(selector: i => i.Count)));

            // initialize shops
            _shops = new ThreadSafeSortedList<int, Shop>();
            Parallel.ForEach(source: DaoFactory.ShopDao.LoadAll(), body: shopGrouping =>
            {
                var shop = new Shop(input: shopGrouping);
                _shops[key: shopGrouping.MapNpcId] = shop;
                shop.Initialize();
            });
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHOPS_LOADED"),
                arg0: _shops.Count));

            // initialize teleporters
            _teleporters = new ThreadSafeSortedList<int, List<TeleporterDto>>();
            Parallel.ForEach(source: DaoFactory.TeleporterDao.LoadAll().GroupBy(keySelector: t => t.MapNpcId),
                body: teleporterGrouping =>
                    _teleporters[key: teleporterGrouping.Key] = teleporterGrouping.Select(selector: t => t).ToList());
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "TELEPORTERS_LOADED"),
                arg0: _teleporters.Sum(selector: i => i.Count)));

            // initialize skills
            Parallel.ForEach(source: DaoFactory.SkillDao.LoadAll(), body: skill =>
            {
                var skillObj = new Skill(input: skill);
                skillObj.Combos.AddRange(collection: DaoFactory.ComboDao.LoadBySkillVnum(skillVNum: skillObj.SkillVNum)
                    .ToList());
                skillObj.BCards = new List<BCard>();
                DaoFactory.BCardDao.LoadBySkillVNum(vNum: skillObj.SkillVNum).ToList()
                    .ForEach(action: o => skillObj.BCards.Add(item: new BCard(input: o)));
                _skills.Add(item: skillObj);
            });
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SKILLS_LOADED"),
                arg0: _skills.Count));

            // initialize cards
            Parallel.ForEach(source: DaoFactory.CardDao.LoadAll(), body: card =>
            {
                var cardObj = new Card(input: card)
                {
                    BCards = new List<BCard>()
                };
                DaoFactory.BCardDao.LoadByCardId(cardId: cardObj.CardId).ToList()
                    .ForEach(action: o => cardObj.BCards.Add(item: new BCard(input: o)));
                _cards.Add(item: cardObj);
            });
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "CARDS_LOADED"),
                arg0: _cards.Count));

            // initialize quests
            Quests = new List<Quest>();
            foreach (var questdto in DaoFactory.QuestDao.LoadAll())
            {
                var quest = new Quest(input: questdto);
                quest.QuestRewards = DaoFactory.QuestRewardDao.LoadByQuestId(questId: quest.QuestId).ToList();
                quest.QuestObjectives = DaoFactory.QuestObjectiveDao.LoadByQuestId(questId: quest.QuestId).ToList();
                Quests.Add(item: quest);
            }

            FlowerQuestId = Quests.FirstOrDefault(predicate: q => q.QuestType == (byte)QuestType.FlowerQuest)?.QuestId;

            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "QUESTS_LOADED"),
                arg0: Quests.Count));

            // intialize mapnpcs
            _mapNpcs = new ThreadSafeSortedList<short, List<MapNpc>>();
            Parallel.ForEach(source: DaoFactory.MapNpcDao.LoadAll().GroupBy(keySelector: t => t.MapId),
                body: mapNpcGrouping => _mapNpcs[key: mapNpcGrouping.Key] =
                    mapNpcGrouping.Select(selector: t => t as MapNpc).ToList());
            Logger.Info(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MAPNPCS_LOADED"),
                    arg0: _mapNpcs.Sum(selector: i => i.Count)));

            try
            {
                var i = 0;
                var monstercount = 0;
                var mapPartitioner =
                    Partitioner.Create(source: DaoFactory.MapDao.LoadAll(),
                        partitionerOptions: EnumerablePartitionerOptions.NoBuffering);
                Parallel.ForEach(source: mapPartitioner,
                    parallelOptions: new ParallelOptions { MaxDegreeOfParallelism = 8 }, body: map =>
                      {
                          var guid = Guid.NewGuid();
                          var mapinfo = new Map(mapId: map.MapId, gridMapId: map.GridMapId, data: map.Data)
                          {
                              Music = map.Music,
                              Name = map.Name,
                              ShopAllowed = map.ShopAllowed,
                              XpRate = map.XpRate
                          };
                          _maps.Add(item: mapinfo);
                          var newMap = new MapInstance(map: mapinfo, guid: guid, shopAllowed: map.ShopAllowed,
                              type: MapInstanceType.BaseMapInstance,
                              instanceBag: new InstanceBag(), dropAllowed: true);
                          _mapinstances.TryAdd(key: guid, value: newMap);

                          Task.Run(action: newMap.LoadPortals);
                          newMap.LoadNpcs();
                          newMap.LoadMonsters();

                          Parallel.ForEach(source: newMap.Npcs, body: mapNpc =>
                          {
                              mapNpc.MapInstance = newMap;
                              newMap.AddNpc(npc: mapNpc);
                          });
                          Parallel.ForEach(source: newMap.Monsters, body: mapMonster =>
                          {
                              mapMonster.MapInstance = newMap;
                              newMap.AddMonster(monster: mapMonster);
                          });
                          monstercount += newMap.Monsters.Count;
                          i++;
                      });
                if (i != 0)
                    Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MAPS_LOADED"),
                        arg0: i));
                else
                    Logger.Error(data: Language.Instance.GetMessageFromKey(key: "NO_MAP"));

                Logger.Info(message: string.Format(
                    format: Language.Instance.GetMessageFromKey(key: "MAPMONSTERS_LOADED"), arg0: monstercount));
                StartedEvents = new List<EventType>();

                LoadFamilies();
                LaunchEvents();
                RefreshRanking();

                CharacterRelations = DaoFactory.CharacterRelationDao.LoadAll().ToList();
                PenaltyLogs = DaoFactory.PenaltyLogDao.LoadAll().ToList();

                #region Normal Arena

                if (DaoFactory.MapDao.LoadById(mapId: 2006) != null)
                {
                    ArenaInstance = GenerateMapInstance(mapId: 2006, type: MapInstanceType.NormalInstance,
                        mapclock: new InstanceBag());
                    ArenaInstance.IsPvp = true;

                    var portal = new Portal
                    {
                        SourceMapId = 2006,
                        SourceX = 37,
                        SourceY = 15,
                        DestinationMapId = 1,
                        DestinationX = 0,
                        DestinationY = 0,
                        Type = -1
                    };

                    ArenaInstance.CreatePortal(portal: portal);
                }

                #endregion

                #region Family Arena

                if (DaoFactory.MapDao.LoadById(mapId: 2106) != null)
                {
                    FamilyArenaInstance = GenerateMapInstance(mapId: 2106, type: MapInstanceType.NormalInstance,
                        mapclock: new InstanceBag());
                    FamilyArenaInstance.IsPvp = true;

                    var portal = new Portal
                    {
                        SourceMapId = 2106,
                        SourceX = 38,
                        SourceY = 3,
                        DestinationMapId = 1,
                        DestinationX = 0,
                        DestinationY = 0,
                        Type = -1
                    };

                    FamilyArenaInstance.CreatePortal(portal: portal);
                }

                #endregion

                #region Specialist Gem Map

                if (DaoFactory.MapDao.LoadById(mapId: 2107) != null)
                {
                    var portal = new Portal
                    {
                        SourceMapId = 2107,
                        SourceX = 10,
                        SourceY = 5,
                        DestinationMapId = 1,
                        DestinationX = 0,
                        DestinationY = 0,
                        Type = -1
                    };

                    void loadSpecialistGemMap(short npcVNum)
                    {
                        MapInstance specialistGemMapInstance;
                        specialistGemMapInstance =
                            GenerateMapInstance(mapId: 2107, type: MapInstanceType.NormalInstance,
                                mapclock: new InstanceBag());
                        specialistGemMapInstance.Npcs.Where(predicate: s => s.NpcVNum != npcVNum).ToList()
                            .ForEach(action: s => specialistGemMapInstance.RemoveNpc(npcToRemove: s));
                        specialistGemMapInstance.CreatePortal(portal: portal);
                        SpecialistGemMapInstances.Add(item: specialistGemMapInstance);
                    }

                    loadSpecialistGemMap(npcVNum: 932); // Pajama
                    loadSpecialistGemMap(npcVNum: 933); // SP 1
                    loadSpecialistGemMap(npcVNum: 934); // SP 2
                    loadSpecialistGemMap(npcVNum: 948); // SP 3
                    loadSpecialistGemMap(npcVNum: 954); // SP 4
                }

                #endregion

                LoadScriptedInstances();
                LoadBannedCharacters();
            }
            catch (Exception ex)
            {
                Logger.Error(data: "General Error", ex: ex);
            }

            WorldId = Guid.NewGuid();
        }

        public bool IsCharacterMemberOfGroup(long characterId)
        {
            return Groups?.Any(predicate: g => g.IsMemberOfGroup(entityId: characterId)) == true;
        }

        public bool IsCharactersGroupFull(long characterId)
        {
            return Groups?.Any(predicate: g =>
                g.IsMemberOfGroup(entityId: characterId) &&
                (g.SessionCount == (byte)g.GroupType || g.GroupType == GroupType.TalentArena)) == true;
        }

        public bool ItemHasRecipe(short itemVNum)
        {
            return _recipeLists.Any(predicate: r => r.ItemVNum == itemVNum);
        }

        public void JoinMiniland(ClientSession session, ClientSession minilandOwner)
        {
            if (session.Character.Miniland.MapInstanceId == minilandOwner.Character.Miniland.MapInstanceId)
                foreach (var mate in session.Character.Mates)
                {
                    if (session.Character.Miniland.Map.IsBlockedZone(x: mate.PositionX, y: mate.PositionY))
                    {
                        var newPos = MinilandRandomPos();
                        mate.MapX = newPos.X;
                        mate.MapY = newPos.Y;
                        mate.PositionX = mate.MapX;
                        mate.PositionY = mate.MapY;
                    }

                    if (!mate.IsAlive || mate.Hp <= 0)
                    {
                        mate.Hp = mate.MaxHp / 2;
                        mate.Mp = mate.MaxMp / 2;
                        mate.IsAlive = true;
                        mate.ReviveDisposable?.Dispose();
                    }
                }

            ChangeMapInstance(characterId: session.Character.CharacterId,
                mapInstanceId: minilandOwner.Character.Miniland.MapInstanceId, mapX: 5, mapY: 8);
            if (session.Character.Miniland.MapInstanceId != minilandOwner.Character.Miniland.MapInstanceId)
            {
                session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(message: minilandOwner.Character.MinilandMessage, type: 0));
                session.SendPacket(packet: minilandOwner.Character.GenerateMlinfobr());
                minilandOwner.Character.GeneralLogs.Add(value: new GeneralLogDto
                {
                    AccountId = session.Account.AccountId,
                    CharacterId = session.Character.CharacterId,
                    IpAddress = session.IpAddress,
                    LogData = "Miniland",
                    LogType = "World",
                    Timestamp = DateTime.Now
                });
                session.SendPacket(packet: minilandOwner.Character.GenerateMinilandObjectForFriends());
            }
            else
            {
                session.SendPacket(packet: session.Character.GenerateMlinfo());
                session.SendPacket(packet: minilandOwner.Character.GetMinilandObjectList());
            }

            minilandOwner.Character.Mates.Where(predicate: s => !s.IsTeamMember).ToList()
                .ForEach(action: s => session.SendPacket(packet: s.GenerateIn()));
            session.SendPackets(packets: minilandOwner.Character.GetMinilandEffects());
            session.SendPacket(packet: session.Character.GenerateSay(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MINILAND_VISITOR"),
                    arg0: session.Character.GeneralLogs.CountLinq(predicate: s =>
                        s.LogData == "Miniland" && s.Timestamp.Day == DateTime.Now.Day),
                    arg1: session.Character.GeneralLogs.CountLinq(predicate: s => s.LogData == "Miniland")), type: 10));
        }

        // Server
        public void Kick(string characterName)
        {
            var session =
                Sessions.FirstOrDefault(predicate: s => s.Character?.Name.Equals(value: characterName) == true);
            session?.Disconnect();
        }

        // Map
        public void LeaveMap(long id)
        {
            var session = GetSessionByCharacterId(characterId: id);
            if (session == null) return;
            session.SendPacket(packet: UserInterfaceHelper.GenerateMapOut());
            if (!session.Character.InvisibleGm)
            {
                session.Character.Mates.Where(predicate: s => s.IsTeamMember).ToList().ForEach(action: s =>
                    session.CurrentMapInstance?.Broadcast(client: session,
                        content: StaticPacketHelper.Out(type: UserType.Npc, callerId: s.MateTransportId),
                        receiver: ReceiverType.AllExceptMe));
                session.CurrentMapInstance?.Broadcast(client: session,
                    content: StaticPacketHelper.Out(type: UserType.Player, callerId: session.Character.CharacterId),
                    receiver: ReceiverType.AllExceptMe);
            }
        }

        public bool MapNpcHasRecipe(int mapNpcId)
        {
            return _recipeLists.Any(predicate: r => r.MapNpcId == mapNpcId);
        }

        //Function to get a random number 
        static readonly Random random = new Random();
        static readonly object syncLock = new object();

        public static int RandomNumber(int min = 0, int max = 100)
        {
            lock (syncLock)
            {
                // synchronize
                return random.Next(minValue: min, maxValue: max);
            }
        }

        public static T RandomNumber<T>(int min = 0, int max = 100)
        {
            return (T)Convert.ChangeType(value: RandomNumber(min: min, max: max), conversionType: typeof(T));
        }

        public static MapCell MinilandRandomPos()
        {
            return new MapCell { X = (short)RandomNumber(min: 5, max: 16), Y = (short)RandomNumber(min: 3, max: 14) };
        }

        public void RefreshRanking()
        {
            TopComplimented = DaoFactory.CharacterDao.GetTopCompliment();
            TopPoints = DaoFactory.CharacterDao.GetTopPoints();
            TopReputation = DaoFactory.CharacterDao.GetTopReputation();
        }

        public void RelationRefresh(long relationId)
        {
            _inRelationRefreshMode = true;
            CommunicationServiceClient.Instance.UpdateRelation(worldGroup: ServerGroup, relationId: relationId);
            SpinWait.SpinUntil(condition: () => !_inRelationRefreshMode);
        }

        public static void RemoveMapInstance(Guid mapId)
        {
            if (_mapinstances == null) return;

            if (_mapinstances.FirstOrDefault(predicate: s => s.Key == mapId) is KeyValuePair<Guid, MapInstance> map &&
                !map.Equals(obj: default))
            {
                map.Value.Dispose();
                ((IDictionary)_mapinstances).Remove(key: map.Key);
            }
        }

        // Map
        public void ReviveFirstPosition(long characterId)
        {
            var session = GetSessionByCharacterId(characterId: characterId);
            if (session?.Character.Hp <= 0)
            {
                if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.TimeSpaceInstance ||
                    session.CurrentMapInstance.MapInstanceType == MapInstanceType.RaidInstance)
                {
                    session.Character.Hp = (int)session.Character.HPLoad();
                    session.Character.Mp = (int)session.Character.MPLoad();
                    session.CurrentMapInstance?.Broadcast(packet: session.Character.GenerateRevive());
                    session.SendPacket(packet: session.Character.GenerateStat());
                }
                else
                {
                    if (ChannelId == 51)
                    {
                        if (session.CurrentMapInstance.MapInstanceId ==
                            session.Character.Family?.Act4RaidBossMap?.MapInstanceId)
                        {
                            session.Character.Hp = 1;
                            session.Character.Mp = 1;

                            switch (session.Character.Family.Act4Raid.MapInstanceType)
                            {
                                case MapInstanceType.Act4Morcos:
                                    Instance.ChangeMapInstance(characterId: session.Character.CharacterId,
                                        mapInstanceId: session.Character.Family.Act4Raid.MapInstanceId, mapX: 43,
                                        mapY: 179);
                                    break;

                                case MapInstanceType.Act4Hatus:
                                    Instance.ChangeMapInstance(characterId: session.Character.CharacterId,
                                        mapInstanceId: session.Character.Family.Act4Raid.MapInstanceId, mapX: 15,
                                        mapY: 9);
                                    break;

                                case MapInstanceType.Act4Calvina:
                                    Instance.ChangeMapInstance(characterId: session.Character.CharacterId,
                                        mapInstanceId: session.Character.Family.Act4Raid.MapInstanceId, mapX: 24,
                                        mapY: 6);
                                    break;

                                case MapInstanceType.Act4Berios:
                                    Instance.ChangeMapInstance(characterId: session.Character.CharacterId,
                                        mapInstanceId: session.Character.Family.Act4Raid.MapInstanceId, mapX: 20,
                                        mapY: 20);
                                    break;
                            }
                        }
                        else
                        {
                            session.Character.Hp = (int)session.Character.HPLoad();
                            session.Character.Mp = (int)session.Character.MPLoad();
                            var x = (short)(39 + RandomNumber(min: -2, max: 3));
                            var y = (short)(42 + RandomNumber(min: -2, max: 3));
                            if (session.Character.Faction == FactionType.Angel)
                                ChangeMap(id: session.Character.CharacterId, mapId: 130, mapX: x, mapY: y);
                            else if (session.Character.Faction == FactionType.Demon)
                                ChangeMap(id: session.Character.CharacterId, mapId: 131, mapX: x, mapY: y);
                        }
                    }
                    else
                    {
                        session.Character.Hp = 1;
                        session.Character.Mp = 1;
                        if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                        {
                            var resp = session.Character.Respawn;
                            var x = (short)(resp.DefaultX + RandomNumber(min: -3, max: 3));
                            var y = (short)(resp.DefaultY + RandomNumber(min: -3, max: 3));
                            ChangeMap(id: session.Character.CharacterId, mapId: resp.DefaultMapId, mapX: x, mapY: y);
                        }
                        else
                        {
                            Instance.ChangeMap(id: session.Character.CharacterId, mapId: session.Character.MapId,
                                mapX: session.Character.MapX, mapY: session.Character.MapY);
                        }
                    }

                    session.CurrentMapInstance?.Broadcast(client: session, content: session.Character.GenerateTp());
                    session.CurrentMapInstance?.Broadcast(packet: session.Character.GenerateRevive());
                    session.SendPacket(packet: session.Character.GenerateStat());
                }
            }
        }

        public void SaveAll()
        {
            CommunicationServiceClient.Instance.CleanupOutdatedSession();
            foreach (var sess in Sessions) sess.Character?.Save();
            DaoFactory.BazaarItemDao.RemoveOutDated();
        }

        public static void Shout(string message, bool noAdminTag = false)
        {
            Instance.Broadcast(packet: UserInterfaceHelper.GenerateSay(
                message: (noAdminTag ? "" : $"({Language.Instance.GetMessageFromKey(key: "ADMINISTRATOR")})") + message,
                type: 10));
            Instance.Broadcast(packet: UserInterfaceHelper.GenerateMsg(message: message, type: 2));
        }

        public async Task ShutdownTaskAsync(int Time = 5)
        {
            Shout(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHUTDOWN_MIN"), arg0: Time));
            if (Time > 1)
            {
                for (var i = 0; i < 60 * (Time - 1); i++)
                {
                    await Task.Delay(millisecondsDelay: 1000).ConfigureAwait(continueOnCapturedContext: false);
                    if (Instance.ShutdownStop)
                    {
                        Instance.ShutdownStop = false;
                        return;
                    }
                }

                Shout(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHUTDOWN_MIN"),
                    arg0: 1));
            }

            for (var i = 0; i < 30; i++)
            {
                await Task.Delay(millisecondsDelay: 1000).ConfigureAwait(continueOnCapturedContext: false);
                if (Instance.ShutdownStop)
                {
                    Instance.ShutdownStop = false;
                    return;
                }
            }

            Shout(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHUTDOWN_SEC"), arg0: 30));
            for (var i = 0; i < 30; i++)
            {
                await Task.Delay(millisecondsDelay: 1000).ConfigureAwait(continueOnCapturedContext: false);
                if (Instance.ShutdownStop)
                {
                    Instance.ShutdownStop = false;
                    return;
                }
            }

            Shout(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHUTDOWN_SEC"), arg0: 10));
            for (var i = 0; i < 10; i++)
            {
                await Task.Delay(millisecondsDelay: 1000).ConfigureAwait(continueOnCapturedContext: false);
                if (Instance.ShutdownStop)
                {
                    Instance.ShutdownStop = false;
                    return;
                }
            }

            InShutdown = true;
            foreach (var sess in Sessions) sess.Character?.Dispose();
            Instance.SaveAll();
            CommunicationServiceClient.Instance.UnregisterWorldServer(worldId: WorldId);
            if (IsReboot)
            {
                if (ChannelId == 51)
                    await Task.Delay(millisecondsDelay: 16000).ConfigureAwait(continueOnCapturedContext: false);
                else
                    await Task.Delay(millisecondsDelay: (ChannelId - 1) * 2000)
                        .ConfigureAwait(continueOnCapturedContext: false);
                Process.Start(fileName: "OpenNos.World.exe",
                    arguments: $"--nomsg{(ChannelId == 51 ? $" --port {Configuration.Act4Port}" : "")}");
            }

            Environment.Exit(exitCode: 0);
        }

        public void TeleportOnRandomPlaceInMap(ClientSession session, Guid guid)
        {
            var map = GetMapInstance(id: guid);
            if (guid != default)
            {
                var pos = map.Map.GetRandomPosition();
                if (pos == null) return;
                ChangeMapInstance(characterId: session.Character.CharacterId, mapInstanceId: guid, mapX: pos.X,
                    mapY: pos.Y);
            }
        }

        // Server
        public void UpdateGroup(long charId)
        {
            try
            {
                if (Groups != null)
                {
                    var myGroup = Groups.Find(match: s => s.IsMemberOfGroup(entityId: charId));
                    if (myGroup == null) return;
                    var groupMembers = Groups.Find(match: s => s.IsMemberOfGroup(entityId: charId))?.Sessions;
                    if (groupMembers != null)
                        foreach (var session in groupMembers.GetAllItems())
                        {
                            session.SendPacket(packet: session.Character.GeneratePinit());
                            session.SendPackets(packets: session.Character.GeneratePst());
                            session.SendPacket(packet: session.Character.GenerateStat());
                        }
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        internal List<NpcMonsterSkill> GetNpcMonsterSkillsByMonsterVNum(short npcMonsterVNum)
        {
            return _monsterSkills.ContainsKey(key: npcMonsterVNum)
                ? _monsterSkills[key: npcMonsterVNum]
                : new List<NpcMonsterSkill>();
        }

        internal Shop GetShopByMapNpcId(int mapNpcId)
        {
            return _shops.ContainsKey(key: mapNpcId) ? _shops[key: mapNpcId] : null;
        }

        internal List<ShopItemDto> GetShopItemsByShopId(int shopId)
        {
            return _shopItems.ContainsKey(key: shopId) ? _shopItems[key: shopId] : new List<ShopItemDto>();
        }

        internal List<ShopSkillDto> GetShopSkillsByShopId(int shopId)
        {
            return _shopSkills.ContainsKey(key: shopId) ? _shopSkills[key: shopId] : new List<ShopSkillDto>();
        }

        internal List<TeleporterDto> GetTeleportersByNpcVNum(int npcMonsterVNum)
        {
            if (_teleporters?.ContainsKey(key: npcMonsterVNum) == true) return _teleporters[key: npcMonsterVNum];
            return new List<TeleporterDto>();
        }

        internal static void StopServer()
        {
            Instance.ShutdownStop = true;
            Instance.TaskShutdown = null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _monsterDrops.Dispose();
                ThreadSafeGroupList.Dispose();
                _monsterSkills.Dispose();
                _shopSkills.Dispose();
                _shopItems.Dispose();
                _shops.Dispose();
                _recipes.Dispose();
                _mapNpcs.Dispose();
                _teleporters.Dispose();
                GC.SuppressFinalize(obj: this);
            }
        }

        void Act4FlowerProcess()
        {
            foreach (var map in GetAllMapInstances().Where(predicate: s =>
                s.Map.MapTypes.Any(predicate: m => m.MapTypeId == (short)MapTypeEnum.Act4) &&
                s.Npcs.Count(predicate: o => o.NpcVNum == 2004 && o.IsOut) <
                s.Npcs.Count(predicate: n => n.NpcVNum == 2004)))
                foreach (var i in map.Npcs.Where(predicate: s => s.IsOut && s.NpcVNum == 2004))
                {
                    var randomPos = map.Map.GetRandomPosition();
                    i.MapX = randomPos.X;
                    i.MapY = randomPos.Y;
                    i.MapInstance.Broadcast(packet: i.GenerateIn());
                }
        }

        void Act4Process()
        {
            if (ChannelId != 51) return;

            var angelMapInstance = GetMapInstance(id: GetBaseMapInstanceIdByMapId(mapId: 132));
            var demonMapInstance = GetMapInstance(id: GetBaseMapInstanceIdByMapId(mapId: 133));

            void SummonMukraju(MapInstance instance, byte faction)
            {
                var monster = new MapMonster
                {
                    MonsterVNum = 556,
                    MapY = faction == 1 ? (short)92 : (short)95,
                    MapX = faction == 1 ? (short)114 : (short)20,
                    MapId = (short)(131 + faction),
                    IsMoving = true,
                    MapMonsterId = instance.GetNextMonsterId(),
                    ShouldRespawn = false
                };
                monster.Initialize(currentMapInstance: instance);
                monster.Faction = (FactionType)faction == FactionType.Angel ? FactionType.Demon : FactionType.Angel;
                instance.AddMonster(monster: monster);
                instance.Broadcast(packet: monster.GenerateIn());

                Observable.Timer(
                        dueTime: TimeSpan.FromSeconds(value: faction == 1
                            ? Act4AngelStat.TotalTime
                            : Act4DemonStat.TotalTime))
                    .Subscribe(onNext: s =>
                    {
                        if (instance.Monsters.ToList().Any(predicate: m => m.MonsterVNum == monster.MonsterVNum))
                        {
                            if (faction == 1)
                                Act4AngelStat.Mode = 0;
                            else
                                Act4DemonStat.Mode = 0;
                            instance.DespawnMonster(monsterVnum: monster.MonsterVNum);
                            Parallel.ForEach(source: Sessions,
                                body: sess => sess.SendPacket(packet: sess.Character.GenerateFc()));
                        }
                    });
            }

            static int CreateRaid(byte faction)
            {
                var raidType = MapInstanceType.Act4Morcos;
                var rng = RandomNumber(min: 1, max: 5);
                switch (rng)
                {
                    case 2:
                        raidType = MapInstanceType.Act4Hatus;
                        break;

                    case 3:
                        raidType = MapInstanceType.Act4Calvina;
                        break;

                    case 4:
                        raidType = MapInstanceType.Act4Berios;
                        break;
                }

                Act4Raid.GenerateRaid(raidType: raidType, faction: faction);
                return rng;
            }

            if (Act4AngelStat.Percentage >= 10000)
            {
                Act4AngelStat.Mode = 1;
                Act4AngelStat.Percentage = 0;
                Act4AngelStat.TotalTime = 300;
                SummonMukraju(instance: angelMapInstance, faction: 1);
                Parallel.ForEach(source: Sessions, body: sess => sess.SendPacket(packet: sess.Character.GenerateFc()));
            }

            if (Act4AngelStat.Mode == 1 && !angelMapInstance.Monsters.Any(predicate: s => s.MonsterVNum == 556))
            {
                Act4AngelStat.Mode = 3;
                Act4AngelStat.TotalTime = 3600;

                switch (CreateRaid(faction: 1))
                {
                    case 1:
                        Act4AngelStat.IsMorcos = true;
                        break;

                    case 2:
                        Act4AngelStat.IsHatus = true;
                        break;

                    case 3:
                        Act4AngelStat.IsCalvina = true;
                        break;

                    case 4:
                        Act4AngelStat.IsBerios = true;
                        break;
                }

                Parallel.ForEach(source: Sessions, body: sess => sess.SendPacket(packet: sess.Character.GenerateFc()));
            }

            if (Act4DemonStat.Percentage >= 10000)
            {
                Act4DemonStat.Mode = 1;
                Act4DemonStat.Percentage = 0;
                Act4DemonStat.TotalTime = 300;
                SummonMukraju(instance: demonMapInstance, faction: 2);
                Parallel.ForEach(source: Sessions, body: sess => sess.SendPacket(packet: sess.Character.GenerateFc()));
            }

            if (Act4DemonStat.Mode == 1 && !demonMapInstance.Monsters.Any(predicate: s => s.MonsterVNum == 556))
            {
                Act4DemonStat.Mode = 3;
                Act4DemonStat.TotalTime = 3600;

                switch (CreateRaid(faction: 2))
                {
                    case 1:
                        Act4DemonStat.IsMorcos = true;
                        break;

                    case 2:
                        Act4DemonStat.IsHatus = true;
                        break;

                    case 3:
                        Act4DemonStat.IsCalvina = true;
                        break;

                    case 4:
                        Act4DemonStat.IsBerios = true;
                        break;
                }

                Parallel.ForEach(source: Sessions, body: sess => sess.SendPacket(packet: sess.Character.GenerateFc()));
            }

            if (DateTime.Now >= LastFCSent.AddMinutes(value: 1))
            {
                Parallel.ForEach(source: Sessions, body: sess => sess.SendPacket(packet: sess.Character.GenerateFc()));
                LastFCSent = DateTime.Now;
            }
        }

        // Server

        void GroupProcess()
        {
            try
            {
                if (Groups != null)
                    Parallel.ForEach(source: Groups, body: grp =>
                    {
                        foreach (var session in grp.Sessions.GetAllItems())
                            if (grp.GroupType == GroupType.Group)
                                session.SendPackets(packets: grp.GeneratePst(player: session));
                            else
                                session.SendPacket(packet: grp.GenerateRdlst());
                    });
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        void LaunchEvents()
        {
            ThreadSafeGroupList = new ThreadSafeSortedList<long, Group>();

            Observable.Interval(period: TimeSpan.FromMinutes(value: 5)).Subscribe(onNext: x => SaveAllProcess());
            Observable.Interval(period: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: x => Act4Process());
            Observable.Interval(period: TimeSpan.FromSeconds(value: 2)).Subscribe(onNext: x => GroupProcess());
            Observable.Interval(period: TimeSpan.FromMinutes(value: 1)).Subscribe(onNext: x => Act4FlowerProcess());
            //Observable.Interval(TimeSpan.FromHours(3)).Subscribe(x => BotProcess());
            Observable.Interval(period: TimeSpan.FromSeconds(value: 5)).Subscribe(onNext: x => MaintenanceProcess());

            EventHelper.Instance.RunEvent(evt: new EventContainer(
                mapInstance: GetMapInstance(id: GetBaseMapInstanceIdByMapId(mapId: 98)),
                eventActionType: EventActionType.Npcseffectchangestate, param: true));
            Parallel.ForEach(source: Schedules, body: schedule => Observable
                .Timer(
                    dueTime: TimeSpan.FromSeconds(value: EventHelper.GetMilisecondsBeforeTime(time: schedule.Time)
                        .TotalSeconds),
                    period: TimeSpan.FromDays(value: 1)).Subscribe(onNext: e =>
                {
                    if (schedule.DayOfWeek == "" || schedule.DayOfWeek == DateTime.Now.DayOfWeek.ToString())
                        EventHelper.GenerateEvent(type: schedule.Event, lvlBracket: schedule.LvlBracket);
                }));
            EventHelper.GenerateEvent(type: EventType.Act4Ship);

            Observable.Interval(period: TimeSpan.FromSeconds(value: 1)).Subscribe(onNext: x => RemoveItemProcess());
            Observable.Interval(period: TimeSpan.FromMilliseconds(value: 400)).Subscribe(onNext: x =>
            {
                Parallel.ForEach(source: _mapinstances, body: map =>
                {
                    Parallel.ForEach(source: map.Value.Npcs, body: npc => npc.StartLife());
                    Parallel.ForEach(source: map.Value.Monsters, body: monster => monster.StartLife());
                });
            });

            CommunicationServiceClient.Instance.SessionKickedEvent += OnSessionKicked;
            CommunicationServiceClient.Instance.MessageSentToCharacter += OnMessageSentToCharacter;
            CommunicationServiceClient.Instance.FamilyRefresh += OnFamilyRefresh;
            CommunicationServiceClient.Instance.RelationRefresh += OnRelationRefresh;
            CommunicationServiceClient.Instance.StaticBonusRefresh += OnStaticBonusRefresh;
            CommunicationServiceClient.Instance.BazaarRefresh += OnBazaarRefresh;
            CommunicationServiceClient.Instance.PenaltyLogRefresh += OnPenaltyLogRefresh;
            CommunicationServiceClient.Instance.GlobalEvent += OnGlobalEvent;
            CommunicationServiceClient.Instance.ShutdownEvent += OnShutdown;
            CommunicationServiceClient.Instance.RestartEvent += OnRestart;
            ConfigurationServiceClient.Instance.ConfigurationUpdate += OnConfiguratinEvent;
            MailServiceClient.Instance.MailSent += OnMailSent;
            _lastGroupId = 1;
        }

        public void SynchronizeSheduling()
        {
            if (Schedules.FirstOrDefault(predicate: s => s.Event == EventType.Talentarena)?.Time is TimeSpan
                    arenaOfTalentsTime
                && IsTimeBetween(dateTime: DateTime.Now, start: arenaOfTalentsTime,
                    end: arenaOfTalentsTime.Add(ts: new TimeSpan(hours: 4, minutes: 0, seconds: 0))))
                EventHelper.GenerateEvent(type: EventType.Talentarena);
            Schedules.Where(predicate: s => s.Event == EventType.Lod).ToList().ForEach(action: lodSchedule =>
            {
                if (IsTimeBetween(dateTime: DateTime.Now, start: lodSchedule.Time,
                    end: lodSchedule.Time.Add(ts: new TimeSpan(hours: 2, minutes: 0, seconds: 0))))
                    EventHelper.GenerateEvent(type: EventType.Lod);
            });
        }

        bool IsTimeBetween(DateTime dateTime, TimeSpan start, TimeSpan end)
        {
            var now = dateTime.TimeOfDay;

            return start < end ? start <= now && now <= end : !(end < now && now < start);
        }

        void OnStaticBonusRefresh(object sender, EventArgs e)
        {
            var characterId = (long)sender;

            var sess = GetSessionByCharacterId(characterId: characterId);
            if (sess != null)
                sess.Character.StaticBonusList =
                    DaoFactory.StaticBonusDao.LoadByCharacterId(characterId: characterId).ToList();
        }

        void OnMailSent(object sender, EventArgs e)
        {
            var mail = (MailDto)sender;

            var session = GetSessionByCharacterId(characterId: mail.IsSenderCopy ? mail.SenderId : mail.ReceiverId);
            if (session != null)
            {
                if (mail.AttachmentVNum != null)
                {
                    session.Character.MailList.Add(
                        key: (session.Character.MailList.Count > 0
                            ? session.Character.MailList.OrderBy(keySelector: s => s.Key).Last().Key
                            : 0) + 1, value: mail);
                    session.SendPacket(packet: session.Character.GenerateParcel(mail: mail));
                    //session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("ITEM_GIFTED"), GetItem(mail.AttachmentVNum.Value)?.Name, mail.AttachmentAmount), 12));
                }
                else
                {
                    session.Character.MailList.Add(
                        key: (session.Character.MailList.Count > 0
                            ? session.Character.MailList.OrderBy(keySelector: s => s.Key).Last().Key
                            : 0) + 1, value: mail);
                    session.SendPacket(packet: session.Character.GeneratePost(mail: mail,
                        type: mail.IsSenderCopy ? (byte)2 : (byte)1));
                }
            }
        }

        void OnConfiguratinEvent(object sender, EventArgs e)
        {
            Configuration = (ConfigurationObject)sender;
        }

        void LoadFamilies()
        {
            FamilyList = new ThreadSafeSortedList<long, Family>();
            Parallel.ForEach(source: DaoFactory.FamilyDao.LoadAll(), body: familyDto =>
            {
                var family = new Family(input: familyDto)
                {
                    FamilyCharacters = new List<FamilyCharacter>()
                };
                foreach (var famchar in DaoFactory.FamilyCharacterDao.LoadByFamilyId(familyId: family.FamilyId)
                    .ToList())
                    family.FamilyCharacters.Add(item: new FamilyCharacter(input: famchar));
                var familyCharacter = family.FamilyCharacters.Find(match: s => s.Authority == FamilyAuthority.Head);
                if (familyCharacter != null)
                {
                    family.Warehouse = new Inventory(Character: new Character(input: familyCharacter.Character));
                    foreach (var inventory in DaoFactory.ItemInstanceDao
                        .LoadByCharacterId(characterId: familyCharacter.CharacterId)
                        .Where(predicate: s => s.Type == InventoryType.FamilyWareHouse).ToList())
                    {
                        inventory.CharacterId = familyCharacter.CharacterId;
                        family.Warehouse[key: inventory.Id] = new ItemInstance(input: inventory);
                    }
                }

                family.FamilyLogs = DaoFactory.FamilyLogDao.LoadByFamilyId(familyId: family.FamilyId).ToList();
                FamilyList[key: family.FamilyId] = family;
            });
        }

        public void LoadScriptedInstances()
        {
            Raids = new ConcurrentBag<ScriptedInstance>();
            TimeSpaces = new ConcurrentBag<ScriptedInstance>();
            Parallel.ForEach(source: _mapinstances, body: map =>
            {
                if (map.Value.MapInstanceType == MapInstanceType.BaseMapInstance)
                {
                    map.Value.ScriptedInstances.Clear();
                    map.Value.Portals.Clear();
                    foreach (var si in DaoFactory.ScriptedInstanceDao.LoadByMap(mapId: map.Value.Map.MapId).ToList())
                    {
                        var siObj = new ScriptedInstance(input: si);
                        switch (siObj.Type)
                        {
                            case ScriptedInstanceType.TimeSpace:
                            case ScriptedInstanceType.QuestTimeSpace:
                                siObj.LoadGlobals();
                                if (siObj.Script != null) TimeSpaces.Add(item: siObj);
                                map.Value.ScriptedInstances.Add(item: siObj);
                                break;
                            case ScriptedInstanceType.Raid:
                                siObj.LoadGlobals();
                                if (siObj.Script != null) Raids.Add(item: siObj);
                                var port = new Portal
                                {
                                    Type = (byte)PortalType.Raid,
                                    SourceMapId = siObj.MapId,
                                    SourceX = siObj.PositionX,
                                    SourceY = siObj.PositionY
                                };
                                map.Value.Portals.Add(item: port);
                                break;
                        }
                    }

                    map.Value.LoadPortals();
                    map.Value.MapClear();
                }
            });
        }

        void LoadBannedCharacters()
        {
            BannedCharacters.Clear();
            DaoFactory.CharacterDao.LoadAll().ToList().ForEach(action: s =>
            {
                if (s.State != CharacterState.Active || DaoFactory.PenaltyLogDao.LoadByAccount(accountId: s.AccountId)
                    .Any(predicate: c => c.DateEnd > DateTime.Now && c.Penalty == PenaltyType.Banned))
                    BannedCharacters.Add(item: s.CharacterId);
            });
        }

        void LoadBossEntities()
        {
            BossVNums = new List<short>
            {
                580, //Perro infernal
                582, //Ginseng fuerte
                583, //Basilisco fuerte
                584, //Rey Tubérculo
                585, //Gran Rey Tubérculo
                586, //Rey Tubérculo monstruoso
                588, //Pollito gigante
                589, //Mandra milenaria
                590, //Perro guardián del infierno
                591, //Gallo de pelea gigante
                592, //Patata milenaria
                593, //Perro de Satanás
                594, //Barepardo gigante
                595, //Boing satélite
                596, //Raíz demoníaca
                597, //Slade musculoso
                598, //Rey de las tinieblas
                599, //Gólem slade
                600, //Mini Castra
                601, //Caballero de la muerte
                602, //Sanguijuela gorda
                603, //Ginseng milenario
                604, //Rey basilisco
                605, //Gigante guerrero
                606, //Árbol maldito
                607, //Rey cangrejo
                2529, //Castra Oscuro
                1904, //Sombra de Kertos
                796, //Rey Pollo
                388, //Rey Pollo
                774, //Reina Gallina
                2331, //Diablilla Hongbi
                2332, //Diablilla Cheongbi
                2309, //Vulpina
                2322, //Maru
                1381, //Jack O´Lantern
                2357, //Lola Cucharón
                533, //Cabeza grande de muñeco de nieve
                1500, //Capitán Pete O'Peng
                282, //Mamá cubi
                284, //Ginseng
                285, //Castra Oscuro
                289, //Araña negra gigante
                286, //Slade gigante
                587, //Lord Mukraju
                563, //Maestro Morcos
                629, //Lady Calvina
                624, //Lord Berios
                577, //Maestro Hatus
                557, //Ross Hisrant
                2305, //Caligor
                2326, //Bruja Laurena
                2327, //Bestia de Laurena
                2639, //Yertirán podrido
                1028, //Ibrahim
                2034, //Lord Draco
                2049, //Glacerus, el frío
                1044, //Valakus, Rey del Fuego
                1905, //Sombra de Valakus
                1046, //Kertos, el Perro Demonio
                1912, //Sombra de Kertos
                637, //Perro demonio Kertos fuerte
                1099, //Fantasma de Grenigas
                1058, //Grenigas, el Dios del Fuego
                2619, //Fafnir, el Codicioso
                2504, //Zenas
                2514, //Erenia
                2574, //Fernon incompleta
                679, //Guardián de los ángeles 
                680, //Guardián de los demonios
                967, //Altar de los ángeles
                968, //Altar de los diablo
                533 // Huge Snowman Head
            };
            MapBossVNums = new List<short>
            {
                580, //Perro infernal
                582, //Ginseng fuerte
                583, //Basilisco fuerte
                584, //Rey Tubérculo
                585, //Gran Rey Tubérculo
                586, //Rey Tubérculo monstruoso
                588, //Pollito gigante
                589, //Mandra milenaria
                590, //Perro guardián del infierno
                591, //Gallo de pelea gigante
                592, //Patata milenaria
                593, //Perro de Satanás
                594, //Barepardo gigante
                595, //Boing satélite
                596, //Raíz demoníaca
                597, //Slade musculoso
                598, //Rey de las tinieblas
                599, //Gólem slade
                600, //Mini Castra
                601, //Caballero de la muerte
                602, //Sanguijuela gorda
                603, //Ginseng milenario
                604, //Rey basilisco
                605, //Gigante guerrero
                606, //Árbol maldito
                607, //Rey cangrejo
                2529, //Castra Oscuro
                1904, //Sombra de Kertos
                1346, //Baúl de la banda de ladrones
                1347, //Baúl del tesoro del olvido
                1348, //Baúl extraño
                1384, //Halloween
                1906,
                1905,
                2350
            };
        }

        void MaintenanceProcess()
        {
            var sessions = Sessions.Where(predicate: c => c.IsConnected).ToList();
            var maintenanceLog = DaoFactory.MaintenanceLogDao.LoadFirst();
            if (maintenanceLog != null)
            {
                if (maintenanceLog.DateStart <= DateTime.Now)
                {
                    Logger.LogUserEvent(logEvent: "MAINTENANCE_STATE", caller: "Caller: ServerManager",
                        data: $"[Maintenance]{Language.Instance.GetMessageFromKey(key: "MAINTENANCE_PLANNED")}");
                    sessions.Where(predicate: s => s.Account.Authority < AuthorityType.Mod).ToList()
                        .ForEach(action: session => session.Disconnect());
                }
                else if (LastMaintenanceAdvert.AddMinutes(value: 1) <= DateTime.Now &&
                         maintenanceLog.DateStart <= DateTime.Now.AddMinutes(value: 5))
                {
                    var min = (maintenanceLog.DateStart - DateTime.Now).Minutes;
                    if (min != 0) Shout(message: $"Maintenance will begin in {min} minutes");
                    LastMaintenanceAdvert = DateTime.Now;
                }
            }
        }

        void OnBazaarRefresh(object sender, EventArgs e)
        {
            var bazaarId = (long)sender;
            var bzdto = DaoFactory.BazaarItemDao.LoadById(bazaarItemId: bazaarId);
            var bzlink = BazaarList.Find(predicate: s => s.BazaarItem.BazaarItemId == bazaarId);
            lock (BazaarList)
            {
                if (bzdto != null)
                {
                    var chara = DaoFactory.CharacterDao.LoadById(characterId: bzdto.SellerId);
                    if (bzlink != null)
                    {
                        BazaarList.Remove(match: bzlink);
                        bzlink.BazaarItem = bzdto;
                        bzlink.Owner = chara.Name;
                        bzlink.Item =
                            new ItemInstance(input: DaoFactory.ItemInstanceDao.LoadById(id: bzdto.ItemInstanceId));
                        BazaarList.Add(value: bzlink);
                    }
                    else
                    {
                        var item = new BazaarItemLink
                        {
                            BazaarItem = bzdto
                        };
                        if (chara != null)
                        {
                            item.Owner = chara.Name;
                            item.Item = new ItemInstance(
                                input: DaoFactory.ItemInstanceDao.LoadById(id: bzdto.ItemInstanceId));
                        }

                        BazaarList.Add(value: item);
                    }
                }
                else if (bzlink != null)
                {
                    BazaarList.Remove(match: bzlink);
                }
            }

            InBazaarRefreshMode = false;
        }

        void OnFamilyRefresh(object sender, EventArgs e)
        {
            var tuple = (Tuple<long, bool>)sender;
            var familyId = tuple.Item1;
            var famdto = DaoFactory.FamilyDao.LoadById(familyId: familyId);
            var fam = FamilyList[key: familyId];
            lock (FamilyList)
            {
                if (famdto != null)
                {
                    var newFam = new Family(input: famdto);
                    if (fam != null)
                    {
                        newFam.LandOfDeath = fam.LandOfDeath;
                        newFam.Act4Raid = fam.Act4Raid;
                        newFam.Act4RaidBossMap = fam.Act4RaidBossMap;
                    }

                    newFam.FamilyCharacters = new List<FamilyCharacter>();
                    foreach (var famchar in DaoFactory.FamilyCharacterDao.LoadByFamilyId(familyId: famdto.FamilyId)
                        .ToList())
                        newFam.FamilyCharacters.Add(item: new FamilyCharacter(input: famchar));
                    var familyHead = newFam.FamilyCharacters.Find(match: s => s.Authority == FamilyAuthority.Head);
                    if (familyHead != null)
                    {
                        newFam.Warehouse = new Inventory(Character: new Character(input: familyHead.Character));
                        foreach (var inventory in DaoFactory.ItemInstanceDao
                            .LoadByCharacterId(characterId: familyHead.CharacterId)
                            .Where(predicate: s => s.Type == InventoryType.FamilyWareHouse).ToList())
                        {
                            inventory.CharacterId = familyHead.CharacterId;
                            newFam.Warehouse[key: inventory.Id] = new ItemInstance(input: inventory);
                        }
                    }

                    newFam.FamilyLogs = DaoFactory.FamilyLogDao.LoadByFamilyId(familyId: famdto.FamilyId).ToList();
                    FamilyList[key: familyId] = newFam;

                    Parallel.ForEach(
                        source: Sessions.Where(predicate: s =>
                            newFam.FamilyCharacters.Any(predicate: m => m.CharacterId == s.Character.CharacterId)),
                        body: session =>
                        {
                            if (session.Character.LastFamilyLeave < DateTime.Now.AddDays(value: -1).Ticks)
                            {
                                session.Character.Family = newFam;

                                if (tuple.Item2)
                                    session.Character.ChangeFaction(faction: (FactionType)newFam.FamilyFaction);
                                session?.CurrentMapInstance?.Broadcast(packet: session?.Character?.GenerateGidx());
                            }
                        });
                }
                else if (fam != null)
                {
                    lock (FamilyList)
                    {
                        FamilyList.Remove(key: fam.FamilyId);
                    }

                    foreach (var sess in Sessions.Where(predicate: s =>
                        fam.FamilyCharacters.Any(predicate: f => f.CharacterId.Equals(obj: s.Character.CharacterId))))
                    {
                        sess.Character.Family = null;
                        sess.SendPacket(packet: sess.Character.GenerateGidx());
                    }
                }
            }
        }

        static void OnGlobalEvent(object sender, EventArgs e)
        {
            EventHelper.GenerateEvent(type: (EventType)sender);
        }

        void OnMessageSentToCharacter(object sender, EventArgs e)
        {
            if (sender != null)
            {
                var message = (ScsCharacterMessage)sender;

                var targetSession =
                    Sessions.SingleOrDefault(predicate: s => s.Character.CharacterId == message.DestinationCharacterId);
                switch (message.Type)
                {
                    case MessageType.WhisperGm:
                    case MessageType.Whisper:
                        if (
                            targetSession ==
                            null /* || (message.Type == MessageType.WhisperGM && targetSession.Account.Authority != AuthorityType.GMmeMaster)*/
                        ) return;

                        if (targetSession.Character.GmPvtBlock)
                        {
                            if (message.DestinationCharacterId != null)
                                CommunicationServiceClient.Instance.SendMessageToCharacter(
                                    message: new ScsCharacterMessage
                                    {
                                        DestinationCharacterId = message.SourceCharacterId,
                                        SourceCharacterId = message.DestinationCharacterId.Value,
                                        SourceWorldId = WorldId,
                                        Message = targetSession.Character.GenerateSay(
                                            message: Language.Instance.GetMessageFromKey(key: "GM_CHAT_BLOCKED"),
                                            type: 10),
                                        Type = MessageType.Other
                                    });
                        }
                        else if (targetSession.Character.WhisperBlocked
                                 && DaoFactory.AccountDao
                                     .LoadById(accountId: DaoFactory.CharacterDao
                                         .LoadById(characterId: message.SourceCharacterId).AccountId)
                                     .Authority < AuthorityType.Mod)
                        {
                            if (message.DestinationCharacterId != null)
                                CommunicationServiceClient.Instance.SendMessageToCharacter(
                                    message: new ScsCharacterMessage
                                    {
                                        DestinationCharacterId = message.SourceCharacterId,
                                        SourceCharacterId = message.DestinationCharacterId.Value,
                                        SourceWorldId = WorldId,
                                        Message = UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "USER_WHISPER_BLOCKED"),
                                            type: 0),
                                        Type = MessageType.Other
                                    });
                        }
                        else
                        {
                            if (message.SourceWorldId != WorldId)
                            {
                                if (message.DestinationCharacterId != null)
                                    CommunicationServiceClient.Instance.SendMessageToCharacter(
                                        message: new ScsCharacterMessage
                                        {
                                            DestinationCharacterId = message.SourceCharacterId,
                                            SourceCharacterId = message.DestinationCharacterId.Value,
                                            SourceWorldId = WorldId,
                                            Message = targetSession.Character.GenerateSay(
                                                message: string.Format(
                                                    format: Language.Instance.GetMessageFromKey(
                                                        key: "MESSAGE_SENT_TO_CHARACTER"),
                                                    arg0: targetSession.Character.Name, arg1: ChannelId), type: 11),
                                            Type = MessageType.Other
                                        });
                                targetSession.SendPacket(
                                    packet:
                                    $"{message.Message} <{Language.Instance.GetMessageFromKey(key: "CHANNEL")}: {CommunicationServiceClient.Instance.GetChannelIdByWorldId(worldId: message.SourceWorldId)}>");
                            }
                            else
                            {
                                targetSession.SendPacket(packet: message.Message);
                            }
                        }

                        break;

                    case MessageType.Shout:
                        Shout(message: message.Message);
                        break;

                    case MessageType.PrivateChat:
                        targetSession?.SendPacket(packet: message.Message);
                        break;

                    case MessageType.FamilyChat:
                        if (message.DestinationCharacterId.HasValue && message.SourceWorldId != WorldId)
                            Parallel.ForEach(source: Instance.Sessions, body: session =>
                            {
                                if (session.HasSelectedCharacter && session.Character.Family != null &&
                                    session.Character.Family.FamilyId == message.DestinationCharacterId)
                                    session.SendPacket(
                                        packet:
                                        $"say 1 0 6 <{Language.Instance.GetMessageFromKey(key: "CHANNEL")}: {CommunicationServiceClient.Instance.GetChannelIdByWorldId(worldId: message.SourceWorldId)}>{message.Message}");
                            });
                        break;

                    case MessageType.Family:
                        if (message.DestinationCharacterId.HasValue)
                            Parallel.ForEach(source: Instance.Sessions, body: session =>
                            {
                                if (session.HasSelectedCharacter && session.Character.Family != null &&
                                    session.Character.Family.FamilyId == message.DestinationCharacterId)
                                    session.SendPacket(packet: message.Message);
                            });
                        break;

                    case MessageType.Other:
                        targetSession?.SendPacket(packet: message.Message);
                        break;

                    case MessageType.Broadcast:
                        Parallel.ForEach(source: Instance.Sessions,
                            body: session => session.SendPacket(packet: message.Message));
                        break;
                }
            }
        }

        void OnPenaltyLogRefresh(object sender, EventArgs e)
        {
            var relId = (int)sender;
            var reldto = DaoFactory.PenaltyLogDao.LoadById(penaltyLogId: relId);
            var rel = PenaltyLogs.Find(match: s => s.PenaltyLogId == relId);
            if (reldto != null)
            {
                if (rel != null)
                {
                }
                else
                {
                    PenaltyLogs.Add(item: reldto);
                }
            }
            else if (rel != null)
            {
                PenaltyLogs.Remove(item: rel);
            }
        }

        void OnRelationRefresh(object sender, EventArgs e)
        {
            _inRelationRefreshMode = true;
            var relId = (long)sender;
            lock (CharacterRelations)
            {
                var reldto = DaoFactory.CharacterRelationDao.LoadById(characterId: relId);
                var rel = CharacterRelations.Find(match: s => s.CharacterRelationId == relId);
                if (reldto != null)
                {
                    if (rel != null)
                        CharacterRelations.Find(match: s => s.CharacterRelationId == rel.CharacterRelationId)
                                .RelationType =
                            reldto.RelationType;
                    else
                        CharacterRelations.Add(item: reldto);
                }
                else if (rel != null)
                {
                    CharacterRelations.Remove(item: rel);
                }
            }

            _inRelationRefreshMode = false;
        }

        void OnSessionKicked(object sender, EventArgs e)
        {
            if (sender != null)
            {
                var kickedSession = (Tuple<long?, long?>)sender;
                if (!kickedSession.Item1.HasValue && !kickedSession.Item2.HasValue) return;
                var accId = kickedSession.Item1;
                var sessId = kickedSession.Item2;

                var targetSession =
                    CharacterScreenSessions.FirstOrDefault(predicate: s =>
                        s.SessionId == sessId || s.Account.AccountId == accId);
                targetSession?.Disconnect();
                targetSession =
                    Sessions.FirstOrDefault(predicate: s => s.SessionId == sessId || s.Account.AccountId == accId);
                targetSession?.Disconnect();
            }
        }

        static void OnShutdown(object sender, EventArgs e)
        {
            if (Instance.TaskShutdown != null)
            {
                Instance.ShutdownStop = true;
                Instance.TaskShutdown = null;
            }
            else
            {
                Instance.TaskShutdown = Instance.ShutdownTaskAsync();
                Instance.TaskShutdown.Start();
            }
        }

        static void OnRestart(object sender, EventArgs e)
        {
            if (Instance.TaskShutdown != null)
            {
                Instance.IsReboot = false;
                Instance.ShutdownStop = true;
                Instance.TaskShutdown = null;
            }
            else
            {
                Instance.IsReboot = true;
                Instance.TaskShutdown = Instance.ShutdownTaskAsync(Time: (int)sender);
            }
        }

        void RemoveItemProcess()
        {
            try
            {
                Parallel.ForEach(source: Sessions.Where(predicate: c => c.IsConnected),
                    body: session => session.Character?.RefreshValidity());
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        static void ReviveTask(ClientSession session)
        {
            Task.Factory.StartNew(function: async () =>
            {
                var revive = true;
                for (var i = 1; i <= 30; i++)
                {
                    await Task.Delay(millisecondsDelay: 1000).ConfigureAwait(continueOnCapturedContext: false);
                    if (session.Character.Hp > 0)
                    {
                        revive = false;
                        break;
                    }
                }

                if (revive) Instance.ReviveFirstPosition(characterId: session.Character.CharacterId);
            });
        }

        // Server
        void SaveAllProcess()
        {
            try
            {
                Logger.Info(message: Language.Instance.GetMessageFromKey(key: "SAVING_ALL"));
                SaveAll();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        #endregion
    }
}