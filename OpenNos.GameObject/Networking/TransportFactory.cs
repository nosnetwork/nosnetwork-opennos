﻿namespace OpenNos.GameObject
{
    public class TransportFactory
    {
        #region Instantiation

        TransportFactory()
        {
            // do nothing
        }

        #endregion

        #region Properties

        public static TransportFactory Instance
        {
            get => _instance ?? (_instance = new TransportFactory());
        }

        #endregion

        #region Methods

        public long GenerateTransportId()
        {
            _lastTransportId++;

            if (_lastTransportId >= long.MaxValue) _lastTransportId = 0;

            return _lastTransportId;
        }

        #endregion

        #region Members

        static TransportFactory _instance;

        long _lastTransportId = 100000;

        #endregion
    }
}