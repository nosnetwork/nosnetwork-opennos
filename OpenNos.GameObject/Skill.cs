﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.GameObject
{
    public class Skill : SkillDto
    {
        #region Instantiation

        public Skill()
        {
            Combos = new List<ComboDto>();
            BCards = new List<BCard>();
        }

        public Skill(SkillDto input) : this()
        {
            AttackAnimation = input.AttackAnimation;
            CastAnimation = input.CastAnimation;
            CastEffect = input.CastEffect;
            CastId = input.CastId;
            CastTime = input.CastTime;
            Class = input.Class;
            Cooldown = input.Cooldown;
            CpCost = input.CpCost;
            Duration = input.Duration;
            Effect = input.Effect;
            Element = input.Element;
            HitType = input.HitType;
            ItemVNum = input.ItemVNum;
            Level = input.Level;
            LevelMinimum = input.LevelMinimum;
            MinimumAdventurerLevel = input.MinimumAdventurerLevel;
            MinimumArcherLevel = input.MinimumArcherLevel;
            MinimumMagicianLevel = input.MinimumMagicianLevel;
            MinimumSwordmanLevel = input.MinimumSwordmanLevel;
            MpCost = input.MpCost;
            Name = input.Name;
            Price = input.Price;
            Range = input.Range;
            SkillType = input.SkillType;
            SkillVNum = input.SkillVNum;
            TargetRange = input.TargetRange;
            TargetType = input.TargetType;
            Type = input.Type;
            UpgradeSkill = input.UpgradeSkill;
            UpgradeType = input.UpgradeType;
        }

        #endregion

        #region Properties

        public PartnerSkill PartnerSkill { get; set; }

        public List<BCard> BCards { get; set; }

        public List<ComboDto> Combos { get; set; }

        #endregion
    }
}