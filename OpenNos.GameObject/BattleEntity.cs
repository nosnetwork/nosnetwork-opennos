using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Threading;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Event.ICEBREAKER;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.PathFinder;
using static OpenNos.Domain.BCardType;

namespace OpenNos.GameObject
{
    public class BattleEntity
    {
        #region Instantiation

        public BattleEntity(Character character, Skill skill)
        {
            Character = character;

            if (character.BattleEntity != null)
            {
                Buffs = character.Buff;
                BuffObservables = character.BuffObservables;
                BCardDisposables = character.BattleEntity.BCardDisposables;
                BCards = character.EquipmentBCards;
                CellonOptions = character.CellonOptions;
                OnDeathEvents = character.OnDeathEvents;
            }
            else
            {
                Buffs = new ThreadSafeSortedList<short, Buff.Buff>();
                BuffObservables = new ThreadSafeSortedList<short, IDisposable>();
                BCardDisposables = new ThreadSafeSortedList<int, IDisposable>();
                BCards = new ThreadSafeGenericLockedList<BCard>();
                CellonOptions = new ThreadSafeGenericList<CellonOptionDto>();
                OnDeathEvents = new List<EventContainer>();
            }

            EntityType = EntityType.Player;
            UserType = UserType.Player;

            DamageMinimum = character.MinHit;
            DamageMaximum = character.MaxHit;
            Hitrate = character.HitRate;
            CritChance = character.HitCriticalChance;
            CritRate = character.HitCriticalRate;
            Morale = character.Level;
            FireResistance = character.FireResistance;
            WaterResistance = character.WaterResistance;
            LightResistance = character.LightResistance;
            ShadowResistance = character.DarkResistance;

            ItemInstance weapon = null;

            if (skill != null)
            {
                switch (skill.Type)
                {
                    case 0:
                        AttackType = AttackType.Melee;
                        if (character.Class == ClassType.Archer)
                        {
                            DamageMinimum = character.SecondWeaponMinHit;
                            DamageMaximum = character.SecondWeaponMaxHit;
                            Hitrate = character.SecondWeaponHitRate;
                            CritChance = character.SecondWeaponCriticalChance;
                            CritRate = character.SecondWeaponCriticalRate;
                            weapon = character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.SecondaryWeapon,
                                type: InventoryType.Wear);
                        }
                        else
                        {
                            weapon = character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon,
                                type: InventoryType.Wear);
                        }

                        break;

                    case 1:
                        AttackType = AttackType.Range;
                        if (character.Class == ClassType.Adventurer || character.Class == ClassType.Swordsman ||
                            character.Class == ClassType.Magician)
                        {
                            DamageMinimum = character.SecondWeaponMinHit;
                            DamageMaximum = character.SecondWeaponMaxHit;
                            Hitrate = character.SecondWeaponHitRate;
                            CritChance = character.SecondWeaponCriticalChance;
                            CritRate = character.SecondWeaponCriticalRate;
                            weapon = character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.SecondaryWeapon,
                                type: InventoryType.Wear);
                        }
                        else
                        {
                            weapon = character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon,
                                type: InventoryType.Wear);
                        }

                        break;

                    case 2:
                        AttackType = AttackType.Magical;
                        weapon = character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon,
                            type: InventoryType.Wear);
                        break;

                    case 3:
                        weapon = character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon,
                            type: InventoryType.Wear);
                        switch (character.Class)
                        {
                            case ClassType.Adventurer:
                            case ClassType.Swordsman:
                                AttackType = AttackType.Melee;
                                break;

                            case ClassType.Archer:
                                AttackType = AttackType.Range;
                                break;

                            case ClassType.Magician:
                                AttackType = AttackType.Magical;
                                break;
                        }

                        break;

                    case 5:
                        AttackType = AttackType.Melee;
                        switch (character.Class)
                        {
                            case ClassType.Adventurer:
                            case ClassType.Swordsman:
                            case ClassType.Magician:
                                weapon = character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon,
                                    type: InventoryType.Wear);
                                break;

                            case ClassType.Archer:
                                DamageMinimum = character.SecondWeaponMinHit;
                                DamageMaximum = character.SecondWeaponMaxHit;
                                Hitrate = character.SecondWeaponHitRate;
                                CritChance = character.SecondWeaponCriticalChance;
                                CritRate = character.SecondWeaponCriticalRate;
                                weapon = character.Inventory.LoadBySlotAndType(
                                    slot: (byte)EquipmentType.SecondaryWeapon,
                                    type: InventoryType.Wear);
                                break;
                        }

                        break;
                }
            }
            else
            {
                weapon = character.Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.SecondaryWeapon,
                    type: InventoryType.Wear);
                switch (character.Class)
                {
                    case ClassType.Adventurer:
                    case ClassType.Swordsman:
                        AttackType = AttackType.Melee;
                        break;

                    case ClassType.Archer:
                        AttackType = AttackType.Range;
                        break;

                    case ClassType.Magician:
                        AttackType = AttackType.Magical;
                        break;
                }
            }

            if (weapon != null)
            {
                AttackUpgrade = weapon.Upgrade;
                WeaponDamageMinimum = weapon.DamageMinimum + weapon.Item.DamageMinimum;
                WeaponDamageMaximum = weapon.DamageMaximum + weapon.Item.DamageMaximum;

                if (weapon == character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.MainWeapon,
                    type: InventoryType.Wear)
                )
                    ShellWeaponEffects = character.ShellEffectMain.ToList();
                else
                    ShellWeaponEffects = character.ShellEffectSecondary.ToList();
            }

            DamageMinimum -= WeaponDamageMinimum;
            DamageMaximum -= WeaponDamageMaximum;

            if (DamageMaximum <= DamageMinimum) DamageMaximum = DamageMinimum + 1;

            var armor = character.Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Armor,
                type: InventoryType.Wear);
            if (armor != null)
            {
                DefenseUpgrade = armor.Upgrade;
                ArmorMeleeDefense = armor.CloseDefence + armor.Item.CloseDefence;
                ArmorRangeDefense = armor.DistanceDefence + armor.Item.DistanceDefence;
                ArmorMagicalDefense = armor.MagicDefence + armor.Item.MagicDefence;

                ShellArmorEffects = character.ShellEffectArmor.ToList();
            }

            MeleeDefense = character.Defence - ArmorMeleeDefense;
            MeleeDefenseDodge = character.DefenceRate;
            RangeDefense = character.DistanceDefence - ArmorRangeDefense;
            RangeDefenseDodge = character.DistanceDefenceRate;
            MagicalDefense = character.MagicalDefence - ArmorMagicalDefense;
            Element = character.Element;
            ElementRate = character.ElementRate + character.ElementRateSP;
        }

        public BattleEntity(Mate mate)
        {
            Mate = mate;

            if (mate.BattleEntity != null)
            {
                Buffs = mate.Buff;
                BuffObservables = mate.BuffObservables;
                BCardDisposables = mate.BattleEntity.BCardDisposables;
                BCards = mate.BattleEntity.BCards;
                OnDeathEvents = mate.OnDeathEvents;
            }
            else
            {
                Buffs = new ThreadSafeSortedList<short, Buff.Buff>();
                BuffObservables = new ThreadSafeSortedList<short, IDisposable>();
                BCardDisposables = new ThreadSafeSortedList<int, IDisposable>();
                BCards = new ThreadSafeGenericLockedList<BCard>(other: mate.Monster.BCards);
                OnDeathEvents = new List<EventContainer>();
            }

            //Add Partner EquipmentBCards

            EntityType = EntityType.Mate;
            UserType = UserType.Npc;
            DamageMinimum = mate.DamageMinimum;
            DamageMaximum = mate.DamageMaximum;
            WeaponDamageMinimum = mate.WeaponInstance?.Item.DamageMinimum ?? 0;
            WeaponDamageMaximum = mate.WeaponInstance?.Item.DamageMaximum ?? 0;
            Hitrate = mate.Concentrate + (mate.WeaponInstance?.Item.HitRate ?? 0);
            CritChance = mate.Monster.CriticalChance + (mate.WeaponInstance?.Item.CriticalLuckRate ?? 0);
            CritRate = mate.Monster.CriticalRate + (mate.WeaponInstance?.Item.CriticalRate ?? 0);
            Morale = mate.Level;
            AttackUpgrade = mate.WeaponInstance?.Upgrade ?? mate.Attack;
            FireResistance = mate.Monster.FireResistance + (mate.GlovesInstance?.FireResistance ?? 0) +
                             (mate.GlovesInstance?.Item.FireResistance ?? 0) +
                             (mate.BootsInstance?.FireResistance ?? 0) + (mate.BootsInstance?.Item.FireResistance ?? 0);
            WaterResistance = mate.Monster.WaterResistance + (mate.GlovesInstance?.WaterResistance ?? 0) +
                              (mate.GlovesInstance?.Item.WaterResistance ?? 0) +
                              (mate.BootsInstance?.WaterResistance ?? 0) +
                              (mate.BootsInstance?.Item.WaterResistance ?? 0);
            LightResistance = mate.Monster.LightResistance + (mate.GlovesInstance?.LightResistance ?? 0) +
                              (mate.GlovesInstance?.Item.LightResistance ?? 0) +
                              (mate.BootsInstance?.LightResistance ?? 0) +
                              (mate.BootsInstance?.Item.LightResistance ?? 0);
            ShadowResistance = mate.Monster.DarkResistance + (mate.GlovesInstance?.DarkResistance ?? 0) +
                               (mate.GlovesInstance?.Item.DarkResistance ?? 0) +
                               (mate.BootsInstance?.DarkResistance ?? 0) +
                               (mate.BootsInstance?.Item.DarkResistance ?? 0);
            AttackType = (AttackType)mate.Monster.AttackClass;

            DefenseUpgrade = mate.ArmorInstance?.Upgrade ?? mate.Defence;
            MeleeDefense = mate.MeleeDefense;
            RangeDefense = mate.RangeDefense;
            MagicalDefense = mate.MagicalDefense;
            MeleeDefenseDodge = (mate.ArmorInstance?.Item.DefenceDodge ?? 0) + mate.MeleeDefenseDodge +
                                (mate.GlovesInstance?.Item.DefenceDodge ?? 0) +
                                (mate.BootsInstance?.Item.DefenceDodge ?? 0);
            RangeDefenseDodge = (mate.ArmorInstance?.Item.DistanceDefenceDodge ?? 0) + mate.RangeDefenseDodge +
                                (mate.GlovesInstance?.Item.DistanceDefenceDodge ?? 0) +
                                (mate.BootsInstance?.Item.DistanceDefenceDodge ?? 0);

            ArmorMeleeDefense = (mate.ArmorInstance?.Item.CloseDefence ?? 0) +
                                (mate.GlovesInstance?.Item.CloseDefence ?? 0) +
                                (mate.BootsInstance?.Item.CloseDefence ?? 0);
            ArmorRangeDefense = (mate.ArmorInstance?.Item.DistanceDefence ?? 0) +
                                (mate.GlovesInstance?.Item.DistanceDefence ?? 0) +
                                (mate.BootsInstance?.Item.DistanceDefence ?? 0);
            ArmorMagicalDefense = (mate.ArmorInstance?.Item.MagicDefence ?? 0) +
                                  (mate.GlovesInstance?.Item.MagicDefence ?? 0) +
                                  (mate.BootsInstance?.Item.MagicDefence ?? 0);

            Element = (byte)(mate.MateType == MateType.Pet ? mate.Monster.Element :
                mate.IsUsingSp ? mate.Sp.Instance.Item.Element : 0);
            ElementRate = mate.Monster.ElementRate;
        }

        public BattleEntity(MapMonster monster)
        {
            MapMonster = monster;

            if (monster.BattleEntity != null)
            {
                Buffs = monster.Buff;
                BuffObservables = monster.BuffObservables;
                BCardDisposables = monster.BattleEntity.BCardDisposables;
                OnDeathEvents = monster.OnDeathEvents;
            }
            else
            {
                Buffs = new ThreadSafeSortedList<short, Buff.Buff>();
                BuffObservables = new ThreadSafeSortedList<short, IDisposable>();
                BCardDisposables = new ThreadSafeSortedList<int, IDisposable>();
                OnDeathEvents = new List<EventContainer>();
            }

            BCards = new ThreadSafeGenericLockedList<BCard>(other: monster.Monster.BCards);

            EntityType = EntityType.Monster;
            UserType = UserType.Monster;
            if (monster.Owner?.Mate != null)
            {
                DamageMinimum = monster.Owner.Mate.DamageMinimum;
                DamageMaximum = monster.Owner.Mate.DamageMaximum;
            }
            else
            {
                DamageMinimum = monster.Monster.DamageMinimum;
                DamageMaximum = monster.Monster.DamageMaximum;
            }

            WeaponDamageMinimum = 0;
            WeaponDamageMaximum = 0;
            Hitrate = monster.Monster.Concentrate;
            CritChance = monster.Monster.CriticalChance;
            CritRate = monster.Monster.CriticalRate;
            Morale = monster.Monster.Level;
            AttackUpgrade = monster.Monster.AttackUpgrade;
            FireResistance = monster.Monster.FireResistance;
            WaterResistance = monster.Monster.WaterResistance;
            LightResistance = monster.Monster.LightResistance;
            ShadowResistance = monster.Monster.DarkResistance;
            AttackType = (AttackType)monster.Monster.AttackClass;
            DefenseUpgrade = monster.Monster.DefenceUpgrade;
            MeleeDefense = monster.Monster.CloseDefence;
            MeleeDefenseDodge = monster.Monster.DefenceDodge;
            RangeDefense = monster.Monster.DistanceDefence;
            RangeDefenseDodge = monster.Monster.DistanceDefenceDodge;
            MagicalDefense = monster.Monster.MagicDefence;
            ArmorMeleeDefense = 0;
            ArmorRangeDefense = 0;
            ArmorMagicalDefense = 0;
            Element = monster.Monster.Element;
            ElementRate = monster.Monster.ElementRate;
            Death = monster.Death;
        }

        public BattleEntity(MapNpc npc)
        {
            MapNpc = npc;

            if (npc.BattleEntity != null)
            {
                Buffs = npc.Buff;
                BuffObservables = npc.BuffObservables;
                BCardDisposables = npc.BattleEntity.BCardDisposables;
                BCards = new ThreadSafeGenericLockedList<BCard>(other: npc.Npc.BCards);
                OnDeathEvents = npc.OnDeathEvents;
            }
            else
            {
                Buffs = new ThreadSafeSortedList<short, Buff.Buff>();
                BuffObservables = new ThreadSafeSortedList<short, IDisposable>();
                BCardDisposables = new ThreadSafeSortedList<int, IDisposable>();
                BCards = new ThreadSafeGenericLockedList<BCard>();
                OnDeathEvents = new List<EventContainer>();
            }

            //npc.Buff.CopyTo(Buffs);
            EntityType = EntityType.Npc;
            UserType = UserType.Npc;
            DamageMinimum = 0;
            DamageMaximum = 0;
            WeaponDamageMinimum = npc.Npc.DamageMinimum;
            WeaponDamageMaximum = npc.Npc.DamageMaximum;
            Hitrate = npc.Npc.Concentrate;
            CritChance = npc.Npc.CriticalChance;
            CritRate = npc.Npc.CriticalRate;
            Morale = npc.Npc.Level;
            AttackUpgrade = npc.Npc.AttackUpgrade;
            FireResistance = npc.Npc.FireResistance;
            WaterResistance = npc.Npc.WaterResistance;
            LightResistance = npc.Npc.LightResistance;
            ShadowResistance = npc.Npc.DarkResistance;
            AttackType = (AttackType)npc.Npc.AttackClass;
            DefenseUpgrade = npc.Npc.DefenceUpgrade;
            MeleeDefense = npc.Npc.CloseDefence;
            MeleeDefenseDodge = npc.Npc.DefenceDodge;
            RangeDefense = npc.Npc.DistanceDefence;
            RangeDefenseDodge = npc.Npc.DistanceDefenceDodge;
            MagicalDefense = npc.Npc.MagicDefence;
            ArmorMeleeDefense = npc.Npc.CloseDefence;
            ArmorRangeDefense = npc.Npc.DistanceDefence;
            ArmorMagicalDefense = npc.Npc.MagicDefence;
            Element = npc.Npc.Element;
            ElementRate = npc.Npc.ElementRate;
        }

        #endregion

        #region Properties

        public Character Character { get; set; }

        public Mate Mate { get; set; }

        public MapMonster MapMonster { get; set; }

        public MapNpc MapNpc { get; set; }

        public double AdditionalHp { get; set; }

        public double AdditionalMp { get; set; }

        public int ArmorDefense { get; set; }

        public int ArmorMagicalDefense { get; }

        public int ArmorMeleeDefense { get; }

        public int ArmorRangeDefense { get; }

        public AttackType AttackType { get; }

        public short AttackUpgrade { get; set; }

        public ThreadSafeGenericLockedList<BCard> BCards { get; }

        public Node[][] BrushFireJagged
        {
            get
            {
                if (Character != null) return Character.BrushFireJagged;
                if (Mate != null) return Mate.BrushFireJagged;
                if (MapMonster != null) return MapMonster.BrushFireJagged;
                if (MapNpc != null) return MapNpc.BrushFireJagged;
                return null;
            }
            set
            {
                if (Character != null) Character.BrushFireJagged = value;
                else if (Mate != null) Mate.BrushFireJagged = value;
                else if (MapMonster != null) MapMonster.BrushFireJagged = value;
                else if (MapNpc != null) MapNpc.BrushFireJagged = value;
            }
        }

        public ThreadSafeSortedList<short, Buff.Buff> Buffs { get; set; }

        public ThreadSafeSortedList<short, IDisposable> BuffObservables { get; set; }

        public ThreadSafeSortedList<int, IDisposable> BCardDisposables { get; set; }

        public ThreadSafeGenericList<CellonOptionDto> CellonOptions { get; set; }

        public int CritChance { get; set; }

        public int CritRate { get; set; }

        public int DamageMaximum { get; }

        public int DamageMinimum { get; }

        public DateTime Death { get; set; }

        public int Defense { get; set; }

        public short DefenseUpgrade { get; set; }

        public int Dodge { get; set; }

        public byte Element { get; }

        public int ElementRate { get; }

        public EntityType EntityType { get; }

        public int FireResistance { get; set; }

        public int Hitrate { get; }

        public int Hp
        {
            get
            {
                if (Character != null) return Character.Hp;
                if (Mate != null) return (int)Mate.Hp;
                if (MapMonster != null) return (int)MapMonster.CurrentHp;
                if (MapNpc != null) return (int)MapNpc.CurrentHp;
                return 0;
            }
            set
            {
                if (Character != null) Character.Hp = value;
                else if (Mate != null) Mate.Hp = value;
                else if (MapMonster != null) MapMonster.CurrentHp = value;
                else if (MapNpc != null) MapNpc.CurrentHp = value;
            }
        }

        public int HpMax
        {
            get
            {
                if (Character != null) return (int)Character.HPLoad();
                if (Mate != null) return (int)Mate.MaxHp;
                if (MapMonster != null) return (int)MapMonster.MaxHp;
                if (MapNpc != null) return (int)MapNpc.MaxHp;
                return 0;
            }
        }

        public bool Invincible { get; set; }

        public DateTime LastDefence
        {
            get
            {
                if (Character != null) return Character.LastDefence;
                if (Mate != null) return Mate.LastDefence;
                if (MapMonster != null) return MapMonster.LastDefence;
                if (MapNpc != null) return MapNpc.LastDefence;
                return new DateTime();
            }
            set
            {
                if (Character != null) Character.LastDefence = value;
                else if (Mate != null) Mate.LastDefence = value;
                else if (MapMonster != null) MapMonster.LastDefence = value;
                else if (MapNpc != null) MapNpc.LastDefence = value;
            }
        }

        public byte Level
        {
            get => Character?.Level ?? Mate?.Level ?? MapMonster?.Monster?.Level ?? MapNpc?.Npc?.Level ?? 0;
        }

        public int LightResistance { get; set; }

        public int MagicalDefense { get; }

        public long MapEntityId
        {
            get
            {
                if (Character != null) return Character.CharacterId;
                if (Mate != null) return Mate.MateTransportId;
                if (MapMonster != null) return MapMonster.MapMonsterId;
                if (MapNpc != null) return MapNpc.MapNpcId;
                return 0;
            }
        }

        public MapInstance MapInstance
        {
            get =>
                Character?.MapInstance ??
                Mate?.Owner?.MapInstance ?? MapMonster?.MapInstance ?? MapNpc?.MapInstance;
        }

        public int MeleeDefense { get; }

        public int MeleeDefenseDodge { get; }

        public int Morale { get; set; }

        public int Mp
        {
            get
            {
                if (Character != null) return Character.Mp;
                if (Mate != null) return (int)Mate.Mp;
                if (MapMonster != null) return (int)MapMonster.CurrentMp;
                if (MapNpc != null) return (int)MapNpc.CurrentMp;
                return 0;
            }
            set
            {
                if (Character != null) Character.Mp = value;
                else if (Mate != null) Mate.Mp = value;
                else if (MapMonster != null) MapMonster.CurrentMp = value;
                else if (MapNpc != null) MapNpc.CurrentMp = value;
            }
        }

        public int MpMax
        {
            get
            {
                if (Character != null) return (int)Character.MPLoad();
                if (Mate != null) return (int)Mate.MaxMp;
                if (MapMonster != null) return (int)MapMonster.MaxMp;
                if (MapNpc != null) return (int)MapNpc.MaxMp;
                return 0;
            }
        }

        public DateTime LastMonsterAggro
        {
            get
            {
                if (Character != null) return Character.LastMonsterAggro;
                if (Mate != null) return Mate.LastMonsterAggro;
                if (MapMonster != null) return MapMonster.LastMonsterAggro;
                if (MapNpc != null) return MapNpc.LastMonsterAggro;
                return new DateTime();
            }
            set
            {
                if (Character != null) Character.LastMonsterAggro = value;
                else if (Mate != null) Mate.LastMonsterAggro = value;
                else if (MapMonster != null) MapMonster.LastMonsterAggro = value;
                else if (MapNpc != null) MapNpc.LastMonsterAggro = value;
            }
        }

        public short PositionX
        {
            get
            {
                if (Character != null) return Character.PositionX;
                if (Mate != null) return Mate.PositionX;
                if (MapMonster != null) return MapMonster.MapX;
                if (MapNpc != null) return MapNpc.MapX;
                return 0;
            }
            set
            {
                if (Character != null) Character.PositionX = value;
                else if (Mate != null) Mate.PositionX = value;
                else if (MapMonster != null) MapMonster.MapX = value;
                else if (MapNpc != null) MapNpc.MapX = value;
            }
        }

        public short PositionY
        {
            get
            {
                if (Character != null) return Character.PositionY;
                if (Mate != null) return Mate.PositionY;
                if (MapMonster != null) return MapMonster.MapY;
                if (MapNpc != null) return MapNpc.MapY;
                return 0;
            }
            set
            {
                if (Character != null) Character.PositionY = value;
                else if (Mate != null) Mate.PositionY = value;
                else if (MapMonster != null) MapMonster.MapY = value;
                else if (MapNpc != null) MapNpc.MapY = value;
            }
        }

        public object PVELockObject
        {
            get
            {
                if (Character != null) return Character.PVELockObject;
                if (Mate != null) return Mate.PveLockObject;
                if (MapMonster != null) return MapMonster.PVELockObject;
                if (MapNpc != null) return MapNpc.PveLockObject;
                return 0;
            }
            set
            {
                if (Character != null) Character.PVELockObject = value;
                else if (Mate != null) Mate.PveLockObject = value;
                else if (MapMonster != null) MapMonster.PVELockObject = value;
                else if (MapNpc != null) MapNpc.PveLockObject = value;
            }
        }

        public int RangeDefense { get; }

        public int RangeDefenseDodge { get; }

        public int Resistance { get; set; }

        public int ShadowResistance { get; set; }

        public List<ShellEffectDto> ShellArmorEffects { get; }

        public List<ShellEffectDto> ShellWeaponEffects { get; }

        public UserType UserType { get; }

        public int WaterResistance { get; set; }

        public int WeaponDamageMaximum { get; }

        public int WeaponDamageMinimum { get; }

        public long FalconFocusedEntityId { get; set; }

        public int ResistForcedMovement
        {
            get =>
                GetBuff(type: CardType.AbsorbedSpirit,
                    subtype: (byte)AdditionalTypes.AbsorbedSpirit.ResistForcedMovement)[0];
        }

        public bool CanBeTargetted
        {
            get =>
                MapInstance.MapInstanceType != MapInstanceType.BaseMapInstance ||
                TargettedByMonstersList(teamCheck: false).Count < MaxTargetedByMonstersCount(teamCheck: false) &&
                TargettedByMonstersList(teamCheck: true).Count < MaxTargetedByMonstersCount(teamCheck: true);
        }

        public List<BattleEntity> TargettedByMonstersList(bool teamCheck)
        {
            if (!teamCheck)
                return MapInstance?.Monsters
                    .Where(predicate: s => s?.Target?.MapEntityId == MapEntityId && s.Target.EntityType == EntityType)
                    .Select(selector: s => s.BattleEntity).ToList();

            var targettedByMonsters = new List<BattleEntity>();
            if (Mate?.Owner != null)
            {
                targettedByMonsters = Mate.Owner.BattleEntity.TargettedByMonstersList(teamCheck: true);
            }
            else
            {
                targettedByMonsters = MapInstance?.Monsters
                    .Where(predicate: s => s?.Target?.MapEntityId == MapEntityId && s.Target.EntityType == EntityType)
                    .Select(selector: s => s.BattleEntity).ToList();
                if (Character != null)
                    Character.Mates.Where(predicate: s => s.IsTeamMember).ToList().ForEach(action: m =>
                        targettedByMonsters.AddRange(collection: MapInstance?.Monsters
                            .Where(predicate: s => s?.Target?.MapEntityId == m.BattleEntity.MapEntityId &&
                                                   s.Target.EntityType == m.BattleEntity.EntityType)
                            .Select(selector: s => s.BattleEntity).ToList()));
            }

            return targettedByMonsters;
        }

        public int MaxTargetedByMonstersCount(bool teamCheck)
        {
            if (MapInstance?.MapInstanceType == MapInstanceType.BaseMapInstance)
            {
                if (teamCheck)
                    switch (EntityType)
                    {
                        case EntityType.Player:
                            return (1 + Character.Mates.Count(predicate: m => m.IsTeamMember || m.IsTemporalMate)) *
                                   5000; //Default = 5

                        case EntityType.Mate:
                            return (1 + Mate.Owner.Mates.Count(predicate: m => m.IsTeamMember || m.IsTemporalMate)) *
                                   5000; //Default = 5
                    }
                else
                    return 5000; //Default = 5
            }

            return int.MaxValue;
        }

        #endregion

        #region Methods

        public int GetDistance(BattleEntity other)
        {
            return (int)Math.Sqrt(d: Math.Pow(x: other.PositionX - PositionX, y: 2) +
                                      Math.Pow(x: other.PositionY - PositionY, y: 2));
        }

        public int HpPercent()
        {
            return (int)(Hp / (double)HpMax * 100D);
        }

        public int MpPercent()
        {
            return (int)(Mp / (double)MpMax * 100D);
        }

        public MapCell GetRandomMapCellInRange(short numberOfCells)
        {
            if (MapInstance?.Map == null) return null;

            var walkableCellsInRange = new List<MapCell>();

            while (numberOfCells > 0)
            {
                for (var dX = -1; dX <= 1; dX++)
                    for (var dY = -1; dY <= 1; dY++)
                    {
                        if (dX == 0 && dY == 0) continue;

                        var x = (short)(PositionX + dX * numberOfCells);
                        var y = (short)(PositionY + dY * numberOfCells);

                        if (!MapInstance.Map.IsBlockedZone(x: x, y: y) && MapInstance.Map.CanWalkAround(x: x, y: y))
                            walkableCellsInRange.Add(item: new MapCell { X = x, Y = y });
                    }

                numberOfCells--;
            }

            return walkableCellsInRange
                .OrderBy(keySelector: s => ServerManager.RandomNumber(min: 0, max: 1000))
                .FirstOrDefault();
        }

        public void AddBuff(Buff.Buff indicator, BattleEntity sender, bool noMessage = false, short x = 0, short y = 0,
            bool forced = false)
        {
            if (indicator.Card != null)
            {
                indicator.Level = sender.MapMonster?.Owner?.Level ?? sender.Level;

                indicator.Sender = sender;

                if (Character != null && (indicator.Card.BuffType == BuffType.Bad && Character.HasGodMode ||
                                          Character.InvisibleGm)
                    || Mate != null && (indicator.Card.BuffType == BuffType.Bad && Mate.Owner.HasGodMode ||
                                        Mate.Owner.InvisibleGm))
                    return;

                if (MapMonster != null &&
                    (MapMonster.IsBoss || ServerManager.Instance.BossVNums.Contains(item: MapMonster.MonsterVNum)))
                    if (!forced && indicator.Card.BuffType == BuffType.Bad &&
                        (indicator.Card.BCards.Any(predicate: b =>
                             b.Type == (byte)CardType.SpecialAttack &&
                             b.SubType == (byte)AdditionalTypes.SpecialAttack.NoAttack / 10)
                         || indicator.Card.BCards.Any(predicate: b =>
                             b.Type == (byte)CardType.Move &&
                             b.SubType == (byte)AdditionalTypes.Move.MovementImpossible / 10)
                         || indicator.Card.BCards.Any(predicate: b =>
                             b.Type == (byte)CardType.Move &&
                             b.SubType == (byte)AdditionalTypes.Move.SetMovement / 10)
                         || indicator.Card.BCards.Any(predicate: b =>
                             b.Type == (byte)CardType.Move &&
                             b.SubType == (byte)AdditionalTypes.Move.MovementSpeedDecreased / 10)
                         || indicator.Card.BCards.Any(predicate: b =>
                             b.Type == (byte)CardType.Move &&
                             b.SubType == (byte)AdditionalTypes.Move.MoveSpeedDecreased / 10)))
                        return;

                if (indicator.Card.BCards.Any(predicate: newbuff => Buffs.GetAllItems().Any(predicate: b =>
                    b.Card.BCards.Any(predicate: buff =>
                        buff.CardId != newbuff.CardId
                        && (buff.Type == 33 && buff.SubType == 5 &&
                            (newbuff.Type == 33 || newbuff.Type == 58) || newbuff.Type == 33 && newbuff.SubType == 5 &&
                                                                       (buff.Type == 33 || buff.Type == 58)
                                                                       || buff.Type == 33 &&
                                                                       (buff.SubType == 1 || buff.SubType == 3) &&
                                                                       newbuff.Type == 58 && newbuff.SubType == 1 ||
                                                                       buff.Type == 33 &&
                                                                       (buff.SubType == 2 || buff.SubType == 4) &&
                                                                       newbuff.Type == 58 && newbuff.SubType == 3
                                                                       || newbuff.Type == 33 &&
                                                                       (newbuff.SubType == 1 || newbuff.SubType == 3) &&
                                                                       buff.Type == 58 && buff.SubType == 1 ||
                                                                       newbuff.Type == 33 &&
                                                                       (newbuff.SubType == 2 || newbuff.SubType == 4) &&
                                                                       buff.Type == 58 && buff.SubType == 3
                                                                       || buff.Type == 33 && newbuff.Type == 33 &&
                                                                       buff.SubType == newbuff.SubType ||
                                                                       buff.Type == 58 && newbuff.Type == 58 &&
                                                                       buff.SubType == newbuff.SubType)))))
                    return;

                switch (indicator.Card.CardId)
                {
                    case 272:
                    case 273:
                    case 274:
                        if (Buffs.Any(predicate: s => new short[] { 272, 273, 274 }.Contains(value: s.Card.CardId)))
                            return;
                        break;
                    case 75:
                    case 28:
                    case 29:
                        if (Buffs.Any(predicate: s => s.Card.CardId == 153)) return;
                        break;
                    case 153:
                        RemoveBuff(id: 28);
                        RemoveBuff(id: 29);
                        RemoveBuff(id: 75);
                        break;
                    case 475: // Devil's Blessing
                        {
                            if (MapInstance?.MapInstanceType == MapInstanceType.RaidInstance
                                && MapMonster?.MonsterVNum == 2326 /* Witch Laurena */)
                            {
                                MapInstance.InstanceBag.LaurenaRound++;

                                MapInstance.Broadcast(packet: StaticPacketHelper.Say(type: 3, callerId: MapEntityId,
                                    secondaryType: 1,
                                    message: Language.Instance.GetMessageFromKey(key: "GET_OVER_HERE")));
                                MapInstance.Broadcast(packet: $"npc_req 3 {MapEntityId} 9685");

                                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1))
                                    .Subscribe(onNext: observer =>
                                    {
                                        if (MapInstance != null)
                                        {
                                            MapInstance.Broadcast(packet: $"ca_t {MapEntityId} 2000");
                                            TeleportTo(mapCell: new MapCell { X = 53, Y = 59 });
                                            MapInstance.Broadcast(packet: $"guri 11 3 {MapEntityId}");

                                            var monstersToSummon = new List<MonsterToSummon>();

                                        // Increase n by 1 every round -- max. 2
                                        var n = Math.Min(val1: MapInstance.InstanceBag.LaurenaRound, val2: 2);

                                            for (var i = 0; i < n; i++)
                                            {
                                                var spawnCell =
                                                    GetRandomMapCellInRange(numberOfCells: 20) ?? new MapCell
                                                    { X = PositionX, Y = PositionY };

                                                monstersToSummon.Add(item: new MonsterToSummon(vnum: 2327,
                                                    spawnCell: spawnCell, target: null, move: true)
                                                {
                                                    DeathEvents =
                                                    {
                                                    new EventContainer(mapInstance: MapInstance,
                                                        eventActionType: EventActionType.Removelaurenabuff,
                                                        param: this)
                                                    }
                                                });
                                            }

                                            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: MapInstance,
                                                eventActionType: EventActionType.Spawnmonsters, param: monstersToSummon));
                                        }
                                    });

                                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 3))
                                    .Subscribe(onNext: observer => { MapInstance?.Broadcast(packet: "npc_req -1 -1"); });
                            }
                        }
                        break;
                    case 532:
                        RemoveBuff(id: 533);
                        RemoveBuff(id: 534);
                        break;
                    case 533:
                        RemoveBuff(id: 532);
                        RemoveBuff(id: 534);
                        break;
                    case 534:
                        RemoveBuff(id: 532);
                        RemoveBuff(id: 533);
                        break;
                    case 562:
                        RemoveBuff(id: 567);
                        break;
                    case 563:
                        RemoveBuff(id: 568);
                        break;
                    case 564:
                        RemoveBuff(id: 562);
                        break;
                    case 565:
                        RemoveBuff(id: 563);
                        break;
                    case 567:
                        RemoveBuff(id: 562);
                        RemoveBuff(id: 563);
                        RemoveBuff(id: 564);
                        RemoveBuff(id: 565);
                        RemoveBuff(id: 568);
                        break;
                    case 568:
                        RemoveBuff(id: 562);
                        RemoveBuff(id: 563);
                        RemoveBuff(id: 564);
                        RemoveBuff(id: 565);
                        RemoveBuff(id: 567);
                        break;
                    case 589:
                        sender?.AddBuff(indicator: new Buff.Buff(id: 592, level: sender.Level), sender: sender);
                        break;
                    case 601:
                        RemoveBuff(id: 602);
                        RemoveBuff(id: 603);
                        break;
                    case 602:
                        RemoveBuff(id: 601);
                        RemoveBuff(id: 603);
                        break;
                    case 603:
                        RemoveBuff(id: 601);
                        RemoveBuff(id: 602);
                        break;
                    case 608:
                        RemoveBuff(id: 617); // Magic Spell
                        RemoveBuff(id: 609); // Fire
                        RemoveBuff(id: 610); // Ice
                        RemoveBuff(id: 611); // Light
                        RemoveBuff(id: 612); // No Element
                        RemoveBuff(id: 613); // Dark
                        break;
                    case 617:
                        RemoveBuff(id: 608); // Magical Fetters
                        break;
                }

                indicator.Card.BCards.ForEach(action: b => BCardDisposables[key: b.BCardId]?.Dispose());
                if (Buffs[key: indicator.Card.CardId] is Buff.Buff oldBuff) Buffs.Remove(key: indicator.Card.CardId);

                Buffs[key: indicator.Card.CardId] = indicator;

                var buffTime = 0;
                var amuletMaxDurability = 0;
                if (Character != null)
                {
                    if (indicator.Card.CardId == 85 && indicator.Card.Duration == 0)
                    {
                        buffTime = ServerManager.RandomNumber(min: 50, max: 350);
                    }
                    else if (indicator.Card.CardId == 559 && indicator.Card.Duration == 0)
                    {
                        buffTime = ServerManager.RandomNumber(min: 250, max: 450);
                    }
                    else if (indicator.Card.CardId == 336 && indicator.Card.Duration == 0)
                    {
                        if (Character.VehicleItem != null)
                            buffTime = Character.VehicleItem.SpeedBoostDuration * 10;
                        else
                            buffTime = ServerManager.RandomNumber(min: 30, max: 70);
                    }
                    else if (indicator.Card.CardId == 0)
                    {
                        buffTime = Character.ChargeValue > 7000 ? 7000 : Character.ChargeValue;
                    }
                    else if (indicator.Card.CardId == 562 || indicator.Card.CardId == 563)
                    {
                        buffTime = 400;
                    }

                    indicator.RemainingTime = indicator.Card.Duration == 0 ? buffTime : indicator.Card.Duration;

                    // Amulet remaining time
                    if (indicator.Card.CardId == 62)
                    {
                        var amulet =
                            Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Amulet,
                                type: InventoryType.Wear);
                        if (amulet?.ItemDeleteTime != null)
                        {
                            buffTime = (int)amulet.ItemDeleteTime.Value.Subtract(value: DateTime.Now).TotalSeconds *
                                       10;
                            indicator.RemainingTime = buffTime;
                        }
                        else if (amulet?.DurabilityPoint > 0)
                        {
                            amuletMaxDurability = amulet.Item.EffectValue;
                            buffTime = amulet.DurabilityPoint;
                            indicator.RemainingTime = buffTime;
                        }
                    }

                    indicator.Start = DateTime.Now;

                    Character.Session.SendPacket(
                        packet:
                        $"bf 1 {MapEntityId} {(indicator.Card.CardId == 0 ? Character.ChargeValue > 7000 ? 7000 : Character.ChargeValue : amuletMaxDurability > 0 ? buffTime : 0)}.{indicator.Card.CardId}.{(indicator.Card.Duration == 0 || indicator.Card.CardId == 62 ? amuletMaxDurability > 0 ? amuletMaxDurability : buffTime : indicator.Card.Duration)} {sender.Level}");

                    if (!noMessage || !Buffs.Any(predicate: s => s.Card.CardId == indicator.Card.CardId))
                        Character.Session.SendPacket(packet: Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "UNDER_EFFECT"),
                                arg0: indicator.Card.Name),
                            type: 20));

                    Character.Session.SendPacket(packet: Character.GenerateStat());

                    if (Mate != null) Mate.Owner.Session.SendPackets(packets: Mate.Owner.GeneratePst());
                }

                if (BuffObservables.ContainsKey(key: indicator.Card.CardId))
                {
                    BuffObservables[key: indicator.Card.CardId]?.Dispose();
                    BuffObservables.Remove(key: indicator.Card.CardId);
                }

                indicator.Card.BCards.ForEach(action: c => c.ApplyBCards(session: this, sender: sender, x: x, y: y));

                if (indicator.Card.BCards.Any(predicate: s =>
                    s.Type == (byte)CardType.Move &&
                    !s.SubType.Equals(obj: (byte)AdditionalTypes.Move.MovementImpossible / 10)))
                {
                    if (Character != null)
                    {
                        Character.LoadSpeed();
                        Character.Session.SendPacket(packet: Character.GenerateCond());
                        Character.LastSpeedChange = DateTime.Now;
                    }
                    else if (Mate != null)
                    {
                        Mate.Owner.Session.SendPacket(packet: Mate.GenerateCond());
                    }
                }

                if (indicator.Card.BCards.Any(predicate: s
                    => s.Type == (byte)CardType.SpecialAttack &&
                       s.SubType == (byte)AdditionalTypes.SpecialAttack.NoAttack / 10
                       || s.Type == (byte)CardType.Move &&
                       s.SubType == (byte)AdditionalTypes.Move.MovementImpossible / 10
                       || s.Type == (byte)CardType.FrozenDebuff &&
                       s.SubType == (byte)AdditionalTypes.FrozenDebuff.EternalIce / 10
                ))
                {
                    if (Character != null) Character.Session.SendPacket(packet: Character.GenerateCond());
                    if (Mate != null) Mate.Owner.Session.SendPacket(packet: Mate.GenerateCond());
                }

                if (indicator.Card.CardId == 518)
                    MapInstance?.Broadcast(packet: $"eff {(byte)UserType} {MapEntityId} 4537");

                MapInstance?.Broadcast(packet: $"bf_e {(short)UserType} {MapEntityId} {indicator.Card.CardId} 100");

                BuffObservables[key: indicator.Card.CardId] = Observable
                    .Timer(dueTime: TimeSpan.FromMilliseconds(
                        value: (indicator.Card.Duration == 0 || indicator.Card.CardId == 62
                            ? buffTime
                            : indicator.Card.Duration) * 100)).Subscribe(onNext: o =>
                    {
                        if (indicator.Card.CardId != 0 && amuletMaxDurability == 0)
                        {
                            RemoveBuff(id: indicator.Card.CardId);
                            if (indicator.Card.TimeoutBuff != 0 &&
                                ServerManager.RandomNumber() < indicator.Card.TimeoutBuffChance)
                                AddBuff(
                                    indicator: new Buff.Buff(id: indicator.Card.TimeoutBuff, level: indicator.Level),
                                    sender: sender);
                        }
                    });
            }
        }

        public void SendBuffsPacket()
        {
            if (Character != null)
                foreach (var indicator in Buffs.GetAllItems())
                    if (indicator.StaticBuff)
                        Character.Session.SendPacket(
                            packet: $"vb {indicator.Card.CardId} 1 {indicator.RemainingTime * 10}");
                    else
                        Character.Session.SendPacket(
                            packet:
                            $"bf 1 {MapEntityId} {(indicator.Card.CardId == 0 ? Character.ChargeValue > 7000 ? 7000 : Character.ChargeValue : 0)}.{indicator.Card.CardId}.{indicator.RemainingTime} {indicator.Level}");
        }

        public bool HasBuff(short cardId)
        {
            return Buffs.GetAllItems().Any(predicate: b => b?.Card?.CardId == cardId);
        }

        public bool HasBuff(CardType type, byte subtype, bool castTypeNotZero = false)
        {
            try
            {
                var bcards = Buffs.GetAllItems().SelectMany(selector: s => s.Card.BCards).ToList();

                bcards.AddRange(collection: BCards.ToList());

                return subtype % 10 == 1
                    ? bcards.Any(predicate: s =>
                        (!castTypeNotZero || s.CastType != 0) && s.Type.Equals(obj: (byte)type) &&
                        s.SubType.Equals(obj: (byte)(subtype / 10)) && s.FirstData >= 0)
                    : bcards.Any(predicate: s =>
                        (!castTypeNotZero || s.CastType != 0) && s.Type.Equals(obj: (byte)type) &&
                        s.SubType.Equals(obj: (byte)(subtype / 10))
                        && (s.FirstData <= 0 || s.ThirdData < 0));
            }
            catch (Exception ex)
            {
                Logger.LogEventError(logEvent: "HASBUFF",
                    data: "Error on HasBuff(CardType type, byte subtype, bool castTypeNotZero = false) method", ex: ex);
                return false;
            }
        }

        public void RemoveBuff(short id, bool removePermaBuff = false)
        {
            if (!Buffs.ContainsKey(key: id)) return;

            var indicator = Buffs[key: id];

            if (indicator?.Card != null)
                lock (indicator)
                {
                    if (indicator.IsPermaBuff && !removePermaBuff)
                    {
                        AddBuff(indicator: indicator, sender: this, noMessage: true);
                        return;
                    }

                    Buffs.Remove(key: id);

                    if (indicator.Card.BCards.Any(predicate: s
                        => s.Type == (byte)CardType.SpecialAttack &&
                           s.SubType == (byte)AdditionalTypes.SpecialAttack.NoAttack / 10
                           || s.Type == (byte)CardType.Move &&
                           s.SubType == (byte)AdditionalTypes.Move.MovementImpossible / 10
                           || s.Type == (byte)CardType.FrozenDebuff &&
                           s.SubType == (byte)AdditionalTypes.FrozenDebuff.EternalIce / 10
                    ))
                    {
                        if (Character != null)
                        {
                            Character.LastSpeedChange = DateTime.Now;
                            Character.LoadSpeed();
                            Character.Session?.SendPacket(packet: Character.GenerateCond());
                        }
                        else if (Mate != null)
                        {
                            Mate.Owner?.Session?.SendPacket(packet: Mate.GenerateCond());
                        }
                    }

                    if (indicator.Card.BCards.Any(predicate: s =>
                            s.Type == (byte)CardType.SpecialEffects &&
                            s.SubType == (byte)AdditionalTypes.SpecialEffects.ShadowAppears / 10)
                        && GetBuff(type: CardType.SpecialEffects,
                            subtype: (byte)AdditionalTypes.SpecialEffects.ShadowAppears) is int[] BuffData)
                        MapInstance?.Broadcast(
                            packet: $"guri 0 {(short)UserType} {MapEntityId} {BuffData[0]} {BuffData[1]}");

                    MapInstance?.Broadcast(packet: $"bf_e {(short)UserType} {MapEntityId} {indicator.Card.CardId} 0");

                    if (Character != null)
                    {
                        if (indicator.StaticBuff)
                        {
                            Character.Session?.SendPacket(
                                packet: $"vb {indicator.Card.CardId} 0 {indicator.Card.Duration}");
                            Character.Session?.SendPacket(packet: Character.GenerateSay(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "EFFECT_TERMINATED"),
                                    arg0: indicator.Card.Name), type: 11));
                        }
                        else
                        {
                            Character.Session?.SendPacket(
                                packet: $"bf 1 {Character.CharacterId} 0.{indicator.Card.CardId}.0 {Level}");
                            Character.Session?.SendPacket(packet: Character.GenerateSay(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "EFFECT_TERMINATED"),
                                    arg0: indicator.Card.Name), type: 20));
                        }

                        if (Buffs[key: indicator.Card.CardId] != null) Buffs.Remove(key: id);
                        if (indicator.Card.BCards.Any(predicate: s =>
                            s.Type == (byte)CardType.Move &&
                            !s.SubType.Equals(obj: (byte)AdditionalTypes.Move.MovementImpossible / 10)))
                        {
                            Character.LastSpeedChange = DateTime.Now;
                            Character.LoadSpeed();
                            Character.Session?.SendPacket(packet: Character.GenerateCond());
                        }

                        if (indicator.Card.BCards.Any(predicate: s =>
                                s.Type == (byte)CardType.SpecialActions &&
                                s.SubType.Equals(obj: (byte)AdditionalTypes.SpecialActions.Hide / 10))
                            || indicator.Card.BCards.Any(predicate: s =>
                                s.Type == (byte)CardType.FalconSkill &&
                                s.SubType.Equals(obj: (byte)AdditionalTypes.FalconSkill.Hide / 10))
                            || indicator.Card.BCards.Any(predicate: s =>
                                s.Type == (byte)CardType.FalconSkill &&
                                s.SubType.Equals(obj: (byte)AdditionalTypes.FalconSkill.Ambush / 10)))
                        {
                            Character.Invisible = false;
                            foreach (var teamMate in Character.Mates?.Where(predicate: m => m != null && m.IsTeamMember)
                            )
                            {
                                teamMate.PositionX = Character.PositionX;
                                teamMate.PositionY = Character.PositionY;
                                teamMate.UpdateBushFire();

                                if (Character.MapInstance?.Sessions != null)
                                    Parallel.ForEach(
                                        source: Character.MapInstance.Sessions.Where(predicate: s =>
                                            s?.Character != null),
                                        body: s =>
                                        {
                                            if (ServerManager.Instance.ChannelId != 51 ||
                                                Character.Faction == s.Character.Faction)
                                                s.SendPacket(packet: teamMate.GenerateIn(hideNickname: false,
                                                    isAct4: ServerManager.Instance.ChannelId == 51));
                                            else
                                                s.SendPacket(packet: teamMate.GenerateIn(hideNickname: true,
                                                    isAct4: ServerManager.Instance.ChannelId == 51,
                                                    receiverAuthority: s.Account.Authority));
                                        });

                                Character.Session?.SendPacket(packet: Character.GeneratePinit());
                                Character.Mates?.ForEach(action: s =>
                                    Character.Session?.SendPacket(packet: s.GenerateScPacket()));
                                Character.Session?.SendPackets(packets: Character.GeneratePst());
                            }

                            MapInstance?.Broadcast(packet: Character.GenerateInvisible());
                        }

                        if (indicator.Card.BCards.Any(predicate: s =>
                            s.Type == (byte)CardType.FearSkill &&
                            s.SubType.Equals(obj: (byte)AdditionalTypes.FearSkill.MoveAgainstWill / 10)))
                            Character.Session?.SendPacket(packet: $"rv_m {MapEntityId} 1 0");
                        if (indicator.Card.BCards.Any(predicate: s =>
                            s.Type == (byte)CardType.FearSkill &&
                            s.SubType.Equals(obj: (byte)AdditionalTypes.FearSkill.AttackRangedIncreased / 10)))
                            if (!Buffs.Any(predicate: s => s.Card.BCards.Any(predicate: b =>
                                b.Type == (byte)CardType.FearSkill &&
                                b.SubType.Equals(obj: (byte)AdditionalTypes.FearSkill.AttackRangedIncreased / 10))))
                                Character.Session?.SendPacket(packet: "bf_d 0 1");

                        if (indicator.Card.BCards.Any(predicate: s => s.Type == (byte)CardType.DarkCloneSummon
                                                                      && s.SubType == (byte)AdditionalTypes
                                                                          .DarkCloneSummon
                                                                          .ConvertDamageToHpChance / 10))
                        {
                            GetDamage(damage: Character.ConvertedDamageToHP, damager: this, dontKill: true);
                            Character.ConvertedDamageToHP = 0;
                            Character.Session?.SendPacket(packet: Character.GenerateStat());
                        }

                        // TODO : Find another way because it is hardcode

                        switch (indicator.Card.CardId)
                        {
                            case 131:
                                Character.Session?.SendPacket(packet: Character.GeneratePairy());
                                break;

                            case 340:
                                if (MapInstance?.Map?.MapTypes != null
                                    && MapInstance.Map.MapTypes.Any(predicate: s =>
                                        s.MapTypeId == (short)MapTypeEnum.Act52))
                                {
                                    Character.AddStaticBuff(staticBuff: new StaticBuffDto
                                    {
                                        CardId = 339,
                                        CharacterId = Character.CharacterId,
                                        RemainingTime = -1
                                    });
                                    Character.Session?.SendPacket(
                                        packet: UserInterfaceHelper.GenerateInfo(
                                            message: Language.Instance.GetMessageFromKey(
                                                key: "ENCASED_BURNING_SWORD")));
                                }

                                break;

                            case 617:
                                {
                                    Character.LastComboCastId = 0;
                                    Character.Session?.SendPacket(packet: "ms_c 1");
                                }
                                break;

                            case 620:
                                {
                                    if (Character.Session != null && Character.SavedLocation != null &&
                                        indicator.Sender?.Character?.CharacterId == Character.CharacterId)
                                    {
                                        Character.SavedLocation = null;

                                        var characterSkill = indicator.SkillVNum.HasValue
                                            ? Character.GetSkill(skillVNum: indicator.SkillVNum.Value)
                                            : null;

                                        var skill = characterSkill?.Skill;

                                        if (skill != null)
                                        {
                                            short cooldown = 600; // 60 seconds * 10

                                            Character.Session.SendPacket(
                                                packet: StaticPacketHelper.SkillResetWithCoolDown(castId: skill.CastId,
                                                    cooldown: cooldown));

                                            characterSkill.LastUse = DateTime.Now.AddMilliseconds(value: cooldown * 100);

                                            Observable.Timer(dueTime: characterSkill.LastUse).Subscribe(onNext: s =>
                                                Character.Session.SendPacket(
                                                    packet: StaticPacketHelper.SkillReset(castId: skill.CastId)));
                                        }
                                    }
                                }
                                break;
                        }
                    }

                    if (BuffObservables != null && BuffObservables.ContainsKey(key: indicator.Card.CardId))
                    {
                        BuffObservables[key: indicator.Card.CardId]?.Dispose();
                        BuffObservables.Remove(key: indicator.Card.CardId);
                    }

                    indicator.Card.BCards.ForEach(action: b => BCardDisposables[key: b.BCardId]?.Dispose());
                    indicator.StaticVisualEffect?.Dispose();
                }
        }

        public void DisableBuffs(BuffType type, int level = 100)
        {
            if (type == BuffType.All || type == BuffType.Good) ClearSacrificeBuff();

            var BuffsCopy = new List<Buff.Buff>();

            lock (Buffs)
            {
                BuffsCopy = Buffs.GetAllItems();
            }

            var buff = BuffsCopy.Where(predicate: s =>
                (type == BuffType.All || s.Card.BuffType == type) && !s.StaticBuff && s.Card.Level < level &&
                s.Card.CardId != 62).ToList();

            buff.ForEach(action: s =>
            {
                s.Card.BCards.ForEach(action: b =>
                {
                    if (BCardDisposables?.ContainsKey(key: b.BCardId) == true &&
                        BCardDisposables[key: b.BCardId] != null)
                    {
                        BCardDisposables[key: b.BCardId]?.Dispose();
                        BCardDisposables[key: b.BCardId] = null;
                    }
                });

                if (BuffObservables != null && BuffObservables.ContainsKey(key: s.Card.CardId))
                {
                    BuffObservables[key: s.Card.CardId]?.Dispose();
                    BuffObservables.Remove(key: s.Card.CardId);
                }

                RemoveBuff(id: s.Card.CardId);
            });

            if (type == BuffType.All)
            {
                var StaticBuffsBCardDisposables = new ThreadSafeSortedList<int, IDisposable>();

                Buffs.Where(predicate: s => s.StaticBuff)
                    .SelectMany(selector: s => s.Card.BCards)
                    .Where(predicate: s => BCardDisposables.ContainsKey(key: s.BCardId)).ToList()
                    .ForEach(
                        action: s => StaticBuffsBCardDisposables[key: s.BCardId] = BCardDisposables[key: s.BCardId]);

                BCardDisposables.GetAllItems().Except(second: StaticBuffsBCardDisposables.GetAllItems()).ToList()
                    .ForEach(action: s => { s?.Dispose(); });

                BCardDisposables = StaticBuffsBCardDisposables;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="types"></param>
        /// <param name="level"></param>
        public void DisableBuffs(List<BuffType> types, int level = 100)
        {
            types.ForEach(action: bt => DisableBuffs(type: bt, level: level));
        }

        public int[] GetBuff(CardType type, byte subtype, int secondData = -1)
        {
            var value1 = 0;
            var value2 = 0;
            var value3 = 0;

            foreach (var entry in BCards.Where(predicate: s =>
                s?.Type.Equals(obj: (byte)type) == true && s.SubType.Equals(obj: (byte)(subtype / 10)) &&
                (secondData == -1 || s.SecondData == secondData)))
            {
                if (entry.IsLevelScaled)
                {
                    if (entry.IsLevelDivided)
                        value1 += Level / entry.FirstData;
                    else
                        value1 += entry.FirstData * Level;
                }
                else
                {
                    value1 += entry.FirstData;
                }

                value2 += entry.SecondData;
                value3 += entry.ThirdData;
            }

            lock (Buffs)
            {
                foreach (var buff in Buffs.GetAllItems())
                    // THIS ONE DOES NOT FOR STUFFS

                    foreach (var entry in buff.Card.BCards
                        .Where(predicate: s => s.Type.Equals(obj: (byte)type) &&
                                               s.SubType.Equals(obj: (byte)(subtype / 10)) &&
                                               (secondData == -1 || s.SecondData == secondData) &&
                                               (s.CastType != 1 || s.CastType == 1 &&
                                                   buff.Start.AddMilliseconds(value: buff.Card.Delay * 100) <
                                                   DateTime.Now)))
                    {
                        if (entry.IsLevelScaled)
                        {
                            if (entry.IsLevelDivided)
                                value1 += buff.Level / entry.FirstData;
                            else
                                value1 += entry.FirstData * buff.Level;
                        }
                        else
                        {
                            value1 += entry.FirstData;
                        }

                        value2 += entry.SecondData;
                        value3 += entry.ThirdData;
                    }
            }

            if (Character != null && Character.Skills != null)
            {
                var PassiveSkillsBCards =
                    PassiveSkillHelper.Instance.PassiveSkillToBCards(
                        skills: Character.Skills.Where(predicate: s => s.Skill.SkillType == 0));
                foreach (var entry in PassiveSkillsBCards.Where(predicate: s =>
                    s?.Type.Equals(obj: (byte)type) == true && s.SubType.Equals(obj: (byte)(subtype / 10)) &&
                    (secondData == -1 || s.SecondData == secondData)))
                {
                    if (entry.IsLevelScaled)
                    {
                        if (entry.IsLevelDivided)
                            value1 += Level / entry.FirstData;
                        else
                            value1 += entry.FirstData * Level;
                    }
                    else
                    {
                        value1 += entry.FirstData;
                    }

                    value2 += entry.SecondData;
                    value3 += entry.ThirdData;
                }
            }

            return new[] { value1, value2, value3 };
        }

        public List<MapMonster> GetOwnedMonsters()
        {
            var ownedMonsters = MapInstance?.Monsters.Where(predicate: m =>
                m.Owner?.MapEntityId == MapEntityId || Character != null &&
                Character.Mates.Any(predicate: mate => m.Owner?.MapEntityId == mate.MateTransportId)).ToList();
            return ownedMonsters;
        }

        public void RemoveOwnedMonsters(bool OnlyFirst = false, int MonsterVNum = -1)
        {
            var ownedMonsters = GetOwnedMonsters()?.Where(predicate: m =>
                MonsterVNum == -1 && !IsMateTrainer(vnum: m.MonsterVNum) || m.MonsterVNum == MonsterVNum);
            if (ownedMonsters != null)
            {
                if (OnlyFirst)
                {
                    if (ownedMonsters.LastOrDefault() is MapMonster first)
                    {
                        first.MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                            callerId: first.MapMonsterId));
                        first.MapInstance.RemoveMonster(monsterToRemove: first);
                    }
                }
                else
                {
                    ownedMonsters.ToList().ForEach(action: m =>
                    {
                        m.MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Monster,
                            callerId: m.MapMonsterId));
                        m.MapInstance.RemoveMonster(monsterToRemove: m);
                    });
                }
            }
        }

        public List<MapNpc> GetOwnedNpcs()
        {
            var ownedNpcs = MapInstance?.Npcs.Where(predicate: m =>
                m.Owner?.MapEntityId == MapEntityId || Character != null &&
                Character.Mates.Any(predicate: mate => m.Owner?.MapEntityId == mate.MateTransportId)).ToList();
            return ownedNpcs;
        }

        public void RemoveOwnedNpcs(bool OnlyFirst = false, int NpcVNum = -1)
        {
            var ownedNpcs = GetOwnedNpcs()?.Where(predicate: m => NpcVNum == -1 || m.NpcVNum == NpcVNum);
            if (ownedNpcs != null)
            {
                if (OnlyFirst)
                {
                    if (ownedNpcs.LastOrDefault() is MapNpc first)
                    {
                        first.MapInstance.Broadcast(packet: StaticPacketHelper.Out(type: UserType.Npc,
                            callerId: first.MapNpcId));
                        first.MapInstance.RemoveNpc(npcToRemove: first);
                    }
                }
                else
                {
                    ownedNpcs.ToList().ForEach(action: m =>
                    {
                        m.MapInstance.Broadcast(
                            packet: StaticPacketHelper.Out(type: UserType.Npc, callerId: m.MapNpcId));
                        m.MapInstance.RemoveNpc(npcToRemove: m);
                    });
                }
            }
        }

        public void ClearEnemyFalcon()
        {
            MapInstance.BattleEntities.Where(predicate: s => s.FalconFocusedEntityId == MapEntityId).ToList()
                .ForEach(action: s => { s.ClearOwnFalcon(); });
        }

        public void ClearOwnFalcon()
        {
            if (FalconFocusedEntityId != 0)
            {
                if (MapInstance.BattleEntities.FirstOrDefault(predicate: s => s.MapEntityId == FalconFocusedEntityId) is
                    BattleEntity FalconFocusedEntity)
                    MapInstance.Broadcast(
                        packet: $"eff_ob  {(byte)FalconFocusedEntity.UserType} {FalconFocusedEntityId} 0 4269");
                FalconFocusedEntityId = 0;
            }
        }

        public void ClearSacrificeBuff()
        {
            if (Buffs.FirstOrDefault(predicate: s => s.Card.BCards.Any(predicate: b =>
                    b.Type.Equals(obj: (byte)CardType.DamageConvertingSkill) &&
                    b.SubType.Equals(obj: (byte)AdditionalTypes.DamageConvertingSkill.TransferInflictedDamage / 10)))
                ?.Sender is BattleEntity SacrificeSender) SacrificeSender.RemoveBuff(id: 546);
            RemoveBuff(id: 531);
        }

        public MapCell GetPos()
        {
            return new MapCell { X = PositionX, Y = PositionY };
        }

        public double HPLoad()
        {
            double MaxHp = 0;
            if (Character != null)
            {
                var multiplicator = 1.0;
                var hp = 0;
                if (Character.UseSp)
                {
                    var specialist =
                        Character.Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);
                    if (specialist != null)
                    {
                        var point = CharacterHelper.SlPoint(spPoint: specialist.SlHp, mode: 3) + Character.slhpbonus;
                        if (point > 100) point = 100;
                        ;

                        if (point <= 50)
                            multiplicator += point / 100.0;
                        else
                            multiplicator += 0.5 + (point - 50.00) / 50.00;
                        hp = specialist.Hp + specialist.SpHp * 100;
                    }
                }

                hp += CellonOptions.Where(predicate: s => s.Type == CellonOptionType.HpMax).Sum(selector: s => s.Value);
                multiplicator += GetBuff(type: CardType.BearSpirit,
                                     subtype: (byte)AdditionalTypes.BearSpirit.IncreaseMaximumHp)[0] /
                                 100D;
                multiplicator += GetBuff(type: CardType.MaxHpmp,
                    subtype: (byte)AdditionalTypes.MaxHpmp.IncreasesMaximumHp)[0] / 100D;

                MaxHp = (int)((CharacterHelper.HPData[(byte)Character.Class, Level] + hp +
                                GetBuff(type: CardType.MaxHpmp,
                                    subtype: (byte)AdditionalTypes.MaxHpmp.MaximumHpIncreased)[0] +
                                GetBuff(type: CardType.MaxHpmp,
                                    subtype: (byte)AdditionalTypes.MaxHpmp.MaximumHpmpIncreased)[0]) *
                               multiplicator);
            }
            else
            {
                MaxHp = HpMax;
            }

            if (Hp > MaxHp) Hp = (int)MaxHp;
            return MaxHp;
        }

        public double MPLoad()
        {
            double MaxMp = 0;
            if (Character != null)
            {
                var mp = 0;
                var multiplicator = 1.0;
                if (Character.UseSp)
                {
                    var specialist =
                        Character.Inventory?.LoadBySlotAndType(slot: (byte)EquipmentType.Sp, type: InventoryType.Wear);
                    if (specialist != null)
                    {
                        var point = CharacterHelper.SlPoint(spPoint: specialist.SlHp, mode: 3) + Character.slhpbonus;
                        if (point > 100) point = 100;
                        ;

                        if (point <= 50)
                            multiplicator += point / 100.0;
                        else
                            multiplicator += 0.5 + (point - 50.00) / 50.00;
                        mp = specialist.Mp + specialist.SpHp * 100;
                    }
                }

                mp += CellonOptions.Where(predicate: s => s.Type == CellonOptionType.MpMax).Sum(selector: s => s.Value);

                multiplicator += GetBuff(type: CardType.BearSpirit,
                                     subtype: (byte)AdditionalTypes.BearSpirit.IncreaseMaximumMp)[0] /
                                 100D;
                multiplicator += GetBuff(type: CardType.MaxHpmp,
                    subtype: (byte)AdditionalTypes.MaxHpmp.IncreasesMaximumMp)[0] / 100D;

                MaxMp = (int)((CharacterHelper.MPData[(byte)Character.Class, Level] + mp +
                                GetBuff(type: CardType.MaxHpmp,
                                    subtype: (byte)AdditionalTypes.MaxHpmp.MaximumMpIncreased)[0] +
                                GetBuff(type: CardType.MaxHpmp,
                                    subtype: (byte)AdditionalTypes.MaxHpmp.MaximumHpmpIncreased)[0]) *
                               multiplicator);
            }
            else
            {
                MaxMp = MpMax;
            }

            if (Mp > MaxMp) Mp = (int)MaxMp;
            return MaxMp;
        }

        public void UpdateBushFire()
        {
            BrushFireJagged = BestFirstSearch.LoadBrushFireJagged(user: new GridPos
            {
                X = PositionX,
                Y = PositionY
            }, Grid: MapInstance.Map.JaggedGrid);
        }

        public int GetDamage(int damage, BattleEntity damager, bool dontKill = false, bool fromDebuff = false)
        {
            if (Character?.HasGodMode == true || Mate?.Owner.HasGodMode == true || HasBuff(
                type: CardType.HideBarrelSkill,
                subtype: (byte)AdditionalTypes.HideBarrelSkill.NoHpConsumption)) return 0;

            if (fromDebuff
            ) // If it comes from attack percent defense, dismin damage percent with chance, and static damages are already applied
            {
                var percentDefense = GetBuff(type: CardType.RecoveryAndDamagePercent,
                    subtype: (byte)AdditionalTypes.RecoveryAndDamagePercent.DecreaseSelfHp);
                if (percentDefense[0] != 0)
                {
                    var percentDefenseDamage = HpMax / 100 * Math.Abs(value: percentDefense[0]);
                    if (percentDefenseDamage < damage) damage = percentDefenseDamage;
                }

                if (MapMonster?.MonsterVNum == 533)
                    if (63 < damage)
                        damage = 63;
            }

            if (damager.MapEntityId != MapEntityId) LastDefence = DateTime.Now;

            Character?.DisposeShopAndExchange();

            if (Character != null)
            {
                if (Character.BattleEntity.AdditionalHp > damage)
                {
                    Character.BattleEntity.AdditionalHp -= damage;
                    damage = 0;
                    Character.Session.SendPacket(packet: Character.GenerateAdditionalHpMp());
                }
                else if (Character.BattleEntity.AdditionalHp > 0)
                {
                    damage -= (int)Character.BattleEntity.AdditionalHp;
                    Character.BattleEntity.AdditionalHp = 0;
                    Character.Session.SendPacket(packet: Character.GenerateAdditionalHpMp());
                }
            }

            if (Mate != null && MapInstance == Mate.Owner.Miniland && Mate.MateType == MateType.Pet)
                if (damager.MapMonster != null)
                    if (IsMateTrainer(vnum: damager.MapMonster.MonsterVNum))
                        Mate.DefendTrainer(trainerVnum: damager.MapMonster.MonsterVNum);

            if (MapMonster != null)
                if (damager.Mate != null && damager.MapInstance == damager.Mate.Owner.Miniland &&
                    damager.Mate.MateType == MateType.Pet)
                    if (IsMateTrainer(vnum: MapMonster.MonsterVNum))
                        damager.Mate.HitTrainer(trainerVnum: MapMonster.MonsterVNum);

            if (MapMonster?.AliveTimeMp > 0)
            {
                Mp -= damage;
            }
            else
            {
                if (Hp <= damage && dontKill)
                    damage = Hp - 1;
                else if (Hp < damage) damage = Hp;

                Hp -= damage;

                if (Hp > 0) return damage;

                Hp = 0;

                if (Character != null)
                {
                    Character.WalkDisposable?.Dispose();
                    RemoveBuff(id: 569);

                    if (MapInstance != null)
                        if (MapInstance.MapInstanceType != MapInstanceType.TalentArenaMapInstance)
                            Character.MapInstance.InstanceBag.DeadList.Add(item: Character.CharacterId);
                }
                else if (Mate != null)
                {
                    Mate.GenerateDeath(killer: damager);
                }

                ClearEnemyFalcon();
            }

            return damage;
        }

        public void DecreaseMp(int amount)
        {
            if (CellonOptions != null)
                amount = (short)(amount * ((100 - CellonOptions
                    .Where(predicate: s => s.Type == CellonOptionType.MpUsage)
                    .Sum(selector: s => s.Value)) / 100D));

            if (GetBuff(type: CardType.HealingBurningAndCasting,
                    subtype: (byte)AdditionalTypes.HealingBurningAndCasting.HpDecreasedByConsumingMp)[0] is int
                HPDecreasedByConsumingMP)
                if (HPDecreasedByConsumingMP < 0)
                {
                    var amountDecreased = -(amount * HPDecreasedByConsumingMP / 100);
                    GetDamage(damage: amountDecreased, damager: this, dontKill: true);
                    amount -= amountDecreased;
                }

            if (Character != null)
            {
                if (Character.BattleEntity.AdditionalMp > amount)
                {
                    Character.BattleEntity.AdditionalMp -= amount;
                    amount = 0;
                    Character.Session.SendPacket(packet: Character.GenerateAdditionalHpMp());
                }
                else if (Character.BattleEntity.AdditionalMp > 0)
                {
                    amount -= (int)Character?.BattleEntity.AdditionalMp;
                    Character.BattleEntity.AdditionalMp = 0;
                    Character.Session.SendPacket(packet: Character.GenerateAdditionalHpMp());
                }
            }

            Mp -= amount;
            if (Mp < 0) Mp = 0;
            Character?.Session.SendPacket(packet: Character.GenerateStat());
        }

        public short[] CantAttackEntitiesList =
        {
            2004, 2020, 965, 966, 967, 968, 433, 238, 239, 240, 258, 259, 260, 433, 797, 798, 2013, 2016, 390,
            424 //Fragmentos de meteorito
            ,
            425 //Tumba grande
            ,
            426 //Tumba pequeña
            ,
            453 //Portal hacia el Reino de la Muerte
            ,
            465 //Almeja gigante
            ,
            861 //Tronco
            ,
            862 //Árbol floreciente
            ,
            863 //Estatua del duende
            ,
            864 //Pentagrama
            ,
            865 //Círculo de la oscuridad
            ,
            866 //Arbusto sagrado
            ,
            867 //Altar sagrado
            ,
            869 //Nido de fisgón
            ,
            880 //Trigo
            ,
            881 //Mineral de hierro
            ,
            892 //Pista rara
            ,
            893 //Fuente misteriosa
            ,
            894 //Fuente secreta
            ,
            895 //Nido de pájaro
            ,
            896 //Nido de oro
            ,
            897 //Fuente mágica
            ,
            898 //Fuente estrafalaria
            ,
            902 //Rama mágica
            ,
            905 //Cartel de personas buscadas
            ,
            906 //Estatua de Shinebone
            ,
            907 //Caseta de vigilancia destruida
            ,
            908 //Suelo blando
            ,
            909 //Suelo áspero
            ,
            910 //Piedra brillante
            ,
            911 //Escoba
            ,
            912 //Planta de arroz
            ,
            913 //Estatua del horror
            ,
            914 //Árbol frutal
            ,
            915 //Marcador de puntos
            ,
            916 //Piedra roja
            ,
            917 //Estatua destruida de Shinebone
            ,
            920 //Cartel indicador pequeño
            ,
            921 //Cartel indicador grande
            ,
            922 //Cristal de arco iris grande
            ,
            923 //Cristal de arco iris mediano
            ,
            924 //Cristal de arco iris pequeño
            ,
            928 //Margarita
            ,
            929 //Lirio
            ,
            930 //Portal time-space
            ,
            931 //Nota de las hadas
            ,
            932 //Gema del alma amarilla
            ,
            933 //Gema del alma roja
            ,
            934 //Gema del alma azul
            ,
            938 //Estatua de Crhysos
            ,
            941 //Flor grande roja
            ,
            944 //Fuente
            ,
            953 //Gema del alma misteriosa
            ,
            954 //Gema del alma gris
            ,
            955 //Torre de teletransporte
            ,
            956 //Hoguera pequeña
            ,
            957 //Hoguera grande
            ,
            959 //Máquina de helado
            ,
            985 //Saco de Pascua
            ,
            988 //Huevo misterioso
            ,
            1264 //Saco con higos chumbos
            ,
            1265 //Buzón de Neil
            ,
            1266 //Humad prisionero
            ,
            1271 //Cofre de los comerciantes de Akamur abandonado
            ,
            1272 //Mercancía destruida
            ,
            1273 //Mercancía saqueada
            ,
            1274 //Bandera de Akamur destruida
            ,
            1275 //Primera mercancía
            ,
            1276 //Segunda mercancía
            ,
            1277 //Tercera mercancía
            ,
            1278 //Cuarta mercancía
            ,
            1279 //Quinta mercancía
            ,
            1280 //Sexta mercancía
            ,
            1281 //Cofre con lista de órdenes
            ,
            1282 //Poción sospechosa
            ,
            1283 //Baúl extraño de Keru
            ,
            1284 //Baúl extraño de Garton
            ,
            1285 //Reliquia robada de Keru
            ,
            1286 //Antigüedades robadas de Garton
            ,
            1287 //Reliquias robadas
            ,
            1288 //Baúl con pergaminos
            ,
            1289 //Fragmento de reliquia extraño
            ,
            1308 //Reliquias rotas
            ,
            1309 //Reliquia destruida
            ,
            1310 //Baúl sellado
            ,
            1311 //Fragmentos de reliquia rotos
            ,
            1312 //Fragmentos de reliquia rotos
            ,
            1313 //Fragmentos de reliquia rotos
            ,
            1314 //Fragmentos de reliquia rotos
            ,
            1315 //Primera lápida
            ,
            1316 //Segunda lápida
            ,
            1317 //Tercera lápida
            ,
            1318 //Lápida rota
            ,
            1319 //Lápida rota
            ,
            1320 //Lápida rota
            ,
            1321 //Lápida rota
            ,
            1322 //Monje muerto
            ,
            1323 //Clérigo muerto
            ,
            1327 //Baúl con polvo de hadas
            ,
            1328 //Carta de los ladrones del desierto
            ,
            1329 //Diario del viajero
            ,
            1330 //Botellita de veneno
            ,
            1331 //Bolsa de piel de un hada
            ,
            1332 //Polvo de hadas de la tierra
            ,
            1333 //Polvo de hadas del viento
            ,
            1334 //Polvo de hadas de la arena
            ,
            1335 //Frasquito de medicina
            ,
            1337 //Baúl de los comerciantes de Akamur
            ,
            1338 //Baúl perdido (campamento de Kerus)
            ,
            1339 //Baúl perdido (campamento de Garton)
            ,
            1340 //Mercancía perdida (Campamento de los Ladrones del Desierto)
            ,
            1341 //Cofre Perdido (Centro de los Ladrones)
            ,
            1342 //Baúl extraño sin descubrir
            ,
            1346 //Baúl de la banda de ladrones
            ,
            1347 //Baúl del tesoro del olvido
            ,
            1385 //Señal de Halloween
            ,
            2004 //Flor de hielo
            ,
            2320 //Cuerda
            ,
            2321 //Hermanos temerosos
            ,
            2350 //Planta venenosa de la condenación
            ,
            4280, 4281, 4282
        };

        public short[] CantAttackToEntitiesList =
        {
            848, 849, 850, 851, 852, 854, 958, 2591, 2592, 2004, 2020, 425 //Tumba grande
            ,
            426 //Tumba pequeña
            ,
            453 //Portal hacia el Reino de la Muerte
            ,
            465 //Almeja gigante
            ,
            861 //Tronco
            ,
            862 //Árbol floreciente
            ,
            863 //Estatua del duende
            ,
            864 //Pentagrama
            ,
            865 //Círculo de la oscuridad
            ,
            866 //Arbusto sagrado
            ,
            867 //Altar sagrado
            ,
            869 //Nido de fisgón
            ,
            880 //Trigo
            ,
            881 //Mineral de hierro
            ,
            892 //Pista rara
            ,
            893 //Fuente misteriosa
            ,
            894 //Fuente secreta
            ,
            895 //Nido de pájaro
            ,
            896 //Nido de oro
            ,
            897 //Fuente mágica
            ,
            898 //Fuente estrafalaria
            ,
            902 //Rama mágica
            ,
            905 //Cartel de personas buscadas
            ,
            906 //Estatua de Shinebone
            ,
            907 //Caseta de vigilancia destruida
            ,
            908 //Suelo blando
            ,
            909 //Suelo áspero
            ,
            910 //Piedra brillante
            ,
            911 //Escoba
            ,
            912 //Planta de arroz
            ,
            913 //Estatua del horror
            ,
            914 //Árbol frutal
            ,
            915 //Marcador de puntos
            ,
            916 //Piedra roja
            ,
            917 //Estatua destruida de Shinebone
            ,
            920 //Cartel indicador pequeño
            ,
            921 //Cartel indicador grande
            ,
            922 //Cristal de arco iris grande
            ,
            923 //Cristal de arco iris mediano
            ,
            924 //Cristal de arco iris pequeño
            ,
            928 //Margarita
            ,
            929 //Lirio
            ,
            930 //Portal time-space
            ,
            931 //Nota de las hadas
            ,
            932 //Gema del alma amarilla
            ,
            933 //Gema del alma roja
            ,
            934 //Gema del alma azul
            ,
            938 //Estatua de Crhysos
            ,
            941 //Flor grande roja
            ,
            944 //Fuente
            ,
            953 //Gema del alma misteriosa
            ,
            954 //Gema del alma gris
            ,
            955 //Torre de teletransporte
            ,
            956 //Hoguera pequeña
            ,
            957 //Hoguera grande
            ,
            959 //Máquina de helado
            ,
            985 //Saco de Pascua
            ,
            988 //Huevo misterioso
            ,
            1264 //Saco con higos chumbos
            ,
            1265 //Buzón de Neil
            ,
            1266 //Humad prisionero
            ,
            1271 //Cofre de los comerciantes de Akamur abandonado
            ,
            1272 //Mercancía destruida
            ,
            1273 //Mercancía saqueada
            ,
            1274 //Bandera de Akamur destruida
            ,
            1275 //Primera mercancía
            ,
            1276 //Segunda mercancía
            ,
            1277 //Tercera mercancía
            ,
            1278 //Cuarta mercancía
            ,
            1279 //Quinta mercancía
            ,
            1280 //Sexta mercancía
            ,
            1281 //Cofre con lista de órdenes
            ,
            1282 //Poción sospechosa
            ,
            1283 //Baúl extraño de Keru
            ,
            1284 //Baúl extraño de Garton
            ,
            1285 //Reliquia robada de Keru
            ,
            1286 //Antigüedades robadas de Garton
            ,
            1287 //Reliquias robadas
            ,
            1288 //Baúl con pergaminos
            ,
            1289 //Fragmento de reliquia extraño
            ,
            1308 //Reliquias rotas
            ,
            1309 //Reliquia destruida
            ,
            1310 //Baúl sellado
            ,
            1311 //Fragmentos de reliquia rotos
            ,
            1312 //Fragmentos de reliquia rotos
            ,
            1313 //Fragmentos de reliquia rotos
            ,
            1314 //Fragmentos de reliquia rotos
            ,
            1315 //Primera lápida
            ,
            1316 //Segunda lápida
            ,
            1317 //Tercera lápida
            ,
            1318 //Lápida rota
            ,
            1319 //Lápida rota
            ,
            1320 //Lápida rota
            ,
            1321 //Lápida rota
            ,
            1322 //Monje muerto
            ,
            1323 //Clérigo muerto
            ,
            1327 //Baúl con polvo de hadas
            ,
            1328 //Carta de los ladrones del desierto
            ,
            1329 //Diario del viajero
            ,
            1330 //Botellita de veneno
            ,
            1331 //Bolsa de piel de un hada
            ,
            1332 //Polvo de hadas de la tierra
            ,
            1333 //Polvo de hadas del viento
            ,
            1334 //Polvo de hadas de la arena
            ,
            1335 //Frasquito de medicina
            ,
            1337 //Baúl de los comerciantes de Akamur
            ,
            1338 //Baúl perdido (campamento de Kerus)
            ,
            1339 //Baúl perdido (campamento de Garton)
            ,
            1340 //Mercancía perdida (Campamento de los Ladrones del Desierto)
            ,
            1341 //Cofre Perdido (Centro de los Ladrones)
            ,
            1342 //Baúl extraño sin descubrir
            ,
            1346 //Baúl de la banda de ladrones
            ,
            1347 //Baúl del tesoro del olvido
            ,
            1385 //Señal de Halloween
            ,
            2004 //Flor de hielo
            ,
            2320 //Cuerda
            ,
            2321 //Hermanos temerosos
            ,
            2350 //Planta venenosa de la condenación
            ,
            1375, 1376, 1377, 1083, 1438, 1439, 856, 955, 853, 2018, 4280, 4281, 4282, 1436, 855, 860,
            2352 // SP8M Meteorite
            ,
            2353 // SP8M Meteorite
            ,
            2329 // Cheese Chunk
            ,
            2330 // Grass Clump
        };

        public bool CanAttackEntity(BattleEntity receiver, bool isOwnerCheck = false)
        {
            if (receiver != null && this != receiver && (Hp > 0 || isOwnerCheck) && (receiver.Hp > 0 || isOwnerCheck))
            {
                if (MapMonster != null && CantAttackEntitiesList.Contains(value: MapMonster.MonsterVNum)) return false;
                if (MapNpc != null && CantAttackEntitiesList.Contains(value: MapNpc.NpcVNum)) return false;
                if (receiver.MapMonster != null &&
                    CantAttackToEntitiesList.Contains(value: receiver.MapMonster.MonsterVNum))
                    return false;
                if (receiver.MapNpc != null && CantAttackToEntitiesList.Contains(value: receiver.MapNpc.NpcVNum))
                    return false;
                if (MapInstance.InstanceBag?.EndState != 0) return false;
                switch (EntityType)
                {
                    case EntityType.Player:
                        {
                            if (Character.Timespace != null && Character.Timespace.InstanceBag.EndState != 0) return false;
                            // User in SafeZone
                            if (MapInstance.MapInstanceId == ServerManager.Instance.ArenaInstance.MapInstanceId
                                && (MapInstance.Map.JaggedGrid[Character.PositionX][Character.PositionY]?.Value != 0 &&
                                    (MapInstance.Map.JaggedGrid[Character.PositionX][Character.PositionY]?.Value != 16 ||
                                     Character.PositionY == 35)
                                    || receiver.MapInstance.Map.JaggedGrid[receiver.PositionX][receiver.PositionY]?.Value !=
                                    0 && (MapInstance.Map.JaggedGrid[receiver.PositionX][receiver.PositionY]?.Value != 16 ||
                                          receiver.PositionY == 35)))
                                return false;
                            if (MapInstance.MapInstanceId == ServerManager.Instance.FamilyArenaInstance.MapInstanceId
                                && (MapInstance.Map.JaggedGrid[Character.PositionX][Character.PositionY]?.Value != 0
                                    || receiver.MapInstance.Map.JaggedGrid[receiver.PositionX][receiver.PositionY]?.Value !=
                                    0))
                                return false;

                            switch (receiver.EntityType)
                            {
                                case EntityType.Player:
                                    {
                                        if (receiver.Character.InvisibleGm) return false;
                                        if (MapInstance.Map.MapTypes.Any(
                                            predicate: s => s.MapTypeId == (short)MapTypeEnum.Act4))
                                        {
                                            if (Character.Faction != receiver.Character.Faction
                                                && MapInstance.Map.MapId != 130
                                                && MapInstance.Map.MapId != 131)
                                                return true;
                                        }
                                        else if (MapInstance.Map.MapTypes.Any(predicate: m =>
                                                m.MapTypeId == (short)MapTypeEnum.PvpMap) || MapInstance.IsPvp
                                                                                           || HasBuff(
                                                                                               type: CardType.SpecialEffects,
                                                                                               subtype: (byte)AdditionalTypes
                                                                                                   .SpecialEffects
                                                                                                   .AbleToFightPvp) &&
                                                                                           receiver.HasBuff(
                                                                                               type: CardType.SpecialEffects,
                                                                                               subtype: (byte)AdditionalTypes
                                                                                                   .SpecialEffects
                                                                                                   .AbleToFightPvp))
                                        {
                                            if (MapInstance == ServerManager.Instance.FamilyArenaInstance
                                                && (Character.Family != null && receiver.Character.Family != null &&
                                                    Character.Family == receiver.Character.Family
                                                    || Character.Family == null && receiver.Character.Family == null))
                                                return false;

                                            ConcurrentBag<ArenaTeamMember> team = null;
                                            if (MapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance)
                                                team = ServerManager.Instance.ArenaTeams.ToList()
                                                    .FirstOrDefault(predicate: s =>
                                                        s.Any(predicate: o => o.Session == Character.Session));

                                            ConcurrentBag<ClientSession> iceteam = null;
                                            if (MapInstance.MapInstanceType == MapInstanceType.IceBreakerInstance)
                                            {
                                                if (IceBreaker.FrozenPlayers.Contains(item: receiver.Character.Session))
                                                    return false;
                                                if (IceBreaker.IceBreakerTeams.FirstOrDefault(predicate: t =>
                                                        t.Contains(value: Character.Session)) !=
                                                    null)
                                                    iceteam = IceBreaker.IceBreakerTeams.FirstOrDefault(predicate: t =>
                                                        t.Contains(value: Character.Session));
                                            }

                                            if (team != null &&
                                                team.FirstOrDefault(predicate: s => s.Session == Character.Session)
                                                    ?.ArenaTeamType !=
                                                team.FirstOrDefault(predicate: s => s.Session == receiver.Character.Session)
                                                    ?.ArenaTeamType
                                                || MapInstance.MapInstanceType != MapInstanceType.TalentArenaMapInstance &&
                                                (Character.Group == null ||
                                                 !Character.Group.IsMemberOfGroup(entityId: receiver.Character.CharacterId))
                                                && (iceteam == null || !iceteam.Contains(value: receiver.Character.Session)))
                                                return true;
                                        }
                                    }
                                    break;
                                case EntityType.Mate:
                                    {
                                        if ((receiver.Mate.Owner.IsVehicled || receiver.Mate.Owner.Invisible) &&
                                            !receiver.Mate.IsTemporalMate) return false;
                                        return CanAttackEntity(receiver: receiver.Mate.Owner.BattleEntity, isOwnerCheck: true);
                                    }
                                case EntityType.Monster:
                                    if (receiver.MapMonster.Owner == null || receiver.MapMonster.Owner.MapEntityId !=
                                                                          MapEntityId &&
                                                                          CanAttackEntity(
                                                                              receiver: receiver.MapMonster.Owner,
                                                                              isOwnerCheck: true)
                                                                          || receiver.MapMonster.Owner.MapEntityId ==
                                                                          MapEntityId &&
                                                                          MapInstance == Character.Miniland &&
                                                                          IsMateTrainer(
                                                                              vnum: receiver.MapMonster.MonsterVNum))
                                        if (ServerManager.Instance.ChannelId != 51 ||
                                            receiver.MapMonster.Faction == FactionType.None ||
                                            receiver.MapMonster.Faction != Character.Faction)
                                        {
                                            if (receiver.MapMonster.IsDisabled ||
                                                !isOwnerCheck && receiver.MapMonster.IsJumping) return false;
                                            if (!IsMateTrainer(vnum: receiver.MapMonster.MonsterVNum) &&
                                                (receiver.MapMonster.Owner?.Character != null ||
                                                 receiver.MapMonster.Owner?.Mate != null)) return false;
                                            return true;
                                        }

                                    break;
                                case EntityType.Npc:
                                    if (receiver.MapNpc.IsDisabled) return false;
                                    break;
                            }
                        }
                        break;

                    case EntityType.Mate:
                        {
                            if ((Mate.Owner.IsVehicled || Mate.Owner.Invisible) && !Mate.IsTemporalMate) return false;
                            return Mate.Owner.BattleEntity.CanAttackEntity(receiver: receiver, isOwnerCheck: true);
                        }

                    case EntityType.Monster:
                        {
                            if (MapMonster.IsDisabled || !isOwnerCheck && MapMonster.IsJumping) return false;
                            if (MapMonster.Owner != null)
                            {
                                if (IsMateTrainer(vnum: MapMonster.MonsterVNum))
                                {
                                    if (receiver.Mate != null &&
                                        receiver.Mate.Owner.CharacterId == MapMonster.Owner.MapEntityId &&
                                        MapInstance == receiver.Mate.Owner.Miniland)
                                        return true;
                                    if (receiver.MapMonster?.Owner != null &&
                                        receiver.MapMonster.Owner == MapMonster.Owner) return false;
                                }

                                return MapMonster.Owner.CanAttackEntity(receiver: receiver, isOwnerCheck: true);
                            }

                            switch (receiver.EntityType)
                            {
                                case EntityType.Player:
                                    {
                                        if (receiver.Character.Timespace != null &&
                                            receiver.Character.Timespace.InstanceBag.EndState != 0) return false;
                                        if ((ServerManager.Instance.ChannelId != 51 ||
                                             MapMonster.Faction == FactionType.None ||
                                             MapMonster.Faction != receiver.Character.Faction)
                                            && !receiver.Character.InvisibleGm)
                                            return true;
                                    }
                                    break;
                                case EntityType.Mate:
                                    {
                                        if ((receiver.Mate.Owner.IsVehicled || receiver.Mate.Owner.Invisible) &&
                                            !receiver.Mate.IsTemporalMate) return false;
                                        return CanAttackEntity(receiver: receiver.Mate.Owner.BattleEntity, isOwnerCheck: true);
                                    }
                                case EntityType.Monster:
                                    {
                                        if (receiver.MapMonster.IsDisabled || receiver.MapMonster.IsJumping) return false;
                                        if (receiver.MapMonster.Owner?.Mate != null) return false;
                                        return CanAttackEntity(receiver: receiver.MapMonster.Owner, isOwnerCheck: true);
                                    }
                                case EntityType.Npc:
                                    {
                                        if (receiver.MapNpc.IsDisabled || receiver.MapNpc.Shop != null) return false;
                                        return true;
                                    }
                            }
                        }
                        break;
                    case EntityType.Npc:
                        {
                            if (MapNpc.IsDisabled || MapNpc.Shop != null) return false;
                            /*if (MapNpc.Owner != null)
                            {
                                return MapNpc.Owner.BattleEntity.CanAttackEntity(receiver, true);
                            }
                            else*/
                            {
                                switch (receiver.EntityType)
                                {
                                    case EntityType.Player:
                                        return false;
                                    /*{

                                        if (!receiver.Character.InvisibleGm)
                                        {
                                            return true;
                                        }
                                    }
                                    break;*/
                                    case EntityType.Mate:
                                        {
                                            if ((receiver.Mate.Owner.IsVehicled || receiver.Mate.Owner.Invisible) &&
                                                !receiver.Mate.IsTemporalMate) return false;
                                            return CanAttackEntity(receiver: receiver.Mate.Owner.BattleEntity,
                                                isOwnerCheck: true);
                                        }
                                    case EntityType.Monster:
                                        {
                                            if (receiver.MapMonster.IsDisabled || receiver.MapMonster.IsJumping) return false;
                                            if (receiver.MapMonster.Owner?.Mate != null) return false;
                                            if (receiver.MapMonster.Owner != null)
                                                return CanAttackEntity(receiver: receiver.MapMonster.Owner, isOwnerCheck: true);
                                            return true;
                                        }
                                    case EntityType.Npc:
                                        {
                                            if (receiver.MapNpc.IsDisabled) return false;
                                            return true;
                                        }
                                }
                            }
                        }
                        break;
                }
            }

            return false;
        }

        public bool IsMateTrainer(int vnum)
        {
            return vnum == 160 || vnum == 900 || vnum == 636 || vnum == 971;
        }

        public bool IsSignpost(int vnum)
        {
            return new[] { 920, 921, 1385, 1428, 1499, 1519 }.Contains(value: vnum);
        }

        public bool IsCampfire(int vnum)
        {
            return new[] { 956, 957, 959 }.Contains(value: vnum);
        }

        public string GenerateRc(int characterHealth)
        {
            return $"rc {(short)UserType} {MapEntityId} {characterHealth} 0";
        }

        public string GenerateDm(int dmg)
        {
            return $"dm {(short)UserType} {MapEntityId} {dmg}";
        }

        public string GenerateTp()
        {
            Character?.WalkDisposable?.Dispose();
            return $"tp {(short)UserType} {MapEntityId} {PositionX} {PositionY} 0";
        }

        public bool HasEntity
        {
            get => Character != null || Mate != null || MapMonster != null || MapNpc != null;
        }

        public List<EventContainer> OnDeathEvents { get; set; }

        public bool IsInRange(int xCoordinate, int yCoordinate, int range = 50)
        {
            return Math.Abs(value: PositionX - xCoordinate) <= range &&
                   Math.Abs(value: PositionY - yCoordinate) <= range;
        }

        public void TeleportTo(MapCell mapCell, short distance = 0)
        {
            if (MapInstance?.Map == null || mapCell == null) return;

            var mapCellTo = MapInstance.Map.GetRandomPositionByDistance(xPos: mapCell.X, yPos: mapCell.Y,
                                distance: distance, randomInRange: true) ??
                            mapCell;

            PositionX = mapCellTo.X;
            PositionY = mapCellTo.Y;
            MapInstance?.Broadcast(packet: GenerateTp());

            RemoveBuff(id: 620);
        }

        #endregion
    }
}