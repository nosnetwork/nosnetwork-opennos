﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class PartnerSp
    {
        #region Istantiation

        public PartnerSp(ItemInstance instance)
        {
            Instance = instance;

            Initialize();
        }

        #endregion

        #region Properties

        public ItemInstance Instance { get; }

        long XpMax
        {
            get => ServerManager.Instance.Configuration.PartnerSpXp;
        }

        List<PartnerSkill> Skills { get; set; }

        #endregion

        #region Methods

        public bool AddSkill(byte castId)
        {
            var skill = GetNewSkill(castId: castId);

            if (skill != null)
            {
                var partnerSkillDTO = new PartnerSkillDto
                {
                    EquipmentSerialId = Instance.EquipmentSerialId,
                    SkillVNum = skill.SkillVNum,
                    Level = ServerManager.RandomNumber<byte>(min: 1, max: 8)
                };

                if (DaoFactory.PartnerSkillDao.Insert(partnerSkillDto: partnerSkillDTO) is PartnerSkillDto result)
                {
                    Skills.Add(item: new PartnerSkill(input: result));

                    return true;
                }
            }
            else
            {
                Logger.Warn(data: $"Partner skill not found (Morph: {Instance.Item.Morph}, CastId: {castId})");
            }

            return false;
        }

        public void AddXp(long amount)
        {
            if (Instance.Xp < XpMax) Instance.Xp = Math.Min(val1: Instance.Xp + amount, val2: XpMax);
        }

        public void FullXp()
        {
            Instance.Xp = XpMax;
        }

        public bool CanLearnSkill()
        {
            return Instance.Xp >= XpMax;
        }

        public void ClearSkills()
        {
            for (byte castId = 0; castId < 3; castId++) RemoveSkill(castId: castId);

            ReloadSkills();
        }

        public string GeneratePski()
        {
            var pski = "pski";

            foreach (var partnerSkill in Skills.OrderBy(keySelector: s => s.Skill.CastId))
                pski += $" {partnerSkill.Skill.SkillVNum}";

            return pski;
        }

        public string GenerateSkills(bool withLevel = true)
        {
            var skills = "";

            for (byte castId = 0; castId < 3; castId++)
            {
                var partnerSkill = GetSkill(castId: castId);

                if (partnerSkill != null)
                    skills += withLevel
                        ? $" {partnerSkill.SkillVNum}.{partnerSkill.Level}"
                        : $" {partnerSkill.SkillVNum}";
                else
                    skills += withLevel ? " 0.0" : " 0";
            }

            return skills;
        }

        public int GetCooldown()
        {
            double maxCooldown = 30000;

            foreach (var partnerSkill in Skills.ToList().Where(predicate: s => !s.CanBeUsed()))
            {
                var remaining = (partnerSkill.LastUse - DateTime.Now).TotalMilliseconds +
                                partnerSkill.Skill.Cooldown * 100;

                if (remaining > maxCooldown) maxCooldown = remaining;
            }

            return (int)(maxCooldown / 1000D);
        }

        public string GetName()
        {
            switch (Instance.ItemVNum)
            {
                case 4324: return "Guardian^Lucifer";
                case 4325: return "Sheriff^Chloe";
                case 4326: return "Bone^Warrior^Ragnar";
                case 4343: return "Mad^Professor^Macavity";
                case 4349: return "Archdaemon^Amon";
                case 4405: return "Magic^Student^Yuna";
                case 4413: return "Amora";
                case 4417: return "Mad^March^Hare";
                case 4800: return "Aegir^the^Angry";
                case 4802: return "Barni^the^Clever";
                case 4803: return "Freya^the^Fateful";
                case 4804: return "Shinobi^the^Silent";
                case 4805: return "Lotus^the^Graceful";
                case 4806: return "Orkani^the^Turbulent";
                case 4807: return "Foxy";
                case 4808: return "Maru";
                case 4809: return "Maru^in^Mother's^Fur";
                case 4810: return "Hongbi";
                case 4811: return "Cheongbi";
                case 4812: return "Lucifer";
                case 4813: return "Witch^Laurena";
                case 4814: return "Amon";
                case 4815: return "Lucy^Lopea﻿rs";
                case 4817: return "Cowgirl^Chloe";
                case 4818: return "Fiona";
                case 4819: return "Jinn";
                case 4820: return "Ice^Princess^Eliza";
                case 4821: return "Daniel^Ducats";
                case 4822: return "Palina^Puppet^Master";
                case 4823: return "Harlequin";
                case 4824: return "Nelia^Nymph";
                case 4825: return "Little^Pri﻿ncess^Venus";
            }

            return Instance.Item.Name.Replace(oldChar: ' ', newChar: '^');
        }

        Skill GetNewSkill(byte castId)
        {
            return ServerManager.GetAllSkill().FirstOrDefault(predicate: s =>
                s.CastId == castId && (s.Class == 28 || s.Class == 29)
                                   && s.UpgradeType == MateHelper.Instance.GetUpgradeType(morph: Instance.Item.Morph));
        }

        public PartnerSkill GetSkill(byte castId)
        {
            return Skills.FirstOrDefault(predicate: s => s.Skill.CastId == castId);
        }

        public int GetSkillsCount()
        {
            return Skills.Count;
        }

        public int GetXpPercent()
        {
            return (int)Math.Floor(d: 100D / XpMax * Instance.Xp);
        }

        void Initialize()
        {
            if (Instance.EquipmentSerialId == Guid.Empty) Instance.EquipmentSerialId = Guid.NewGuid();

            LoadSkills();
        }

        void LoadSkills()
        {
            Skills = DaoFactory.PartnerSkillDao.LoadByEquipmentSerialId(equipmentSerialId: Instance.EquipmentSerialId)
                .Select(selector: partnerSkillDTO => new PartnerSkill(input: partnerSkillDTO)).ToList();
        }

        public void ReloadSkills()
        {
            LoadSkills();
        }

        public bool RemoveSkill(byte castId)
        {
            var partnerSkill = GetSkill(castId: castId);

            return partnerSkill != null &&
                   DaoFactory.PartnerSkillDao.Remove(partnerSkillId: partnerSkill.PartnerSkillId) != DeleteResult.Error;
        }

        public void ResetXp()
        {
            Instance.Xp = 0;
        }

        #endregion
    }
}