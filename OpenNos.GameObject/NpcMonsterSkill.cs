﻿using System;
using OpenNos.Data;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class NpcMonsterSkill : NpcMonsterSkillDto
    {
        #region Members

        Skill _skill;

        #endregion

        #region Instantiation

        public NpcMonsterSkill()
        {
        }

        public NpcMonsterSkill(NpcMonsterSkillDto input)
        {
            NpcMonsterSkillId = input.NpcMonsterSkillId;
            NpcMonsterVNum = input.NpcMonsterVNum;
            Rate = input.Rate;
            SkillVNum = input.SkillVNum;
        }

        #endregion

        #region Properties

        public short Hit { get; set; }

        public DateTime LastSkillUse { get; set; }

        public Skill Skill
        {
            get => _skill ?? (_skill = ServerManager.GetSkill(skillVNum: SkillVNum));
        }

        public bool CanBeUsed()
        {
            return Skill != null && LastSkillUse.AddMilliseconds(value: Skill.Cooldown * 100) < DateTime.Now;
        }

        #endregion
    }
}