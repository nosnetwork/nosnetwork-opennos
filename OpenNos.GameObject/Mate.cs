﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Threading;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Battle;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.PathFinder;
using static OpenNos.Domain.BCardType;

namespace OpenNos.GameObject
{
    public class Mate : MateDto
    {
        public void UpdateBushFire()
        {
            BattleEntity.UpdateBushFire();
        }

        public void GetDamage(int damage, BattleEntity damager)
        {
            BattleEntity.GetDamage(damage: damage, damager: damager);
        }

        byte GetMateType()
        {
            return Monster.AttackClass;
        }

        public string GenerateTp()
        {
            return $"tp 2 {MateTransportId} {PositionX} {PositionY} 0";
        }

        public void HitRequest(HitRequest hitRequest)
        {
            if (IsAlive && (hitRequest.Session?.Character == null || hitRequest.Session.Character.Hp > 0) &&
                (hitRequest.Mate == null || hitRequest.Mate.Hp > 0))
            {
                double cooldownReduction = 0;

                if (hitRequest.Session?.Character != null)
                {
                    cooldownReduction = hitRequest.Session.Character.GetBuff(type: CardType.Morale,
                        subtype: (byte)AdditionalTypes.Morale.SkillCooldownDecreased)[0];

                    var increaseEnemyCooldownChance = hitRequest.Session.Character.GetBuff(
                        type: CardType.DarkCloneSummon,
                        subtype: (byte)AdditionalTypes.DarkCloneSummon.IncreaseEnemyCooldownChance);

                    if (ServerManager.RandomNumber() < increaseEnemyCooldownChance[0])
                        cooldownReduction -= increaseEnemyCooldownChance[1];
                }

                var hitmode = 0;

                // calculate damage
                var onyxWings = false;
                var attackerBattleEntity = hitRequest.Mate != null
                    ? new BattleEntity(mate: hitRequest.Mate)
                    : hitRequest.Session?.Character != null
                        ? new BattleEntity(character: hitRequest.Session.Character, skill: hitRequest.Skill)
                        : hitRequest.Monster != null
                            ? new BattleEntity(monster: hitRequest.Monster)
                            : null;

                if (attackerBattleEntity == null) return;

                var attackGreaterDistance = false;
                if (hitRequest.Skill != null && hitRequest.Skill.TargetType == 1 && hitRequest.Skill.HitType == 1 &&
                    hitRequest.Skill.TargetRange == 0 && hitRequest.Skill.Range > 0) attackGreaterDistance = true;

                var damage = DamageHelper.Instance.CalculateDamage(attacker: attackerBattleEntity,
                    defender: new BattleEntity(mate: this),
                    skill: hitRequest.Skill, hitMode: ref hitmode, onyxWings: ref onyxWings,
                    attackGreaterDistance: attackGreaterDistance);
                if (Monster.BCards.Find(match: s =>
                    s.Type == (byte)CardType.LightAndShadow &&
                    s.SubType == (byte)AdditionalTypes.LightAndShadow.InflictDamageToMp) is { } card)
                {
                    var reduce = damage / 100 * card.FirstData;
                    if (Hp < reduce)
                    {
                        reduce = (int)Mp;
                        Mp = 0;
                    }
                    else
                    {
                        DecreaseMp(amount: reduce);
                    }

                    damage -= reduce;
                }

                if (damage >= Hp &&
                    Monster.BCards.Any(predicate: s =>
                        s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                        s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoDeath / 10 &&
                        s.FirstData == -1))
                {
                    damage = (int)Hp - 1;
                }
                else if (onyxWings)
                {
                    var onyxX = (short)(attackerBattleEntity.PositionX + 2);
                    var onyxY = (short)(attackerBattleEntity.PositionY + 2);
                    var onyxId = BattleEntity.MapInstance.GetNextMonsterId();
                    var onyx = new MapMonster
                    {
                        MonsterVNum = 2371,
                        MapX = onyxX,
                        MapY = onyxY,
                        MapMonsterId = onyxId,
                        IsHostile = false,
                        IsMoving = false,
                        ShouldRespawn = false
                    };
                    BattleEntity.MapInstance.Broadcast(packet: UserInterfaceHelper.GenerateGuri(type: 31, argument: 1,
                        callerId: attackerBattleEntity.MapEntityId, value: onyxX, value2: onyxY));
                    onyx.Initialize(currentMapInstance: BattleEntity.MapInstance);
                    BattleEntity.MapInstance.AddMonster(monster: onyx);
                    BattleEntity.MapInstance.Broadcast(packet: onyx.GenerateIn());
                    BattleEntity.GetDamage(damage: damage / 2, damager: attackerBattleEntity);
                    var request = hitRequest;
                    var damage1 = damage;
                    Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 350)).Subscribe(onNext: o =>
                    {
                        BattleEntity.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(type: UserType.Monster,
                            callerId: onyxId,
                            secondaryType: (byte)BattleEntity.UserType,
                            targetId: BattleEntity.MapEntityId, skillVNum: -1, cooldown: 0, attackAnimation: -1,
                            skillEffect: request.Skill?.Effect ?? 0, x: -1, y: -1, isAlive: IsAlive,
                            health: (int)(Hp / MaxHp * 100), damage: damage1 / 2, hitmode: 0,
                            skillType: 0));
                        BattleEntity.MapInstance.RemoveMonster(monsterToRemove: onyx);
                        BattleEntity.MapInstance.Broadcast(
                            packet: StaticPacketHelper.Out(type: UserType.Monster, callerId: onyx.MapMonsterId));
                    });
                }

                attackerBattleEntity.BCards.Where(predicate: s => s.CastType == 1).ForEach(action: s =>
                {
                    if (s.Type != (byte)CardType.Buff &&
                        (hitRequest.TargetHitType != TargetHitType.AoeTargetHit || s.Type != (byte)CardType.Summons &&
                            s.Type != (byte)CardType.SummonSkill))
                        s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity);
                });

                hitRequest.SkillBCards.Where(predicate: s =>
                        !s.Type.Equals(obj: (byte)CardType.Buff) &&
                        (hitRequest.TargetHitType != TargetHitType.AoeTargetHit || s.Type != (byte)CardType.Summons &&
                            s.Type != (byte)CardType.SummonSkill) && !s.Type.Equals(obj: (byte)CardType.Capture) &&
                        s.CardId == null).ToList()
                    .ForEach(action: s => s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity));

                if (hitmode != 4 && hitmode != 2)
                {
                    if (damage > 0)
                    {
                        RemoveBuff(cardId: 36);
                        RemoveBuff(cardId: 548);
                    }

                    attackerBattleEntity.BCards.Where(predicate: s => s.CastType == 1).ForEach(action: s =>
                    {
                        if (s.Type == (byte)CardType.Buff)
                        {
                            var b = new Buff.Buff(id: (short)s.SecondData, level: Monster.Level);
                            if (b.Card != null)
                                switch (b.Card?.BuffType)
                                {
                                    case BuffType.Bad:
                                        s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity);
                                        break;

                                    case BuffType.Good:
                                    case BuffType.Neutral:
                                        s.ApplyBCards(session: attackerBattleEntity, sender: attackerBattleEntity);
                                        break;
                                }
                        }
                    });

                    BattleEntity.BCards.Where(predicate: s => s.CastType == 0).ForEach(action: s =>
                    {
                        if (s.Type == (byte)CardType.Buff)
                        {
                            var b = new Buff.Buff(id: (short)s.SecondData, level: BattleEntity.Level);
                            if (b.Card != null)
                                switch (b.Card?.BuffType)
                                {
                                    case BuffType.Bad:
                                        s.ApplyBCards(session: attackerBattleEntity, sender: BattleEntity);
                                        break;

                                    case BuffType.Good:
                                    case BuffType.Neutral:
                                        s.ApplyBCards(session: BattleEntity, sender: BattleEntity);
                                        break;
                                }
                        }
                    });

                    hitRequest.SkillBCards.Where(predicate: s =>
                            s.Type.Equals(obj: (byte)CardType.Buff) &&
                            new Buff.Buff(id: (short)s.SecondData, level: attackerBattleEntity.Level).Card?.BuffType ==
                            BuffType.Bad)
                        .ToList()
                        .ForEach(action: s => s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity));

                    hitRequest.SkillBCards.Where(predicate: s => s.Type.Equals(obj: (byte)CardType.SniperAttack))
                        .ToList()
                        .ForEach(action: s => s.ApplyBCards(session: BattleEntity, sender: attackerBattleEntity));

                    if (attackerBattleEntity.ShellWeaponEffects != null)
                        foreach (var shell in attackerBattleEntity.ShellWeaponEffects)
                            switch (shell.Effect)
                            {
                                case (byte)ShellWeaponEffectType.Blackout:
                                    {
                                        var buff = new Buff.Buff(id: 7, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.DeadlyBlackout:
                                    {
                                        var buff = new Buff.Buff(id: 66, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.MinorBleeding:
                                    {
                                        var buff = new Buff.Buff(id: 1, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.Bleeding:
                                    {
                                        var buff = new Buff.Buff(id: 21, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.HeavyBleeding:
                                    {
                                        var buff = new Buff.Buff(id: 42, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                                case (byte)ShellWeaponEffectType.Freeze:
                                    {
                                        var buff = new Buff.Buff(id: 27, level: attackerBattleEntity.Level);
                                        if (ServerManager.RandomNumber() < shell.Value)
                                            AddBuff(indicator: buff, battleEntity: attackerBattleEntity);

                                        break;
                                    }
                            }
                }

                BattleEntity.GetDamage(damage: damage, damager: attackerBattleEntity);

                if (IsSitting) Owner.MapInstance.Broadcast(packet: GenerateRest(ownerSit: false));

                if (attackerBattleEntity.MapMonster?.DamageList != null)
                    lock (attackerBattleEntity.MapMonster)
                    {
                        lock (attackerBattleEntity.MapMonster.DamageList)
                        {
                            if (!attackerBattleEntity.MapMonster.DamageList.Any(predicate: s =>
                                s.Key.MapEntityId == BattleEntity.MapEntityId))
                                attackerBattleEntity.MapMonster.AddToAggroList(aggroEntity: BattleEntity);
                        }
                    }

                if (hitmode != 2)
                {
                    if (hitRequest.Skill != null)
                        switch (hitRequest.TargetHitType)
                        {
                            case TargetHitType.SingleTargetHit:
                                BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: attackerBattleEntity.UserType,
                                    callerId: attackerBattleEntity.MapEntityId,
                                    secondaryType: (byte)BattleEntity.UserType,
                                    targetId: BattleEntity.MapEntityId,
                                    skillVNum: hitRequest.Skill.SkillVNum,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.Skill.AttackAnimation,
                                    skillEffect: hitRequest.SkillEffect,
                                    x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                    isAlive: IsAlive, health: (int)(Hp / MaxHp * 100), damage: damage,
                                    hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                break;

                            case TargetHitType.SingleTargetHitCombo:
                                BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: attackerBattleEntity.UserType,
                                    callerId: attackerBattleEntity.MapEntityId,
                                    secondaryType: (byte)BattleEntity.UserType,
                                    targetId: BattleEntity.MapEntityId,
                                    skillVNum: hitRequest.Skill.SkillVNum,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.SkillCombo.Animation,
                                    skillEffect: hitRequest.SkillCombo.Effect,
                                    x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                    isAlive: IsAlive, health: (int)(Hp / MaxHp * 100), damage: damage,
                                    hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                break;

                            case TargetHitType.SingleAoeTargetHit:
                                if (hitRequest.ShowTargetHitAnimation)
                                {
                                    if (hitRequest.Session?.Character != null)
                                        if (hitRequest.Skill.SkillVNum == 1085 || hitRequest.Skill.SkillVNum == 1091 ||
                                            hitRequest.Skill.SkillVNum == 1060)
                                        {
                                            attackerBattleEntity.PositionX = PositionX;
                                            attackerBattleEntity.PositionY = PositionY;
                                            attackerBattleEntity.MapInstance?.Broadcast(packet: hitRequest.Session
                                                .Character
                                                .GenerateTp());
                                        }

                                    BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                        type: attackerBattleEntity.UserType,
                                        callerId: attackerBattleEntity.MapEntityId,
                                        secondaryType: (byte)BattleEntity.UserType,
                                        targetId: BattleEntity.MapEntityId,
                                        skillVNum: hitRequest.Skill.SkillVNum,
                                        cooldown: (short)(hitRequest.Skill.Cooldown +
                                                           hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                        attackAnimation: hitRequest.Skill.AttackAnimation,
                                        skillEffect: hitRequest.SkillEffect,
                                        x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                        isAlive: IsAlive, health: (int)(Hp / MaxHp * 100), damage: damage,
                                        hitmode: hitmode,
                                        skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                }
                                else
                                {
                                    switch (hitmode)
                                    {
                                        case 1:
                                        case 4:
                                            hitmode = 7;
                                            break;

                                        case 2:
                                            hitmode = 2;
                                            break;

                                        case 3:
                                            hitmode = 6;
                                            break;

                                        default:
                                            hitmode = 5;
                                            break;
                                    }

                                    BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                        type: attackerBattleEntity.UserType,
                                        callerId: attackerBattleEntity.MapEntityId,
                                        secondaryType: (byte)BattleEntity.UserType,
                                        targetId: BattleEntity.MapEntityId,
                                        skillVNum: -1,
                                        cooldown: (short)(hitRequest.Skill.Cooldown +
                                                           hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                        attackAnimation: hitRequest.Skill.AttackAnimation,
                                        skillEffect: hitRequest.SkillEffect,
                                        x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                        isAlive: IsAlive, health: (int)(Hp / MaxHp * 100), damage: damage,
                                        hitmode: hitmode,
                                        skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                }

                                break;

                            case TargetHitType.AoeTargetHit:
                                switch (hitmode)
                                {
                                    case 1:
                                    case 4:
                                        hitmode = 7;
                                        break;

                                    case 2:
                                        hitmode = 2;
                                        break;

                                    case 3:
                                        hitmode = 6;
                                        break;

                                    default:
                                        hitmode = 5;
                                        break;
                                }

                                BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: attackerBattleEntity.UserType,
                                    callerId: attackerBattleEntity.MapEntityId,
                                    secondaryType: (byte)BattleEntity.UserType,
                                    targetId: BattleEntity.MapEntityId,
                                    skillVNum: hitRequest.Skill.SkillVNum,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.Skill.AttackAnimation,
                                    skillEffect: hitRequest.SkillEffect,
                                    x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                    isAlive: IsAlive, health: (int)(Hp / MaxHp * 100), damage: damage,
                                    hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                break;

                            case TargetHitType.ZoneHit:
                                BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: attackerBattleEntity.UserType,
                                    callerId: attackerBattleEntity.MapEntityId,
                                    secondaryType: (byte)BattleEntity.UserType,
                                    targetId: BattleEntity.MapEntityId,
                                    skillVNum: hitRequest.Skill.SkillVNum,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.Skill.AttackAnimation,
                                    skillEffect: hitRequest.SkillEffect, x: hitRequest.MapX,
                                    y: hitRequest.MapY, isAlive: IsAlive, health: (int)(Hp / MaxHp * 100),
                                    damage: damage, hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                break;

                            case TargetHitType.SpecialZoneHit:
                                BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                    type: attackerBattleEntity.UserType,
                                    callerId: attackerBattleEntity.MapEntityId,
                                    secondaryType: (byte)BattleEntity.UserType,
                                    targetId: BattleEntity.MapEntityId,
                                    skillVNum: hitRequest.Skill.SkillVNum,
                                    cooldown: (short)(hitRequest.Skill.Cooldown +
                                                       hitRequest.Skill.Cooldown * cooldownReduction / 100D),
                                    attackAnimation: hitRequest.Skill.AttackAnimation,
                                    skillEffect: hitRequest.SkillEffect,
                                    x: attackerBattleEntity.PositionX, y: attackerBattleEntity.PositionY,
                                    isAlive: IsAlive, health: (int)(Hp / MaxHp * 100), damage: damage,
                                    hitmode: hitmode,
                                    skillType: (byte)(hitRequest.Skill.SkillType - 1)));
                                break;
                        }
                    else
                        BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.SkillUsed(
                            type: attackerBattleEntity.UserType,
                            callerId: attackerBattleEntity.MapEntityId, secondaryType: (byte)BattleEntity.UserType,
                            targetId: BattleEntity.MapEntityId,
                            skillVNum: 0,
                            cooldown: (short)(hitRequest.Mate != null ? hitRequest.Mate.Monster.BasicCooldown : 12),
                            attackAnimation: 11,
                            skillEffect: (short)(hitRequest.Mate != null ? hitRequest.Mate.Monster.BasicSkill : 12),
                            x: 0, y: 0, isAlive: IsAlive,
                            health: (int)(Hp / MaxHp * 100), damage: damage, hitmode: hitmode, skillType: 0));
                }
                else
                {
                    hitRequest.Session?.SendPacket(
                        packet: StaticPacketHelper.Cancel(type: 2, callerId: BattleEntity.MapEntityId));
                }

                if (attackerBattleEntity.Character != null)
                {
                    if (hitmode != 4 && hitmode != 2 && damage > 0)
                        attackerBattleEntity.Character.RemoveBuffByBCardTypeSubType(
                            bcardTypes: new List<KeyValuePair<byte, byte>>
                            {
                                new KeyValuePair<byte, byte>(key: (byte) CardType.SpecialActions,
                                    value: (byte) AdditionalTypes.SpecialActions.Hide)
                            });

                    if (attackerBattleEntity.HasBuff(type: CardType.FalconSkill,
                        subtype: (byte)AdditionalTypes.FalconSkill.Hide))
                    {
                        attackerBattleEntity.Character.RemoveBuffByBCardTypeSubType(
                            bcardTypes: new List<KeyValuePair<byte, byte>>
                            {
                                new KeyValuePair<byte, byte>(key: (byte) CardType.FalconSkill,
                                    value: (byte) AdditionalTypes.FalconSkill.Hide)
                            });
                        attackerBattleEntity.AddBuff(
                            indicator: new Buff.Buff(id: 560, level: attackerBattleEntity.Level),
                            sender: attackerBattleEntity);
                    }

                    if (Hp <= 0)
                        if (hitRequest.SkillBCards.FirstOrDefault(predicate: s =>
                            s.Type == (byte)CardType.TauntSkill &&
                            s.SubType == (byte)AdditionalTypes.TauntSkill.EffectOnKill / 10) is { } effectOnKill)
                            if (ServerManager.RandomNumber() < effectOnKill.FirstData)
                                attackerBattleEntity.AddBuff(
                                    indicator: new Buff.Buff(id: (short)effectOnKill.SecondData,
                                        level: attackerBattleEntity.Level),
                                    sender: attackerBattleEntity);
                }
            }
            else
            {
                hitRequest.Session?.SendPacket(
                    packet: StaticPacketHelper.Cancel(type: 2, callerId: BattleEntity.MapEntityId));
            }
        }

        /// <summary>
        ///     Hit the Target.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="npcMonsterSkill"></param>
        public void TargetHit(BattleEntity target, NpcMonsterSkill npcMonsterSkill)
        {
            if (Monster != null && !HasBuff(type: CardType.SpecialAttack,
                    subtype: (byte)AdditionalTypes.SpecialAttack.NoAttack) &&
                BattleEntity.CanAttackEntity(receiver: target))
            {
                if (npcMonsterSkill != null)
                {
                    if (BattleEntity.Mp < npcMonsterSkill.Skill.MpCost) return;

                    if (!npcMonsterSkill.CanBeUsed()) return;

                    if (npcMonsterSkill.Skill.TargetType == 0 &&
                        BattleEntity.GetDistance(other: target) > npcMonsterSkill.Skill.Range) return;

                    npcMonsterSkill.LastSkillUse = DateTime.Now;

                    DecreaseMp(amount: npcMonsterSkill.Skill.MpCost);

                    BattleEntity.MapInstance?.Broadcast(packet: StaticPacketHelper.CastOnTarget(
                        attackerType: BattleEntity.UserType,
                        attackerId: BattleEntity.MapEntityId, defenderType: target.UserType,
                        defenderId: target.MapEntityId,
                        castAnimation: npcMonsterSkill.Skill.CastAnimation,
                        castEffect: npcMonsterSkill.Skill.CastEffect,
                        skillVNum: npcMonsterSkill.Skill.SkillVNum));
                }
                else
                {
                    if (!CanUseBasicSkill()) return;

                    if (BattleEntity.GetDistance(other: target) > Monster.BasicRange) return;

                    LastBasicSkillUse = DateTime.Now;
                }

                Owner.Session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                    callerId: MateTransportId, effectId: 5005));

                LastSkillUse = DateTime.Now;

                var hitmode = 0;
                var onyxWings = false;
                BattleEntity targetEntity = null;
                switch (target.EntityType)
                {
                    case EntityType.Player:
                        targetEntity = new BattleEntity(character: target.Character, skill: null);
                        break;
                    case EntityType.Mate:
                        targetEntity = new BattleEntity(mate: target.Mate);
                        break;
                    case EntityType.Monster:
                        targetEntity = new BattleEntity(monster: target.MapMonster);
                        break;
                    case EntityType.Npc:
                        targetEntity = new BattleEntity(npc: target.MapNpc);
                        break;
                }

                var damage = DamageHelper.Instance.CalculateDamage(attacker: new BattleEntity(mate: this),
                    defender: targetEntity, skill: npcMonsterSkill?.Skill, hitMode: ref hitmode,
                    onyxWings: ref onyxWings);

                // deal 0 damage to GM with GodMode
                if (target.Character != null && target.Character.HasGodMode ||
                    target.Mate != null && target.Mate.Owner.HasGodMode) damage = 0;

                if (target.Character != null)
                    if (ServerManager.RandomNumber() < target.Character.GetBuff(type: CardType.DarkCloneSummon,
                        subtype: (byte)AdditionalTypes.DarkCloneSummon.ConvertDamageToHpChance)[0])
                    {
                        var amount = damage;

                        target.Character.ConvertedDamageToHP += amount;
                        target.Character.MapInstance?.Broadcast(
                            packet: target.Character.GenerateRc(characterHealth: amount));
                        target.Character.Hp += amount;

                        if (target.Character.Hp > target.Character.HPLoad())
                            target.Character.Hp = (int)target.Character.HPLoad();

                        target.Character.Session?.SendPacket(packet: target.Character.GenerateStat());

                        damage = 0;
                    }

                var manaShield = target.GetBuff(type: CardType.LightAndShadow,
                    subtype: (byte)AdditionalTypes.LightAndShadow.InflictDamageToMp);
                if (manaShield[0] != 0 && hitmode != 4)
                {
                    var reduce = damage / 100 * manaShield[0];
                    if (target.Mp < reduce)
                    {
                        reduce = target.Mp;
                        target.Mp = 0;
                    }
                    else
                    {
                        target.DecreaseMp(amount: reduce);
                    }

                    damage -= reduce;
                }

                if (target.Character != null && target.Character.IsSitting)
                {
                    target.Character.IsSitting = false;
                    BattleEntity.MapInstance?.Broadcast(packet: target.Character.GenerateRest());
                }

                var castTime = 0;
                if (npcMonsterSkill != null && npcMonsterSkill.Skill.CastEffect != 0)
                {
                    BattleEntity.MapInstance?.Broadcast(
                        packet: StaticPacketHelper.GenerateEff(effectType: BattleEntity.UserType,
                            callerId: BattleEntity.MapEntityId,
                            effectId: npcMonsterSkill.Skill.CastEffect), xRangeCoordinate: MapX,
                        yRangeCoordinate: MapY);
                    castTime = npcMonsterSkill.Skill.CastTime * 100;
                }

                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: castTime)).Subscribe(onNext: o =>
                {
                    if (target.Hp > 0)
                        TargetHit2(target: target, npcMonsterSkill: npcMonsterSkill, damage: damage, hitmode: hitmode);
                });
            }
        }

        public void TargetHit2(BattleEntity target, NpcMonsterSkill npcMonsterSkill, int damage, int hitmode)
        {
            var bCards = new List<BCard>();
            bCards.AddRange(collection: Monster.BCards.ToList());
            if (npcMonsterSkill != null) bCards.AddRange(collection: npcMonsterSkill.Skill.BCards.ToList());

            lock (target.PVELockObject)
            {
                if (target.Hp > 0 && target.MapInstance == BattleEntity.MapInstance)
                {
                    if (target.MapMonster != null)
                    {
                        target.MapMonster.HitQueue.Enqueue(
                            item: new HitRequest(targetHitType: TargetHitType.SingleTargetHit, session: Owner.Session,
                                mate: this, skill: npcMonsterSkill));
                    }
                    else if (target.Mate != null)
                    {
                        target.Mate.HitRequest(hitRequest: new HitRequest(targetHitType: TargetHitType.SingleTargetHit,
                            session: Owner.Session, mate: this,
                            skill: npcMonsterSkill));
                    }
                    else
                    {
                        if (damage >= target.Hp &&
                            Monster.BCards.Any(predicate: s =>
                                s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                                s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoKill / 10 &&
                                s.FirstData == 1))
                            damage = target.Hp - 1;

                        target.GetDamage(damage: damage, damager: BattleEntity);

                        if (target.Character != null)
                            BattleEntity.MapInstance.Broadcast(client: null, content: target.Character.GenerateStat(),
                                receiver: ReceiverType.OnlySomeone,
                                characterName: "", characterId: target.MapEntityId);
                        if (target.Mate != null)
                            target.Mate.Owner.Session.SendPacket(packet: target.Mate.Owner.GeneratePst()
                                .FirstOrDefault(predicate: s =>
                                    s.Contains(value: target.Mate.MateTransportId.ToString())));
                        if (target.MapMonster != null && Owner != null)
                            target.MapMonster.AddToDamageList(damagerEntity: Owner.BattleEntity, damage: damage);
                        BattleEntity.MapInstance.Broadcast(packet: npcMonsterSkill != null
                            ? StaticPacketHelper.SkillUsed(type: BattleEntity.UserType,
                                callerId: BattleEntity.MapEntityId,
                                secondaryType: (byte)target.UserType, targetId: target.MapEntityId,
                                skillVNum: npcMonsterSkill.SkillVNum, cooldown: npcMonsterSkill.Skill.Cooldown,
                                attackAnimation: npcMonsterSkill.Skill.AttackAnimation,
                                skillEffect: npcMonsterSkill.Skill.Effect, x: MapX, y: MapY,
                                isAlive: target.Hp > 0,
                                health: (int)(target.Hp / target.HPLoad() * 100), damage: damage,
                                hitmode: hitmode, skillType: 0)
                            : StaticPacketHelper.SkillUsed(type: BattleEntity.UserType,
                                callerId: BattleEntity.MapEntityId,
                                secondaryType: (byte)target.UserType, targetId: target.MapEntityId, skillVNum: 0,
                                cooldown: Monster.BasicCooldown, attackAnimation: 11, skillEffect: Monster.BasicSkill,
                                x: 0, y: 0, isAlive: target.Hp > 0,
                                health: (int)(target.Hp / target.HPLoad() * 100), damage: damage,
                                hitmode: hitmode, skillType: 0));

                        if (hitmode != 4 && hitmode != 2)
                        {
                            bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                                action: s =>
                                {
                                    if (s.Type != (byte)CardType.Buff)
                                        s.ApplyBCards(session: target, sender: BattleEntity);
                                });

                            bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                                action: s =>
                                {
                                    if (s.Type == (byte)CardType.Buff)
                                    {
                                        var b = new Buff.Buff(id: (short)s.SecondData, level: Monster.Level);
                                        if (b.Card != null)
                                            switch (b.Card?.BuffType)
                                            {
                                                case BuffType.Bad:
                                                    s.ApplyBCards(session: target, sender: BattleEntity);
                                                    break;

                                                case BuffType.Good:
                                                case BuffType.Neutral:
                                                    s.ApplyBCards(session: BattleEntity, sender: BattleEntity);
                                                    break;
                                            }
                                    }
                                });

                            target.BCards.Where(predicate: s => s.CastType == 0).ForEach(action: s =>
                            {
                                if (s.Type == (byte)CardType.Buff)
                                {
                                    var b = new Buff.Buff(id: (short)s.SecondData, level: BattleEntity.Level);
                                    if (b.Card != null)
                                        switch (b.Card?.BuffType)
                                        {
                                            case BuffType.Bad:
                                                s.ApplyBCards(session: BattleEntity, sender: target);
                                                break;

                                            case BuffType.Good:
                                            case BuffType.Neutral:
                                                s.ApplyBCards(session: target, sender: target);
                                                break;
                                        }
                                }
                            });

                            if (damage > 0)
                            {
                                target.Character?.RemoveBuffByBCardTypeSubType(
                                    bcardTypes: new List<KeyValuePair<byte, byte>>
                                    {
                                        new KeyValuePair<byte, byte>(key: (byte) CardType.SpecialActions,
                                            value: (byte) AdditionalTypes.SpecialActions.Hide)
                                    });
                                target.RemoveBuff(id: 36);
                                target.RemoveBuff(id: 548);
                            }
                        }

                        if (target.Hp <= 0)
                        {
                            if (target.Character != null)
                            {
                                if (target.Character.IsVehicled) target.Character.RemoveVehicle();
                                if (Owner != null)
                                    Owner.MapInstance?.Broadcast(packet: Owner.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "PVP_KILL"),
                                            arg0: Owner.Name, arg1: target.Character.Name), type: 10));
                                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000)).Subscribe(onNext: o =>
                                    ServerManager.Instance.AskPvpRevive(
                                        characterId: (long)target.Character?.CharacterId));
                            }
                            else if (target.MapNpc != null)
                            {
                                target.MapNpc.RunDeathEvent();
                            }
                        }
                    }
                }
            }

            // In range entities

            var rangeBaseX = target.PositionX;
            var rangeBaseY = target.PositionY;

            if (npcMonsterSkill != null && npcMonsterSkill.Skill.HitType == 1 && npcMonsterSkill.Skill.TargetType == 1)
            {
                rangeBaseX = MapX;
                rangeBaseY = MapY;
            }

            if (npcMonsterSkill != null && (npcMonsterSkill.Skill.Range > 0 || npcMonsterSkill.Skill.TargetRange > 0))
            {
                var onyxWings = false;
                foreach (var characterInRange in BattleEntity.MapInstance
                    .GetCharactersInRange(
                        mapX: npcMonsterSkill.Skill.TargetRange == 0 ? MapX : rangeBaseX,
                        mapY: npcMonsterSkill.Skill.TargetRange == 0 ? MapY : rangeBaseY,
                        distance: npcMonsterSkill.Skill.TargetRange)
                    .Where(predicate: s => s.CharacterId != target.MapEntityId))
                {
                    if (!BattleEntity.CanAttackEntity(receiver: characterInRange.BattleEntity))
                    {
                        npcMonsterSkill.Skill.BCards.Where(predicate: s => s.Type == (byte)CardType.Buff).ToList()
                            .ForEach(action: s =>
                            {
                                if (new Buff.Buff(id: (short)s.SecondData, level: Monster.Level) is { } b)
                                    switch (b.Card?.BuffType)
                                    {
                                        case BuffType.Good:
                                        case BuffType.Neutral:
                                            s.ApplyBCards(session: characterInRange.BattleEntity, sender: BattleEntity);
                                            break;
                                    }
                            });
                    }
                    else
                    {
                        if (characterInRange.IsSitting)
                        {
                            characterInRange.IsSitting = false;
                            BattleEntity.MapInstance.Broadcast(packet: characterInRange.GenerateRest());
                        }

                        if (characterInRange.HasGodMode) hitmode = 4;

                        if (characterInRange.Hp > 0)
                        {
                            var dmg = DamageHelper.Instance.CalculateDamage(attacker: BattleEntity,
                                defender: characterInRange.BattleEntity,
                                skill: npcMonsterSkill.Skill, hitMode: ref hitmode, onyxWings: ref onyxWings);
                            if (dmg >= characterInRange.Hp &&
                                Monster.BCards.Any(predicate: s =>
                                    s.Type == (byte)CardType.NoDefeatAndNoDamage &&
                                    s.SubType == (byte)AdditionalTypes.NoDefeatAndNoDamage.DecreaseHpNoKill / 10 &&
                                    s.FirstData == 1))
                                dmg = characterInRange.Hp - 1;

                            if (hitmode != 4 && hitmode != 2)
                            {
                                bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                                    action: s =>
                                    {
                                        if (s.Type != (byte)CardType.Buff)
                                            s.ApplyBCards(session: characterInRange.BattleEntity, sender: BattleEntity);
                                    });

                                if (dmg > 0)
                                {
                                    characterInRange.RemoveBuff(cardId: 36);
                                    characterInRange.RemoveBuff(cardId: 548);
                                }

                                bCards.Where(predicate: s => s.CastType == 1 || s.SkillVNum != null).ToList().ForEach(
                                    action: s =>
                                    {
                                        if (s.Type == (byte)CardType.Buff)
                                        {
                                            var b = new Buff.Buff(id: (short)s.SecondData, level: Monster.Level);
                                            if (b.Card != null)
                                                switch (b.Card?.BuffType)
                                                {
                                                    case BuffType.Bad:
                                                        s.ApplyBCards(session: characterInRange.BattleEntity,
                                                            sender: BattleEntity);
                                                        break;

                                                    case BuffType.Good:
                                                    case BuffType.Neutral:
                                                        s.ApplyBCards(session: BattleEntity, sender: BattleEntity);
                                                        break;
                                                }
                                        }
                                    });

                                characterInRange.BattleEntity.BCards.Where(predicate: s => s.CastType == 0).ForEach(
                                    action: s =>
                                    {
                                        if (s.Type == (byte)CardType.Buff)
                                        {
                                            var b = new Buff.Buff(id: (short)s.SecondData, level: BattleEntity.Level);
                                            if (b.Card != null)
                                                switch (b.Card?.BuffType)
                                                {
                                                    case BuffType.Bad:
                                                        s.ApplyBCards(session: BattleEntity,
                                                            sender: characterInRange.BattleEntity);
                                                        break;

                                                    case BuffType.Good:
                                                    case BuffType.Neutral:
                                                        s.ApplyBCards(session: characterInRange.BattleEntity,
                                                            sender: characterInRange.BattleEntity);
                                                        break;
                                                }
                                        }
                                    });
                            }

                            characterInRange.GetDamage(damage: dmg, damager: BattleEntity);
                            BattleEntity.MapInstance.Broadcast(client: null, content: characterInRange.GenerateStat(),
                                receiver: ReceiverType.OnlySomeone,
                                characterName: "", characterId: characterInRange.CharacterId);

                            BattleEntity.MapInstance.Broadcast(packet: npcMonsterSkill != null
                                ? StaticPacketHelper.SkillUsed(type: BattleEntity.UserType,
                                    callerId: BattleEntity.MapEntityId,
                                    secondaryType: (byte)UserType.Player, targetId: characterInRange.CharacterId,
                                    skillVNum: npcMonsterSkill.SkillVNum, cooldown: npcMonsterSkill.Skill.Cooldown,
                                    attackAnimation: npcMonsterSkill.Skill.AttackAnimation,
                                    skillEffect: npcMonsterSkill.Skill.Effect, x: MapX, y: MapY,
                                    isAlive: characterInRange.Hp > 0,
                                    health: (int)(characterInRange.Hp / characterInRange.HPLoad() * 100), damage: dmg,
                                    hitmode: hitmode, skillType: 0)
                                : StaticPacketHelper.SkillUsed(type: BattleEntity.UserType,
                                    callerId: BattleEntity.MapEntityId,
                                    secondaryType: (byte)UserType.Player, targetId: characterInRange.CharacterId,
                                    skillVNum: 0,
                                    cooldown: Monster.BasicCooldown, attackAnimation: 11,
                                    skillEffect: Monster.BasicSkill, x: 0, y: 0, isAlive: characterInRange.Hp > 0,
                                    health: (int)(characterInRange.Hp / characterInRange.HPLoad() * 100), damage: dmg,
                                    hitmode: hitmode, skillType: 0));

                            if (hitmode != 4 && hitmode != 2)
                                characterInRange.RemoveBuffByBCardTypeSubType(
                                    bcardTypes: new List<KeyValuePair<byte, byte>>
                                    {
                                        new KeyValuePair<byte, byte>(key: (byte) CardType.SpecialActions,
                                            value: (byte) AdditionalTypes.SpecialActions.Hide)
                                    });
                            if (characterInRange.Hp <= 0)
                            {
                                if (characterInRange.IsVehicled) characterInRange.RemoveVehicle();
                                Owner.MapInstance?.Broadcast(packet: Owner.GenerateSay(
                                    message: string.Format(format: Language.Instance.GetMessageFromKey(key: "PVP_KILL"),
                                        arg0: Owner.Name, arg1: characterInRange?.Name), type: 10));
                                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000)).Subscribe(onNext: o =>
                                {
                                    ServerManager.Instance.AskPvpRevive(
                                        characterId: (long)characterInRange?.CharacterId); //Cleanup not possible
                                });
                            }
                        }
                    }

                    foreach (var mateInRange in BattleEntity.MapInstance
                        .GetListMateInRange(
                            mapX: npcMonsterSkill.Skill.TargetRange == 0 ? BattleEntity.PositionX : rangeBaseX,
                            mapY: npcMonsterSkill.Skill.TargetRange == 0 ? BattleEntity.PositionY : rangeBaseY,
                            distance: npcMonsterSkill.Skill.TargetRange))
                        if (!BattleEntity.CanAttackEntity(receiver: mateInRange.BattleEntity))
                            npcMonsterSkill.Skill.BCards.Where(predicate: s => s.Type == (byte)CardType.Buff).ToList()
                                .ForEach(
                                    action: s =>
                                    {
                                        if (new Buff.Buff(id: (short)s.SecondData, level: Monster.Level) is { } b)
                                            switch (b.Card?.BuffType)
                                            {
                                                case BuffType.Good:
                                                case BuffType.Neutral:
                                                    s.ApplyBCards(session: mateInRange.BattleEntity,
                                                        sender: BattleEntity);
                                                    break;
                                            }
                                    });
                        else
                            mateInRange.HitRequest(hitRequest: new HitRequest(targetHitType: TargetHitType.AoeTargetHit,
                                session: Owner.Session, mate: this,
                                skill: npcMonsterSkill));

                    foreach (var monsterInRange in BattleEntity.MapInstance
                        .GetMonsterInRangeList(
                            mapX: npcMonsterSkill.Skill.TargetRange == 0 ? BattleEntity.PositionX : rangeBaseX,
                            mapY: npcMonsterSkill.Skill.TargetRange == 0 ? BattleEntity.PositionY : rangeBaseY,
                            distance: npcMonsterSkill.Skill.TargetRange)
                        .Where(predicate: s => s.MapMonsterId != target.MapEntityId))
                        if (!BattleEntity.CanAttackEntity(receiver: monsterInRange.BattleEntity))
                            npcMonsterSkill.Skill.BCards.Where(predicate: s => s.Type == (byte)CardType.Buff).ToList()
                                .ForEach(
                                    action: s =>
                                    {
                                        if (new Buff.Buff(id: (short)s.SecondData, level: Monster.Level) is { } b)
                                            switch (b.Card?.BuffType)
                                            {
                                                case BuffType.Good:
                                                case BuffType.Neutral:
                                                    s.ApplyBCards(session: monsterInRange.BattleEntity,
                                                        sender: BattleEntity);
                                                    break;
                                            }
                                    });
                        else
                            monsterInRange.HitQueue.Enqueue(item: new HitRequest(
                                targetHitType: TargetHitType.AoeTargetHit, session: Owner.Session,
                                mate: this, skill: npcMonsterSkill));
                }
            }
        }

        public void HitTrainer(int trainerVnum, int amount = 1)
        {
            var canDown = trainerVnum != 636 && trainerVnum != 971;

            TrainerHits += amount;
            if (TrainerHits >= MateHelper.Instance.TrainerUpgradeHits[Attack])
            {
                TrainerHits = 0;

                int upRate = MateHelper.Instance.TrainerUpRate[Attack];
                int downRate = MateHelper.Instance.TrainerDownRate[Attack];

                var rnd = ServerManager.RandomNumber();

                if (downRate < upRate)
                {
                    if (rnd < downRate && canDown)
                        DownAttack();
                    else if (rnd < upRate)
                        UpAttack();
                    else
                        EqualAttack();
                }
                else
                {
                    if (rnd < upRate)
                        UpAttack();
                    else if (rnd < downRate && canDown)
                        DownAttack();
                    else
                        EqualAttack();
                }

                void UpAttack()
                {
                    if (Attack < 10)
                    {
                        Attack++;
                        BattleEntity.AttackUpgrade++;
                        Owner.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "MATE_ATTACK_CHANGED"), arg0: Attack),
                            type: 0));
                        Owner.Session.SendPacket(packet: Owner.GenerateSay(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "MATE_ATTACK_CHANGED"), arg0: Attack),
                            type: 12));
                    }
                    else
                    {
                        Owner.Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "MATE_MAX_ATTACK"), type: 0));
                    }
                }

                void DownAttack()
                {
                    if (Attack > 0)
                    {
                        Attack--;
                        BattleEntity.AttackUpgrade--;
                        Owner.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "MATE_ATTACK_CHANGED"), arg0: Attack),
                            type: 0));
                        Owner.Session.SendPacket(packet: Owner.GenerateSay(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "MATE_ATTACK_CHANGED"), arg0: Attack),
                            type: 12));
                    }
                    else
                    {
                        Owner.Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "MATE_MIN_ATTACK"), type: 0));
                    }
                }

                void EqualAttack()
                {
                    Owner.Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "MATE_ATTACK_EQUAL"), type: 0));
                    Owner.Session.SendPacket(packet: Owner.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "MATE_ATTACK_EQUAL"),
                        type: 12));
                }

                Owner.Session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());
                Owner.Session.SendPackets(packets: Owner.GenerateScP());
                Owner.Session.SendPackets(packets: Owner.GenerateScN());
            }
        }

        public void DefendTrainer(int trainerVnum, int amount = 1)
        {
            var canDown = trainerVnum != 636 && trainerVnum != 971;

            TrainerDefences += amount;
            if (TrainerDefences >= MateHelper.Instance.TrainerUpgradeHits[Defence])
            {
                TrainerDefences = 0;
                int upRate = MateHelper.Instance.TrainerUpRate[Defence];
                int downRate = MateHelper.Instance.TrainerDownRate[Defence];

                var rnd = ServerManager.RandomNumber();

                if (downRate < upRate)
                {
                    if (rnd < downRate && canDown)
                        DownDefence();
                    else if (rnd < upRate)
                        UpDefence();
                    else
                        EqualDefence();
                }
                else
                {
                    if (rnd < upRate)
                        UpDefence();
                    else if (rnd < downRate && canDown)
                        DownDefence();
                    else
                        EqualDefence();
                }

                void UpDefence()
                {
                    if (Defence < 10)
                    {
                        Defence++;
                        BattleEntity.DefenseUpgrade++;
                        Owner.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "MATE_DEFENCE_CHANGED"),
                                arg0: Defence), type: 0));
                        Owner.Session.SendPacket(packet: Owner.GenerateSay(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "MATE_DEFENCE_CHANGED"),
                                arg0: Defence), type: 12));
                    }
                    else
                    {
                        Owner.Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "MATE_MAX_DEFENCE"),
                                type: 0));
                    }
                }

                void DownDefence()
                {
                    if (Defence > 0)
                    {
                        Defence--;
                        BattleEntity.DefenseUpgrade--;
                        Owner.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "MATE_DEFENCE_CHANGED"),
                                arg0: Defence), type: 0));
                        Owner.Session.SendPacket(packet: Owner.GenerateSay(
                            message: string.Format(
                                format: Language.Instance.GetMessageFromKey(key: "MATE_DEFENCE_CHANGED"),
                                arg0: Defence), type: 12));
                    }
                    else
                    {
                        Owner.Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "MATE_MIN_DEFENCE"),
                                type: 0));
                    }
                }

                void EqualDefence()
                {
                    Owner.Session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "MATE_DEFENCE_EQUAL"), type: 0));
                    Owner.Session.SendPacket(
                        packet: Owner.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "MATE_DEFENCE_EQUAL"), type: 12));
                }

                Owner.Session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());
                Owner.Session.SendPackets(packets: Owner.GenerateScP());
                Owner.Session.SendPackets(packets: Owner.GenerateScN());
            }
        }

        #region Members

        NpcMonster _monster;
        Character _owner;
        public object PveLockObject;

        #endregion

        #region Instantiation

        public Mate(MateDto input)
        {
            PveLockObject = new object();
            Attack = input.Attack;
            CanPickUp = input.CanPickUp;
            CharacterId = input.CharacterId;
            Defence = input.Defence;
            Direction = input.Direction;
            Experience = input.Experience;
            Hp = input.Hp;
            IsSummonable = input.IsSummonable;
            IsTeamMember = input.IsTeamMember;
            Level = input.Level;
            Loyalty = input.Loyalty;
            MapX = input.MapX;
            MapY = input.MapY;
            PositionX = MapX;
            PositionY = MapY;
            MateId = input.MateId;
            MateType = input.MateType;
            Mp = input.Mp;
            Name = input.Name;
            NpcMonsterVNum = input.NpcMonsterVNum;
            Skin = input.Skin;
            Skills = new List<NpcMonsterSkill>();
            foreach (var ski in Monster.Skills)
                Skills.Add(item: new NpcMonsterSkill { SkillVNum = ski.SkillVNum, Rate = ski.Rate });
            GenerateMateTransportId();
            IsAlive = true;
            BattleEntity = new BattleEntity(mate: this);
            if (IsTeamMember) AddTeamMember();
            if (Monster.CriticalChance == 0 && Monster.CriticalRate == 0)
                try
                {
                    var streamWriter = new StreamWriter(path: "MissingMateStats.txt", append: true)
                    {
                        AutoFlush = true
                    };
                    streamWriter.WriteLine(value: $"{Monster.NpcMonsterVNum} is missing critical stats.");
                    streamWriter.Close();
                }
                catch (IOException)
                {
                    Logger.Warn(data: "MissingMateStats.txt was in use, but i was able to catch this exception",
                        innerException: null,
                        // ReSharper disable once ExplicitCallerInfoArgument
                        memberName: "MissingMateStats");
                }
        }

        public Mate(Character owner, NpcMonster npcMonster, byte level, MateType matetype, bool temporal = false,
            bool tsReward = false, bool tsProtected = false)
        {
            IsTemporalMate = temporal;
            IsTsReward = tsReward;
            IsTsProtected = tsProtected;
            PveLockObject = new object();
            NpcMonsterVNum = npcMonster.NpcMonsterVNum;
            Monster = npcMonster;
            Level = level;
            Hp = MaxHp;
            Mp = MaxMp;
            Name = npcMonster.Name;
            MateType = matetype;
            Loyalty = 1000;
            PositionY = (short)(owner.PositionY + 1);
            PositionX = (short)(owner.PositionX + 1);
            if (owner.MapInstance.Map.IsBlockedZone(x: MapX, y: MapY))
            {
                PositionY = owner.PositionY;
                PositionX = owner.PositionX;
            }

            MapY = PositionY;
            MapX = PositionX;
            Direction = 2;
            CharacterId = owner.CharacterId;
            Skills = new List<NpcMonsterSkill>();
            foreach (var ski in Monster.Skills)
                Skills.Add(item: new NpcMonsterSkill { SkillVNum = ski.SkillVNum, Rate = ski.Rate });
            Owner = owner;
            GenerateMateTransportId();
            IsAlive = true;
            BattleEntity = new BattleEntity(mate: this);
            if (IsTeamMember) AddTeamMember();
        }

        #endregion

        #region Properties

        public ItemInstance ArmorInstance { get; set; }

        public ItemInstance BootsInstance { get; set; }

        public ThreadSafeSortedList<short, Buff.Buff> Buff
        {
            get => BattleEntity.Buffs;
        }

        public ThreadSafeSortedList<short, IDisposable> BuffObservables
        {
            get => BattleEntity.BuffObservables;
        }

        public int Concentrate
        {
            get => ConcentrateLoad();
        }

        public int DamageMinimum
        {
            get => DamageMinimumLoad();
        }

        public int DamageMaximum
        {
            get => DamageMaximumLoad();
        }

        public ItemInstance GlovesInstance { get; set; }

        public bool IsAlive { get; set; }

        public bool IsSitting { get; set; }

        public bool IsUsingSp { get; set; }

        public DateTime LastHealth { get; set; }

        public DateTime LastDefence { get; set; }

        public DateTime LastSpeedChange { get; set; }

        public DateTime LastSkillUse { get; set; }

        public DateTime LastBasicSkillUse { get; set; }

        public int MagicalDefense
        {
            get => MagicalDefenseLoad();
        }

        public int MateTransportId { get; private set; }

        public double MaxHp
        {
            get => HpLoad();
        }

        public double MaxMp
        {
            get => MpLoad();
        }

        public int MeleeDefense
        {
            get => MeleeDefenseLoad();
        }

        public int MeleeDefenseDodge
        {
            get => MeleeDefenseDodgeLoad();
        }

        public NpcMonster Monster
        {
            get => _monster ?? ServerManager.GetNpcMonster(npcVNum: NpcMonsterVNum);

            set => _monster = value;
        }

        public Character Owner
        {
            get => _owner ?? ServerManager.Instance.GetSessionByCharacterId(characterId: CharacterId)?.Character;
            set => _owner = value;
        }

        public byte PetId { get; set; }

        public short PositionX { get; set; }

        public short PositionY { get; set; }

        public int RangeDefense
        {
            get => RangeDefenseLoad();
        }

        public int RangeDefenseDodge
        {
            get => RangeDefenseDodgeLoad();
        }

        public IDisposable ReviveDisposable { get; set; }

        public List<NpcMonsterSkill> Skills { get; set; }

        public byte Speed
        {
            get
            {
                var tempSpeed = (byte)(Monster.Speed + 4);

                var fixSpeed = (byte)GetBuff(type: CardType.Move, subtype: (byte)AdditionalTypes.Move.SetMovement)[0];
                if (fixSpeed != 0) return fixSpeed;

                tempSpeed += (byte)GetBuff(type: CardType.Move,
                    subtype: (byte)AdditionalTypes.Move.MovementSpeedIncreased)[0];
                tempSpeed = (byte)(tempSpeed * (1 + GetBuff(type: CardType.Move,
                        subtype: (byte)AdditionalTypes.Move.MoveSpeedIncreased)[0] /
                    100D));

                if (tempSpeed >= 59 || tempSpeed < 1) return 1;

                return tempSpeed;
            }
            set
            {
                LastSpeedChange = DateTime.Now;
                Monster.Speed = value > 59 ? (byte)59 : value;
            }
        }

        public int SpCooldown { get; set; }

        public DateTime LastSpCooldown { get; set; }

        public PartnerSp Sp { get; set; }

        public bool IsTemporalMate { get; set; }

        public bool IsTsReward { get; set; }

        public bool IsTsProtected { get; set; }

        public int TrainerHits { get; set; }

        public int TrainerDefences { get; set; }

        public ItemInstance WeaponInstance { get; set; }

        public DateTime LastMonsterAggro { get; set; }

        public Node[][] BrushFireJagged { get; set; }

        public List<EventContainer> OnDeathEvents
        {
            get => BattleEntity.OnDeathEvents;
        }

        #endregion

        #region BattleEntityProperties

        public BattleEntity BattleEntity { get; set; }

        public void AddBuff(Buff.Buff indicator, BattleEntity battleEntity)
        {
            BattleEntity.AddBuff(indicator: indicator, sender: battleEntity);
        }

        public void RemoveBuff(short cardId)
        {
            BattleEntity.RemoveBuff(id: cardId);
        }

        public int[] GetBuff(CardType type, byte subtype)
        {
            return BattleEntity != null ? BattleEntity.GetBuff(type: type, subtype: subtype) : new[] { 0, 0 };
        }

        public bool HasBuff(CardType type, byte subtype)
        {
            return BattleEntity != null ? BattleEntity.HasBuff(type: type, subtype: subtype) : false;
        }

        public void DisableBuffs(BuffType type, int level = 100)
        {
            BattleEntity.DisableBuffs(type: type, level: level);
        }

        public void DisableBuffs(List<BuffType> types, int level = 100)
        {
            BattleEntity.DisableBuffs(types: types, level: level);
        }

        public void DecreaseMp(int amount)
        {
            BattleEntity.DecreaseMp(amount: amount);
        }

        #endregion

        #region Methods

        public bool CanUseBasicSkill()
        {
            return Monster != null &&
                   LastBasicSkillUse.AddMilliseconds(value: Monster.BasicCooldown * 100) < DateTime.Now;
        }

        public void StartSpCooldown()
        {
            if (Sp != null)
            {
                SpCooldown = Sp.GetCooldown();

                Observable.Timer(dueTime: TimeSpan.FromSeconds(value: SpCooldown))
                    .Subscribe(onNext: o => Owner?.Session?.SendPacket(packet: "psd 0"));

                Owner?.Session?.SendPacket(packet: $"psd {SpCooldown}");

                LastSpCooldown = DateTime.Now;
            }
        }

        public bool CanUseSp()
        {
            return LastSpCooldown.AddSeconds(value: SpCooldown) < DateTime.Now;
        }

        public int GetSpRemainingCooldown()
        {
            return (int)(LastSpCooldown - DateTime.Now).TotalSeconds + SpCooldown;
        }


        public string GenerateDpski()
        {
            return "dpski";
        }

        public void RemoveSp(bool isBackToMiniland = false)
        {
            if (Owner?.Session == null || Owner.MapInstance == null) return;

            IsUsingSp = false;

            Owner.Session.SendPacket(packet: GenerateScPacket());

            if (IsTeamMember)
            {
                Owner.MapInstance.Broadcast(packet: GenerateCMode(morphId: -1));
                Owner.Session.SendPacket(packet: GenerateDpski());
                Owner.Session.SendPacket(packet: GenerateCond());
                Owner.MapInstance.Broadcast(packet: GenerateOut());

                if (!isBackToMiniland)
                {
                    var isAct4 = ServerManager.Instance.ChannelId == 51;

                    Parallel.ForEach(source: Owner.MapInstance.Sessions.Where(predicate: s => s.Character != null),
                        body: s =>
                        {
                            if (!isAct4 || Owner.Faction == s.Character.Faction)
                                s.SendPacket(packet: GenerateIn(hideNickname: false, isAct4: isAct4));
                            else
                                s.SendPacket(packet: GenerateIn(hideNickname: true, isAct4: true,
                                    receiverAuthority: s.Account.Authority));
                        });
                }

                Owner.Session.SendPacket(packet: Owner.GeneratePinit());
            }
        }

        public void GenerateMateTransportId()
        {
            var nextId = ServerManager.Instance.MateIds.Count > 0 ? ServerManager.Instance.MateIds.Last() + 1 : 2000000;
            ServerManager.Instance.MateIds.Add(item: nextId);
            MateTransportId = nextId;
        }

        public string GenerateCMode(short morphId)
        {
            return $"c_mode 2 {MateTransportId} {morphId} 0 0";
        }

        public string GenerateCond()
        {
            return
                $"cond 2 {MateTransportId} {(HasBuff(type: CardType.SpecialAttack, subtype: (byte)AdditionalTypes.SpecialAttack.NoAttack) || Loyalty <= 0 ? 1 : 0)} {(HasBuff(type: CardType.Move, subtype: (byte)AdditionalTypes.Move.MovementImpossible) || Loyalty <= 0 ? 1 : 0)} {Speed}";
        }

        public string GenerateEInfo()
        {
            return "e_info 10 " +
                   $"{NpcMonsterVNum} " +
                   $"{Level} " +
                   $"{(MateType == MateType.Pet ? Monster.Element : /*SP Element*/0)} " +
                   $"{Monster.AttackClass} " +
                   $"{Monster.ElementRate} " +
                   $"{Attack + (WeaponInstance?.Upgrade ?? 0)} " +
                   $"{DamageMinimum + (WeaponInstance?.Item.DamageMinimum ?? 0)} " +
                   $"{DamageMaximum + (WeaponInstance?.Item.DamageMaximum ?? 0)} " +
                   $"{Concentrate + (WeaponInstance?.Item.HitRate ?? 0)} " +
                   $"{Monster.CriticalChance + (WeaponInstance?.Item.CriticalLuckRate ?? 0)} " +
                   $"{Monster.CriticalRate + (WeaponInstance?.Item.CriticalRate ?? 0)} " +
                   $"{Defence + (ArmorInstance?.Upgrade ?? 0)} " +
                   $"{MeleeDefense + (ArmorInstance?.Item.CloseDefence ?? 0) + (GlovesInstance?.Item.CloseDefence ?? 0) + (BootsInstance?.Item.CloseDefence ?? 0)} " +
                   $"{MeleeDefenseDodge + (ArmorInstance?.Item.DefenceDodge ?? 0) + (GlovesInstance?.Item.DefenceDodge ?? 0) + (BootsInstance?.Item.DefenceDodge ?? 0)} " +
                   $"{RangeDefense + (ArmorInstance?.Item.DistanceDefence ?? 0) + (GlovesInstance?.Item.DistanceDefence ?? 0) + (BootsInstance?.Item.DistanceDefence ?? 0)} " +
                   $"{RangeDefenseDodge + (ArmorInstance?.Item.DistanceDefenceDodge ?? 0) + (GlovesInstance?.Item.DistanceDefenceDodge ?? 0) + (BootsInstance?.Item.DistanceDefenceDodge ?? 0)} " +
                   $"{MagicalDefense + (ArmorInstance?.Item.MagicDefence ?? 0) + (GlovesInstance?.Item.MagicDefence ?? 0) + (BootsInstance?.Item.MagicDefence ?? 0)} " +
                   $"{EquipmentFireResistance + Monster.FireResistance + (GlovesInstance?.FireResistance ?? 0) + (GlovesInstance?.Item.FireResistance ?? 0) + (BootsInstance?.FireResistance ?? 0) + (BootsInstance?.Item.FireResistance ?? 0)} " +
                   $"{EquipmentWaterResistance + Monster.WaterResistance + (GlovesInstance?.WaterResistance ?? 0) + (GlovesInstance?.Item.WaterResistance ?? 0) + (BootsInstance?.WaterResistance ?? 0) + (BootsInstance?.Item.WaterResistance ?? 0)} " +
                   $"{EquipmentLightResistance + Monster.LightResistance + (GlovesInstance?.LightResistance ?? 0) + (GlovesInstance?.Item.LightResistance ?? 0) + (BootsInstance?.LightResistance ?? 0) + (BootsInstance?.Item.LightResistance ?? 0)} " +
                   $"{EquipmentDarkResistance + Monster.DarkResistance + (GlovesInstance?.DarkResistance ?? 0) + (GlovesInstance?.Item.DarkResistance ?? 0) + (BootsInstance?.DarkResistance ?? 0) + (BootsInstance?.Item.DarkResistance ?? 0)} " +
                   $"{MaxHp} " +
                   $"{MaxMp} " +
                   $"-1 {Name.Replace(oldChar: ' ', newChar: '^')}";
        }

        public string GenerateIn(bool hideNickname = false, bool isAct4 = false,
            AuthorityType receiverAuthority = AuthorityType.User)
        {
            if (!IsTemporalMate &&
                (Owner.Invisible || Owner.InvisibleGm || Owner.IsVehicled || Owner.IsSeal || !IsAlive) &&
                Owner.MapInstance.Map.MapId != 20001) return "";

            var name = IsUsingSp ? Sp.GetName() : Name.Replace(oldChar: ' ', newChar: '^');

            if (receiverAuthority >= AuthorityType.Mod)
            {
                hideNickname = false;
                name = $"[{Owner.Faction}]{name}";
            }

            if (hideNickname) name = "!§$%&/()=?*+~#";

            var faction = isAct4 ? (byte)Owner.Faction + 2 : 0;

            return
                $"in 2 {NpcMonsterVNum} {MateTransportId} {PositionX} {PositionY} {Direction} {(int)(Hp / MaxHp * 100)} {(int)(Mp / MaxMp * 100)} 0 {faction} 3 {CharacterId} 1 0 {(IsUsingSp && Sp != null ? Sp.Instance.Item.Morph : Skin != 0 ? Skin : -1)} {name} {(Sp != null ? 1 : 0)} {(IsUsingSp ? 1 : 0)} {(IsUsingSp ? 1 : 0)}{(IsUsingSp ? Sp?.GenerateSkills(withLevel: false) : " 0 0 0")} 0 0 0 0";
        }

        public string GenerateOut()
        {
            return $"out 2 {MateTransportId}";
        }

        public string GenerateRest(bool ownerSit)
        {
            IsSitting = ownerSit ? Owner.IsSitting : !IsSitting;
            return $"rest 2 {MateTransportId} {(IsSitting ? 1 : 0)}";
        }

        /// <summary>
        ///     Get Stuff Buffs Useful for Stats for example
        /// </summary>
        /// <param name="type"></param>
        /// <param name="subtype"></param>
        /// <returns></returns>
        public int[] GetStuffBuff(CardType type, byte subtype)
        {
            var equipmentBCards = new List<BCard>();
            if (WeaponInstance != null) equipmentBCards.AddRange(collection: WeaponInstance.Item.BCards);
            if (ArmorInstance != null) equipmentBCards.AddRange(collection: ArmorInstance.Item.BCards);

            var value1 = 0;
            var value2 = 0;
            foreach (var entry in equipmentBCards.Where(predicate: s =>
                s.Type.Equals(obj: (byte)type) && s.SubType.Equals(obj: (byte)(subtype / 10)) && s.FirstData > 0))
            {
                if (entry.IsLevelScaled)
                    value1 += entry.FirstData * Level;
                else
                    value1 += entry.FirstData;
                value2 += entry.SecondData;
            }

            return new[] { value1, value2 };
        }

        int EquipmentFireResistance
        {
            get =>
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.FireIncreased)[0] +
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
        }

        int EquipmentWaterResistance
        {
            get =>
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.WaterIncreased)[0] +
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
        }

        int EquipmentLightResistance
        {
            get =>
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.LightIncreased)[0] +
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
        }

        int EquipmentDarkResistance
        {
            get =>
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.DarkIncreased)[0] +
                GetStuffBuff(type: CardType.ElementResistance,
                    subtype: (byte)AdditionalTypes.ElementResistance.AllIncreased)[0];
        }

        public string GenerateScPacket()
        {
            if (IsTemporalMate) return "";

            var xp = XpLoad();

            if (xp > int.MaxValue) xp = (int)(xp / 100);

            switch (MateType)
            {
                case MateType.Partner:
                    return
                        "sc_n " +
                        $"{PetId} " +
                        $"{NpcMonsterVNum} " +
                        $"{MateTransportId} " +
                        $"{Level} " +
                        $"{Loyalty} " +
                        $"{Experience} " +
                        $"{(WeaponInstance != null ? $"{WeaponInstance.ItemVNum}.{WeaponInstance.Rare}.{WeaponInstance.Upgrade}" : "-1")} " +
                        $"{(ArmorInstance != null ? $"{ArmorInstance.ItemVNum}.{ArmorInstance.Rare}.{ArmorInstance.Upgrade}" : "-1")} " +
                        $"{(GlovesInstance != null ? $"{GlovesInstance.ItemVNum}.0.0" : "-1")} " +
                        $"{(BootsInstance != null ? $"{BootsInstance.ItemVNum}.0.0" : "-1")} " +
                        "0 0 1 " +
                        $"{WeaponInstance?.Upgrade ?? 0} " +
                        $"{DamageMinimum + (WeaponInstance?.Item.DamageMinimum ?? 0)} " +
                        $"{DamageMaximum + (WeaponInstance?.Item.DamageMaximum ?? 0)} " +
                        $"{Concentrate + (WeaponInstance?.Item.HitRate ?? 0)} " +
                        $"{Monster.CriticalChance + (WeaponInstance?.Item.CriticalLuckRate ?? 0)} " +
                        $"{Monster.CriticalRate + (WeaponInstance?.Item.CriticalRate ?? 0)} " +
                        $"{ArmorInstance?.Upgrade ?? 0} {Monster.CloseDefence + MeleeDefense + (ArmorInstance?.Item.CloseDefence ?? 0) + (GlovesInstance?.Item.CloseDefence ?? 0) + (BootsInstance?.Item.CloseDefence ?? 0)} " +
                        $"{MeleeDefenseDodge + (ArmorInstance?.Item.DefenceDodge ?? 0) + (GlovesInstance?.Item.DefenceDodge ?? 0) + (BootsInstance?.Item.DefenceDodge ?? 0)} " +
                        $"{RangeDefense + (ArmorInstance?.Item.DistanceDefence ?? 0) + (GlovesInstance?.Item.DistanceDefence ?? 0) + (BootsInstance?.Item.DistanceDefence ?? 0)} " +
                        $"{RangeDefenseDodge + (ArmorInstance?.Item.DistanceDefenceDodge ?? 0) + (GlovesInstance?.Item.DistanceDefenceDodge ?? 0) + (BootsInstance?.Item.DistanceDefenceDodge ?? 0)} " +
                        $"{MagicalDefense + (ArmorInstance?.Item.MagicDefence ?? 0) + (GlovesInstance?.Item.MagicDefence ?? 0) + (BootsInstance?.Item.MagicDefence ?? 0)} " +
                        $"{(IsUsingSp ? Sp.Instance.Item.Element : 0)} " +
                        $"{EquipmentFireResistance + Monster.FireResistance + (GlovesInstance?.FireResistance ?? 0) + (GlovesInstance?.Item.FireResistance ?? 0) + (BootsInstance?.FireResistance ?? 0) + (BootsInstance?.Item.FireResistance ?? 0)} " +
                        $"{EquipmentWaterResistance + Monster.WaterResistance + (GlovesInstance?.WaterResistance ?? 0) + (GlovesInstance?.Item.WaterResistance ?? 0) + (BootsInstance?.WaterResistance ?? 0) + (BootsInstance?.Item.WaterResistance ?? 0)} " +
                        $"{EquipmentLightResistance + Monster.LightResistance + (GlovesInstance?.LightResistance ?? 0) + (GlovesInstance?.Item.LightResistance ?? 0) + (BootsInstance?.LightResistance ?? 0) + (BootsInstance?.Item.LightResistance ?? 0)} " +
                        $"{EquipmentDarkResistance + Monster.DarkResistance + (GlovesInstance?.DarkResistance ?? 0) + (GlovesInstance?.Item.DarkResistance ?? 0) + (BootsInstance?.DarkResistance ?? 0) + (BootsInstance?.Item.DarkResistance ?? 0)} " +
                        $"{Hp} " +
                        $"{MaxHp} " +
                        $"{Mp} " +
                        $"{MaxMp} " +
                        "0 " +
                        $"{xp} " +
                        $"{(IsUsingSp ? Sp.GetName() : Name.Replace(oldChar: ' ', newChar: '^'))} " +
                        $"{(IsUsingSp && Sp != null ? Sp.Instance.Item.Morph : Skin != 0 ? Skin : -1)} " +
                        $"{(IsSummonable ? 1 : 0)} " +
                        $"{(Sp != null ? $"{Sp.Instance.ItemVNum}.{Sp.GetXpPercent()}" : "-1")}" +
                        $"{(Sp != null ? Sp.GenerateSkills() : " -1 -1 -1")}";

                case MateType.Pet:
                    return
                        "sc_p " +
                        $"{PetId} " +
                        $"{NpcMonsterVNum} " +
                        $"{MateTransportId} " +
                        $"{Level} " +
                        $"{Loyalty} " +
                        $"{Experience} " +
                        "0 " +
                        $"{Attack} " +
                        $"{DamageMinimum} " +
                        $"{DamageMaximum} " +
                        $"{Concentrate} " +
                        $"{Monster.CriticalChance} " +
                        $"{Monster.CriticalRate} " +
                        $"{Defence} " +
                        $"{MeleeDefense} " +
                        $"{MeleeDefenseDodge} " +
                        $"{RangeDefense} " +
                        $"{RangeDefenseDodge} " +
                        $"{MagicalDefense} " +
                        $"{Monster.Element} " +
                        $"{Monster.FireResistance} " +
                        $"{Monster.WaterResistance} " +
                        $"{Monster.LightResistance} " +
                        $"{Monster.DarkResistance} " +
                        $"{Hp} " +
                        $"{MaxHp} " +
                        $"{Mp} " +
                        $"{MaxMp} " +
                        "0 " +
                        $"{xp} " +
                        $"{(CanPickUp ? 1 : 0)} " +
                        $"{Name.Replace(oldChar: ' ', newChar: '^')} " +
                        $"{(IsSummonable ? 1 : 0)}";
            }

            return "";
        }

        public string GenerateStatInfo()
        {
            return
                $"st 2 {MateTransportId} {Level} 0 {(int)(Hp / MaxHp * 100)} {(int)(Mp / MaxMp * 100)} {Hp} {Mp}{Buff.GetAllItems().Aggregate(seed: "", func: (current, buff) => current + $" {buff.Card.CardId}.{buff.Level}")}";
        }

        public void GenerateXp(int xp)
        {
            if (Level < ServerManager.Instance.Configuration.MaxLevel)
            {
                Experience += (int)(xp * (1 + (Owner.Buff.ContainsKey(key: 122) ? 0.5 : 0)));
                if (Experience >= XpLoad())
                    if (Level + 1 < Owner.Level)
                    {
                        Experience = (long)(Experience - XpLoad());
                        Level++;
                        Hp = MaxHp;
                        Mp = MaxMp;
                        Owner.MapInstance?.Broadcast(
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc, callerId: MateTransportId,
                                effectId: 6),
                            xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
                        Owner.MapInstance?.Broadcast(
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc, callerId: MateTransportId,
                                effectId: 198),
                            xRangeCoordinate: PositionX, yRangeCoordinate: PositionY);
                        RefreshStats();
                    }
            }

            Owner.Session.SendPacket(packet: GenerateScPacket());
        }

        public List<ItemInstance> GetInventory()
        {
            return MateType == MateType.Pet
                ? new List<ItemInstance>()
                : Owner.Inventory.Where(predicate: s => s.Type == (InventoryType)(13 + PetId));
        }

        #region Stats Load

        public int HealthHpLoad()
        {
            var regen =
                GetBuff(type: CardType.Recovery, subtype: (byte)AdditionalTypes.Recovery.HpRecoveryIncreased)[0];
            //- GetBuff(CardType.Recovery, (byte)AdditionalTypes.Recovery.HPRecoveryDecreased)[0];
            return IsSitting ? regen + 50 :
                (DateTime.Now - LastDefence).TotalSeconds > 4 ? regen + 20 : 0;
        }

        public int HealthMpLoad()
        {
            var regen =
                GetBuff(type: CardType.Recovery, subtype: (byte)AdditionalTypes.Recovery.MpRecoveryIncreased)[0];
            //- GetBuff(CardType.Recovery, (byte)AdditionalTypes.Recovery.MPRecoveryDecreased)[0];
            return IsSitting ? regen + 50 :
                (DateTime.Now - LastDefence).TotalSeconds > 4 ? regen + 20 : 0;
        }

        public int HpLoad()
        {
            if (IsTemporalMate) return Monster.MaxHp;

            var multiplicator = 1.0;
            var hp = 0;

            multiplicator += GetBuff(type: CardType.BearSpirit,
                                 subtype: (byte)AdditionalTypes.BearSpirit.IncreaseMaximumHp)[0]
                             / 100D;
            multiplicator +=
                GetBuff(type: CardType.MaxHpmp, subtype: (byte)AdditionalTypes.MaxHpmp.IncreasesMaximumHp)[0] / 100D;
            hp += GetBuff(type: CardType.MaxHpmp, subtype: (byte)AdditionalTypes.MaxHpmp.MaximumHpIncreased)[0];
            hp += GetBuff(type: CardType.MaxHpmp, subtype: (byte)AdditionalTypes.MaxHpmp.MaximumHpmpIncreased)[0];

            return (int)((MateHelper.Instance.HpData[GetMateType(), Level] + hp) * multiplicator);
        }

        public int MpLoad()
        {
            if (IsTemporalMate) return Monster.MaxMp;

            var mp = 0;
            var multiplicator = 1.0;
            multiplicator += GetBuff(type: CardType.BearSpirit,
                                 subtype: (byte)AdditionalTypes.BearSpirit.IncreaseMaximumMp)[0]
                             / 100D;
            multiplicator +=
                GetBuff(type: CardType.MaxHpmp, subtype: (byte)AdditionalTypes.MaxHpmp.IncreasesMaximumMp)[0] / 100D;
            mp += GetBuff(type: CardType.MaxHpmp, subtype: (byte)AdditionalTypes.MaxHpmp.MaximumMpIncreased)[0];
            mp += GetBuff(type: CardType.MaxHpmp, subtype: (byte)AdditionalTypes.MaxHpmp.MaximumHpmpIncreased)[0];

            return (int)((MateHelper.Instance.MpData[GetMateType(), Level] + mp) * multiplicator);
        }

        public int ConcentrateLoad()
        {
            if (IsTemporalMate) return Monster.Concentrate;

            return MateHelper.Instance.Concentrate[GetMateType(), Level] / (MateType == MateType.Partner ? 3 : 1);
        }

        public int DamageMinimumLoad()
        {
            if (IsTemporalMate) return Monster.DamageMinimum;

            return MateHelper.Instance.MinDamageData[GetMateType(), Level] / (MateType == MateType.Partner ? 3 : 1);
        }

        public int DamageMaximumLoad()
        {
            if (IsTemporalMate) return Monster.DamageMaximum;

            return MateHelper.Instance.MaxDamageData[GetMateType(), Level] / (MateType == MateType.Partner ? 3 : 1);
        }

        public int MeleeDefenseLoad()
        {
            if (IsTemporalMate) return Monster.CloseDefence;

            return MateHelper.Instance.MeleeDefenseData[GetMateType(), Level] / (MateType == MateType.Partner ? 3 : 1);
        }

        public int MeleeDefenseDodgeLoad()
        {
            if (IsTemporalMate) return Monster.DefenceDodge;

            return MateHelper.Instance.MeleeDefenseDodgeData[GetMateType(), Level] /
                   (MateType == MateType.Partner ? 3 : 1);
        }

        public int RangeDefenseLoad()
        {
            if (IsTemporalMate) return Monster.DistanceDefence;

            return MateHelper.Instance.RangeDefenseData[GetMateType(), Level] / (MateType == MateType.Partner ? 3 : 1);
        }

        public int RangeDefenseDodgeLoad()
        {
            if (IsTemporalMate) return Monster.DistanceDefenceDodge;

            return MateHelper.Instance.RangeDefenseDodgeData[GetMateType(), Level] /
                   (MateType == MateType.Partner ? 3 : 1);
        }

        public int MagicalDefenseLoad()
        {
            if (IsTemporalMate) return Monster.MagicDefence;

            return MateHelper.Instance.MagicDefenseData[GetMateType(), Level] / (MateType == MateType.Partner ? 3 : 1);
        }

        #endregion

        public string GenerateRc(int characterHealth)
        {
            return $"rc 2 {MateTransportId} {characterHealth} 0";
        }

        /// <summary>
        ///     Checks if the current character is in range of the given position
        /// </summary>
        /// <param name="xCoordinate">The x coordinate of the object to check.</param>
        /// <param name="yCoordinate">The y coordinate of the object to check.</param>
        /// <param name="range">The range of the coordinates to be maximal distanced.</param>
        /// <returns>True if the object is in Range, False if not.</returns>
        public bool IsInRange(int xCoordinate, int yCoordinate, int range)
        {
            return Math.Abs(value: PositionX - xCoordinate) <= range &&
                   Math.Abs(value: PositionY - yCoordinate) <= range;
        }

        public void LoadInventory()
        {
            var inv = GetInventory();

            if (inv.Count == 0) return;

            WeaponInstance = inv.Find(match: s => s.Item.EquipmentSlot == EquipmentType.MainWeapon);
            ArmorInstance = inv.Find(match: s => s.Item.EquipmentSlot == EquipmentType.Armor);
            GlovesInstance = inv.Find(match: s => s.Item.EquipmentSlot == EquipmentType.Gloves);
            BootsInstance = inv.Find(match: s => s.Item.EquipmentSlot == EquipmentType.Boots);

            var partnerSpInstance = inv.Find(match: s => s.Item.EquipmentSlot == EquipmentType.Sp);

            if (partnerSpInstance != null) Sp = new PartnerSp(instance: partnerSpInstance);
        }

        public void BackToMiniland()
        {
            RemoveSp(isBackToMiniland: true);
            ReviveDisposable?.Dispose();
            IsAlive = true;
            RemoveTeamMember();
            Owner.Session.SendPacket(packet: Owner.GeneratePinit());
            Owner.MapInstance.Broadcast(packet: GenerateOut());
        }

        public void GenerateRevive()
        {
            if (Owner == null || IsAlive) return;

            Owner.MapInstance?.Broadcast(packet: GenerateOut());
            IsAlive = true;
            Hp = MaxHp / 2;
            Mp = MaxMp / 2;
            PositionY = (short)(Owner.PositionY + 1);
            PositionX = (short)(Owner.PositionX + 1);
            if (Owner.MapInstance != null && Owner.MapInstance.Map.IsBlockedZone(x: PositionX, y: PositionY))
            {
                PositionY = Owner.PositionY;
                PositionX = Owner.PositionX;
            }

            Parallel.ForEach(
                source: Owner.Session.CurrentMapInstance.Sessions.Where(predicate: s => s.Character != null), body: s =>
                {
                    if (ServerManager.Instance.ChannelId != 51 ||
                        Owner.Session.Character.Faction == s.Character.Faction)
                        s.SendPacket(packet: GenerateIn(hideNickname: false,
                            isAct4: ServerManager.Instance.ChannelId == 51));
                    else
                        s.SendPacket(packet: GenerateIn(hideNickname: true,
                            isAct4: ServerManager.Instance.ChannelId == 51, receiverAuthority: s.Account.Authority));
                });
            //Owner.Session.SendPacket(GenerateCond());
            Owner.Session.SendPacket(packet: Owner.GeneratePinit());
            Owner.Session.SendPackets(packets: Owner.GeneratePst());
            if (Loyalty <= 100)
                Owner.Session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                    callerId: MateTransportId, effectId: 5003));
        }

        public void GenerateDeath(BattleEntity killer)
        {
            if (Hp > 0 || !IsAlive) return;

            IsAlive = false;
            BattleEntity.RemoveOwnedMonsters();
            DisableBuffs(type: BuffType.All);

            if (IsTemporalMate && Owner.Timespace != null) return;

            Owner.Session.SendPacket(packet: GenerateScPacket());
            if (Loyalty > 0 && Loyalty - 50 <= 0)
            {
                Owner.Session.SendPacket(
                    packet: UserInterfaceHelper.GenerateMsg(
                        message: Language.Instance.GetMessageFromKey(key: "MATE_LOYALTY_ZERO"), type: 0));
                Owner.Session.SendPacket(
                    packet: Owner.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "MATE_LOYALTY_ZERO"),
                        type: 11));
                Owner.Session.SendPacket(
                    packet: Owner.GenerateSay(message: Language.Instance.GetMessageFromKey(key: "MATE_NEED_FEED"),
                        type: 11));
            }

            if (MateType == MateType.Pet) Loyalty -= 0;
            if (Loyalty <= 0) Loyalty = 0;

            Owner.Session.SendPacket(packet: GenerateScPacket());

            if (Owner.Session.CurrentMapInstance == Owner.Miniland)
            {
                GenerateRevive();
                return;
            }

            if (MateType == MateType.Pet ? Owner.IsPetAutoRelive : Owner.IsPartnerAutoRelive)
            {
                if (MateType == MateType.Pet)
                {
                    if (Owner.Inventory.CountItem(itemVNum: 2089) > 0)
                    {
                        Owner.Inventory.RemoveItemAmount(vnum: 2089);
                        GenerateRevive();
                        return;
                    }

                    if (Owner.Inventory.CountItem(itemVNum: 10016) > 0)
                    {
                        Owner.Inventory.RemoveItemAmount(vnum: 10016);
                        GenerateRevive();
                        return;
                    }
                }

                if (MateType == MateType.Partner)
                {
                    if (Owner.Inventory.CountItem(itemVNum: 2329) > 0)
                    {
                        Owner.Inventory.RemoveItemAmount(vnum: 2329);
                        GenerateRevive();
                        return;
                    }

                    if (Owner.Inventory.CountItem(itemVNum: 10050) > 0)
                    {
                        Owner.Inventory.RemoveItemAmount(vnum: 10050);
                        GenerateRevive();
                        return;
                    }
                }

                if (Owner.Inventory.CountItem(itemVNum: 1012) < 5)
                {
                    Owner.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "NO_ITEM_REQUIRED"),
                            arg0: ServerManager.GetItem(vnum: 1012).Name), type: 0));
                    if (MateType == MateType.Pet)
                        Owner.IsPetAutoRelive = false;
                    else
                        Owner.IsPartnerAutoRelive = false;
                }
                else
                {
                    Owner.Inventory.RemoveItemAmount(vnum: 1012, amount: 5);
                    ReviveDisposable = Observable.Timer(dueTime: TimeSpan.FromMinutes(value: 3))
                        .Subscribe(onNext: s => GenerateRevive());
                    Owner.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "WILL_BE_BACK"),
                            arg0: MateType), type: 0));
                    return;
                }
            }

            Owner.Session.SendPacket(
                packet: UserInterfaceHelper.GenerateMsg(
                    message: Language.Instance.GetMessageFromKey(key: "BACK_TO_MINILAND"), type: 0));
            BackToMiniland();
        }

        public void RefreshStats()
        {
            MateHelper.Instance.LoadConcentrate();
            MateHelper.Instance.LoadHpData();
            MateHelper.Instance.LoadMinDamageData();
            MateHelper.Instance.LoadMaxDamageData();
            MateHelper.Instance.LoadMpData();
            MateHelper.Instance.LoadXpData();
            MateHelper.Instance.LoadDefences();
        }

        public void AddTeamMember()
        {
            if (Owner.Mates.Any(predicate: m => m.IsTeamMember && m.MateType == MateType)) return;
            IsTeamMember = true;
            IsAlive = true;
            Hp = MaxHp;
            Mp = MaxMp;
            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 2)).Subscribe(onNext: s =>
            {
                if (IsTeamMember)
                {
                    // Add Pet Buff
                    int cardId;
                    if (MateHelper.Instance.MateBuffs.TryGetValue(key: NpcMonsterVNum, value: out cardId) &&
                        Owner.Buff.All(predicate: b => b.Card.CardId != cardId))
                        Owner.AddBuff(indicator: new Buff.Buff(id: (short)cardId, level: Level, isPermaBuff: true),
                            sender: BattleEntity);
                }
            });
            foreach (var skill in Monster.Skills.Where(predicate: sk =>
                MateHelper.Instance.PetSkills.Contains(item: sk.SkillVNum)))
                Owner.Session.SendPacket(packet: Owner.GeneratePetskill(VNum: skill.SkillVNum));
        }

        public void RemoveTeamMember(bool maxHpMp = true)
        {
            IsTeamMember = false;
            if (BattleEntity.MapInstance != Owner.Miniland)
                Owner.Session.CurrentMapInstance.Broadcast(packet: GenerateOut());
            if (maxHpMp)
            {
                Hp = MaxHp;
                Mp = MaxMp;
            }

            // Remove Pet Buffs
            foreach (var mateBuff in Owner.BattleEntity.Buffs.Where(predicate: b =>
                MateHelper.Instance.MateBuffs.Values.Any(predicate: v => v == b.Card.CardId)))
                Owner.RemoveBuff(cardId: mateBuff.Card.CardId, removePermaBuff: true);
            Owner.Session.SendPacket(packet: Owner.GeneratePetskill());
        }

        double XpLoad()
        {
            try
            {
                return MateHelper.Instance.XpData[Level - 1];
            }
            catch
            {
                return 0;
            }
        }

        #endregion
    }
}