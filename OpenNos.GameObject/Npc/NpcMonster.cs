﻿using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class NpcMonster : NpcMonsterDto
    {
        #region Instantiation

        public NpcMonster()
        {
        }

        public NpcMonster(NpcMonsterDto input)
        {
            AmountRequired = input.AmountRequired;
            AttackClass = input.AttackClass;
            AttackUpgrade = input.AttackUpgrade;
            BasicArea = input.BasicArea;
            BasicCooldown = input.BasicCooldown;
            BasicRange = input.BasicRange;
            BasicSkill = input.BasicSkill;
            Catch = input.Catch;
            CloseDefence = input.CloseDefence;
            Concentrate = input.Concentrate;
            CriticalChance = input.CriticalChance;
            CriticalRate = input.CriticalRate;
            DamageMaximum = input.DamageMaximum;
            DamageMinimum = input.DamageMinimum;
            DarkResistance = input.DarkResistance;
            DefenceDodge = input.DefenceDodge;
            DefenceUpgrade = input.DefenceUpgrade;
            DistanceDefence = input.DistanceDefence;
            DistanceDefenceDodge = input.DistanceDefenceDodge;
            Element = input.Element;
            ElementRate = input.ElementRate;
            FireResistance = input.FireResistance;
            HeroLevel = input.HeroLevel;
            HeroXp = input.HeroXp;
            IsHostile = input.IsHostile;
            JobXp = input.JobXp;
            Level = input.Level;
            LightResistance = input.LightResistance;
            MagicDefence = input.MagicDefence;
            MaxHp = input.MaxHp;
            MaxMp = input.MaxMp;
            MonsterType = input.MonsterType;
            Name = input.Name;
            NoAggresiveIcon = input.NoAggresiveIcon;
            NoticeRange = input.NoticeRange;
            NpcMonsterVNum = input.NpcMonsterVNum;
            OriginalNpcMonsterVNum = input.OriginalNpcMonsterVNum;
            Race = input.Race;
            RaceType = input.RaceType;
            RespawnTime = input.RespawnTime;
            Speed = input.Speed;
            VNumRequired = input.VNumRequired;
            WaterResistance = input.WaterResistance;
            Xp = input.Xp;
        }

        #endregion

        #region Properties

        public List<BCard> BCards { get; set; }

        public List<DropDto> Drops { get; set; }

        public short FirstX { get; set; }

        public short FirstY { get; set; }

        public DateTime LastEffect { get; private set; }

        public DateTime LastMove { get; private set; }

        public List<NpcMonsterSkill> Skills { get; set; }

        public List<TeleporterDto> Teleporters { get; set; }

        #endregion

        #region Methods

        public string GenerateEInfo()
        {
            return
                $"e_info 10 {(OriginalNpcMonsterVNum > 0 ? OriginalNpcMonsterVNum : NpcMonsterVNum)} {Level} {Element} {AttackClass} {ElementRate} {AttackUpgrade} {DamageMinimum} {DamageMaximum} {Concentrate} {CriticalChance} {CriticalRate} {DefenceUpgrade} {CloseDefence} {DefenceDodge} {DistanceDefence} {DistanceDefenceDodge} {MagicDefence} {FireResistance} {WaterResistance} {LightResistance} {DarkResistance} {MaxHp} {MaxMp} -1 {Name.Replace(oldChar: ' ', newChar: '^')}";
        }

        public float GetRes(int skillelement)
        {
            switch (skillelement)
            {
                case 0:
                    return FireResistance / 100;

                case 1:
                    return WaterResistance / 100;

                case 2:
                    return LightResistance / 100;

                case 3:
                    return DarkResistance / 100;

                default:
                    return 0f;
            }
        }

        /// <summary>
        ///     Intializes the GameObject, will be injected by AutoMapper after Entity -&gt; GO mapping
        /// </summary>
        public void Initialize()
        {
            Teleporters = ServerManager.Instance.GetTeleportersByNpcVNum(npcMonsterVNum: NpcMonsterVNum);
            Drops = ServerManager.Instance.GetDropsByMonsterVNum(monsterVNum: NpcMonsterVNum);
            LastEffect = LastMove = DateTime.Now;
            Skills = ServerManager.Instance.GetNpcMonsterSkillsByMonsterVNum(npcMonsterVNum: OriginalNpcMonsterVNum > 0
                ? OriginalNpcMonsterVNum
                : NpcMonsterVNum);
            if (Skills.Count == 0 && OriginalNpcMonsterVNum > 0)
                Skills = ServerManager.Instance.GetNpcMonsterSkillsByMonsterVNum(npcMonsterVNum: NpcMonsterVNum);
        }

        #endregion
    }
}