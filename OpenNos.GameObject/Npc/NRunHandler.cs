﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;

namespace OpenNos.GameObject
{
    public static class NRunHandler
    {
        #region Methods

        //NosTale Steuern.
        /*public static void Steuern(ClientSession Session, ClientSession session, NRunPacket packet)
        {
            if (Session.Character.Gold > 100000000)
            {
                var Prozent = 100;
                string ProzentString = Convert.ToString(Prozent);
                var Gold = 100000000;
                string GoldString = Convert.ToString(Gold);
                var Steuern = 5;
                string SteuernString = Convert.ToString(Steuern);
                Session.Character.Gold = Steuern / Gold * Prozent;
                var ergebnis = (Steuern / Gold) * Prozent;
                string ergebnisString = Convert.ToString(ergebnis);
                Session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromergenisString("DU_HAST_STEUERN_IN_HÖHE_VON", ergebnisString)), 0));
            }
            else
            {
                Parallel.ForEach(ServerManager.Instance.Sessions, session => session.Disconnect());
            }
        }*/

        public static void NRun(ClientSession Session, NRunPacket packet)
        {
            if (!Session.HasCurrentMapInstance) return;

            var npc = Session.CurrentMapInstance.Npcs.Find(match: s => s.MapNpcId == packet.NpcId);

            TeleporterDto tp;

            var rand = new Random();
            switch (packet.Runner)
            {
                case 1:
                    if (Session.Character.Class != (byte)ClassType.Adventurer)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_ADVENTURER"), type: 0));
                        return;
                    }

                    if (Session.Character.Level < 15 || Session.Character.JobLevel < 20)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "LOW_LVL"), type: 0));
                        return;
                    }

                    if (packet.Type > 3 || packet.Type < 1) return;
                    if (packet.Type == (byte)Session.Character.Class) return;
                    if (Session.Character.Inventory.All(predicate: i => i.Type != InventoryType.Wear))
                    {
                        Session.Character.Inventory.AddNewToInventory(vnum: (short)(4 + packet.Type * 14), amount: 1,
                            type: InventoryType.Wear);
                        Session.Character.Inventory.AddNewToInventory(vnum: (short)(81 + packet.Type * 13), amount: 1,
                            type: InventoryType.Wear);

                        switch (packet.Type)
                        {
                            case 1:
                                Session.Character.Inventory.AddNewToInventory(vnum: 68, amount: 1,
                                    type: InventoryType.Wear);
                                break;

                            case 2:
                                Session.Character.Inventory.AddNewToInventory(vnum: 78, amount: 1,
                                    type: InventoryType.Wear);
                                break;

                            case 3:
                                Session.Character.Inventory.AddNewToInventory(vnum: 86, amount: 1,
                                    type: InventoryType.Wear);
                                break;
                        }

                        Session.CurrentMapInstance?.Broadcast(packet: Session.Character.GenerateEq());
                        Session.SendPacket(packet: Session.Character.GenerateEquipment());
                        Session.Character.ChangeClass(characterClass: (ClassType)packet.Type, fromCommand: false);
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "EQ_NOT_EMPTY"), type: 0));
                    }

                    break;

                case 2:
                    Session.SendPacket(packet: "wopen 1 0");
                    break;

                case 3:
                    var heldMonster = ServerManager.GetNpcMonster(npcVNum: packet.Type);
                    if (heldMonster != null &&
                        !Session.Character.Mates.Any(predicate: m =>
                            m.NpcMonsterVNum == heldMonster.NpcMonsterVNum && !m.IsTemporalMate) &&
                        Session.Character.Mates.FirstOrDefault(predicate: s =>
                                s.NpcMonsterVNum == heldMonster.NpcMonsterVNum && s.IsTemporalMate &&
                                s.IsTsReward) is Mate
                            partnerToReceive)
                    {
                        Session.Character.RemoveTemporalMates();
                        var partner = new Mate(owner: Session.Character, npcMonster: heldMonster,
                            level: heldMonster.Level, matetype: MateType.Partner);
                        partner.Experience = partnerToReceive.Experience;
                        if (!Session.Character.Mates.Any(predicate: s =>
                            s.MateType == MateType.Partner && s.IsTeamMember))
                            partner.IsTeamMember = true;
                        Session.Character.AddPet(mate: partner);
                    }

                    break;

                case 4:
                    var mate = Session.Character.Mates.Find(match: s => s.MateTransportId == packet.NpcId);
                    switch (packet.Type)
                    {
                        case 2:
                            if (mate != null)
                                if (Session.Character.Miniland == Session.Character.MapInstance)
                                {
                                    if (Session.Character.Level >= mate.Level)
                                    {
                                        var teammate = Session.Character.Mates.Where(predicate: s => s.IsTeamMember)
                                            .FirstOrDefault(predicate: s => s.MateType == mate.MateType);
                                        if (teammate != null) teammate.RemoveTeamMember();
                                        mate.AddTeamMember();
                                    }
                                    else
                                    {
                                        Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "PET_HIGHER_LEVEL"),
                                            type: 0));
                                    }
                                }

                            break;

                        case 3:
                            if (mate != null && Session.Character.Miniland == Session.Character.MapInstance)
                                mate.RemoveTeamMember();
                            break;

                        case 4:
                            if (mate != null)
                            {
                                if (Session.Character.Miniland == Session.Character.MapInstance)
                                {
                                    mate.RemoveTeamMember(maxHpMp: false);
                                    mate.MapX = mate.PositionX;
                                    mate.MapY = mate.PositionY;
                                }
                                else
                                {
                                    Session.SendPacket(
                                        packet:
                                        $"qna #n_run^4^5^3^{mate.MateTransportId} {Language.Instance.GetMessageFromKey(key: "ASK_KICK_PET")}");
                                }
                            }

                            break;

                        case 5:
                            if (mate != null)
                                Session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 3000, type: 10,
                                    argument: $"#n_run^4^6^3^{mate.MateTransportId}"));
                            break;

                        case 6:
                            if (mate != null && Session.Character.Miniland != Session.Character.MapInstance)
                                mate.BackToMiniland();
                            break;

                        case 7:
                            if (mate != null)
                            {
                                if (Session.Character.Mates.Any(predicate: s =>
                                    s.MateType == mate.MateType && s.IsTeamMember))
                                {
                                    Session.SendPacket(packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "ALREADY_PET_IN_TEAM"),
                                        type: 11));
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "ALREADY_PET_IN_TEAM"),
                                        type: 0));
                                }
                                else
                                {
                                    mate.RemoveTeamMember();
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 3000, type: 10,
                                        argument: $"#n_run^4^9^3^{mate.MateTransportId}"));
                                }
                            }

                            break;

                        case 9:
                            if (mate != null && mate.IsSummonable && Session.Character.MapInstance.MapInstanceType !=
                                MapInstanceType.TalentArenaMapInstance)
                            {
                                if (Session.Character.Level >= mate.Level)
                                {
                                    mate.PositionX =
                                        (short)(Session.Character.PositionX +
                                                 (mate.MateType == MateType.Partner ? -1 : 1));
                                    mate.PositionY = (short)(Session.Character.PositionY + 1);
                                    mate.AddTeamMember();
                                    Parallel.ForEach(
                                        source: Session.CurrentMapInstance.Sessions.Where(predicate: s =>
                                            s.Character != null), body: s =>
                                        {
                                            if (ServerManager.Instance.ChannelId != 51 ||
                                                Session.Character.Faction == s.Character.Faction)
                                                s.SendPacket(packet: mate.GenerateIn(hideNickname: false,
                                                    isAct4: ServerManager.Instance.ChannelId == 51));
                                            else
                                                s.SendPacket(packet: mate.GenerateIn(hideNickname: true,
                                                    isAct4: ServerManager.Instance.ChannelId == 51,
                                                    receiverAuthority: s.Account.Authority));
                                        });
                                }
                                else
                                {
                                    Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "PET_HIGHER_LEVEL"),
                                            type: 0));
                                }
                            }

                            break;
                    }

                    Session.SendPacket(packet: Session.Character.GeneratePinit());
                    Session.SendPackets(packets: Session.Character.GeneratePst());
                    break;

                case 10:
                    Session.SendPacket(packet: "wopen 3 0");
                    break;

                case 12:
                    Session.SendPacket(packet: $"wopen {packet.Type} 0");
                    break;

                case 14:
                    Session.SendPacket(packet: "wopen 27 0");
                    var recipelist = "m_list 2";
                    if (npc != null)
                    {
                        var tps = npc.Recipes;
                        recipelist = tps.Where(predicate: s => s.Amount > 0)
                            .Aggregate(seed: recipelist, func: (current, s) => current + $" {s.ItemVNum}");
                        recipelist += " -100";
                        Session.SendPacket(packet: recipelist);
                    }

                    break;

                case 15:
                    if (npc != null)
                    {
                        if (packet.Value == 2)
                        {
                            Session.SendPacket(
                                packet:
                                $"qna #n_run^15^1^1^{npc.MapNpcId} {Language.Instance.GetMessageFromKey(key: "ASK_CHANGE_SPAWNLOCATION")}");
                        }
                        else
                        {
                            switch (npc.MapId)
                            {
                                case 1:
                                    Session.Character.SetRespawnPoint(mapId: 1, mapX: 79, mapY: 116);
                                    break;

                                case 20:
                                    Session.Character.SetRespawnPoint(mapId: 20, mapX: 9, mapY: 92);
                                    break;

                                case 145:
                                    Session.Character.SetRespawnPoint(mapId: 145, mapX: 13, mapY: 110);
                                    break;

                                case 170:
                                    Session.Character.SetRespawnPoint(mapId: 170, mapX: 79, mapY: 47);
                                    break;

                                case 177:
                                    Session.Character.SetRespawnPoint(mapId: 177, mapX: 149, mapY: 74);
                                    break;

                                case 189:
                                    Session.Character.SetRespawnPoint(mapId: 189, mapX: 58, mapY: 166);
                                    break;
                            }

                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "RESPAWNLOCATION_CHANGED"),
                                    type: 0));
                        }
                    }

                    break;

                case 16:
                    tp = npc?.Teleporters?.FirstOrDefault(predicate: s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        if (packet.Type >= 0 && Session.Character.Gold >= 1000 * packet.Type)
                        {
                            Session.Character.Gold -= 1000 * packet.Type;
                            Session.SendPacket(packet: Session.Character.GenerateGold());
                            ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: tp.MapId,
                                mapX: tp.MapX, mapY: tp.MapY);
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                    type: 10));
                        }
                    }

                    break;

                case 17:
                    if (Session.Character.MapInstance.MapInstanceType != MapInstanceType.BaseMapInstance) return;
                    if (packet.Value == 1)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^{packet.Type}^2^{packet.NpcId} {string.Format(format: Language.Instance.GetMessageFromKey(key: "ASK_ENETER_GOLD"), arg0: 500 * (1 + packet.Type))}");
                    }
                    else
                    {
                        var currentRunningSeconds =
                            (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(value: -50)).TotalSeconds;
                        var timeSpanSinceLastPortal = currentRunningSeconds - Session.Character.LastPortal;
                        if (!(timeSpanSinceLastPortal >= 4) || !Session.HasCurrentMapInstance ||
                            ServerManager.Instance.ChannelId == 51 ||
                            Session.CurrentMapInstance.MapInstanceId ==
                            ServerManager.Instance.ArenaInstance.MapInstanceId ||
                            Session.CurrentMapInstance.MapInstanceId ==
                            ServerManager.Instance.FamilyArenaInstance.MapInstanceId)
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "CANT_MOVE"), type: 10));
                            return;
                        }

                        if (packet.Type >= 0 && Session.Character.Gold >= 500 * (1 + packet.Type))
                        {
                            Session.Character.LastPortal = currentRunningSeconds;
                            Session.Character.Gold -= 500 * (1 + packet.Type);
                            Session.SendPacket(packet: Session.Character.GenerateGold());
                            var pos = packet.Type == 0
                                ? ServerManager.Instance.ArenaInstance.Map.GetRandomPosition()
                                : ServerManager.Instance.FamilyArenaInstance.Map.GetRandomPosition();
                            ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                                mapInstanceId: packet.Type == 0
                                    ? ServerManager.Instance.ArenaInstance.MapInstanceId
                                    : ServerManager.Instance.FamilyArenaInstance.MapInstanceId, mapX: pos.X,
                                mapY: pos.Y);
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                    type: 10));
                        }
                    }

                    break;

                case 18:
                    if (Session.Character.MapInstance.MapInstanceType != MapInstanceType.BaseMapInstance) return;
                    Session.SendPacket(packet: Session.Character.GenerateNpcDialog(value: 17));
                    break;

                case 26:
                    tp = npc?.Teleporters?.FirstOrDefault(predicate: s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        if (Session.Character.Gold >= 5000 * packet.Type)
                        {
                            Session.Character.Gold -= 5000 * packet.Type;
                            ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: tp.MapId,
                                mapX: tp.MapX, mapY: tp.MapY);
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                    type: 10));
                        }
                    }

                    break;

                case 45:
                    tp = npc?.Teleporters?.FirstOrDefault(predicate: s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        if (Session.Character.Gold >= 500)
                        {
                            Session.Character.Gold -= 500;
                            Session.SendPacket(packet: Session.Character.GenerateGold());
                            ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: tp.MapId,
                                mapX: tp.MapX, mapY: tp.MapY);
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                    type: 10));
                        }
                    }

                    break;

                case 61:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 5917) < 1 ||
                            Session.Character.Inventory.CountItem(itemVNum: 5918) < 1)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 5922, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 5917);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 5918);
                    }

                    break;

                case 62:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 5919) < 1)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: 2536, mapX: 26,
                            mapY: 31);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 5919);
                    }

                    break;

                case 65:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: 5514);
                    }
                    break;

                case 66:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: 5914);
                    }
                    break;

                case 67:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: 5908);
                    }
                    break;

                case 68:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: 5919);
                    }
                    break;

                case 69:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 5910) < 5)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 5929, amount: 10);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 5910, amount: 5);
                    }

                    break;

                case 70:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 5910) < 90)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 5923, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 5910, amount: 90);
                    }

                    break;

                case 71:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 5910) < 300)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 5914, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 5910, amount: 300);
                    }

                    break;

                case 72: // Exchange 10 Yellow Pumpkin Sweets (2322) for a Halloween Costume Scroll (1915)
                    if (npc == null || !ServerManager.Instance.Configuration.HalloweenEvent) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 2322) < 10)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 1915, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 2322, amount: 10);
                    }

                    break;

                case 73: // Exchange 10 Black Pumpkin Sweets (2324) for a Halloween Costume Scroll (1915)
                    if (npc == null || !ServerManager.Instance.Configuration.HalloweenEvent) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 2324) < 10)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 1915, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 2324, amount: 10);
                    }

                    break;

                case 74: // Exchange 30 Yellow Pumpkin Sweets (2322) for Jack O'Lantern's Seal (1916)
                    if (npc == null || !ServerManager.Instance.Configuration.HalloweenEvent) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 2322) < 30)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 1916, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 2322, amount: 30);
                    }

                    break;

                case 75: // Exchange 30 Black Pumpkin Sweets (2324) for Jack O'Lantern's Seal (1916)
                    if (npc == null || !ServerManager.Instance.Configuration.HalloweenEvent) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 2324) < 30 || npc == null ||
                            !ServerManager.Instance.Configuration.HalloweenEvent)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 1916, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 2324, amount: 30);
                    }

                    break;

                case 76: // Exchange Bag of Sweets (1917) for Jack O'Lantern's Seal (1916)
                    if (npc == null || !ServerManager.Instance.Configuration.HalloweenEvent) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 1917) < 1)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 1916, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1917);
                    }

                    break;

                case 77:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.HalloweenEvent)
                            Session.Character.AddQuest(questId: 5924);
                    }
                    break;

                case 78:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.HalloweenEvent)
                            Session.Character.AddQuest(questId: 5926);
                    }
                    break;

                case 79:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.HalloweenEvent)
                            Session.Character.AddQuest(questId: 5928);
                    }
                    break;

                case 80:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.HalloweenEvent)
                            Session.Character.AddQuest(questId: 5930);
                    }
                    break;

                case 81:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.HalloweenEvent)
                            Session.Character.AddQuest(questId: 5922);
                    }
                    break;

                case 82:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.ChristmasEvent)
                            Session.Character.AddQuest(questId: 5932);
                    }
                    break;

                case 84:
                    {
                        if (npc == null || !ServerManager.Instance.Configuration.ChristmasEvent) return;

                        if (packet.Type == 0)
                        {
                            Session.SendPacket(
                                packet:
                                $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                        }
                        else
                        {
                            if (Session.Character.Inventory.CountItem(itemVNum: 2327) < 30)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                        type: 0));
                                return;
                            }

                            Session.Character.GiftAdd(itemVNum: 5064, amount: 1);
                            Session.Character.Inventory.RemoveItemAmount(vnum: 2327, amount: 30);
                        }
                    }
                    break;

                case 85:
                    {
                        if (npc == null || !ServerManager.Instance.Configuration.ChristmasEvent) return;

                        if (packet.Type == 0)
                        {
                            Session.SendPacket(
                                packet:
                                $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                        }
                        else
                        {
                            if (Session.Character.Inventory.CountItem(itemVNum: 2326) < 30)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                        type: 0));
                                return;
                            }

                            Session.Character.GiftAdd(itemVNum: 5064, amount: 1);
                            Session.Character.Inventory.RemoveItemAmount(vnum: 2326, amount: 30);
                        }
                    }
                    break;

                case 86:
                    {
                        if (npc == null || !ServerManager.Instance.Configuration.ChristmasEvent) return;

                        if (packet.Type == 0)
                        {
                            Session.SendPacket(
                                packet:
                                $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                        }
                        else
                        {
                            if (Session.Character.Inventory.CountItem(itemVNum: 2327) < 30)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                        type: 0));
                                return;
                            }

                            Session.Character.GiftAdd(itemVNum: 1371, amount: 1);
                            Session.Character.Inventory.RemoveItemAmount(vnum: 2327, amount: 30);
                        }
                    }
                    break;

                case 87:
                    {
                        if (npc == null || !ServerManager.Instance.Configuration.ChristmasEvent) return;

                        if (packet.Type == 0)
                        {
                            Session.SendPacket(
                                packet:
                                $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                        }
                        else
                        {
                            if (Session.Character.Inventory.CountItem(itemVNum: 2326) < 30)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                        type: 0));
                                return;
                            }

                            Session.Character.GiftAdd(itemVNum: 1371, amount: 1);
                            Session.Character.Inventory.RemoveItemAmount(vnum: 2326, amount: 30);
                        }
                    }
                    break;

                case 88:
                    {
                        if (npc == null || !ServerManager.Instance.Configuration.ChristmasEvent) return;

                        if (packet.Type == 0)
                        {
                            Session.SendPacket(
                                packet:
                                $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                        }
                        else
                        {
                            if (Session.Character.Inventory.CountItem(itemVNum: 1367) < 5)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                        type: 0));
                                return;
                            }

                            Session.Character.GiftAdd(itemVNum: 5206, amount: 1);
                            Session.Character.Inventory.RemoveItemAmount(vnum: 1367, amount: 5);
                        }
                    }
                    break;

                case 325:
                    {
                        if (npc == null || !ServerManager.Instance.Configuration.ChristmasEvent) return;

                        if (packet.Type == 0)
                        {
                            Session.SendPacket(
                                packet:
                                $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                        }
                        else
                        {
                            if (Session.Character.Inventory.CountItem(itemVNum: 5712) < 1 &&
                                Session.Character.Inventory.CountItem(itemVNum: 9138) < 1 ||
                                Session.Character.Inventory.CountItem(itemVNum: 4406) < 1 &&
                                Session.Character.Inventory.CountItem(itemVNum: 8369) < 1)
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                        type: 0));
                                return;
                            }

                            if (Session.Character.Inventory.CountItem(itemVNum: 9138) > 0)
                            {
                                Session.Character.Inventory.RemoveItemAmount(vnum: 9138);
                                Session.Character.GiftAdd(itemVNum: 9140, amount: 1);
                            }
                            else
                            {
                                Session.Character.Inventory.RemoveItemAmount(vnum: 5712);
                                Session.Character.GiftAdd(itemVNum: 5714, amount: 1);
                            }

                            if (Session.Character.Inventory.CountItem(itemVNum: 8369) > 0)
                                Session.Character.Inventory.RemoveItemAmount(vnum: 8369);
                            else
                                Session.Character.Inventory.RemoveItemAmount(vnum: 4406);
                        }
                    }
                    break;

                case 326:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.ChristmasEvent)
                            Session.Character.AddQuest(questId: 6325);
                    }
                    break;

                case 89:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.ChristmasEvent)
                            Session.Character.AddQuest(questId: 5934);
                    }
                    break;

                case 90:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.ChristmasEvent)
                            Session.Character.AddQuest(questId: 5936);
                    }
                    break;

                case 91:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.ChristmasEvent)
                            Session.Character.AddQuest(questId: 5938);
                    }
                    break;

                case 92:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.ChristmasEvent)
                            Session.Character.AddQuest(questId: 5940);
                    }
                    break;

                case 93:
                    {
                        if (npc != null && ServerManager.Instance.Configuration.ChristmasEvent)
                            Session.Character.AddQuest(questId: 5942);
                    }
                    break;

                case 6013:
                    if (!ServerManager.Instance.Configuration.ChristmasEvent) return;
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 1611) < 1 || Session.Character.Inventory
                                                                                          .CountItem(
                                                                                              itemVNum: 1612) < 1
                                                                                      || Session.Character.Inventory
                                                                                          .CountItem(
                                                                                              itemVNum: 1613) < 2 ||
                                                                                      Session.Character.Inventory
                                                                                          .CountItem(itemVNum: 1614) <
                                                                                      1)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 1621, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1611);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1612);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1613, amount: 2);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1614);
                    }

                    break;

                case 129:
                    if (!ServerManager.Instance.Configuration.ChristmasEvent) return;
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 1611) < 10 || Session.Character.Inventory
                                                                                           .CountItem(
                                                                                               itemVNum: 1612) < 10
                                                                                       || Session.Character.Inventory
                                                                                           .CountItem(
                                                                                               itemVNum: 1613) < 20 ||
                                                                                       Session.Character.Inventory
                                                                                           .CountItem(itemVNum: 1614) <
                                                                                       10)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 1621, amount: 10);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1611, amount: 10);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1612, amount: 10);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1613, amount: 20);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1614, amount: 10);
                    }

                    break;

                case 6014:
                    if (!ServerManager.Instance.Configuration.ChristmasEvent) return;
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 1615) < 1 ||
                            Session.Character.Inventory.CountItem(itemVNum: 1616) < 2 ||
                            Session.Character.Inventory.CountItem(itemVNum: 1617) < 1
                            || Session.Character.Inventory.CountItem(itemVNum: 1618) < 1 ||
                            Session.Character.Inventory.CountItem(itemVNum: 1619) < 1 ||
                            Session.Character.Inventory.CountItem(itemVNum: 1620) < 1)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 1622, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1615);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1616, amount: 2);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1617);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1618);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1619);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1620);
                    }

                    break;

                case 130:
                    if (!ServerManager.Instance.Configuration.ChristmasEvent) return;
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 1615) < 10 ||
                            Session.Character.Inventory.CountItem(itemVNum: 1616) < 20 ||
                            Session.Character.Inventory.CountItem(itemVNum: 1617) < 10
                            || Session.Character.Inventory.CountItem(itemVNum: 1618) < 10 ||
                            Session.Character.Inventory.CountItem(itemVNum: 1619) < 10 ||
                            Session.Character.Inventory.CountItem(itemVNum: 1620) < 10)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 1622, amount: 10);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1615, amount: 10);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1616, amount: 20);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1617, amount: 10);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1618, amount: 10);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1619, amount: 10);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1620, amount: 10);
                    }

                    break;

                case 1503:
                    if (!ServerManager.Instance.Configuration.HalloweenEvent) return;

                    if (Session.Character.Level < 20)
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "TOO_LOW_LVL"), type: 0));
                        return;
                    }

                    if (!Session.Character.GeneralLogs.Any(predicate: s =>
                        s.LogType == "DailyReward" && short.Parse(s: s.LogData) == 1917 &&
                        s.Timestamp.Date == DateTime.Today))
                    {
                        Session.Character.GeneralLogs.Add(value: new GeneralLogDto
                        {
                            AccountId = Session.Account.AccountId,
                            CharacterId = Session.Character.CharacterId,
                            IpAddress = Session.IpAddress,
                            LogData = "1917",
                            LogType = "DailyReward",
                            Timestamp = DateTime.Now
                        });
                        short amount = 1;
                        if (Session.Character.IsMorphed) amount *= 2;
                        Session.Character.GiftAdd(itemVNum: 1917, amount: amount);
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "QUEST_ALREADY_DONE"),
                                type: 0));
                    }

                    break;

                case 110:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: 5954);
                    }
                    break;

                case 111:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 1012) < 20 ||
                            Session.Character.Inventory.CountItem(itemVNum: 1013) < 20 ||
                            Session.Character.Inventory.CountItem(itemVNum: 1027) < 20)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 5500, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1012, amount: 20);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1013, amount: 20);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1027, amount: 20);
                    }

                    break;

                case 131:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: 5982);
                    }
                    break;

                case 132:
                    tp = npc?.Teleporters?.FirstOrDefault(predicate: s => s.Index == packet.Type);
                    if (tp != null)
                        ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: tp.MapId,
                            mapX: tp.MapX, mapY: tp.MapY);
                    break;

                case 133:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 1012) < 20 ||
                            Session.Character.Inventory.CountItem(itemVNum: 2307) < 20 ||
                            Session.Character.Inventory.CountItem(itemVNum: 5911) < 20)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 5512, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 1012, amount: 20);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 2307, amount: 20);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 5911, amount: 20);
                    }

                    break;

                case 134:
                    if (npc == null ||
                        !Session.Character.Quests.Any(predicate: s =>
                            s.Quest.QuestObjectives.Any(predicate: o => o.SpecialData == 5518)))
                        return;
                    short vNum = 0;
                    for (short i = 4494; i <= 4496; i++)
                        if (Session.Character.Inventory.CountItem(itemVNum: i) > 0)
                        {
                            vNum = i;
                            break;
                        }

                    if (vNum > 0)
                    {
                        Session.Character.GiftAdd(itemVNum: 5518, amount: 1);
                        Session.Character.GiftAdd(itemVNum: 4504, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: vNum);
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"), type: 0));
                    }

                    break;

                case 137:
                    Session.SendPacket(packet: "taw_open");
                    break;

                case 138:
                    var at = ServerManager.Instance.ArenaTeams.ToList()
                        .Where(predicate: s => s.Any(predicate: c => c.Session?.CurrentMapInstance != null))
                        .OrderBy(keySelector: s => rand.Next())
                        .FirstOrDefault();
                    if (at != null)
                    {
                        ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                            mapInstanceId: at.FirstOrDefault().Session.CurrentMapInstance.MapInstanceId, mapX: 69,
                            mapY: 100);

                        var zenas = at.OrderBy(keySelector: s => s.Order).FirstOrDefault(predicate: s =>
                            s.Session != null && !s.Dead && s.ArenaTeamType == ArenaTeamType.Zenas);
                        var erenia = at.OrderBy(keySelector: s => s.Order).FirstOrDefault(predicate: s =>
                            s.Session != null && !s.Dead && s.ArenaTeamType == ArenaTeamType.Erenia);
                        Session.SendPacket(packet: erenia?.Session?.Character?.GenerateTaM(type: 0));
                        Session.SendPacket(packet: erenia?.Session?.Character?.GenerateTaM(type: 3));
                        Session.SendPacket(packet: "taw_sv 0");
                        Session.SendPacket(
                            packet: zenas?.Session?.Character?.GenerateTaP(tatype: 0, showOponent: true));
                        Session.SendPacket(
                            packet: erenia?.Session?.Character?.GenerateTaP(tatype: 2, showOponent: true));
                        Session.SendPacket(packet: zenas?.Session?.Character?.GenerateTaFc(type: 0));
                        Session.SendPacket(packet: erenia?.Session?.Character?.GenerateTaFc(type: 1));
                    }
                    else
                    {
                        Session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "NO_TEAM_ARENA")));
                    }

                    break;

                case 135:
                    if (!ServerManager.Instance.StartedEvents.Contains(item: EventType.Talentarena))
                    {
                        var time = ServerManager.Instance.Schedules.ToList()
                                       .FirstOrDefault(predicate: s => s.Event == EventType.Talentarena)?.Time ??
                                   TimeSpan.FromSeconds(value: 0);
                        Session.SendPacket(packet: npc?.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ARENA_NOT_OPEN"),
                                arg0: string.Format(format: "{0:D2}:{1:D2} - {2:D2}:{3:D2}", time.Hours, time.Minutes,
                                    (time.Hours + 4) % 24, time.Minutes)), type: 10));
                    }
                    else
                    {
                        if (Session.Character.Level < 30)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "LOW_LVL_30")));
                            return;
                        }

                        var tickets = 5 - Session.Character.GeneralLogs.CountLinq(predicate: s =>
                            s.LogType == "TalentArena" && s.Timestamp.Date == DateTime.Today);

                        if (tickets > 0)
                        {
                            if (ServerManager.Instance.ArenaMembers.ToList().All(predicate: s => s.Session != Session))
                            {
                                if (ServerManager.Instance.IsCharacterMemberOfGroup(
                                    characterId: Session.Character.CharacterId))
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "TALENT_ARENA_GROUP"),
                                        type: 0));
                                    Session.SendPacket(packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "TALENT_ARENA_GROUP"),
                                        type: 10));
                                }
                                else
                                {
                                    Session.SendPacket(packet: Session.Character.GenerateSay(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "ARENA_TICKET_LEFT"),
                                            arg0: tickets), type: 10));

                                    ServerManager.Instance.ArenaMembers.Add(item: new ArenaMember
                                    {
                                        ArenaType = EventType.Talentarena,
                                        Session = Session,
                                        GroupId = null,
                                        Time = 0
                                    });
                                }
                            }
                        }
                        else
                        {
                            Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "TALENT_ARENA_NO_MORE_TICKET"),
                                type: 0));
                            Session.SendPacket(packet: Session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "TALENT_ARENA_NO_MORE_TICKET"),
                                type: 10));
                        }
                    }

                    break;

                case 19:
                case 144:
                    if (Session.Character.Timespace != null)
                        if (Session.Character.MapInstance.InstanceBag.EndState == 10)
                            EventHelper.Instance.RunEvent(evt: new EventContainer(
                                mapInstance: Session.Character.MapInstance,
                                eventActionType: EventActionType.Scriptend, param: (byte)5));
                    break;

                case 145:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 2522) < 50)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        switch (Session.Character.Class)
                        {
                            case ClassType.Swordsman:
                                Session.Character.GiftAdd(itemVNum: 4500, amount: 1);
                                break;

                            case ClassType.Archer:
                                Session.Character.GiftAdd(itemVNum: 4501, amount: 1);
                                break;

                            case ClassType.Magician:
                                Session.Character.GiftAdd(itemVNum: 4502, amount: 1);
                                break;
                        }

                        Session.Character.Inventory.RemoveItemAmount(vnum: 2522, amount: 50);
                    }

                    break;

                case 146:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 2522) < 50)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 2518, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 2522, amount: 50);
                    }

                    break;

                case 147:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 2523) < 50)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        switch (Session.Character.Class)
                        {
                            case ClassType.Swordsman:
                                Session.Character.GiftAdd(itemVNum: 4497, amount: 1);
                                break;

                            case ClassType.Archer:
                                Session.Character.GiftAdd(itemVNum: 4498, amount: 1);
                                break;

                            case ClassType.Magician:
                                Session.Character.GiftAdd(itemVNum: 4499, amount: 1);
                                break;
                        }

                        Session.Character.Inventory.RemoveItemAmount(vnum: 2523, amount: 50);
                    }

                    break;

                case 148:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 2523) < 50)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 2519, amount: 1);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 2523, amount: 50);
                    }

                    break;

                case 150:
                    if (npc != null)
                    {
                        if (Session.Character.Family != null)
                        {
                            if (Session.Character.Family.LandOfDeath != null &&
                                ServerManager.Instance.StartedEvents.Contains(item: EventType.Lod) && npc.Effect != 0)
                            {
                                if (Session.Character.Level >= 55)
                                    ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                                        mapInstanceId: Session.Character.Family.LandOfDeath.MapInstanceId, mapX: 153,
                                        mapY: 145);
                                else
                                    Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "LOD_REQUIERE_LVL"),
                                            type: 0));
                            }
                            else
                            {
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "LOD_CLOSED"),
                                        type: 0));
                            }
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NEED_FAMILY"), type: 0));
                        }
                    }

                    break;

                case 193:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: 6021);
                    }
                    break;

                case 194:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 5986) < 3)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 5984, amount: 3);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 5986, amount: 3);
                    }

                    break;

                case 195:
                    if (npc == null) return;
                    if (packet.Type == 0)
                    {
                        Session.SendPacket(
                            packet:
                            $"qna #n_run^{packet.Runner}^56^{packet.Value}^{packet.NpcId} {Language.Instance.GetMessageFromKey(key: "ASK_TRADE")}");
                    }
                    else
                    {
                        if (Session.Character.Inventory.CountItem(itemVNum: 5987) < 5)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_INGREDIENTS"),
                                    type: 0));
                            return;
                        }

                        Session.Character.GiftAdd(itemVNum: 5977, amount: 2);
                        Session.Character.Inventory.RemoveItemAmount(vnum: 5987, amount: 5);
                    }

                    break;

                case 300:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: 6040);
                    }
                    break;

                case 301:
                    tp = npc?.Teleporters?.FirstOrDefault(predicate: s => s.Index == packet.Type);
                    if (tp != null)
                        ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: tp.MapId,
                            mapX: tp.MapX, mapY: tp.MapY);
                    break;

                case 1000:
                    if (npc == null) return;

                    if (Session.Character.Quests.Any(predicate: s =>
                        s.Quest.DialogNpcVNum == npc.NpcVNum &&
                        s.Quest.QuestObjectives.Any(predicate: o => o.SpecialData == packet.Type)))
                        if (ServerManager.Instance.TimeSpaces.FirstOrDefault(predicate: s =>
                                s.QuestTimeSpaceId == packet.Type) is
                            ScriptedInstance timeSpace)
                            Session.Character.EnterInstance(input: timeSpace);

                    break;

                case 1500:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: 2255);
                    }
                    break;

                case 1600:
                    {
                        if (Session.Account.IsLimited)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                            return;
                        }

                        Session.SendPacket(packet: Session.Character.OpenFamilyWarehouse());
                    }
                    break;

                case 1601:
                    Session.SendPackets(packets: Session.Character.OpenFamilyWarehouseHist());
                    break;

                case 1602:
                    if (Session.Character.Family?.FamilyLevel >= 2 && Session.Character.Family.WarehouseSize < 21)
                    {
                        if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head)
                        {
                            if (500000 >= Session.Character.Gold)
                            {
                                Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                        type: 10));
                                return;
                            }

                            Session.Character.Family.WarehouseSize = 21;
                            Session.Character.Gold -= 500000;
                            Session.SendPacket(packet: Session.Character.GenerateGold());
                            FamilyDto fam = Session.Character.Family;
                            DaoFactory.FamilyDao.InsertOrUpdate(family: ref fam);
                            ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId);
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_HEAD"),
                                    type: 10));
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateModal(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_HEAD"),
                                    type: 1));
                        }
                    }

                    break;

                case 1603:
                    if (Session.Character.Family?.FamilyLevel >= 7 && Session.Character.Family.WarehouseSize < 49)
                    {
                        if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head)
                        {
                            if (2000000 >= Session.Character.Gold)
                            {
                                Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                        type: 10));
                                return;
                            }

                            Session.Character.Family.WarehouseSize = 49;
                            Session.Character.Gold -= 2000000;
                            Session.SendPacket(packet: Session.Character.GenerateGold());
                            FamilyDto fam = Session.Character.Family;
                            DaoFactory.FamilyDao.InsertOrUpdate(family: ref fam);
                            ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId);
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_HEAD"),
                                    type: 10));
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateModal(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_HEAD"),
                                    type: 1));
                        }
                    }

                    break;

                case 1604:
                    if (Session.Character.Family?.FamilyLevel >= 5 && Session.Character.Family.MaxSize < 70)
                    {
                        if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head)
                        {
                            if (5000000 >= Session.Character.Gold)
                            {
                                Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                        type: 10));
                                return;
                            }

                            Session.Character.Family.MaxSize = 70;
                            Session.Character.Gold -= 5000000;
                            Session.SendPacket(packet: Session.Character.GenerateGold());
                            FamilyDto fam = Session.Character.Family;
                            DaoFactory.FamilyDao.InsertOrUpdate(family: ref fam);
                            ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId);
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_HEAD"),
                                    type: 10));
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateModal(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_HEAD"),
                                    type: 1));
                        }
                    }

                    break;

                case 1605:
                    if (Session.Character.Family?.FamilyLevel >= 9 && Session.Character.Family.MaxSize < 100)
                    {
                        if (Session.Character.FamilyCharacter.Authority == FamilyAuthority.Head)
                        {
                            if (10000000 >= Session.Character.Gold)
                            {
                                Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                        type: 10));
                                return;
                            }

                            Session.Character.Family.MaxSize = 100;
                            Session.Character.Gold -= 10000000;
                            Session.SendPacket(packet: Session.Character.GenerateGold());
                            FamilyDto fam = Session.Character.Family;
                            DaoFactory.FamilyDao.InsertOrUpdate(family: ref fam);
                            ServerManager.Instance.FamilyRefresh(familyId: Session.Character.Family.FamilyId);
                        }
                        else
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_HEAD"),
                                    type: 10));
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateModal(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY_HEAD"),
                                    type: 1));
                        }
                    }

                    break;

                case 23:
                    if (packet.Type == 0)
                    {
                        if (Session.Character.Group?.SessionCount == 3)
                            foreach (var s in Session.Character.Group.Sessions.GetAllItems())
                                if (s.Character.Family != null)
                                {
                                    Session.SendPacket(packet: UserInterfaceHelper.GenerateInfo(
                                        message: Language.Instance.GetMessageFromKey(
                                            key: "GROUP_MEMBER_ALREADY_IN_FAMILY")));
                                    return;
                                }

                        if (Session.Character.Group == null || Session.Character.Group.SessionCount != 3)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "FAMILY_GROUP_NOT_FULL")));
                            return;
                        }

                        Session.SendPacket(packet: UserInterfaceHelper.GenerateInbox(
                            value:
                            $"#glmk^ {14} 1 {Language.Instance.GetMessageFromKey(key: "CREATE_FAMILY").Replace(oldChar: ' ', newChar: '^')}"));
                    }
                    else
                    {
                        if (Session.Character.Family == null)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_IN_FAMILY")));
                            return;
                        }

                        if (Session.Character.Family != null && Session.Character.FamilyCharacter != null &&
                            Session.Character.FamilyCharacter.Authority != FamilyAuthority.Head)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_FAMILY_HEAD")));
                            return;
                        }

                        Session.SendPacket(
                            packet: $"qna #glrm^1 {Language.Instance.GetMessageFromKey(key: "DISSOLVE_FAMILY")}");
                    }

                    break;

                case 60:
                    {
                        if (!Session.Character.CanUseNosBazaar())
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateInfo(
                                    message: Language.Instance.GetMessageFromKey(key: "INFO_BAZAAR")));
                            return;
                        }

                        MedalType medalType = 0;
                        var time = 0;

                        var medal = Session.Character.StaticBonusList.Find(match: s =>
                            s.StaticBonusType == StaticBonusType.BazaarMedalGold ||
                            s.StaticBonusType == StaticBonusType.BazaarMedalSilver);

                        if (medal != null)
                        {
                            time = (int)(medal.DateEnd - DateTime.Now).TotalHours;

                            switch (medal.StaticBonusType)
                            {
                                case StaticBonusType.BazaarMedalGold:
                                    medalType = MedalType.Gold;
                                    break;
                                case StaticBonusType.BazaarMedalSilver:
                                    medalType = MedalType.Silver;
                                    break;
                            }
                        }

                        Session.SendPacket(packet: $"wopen 32 {(byte)medalType} {time}");
                    }
                    break;

                case 3000:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: 5478, isMain: true);
                    }
                    break;

                case 3006:
                    {
                        if (npc != null) Session.Character.AddQuest(questId: packet.Type);
                    }
                    break;

                case 200:
                    {
                        if (npc != null)
                            if (Session.Character.Quests.Any(predicate: s =>
                                s.Quest.QuestType == (int)QuestType.Dialog2 &&
                                s.Quest.QuestObjectives.Any(predicate: b => b.Data == npc.NpcVNum)))
                            {
                                Session.Character.AddQuest(questId: packet.Type);
                                Session.Character.IncrementQuests(type: QuestType.Dialog2, firstData: npc.NpcVNum);
                            }
                    }
                    break;

                case 5001:
                    if (npc != null)
                    {
                        MapInstance map = null;
                        switch (Session.Character.Faction)
                        {
                            case FactionType.None:
                                Session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateInfo(
                                        message: "You need to be part of a faction to join Act 4!"));
                                return;

                            case FactionType.Angel:
                                map = ServerManager.GetAllMapInstances().Find(match: s =>
                                    s.MapInstanceType.Equals(obj: MapInstanceType.Act4ShipAngel));

                                break;

                            case FactionType.Demon:
                                map = ServerManager.GetAllMapInstances().Find(match: s =>
                                    s.MapInstanceType.Equals(obj: MapInstanceType.Act4ShipDemon));

                                break;
                        }

                        if (map == null || npc.EffectActivated)
                        {
                            Session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "SHIP_NOTARRIVED"),
                                    type: 0));
                            return;
                        }

                        if (3000 > Session.Character.Gold)
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                    type: 10));
                            return;
                        }

                        Session.Character.Gold -= 3000;
                        Session.SendPacket(packet: Session.Character.GenerateGold());
                        var pos = map.Map.GetRandomPosition();
                        ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                            mapInstanceId: map.MapInstanceId,
                            mapX: pos.X, mapY: pos.Y);
                    }

                    break;

                case 5002:
                    tp = npc?.Teleporters?.FirstOrDefault(predicate: s => s.Index == packet.Type);
                    if (tp != null)
                    {
                        Session.SendPacket(packet: "it 3"); //Default Deactivate
                        if (ServerManager.Instance.ChannelId == 51)
                        {
                            var connection =
                                CommunicationServiceClient.Instance.RetrieveOriginWorld(
                                    accountId: Session.Account.AccountId);
                            if (string.IsNullOrWhiteSpace(value: connection)) return;
                            Session.Character.MapId = tp.MapId;
                            Session.Character.MapX = tp.MapX;
                            Session.Character.MapY = tp.MapY;
                            var port = Convert.ToInt32(value: connection.Split(':')[1]);
                            Session.Character.ChangeChannel(ip: connection.Split(':')[0], port: port, mode: 3);
                        }
                        else
                        {
                            ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: tp.MapId,
                                mapX: tp.MapX, mapY: tp.MapY);
                        }
                    }

                    break;

                case 5004:
                    if (npc != null)
                        ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: 145, mapX: 50,
                            mapY: 41);
                    break;

                case 5011:
                    if (npc != null)
                    {
                        if (30000 > Session.Character.Gold)
                        {
                            Session.SendPacket(
                                packet: Session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                    type: 10));
                            return;
                        }

                        Session.Character.Gold -= 30000;
                        Session.SendPacket(packet: Session.Character.GenerateGold());
                        ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: 170, mapX: 127,
                            mapY: 46);
                    }

                    break;

                case 5012:
                    tp = npc?.Teleporters?.FirstOrDefault(predicate: s => s.Index == packet.Type);
                    if (tp != null)
                        ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: tp.MapId,
                            mapX: tp.MapX, mapY: tp.MapY);
                    break;

                case 2000:
                    {
                        if (npc != null)
                            if (packet.Type == 2000 && npc.NpcVNum == 932 &&
                                !Session.Character.Quests.Any(predicate: s =>
                                    s.QuestId >= 2000 && s.QuestId <= 2007) // Pajama
                                || packet.Type == 2008 && npc.NpcVNum == 933 &&
                                !Session.Character.Quests.Any(predicate: s =>
                                    s.QuestId >= 2008 && s.QuestId <= 2013) // SP 1
                                || packet.Type == 2014 && npc.NpcVNum == 934 &&
                                !Session.Character.Quests.Any(predicate: s =>
                                    s.QuestId >= 2014 && s.QuestId <= 2020) // SP 2
                                || packet.Type == 2060 && npc.NpcVNum == 948 &&
                                !Session.Character.Quests.Any(predicate: s =>
                                    s.QuestId >= 2060 && s.QuestId <= 2095) // SP 3
                                || packet.Type == 2100 && npc.NpcVNum == 954 &&
                                !Session.Character.Quests.Any(predicate: s =>
                                    s.QuestId >= 2100 && s.QuestId <= 2134) // SP 4
                                || packet.Type == 2030 && npc.NpcVNum == 422 &&
                                !Session.Character.Quests.Any(predicate: s => s.QuestId >= 2030 && s.QuestId <= 2046)
                                || packet.Type == 2048 && npc.NpcVNum == 303 &&
                                !Session.Character.Quests.Any(predicate: s => s.QuestId >= 2048 && s.QuestId <= 2050))
                                Session.Character.AddQuest(questId: packet.Type);
                    }
                    break;

                case 2001: //Custom Items for SP1 - SP4
                    {
                        switch (packet.Type)
                        {
                            case 1: // Pajama
                                {
                                    if (Session.Character.MapInstance.Npcs.Any(predicate: s => s.NpcVNum == 932)) return;
                                }
                                break;

                            case 2: // SP 1
                                {
                                    if (Session.Character.MapInstance.Npcs.Any(predicate: s => s.NpcVNum == 933))
                                        switch (Session.Character.Class)
                                        {
                                            case ClassType.Swordsman:
                                                break;
                                            case ClassType.Archer:
                                                break;
                                            case ClassType.Magician:
                                                break;
                                        }
                                }
                                break;

                            case 3: // SP 2
                                {
                                    if (Session.Character.MapInstance.Npcs.Any(predicate: s => s.NpcVNum == 934))
                                        switch (Session.Character.Class)
                                        {
                                            case ClassType.Swordsman:
                                                break;
                                            case ClassType.Archer:
                                                break;
                                            case ClassType.Magician:
                                                break;
                                        }
                                }
                                break;
                        }
                    }
                    break;

                case 5:
                    if (Session.Character.MapInstance.Npcs.Any(predicate: s => s.NpcVNum == 948)) // SP 3
                        // SP 3

                        switch (Session.Character.Class)
                        {
                            case ClassType.Swordsman:
                                break;
                            case ClassType.Archer:
                                break;
                            case ClassType.Magician:
                                break;
                        }

                    break;

                case 6:
                    if (Session.Character.MapInstance.Npcs.Any(predicate: s => s.NpcVNum == 954 /* SP 4 */))
                        // SP 4

                        switch (Session.Character.Class)
                        {
                            case ClassType.Swordsman:
                                break;
                            case ClassType.Archer:
                                break;
                            case ClassType.Magician:
                                break;
                        }

                    break;

                case 2002:
                    if (npc != null)
                    {
                        var gemNpcVnum = 0;

                        switch (npc.NpcVNum)
                        {
                            case 935:
                                gemNpcVnum = 932;
                                break;
                            case 936:
                                gemNpcVnum = 933;
                                break;
                            case 937:
                                gemNpcVnum = 934;
                                break;
                            case 952:
                                gemNpcVnum = 948;
                                break;
                            case 953:
                                gemNpcVnum = 954;
                                break;
                        }

                        if (ServerManager.Instance.SpecialistGemMapInstances?.FirstOrDefault(predicate: s =>
                                s.Npcs.Any(predicate: n => n.NpcVNum == gemNpcVnum)) is MapInstance
                            specialistGemMapInstance
                        )
                            ServerManager.Instance.ChangeMapInstance(characterId: Session.Character.CharacterId,
                                mapInstanceId: specialistGemMapInstance.MapInstanceId, mapX: 3, mapY: 3);
                    }

                    break;

                case 666: // Hero Equipment Downgrade
                    {
                        // 4949 ~ 4966 = c25/c28
                        // 4978 ~ 4986 = c45/c48

                        const long price = 10000000;

                        var itemInstance =
                            Session?.Character?.Inventory?.LoadBySlotAndType(slot: 0, type: InventoryType.Equipment);

                        if (itemInstance?.Item != null &&
                            (itemInstance.ItemVNum >= 4949 && itemInstance.ItemVNum <= 4966 ||
                             itemInstance.ItemVNum >= 4978 && itemInstance.ItemVNum <= 4986) && itemInstance.Rare == 8)
                        {
                            if (Session.Character.Gold < price)
                            {
                                Session.SendPacket(
                                    packet: Session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_MONEY"),
                                        type: 10));
                                return;
                            }

                            Session.Character.Gold -= price;
                            Session.SendPacket(packet: Session.Character.GenerateGold());

                            itemInstance.RarifyItem(session: Session, mode: RarifyMode.HeroEquipmentDowngrade,
                                protection: RarifyProtection.None);

                            Session.SendPacket(packet: itemInstance.GenerateInventoryAdd());
                        }
                    }
                    break;

                default:
                    {
                        Logger.Warn(data: string.Format(format: Language.Instance.GetMessageFromKey(key: "NO_NRUN_HANDLER"),
                            arg0: packet.Runner));
                    }
                    break;

                case 336:
                    if (npc != null)
                        ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: 2631, mapX: 7,
                            mapY: 47);

                    break;

                case 332:
                    if (npc == null) return;

                    Session.Character.AddQuest(questId: 6500);
                    break;

                case 334:
                    if (npc != null)
                        ServerManager.Instance.ChangeMap(id: Session.Character.CharacterId, mapId: 2631, mapX: 7,
                            mapY: 47);
                    //ServerManager.Instance.ChangeMap(session.Character .CharacterId, 2629, 5, 32);

                    break;

                    //The one with comments is where the pg needs to go really(shoud go there kill 4 / 5 mobs and after 10 seconds gets tp to Moritius) but i'm not expert so it's up to you. 
            }
        }

        #endregion
    }
}