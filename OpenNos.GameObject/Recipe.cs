﻿using System.Collections.Generic;
using System.Linq;
using OpenNos.DAL;
using OpenNos.Data;

namespace OpenNos.GameObject
{
    public class Recipe : RecipeDto
    {
        public Recipe()
        {
        }

        public Recipe(RecipeDto input)
        {
            Amount = input.Amount;
            ItemVNum = input.ItemVNum;
            RecipeId = input.RecipeId;
        }

        #region Properties

        public List<RecipeItemDto> Items { get; set; }

        #endregion

        #region Methods

        public void Initialize()
        {
            Items = new List<RecipeItemDto>();
            foreach (var recipe in DaoFactory.RecipeItemDao.LoadByRecipe(recipeId: RecipeId).ToList())
                Items.Add(item: recipe);
        }

        #endregion
    }
}