﻿using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;

namespace OpenNos.GameObject
{
    public class UpgradeItem : Item
    {
        #region Instantiation

        public UpgradeItem(ItemDto item) : base(item: item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte option = 0,
            string[] packetsplit = null)
        {
            if (session.Character.IsVehicled)
            {
                session.SendPacket(
                    packet: session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_DO_VEHICLED"), type: 10));
                return;
            }

            if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance) return;

            if (Effect == 0)
            {
                if (EffectValue != 0)
                {
                    if (session.Character.IsSitting)
                    {
                        session.Character.IsSitting = false;
                        session.SendPacket(packet: session.Character.GenerateRest());
                    }

                    session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: 12, argument: 1,
                        callerId: session.Character.CharacterId,
                        value: EffectValue));
                }
                else if (EffectValue == 0)
                {
                    if (packetsplit?.Length > 9 && byte.TryParse(s: packetsplit[8], result: out var typeEquip) &&
                        short.TryParse(s: packetsplit[9], result: out var slotEquip))
                    {
                        if (session.Character.IsSitting)
                        {
                            session.Character.IsSitting = false;
                            session.SendPacket(packet: session.Character.GenerateRest());
                        }

                        if (option != 0)
                        {
                            var isUsed = false;
                            switch (inv.ItemVNum)
                            {
                                case 1219:
                                case 9130:
                                    var equip = session.Character.Inventory.LoadBySlotAndType(slot: slotEquip,
                                        type: (InventoryType)typeEquip);
                                    if (equip?.IsFixed == true)
                                    {
                                        equip.IsFixed = false;
                                        session.SendPacket(packet: StaticPacketHelper.GenerateEff(
                                            effectType: UserType.Player,
                                            callerId: session.Character.CharacterId, effectId: 3003));
                                        session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: 17,
                                            argument: 1,
                                            callerId: session.Character.CharacterId, value: slotEquip));
                                        session.SendPacket(
                                            packet: session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(key: "ITEM_UNFIXED"),
                                                type: 12));
                                        isUsed = true;
                                    }

                                    break;

                                case 1365:
                                case 9039:
                                    var specialist =
                                        session.Character.Inventory.LoadBySlotAndType(slot: slotEquip,
                                            type: (InventoryType)typeEquip);
                                    if (specialist?.Rare == -2)
                                    {
                                        specialist.Rare = 0;
                                        session.SendPacket(
                                            packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "SP_RESURRECTED"),
                                                type: 0));
                                        session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: 13,
                                            argument: 1,
                                            callerId: session.Character.CharacterId, value: 1));
                                        session.Character.SpPoint = 10000;
                                        if (session.Character.SpPoint > 10000) session.Character.SpPoint = 10000;
                                        session.SendPacket(packet: session.Character.GenerateSpPoint());
                                        session.SendPacket(packet: specialist.GenerateInventoryAdd());
                                        isUsed = true;
                                    }

                                    break;
                            }

                            if (!isUsed)
                                session.SendPacket(
                                    packet: session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "ITEM_IS_NOT_FIXED"),
                                        type: 11));
                            else
                                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        }
                        else
                        {
                            session.SendPacket(
                                packet:
                                $"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^0^1^{typeEquip}^{slotEquip} {Language.Instance.GetMessageFromKey(key: "QNA_ITEM")}");
                        }
                    }
                }
            }
            else
            {
                Logger.Warn(data: string.Format(format: Language.Instance.GetMessageFromKey(key: "NO_HANDLER_ITEM"),
                    GetType(), VNum,
                    Effect, EffectValue));
            }
        }

        #endregion
    }
}