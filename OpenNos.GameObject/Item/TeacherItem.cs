﻿using System;
using System.Linq;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class TeacherItem : Item
    {
        #region Instantiation

        public TeacherItem(ItemDto item) : base(item: item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte option = 0,
            string[] packetsplit = null)
        {
            if (session.Character.IsVehicled)
            {
                session.SendPacket(
                    packet: session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_DO_VEHICLED"), type: 10));
                return;
            }

            if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance) return;

            if (packetsplit == null) return;

            void ReleasePet(MateType mateType, Guid itemToRemoveId)
            {
                if (int.TryParse(s: packetsplit[3], result: out var mateTransportId))
                {
                    var mate = session.Character.Mates.Find(match: s =>
                        s.MateTransportId == mateTransportId && s.MateType == mateType);
                    if (mate != null)
                    {
                        if (!mate.IsTeamMember)
                        {
                            var mateInventory = mate.GetInventory();
                            if (mateInventory.Count > 0)
                            {
                                session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "EQ_NOT_EMPTY"),
                                        type: 0));
                            }
                            else
                            {
                                session.Character.Mates.Remove(item: mate);
                                byte i = 0;
                                session.Character.Mates.Where(predicate: s => s.MateType == MateType.Partner).ToList()
                                    .ForEach(action: s =>
                                    {
                                        s.GetInventory().ForEach(action: item => item.Type = (InventoryType)(13 + i));
                                        s.PetId = i;
                                        i++;
                                    });
                                session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateInfo(
                                        message: Language.Instance.GetMessageFromKey(key: "PET_RELEASED")));
                                session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());
                                session.SendPackets(packets: session.Character.GenerateScP());
                                session.SendPackets(packets: session.Character.GenerateScN());
                                session.CurrentMapInstance?.Broadcast(packet: mate.GenerateOut());
                                session.Character.Inventory.RemoveItemFromInventory(id: itemToRemoveId);
                            }
                        }
                        else
                        {
                            session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "PET_IN_TEAM_UNRELEASABLE"),
                                type: 0));
                        }
                    }
                }
            }

            if (BCards.Count > 0 && session.Character.MapInstance == session.Character.Miniland)
            {
                BCards.ForEach(action: c =>
                    c.ApplyBCards(session: session.Character.BattleEntity, sender: session.Character.BattleEntity));
                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                return;
            }

            switch (Effect)
            {
                case 10:
                    if (int.TryParse(s: packetsplit[3], result: out var mateTransportId))
                    {
                        var mate = session.Character.Mates.Find(match: s => s.MateTransportId == mateTransportId);
                        if (mate == null || mate.MateType != MateType.Pet || mate.Loyalty >= 1000) return;
                        mate.Loyalty += 100;
                        if (mate.Loyalty > 1000) mate.Loyalty = 1000;
                        mate.GenerateXp(xp: EffectValue);
                        session.SendPacket(packet: mate.GenerateCond());
                        session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                            callerId: mate.MateTransportId, effectId: 5002));
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                case 11:
                    if (int.TryParse(s: packetsplit[3], result: out mateTransportId))
                    {
                        var mate = session.Character.Mates.Find(match: s => s.MateTransportId == mateTransportId);
                        if (mate == null || mate.MateType != MateType.Pet ||
                            mate.Level >= session.Character.Level - 5 ||
                            mate.Level + 1 > ServerManager.Instance.Configuration.MaxLevel) return;
                        mate.Level++;
                        session.CurrentMapInstance?.Broadcast(
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                callerId: mate.MateTransportId, effectId: 8), xRangeCoordinate: mate.PositionX,
                            yRangeCoordinate: mate.PositionY);
                        session.CurrentMapInstance?.Broadcast(
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                callerId: mate.MateTransportId, effectId: 198), xRangeCoordinate: mate.PositionX,
                            yRangeCoordinate: mate.PositionY);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                case 12:
                    if (int.TryParse(s: packetsplit[3], result: out mateTransportId))
                    {
                        var mate = session.Character.Mates.Find(match: s => s.MateTransportId == mateTransportId);
                        if (mate == null || mate.MateType != MateType.Partner ||
                            mate.Level >= session.Character.Level - 5 ||
                            mate.Level + 1 > ServerManager.Instance.Configuration.MaxLevel) return;
                        mate.Level++;
                        session.CurrentMapInstance?.Broadcast(
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                callerId: mate.MateTransportId, effectId: 8), xRangeCoordinate: mate.PositionX,
                            yRangeCoordinate: mate.PositionY);
                        session.CurrentMapInstance?.Broadcast(
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                callerId: mate.MateTransportId, effectId: 198), xRangeCoordinate: mate.PositionX,
                            yRangeCoordinate: mate.PositionY);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                case 13:
                    if (int.TryParse(s: packetsplit[3], result: out mateTransportId) &&
                        session.Character.Mates.FirstOrDefault(predicate: s => s.MateTransportId == mateTransportId) is
                    { } pet)
                    {
                        if (pet.MateType == MateType.Pet)
                            session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: 10, argument: 1,
                                callerId: mateTransportId, value: 2));
                        else
                            session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "CANT_CHANGE_PARTNER_NAME"),
                                type: 0));
                    }

                    break;

                case 14:
                    if (int.TryParse(s: packetsplit[3], result: out mateTransportId))
                    {
                        if (session.Character.MapInstance == session.Character.Miniland)
                        {
                            var mate = session.Character.Mates.Find(match: s =>
                                s.MateTransportId == mateTransportId && s.MateType == MateType.Pet);
                            if (mate?.CanPickUp == false)
                            {
                                session.CurrentMapInstance.Broadcast(
                                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                        callerId: mate.MateTransportId, effectId: 5));
                                session.CurrentMapInstance.Broadcast(
                                    packet: StaticPacketHelper.GenerateEff(effectType: UserType.Npc,
                                        callerId: mate.MateTransportId, effectId: 5002));
                                mate.CanPickUp = true;
                                session.SendPackets(packets: session.Character.GenerateScP());
                                session.SendPacket(
                                    packet: session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "PET_CAN_PICK_UP"),
                                        type: 10));
                                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                            }
                        }
                        else
                        {
                            session.SendPacket(
                                packet: session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_IN_MINILAND"),
                                    type: 12));
                        }
                    }

                    break;

                case 16:
                    if (int.TryParse(s: packetsplit[3], result: out mateTransportId))
                    {
                        var mate = session.Character.Mates.Find(match: s => s.MateTransportId == mateTransportId);
                        if (mate == null || mate.MateType != MateType.Pet || mate.Level == 1) return;
                        mate.Level--;
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                case 17:
                    if (int.TryParse(s: packetsplit[3], result: out mateTransportId))
                    {
                        var mate = session.Character.Mates.Find(match: s => s.MateTransportId == mateTransportId);
                        if (mate?.IsSummonable == false)
                        {
                            mate.IsSummonable = true;
                            session.SendPackets(packets: session.Character.GenerateScP());
                            session.SendPacket(packet: session.Character.GenerateSay(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "PET_SUMMONABLE"),
                                    arg0: mate.Name), type: 10));
                            session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "PET_SUMMONABLE"),
                                    arg0: mate.Name), type: 0));
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        }
                    }

                    break;

                case 18:
                    if (int.TryParse(s: packetsplit[3], result: out mateTransportId))
                    {
                        var mate = session.Character.Mates.Find(match: s => s.MateTransportId == mateTransportId);
                        if (mate == null || mate.MateType != MateType.Partner || mate.Level == 1) return;
                        mate.Level--;
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                case 1000:
                    ReleasePet(mateType: MateType.Pet, itemToRemoveId: inv.Id);
                    break;

                case 1001:
                    ReleasePet(mateType: MateType.Partner, itemToRemoveId: inv.Id);
                    break;

                default:
                    Logger.Warn(data: string.Format(format: Language.Instance.GetMessageFromKey(key: "NO_HANDLER_ITEM"),
                        GetType(), VNum,
                        Effect, EffectValue));
                    break;
            }
        }

        #endregion
    }
}