﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class SnackItem : Item
    {
        #region Instantiation

        public SnackItem(ItemDto item) : base(item: item)
        {
        }

        #endregion

        #region Members

        static IDisposable RegenerateDisposable { get; set; }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte option = 0,
            string[] packetsplit = null)
        {
            /*if (session.Character.IsVehicled)
            {
                session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("CANT_DO_VEHICLED"), 10));
                return;
            }*/

            if (session.CurrentMapInstance?.MapInstanceType != MapInstanceType.TalentArenaMapInstance &&
                VNum == 2802) return;
            if (session.CurrentMapInstance != null &&
                session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance &&
                VNum != 2802) return;

            var item = inv.Item;
            switch (Effect)
            {
                default:
                    if (session.Character.Hp <= 0) return;
                    if (item.BCards.Find(match: s => s.Type == 25) is { } buff)
                    {
                        if (ServerManager.RandomNumber() < buff.FirstData)
                            session.Character.AddBuff(
                                indicator: new Buff.Buff(id: (short)buff.SecondData, level: session.Character.Level),
                                sender: session.Character.BattleEntity);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }
                    else
                    {
                        if (session.Character.SnackAmount < 0) session.Character.SnackAmount = 0;
                        var amount = session.Character.SnackAmount;
                        if (amount < 5)
                        {
                            var workerThread = new Thread(start: () => Regenerate(session: session, item: item));
                            workerThread.Start();
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        }
                        else
                        {
                            session.SendPacket(packet: session.Character.Gender == GenderType.Female
                                ? session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_HUNGRY_FEMALE"), type: 1)
                                : session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_HUNGRY_MALE"),
                                    type: 1));
                        }

                        if (amount == 0)
                        {
                            var workerThread2 = new Thread(start: () => Sync(session: session));
                            workerThread2.Start();
                        }
                    }

                    break;
            }
        }

        static void Regenerate(ClientSession session, Item item)
        {
            session.SendPacket(
                packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                    callerId: session.Character.CharacterId, effectId: 6000));
            session.Character.SnackAmount++;
            session.Character.MaxSnack = 0;
            session.Character.SnackHp += item.Hp / 5;
            session.Character.SnackMp += item.Mp / 5;
            RegenerateDisposable = Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1800 * 5)).Subscribe(
                onNext: obs =>
                {
                    if (session.Character.SnackHp > 0 || session.Character.SnackMp > 0)
                    {
                        session.Character.SnackHp -= item.Hp / 5;
                        session.Character.SnackMp -= item.Mp / 5;
                        session.Character.SnackAmount--;
                    }
                });
        }

        static void Sync(ClientSession session)
        {
            for (session.Character.MaxSnack = 0; session.Character.MaxSnack < 5; session.Character.MaxSnack++)
            {
                if (session.Character.Hp <= 0)
                {
                    RegenerateDisposable?.Dispose();
                    session.Character.SnackHp = 0;
                    session.Character.SnackMp = 0;
                    session.Character.SnackAmount = 0;
                    return;
                }

                var hpLoad = (int)session.Character.HPLoad();
                var mpLoad = (int)session.Character.MPLoad();

                var buffRc = session.Character.GetBuff(type: BCardType.CardType.LeonaPassiveSkill,
                    subtype: (byte)AdditionalTypes.LeonaPassiveSkill.IncreaseRecoveryItems)[0] / 100D;

                var hpAmount = session.Character.SnackHp + (int)(session.Character.SnackHp * buffRc);
                var mpAmount = session.Character.SnackMp + (int)(session.Character.SnackMp * buffRc);

                if (session.Character.Hp + hpAmount > hpLoad) hpAmount = hpLoad - session.Character.Hp;

                if (session.Character.Mp + mpAmount > mpLoad) mpAmount = mpLoad - session.Character.Mp;

                var convertRecoveryToDamage = ServerManager.RandomNumber() <
                                              session.Character.GetBuff(type: BCardType.CardType.DarkCloneSummon,
                                                  subtype: (byte)AdditionalTypes.DarkCloneSummon
                                                      .ConvertRecoveryToDamage)[0];

                if (convertRecoveryToDamage)
                {
                    session.Character.Hp -= hpAmount;

                    if (session.Character.Hp < 1) session.Character.Hp = 1;

                    if (hpAmount > 0)
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GenerateDm(dmg: hpAmount));
                }
                else
                {
                    session.Character.Hp += hpAmount;

                    if (hpAmount > 0)
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GenerateRc(characterHealth: hpAmount));
                }

                session.Character.Mp += mpAmount;

                foreach (var mate in session.Character.Mates.Where(predicate: s => s.IsTeamMember && s.IsAlive))
                {
                    hpLoad = mate.HpLoad();
                    mpLoad = mate.MpLoad();

                    hpAmount = session.Character.SnackHp;
                    mpAmount = session.Character.SnackMp;

                    if (mate.Hp + hpAmount > hpLoad) hpAmount = hpLoad - (int)mate.Hp;

                    if (mate.Mp + mpAmount > mpLoad) mpAmount = mpLoad - (int)mate.Mp;

                    mate.Hp += hpAmount;
                    mate.Mp += mpAmount;

                    if (hpAmount > 0)
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: mate.GenerateRc(characterHealth: hpAmount));
                }

                if (session.IsConnected)
                {
                    session.SendPacket(packet: session.Character.GenerateStat());

                    if (session.Character.Mates.Any(predicate: m => m.IsTeamMember && m.IsAlive))
                        session.SendPackets(packets: session.Character.GeneratePst());

                    Thread.Sleep(millisecondsTimeout: 1800);
                }
                else
                {
                    return;
                }
            }

            session.Character.SnackAmount = 0;
        }

        #endregion
    }
}