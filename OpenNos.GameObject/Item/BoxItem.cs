﻿using System;
using System.Linq;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class BoxItem : Item
    {
        #region Instantiation

        public BoxItem(ItemDto item) : base(item: item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte option = 0,
            string[] packetsplit = null)
        {
            if (session.Character.IsVehicled && Effect != 888)
            {
                session.SendPacket(
                    packet: session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_DO_VEHICLED"), type: 10));
                return;
            }

            if (inv.ItemVNum == 333 || inv.ItemVNum == 334
            ) // Sealed Jajamaru Specialist Card & Sealed Princess Sakura Bead
                return;

            switch (Effect)
            {
                case 0:
                    if (option == 0)
                    {
                        if (packetsplit?.Length == 9)
                        {
                            var box = session.Character.Inventory.LoadBySlotAndType(slot: inv.Slot,
                                type: InventoryType.Equipment);
                            if (box != null)
                            {
                                if (box.Item.ItemSubType == 3)
                                    session.SendPacket(
                                        packet:
                                        $"qna #guri^300^8023^{inv.Slot} {Language.Instance.GetMessageFromKey(key: "ASK_OPEN_BOX")}");
                                else if (box.HoldingVNum == 0)
                                    session.SendPacket(
                                        packet:
                                        $"qna #guri^300^8023^{inv.Slot}^{packetsplit[3]} {Language.Instance.GetMessageFromKey(key: "ASK_STORE_PET")}");
                                else
                                    session.SendPacket(
                                        packet:
                                        $"qna #guri^300^8023^{inv.Slot} {Language.Instance.GetMessageFromKey(key: "ASK_RELEASE_PET")}");
                            }
                        }
                    }
                    else
                    {
                        //u_i 2 2000000 0 21 0 0
                        var box = session.Character.Inventory.LoadBySlotAndType(slot: inv.Slot,
                            type: InventoryType.Equipment);
                        if (box != null)
                        {
                            if (box.Item.ItemSubType == 3)
                            {
                                var roll = box.Item.RollGeneratedItems.Where(predicate: s =>
                                        s.MinimumOriginalItemRare <= box.Rare
                                        && s.MaximumOriginalItemRare >=
                                        box.Rare
                                        && s.OriginalItemDesign == box.Design)
                                    .ToList();
                                var probabilities = roll.Sum(selector: s => s.Probability);
                                var rnd = ServerManager.RandomNumber(min: 0, max: probabilities);
                                var currentrnd = 0;
                                foreach (var rollitem in roll.OrderBy(keySelector: s => ServerManager.RandomNumber()))
                                {
                                    currentrnd += rollitem.Probability;
                                    if (currentrnd >= rnd)
                                    {
                                        var i = ServerManager.GetItem(vnum: rollitem.ItemGeneratedVNum);
                                        sbyte rare = 0;
                                        byte upgrade = 0;
                                        if (i.ItemType == ItemType.Armor || i.ItemType == ItemType.Weapon ||
                                            i.ItemType == ItemType.Shell || i.ItemType == ItemType.Box) rare = box.Rare;
                                        if (i.ItemType == ItemType.Shell)
                                        {
                                            if (rare < 1)
                                                rare = 1;
                                            else if (rare > 7) rare = 7;
                                            upgrade = (byte)ServerManager.RandomNumber(min: 50, max: 81);
                                        }

                                        if (rollitem.IsRareRandom)
                                        {
                                            rnd = ServerManager.RandomNumber();

                                            for (var j = ItemHelper.RareRate.Length - 1; j >= 0; j--)
                                                if (rnd < ItemHelper.RareRate[j])
                                                {
                                                    rare = (sbyte)j;
                                                    break;
                                                }

                                            if (rare < 1) rare = 1;
                                        }

                                        session.Character.GiftAdd(itemVNum: rollitem.ItemGeneratedVNum,
                                            amount: rollitem.ItemGeneratedAmount, rare: (byte)rare, upgrade: upgrade,
                                            design: rollitem.ItemGeneratedDesign);
                                        session.SendPacket(
                                            packet: $"rdi {rollitem.ItemGeneratedVNum} {rollitem.ItemGeneratedAmount}");
                                        session.Character.Inventory.RemoveItemFromInventory(id: box.Id);
                                        return;

                                        //newInv = session.Character.Inventory.AddNewToInventory(rollitem.ItemGeneratedVNum, amount: rollitem.ItemGeneratedAmount, Design: design, Rare: rare);
                                        //if (newInv.Count > 0)
                                        //{
                                        //    short Slot = inv.Slot;
                                        //    if (Slot != -1)
                                        //    {
                                        //        session.SendPacket(session.Character.GenerateSay(
                                        //            $"{Language.Instance.GetMessageFromKey("ITEM_ACQUIRED")}: {newInv.FirstOrDefault(s => s != null)?.Item?.Name} x {rollitem.ItemGeneratedAmount}",
                                        //            12));
                                        //        newInv.Where(s => s != null).ToList()
                                        //            .ForEach(s => session.SendPacket(s.GenerateInventoryAdd()));
                                        //        session.Character.Inventory
                                        //            .RemoveItemAmountFromInventory(box.Id);
                                        //    }
                                        //}
                                    }
                                }
                            }
                            else if (box.HoldingVNum == 0)
                            {
                                if (packetsplit.Length == 1 && int.TryParse(s: packetsplit[0], result: out var petId) &&
                                    session.Character.Mates.Find(match: s => s.MateTransportId == petId) is { } mate)
                                {
                                    if (ItemSubType == 0 && mate.MateType != MateType.Pet ||
                                        ItemSubType == 1 && mate.MateType != MateType.Partner) return;
                                    if (mate.MateType == MateType.Partner && mate.GetInventory().Count > 0)
                                    {
                                        session.SendPacket(
                                            packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "EQ_NOT_EMPTY"),
                                                type: 0));
                                        return;
                                    }

                                    box.HoldingVNum = mate.NpcMonsterVNum;
                                    box.SpLevel = mate.Level;
                                    box.SpDamage = mate.Attack;
                                    box.SpDefence = mate.Defence;
                                    session.Character.Mates.Remove(item: mate);
                                    if (mate.MateType == MateType.Partner)
                                    {
                                        byte i = 0;
                                        session.Character.Mates.Where(predicate: s => s.MateType == MateType.Partner)
                                            .ToList()
                                            .ForEach(action: s =>
                                            {
                                                s.GetInventory().ForEach(action: item =>
                                                    item.Type = (InventoryType)(13 + i));
                                                s.PetId = i;
                                                i++;
                                            });
                                    }

                                    session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateInfo(
                                            message: Language.Instance.GetMessageFromKey(key: "PET_STORED")));
                                    session.SendPacket(packet: UserInterfaceHelper.GeneratePClear());
                                    session.SendPackets(packets: session.Character.GenerateScP());
                                    session.SendPackets(packets: session.Character.GenerateScN());
                                    session.CurrentMapInstance?.Broadcast(packet: mate.GenerateOut());
                                }
                            }
                            else
                            {
                                var heldMonster = ServerManager.GetNpcMonster(npcVNum: box.HoldingVNum);
                                if (heldMonster != null)
                                {
                                    var mate = new Mate(owner: session.Character, npcMonster: heldMonster,
                                        level: box.SpLevel,
                                        matetype: ItemSubType == 0 ? MateType.Pet : MateType.Partner)
                                    {
                                        Attack = box.SpDamage,
                                        Defence = box.SpDefence
                                    };
                                    if (session.Character.AddPet(mate: mate))
                                    {
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                        session.SendPacket(
                                            packet: UserInterfaceHelper.GenerateInfo(
                                                message: Language.Instance.GetMessageFromKey(key: "PET_LEAVE_BEAD")));
                                    }
                                }
                            }
                        }
                    }

                    break;

                case 1:
                    if (option == 0)
                    {
                        session.SendPacket(
                            packet:
                            $"qna #guri^300^8023^{inv.Slot} {Language.Instance.GetMessageFromKey(key: "ASK_RELEASE_PET")}");
                    }
                    else
                    {
                        var heldMonster = ServerManager.GetNpcMonster(npcVNum: (short)EffectValue);
                        if (session.CurrentMapInstance == session.Character.Miniland && heldMonster != null)
                        {
                            var mate = new Mate(owner: session.Character, npcMonster: heldMonster, level: LevelMinimum,
                                matetype: ItemSubType == 1 ? MateType.Partner : MateType.Pet);
                            if (session.Character.AddPet(mate: mate))
                            {
                                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateInfo(
                                        message: Language.Instance.GetMessageFromKey(key: "PET_LEAVE_BEAD")));
                            }
                        }
                        else
                        {
                            session.SendPacket(
                                packet: session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_IN_MINILAND"),
                                    type: 12));
                        }
                    }

                    break;

                case 69:
                    if (EffectValue == 1 || EffectValue == 2)
                    {
                        var box = session.Character.Inventory.LoadBySlotAndType(slot: inv.Slot,
                            type: InventoryType.Equipment);
                        if (box != null)
                        {
                            if (box.HoldingVNum == 0)
                            {
                                session.SendPacket(packet: $"wopen 44 {inv.Slot}");
                            }
                            else
                            {
                                var newInv = session.Character.Inventory.AddNewToInventory(vnum: box.HoldingVNum);
                                if (newInv.Count > 0)
                                {
                                    var itemInstance = newInv[index: 0];
                                    var specialist =
                                        session.Character.Inventory.LoadBySlotAndType(slot: itemInstance.Slot,
                                            type: itemInstance.Type);
                                    if (specialist != null)
                                    {
                                        specialist.SlDamage = box.SlDamage;
                                        specialist.SlDefence = box.SlDefence;
                                        specialist.SlElement = box.SlElement;
                                        specialist.SlHp = box.SlHp;
                                        specialist.SpDamage = box.SpDamage;
                                        specialist.SpDark = box.SpDark;
                                        specialist.SpDefence = box.SpDefence;
                                        specialist.SpElement = box.SpElement;
                                        specialist.SpFire = box.SpFire;
                                        specialist.SpHp = box.SpHp;
                                        specialist.SpLevel = box.SpLevel;
                                        specialist.SpLight = box.SpLight;
                                        specialist.SpStoneUpgrade = box.SpStoneUpgrade;
                                        specialist.SpWater = box.SpWater;
                                        specialist.Upgrade = box.Upgrade;
                                        specialist.EquipmentSerialId = box.EquipmentSerialId;
                                        specialist.Xp = box.Xp;
                                    }

                                    var slot = inv.Slot;
                                    if (slot != -1)
                                    {
                                        if (specialist != null)
                                        {
                                            session.SendPacket(packet: session.Character.GenerateSay(
                                                message:
                                                $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {specialist.Item.Name} + {specialist.Upgrade}",
                                                type: 12));
                                            newInv.ForEach(action: s =>
                                                session.SendPacket(packet: specialist.GenerateInventoryAdd()));
                                        }

                                        session.Character.Inventory.RemoveItemFromInventory(id: box.Id);
                                    }
                                }
                                else
                                {
                                    session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                            type: 0));
                                }
                            }
                        }
                    }

                    if (EffectValue == 3)
                    {
                        var box = session.Character.Inventory.LoadBySlotAndType(slot: inv.Slot,
                            type: InventoryType.Equipment);
                        if (box != null)
                        {
                            if (box.HoldingVNum == 0)
                            {
                                session.SendPacket(packet: $"guri 26 0 {inv.Slot}");
                            }
                            else
                            {
                                var newInv = session.Character.Inventory.AddNewToInventory(vnum: box.HoldingVNum);
                                if (newInv.Count > 0)
                                {
                                    var itemInstance = newInv[index: 0];
                                    var fairy = session.Character.Inventory.LoadBySlotAndType(slot: itemInstance.Slot,
                                        type: itemInstance.Type);
                                    if (fairy != null) fairy.ElementRate = box.ElementRate;
                                    var slot = inv.Slot;
                                    if (slot != -1)
                                    {
                                        if (fairy != null)
                                        {
                                            session.SendPacket(packet: session.Character.GenerateSay(
                                                message:
                                                $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {fairy.Item.Name} ({fairy.ElementRate}%)",
                                                type: 12));
                                            newInv.ForEach(action: s =>
                                                session.SendPacket(packet: fairy.GenerateInventoryAdd()));
                                        }

                                        session.Character.Inventory.RemoveItemFromInventory(id: box.Id);
                                    }
                                }
                                else
                                {
                                    session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                            type: 0));
                                }
                            }
                        }
                    }

                    if (EffectValue == 4)
                    {
                        var box = session.Character.Inventory.LoadBySlotAndType(slot: inv.Slot,
                            type: InventoryType.Equipment);
                        if (box != null)
                        {
                            if (box.HoldingVNum == 0)
                            {
                                session.SendPacket(packet: $"guri 24 0 {inv.Slot}");
                            }
                            else
                            {
                                var newInv = session.Character.Inventory.AddNewToInventory(vnum: box.HoldingVNum);
                                if (newInv.Count > 0)
                                {
                                    var slot = inv.Slot;
                                    if (slot != -1)
                                    {
                                        session.SendPacket(packet: session.Character.GenerateSay(
                                            message:
                                            $"{Language.Instance.GetMessageFromKey(key: "ITEM_ACQUIRED")}: {newInv[index: 0].Item.Name} x 1)",
                                            type: 12));
                                        newInv.ForEach(
                                            action: s => session.SendPacket(packet: s.GenerateInventoryAdd()));
                                        session.Character.Inventory.RemoveItemFromInventory(id: box.Id);
                                    }
                                }
                                else
                                {
                                    session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "NOT_ENOUGH_PLACE"),
                                            type: 0));
                                }
                            }
                        }
                    }

                    break;

                case 888:
                    if (session.Character.IsVehicled)
                        if (!session.Character.Buff.Any(predicate: s => s.Card.CardId == 336))
                        {
                            if (inv.ItemDeleteTime == null)
                                inv.ItemDeleteTime = DateTime.Now.AddHours(value: LevelMinimum);
                            session.Character.VehicleItem.BCards.ForEach(action: s =>
                                s.ApplyBCards(session: session.Character.BattleEntity,
                                    sender: session.Character.BattleEntity));
                            session.CurrentMapInstance.Broadcast(packet: $"eff 1 {session.Character.CharacterId} 885");
                        }

                    break;

                default:
                    Logger.Warn(data: string.Format(format: Language.Instance.GetMessageFromKey(key: "NO_HANDLER_ITEM"),
                        GetType(), VNum,
                        Effect, EffectValue));
                    break;
            }
        }

        #endregion
    }
}