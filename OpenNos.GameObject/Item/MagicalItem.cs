﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using OpenNos.Core;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class MagicalItem : Item
    {
        #region Instantiation

        public MagicalItem(ItemDto item) : base(item: item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte option = 0,
            string[] packetsplit = null)
        {
            if (session.Character.IsVehicled)
            {
                session.SendPacket(
                    packet: session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_DO_VEHICLED"), type: 10));
                return;
            }

            if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance) return;

            switch (Effect)
            {
                // airwaves - eventitems
                case 0:
                    if (inv.Item.ItemType == ItemType.Shell)
                    {
                        if (!inv.ShellEffects.Any())
                        {
                            session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "SHELL_MUST_BE_IDENTIFIED"),
                                type: 0));
                            return;
                        }

                        if (packetsplit?.Length < 9) return;

                        Debug.Assert(condition: packetsplit != null, message: nameof(packetsplit) + " != null");
                        if (!int.TryParse(s: packetsplit[6], result: out var requestType)) return;
                        if (!Enum.TryParse(value: packetsplit[8], result: out InventoryType eqType)) return;

                        if (inv.ShellEffects.Count != 0 && packetsplit.Length > 9 &&
                            byte.TryParse(s: packetsplit[9], result: out var islot))
                        {
                            var wearable =
                                session.Character.Inventory.LoadBySlotAndType(slot: islot,
                                    type: InventoryType.Equipment);
                            if (wearable != null &&
                                (wearable.Item.ItemType == ItemType.Weapon ||
                                 wearable.Item.ItemType == ItemType.Armor) &&
                                wearable.Item.LevelMinimum >= inv.Upgrade && wearable.Rare >= inv.Rare &&
                                !wearable.Item.IsHeroic)
                                switch (requestType)
                                {
                                    case 0:
                                        session.SendPacket(packet: wearable.ShellEffects.Any()
                                            ? $"qna #u_i^1^{session.Character.CharacterId}^{(short)inv.Type}^{inv.Slot}^1^1^{(short)eqType}^{islot} {Language.Instance.GetMessageFromKey(key: "ADD_OPTION_ON_STUFF_NOT_EMPTY")}"
                                            : $"qna #u_i^1^{session.Character.CharacterId}^{(short)inv.Type}^{inv.Slot}^1^1^{(short)eqType}^{islot} {Language.Instance.GetMessageFromKey(key: "ADD_OPTION_ON_STUFF")}");
                                        break;
                                    case 1:

                                        if (inv.ShellEffects == null) return;
                                        if (wearable.BoundCharacterId != session.Character.CharacterId &&
                                            wearable.BoundCharacterId != null)
                                        {
                                            session.SendPacket(
                                                packet: UserInterfaceHelper.GenerateMsg(
                                                    message: Language.Instance.GetMessageFromKey(key: "NEED_FRAGANCE"),
                                                    type: 0));
                                            return;
                                        }

                                        if (wearable.Rare < inv.Rare)
                                        {
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(
                                                    key: "SHELL_RARITY_TOO_HIGH"), type: 0));
                                            return;
                                        }

                                        if (wearable.Item.LevelMinimum < inv.Upgrade)
                                        {
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(
                                                    key: "SHELL_LEVEL_TOO_HIGH"), type: 0));
                                            return;
                                        }

                                        bool weapon;
                                        if (inv.ItemVNum >= 565 && inv.ItemVNum <= 576 ||
                                            inv.ItemVNum >= 589 && inv.ItemVNum <= 598)
                                            weapon = true;
                                        else if (inv.ItemVNum >= 577 && inv.ItemVNum <= 588 ||
                                                 inv.ItemVNum >= 656 && inv.ItemVNum <= 664 || inv.ItemVNum == 599)
                                            weapon = false;
                                        else
                                            return;
                                        if (wearable.Item.ItemType == ItemType.Weapon && weapon ||
                                            wearable.Item.ItemType == ItemType.Armor && !weapon)
                                        {
                                            if (wearable.ShellEffects.Count > 0 && ServerManager.RandomNumber() < 50)
                                            {
                                                session.Character.DeleteItemByItemInstanceId(id: inv.Id);
                                                session.SendPacket(
                                                    packet: UserInterfaceHelper.GenerateMsg(
                                                        message: Language.Instance
                                                            .GetMessageFromKey(key: "OPTION_FAIL"), type: 0));
                                                return;
                                            }

                                            wearable.BoundCharacterId = session.Character.CharacterId;
                                            wearable.ShellRarity = inv.Rare;
                                            wearable.ShellEffects.Clear();
                                            DaoFactory.ShellEffectDao.DeleteByEquipmentSerialId(
                                                id: wearable.EquipmentSerialId);
                                            wearable.ShellEffects.AddRange(collection: inv.ShellEffects);
                                            if (wearable.EquipmentSerialId == Guid.Empty)
                                                wearable.EquipmentSerialId = Guid.NewGuid();
                                            DaoFactory.ShellEffectDao.InsertOrUpdateFromList(
                                                shellEffects: wearable.ShellEffects,
                                                equipmentSerialId: wearable.EquipmentSerialId);
                                            session.Character.DeleteItemByItemInstanceId(id: inv.Id);
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "OPTION_SUCCESS"),
                                                type: 0));
                                        }

                                        break;
                                }
                        }

                        return;
                    }

                    if (ItemType == ItemType.Event)
                    {
                        session.CurrentMapInstance?.Broadcast(packet: StaticPacketHelper.GenerateEff(
                            effectType: UserType.Player,
                            callerId: session.Character.CharacterId, effectId: EffectValue));
                        if (MappingHelper.GuriItemEffects.ContainsKey(key: EffectValue))
                            session.CurrentMapInstance?.Broadcast(
                                packet: UserInterfaceHelper.GenerateGuri(type: 19, argument: 1,
                                    callerId: session.Character.CharacterId,
                                    value: MappingHelper.GuriItemEffects[key: EffectValue]),
                                xRangeCoordinate: session.Character.PositionX,
                                yRangeCoordinate: session.Character.PositionY);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                //respawn objects
                case 1:
                    if (session.Character.MapInstance.MapInstanceType != MapInstanceType.BaseMapInstance ||
                        session.Character.IsSeal)
                    {
                        session.SendPacket(
                            packet: session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "CANT_USE_THAT"), type: 10));
                        return;
                    }

                    int type, secondaryType, inventoryType, slot;
                    if (packetsplit?.Length > 6 && int.TryParse(s: packetsplit[2], result: out type) &&
                        int.TryParse(s: packetsplit[3], result: out secondaryType) &&
                        int.TryParse(s: packetsplit[4], result: out inventoryType) &&
                        int.TryParse(s: packetsplit[5], result: out slot))
                    {
                        int packetType;
                        switch (EffectValue)
                        {
                            case 0:
                                if (inv.ItemVNum != 2070 && inv.ItemVNum != 10010 ||
                                    session.CurrentMapInstance.Map.MapTypes.Any(predicate: m =>
                                        m.MapTypeId == (short)MapTypeEnum.Act51 ||
                                        m.MapTypeId == (short)MapTypeEnum.Act52)) return;
                                if (option == 0)
                                {
                                    if (ServerManager.Instance.ChannelId == 51)
                                        session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                            dialog:
                                            $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^2 #u_i^{type}^{secondaryType}^{inventoryType}^{slot}^0 {Language.Instance.GetMessageFromKey(key: "WANT_TO_GO_BASE")}"));
                                    else
                                        session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                            dialog:
                                            $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^1 #u_i^{type}^{secondaryType}^{inventoryType}^{slot}^2 {Language.Instance.GetMessageFromKey(key: "WANT_TO_SAVE_POSITION")}"));
                                }
                                else if (int.TryParse(s: packetsplit[6], result: out packetType))
                                {
                                    switch (packetType)
                                    {
                                        case 1:
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000,
                                                type: 7,
                                                argument: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^3"));
                                            break;

                                        case 2:
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000,
                                                type: 7,
                                                argument: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^4"));
                                            break;

                                        case 3:
                                            session.Character.SetReturnPoint(mapId: session.Character.MapId,
                                                mapX: session.Character.MapX, mapY: session.Character.MapY);
                                            var respawn = session.Character.Respawn;
                                            if (respawn.DefaultX != 0 && respawn.DefaultY != 0 &&
                                                respawn.DefaultMapId != 0)
                                            {
                                                var mapCell = new MapCell();
                                                for (var i = 0; i < 5; i++)
                                                {
                                                    mapCell.X = (short)(respawn.DefaultX +
                                                                         ServerManager.RandomNumber(min: -3, max: 3));
                                                    mapCell.Y = (short)(respawn.DefaultY +
                                                                         ServerManager.RandomNumber(min: -3, max: 3));
                                                    if (ServerManager.GetMapInstanceByMapId(mapId: respawn.DefaultMapId)
                                                        is { } goToMap)
                                                        if (!goToMap.Map.IsBlockedZone(x: mapCell.X, y: mapCell.Y))
                                                            break;
                                                }

                                                ServerManager.Instance.ChangeMap(id: session.Character.CharacterId,
                                                    mapId: respawn.DefaultMapId, mapX: mapCell.X, mapY: mapCell.Y);
                                            }

                                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                            break;

                                        case 4:
                                            var respawnObj = session.Character.Respawn;
                                            if (ServerManager.Instance.ChannelId == 51)
                                            {
                                                respawnObj.DefaultMapId = (short)(129 + session.Character.Faction);
                                                respawnObj.DefaultX = 41;
                                                respawnObj.DefaultY = 42;
                                            }

                                            if (respawnObj.DefaultX != 0 && respawnObj.DefaultY != 0 &&
                                                respawnObj.DefaultMapId != 0)
                                            {
                                                var mapCell = new MapCell();
                                                for (var i = 0; i < 5; i++)
                                                {
                                                    mapCell.X = (short)(respawnObj.DefaultX +
                                                                         ServerManager.RandomNumber(min: -3, max: 3));
                                                    mapCell.Y = (short)(respawnObj.DefaultY +
                                                                         ServerManager.RandomNumber(min: -3, max: 3));
                                                    if (ServerManager.GetMapInstanceByMapId(
                                                        mapId: respawnObj.DefaultMapId) is { } goToMap)
                                                        if (!goToMap.Map.IsBlockedZone(x: mapCell.X, y: mapCell.Y))
                                                            break;
                                                }

                                                ServerManager.Instance.ChangeMap(id: session.Character.CharacterId,
                                                    mapId: respawnObj.DefaultMapId, mapX: mapCell.X, mapY: mapCell.Y);
                                            }

                                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                            break;
                                    }
                                }

                                break;

                            case 1:
                                if (inv.ItemVNum != 2071 && inv.ItemVNum != 10011) return;
                                if (int.TryParse(s: packetsplit[6], result: out packetType))
                                {
                                    if (ServerManager.Instance.ChannelId == 51)
                                    {
                                        session.SendPacket(
                                            packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "CANNOT_USE"),
                                                type: 10));
                                        return;
                                    }

                                    var respawn = session.Character.Return;
                                    switch (packetType)
                                    {
                                        case 0:
                                            if (respawn.DefaultX != 0 && respawn.DefaultY != 0 &&
                                                respawn.DefaultMapId != 0)
                                                session.SendPacket(packet: UserInterfaceHelper.GenerateRp(
                                                    mapid: respawn.DefaultMapId,
                                                    x: respawn.DefaultX, y: respawn.DefaultY,
                                                    param: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^1"));
                                            break;

                                        case 1:
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000,
                                                type: 7,
                                                argument: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^2"));
                                            break;

                                        case 2:
                                            if (respawn.DefaultX != 0 && respawn.DefaultY != 0 &&
                                                respawn.DefaultMapId != 0)
                                                ServerManager.Instance.ChangeMap(id: session.Character.CharacterId,
                                                    mapId: respawn.DefaultMapId, mapX: respawn.DefaultX,
                                                    mapY: respawn.DefaultY);
                                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                            break;
                                    }
                                }

                                break;

                            case 2:
                                if (inv.ItemVNum != 2072 && inv.ItemVNum != 10012) return;
                                if (option == 0)
                                {
                                    session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000, type: 7,
                                        argument: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^1"));
                                }
                                else
                                {
                                    ServerManager.Instance.JoinMiniland(session: session, minilandOwner: session);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                }

                                break;

                            case 4:
                                if (inv.ItemVNum != 2188 ||
                                    session.Character.MapInstance.Map.MapTypes.Any(predicate: s =>
                                        s.MapTypeId == (short)MapTypeEnum.Act4)) return;
                                if (option == 0)
                                {
                                    session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000, type: 7,
                                        argument: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^1"));
                                }
                                else
                                {
                                    ServerManager.Instance.ChangeMap(id: session.Character.CharacterId, mapId: 98,
                                        mapX: 28, mapY: 34);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                }

                                break;

                            case 5:
                                if (inv.ItemVNum != 2311 || ServerManager.Instance.ChannelId != 51) return;
                                if (ServerManager.GetAllMapInstances().SelectMany(selector: s => s.Monsters.ToList())
                                    .LastOrDefault(predicate: s =>
                                        s.MonsterVNum == (short)session.Character.Faction + 964) is { } flag)
                                {
                                    if (option == 0)
                                    {
                                        session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000,
                                            type: 7,
                                            argument: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^1"));
                                    }
                                    else
                                    {
                                        ServerManager.Instance.ChangeMapInstance(
                                            characterId: session.Character.CharacterId,
                                            mapInstanceId: flag.MapInstance.MapInstanceId, mapX: flag.MapX,
                                            mapY: flag.MapY);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    }
                                }

                                break;

                            case 6:
                                if (inv.ItemVNum != 2384 || ServerManager.Instance.ChannelId == 51 ||
                                    session.CurrentMapInstance.MapInstanceType != MapInstanceType.BaseMapInstance)
                                    return;
                                if (option == 0)
                                    session.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                        dialog:
                                        $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^1 #u_i^{type}^{secondaryType}^{inventoryType}^{slot}^2 {Language.Instance.GetMessageFromKey(key: "WANT_TO_SAVE_POSITION")}"));
                                else if (int.TryParse(s: packetsplit[6], result: out packetType))
                                    switch (packetType)
                                    {
                                        case 1:
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000,
                                                type: 7,
                                                argument: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^3"));
                                            break;

                                        case 2:
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000,
                                                type: 7,
                                                argument: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^4"));
                                            break;

                                        case 3:
                                            if (session.CurrentMapInstance.Map.MapTypes.Any(predicate: s =>
                                                s.MapTypeId == (short)MapTypeEnum.Act51 ||
                                                s.MapTypeId == (short)MapTypeEnum.Act52))
                                            {
                                                session.Character.SetReturnPoint(mapId: session.Character.MapId,
                                                    mapX: session.Character.MapX, mapY: session.Character.MapY);
                                                var respawn = session.Character.Respawn;
                                                if (respawn.DefaultX != 0 && respawn.DefaultY != 0 &&
                                                    respawn.DefaultMapId != 0)
                                                {
                                                    var mapCell = new MapCell();
                                                    for (var i = 0; i < 5; i++)
                                                    {
                                                        mapCell.X = (short)(respawn.DefaultX +
                                                                             ServerManager.RandomNumber(min: -3,
                                                                                 max: 3));
                                                        mapCell.Y = (short)(respawn.DefaultY +
                                                                             ServerManager.RandomNumber(min: -3,
                                                                                 max: 3));
                                                        if (ServerManager.GetMapInstanceByMapId(
                                                            mapId: respawn.DefaultMapId) is { } goToMap)
                                                            if (!goToMap.Map.IsBlockedZone(x: mapCell.X, y: mapCell.Y))
                                                                break;
                                                    }

                                                    ServerManager.Instance.ChangeMap(id: session.Character.CharacterId,
                                                        mapId: respawn.DefaultMapId, mapX: mapCell.X, mapY: mapCell.Y);
                                                }

                                                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                            }
                                            else
                                            {
                                                goto case 4;
                                            }

                                            break;

                                        case 4:
                                            var mapInstanceBackup = session.CurrentMapInstance;
                                            session.CurrentMapInstance =
                                                ServerManager.GetMapInstanceByMapId(mapId: 170);
                                            var respawnObj = session.Character.Respawn;
                                            session.CurrentMapInstance = mapInstanceBackup;
                                            if (ServerManager.Instance.ChannelId == 51)
                                            {
                                                respawnObj.DefaultMapId = (short)(129 + session.Character.Faction);
                                                respawnObj.DefaultX = 41;
                                                respawnObj.DefaultY = 42;
                                            }

                                            if (respawnObj.DefaultX != 0 && respawnObj.DefaultY != 0 &&
                                                respawnObj.DefaultMapId != 0)
                                            {
                                                var mapCell = new MapCell();
                                                for (var i = 0; i < 5; i++)
                                                {
                                                    mapCell.X = (short)(respawnObj.DefaultX +
                                                                         ServerManager.RandomNumber(min: -3, max: 3));
                                                    mapCell.Y = (short)(respawnObj.DefaultY +
                                                                         ServerManager.RandomNumber(min: -3, max: 3));
                                                    if (ServerManager.GetMapInstanceByMapId(
                                                        mapId: respawnObj.DefaultMapId) is { } goToMap)
                                                        if (!goToMap.Map.IsBlockedZone(x: mapCell.X, y: mapCell.Y))
                                                            break;
                                                }

                                                ServerManager.Instance.ChangeMap(id: session.Character.CharacterId,
                                                    mapId: respawnObj.DefaultMapId, mapX: mapCell.X, mapY: mapCell.Y);
                                            }

                                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                            break;
                                    }

                                break;
                        }
                    }

                    break;

                // dyes or waxes
                case 10:
                case 11:
                    if (!session.Character.IsVehicled)
                    {
                        if (Effect == 10)
                        {
                            if (EffectValue == 99)
                            {
                                var nextValue = (byte)ServerManager.RandomNumber(min: 0, max: 127);
                                session.Character.HairColor =
                                    Enum.IsDefined(enumType: typeof(HairColorType), value: nextValue)
                                        ? (HairColorType)nextValue
                                        : 0;
                            }
                            else
                            {
                                session.Character.HairColor = Enum.IsDefined(enumType: typeof(HairColorType),
                                    value: (byte)EffectValue)
                                    ? (HairColorType)EffectValue
                                    : 0;
                            }
                        }
                        else if (Effect == 11)
                        {
                            if (session.Character.Class == (byte)ClassType.Adventurer && EffectValue > 1)
                            {
                                session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "ADVENTURERS_CANT_USE"),
                                    type: 10));
                                return;
                            }

                            if (session.Character.Gender != (GenderType)Sex && (VNum < 2130 || VNum > 2162) &&
                                (VNum < 10025 || VNum > 10026))
                            {
                                session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "CANNOT_USE"),
                                        type: 10));
                                return;
                            }

                            session.Character.HairStyle = Enum.IsDefined(enumType: typeof(HairStyleType),
                                value: (byte)EffectValue)
                                ? (HairStyleType)EffectValue
                                : 0;
                        }
                        else
                        {
                            if (session.Character.Class == (byte)ClassType.Adventurer && EffectValue > 1)
                            {
                                session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "ADVENTURERS_CANT_USE"),
                                    type: 10));
                                return;
                            }

                            session.Character.HairStyle = Enum.IsDefined(enumType: typeof(HairStyleType),
                                value: (byte)EffectValue)
                                ? (HairStyleType)EffectValue
                                : 0;
                        }

                        session.SendPacket(packet: session.Character.GenerateEq());
                        session.CurrentMapInstance?.Broadcast(client: session, content: session.Character.GenerateIn());
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GenerateGidx());
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                // TS npcs health
                case 12:
                    if (EffectValue > 0)
                        if (session.Character.Timespace != null)
                        {
                            session.Character.MapInstance.GetBattleEntitiesInRange(
                                    pos: new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    distance: 6)
                                .Where(predicate: s => (s.MapNpc != null || s.Mate != null && s.Mate.IsTemporalMate) &&
                                                       !session.Character.BattleEntity.CanAttackEntity(receiver: s))
                                .ToList()
                                .ForEach(action: s =>
                                {
                                    var health = EffectValue;
                                    if (s.Hp + EffectValue > s.HpMax) health = s.HpMax - s.Hp;
                                    s.Hp += health;
                                    session.Character.MapInstance.Broadcast(
                                        packet: s.GenerateRc(characterHealth: health));
                                    s.Mate?.Owner.Session.SendPacket(packet: s.Mate.GenerateStatInfo());
                                });
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                            session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                callerId: session.Character.CharacterId, effectId: 48));
                        }

                    break;

                // dignity restoration
                case 14:
                    if ((EffectValue == 100 || EffectValue == 200) && session.Character.Dignity < 100 &&
                        !session.Character.IsVehicled)
                    {
                        session.Character.Dignity += EffectValue;
                        if (session.Character.Dignity > 100) session.Character.Dignity = 100;
                        session.SendPacket(packet: session.Character.GenerateFd());
                        session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: session.Character.CharacterId, effectId: 49 - (byte)session.Character.Faction));
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GenerateIn(InEffect: 1),
                            receiver: ReceiverType.AllExceptMe);
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GenerateGidx(),
                            receiver: ReceiverType.AllExceptMe);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }
                    else if (EffectValue == 2000 && session.Character.Dignity < 100 && !session.Character.IsVehicled)
                    {
                        session.Character.Dignity = 100;
                        session.SendPacket(packet: session.Character.GenerateFd());
                        session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: session.Character.CharacterId, effectId: 49 - (byte)session.Character.Faction));
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GenerateIn(InEffect: 1),
                            receiver: ReceiverType.AllExceptMe);
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GenerateGidx(),
                            receiver: ReceiverType.AllExceptMe);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                // speakers
                case 15:
                    if (!session.Character.IsVehicled && option == 0)
                        session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: 10, argument: 3,
                            callerId: session.Character.CharacterId, value: 1));
                    break;

                // bubbles
                case 16:
                    if (!session.Character.IsVehicled && option == 0)
                        session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: 10, argument: 4,
                            callerId: session.Character.CharacterId, value: 1));
                    break;

                // wigs
                case 30:
                    if (!session.Character.IsVehicled)
                    {
                        var wig = session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Hat,
                            type: InventoryType.Wear);
                        if (wig != null)
                        {
                            wig.Design = (byte)ServerManager.RandomNumber(min: 0, max: 15);
                            session.SendPacket(packet: session.Character.GenerateEq());
                            session.SendPacket(packet: session.Character.GenerateEquipment());
                            session.CurrentMapInstance?.Broadcast(client: session,
                                content: session.Character.GenerateIn());
                            session.CurrentMapInstance?.Broadcast(client: session,
                                content: session.Character.GenerateGidx());
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        }
                        else
                        {
                            session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NO_WIG"), type: 0));
                        }
                    }

                    break;

                case 31:
                    if (!session.Character.IsVehicled && session.Character.HairStyle == HairStyleType.Hair7)
                    {
                        session.Character.HairStyle = HairStyleType.Hair8;
                        session.SendPacket(packet: session.Character.GenerateEq());
                        session.CurrentMapInstance?.Broadcast(client: session, content: session.Character.GenerateIn());
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GenerateGidx());
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        // idk how it works yet but seems like all characters with this hairstyle have DarkPurple hair
                        //session.Character.HairColor = HairColorType.DarkPurple;
                    }

                    break;

                // Passive skills books
                case 99:
                    if (session.Character.HeroLevel >= EffectValue)
                    {
                        if (session.Character.AddSkill(skillVNum: (short)(Sex + 1)))
                        {
                            session.SendPacket(packet: session.Character.GenerateSki());
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(
                            packet: session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "LOW_HERO_LVL"), type: 10));
                    }

                    break;

                case 300:
                    if (session.Character.Group != null && session.Character.Group.GroupType != GroupType.Group &&
                        session.Character.Group.IsLeader(session: session) &&
                        session.CurrentMapInstance.Portals.Any(predicate: s => s.Type == (short)PortalType.Raid))
                    {
                        var delay = 0;
                        foreach (var sess in session.Character.Group.Sessions.GetAllItems())
                        {
                            Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: delay)).Subscribe(onNext: o =>
                            {
                                if (sess?.Character != null && session.CurrentMapInstance != null &&
                                    session.Character != null &&
                                    sess.Character != session.Character)
                                    ServerManager.Instance.TeleportOnRandomPlaceInMap(session: sess,
                                        guid: session.CurrentMapInstance.MapInstanceId);
                            });
                            delay += 100;
                        }

                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                default:
                    Logger.Warn(data: string.Format(format: Language.Instance.GetMessageFromKey(key: "NO_HANDLER_ITEM"),
                        GetType(), VNum,
                        Effect, EffectValue));
                    break;
            }
        }

        #endregion
    }
}