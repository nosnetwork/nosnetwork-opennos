﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class FoodItem : Item
    {
        #region Instantiation

        public FoodItem(ItemDto item) : base(item: item)
        {
        }

        #endregion

        #region Members

        static IDisposable RegenerateDisposable { get; set; }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte option = 0,
            string[] packetsplit = null)
        {
            if (session.Character.IsVehicled)
            {
                session.SendPacket(
                    packet: session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_DO_VEHICLED"), type: 10));
                return;
            }

            if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance) return;

            if ((DateTime.Now - session.Character.LastPotion).TotalMilliseconds < 750) return;
            session.Character.LastPotion = DateTime.Now;
            var item = inv.Item;
            switch (Effect)
            {
                default:
                    if (session.Character.Hp <= 0) return;

                    if (item.VNum == 2291 || item.VNum == 10035)
                    {
                        if (!session.Character.IsSitting) session.Character.Rest();

                        session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                            callerId: session.Character.CharacterId, effectId: 6000));

                        session.Character.SpPoint += 1500;

                        if (session.Character.SpPoint > 10000) session.Character.SpPoint = 10000;

                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateSpPoint());
                        return;
                    }

                    if (session.Character.FoodAmount < 0) session.Character.FoodAmount = 0;
                    var amount = session.Character.FoodAmount;

                    if (amount < 5)
                    {
                        if (item.BCards.Find(match: s =>
                                s.Type == (byte)BCardType.CardType.Hpmp &&
                                s.SubType == (byte)AdditionalTypes.Hpmp.ReceiveAdditionalHp / 10 &&
                                s.FirstData > 0) is { }
                            additionalHpBCard)
                        {
                            // MaxAdditionalHpPercent = AdditionalHp.SecondData;
                            double additionalHp = 0;
                            if (session.Character.BattleEntity.AdditionalHp + additionalHpBCard.FirstData <=
                                session.Character.HPLoad() * 0.2)
                                additionalHp = additionalHpBCard.FirstData;
                            else if (session.Character.BattleEntity.AdditionalHp < session.Character.HPLoad() * 0.2)
                                additionalHp = session.Character.HPLoad() * 0.2 -
                                               session.Character.BattleEntity.AdditionalHp;
                            if (additionalHp > 0 && additionalHp <= additionalHpBCard.FirstData)
                            {
                                session.Character.FoodAmount++;
                                session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                    callerId: session.Character.CharacterId, effectId: 6000));
                                session.Character.BattleEntity.AdditionalHp += additionalHp;
                                session.SendPacket(packet: session.Character.GenerateAdditionalHpMp());
                                session.SendPacket(packet: session.Character.GenerateStat());
                                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1800))
                                    .Subscribe(onNext: s => session.Character.FoodAmount--);
                            }

                            return;
                        }

                        if (item.BCards.Find(match: s =>
                                s.Type == (byte)BCardType.CardType.Hpmp &&
                                s.SubType == (byte)AdditionalTypes.Hpmp.ReceiveAdditionalMp / 10 &&
                                s.FirstData < 0) is { }
                            additionalMpBCard)
                        {
                            // MaxAdditionalMpPercent = AdditionalMp.SecondData;
                            double additionalMp = 0;
                            if (session.Character.BattleEntity.AdditionalMp + -additionalMpBCard.FirstData <=
                                session.Character.MPLoad() * 0.2)
                                additionalMp = -additionalMpBCard.FirstData;
                            else if (session.Character.BattleEntity.AdditionalMp < session.Character.MPLoad() * 0.2)
                                additionalMp = session.Character.MPLoad() * 0.2 -
                                               session.Character.BattleEntity.AdditionalMp;
                            if (additionalMp > 0 && additionalMp <= -additionalMpBCard.FirstData)
                            {
                                session.Character.FoodAmount++;
                                session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                    callerId: session.Character.CharacterId, effectId: 6000));
                                session.Character.BattleEntity.AdditionalMp += additionalMp;
                                session.SendPacket(packet: session.Character.GenerateAdditionalHpMp());
                                session.SendPacket(packet: session.Character.GenerateStat());
                                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1800))
                                    .Subscribe(onNext: s => session.Character.FoodAmount--);
                            }

                            return;
                        }
                    }

                    if (!session.Character.IsSitting) session.Character.Rest();
                    session.Character.Mates.Where(predicate: s => s.IsTeamMember).ToList().ForEach(action: m =>
                        session.CurrentMapInstance?.Broadcast(packet: m.GenerateRest(ownerSit: true)));

                    if (amount < 5)
                    {
                        if (!session.Character.IsSitting) return;
                        var workerThread = new Thread(start: () => Regenerate(session: session, item: item));
                        workerThread.Start();
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }
                    else
                    {
                        session.SendPacket(packet: session.Character.Gender == GenderType.Female
                            ? session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_HUNGRY_FEMALE"), type: 1)
                            : session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_HUNGRY_MALE"), type: 1));
                    }

                    if (amount == 0)
                    {
                        if (!session.Character.IsSitting) return;
                        var workerThread2 = new Thread(start: () => Sync(session: session));
                        workerThread2.Start();
                    }

                    break;
            }
        }

        static void Regenerate(ClientSession session, Item item)
        {
            session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                callerId: session.Character.CharacterId, effectId: 6000));
            session.Character.FoodAmount++;
            session.Character.MaxFood = 0;
            session.Character.FoodHp += item.Hp / 5;
            session.Character.FoodMp += item.Mp / 5;
            RegenerateDisposable = Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1800 * 5)).Subscribe(
                onNext: obs =>
                {
                    if (session.Character.FoodHp > 0 || session.Character.FoodMp > 0)
                    {
                        session.Character.FoodHp -= item.Hp / 5;
                        session.Character.FoodMp -= item.Mp / 5;
                        session.Character.FoodAmount--;
                    }
                });
        }

        static void Sync(ClientSession session)
        {
            for (session.Character.MaxFood = 0; session.Character.MaxFood < 5; session.Character.MaxFood++)
            {
                if (session.Character.Hp <= 0 || !session.Character.IsSitting)
                {
                    RegenerateDisposable?.Dispose();
                    session.Character.FoodAmount = 0;
                    session.Character.FoodHp = 0;
                    session.Character.FoodMp = 0;
                    return;
                }

                var hpLoad = (int)session.Character.HPLoad();
                var mpLoad = (int)session.Character.MPLoad();

                var buffRc = session.Character.GetBuff(type: BCardType.CardType.LeonaPassiveSkill,
                    subtype: (byte)AdditionalTypes.LeonaPassiveSkill.IncreaseRecoveryItems)[0] / 100D;

                var hpAmount = session.Character.FoodHp + (int)(session.Character.FoodHp * buffRc);
                var mpAmount = session.Character.FoodMp + (int)(session.Character.FoodMp * buffRc);

                if (session.Character.Hp + hpAmount > hpLoad) hpAmount = hpLoad - session.Character.Hp;

                if (session.Character.Mp + mpAmount > mpLoad) mpAmount = mpLoad - session.Character.Mp;

                var convertRecoveryToDamage = ServerManager.RandomNumber() <
                                              session.Character.GetBuff(type: BCardType.CardType.DarkCloneSummon,
                                                  subtype: (byte)AdditionalTypes.DarkCloneSummon
                                                      .ConvertRecoveryToDamage)[0];

                if (convertRecoveryToDamage)
                {
                    session.Character.Hp -= hpAmount;

                    if (session.Character.Hp < 1) session.Character.Hp = 1;

                    if (hpAmount > 0)
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GenerateDm(dmg: hpAmount));
                }
                else
                {
                    session.Character.Hp += hpAmount;

                    if (hpAmount > 0)
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: session.Character.GenerateRc(characterHealth: hpAmount));
                }

                session.Character.Mp += mpAmount;

                foreach (var mate in session.Character.Mates.Where(predicate: s =>
                    s.IsTeamMember && s.IsAlive && s.IsSitting))
                {
                    hpLoad = mate.HpLoad();
                    mpLoad = mate.MpLoad();

                    hpAmount = session.Character.FoodHp;
                    mpAmount = session.Character.FoodMp;

                    if (mate.Hp + hpAmount > hpLoad) hpAmount = hpLoad - (int)mate.Hp;

                    if (mate.Mp + mpAmount > mpLoad) mpAmount = mpLoad - (int)mate.Mp;

                    mate.Hp += hpAmount;
                    mate.Mp += mpAmount;

                    if (hpAmount > 0)
                        session.CurrentMapInstance?.Broadcast(client: session,
                            content: mate.GenerateRc(characterHealth: hpAmount));
                }

                if (session.IsConnected)
                {
                    session.SendPacket(packet: session.Character.GenerateStat());

                    if (session.Character.Mates.Any(predicate: m => m.IsTeamMember && m.IsAlive && m.IsSitting))
                        session.SendPackets(packets: session.Character.GeneratePst());

                    Thread.Sleep(millisecondsTimeout: 1800);
                }
            }
        }

        #endregion
    }
}