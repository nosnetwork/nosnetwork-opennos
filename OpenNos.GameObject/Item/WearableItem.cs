﻿using System;
using System.Diagnostics;
using System.Linq;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class WearableItem : Item
    {
        #region Instantiation

        public WearableItem(ItemDto item) : base(item: item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte option = 0,
            string[] packetsplit = null)
        {
            switch (Effect)
            {
                default:
                    var delay = false;
                    if (option == 255)
                    {
                        delay = true;
                        option = 0;
                    }

                    Mate mate = null;
                    if (option != 0)
                    {
                        mate = session.Character.Mates.Find(
                            match: s => s.MateType == MateType.Partner && s.PetId == option - 1);
                        if (mate == null || mate.IsTemporalMate) return;
                    }

                    var slot = inv.Slot;
                    var equipment = InventoryType.Wear;
                    if (option > 0) equipment = (InventoryType)(12 + option);

                    var itemToWearType = inv.Type;

                    if (inv == null) return;
                    if (ItemValidTime > 0 && !inv.IsBound)
                        inv.ItemDeleteTime = DateTime.Now.AddSeconds(value: ItemValidTime);
                    else if (!inv.IsBound && ItemType == ItemType.Jewelery &&
                             new[] { 3951, 3952, 3953, 3954, 3955, 7427 }.Contains(value: EffectValue))
                        inv.ItemDeleteTime = DateTime.Now.AddSeconds(value: 3600);
                    if (!inv.IsBound)
                    {
                        switch (inv.Item.Effect)
                        {
                            case 790: // Tarot
                            case 932: // Attack amulet
                            case 933: // defense amulet
                                inv.BoundCharacterId = session.Character.CharacterId;
                                break;
                        }

                        if (!delay &&
                            (EquipmentSlot == EquipmentType.Fairy && (MaxElementRate == 70 || MaxElementRate == 80) ||
                             EquipmentSlot == EquipmentType.CostumeHat || EquipmentSlot == EquipmentType.CostumeSuit ||
                             EquipmentSlot == EquipmentType.WeaponSkin))
                        {
                            session.SendPacket(
                                packet:
                                $"qna #u_i^1^{session.Character.CharacterId}^{(byte)itemToWearType}^{slot}^1 {Language.Instance.GetMessageFromKey(key: "ASK_BIND")}");
                            return;
                        }

                        if (delay) inv.BoundCharacterId = session.Character.CharacterId;
                    }

                    var timeSpanSinceLastSpUsage =
                        (DateTime.Now - Process.GetCurrentProcess().StartTime.AddSeconds(value: -50)).TotalSeconds -
                        session.Character.LastSp;

                    if (EquipmentSlot == EquipmentType.Sp && inv.Rare == -2)
                    {
                        session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "CANT_EQUIP_DESTROYED_SP"), type: 0));
                        return;
                    }

                    if (option == 0)
                    {
                        if (EquipmentSlot == EquipmentType.Sp &&
                            timeSpanSinceLastSpUsage <= session.Character.SpCooldown &&
                            session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                                type: InventoryType.Specialist) != null)
                        {
                            session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SP_INLOADING"),
                                    arg0: session.Character.SpCooldown - (int)Math.Round(a: timeSpanSinceLastSpUsage)),
                                type: 0));
                            return;
                        }

                        if (ItemType != ItemType.Weapon
                            && ItemType != ItemType.Armor
                            && ItemType != ItemType.Fashion
                            && ItemType != ItemType.Jewelery
                            && ItemType != ItemType.Specialist
                            || LevelMinimum > (IsHeroic ? session.Character.HeroLevel : session.Character.Level) ||
                            Sex != 0 && Sex != (byte)session.Character.Gender + 1
                            || ItemType != ItemType.Jewelery && EquipmentSlot != EquipmentType.Boots &&
                            EquipmentSlot != EquipmentType.Gloves &&
                            (Class >> (byte)session.Character.Class & 1) != 1)
                        {
                            session.SendPacket(
                                packet: session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "BAD_EQUIPMENT"),
                                    type: 10));
                            return;
                        }

                        if (session.Character.UseSp)
                            if (session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                                    type: equipment) is { } sp && sp.Item.Element != 0 &&
                                EquipmentSlot == EquipmentType.Fairy &&
                                Element != sp.Item.Element && Element != sp.Item.SecondaryElement)
                            {
                                session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "BAD_FAIRY"),
                                        type: 0));
                                return;
                            }

                        if (ItemType == ItemType.Weapon || ItemType == ItemType.Armor)
                            if (inv.BoundCharacterId.HasValue &&
                                inv.BoundCharacterId.Value != session.Character.CharacterId)
                            {
                                session.SendPacket(
                                    packet: session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "BAD_EQUIPMENT"),
                                        type: 10));
                                return;
                            }

                        if (session.Character.UseSp && EquipmentSlot == EquipmentType.Sp)
                        {
                            session.SendPacket(
                                packet: session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "SP_BLOCKED"), type: 10));
                            return;
                        }

                        if (session.Character.JobLevel < LevelJobMinimum)
                        {
                            session.SendPacket(
                                packet: session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "LOW_JOB_LVL"), type: 10));
                            return;
                        }
                    }
                    else if (mate != null)
                    {
                        if (inv.Item.EquipmentSlot == EquipmentType.MainWeapon ||
                            inv.Item.EquipmentSlot == EquipmentType.SecondaryWeapon ||
                            inv.Item.EquipmentSlot == EquipmentType.Armor)
                            switch (mate.Monster.AttackClass)
                            {
                                case 0:
                                    if (inv.ItemVNum != 990 && inv.ItemVNum != 997)
                                    {
                                        session.SendPacket(
                                            packet: session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(key: "BAD_EQUIPMENT"),
                                                type: 10));
                                        return;
                                    }

                                    break;
                                case 1:
                                    if (inv.ItemVNum != 991 && inv.ItemVNum != 996)
                                    {
                                        session.SendPacket(
                                            packet: session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(key: "BAD_EQUIPMENT"),
                                                type: 10));
                                        return;
                                    }

                                    break;
                                case 2:
                                    if (inv.ItemVNum != 992 && inv.ItemVNum != 995)
                                    {
                                        session.SendPacket(
                                            packet: session.Character.GenerateSay(
                                                message: Language.Instance.GetMessageFromKey(key: "BAD_EQUIPMENT"),
                                                type: 10));
                                        return;
                                    }

                                    break;
                            }

                        if (mate.Level < LevelMinimum)
                        {
                            session.SendPacket(
                                packet: session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "BAD_EQUIPMENT"),
                                    type: 10));
                            return;
                        }

                        var partnerEquipment = ServerManager.GetItem(vnum: inv.ItemVNum);

                        var isBadEquipment = false;

                        switch (partnerEquipment.EquipmentSlot)
                        {
                            case EquipmentType.MainWeapon:
                                {
                                    if (partnerEquipment.ItemSubType != 12)
                                        isBadEquipment = true;
                                    else
                                        mate.WeaponInstance = inv;
                                }
                                break;

                            case EquipmentType.Armor:
                                {
                                    if (partnerEquipment.ItemSubType != 4)
                                        isBadEquipment = true;
                                    else
                                        mate.ArmorInstance = inv;
                                }
                                break;

                            case EquipmentType.Gloves:
                                {
                                    mate.GlovesInstance = inv;
                                }
                                break;

                            case EquipmentType.Boots:
                                {
                                    mate.BootsInstance = inv;
                                }
                                break;

                            case EquipmentType.Sp:
                                {
                                    if (mate.IsUsingSp) return;

                                    if (partnerEquipment.ItemSubType != 4
                                        || !PartnerHelper.CanWearSp(partnerVNum: mate.NpcMonsterVNum,
                                            itemVNum: inv.ItemVNum))
                                    {
                                        isBadEquipment = true;
                                    }
                                    else
                                    {
                                        if (!mate.CanUseSp())
                                        {
                                            var spRemainingCooldown = mate.GetSpRemainingCooldown();
                                            session.SendPacket(packet: session.Character.GenerateSay(
                                                message: string.Format(
                                                    format: Language.Instance.GetMessageFromKey(key: "STAY_TIME"),
                                                    arg0: spRemainingCooldown), type: 11));
                                            session.SendPacket(packet: $"psd {spRemainingCooldown}");
                                            return;
                                        }

                                        mate.Sp = new PartnerSp(instance: inv);
                                    }
                                }
                                break;
                        }

                        if (isBadEquipment)
                        {
                            session.SendPacket(
                                packet: session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "BAD_EQUIPMENT"),
                                    type: 10));
                            return;
                        }
                    }

                    var currentlyEquippedItem =
                        session.Character.Inventory.LoadBySlotAndType(slot: (short)EquipmentSlot, type: equipment);

                    if (currentlyEquippedItem == null)
                    {
                        // move from equipment to wear
                        session.Character.Inventory.MoveInInventory(sourceSlot: inv.Slot, sourceType: itemToWearType,
                            targetType: equipment);
                        session.SendPacket(
                            packet: UserInterfaceHelper.Instance.GenerateInventoryRemove(type: itemToWearType,
                                slot: slot));
                    }
                    else
                    {
                        Logger.LogUserEvent(logEvent: "EQUIPMENT_TAKEOFF", caller: session.GenerateIdentity(),
                            data:
                            $"IIId: {currentlyEquippedItem.Id} ItemVnum: {currentlyEquippedItem.ItemVNum} Upgrade: {currentlyEquippedItem.Upgrade} Rare: {currentlyEquippedItem.Rare}");

                        // move from wear to equipment and back
                        session.Character.Inventory.MoveInInventory(sourceSlot: currentlyEquippedItem.Slot,
                            sourceType: equipment,
                            targetType: itemToWearType, targetSlot: inv.Slot);
                        session.SendPacket(packet: currentlyEquippedItem.GenerateInventoryAdd());
                        session.Character.EquipmentBCards.RemoveAll(match: o =>
                            o.ItemVNum == currentlyEquippedItem.ItemVNum);
                    }

                    Logger.LogUserEvent(logEvent: "EQUIPMENT_WEAR", caller: session.GenerateIdentity(),
                        data: $"IIId: {inv.Id} ItemVnum: {inv.ItemVNum} Upgrade: {inv.Upgrade} Rare: {inv.Rare}");

                    var raid = ServerManager.Instance.Raids
                        .FirstOrDefault(predicate: s =>
                            s.RequiredItems?.Any(predicate: obj => obj?.VNum == VNum) == true)?.Copy();
                    if (raid?.DailyEntries > 0)
                    {
                        var entries = raid.DailyEntries - session.Character.GeneralLogs.CountLinq(predicate: s =>
                            s.LogType == "InstanceEntry" && short.Parse(s: s.LogData) == raid.Id &&
                            s.Timestamp.Date == DateTime.Today);
                        session.SendPacket(packet: session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSTANCE_ENTRIES"),
                                arg0: entries), type: 10));
                    }

                    if (mate == null)
                    {
                        session.Character.EquipmentBCards.AddRange(collection: inv.Item.BCards);

                        switch (inv.Item.ItemType)
                        {
                            case ItemType.Armor:
                                session.Character.ShellEffectArmor.Clear();

                                foreach (var dto in inv.ShellEffects) session.Character.ShellEffectArmor.Add(item: dto);
                                break;
                            case ItemType.Weapon:
                                switch (inv.Item.EquipmentSlot)
                                {
                                    case EquipmentType.MainWeapon:
                                        session.Character.ShellEffectMain.Clear();

                                        foreach (var dto in inv.ShellEffects)
                                            session.Character.ShellEffectMain.Add(item: dto);
                                        break;

                                    case EquipmentType.SecondaryWeapon:
                                        session.Character.ShellEffectSecondary.Clear();

                                        foreach (var dto in inv.ShellEffects)
                                            session.Character.ShellEffectSecondary.Add(item: dto);
                                        break;
                                }

                                break;
                        }
                    }
                    else
                    {
                        mate.BattleEntity.BCards.AddRange(collection: inv.Item.BCards);
                    }

                    if (option == 0)
                    {
                        session.SendPackets(packets: session.Character.GenerateStatChar());
                        session.CurrentMapInstance?.Broadcast(packet: session.Character.GenerateEq());
                        session.SendPacket(packet: session.Character.GenerateEquipment());
                        session.CurrentMapInstance?.Broadcast(packet: session.Character.GeneratePairy());

                        if (EquipmentSlot == EquipmentType.Fairy)
                        {
                            var fairy = session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Fairy,
                                type: equipment);
                            session.SendPacket(packet: session.Character.GenerateSay(
                                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "FAIRYSTATS"),
                                    arg0: fairy.Xp,
                                    arg1: CharacterHelper.LoadFairyXPData(
                                        elementRate: fairy.ElementRate + fairy.Item.ElementRate)), type: 10));
                        }

                        if (EquipmentSlot == EquipmentType.Amulet)
                        {
                            session.SendPacket(packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                callerId: session.Character.CharacterId, effectId: 39));
                            inv.BoundCharacterId = session.Character.CharacterId;
                            if (inv.ItemDeleteTime > DateTime.Now || inv.DurabilityPoint > 0)
                                session.Character.AddBuff(
                                    indicator: new Buff.Buff(id: 62, level: session.Character.Level),
                                    sender: session.Character.BattleEntity);
                        }
                    }
                    else if (mate != null)
                    {
                        session.SendPacket(packet: mate.GenerateScPacket());
                    }

                    break;
            }
        }

        #endregion
    }
}