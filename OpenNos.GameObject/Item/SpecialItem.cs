﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;

namespace OpenNos.GameObject
{
    public class SpecialItem : Item
    {
        #region Instantiation

        public SpecialItem(ItemDto item) : base(item: item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte Option = 0,
            string[] packetsplit = null)
        {
            var itemDesign = inv.Design;

            if (session.Character.IsVehicled && Effect != 1000)
            {
                if (VNum == 5119 || VNum == 9071) // Speed Booster
                {
                    if (!session.Character.Buff.Any(predicate: s => s.Card.CardId == 336))
                    {
                        session.Character.VehicleItem.BCards.ForEach(action: s =>
                            s.ApplyBCards(session: session.Character.BattleEntity,
                                sender: session.Character.BattleEntity));
                        session.CurrentMapInstance.Broadcast(packet: $"eff 1 {session.Character.CharacterId} 885");
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }
                }
                else
                {
                    session.SendPacket(
                        packet: session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "CANT_DO_VEHICLED"), type: 10));
                }

                return;
            }

            if (VNum == 5511)
            {
                session.Character.GeneralLogs.Where(predicate: s =>
                    s.LogType == "InstanceEntry" &&
                    (short.Parse(s: s.LogData) == 16 || short.Parse(s: s.LogData) == 17) &&
                    s.Timestamp.Date == DateTime.Today).ToList().ForEach(action: s =>
                {
                    s.LogType = "NulledInstanceEntry";
                    DaoFactory.GeneralLogDao.InsertOrUpdate(generalLog: ref s);
                });
                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                return;
            }

            if (session.CurrentMapInstance?.MapInstanceType != MapInstanceType.TalentArenaMapInstance
                && (VNum == 5936 || VNum == 5937 || VNum == 5938 || VNum == 5939 || VNum == 5940 || VNum == 5942 ||
                    VNum == 5943 || VNum == 5944 || VNum == 5945 || VNum == 5946))
                return;
            if (session.CurrentMapInstance?.MapInstanceType == MapInstanceType.TalentArenaMapInstance
                && VNum != 5936 && VNum != 5937 && VNum != 5938 && VNum != 5939 && VNum != 5940 && VNum != 5942 &&
                VNum != 5943 && VNum != 5944 && VNum != 5945 && VNum != 5946)
                return;

            if (BCards.Count > 0 && Effect != 1000)
            {
                if (BCards.Any(predicate: s => s.Type == (byte)BCardType.CardType.Buff && s.SubType == 1 &&
                                               new Buff.Buff(id: (short)s.SecondData, level: session.Character.Level)
                                                   .Card
                                                   .BCards.Any(predicate: newbuff =>
                                                       session.Character.Buff.GetAllItems().Any(predicate: b =>
                                                           b.Card.BCards.Any(predicate: buff =>
                                                               buff.CardId != newbuff.CardId
                                                               && (buff.Type == 33 && buff.SubType == 5 &&
                                                                   (newbuff.Type == 33 ||
                                                                    newbuff.Type == 58) || newbuff.Type == 33 &&
                                                                                        newbuff.SubType == 5 &&
                                                                                        (buff.Type == 33 ||
                                                                                         buff.Type == 58)
                                                                                        || buff.Type == 33 &&
                                                                                        (buff.SubType == 1 ||
                                                                                         buff.SubType == 3) &&
                                                                                        newbuff.Type == 58 &&
                                                                                        newbuff.SubType == 1 ||
                                                                                        buff.Type == 33 &&
                                                                                        (buff.SubType == 2 ||
                                                                                         buff.SubType == 4) &&
                                                                                        newbuff.Type == 58 &&
                                                                                        newbuff.SubType == 3
                                                                                        || newbuff.Type == 33 &&
                                                                                        (newbuff.SubType == 1 ||
                                                                                         newbuff.SubType == 3) &&
                                                                                        buff.Type == 58 &&
                                                                                        buff.SubType == 1 ||
                                                                                        newbuff.Type == 33 &&
                                                                                        (newbuff.SubType == 2 ||
                                                                                         newbuff.SubType == 4) &&
                                                                                        buff.Type == 58 &&
                                                                                        buff.SubType == 3
                                                                                        || buff.Type == 33 &&
                                                                                        newbuff.Type == 33 &&
                                                                                        buff.SubType ==
                                                                                        newbuff.SubType ||
                                                                                        buff.Type == 58 &&
                                                                                        newbuff.Type == 58 &&
                                                                                        buff.SubType ==
                                                                                        newbuff.SubType))))))
                    return;
                BCards.ForEach(action: c =>
                    c.ApplyBCards(session: session.Character.BattleEntity, sender: session.Character.BattleEntity));
                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                return;
            }

            switch (Effect)
            {
                // Honour Medals
                case 69:
                    session.Character.Reputation += ReputPrice;
                    session.SendPacket(packet: session.Character.GenerateFd());
                    session.SendPacket(packet: session.Character.GenerateSay(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "REPUT_INCREASE"),
                            arg0: ReputPrice), type: 11));
                    /*session.CurrentMapInstance?.Broadcast(session, session.Character.GenerateIn(InEffect: 1), ReceiverType.AllExceptMe);
                    session.CurrentMapInstance?.Broadcast(session, session.Character.GenerateGidx(), ReceiverType.AllExceptMe);*/
                    //Deactivate Broadcast Message
                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    break;

                // TimeSpace Stones
                case 140:
                    if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                        if (ServerManager.Instance.TimeSpaces.FirstOrDefault(predicate: s => s.Id == EffectValue) is
                            ScriptedInstance timeSpace)
                            session.Character.EnterInstance(input: timeSpace);
                    break;

                #region Random Boxes

                //Flügelbox Random Drop 5951
                case 5951:
                    var rnd68 = ServerManager.RandomNumber(min: 0, max: 21);
                    if (rnd68 < 21)
                    {
                        short[] vnums =
                        {
                            5560, 5553, 5498, 5499, 5087, 5203, 1685, 1686, 5431, 5432, 5372, 5837, 5702, 5591, 1012,
                            2429, 2427, 5983, 1286, 1296, 2339
                        };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 99, 5, 5, 10, 5, 5, 10 };
                        var item = ServerManager.RandomNumber(min: 0, max: 21);
                        session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                //FeenBox Random Drop 5991
                case 5991:
                    var rnd69 = ServerManager.RandomNumber(min: 0, max: 23);
                    if (rnd69 < 23)
                    {
                        short[] vnums =
                        {
                            800, 801, 802, 803, 274, 275, 276, 277, 278, 279, 280, 281, 4129, 4130, 4131, 4132, 1012,
                            2429, 2427, 5983, 1286, 1296, 2339
                        };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 99, 5, 5, 10, 5, 5, 10 };
                        var item = ServerManager.RandomNumber(min: 0, max: 23);
                        session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                //Waffenskins Random Drop 5957
                case 5957:
                    var rnd70 = ServerManager.RandomNumber(min: 0, max: 22);
                    if (rnd70 < 22)
                    {
                        short[] vnums =
                        {
                            4309, 4310, 4311, 4371, 4372, 4373, 4353, 4354, 4355, 4271, 4273, 4275, 4277, 4279, 4281,
                            1012, 2429, 2427, 5983, 1286, 1296, 2339
                        };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 99, 5, 5, 10, 5, 5, 10 };
                        var item = ServerManager.RandomNumber(min: 0, max: 22);
                        session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                #endregion

                #region Compliment Boxes

                // Box C 1291 = Compliment
                case 1291:
                    var rnd67 = ServerManager.RandomNumber();
                    if (rnd67 < 90)
                    {
                        session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "YOU_ARENT_LUCKY_TODAY"),
                                type: 0)); //must added
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }
                    else
                    {
                        session.Character.Compliment += 5;
                        session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(
                                key: "CONGRATIULATIONS_YOU_GOT_A_COMPLIMENT_POINT"),
                            type: 0)); //must added
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                #endregion

                #region Prestige Item

                case 802:
                    {
                        if (session.Character.Level == 99 &&
                            session.Character.HeroLevel ==
                            50 /*&& session.Character.PrestigeLevel == 10 /*Max Prestige 10*/ &&
                            session.Character.Inventory.All(predicate: i => i.Type != InventoryType.Wear))
                        {
                            session.Character.RemoveBuff(cardId: 131);
                            session.Character.RemoveBuff(cardId: 121);
                            session.Character.Level = 15;
                            session.Character.JobLevel = 1;
                            session.Character.JobLevelXp = 0;
                            session.Character.HeroLevel = 0;
                            session.Character.HeroXp = 0;
                            session.Character.PrestigeLevel += 1;
                            session.Character.GenerateFamilyXp(FXP: 500);
                            session.Character.GiftAdd(itemVNum: 5422, amount: 10);
                            session.Character.GiftAdd(itemVNum: 2427, amount: 1);
                            session.Character.GiftAdd(itemVNum: 2429, amount: 1);
                            session.Character.GiftAdd(itemVNum: 5983, amount: 5);
                            ServerManager.Instance.ChangeMap(id: session.Character.CharacterId, mapId: 9, mapX: 13,
                                mapY: 121);
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                            break;
                        }

                        if (session.Character.Level == 99 && session.Character.HeroLevel == 50)
                        {
                            session.SendPacket(packet: "msg 4 UNWEAR_YOUR_EQUIPMENT");
                            session.Character.RemoveBuff(cardId: 131);
                            session.Character.RemoveBuff(cardId: 121);
                            return;
                        }

                        if (session.Character.Inventory.All(predicate: i => i.Type != InventoryType.Wear))
                            session.SendPacket(packet: "msg 3 YOUR_LEVEL_IS_NOT_HIGH_ENOUGH_LVL_99_HEROLVL_50");
                    }
                    break;

                #endregion

                // SP Potions
                case 150:
                case 151:
                    {
                        if (session.Character.SpAdditionPoint >= 1000000)
                        {
                            session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "SP_POINTS_FULL"), type: 0));
                            break;
                        }

                        session.Character.SpAdditionPoint += EffectValue;

                        if (session.Character.SpAdditionPoint > 1000000) session.Character.SpAdditionPoint = 1000000;

                        session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SP_POINTSADDED"),
                                arg0: EffectValue), type: 0));
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateSpPoint());
                    }
                    break;

                // Specialist Medal
                case 204:
                    {
                        if (session.Character.SpPoint >= 10000
                            && session.Character.SpAdditionPoint >= 1000000)
                        {
                            session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "SP_POINTS_FULL"), type: 0));
                            break;
                        }

                        session.Character.SpPoint += EffectValue;

                        if (session.Character.SpPoint > 10000) session.Character.SpPoint = 10000;

                        session.Character.SpAdditionPoint += EffectValue * 3;

                        if (session.Character.SpAdditionPoint > 1000000) session.Character.SpAdditionPoint = 1000000;

                        session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SP_POINTSADDEDBOTH"),
                                arg0: EffectValue,
                                arg1: EffectValue * 3), type: 0));
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateSpPoint());
                    }
                    break;

                // Raid Seals
                case 301:
                    var raidSeal =
                        session.Character.Inventory.LoadBySlotAndType(slot: inv.Slot, type: InventoryType.Main);

                    if (raidSeal != null)
                    {
                        var raid = ServerManager.Instance.Raids
                            .FirstOrDefault(predicate: s => s.Id == raidSeal.Item.EffectValue)
                            ?.Copy();

                        if (raid != null)
                        {
                            if (ServerManager.Instance.ChannelId == 51 || session.CurrentMapInstance.MapInstanceType !=
                                MapInstanceType.BaseMapInstance) return;

                            if (ServerManager.Instance.IsCharacterMemberOfGroup(
                                characterId: session.Character.CharacterId))
                            {
                                session.SendPacket(
                                    packet: session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "RAID_OPEN_GROUP"),
                                        type: 12));
                                return;
                            }

                            var entries = raid.DailyEntries - session.Character.GeneralLogs.CountLinq(predicate: s =>
                                s.LogType == "InstanceEntry" && short.Parse(s: s.LogData) == raid.Id &&
                                s.Timestamp.Date == DateTime.Today);

                            if (raid.DailyEntries > 0 && entries <= 0)
                            {
                                session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "INSTANCE_NO_MORE_ENTRIES"),
                                    type: 0));
                                session.SendPacket(packet: session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "INSTANCE_NO_MORE_ENTRIES"),
                                    type: 10));
                                return;
                            }

                            if (session.Character.Level > raid.LevelMaximum ||
                                session.Character.Level < raid.LevelMinimum)
                            {
                                session.SendPacket(packet: session.Character.GenerateSay(
                                    message: Language.Instance.GetMessageFromKey(key: "RAID_LEVEL_INCORRECT"),
                                    type: 10));
                                return;
                            }

                            var group = new Group
                            {
                                GroupType = raid.IsGiantTeam ? GroupType.GiantTeam : GroupType.BigTeam,
                                Raid = raid
                            };

                            if (group.JoinGroup(session: session))
                            {
                                ServerManager.Instance.AddGroup(group: group);
                                session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "RAID_LEADER"),
                                        arg0: session.Character.Name), type: 0));
                                session.SendPacket(packet: session.Character.GenerateSay(
                                    message: string.Format(
                                        format: Language.Instance.GetMessageFromKey(key: "RAID_LEADER"),
                                        arg0: session.Character.Name), type: 10));
                                session.SendPacket(packet: session.Character.GenerateRaid(Type: 2));
                                session.SendPacket(packet: session.Character.GenerateRaid(Type: 0));
                                session.SendPacket(packet: session.Character.GenerateRaid(Type: 1));
                                session.SendPacket(packet: group.GenerateRdlst());
                                session.Character.Inventory.RemoveItemFromInventory(id: raidSeal.Id);
                            }
                        }
                    }

                    break;

                // Partner Suits/Skins
                case 305:
                    var mate = session.Character.Mates.Find(match: s =>
                        s.MateTransportId == int.Parse(s: packetsplit[3]));
                    if (mate != null && EffectValue == mate.NpcMonsterVNum && mate.Skin == 0)
                    {
                        mate.Skin = Morph;
                        session.SendPacket(packet: mate.GenerateCMode(morphId: mate.Skin));
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                //suction Funnel (Quest Item / QuestId = 1724)
                case 400:
                    if (session.Character == null ||
                        session.Character.Quests.All(predicate: q => q.QuestId != 1724)) break;
                    if (session.Character.Quests.FirstOrDefault(predicate: q => q.QuestId == 1724) is CharacterQuest
                        kenkoQuest)
                    {
                        var kenko = session.CurrentMapInstance?.Monsters.FirstOrDefault(predicate: m =>
                            m.MapMonsterId == session.Character.LastNpcMonsterId && m.MonsterVNum > 144 &&
                            m.MonsterVNum < 154);
                        if (kenko == null || session.Character.Inventory.CountItem(itemVNum: 1174) > 0) break;
                        if (session.Character.LastFunnelUse.AddSeconds(value: 30) <= DateTime.Now)
                        {
                            if (kenko.CurrentHp / kenko.MaxHp * 100 < 30)
                            {
                                if (ServerManager.RandomNumber() < 30)
                                {
                                    kenko.SetDeathStatement();
                                    session.Character.MapInstance.Broadcast(
                                        packet: StaticPacketHelper.Out(type: UserType.Monster,
                                            callerId: kenko.MapMonsterId));
                                    session.Character.Inventory.AddNewToInventory(vnum: 1174); // Kenko Bead
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "KENKO_CATCHED"),
                                            type: 0));
                                }
                                else
                                {
                                    session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: Language.Instance.GetMessageFromKey(key: "QUEST_CATCH_FAIL"),
                                            type: 0));
                                }

                                session.Character.LastFunnelUse = DateTime.Now;
                            }
                            else
                            {
                                session.SendPacket(
                                    packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "HP_TOO_HIGH"),
                                        type: 0));
                            }
                        }
                    }

                    break;

                // Fairy Booster
                case 250:
                    if (!session.Character.Buff.ContainsKey(key: 131))
                    {
                        session.Character.AddStaticBuff(staticBuff: new StaticBuffDto { CardId = 131 });
                        session.CurrentMapInstance?.Broadcast(packet: session.Character.GeneratePairy());
                        session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "EFFECT_ACTIVATED"),
                                arg0: inv.Item.Name), type: 0));
                        session.CurrentMapInstance?.Broadcast(
                            packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                callerId: session.Character.CharacterId, effectId: 3014),
                            xRangeCoordinate: session.Character.PositionX,
                            yRangeCoordinate: session.Character.PositionY);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }
                    else
                    {
                        session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "ITEM_IN_USE"), type: 0));
                    }

                    break;

                // Rainbow Pearl/Magic Eraser
                case 666:
                    if (EffectValue == 1 && byte.TryParse(s: packetsplit[9], result: out var islot))
                    {
                        var wearInstance =
                            session.Character.Inventory.LoadBySlotAndType(slot: islot, type: InventoryType.Equipment);

                        if (wearInstance != null &&
                            (wearInstance.Item.ItemType == ItemType.Weapon ||
                             wearInstance.Item.ItemType == ItemType.Armor) && wearInstance.ShellEffects.Count != 0 &&
                            !wearInstance.Item.IsHeroic)
                        {
                            wearInstance.ShellEffects.Clear();
                            wearInstance.ShellRarity = null;
                            DaoFactory.ShellEffectDao.DeleteByEquipmentSerialId(id: wearInstance.EquipmentSerialId);
                            if (wearInstance.EquipmentSerialId == Guid.Empty)
                                wearInstance.EquipmentSerialId = Guid.NewGuid();
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                            session.SendPacket(
                                packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "OPTION_DELETE"),
                                    type: 0));
                        }
                    }
                    else
                    {
                        session.SendPacket(packet: "guri 18 0");
                    }

                    break;

                // Atk/Def/HP/Exp potions
                case 6600:
                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    break;

                // Ancelloan's Blessing
                case 208:
                    if (!session.Character.Buff.ContainsKey(key: 121))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.Character.AddStaticBuff(staticBuff: new StaticBuffDto { CardId = 121 });
                    }
                    else
                    {
                        session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "ITEM_IN_USE"), type: 0));
                    }

                    break;

                // Guardian Angel's Blessing
                case 210:
                    if (!session.Character.Buff.ContainsKey(key: 122))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.Character.AddStaticBuff(staticBuff: new StaticBuffDto { CardId = 122 });
                    }
                    else
                    {
                        session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "ITEM_IN_USE"), type: 0));
                    }

                    break;

                case 2081:
                    if (!session.Character.Buff.ContainsKey(key: 146))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.Character.AddStaticBuff(staticBuff: new StaticBuffDto { CardId = 146 });
                    }
                    else
                    {
                        session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "ITEM_IN_USE"), type: 0));
                    }

                    break;

                // Divorce letter
                case 6969:
                    if (session.Character.Group != null)
                    {
                        session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "NOT_ALLOWED_IN_GROUP"),
                                type: 0));
                        return;
                    }

                    var rel = session.Character.CharacterRelations.FirstOrDefault(predicate: s =>
                        s.RelationType == CharacterRelationType.Spouse);
                    if (rel != null)
                    {
                        session.Character.DeleteRelation(
                            characterId: rel.CharacterId == session.Character.CharacterId
                                ? rel.RelatedCharacterId
                                : rel.CharacterId,
                            relationType: CharacterRelationType.Spouse);
                        session.SendPacket(
                            packet: UserInterfaceHelper.GenerateInfo(
                                message: Language.Instance.GetMessageFromKey(key: "DIVORCED")));
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                // Cupid's arrow
                case 34:
                    if (packetsplit != null && packetsplit.Length > 3)
                        if (long.TryParse(s: packetsplit[3], result: out var characterId))
                        {
                            if (session.Character.CharacterId == characterId) return;
                            if (session.Character.CharacterRelations.Any(predicate: s =>
                                s.RelationType == CharacterRelationType.Spouse))
                            {
                                session.SendPacket(
                                    packet: $"info {Language.Instance.GetMessageFromKey(key: "ALREADY_MARRIED")}");
                                return;
                            }

                            if (session.Character.Group != null)
                            {
                                session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "NOT_ALLOWED_IN_GROUP"),
                                    type: 0));
                                return;
                            }

                            if (!session.Character.IsFriendOfCharacter(characterId: characterId))
                            {
                                session.SendPacket(
                                    packet: $"info {Language.Instance.GetMessageFromKey(key: "MUST_BE_FRIENDS")}");
                                return;
                            }

                            var otherSession = ServerManager.Instance.GetSessionByCharacterId(characterId: characterId);
                            if (otherSession != null)
                            {
                                if (otherSession.Character.Group != null)
                                {
                                    session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: Language.Instance.GetMessageFromKey(key: "OTHER_PLAYER_IN_GROUP"),
                                        type: 0));
                                    return;
                                }

                                otherSession.SendPacket(packet: UserInterfaceHelper.GenerateDialog(
                                    dialog:
                                    $"#fins^34^{session.Character.CharacterId} #fins^69^{session.Character.CharacterId} {string.Format(format: Language.Instance.GetMessageFromKey(key: "MARRY_REQUEST"), arg0: session.Character.Name)}"));
                                session.Character.MarryRequestCharacters.Add(value: characterId);
                                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                            }
                        }

                    break;

                case 100: // Miniland Signpost
                    {
                        if (session.Character.BattleEntity.GetOwnedNpcs()
                            .Any(predicate: s => session.Character.BattleEntity.IsSignpost(vnum: s.NpcVNum))) return;
                        if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance &&
                            new short[] { 1, 145 }.Contains(value: session.CurrentMapInstance.Map.MapId))
                        {
                            var signPost = new MapNpc
                            {
                                NpcVNum = (short)EffectValue,
                                MapX = session.Character.PositionX,
                                MapY = session.Character.PositionY,
                                MapId = session.CurrentMapInstance.Map.MapId,
                                ShouldRespawn = false,
                                IsMoving = false,
                                MapNpcId = session.CurrentMapInstance.GetNextNpcId(),
                                Owner = session.Character.BattleEntity,
                                Dialog = 10000,
                                Position = 2,
                                Name = $"{session.Character.Name}'s^[Miniland]"
                            };
                            switch (EffectValue)
                            {
                                case 1428:
                                case 1499:
                                case 1519:
                                    signPost.AliveTime = 3600;
                                    break;
                                default:
                                    signPost.AliveTime = 1800;
                                    break;
                            }

                            signPost.Initialize(currentMapInstance: session.CurrentMapInstance);
                            session.CurrentMapInstance.AddNpc(npc: signPost);
                            session.CurrentMapInstance.Broadcast(packet: signPost.GenerateIn());
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        }
                    }
                    break;

                case 550: // Campfire and other craft npcs
                    {
                        if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                        {
                            short dialog = 10023;
                            switch (EffectValue)
                            {
                                case 956:
                                    dialog = 10023;
                                    break;
                                case 957:
                                    dialog = 10024;
                                    break;
                                case 959:
                                    dialog = 10026;
                                    break;
                            }

                            var campfire = new MapNpc
                            {
                                NpcVNum = (short)EffectValue,
                                MapX = session.Character.PositionX,
                                MapY = session.Character.PositionY,
                                MapId = session.CurrentMapInstance.Map.MapId,
                                ShouldRespawn = false,
                                IsMoving = false,
                                MapNpcId = session.CurrentMapInstance.GetNextNpcId(),
                                Owner = session.Character.BattleEntity,
                                Dialog = dialog,
                                Position = 2
                            };
                            campfire.AliveTime = 180;
                            campfire.Initialize(currentMapInstance: session.CurrentMapInstance);
                            session.CurrentMapInstance.AddNpc(npc: campfire);
                            session.CurrentMapInstance.Broadcast(packet: campfire.GenerateIn());
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        }
                    }
                    break;

                // Faction Egg
                case 570:
                    if (session.Character.Faction == (FactionType)EffectValue) return;
                    if (EffectValue < 3)
                        session.SendPacket(packet: session.Character.Family == null
                            ? $"qna #guri^750^{EffectValue} {Language.Instance.GetMessageFromKey(key: $"ASK_CHANGE_FACTION{EffectValue}")}"
                            : UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "IN_FAMILY"),
                                type: 0));
                    else
                        session.SendPacket(packet: session.Character.Family != null
                            ? $"qna #guri^750^{EffectValue} {Language.Instance.GetMessageFromKey(key: $"ASK_CHANGE_FACTION{EffectValue}")}"
                            : UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "NO_FAMILY"),
                                type: 0));

                    break;

                case 13: //Handkerchief of Separation
                    int CharacterId;
                    if (int.TryParse(s: packetsplit[3], result: out CharacterId) && EffectValue == 1)
                    {
                        if (CharacterId == session.Character.CharacterId)
                            session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: 10, argument: 1,
                                callerId: CharacterId, value: 1001));
                        else
                            session.SendPacket(packet: session.Character.GenerateSay(
                                message: "Please, untarget/unselect your pet, npc or player you're targeting.",
                                type: 10));
                    }

                    break;

                // SP Wings
                case 650:
                    var specialistInstance =
                        session.Character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                            type: InventoryType.Wear);
                    if (session.Character.UseSp && specialistInstance != null && !session.Character.IsSeal)
                    {
                        if (Option == 0)
                        {
                            session.SendPacket(
                                packet:
                                $"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey(key: "ASK_WINGS_CHANGE")}");
                        }
                        else
                        {
                            void disposeBuff(short vNum)
                            {
                                if (session.Character.BuffObservables.ContainsKey(key: vNum))
                                {
                                    session.Character.BuffObservables[key: vNum].Dispose();
                                    session.Character.BuffObservables.Remove(key: vNum);
                                }

                                session.Character.RemoveBuff(cardId: vNum);
                            }

                            disposeBuff(vNum: 387);
                            disposeBuff(vNum: 395);
                            disposeBuff(vNum: 396);
                            disposeBuff(vNum: 397);
                            disposeBuff(vNum: 398);
                            disposeBuff(vNum: 410);
                            disposeBuff(vNum: 411);
                            disposeBuff(vNum: 444);
                            disposeBuff(vNum: 663);
                            disposeBuff(vNum: 686);

                            specialistInstance.Design = (byte)EffectValue;

                            session.Character.MorphUpgrade2 = EffectValue;
                            session.CurrentMapInstance?.Broadcast(packet: session.Character.GenerateCMode());
                            session.SendPacket(packet: session.Character.GenerateStat());
                            session.SendPackets(packets: session.Character.GenerateStatChar());
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "NO_SP"),
                            type: 0));
                    }

                    break;

                // Self-Introduction
                case 203:
                    if (!session.Character.IsVehicled && Option == 0)
                        session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: 10, argument: 2,
                            callerId: session.Character.CharacterId, value: 1));
                    break;

                // Magic Lamp
                case 651:
                    if (session.Character.Inventory.All(predicate: i => i.Type != InventoryType.Wear))
                    {
                        if (Option == 0)
                        {
                            session.SendPacket(
                                packet:
                                $"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey(key: "ASK_USE")}");
                        }
                        else
                        {
                            session.Character.ChangeSex();
                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(
                            packet: UserInterfaceHelper.GenerateMsg(
                                message: Language.Instance.GetMessageFromKey(key: "EQ_NOT_EMPTY"), type: 0));
                    }

                    break;

                // Vehicles
                case 1000:
                    if (EffectValue != 0
                        || session.CurrentMapInstance?.MapInstanceType == MapInstanceType.EventGameInstance
                        || session.CurrentMapInstance?.MapInstanceType == MapInstanceType.TalentArenaMapInstance
                        || session.CurrentMapInstance?.MapInstanceType == MapInstanceType.IceBreakerInstance
                        || session.Character.IsSeal || session.Character.IsMorphed)
                        return;
                    var morph = Morph;
                    var speed = Speed;
                    if (Morph < 0)
                        switch (VNum)
                        {
                            case 5923:
                                morph = 2513;
                                speed = 14;
                                break;
                        }

                    if (morph > 0)
                    {
                        if (Option == 0 && !session.Character.IsVehicled)
                        {
                            if (session.Character.Buff.Any(predicate: s => s.Card.BuffType == BuffType.Bad))
                            {
                                session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                    message: Language.Instance.GetMessageFromKey(key: "CANT_TRASFORM_WITH_DEBUFFS"),
                                    type: 0));
                                return;
                            }

                            if (session.Character.IsSitting)
                            {
                                session.Character.IsSitting = false;
                                session.CurrentMapInstance?.Broadcast(packet: session.Character.GenerateRest());
                            }

                            session.Character.LastDelay = DateTime.Now;
                            session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 3000, type: 3,
                                argument: $"#u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2"));
                        }
                        else
                        {
                            if (!session.Character.IsVehicled && Option != 0)
                            {
                                var delay = DateTime.Now.AddSeconds(value: -4);
                                if (session.Character.LastDelay > delay &&
                                    session.Character.LastDelay < delay.AddSeconds(value: 2))
                                {
                                    session.Character.IsVehicled = true;
                                    session.Character.VehicleSpeed = speed;
                                    session.Character.VehicleItem = this;
                                    session.Character.LoadSpeed();
                                    session.Character.MorphUpgrade = 0;
                                    session.Character.MorphUpgrade2 = 0;
                                    session.Character.Morph = morph + (byte)session.Character.Gender;
                                    session.CurrentMapInstance?.Broadcast(
                                        packet: StaticPacketHelper.GenerateEff(effectType: UserType.Player,
                                            callerId: session.Character.CharacterId,
                                            effectId: 196), xRangeCoordinate: session.Character.PositionX,
                                        yRangeCoordinate: session.Character.PositionY);
                                    session.CurrentMapInstance?.Broadcast(packet: session.Character.GenerateCMode());
                                    session.SendPacket(packet: session.Character.GenerateCond());
                                    session.Character.LastSpeedChange = DateTime.Now;
                                    session.Character.Mates.Where(predicate: s => s.IsTeamMember).ToList()
                                        .ForEach(action: s =>
                                            session.CurrentMapInstance?.Broadcast(packet: s.GenerateOut()));
                                    if (Morph < 0) session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                }
                            }
                            else if (session.Character.IsVehicled)
                            {
                                session.Character.RemoveVehicle();
                                foreach (var teamMate in session.Character.Mates.Where(predicate: m => m.IsTeamMember))
                                {
                                    teamMate.PositionX =
                                        (short)(session.Character.PositionX +
                                                 (teamMate.MateType == MateType.Partner ? -1 : 1));
                                    teamMate.PositionY = (short)(session.Character.PositionY + 1);
                                    if (session.Character.MapInstance.Map.IsBlockedZone(x: teamMate.PositionX,
                                        y: teamMate.PositionY))
                                    {
                                        teamMate.PositionX = session.Character.PositionX;
                                        teamMate.PositionY = session.Character.PositionY;
                                    }

                                    teamMate.UpdateBushFire();
                                    Parallel.ForEach(
                                        source: session.CurrentMapInstance.Sessions.Where(predicate: s =>
                                            s.Character != null), body: s =>
                                        {
                                            if (ServerManager.Instance.ChannelId != 51 ||
                                                session.Character.Faction == s.Character.Faction)
                                                s.SendPacket(packet: teamMate.GenerateIn(hideNickname: false,
                                                    isAct4: ServerManager.Instance.ChannelId == 51));
                                            else
                                                s.SendPacket(packet: teamMate.GenerateIn(hideNickname: true,
                                                    isAct4: ServerManager.Instance.ChannelId == 51,
                                                    receiverAuthority: s.Account.Authority));
                                        });
                                }

                                session.SendPacket(packet: session.Character.GeneratePinit());
                                session.Character.Mates.ForEach(action: s =>
                                    session.SendPacket(packet: s.GenerateScPacket()));
                                session.SendPackets(packets: session.Character.GeneratePst());
                            }
                        }
                    }

                    break;

                // Sealed Vessel
                case 1002:
                    int type, secondaryType, inventoryType, slot;
                    if (packetsplit != null && int.TryParse(s: packetsplit[2], result: out type) &&
                        int.TryParse(s: packetsplit[3], result: out secondaryType) &&
                        int.TryParse(s: packetsplit[4], result: out inventoryType) &&
                        int.TryParse(s: packetsplit[5], result: out slot))
                    {
                        int packetType;
                        switch (EffectValue)
                        {
                            case 69:
                                if (int.TryParse(s: packetsplit[6], result: out packetType))
                                    switch (packetType)
                                    {
                                        case 0:
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000,
                                                type: 7,
                                                argument: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^1"));
                                            break;
                                        case 1:
                                            var rnd = ServerManager.RandomNumber(min: 0, max: 1000);
                                            if (rnd < 5)
                                            {
                                                short[] vnums =
                                                {
                                                    5560, 5591, 4099, 907, 1160, 4705, 4706, 4707, 4708, 4709, 4710,
                                                    4711, 4712, 4713, 4714,
                                                    4715, 4716
                                                };
                                                byte[] counts = { 1, 1, 1, 1, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                                                var item = ServerManager.RandomNumber(min: 0, max: 17);
                                                session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                                            }
                                            else if (rnd < 30)
                                            {
                                                short[] vnums = { 361, 362, 363, 366, 367, 368, 371, 372, 373 };
                                                session.Character.GiftAdd(
                                                    itemVNum: vnums[ServerManager.RandomNumber(min: 0, max: 9)],
                                                    amount: 1);
                                            }
                                            else
                                            {
                                                short[] vnums =
                                                {
                                                    1161, 2282, 1030, 1244, 1218, 5369, 1012, 1363, 1364, 2160, 2173,
                                                    5959, 5983, 2514,
                                                    2515, 2516, 2517, 2518, 2519, 2520, 2521, 1685, 1686, 5087, 5203,
                                                    2418, 2310, 2303,
                                                    2169, 2280, 5892, 5893, 5894, 5895, 5896, 5897, 5898, 5899, 5332,
                                                    5105, 2161, 2162
                                                };
                                                byte[] counts =
                                                {
                                                    10, 10, 20, 5, 1, 1, 99, 1, 1, 5, 5, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                                                    1, 1, 1, 1, 5, 20,
                                                    20, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
                                                };
                                                var item = ServerManager.RandomNumber(min: 0, max: 42);
                                                session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                                            }

                                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                            break;
                                    }

                                break;
                            default:
                                if (int.TryParse(s: packetsplit[6], result: out packetType))
                                {
                                    if (session.Character.MapInstance.Map.MapTypes.Any(predicate: s =>
                                        s.MapTypeId == (short)MapTypeEnum.Act4)) return;

                                    if (session.Account.IsLimited)
                                    {
                                        session.SendPacket(
                                            packet: UserInterfaceHelper.GenerateInfo(
                                                message: Language.Instance.GetMessageFromKey(key: "LIMITED_ACCOUNT")));
                                        return;
                                    }

                                    switch (packetType)
                                    {
                                        case 0:
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 5000,
                                                type: 7,
                                                argument: $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^1"));
                                            break;

                                        case 1:
                                            if (session.HasCurrentMapInstance &&
                                                (session.Character.MapInstance == session.Character.Miniland ||
                                                 session.CurrentMapInstance.MapInstanceType ==
                                                 MapInstanceType.BaseMapInstance) &&
                                                (session.Character.LastVessel.AddSeconds(value: 1) <= DateTime.Now ||
                                                 session.Character.StaticBonusList.Any(predicate: s =>
                                                     s.StaticBonusType == StaticBonusType.FastVessels)))
                                            {
                                                short[] vnums =
                                                {
                                                    1386, 1387, 1388, 1389, 1390, 1391, 1392, 1393, 1394, 1395, 1396,
                                                    1397, 1398, 1399, 1400, 1401, 1402, 1403, 1404, 1405
                                                };
                                                var vnum = vnums[ServerManager.RandomNumber(min: 0, max: 20)];

                                                var npcmonster = ServerManager.GetNpcMonster(npcVNum: vnum);
                                                if (npcmonster == null) return;
                                                var monster = new MapMonster
                                                {
                                                    MonsterVNum = vnum,
                                                    MapX = session.Character.PositionX,
                                                    MapY = session.Character.PositionY,
                                                    MapId = session.Character.MapInstance.Map.MapId,
                                                    Position = session.Character.Direction,
                                                    IsMoving = true,
                                                    MapMonsterId = session.CurrentMapInstance.GetNextMonsterId(),
                                                    ShouldRespawn = false
                                                };
                                                monster.Initialize(currentMapInstance: session.CurrentMapInstance);
                                                session.CurrentMapInstance.AddMonster(monster: monster);
                                                session.CurrentMapInstance.Broadcast(packet: monster.GenerateIn());
                                                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                                session.Character.LastVessel = DateTime.Now;
                                            }

                                            break;
                                    }
                                }

                                break;
                        }
                    }

                    break;

                // Golden Bazaar Medal
                case 1003:
                    if (!session.Character.StaticBonusList.Any(predicate: s =>
                        s.StaticBonusType == StaticBonusType.BazaarMedalGold ||
                        s.StaticBonusType == StaticBonusType.BazaarMedalSilver))
                    {
                        session.Character.StaticBonusList.Add(item: new StaticBonusDto
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(value: EffectValue),
                            StaticBonusType = StaticBonusType.BazaarMedalGold
                        });
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "EFFECT_ACTIVATED"),
                                arg0: Name), type: 12));
                    }

                    break;

                // Silver Bazaar Medal
                case 1004:
                    if (!session.Character.StaticBonusList.Any(predicate: s =>
                        s.StaticBonusType == StaticBonusType.BazaarMedalGold ||
                        s.StaticBonusType == StaticBonusType.BazaarMedalGold))
                    {
                        session.Character.StaticBonusList.Add(item: new StaticBonusDto
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(value: EffectValue),
                            StaticBonusType = StaticBonusType.BazaarMedalSilver
                        });
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "EFFECT_ACTIVATED"),
                                arg0: Name), type: 12));
                    }

                    break;

                // Pet Slot Expansion
                case 1006:
                    if (Option == 0)
                    {
                        session.SendPacket(
                            packet:
                            $"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2 {Language.Instance.GetMessageFromKey(key: "ASK_PET_MAX")}");
                    }
                    else if (inv.Item?.IsSoldable == true && session.Character.MaxMateCount < 90 ||
                             session.Character.MaxMateCount < 30)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.Character.MaxMateCount += 10;
                        session.SendPacket(
                            packet: session.Character.GenerateSay(
                                message: Language.Instance.GetMessageFromKey(key: "GET_PET_PLACES"), type: 10));
                        session.SendPacket(packet: session.Character.GenerateScpStc());
                    }

                    break;

                // Permanent Backpack Expansion
                case 601:
                    if (session.Character.StaticBonusList.All(predicate: s =>
                        s.StaticBonusType != StaticBonusType.BackPack))
                    {
                        session.Character.StaticBonusList.Add(item: new StaticBonusDto
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddYears(value: 15),
                            StaticBonusType = StaticBonusType.BackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateExts());
                        session.SendPacket(packet: session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "EFFECT_ACTIVATED"),
                                arg0: Name), type: 12));
                    }

                    break;

                // Permanent Partner's Backpack
                case 602:
                    if (session.Character.StaticBonusList.All(predicate: s =>
                        s.StaticBonusType != StaticBonusType.PetBackPack))
                    {
                        session.Character.StaticBonusList.Add(item: new StaticBonusDto
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddYears(value: 15),
                            StaticBonusType = StaticBonusType.PetBackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateExts());
                        session.SendPacket(packet: session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "EFFECT_ACTIVATED"),
                                arg0: Name), type: 12));
                    }

                    break;

                // Permanent Pet Basket
                case 603:
                    if (session.Character.StaticBonusList.All(predicate: s =>
                        s.StaticBonusType != StaticBonusType.PetBasket))
                    {
                        session.Character.StaticBonusList.Add(item: new StaticBonusDto
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddYears(value: 15),
                            StaticBonusType = StaticBonusType.PetBasket
                        });
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateExts());
                        session.SendPacket(packet: "ib 1278 1");
                        session.SendPacket(packet: session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "EFFECT_ACTIVATED"),
                                arg0: Name), type: 12));
                    }

                    break;

                // Pet Basket
                case 1007:
                    if (session.Character.StaticBonusList.All(predicate: s =>
                        s.StaticBonusType != StaticBonusType.PetBasket))
                    {
                        session.Character.StaticBonusList.Add(item: new StaticBonusDto
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(value: EffectValue),
                            StaticBonusType = StaticBonusType.PetBasket
                        });
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateExts());
                        session.SendPacket(packet: "ib 1278 1");
                        session.SendPacket(packet: session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "EFFECT_ACTIVATED"),
                                arg0: Name), type: 12));
                    }

                    break;

                // Partner's Backpack
                case 1008:
                    if (session.Character.StaticBonusList.All(predicate: s =>
                        s.StaticBonusType != StaticBonusType.PetBackPack))
                    {
                        session.Character.StaticBonusList.Add(item: new StaticBonusDto
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(value: EffectValue),
                            StaticBonusType = StaticBonusType.PetBackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateExts());
                        session.SendPacket(packet: session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "EFFECT_ACTIVATED"),
                                arg0: Name), type: 12));
                    }

                    break;

                // Backpack Expansion
                case 1009:
                    if (session.Character.StaticBonusList.All(predicate: s =>
                        s.StaticBonusType != StaticBonusType.BackPack))
                    {
                        session.Character.StaticBonusList.Add(item: new StaticBonusDto
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(value: EffectValue),
                            StaticBonusType = StaticBonusType.BackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                        session.SendPacket(packet: session.Character.GenerateExts());
                        session.SendPacket(packet: session.Character.GenerateSay(
                            message: string.Format(format: Language.Instance.GetMessageFromKey(key: "EFFECT_ACTIVATED"),
                                arg0: Name), type: 12));
                    }

                    break;

                // Sealed Tarot Card
                case 1005:
                    session.Character.GiftAdd(itemVNum: (short)(VNum - Effect), amount: 1);
                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    break;

                // Tarot Card Game
                case 1894:
                    if (EffectValue == 0)
                    {
                        for (var i = 0; i < 5; i++)
                            session.Character.GiftAdd(
                                itemVNum: (short)(Effect + ServerManager.RandomNumber(min: 0, max: 10)), amount: 1);
                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    }

                    break;

                // Sealed Tarot Card
                case 2152:
                    session.Character.GiftAdd(itemVNum: (short)(VNum + Effect), amount: 1);
                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                    break;

                // Transformation scrolls
                case 1001:
                    if (session.Character.IsMorphed)
                    {
                        session.Character.IsMorphed = false;
                        session.Character.Morph = 0;
                        session.CurrentMapInstance?.Broadcast(packet: session.Character.GenerateCMode());
                    }
                    else if (!session.Character.UseSp && !session.Character.IsVehicled)
                    {
                        if (Option == 0)
                        {
                            session.Character.LastDelay = DateTime.Now;
                            session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 3000, type: 3,
                                argument: $"#u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^1"));
                        }
                        else
                        {
                            int[] possibleTransforms = null;

                            switch (EffectValue)
                            {
                                case 1: // Halloween
                                    possibleTransforms = new[]
                                    {
                                        404, //Torturador pellizcador
                                        405, //Torturador enrollador
                                        406, //Torturador de acero
                                        446, //Guerrero yak
                                        447, //Mago yak
                                        441, //Guerrero de la muerte
                                        276, //Rey polvareda
                                        324, //Princesa Catrisha
                                        248, //Bruja oscura
                                        249, //Bruja de sangre
                                        438, //Bruja blanca fuerte
                                        236, //Guerrero esqueleto
                                        245, //Sombra nocturna
                                        439, //Guerrero esqueleto resucitado
                                        272, //Arquero calavera
                                        274, //Guerrero calavera
                                        2691 //Frankenstein
                                    };
                                    break;

                                case 2: // Ice Costume
                                    break;

                                case 3: // Bushtail Costume
                                    break;
                            }

                            if (possibleTransforms != null)
                            {
                                session.Character.IsMorphed = true;
                                session.Character.Morph =
                                    1000 + possibleTransforms[
                                        ServerManager.RandomNumber(min: 0, max: possibleTransforms.Length)];
                                session.CurrentMapInstance?.Broadcast(packet: session.Character.GenerateCMode());
                                if (VNum != 1914) session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                            }
                        }
                    }

                    break;


                default:
                    switch (VNum)
                    {
                        case 5856: // Partner Slot Expansion
                        case 9113: // Partner Slot Expansion (Limited)
                            {
                                if (Option == 0)
                                {
                                    session.SendPacket(
                                        packet:
                                        $"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2 {Language.Instance.GetMessageFromKey(key: "ASK_PARTNER_MAX")}");
                                }
                                else if (session.Character.MaxPartnerCount < 12)
                                {
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    session.Character.MaxPartnerCount++;
                                    session.SendPacket(packet: session.Character.GenerateSay(
                                        message: Language.Instance.GetMessageFromKey(key: "GET_PARTNER_PLACES"), type: 10));
                                    session.SendPacket(packet: session.Character.GenerateScpStc());
                                }
                            }
                            break;

                        case 5931: // Tique de habilidad de compañero (una)
                            {
                                if (session?.Character?.Mates == null) return;

                                if (packetsplit.Length != 10 || !byte.TryParse(s: packetsplit[8], result: out var petId) ||
                                    !byte.TryParse(s: packetsplit[9], result: out var castId)) return;

                                if (castId < 0 || castId > 2) return;

                                var partner = session.Character.Mates.ToList().FirstOrDefault(predicate: s =>
                                    s.IsTeamMember && s.MateType == MateType.Partner && s.PetId == petId);

                                if (partner?.Sp == null || partner.IsUsingSp) return;

                                var skill = partner.Sp.GetSkill(castId: castId);

                                if (skill?.Skill == null) return;

                                if (skill.Level == (byte)PartnerSkillLevelType.S) return;

                                if (partner.Sp.RemoveSkill(castId: castId))
                                {
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);

                                    partner.Sp.ReloadSkills();
                                    partner.Sp.FullXp();

                                    session.SendPacket(packet: UserInterfaceHelper.GenerateModal(
                                        message: Language.Instance.GetMessageFromKey(key: "PSP_SKILL_RESETTED"), type: 1));
                                }

                                session.SendPacket(packet: partner.GenerateScPacket());
                            }
                            break;

                        case 5932: // Tique de habilidad de compañero (todas)
                            {
                                if (packetsplit.Length != 10
                                    || session?.Character?.Mates == null)
                                    return;

                                if (!byte.TryParse(s: packetsplit[8], result: out var petId) ||
                                    !byte.TryParse(s: packetsplit[9], result: out var castId)) return;

                                if (castId < 0 || castId > 2) return;

                                var partner = session.Character.Mates.ToList().FirstOrDefault(predicate: s =>
                                    s.IsTeamMember && s.MateType == MateType.Partner && s.PetId == petId);

                                if (partner?.Sp == null || partner.IsUsingSp) return;

                                if (partner.Sp.GetSkillsCount() < 1) return;

                                session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);

                                partner.Sp.ClearSkills();
                                partner.Sp.FullXp();

                                session.SendPacket(packet: UserInterfaceHelper.GenerateModal(
                                    message: Language.Instance.GetMessageFromKey(key: "PSP_ALL_SKILLS_RESETTED"), type: 1));

                                session.SendPacket(packet: partner.GenerateScPacket());
                            }
                            break;

                        // Event Upgrade Scrolls
                        case 5107:
                        case 5207:
                        case 5519:
                            if (EffectValue != 0)
                            {
                                if (session.Character.IsSitting)
                                {
                                    session.Character.IsSitting = false;
                                    session.SendPacket(packet: session.Character.GenerateRest());
                                }

                                session.SendPacket(packet: UserInterfaceHelper.GenerateGuri(type: 12, argument: 1,
                                    callerId: session.Character.CharacterId, value: EffectValue));
                            }

                            break;

                        default:
                            switch (EffectValue)
                            {
                                // Angel Base Flag
                                case 965:
                                // Demon Base Flag
                                case 966:
                                    if (ServerManager.Instance.ChannelId == 51 &&
                                        session.CurrentMapInstance?.Map.MapId != 130 &&
                                        session.CurrentMapInstance?.Map.MapId != 131 &&
                                        EffectValue - 964 == (short)session.Character.Faction)
                                    {
                                        session.CurrentMapInstance?.SummonMonster(
                                            summon: new MonsterToSummon(vnum: (short)EffectValue,
                                                spawnCell: new MapCell
                                                {
                                                    X = session.Character.PositionX,
                                                    Y = session.Character.PositionY
                                                }, target: null, move: false, isHostile: false, aliveTime: 1800));
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    }

                                    break;

                                case 550: // Campfire and other craft npcs
                                    {
                                        if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                                        {
                                            short dialog = 10023;
                                            switch (EffectValue)
                                            {
                                                case 956:
                                                    dialog = 10023;
                                                    break;
                                                case 957:
                                                    dialog = 10024;
                                                    break;
                                                case 959:
                                                    dialog = 10026;
                                                    break;
                                            }

                                            var campfire = new MapNpc
                                            {
                                                NpcVNum = (short)EffectValue,
                                                MapX = session.Character.PositionX,
                                                MapY = session.Character.PositionY,
                                                MapId = session.CurrentMapInstance.Map.MapId,
                                                ShouldRespawn = false,
                                                IsMoving = false,
                                                MapNpcId = session.CurrentMapInstance.GetNextNpcId(),
                                                Owner = session.Character.BattleEntity,
                                                Dialog = dialog,
                                                Position = 2
                                            };
                                            campfire.AliveTime = 180;
                                            campfire.Initialize(currentMapInstance: session.CurrentMapInstance);
                                            session.CurrentMapInstance.AddNpc(npc: campfire);
                                            session.CurrentMapInstance.Broadcast(packet: campfire.GenerateIn());
                                            session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                        }
                                    }
                                    break;


                                //Halloween Scroll
                                case 1001:
                                    if (session.Character.IsMorphed)
                                    {
                                        session.Character.IsMorphed = false;
                                        session.Character.Morph = 0;
                                        session.CurrentMapInstance?.Broadcast(
                                            packet: session.Character.GenerateCMode());
                                    }
                                    else if (!session.Character.UseSp && !session.Character.IsVehicled)
                                    {
                                        if (Option == 0)
                                        {
                                            session.Character.LastDelay = DateTime.Now;
                                            session.SendPacket(packet: UserInterfaceHelper.GenerateDelay(delay: 3000,
                                                type: 3,
                                                argument:
                                                $"#u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^1"));
                                        }
                                        else
                                        {
                                            int[] possibleTransforms = null;

                                            switch (EffectValue)
                                            {
                                                case 1: // Halloween
                                                    possibleTransforms = new[]
                                                    {
                                                        404,
                                                        405,
                                                        406,
                                                        446,
                                                        447,
                                                        441,
                                                        276,
                                                        324,
                                                        248,
                                                        249,
                                                        438,
                                                        236,
                                                        245,
                                                        439,
                                                        272,
                                                        274,
                                                        2691
                                                    };
                                                    break;

                                                case 2: // Ice Costume
                                                    break;

                                                case 3: // Bushtail Costume
                                                    break;
                                            }

                                            if (possibleTransforms != null)
                                            {
                                                session.Character.IsMorphed = true;
                                                session.Character.Morph =
                                                    1000 + possibleTransforms[
                                                        ServerManager.RandomNumber(min: 0,
                                                            max: possibleTransforms.Length)];
                                                session.CurrentMapInstance?.Broadcast(packet: session.Character
                                                    .GenerateCMode());
                                                if (VNum != 1914)
                                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                            }
                                        }
                                    }

                                    break;

                                // Martial Artist Starter Pack
                                case 5832:
                                    {
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);

                                        // Steel Fist
                                        session.Character.GiftAdd(itemVNum: 4756, amount: 1, rare: 5);

                                        // Trainee Martial Artist's Uniform
                                        session.Character.GiftAdd(itemVNum: 4757, amount: 1, rare: 5);

                                        // Mystical Glacier Stone
                                        session.Character.GiftAdd(itemVNum: 4504, amount: 1);

                                        // Hero's Amulet of Fire
                                        session.Character.GiftAdd(itemVNum: 4503, amount: 1);

                                        // Fairy Fire/Water/Light/Dark (30%)
                                        for (short itemVNum = 884; itemVNum <= 887; itemVNum++)
                                            session.Character.GiftAdd(itemVNum: itemVNum, amount: 1);
                                    }
                                    break;

                                // Soulstone Blessing
                                case 1362:
                                case 5195:
                                case 5211:
                                case 9075:
                                    if (!session.Character.Buff.ContainsKey(key: 146))
                                    {
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                        session.Character.AddStaticBuff(staticBuff: new StaticBuffDto { CardId = 146 });
                                    }
                                    else
                                    {
                                        session.SendPacket(
                                            packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "ITEM_IN_USE"),
                                                type: 0));
                                    }

                                    break;
                                case 1428:
                                    session.SendPacket(packet: "guri 18 1");
                                    break;
                                case 1429:
                                    session.SendPacket(packet: "guri 18 0");
                                    break;
                                case 1904:
                                    short[] items = { 1894, 1895, 1896, 1897, 1898, 1899, 1900, 1901, 1902, 1903 };
                                    for (var i = 0; i < 5; i++)
                                        session.Character.GiftAdd(
                                            itemVNum: items[ServerManager.RandomNumber(min: 0, max: items.Length)],
                                            amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;
                                case 5370:
                                    if (session.Character.Buff.Any(predicate: s => s.Card.CardId == 393))
                                    {
                                        session.SendPacket(packet: session.Character.GenerateSay(
                                            message: string.Format(
                                                format: Language.Instance.GetMessageFromKey(key: "ALREADY_GOT_BUFF"),
                                                arg0: session.Character.Buff
                                                    .FirstOrDefault(predicate: s => s.Card.CardId == 393)?.Card
                                                    .Name), type: 10));
                                        return;
                                    }

                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    session.Character.AddStaticBuff(staticBuff: new StaticBuffDto { CardId = 393 });
                                    break;
                                case 5841:
                                    var rnd = ServerManager.RandomNumber(min: 0, max: 1000);
                                    short[] vnums = null;
                                    if (rnd < 900)
                                        vnums = new short[] { 4356, 4357, 4358, 4359 };
                                    else
                                        vnums = new short[] { 4360, 4361, 4362, 4363 };
                                    session.Character.GiftAdd(
                                        itemVNum: vnums[ServerManager.RandomNumber(min: 0, max: 4)], amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;
                                case 5916:
                                case 5927:
                                    session.Character.AddStaticBuff(staticBuff: new StaticBuffDto
                                    {
                                        CardId = 340,
                                        CharacterId = session.Character.CharacterId,
                                        RemainingTime = 7200
                                    });
                                    session.Character.RemoveBuff(cardId: 339);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;
                                case 5929:
                                case 5930:
                                    session.Character.AddStaticBuff(staticBuff: new StaticBuffDto
                                    {
                                        CardId = 340,
                                        CharacterId = session.Character.CharacterId,
                                        RemainingTime = 600
                                    });
                                    session.Character.RemoveBuff(cardId: 339);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                case 5744: // Magic Donkey Pinata Box
                                    {
                                        session.Character.GiftAdd(itemVNum: 5743, amount: 1);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                        break;
                                    }

                                case 5018: // Wedding Box
                                    {
                                        session.Character.GiftAdd(itemVNum: 1981, amount: 1);
                                        session.Character.GiftAdd(itemVNum: 982, amount: 1);
                                        session.Character.GiftAdd(itemVNum: 982, amount: 1);
                                        session.Character.GiftAdd(itemVNum: 984, amount: 1);
                                        session.Character.GiftAdd(itemVNum: 984, amount: 1);
                                        session.Character.GiftAdd(itemVNum: 1984, amount: 10);
                                        session.Character.GiftAdd(itemVNum: 1986, amount: 10);
                                        session.Character.GiftAdd(itemVNum: 1988, amount: 10);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                        break;
                                    }

                                case 5835: // Magic Jaguar Box
                                    {
                                        session.Character.GiftAdd(itemVNum: 5834, amount: 1);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                        break;
                                    }
                                //Otofuchskostümset = 5302
                                case 5302:
                                    session.Character.GiftAdd(itemVNum: 4177, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4179, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Schneeweißestigerkostümset = 5487
                                case 5487:
                                    session.Character.GiftAdd(itemVNum: 4248, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4256, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Fußballkostümset = 5441
                                case 5441:
                                    session.Character.GiftAdd(itemVNum: 4196, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4195, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Zauberset des Lichts = 5358
                                case 5358:
                                    session.Character.GiftAdd(itemVNum: 4181, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4185, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Zauberset des Schattens = 5359
                                case 5359:
                                    session.Character.GiftAdd(itemVNum: 4183, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4187, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //IllusionistenKostümset = 5572
                                case 5572:
                                    session.Character.GiftAdd(itemVNum: 4258, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4260, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Wüstenset = 5638
                                case 5638:
                                    session.Character.GiftAdd(itemVNum: 4317, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4321, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Tänzerin-Set = 5639
                                case 5639:
                                    session.Character.GiftAdd(itemVNum: 4323, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4319, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Portier-Set = 5604
                                case 5604:
                                    session.Character.GiftAdd(itemVNum: 4287, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4289, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //AquaBuschikostümset = 5052
                                case 5052:
                                    session.Character.GiftAdd(itemVNum: 4214, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4211, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Niedliches Tigerkostümset = 5486
                                case 5486:
                                    session.Character.GiftAdd(itemVNum: 4244, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4252, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Feuerteufel Kostümset = 5716
                                case 5716:
                                    session.Character.GiftAdd(itemVNum: 4409, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4411, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Rosarotes Partyset = 5412
                                case 5412:
                                    session.Character.GiftAdd(itemVNum: 4219, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4225, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Gelbes Partyset = 5413
                                case 5413:
                                    session.Character.GiftAdd(itemVNum: 4220, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4226, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Blaues Partyset = 5414
                                case 5414:
                                    session.Character.GiftAdd(itemVNum: 4221, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4227, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Frostkrieger Set = 5715
                                case 5715:
                                    session.Character.GiftAdd(itemVNum: 4382, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4384, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Rotes Detektiv Set = 5415
                                case 5415:
                                    session.Character.GiftAdd(itemVNum: 4231, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4237, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Goldenes Detektiv Set = 5416
                                case 5416:
                                    session.Character.GiftAdd(itemVNum: 4232, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4238, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Braunes Detektiv Set = 5417
                                case 5417:
                                    session.Character.GiftAdd(itemVNum: 4233, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4239, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Nussknackerset = 5878
                                case 5878:
                                    session.Character.GiftAdd(itemVNum: 4827, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4829, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Cooles Strandkostümset = 5592
                                case 5592:
                                    session.Character.GiftAdd(itemVNum: 4266, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4268, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Polizeiuniformset = 5599
                                case 5599:
                                    session.Character.GiftAdd(itemVNum: 4283, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4285, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Wikinger-Kostümset = 5610
                                case 5610:
                                    session.Character.GiftAdd(itemVNum: 4301, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4303, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Flauschiges Hasenkostümset für Männer = 5267
                                case 5267:
                                    session.Character.GiftAdd(itemVNum: 4138, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4146, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Flauschiges Hasenkostümset für Frauen = 5268
                                case 5268:
                                    session.Character.GiftAdd(itemVNum: 4142, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4150, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Schwarzes Buschi-Kostümset = 5183
                                case 5183:
                                    session.Character.GiftAdd(itemVNum: 4107, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4114, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Blaues Buschi-Kostümset = 5184
                                case 5184:
                                    session.Character.GiftAdd(itemVNum: 4108, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4115, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Grünes Buschi-Kostümset = 5185
                                case 5185:
                                    session.Character.GiftAdd(itemVNum: 4109, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4116, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Rotes Buschi-Kostümset = 5186
                                case 5186:
                                    session.Character.GiftAdd(itemVNum: 4110, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4117, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Pinkfarbenes Buschi-Kostümset = 5187
                                case 5187:
                                    session.Character.GiftAdd(itemVNum: 4111, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4118, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Türkisfarbenes Buschi-Kostümset = 5188
                                case 5188:
                                    session.Character.GiftAdd(itemVNum: 4112, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4119, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Gelbes Buschi-Kostümset = 5189
                                case 5189:
                                    session.Character.GiftAdd(itemVNum: 4113, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4120, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Klassisches Buschi-Kostümset = 5190
                                case 5190:
                                    session.Character.GiftAdd(itemVNum: 970, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 972, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                //Santa Buschi-Kostümset = 5080
                                case 5080:
                                    session.Character.GiftAdd(itemVNum: 4073, amount: 1);
                                    session.Character.GiftAdd(itemVNum: 4074, amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                case 5958: //cali box
                                    {
                                        vnums = null;
                                        vnums = new short[]
                                        {
                                        2282, 1030, 1296, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576,
                                        577, 578, 579, 580, 581, 582, 583, 584, 585, 586, 587, 588, 1429, 4487, 4488,
                                        4489, 5572
                                        };
                                        byte[] counts =
                                        {
                                        30, 30, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                        1, 10, 1, 1, 1, 1
                                    };
                                        var item = ServerManager.RandomNumber(min: 0, max: 32);
                                        session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                        break;
                                    }

                                case 5536: // laurena box
                                    {
                                        vnums = null;
                                        vnums = new short[]
                                            {4699, 5835, 5836, 5837, 2282, 1030, 1296, 4491, 4492, 4493, 5604};
                                        byte[] counts = { 1, 1, 1, 1, 50, 50, 5, 1, 1, 1, 1 };
                                        var item = ServerManager.RandomNumber(min: 0, max: 11);
                                        session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                        break;
                                    }
                                case 882: // morcos
                                    var rnd7 = ServerManager.RandomNumber(min: 0, max: 1000);
                                    var random = ServerManager.RandomNumber(min: 5, max: 8);
                                    short[] vnums7 = null;
                                    if (rnd7 < 900)
                                        vnums7 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                                    else
                                        vnums7 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                                    session.Character.GiftAdd(
                                        itemVNum: vnums7[ServerManager.RandomNumber(min: 0, max: 7)], amount: 1,
                                        rare: (byte)random);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                case 5300: // Bushi Random Box
                                    {
                                        rnd = ServerManager.RandomNumber(min: 0, max: 1000);
                                        vnums = null;
                                        vnums = new short[]
                                        {
                                        284, 1904, 1285, 9029, 1119, 2160, 1296, 4085, 4086, 4087, 4088, 4089, 4090,
                                        4091, 5119
                                        };
                                        byte[] counts = { 1, 1, 1, 1, 40, 1, 1, 1, 1, 1, 1, 1, 1, 10 };
                                        var item = ServerManager.RandomNumber(min: 0, max: 11);
                                        session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);

                                        break;
                                    }

                                case 5131: //2 Bushi Random Box
                                    {
                                        rnd = ServerManager.RandomNumber(min: 0, max: 1000);
                                        vnums = null;
                                        vnums = new short[]
                                        {
                                        284, 1904, 1285, 9029, 1119, 2160, 1296, 4085, 4086, 4087, 4088, 4089, 4090,
                                        4091, 5119
                                        };
                                        byte[] counts = { 1, 1, 1, 16, 1, 40, 10, 1, 1, 1, 1, 1, 1, 20 };
                                        var item = ServerManager.RandomNumber(min: 0, max: 11);
                                        session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);

                                        break;
                                    }

                                case 5132: // Sammler Bushi Random Box
                                    {
                                        rnd = ServerManager.RandomNumber(min: 0, max: 1000);
                                        vnums = null;
                                        vnums = new short[]
                                        {
                                        284, 1904, 1285, 9029, 1119, 2160, 1296, 4080, 192, 943, 399, 398, 888, 980,
                                        5119
                                        };
                                        byte[] counts = { 1, 1, 1, 16, 1, 40, 10, 1, 1, 1, 1, 1, 1, 20 };
                                        var item = ServerManager.RandomNumber(min: 0, max: 11);
                                        session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);

                                        break;
                                    }

                                case 999: // berios
                                    var rnd9 = ServerManager.RandomNumber(min: 0, max: 1000);
                                    var random9 = ServerManager.RandomNumber(min: 5, max: 8);
                                    short[] vnums9 = null;
                                    if (rnd9 < 900)
                                        vnums9 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                                    else
                                        vnums9 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                                    session.Character.GiftAdd(
                                        itemVNum: vnums9[ServerManager.RandomNumber(min: 0, max: 7)], amount: 1,
                                        rare: (byte)random9);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                case 942: // morcos
                                    var rnd10 = ServerManager.RandomNumber(min: 0, max: 1000);
                                    var random10 = ServerManager.RandomNumber(min: 5, max: 8);
                                    short[] vnums10 = null;
                                    if (rnd10 < 900)
                                        vnums10 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                                    else
                                        vnums10 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                                    session.Character.GiftAdd(
                                        itemVNum: vnums10[ServerManager.RandomNumber(min: 0, max: 7)], amount: 1,
                                        rare: (byte)random10);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                case 5960: //better caligfor
                                    {
                                        var caligor1 = ServerManager.RandomNumber(min: 0, max: 1000);
                                        short[] caligorvnums1 = null;
                                        if (caligor1 < 50)
                                            caligorvnums1 = new short[]
                                                {4491, 4492, 4493, 4491, 9600, 4490, 4483, 4842, 4841};
                                        else
                                            caligorvnums1 = new short[]
                                                {4490, 4329, 4330, 4334, 4315, 4304, 4305, 1296, 5370};
                                        session.Character.GiftAdd(
                                            itemVNum: caligorvnums1[ServerManager.RandomNumber(min: 0, max: 8)], amount: 1,
                                            rare: 0, upgrade: 0,
                                            design: 1);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                        break;
                                    }

                                case 5961: // syf caligor drop
                                    {
                                        var caligorr1 = ServerManager.RandomNumber(min: 0, max: 1000);
                                        short[] caligorrvnums1 = null;
                                        if (caligorr1 < 500)
                                            caligorrvnums1 = new short[]
                                                {4490, 4329, 4491, 4493, 4492, 4304, 4305, 5553, 5553};
                                        else
                                            session.SendPacket(
                                                packet: UserInterfaceHelper.GenerateInfo(message: "NOT_THIS_TIME"));
                                        session.Character.GiftAdd(
                                            itemVNum: caligorrvnums1[ServerManager.RandomNumber(min: 0, max: 8)], amount: 1,
                                            rare: 0, upgrade: 0,
                                            design: 1);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                        break;
                                    }

                                case 9762: // family 50k pdr
                                    {
                                        if (session.Character.Family != null)
                                        {
                                            if (session.Character.Family.FamilyLevel == 20)
                                            {
                                                session.SendPacket(
                                                    packet: UserInterfaceHelper.GenerateInfo(message: "FAMILY_LEVEL_MAX"));
                                            }
                                            else
                                            {
                                                session.Character.GenerateFamilyXp(FXP: 500000);
                                                session.Character.Inventory.RemoveItemAmount(vnum: 9600);
                                                CommunicationServiceClient.Instance.SendMessageToCharacter(
                                                    message: new ScsCharacterMessage
                                                    {
                                                        DestinationCharacterId = null,
                                                        SourceCharacterId = session.Character.CharacterId,
                                                        SourceWorldId = ServerManager.Instance.WorldId,
                                                        Message =
                                                            $"{session.Character.Name} used bonus and got 50.000 Family Exp!",
                                                        Type = MessageType.Shout
                                                    });
                                            }
                                        }
                                        else
                                        {
                                            session.SendPacket(
                                                packet: UserInterfaceHelper.GenerateInfo(message: "YOU_DONT_HAVE_FAMILY"));
                                        }
                                    }
                                    break;

                                case 185: // hatus
                                    var rnd8 = ServerManager.RandomNumber(min: 0, max: 1000);
                                    var random8 = ServerManager.RandomNumber(min: 5, max: 8);
                                    short[] vnums8 = null;
                                    if (rnd8 < 900)
                                        vnums8 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                                    else
                                        vnums8 = new short[] { 567, 570, 573, 576, 579, 582, 585, 588 };
                                    session.Character.GiftAdd(
                                        itemVNum: vnums8[ServerManager.RandomNumber(min: 0, max: 7)], amount: 1,
                                        rare: (byte)random8);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                case 4718: // Fernon Mystery Box
                                    {
                                        rnd = ServerManager.RandomNumber(min: 0, max: 1000);
                                        vnums = null;
                                        vnums = new short[]
                                        {
                                        5560, 5553, 442, 5950, 4315, 5831, 5841, 5604, 429, 4425, 9675, 5837, 5702, 2096
                                        };
                                        byte[] counts = { 1, 1, 1, 5, 1, 1, 1, 1, 1, 1, 5, 1, 1, 2 };
                                        var item = ServerManager.RandomNumber(min: 0, max: 13);
                                        session.Character.GiftAdd(itemVNum: vnums[item], amount: counts[item]);
                                        session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);

                                        break;
                                    }

                                // Mother Nature's Rune Pack (limited)
                                case 9117:
                                    rnd = ServerManager.RandomNumber(min: 0, max: 1000);
                                    vnums = null;
                                    if (rnd < 900)
                                        vnums = new short[] { 8312, 8313, 8314, 8315 };
                                    else
                                        vnums = new short[] { 8316, 8317, 8318, 8319 };
                                    session.Character.GiftAdd(
                                        itemVNum: vnums[ServerManager.RandomNumber(min: 0, max: 4)], amount: 1);
                                    session.Character.Inventory.RemoveItemFromInventory(id: inv.Id);
                                    break;

                                default:
                                    var roll = DaoFactory.RollGeneratedItemDao.LoadByItemVNum(vnum: VNum);
                                    IEnumerable<RollGeneratedItemDto> rollGeneratedItemDtos =
                                        roll as IList<RollGeneratedItemDto> ?? roll.ToList();
                                    if (!rollGeneratedItemDtos.Any())
                                    {
                                        Logger.Warn(data: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "NO_HANDLER_ITEM"),
                                            GetType(), VNum,
                                            Effect, EffectValue));
                                        return;
                                    }

                                    var probabilities = rollGeneratedItemDtos
                                        .Where(predicate: s => s.Probability != 10000)
                                        .Sum(selector: s => s.Probability);
                                    var rnd2 = ServerManager.RandomNumber(min: 0, max: probabilities);
                                    var currentrnd = 0;
                                    foreach (var rollitem in rollGeneratedItemDtos.Where(predicate: s =>
                                        s.Probability == 10000))
                                    {
                                        sbyte rare = 0;
                                        if (rollitem.IsRareRandom)
                                        {
                                            rnd = ServerManager.RandomNumber();

                                            for (var j = ItemHelper.RareRate.Length - 1; j >= 0; j--)
                                                if (rnd < ItemHelper.RareRate[j])
                                                {
                                                    rare = (sbyte)j;
                                                    break;
                                                }

                                            if (rare < 1) rare = 1;
                                        }

                                        session.Character.GiftAdd(itemVNum: rollitem.ItemGeneratedVNum,
                                            amount: rollitem.ItemGeneratedAmount, rare: (byte)rare,
                                            design: rollitem.ItemGeneratedDesign);
                                    }

                                    foreach (var rollitem in rollGeneratedItemDtos
                                        .Where(predicate: s => s.Probability != 10000)
                                        .OrderBy(keySelector: s => ServerManager.RandomNumber()))
                                    {
                                        sbyte rare = 0;
                                        if (rollitem.IsRareRandom)
                                        {
                                            rnd = ServerManager.RandomNumber();

                                            for (var j = ItemHelper.RareRate.Length - 1; j >= 0; j--)
                                                if (rnd < ItemHelper.RareRate[j])
                                                {
                                                    rare = (sbyte)j;
                                                    break;
                                                }

                                            if (rare < 1) rare = 1;
                                        }

                                        currentrnd += rollitem.Probability;
                                        if (currentrnd < rnd2) continue;
                                        /*if (rollitem.IsSuperReward)
                                        {
                                            CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                                            {
                                                DestinationCharacterId = null,
                                                SourceCharacterId = session.Character.CharacterId,
                                                SourceWorldId = ServerManager.Instance.WorldId,
                                                Message = Language.Instance.GetMessageFromKey("SUPER_REWARD"),
                                                Type = MessageType.Shout
                                            });
                                        }*/
                                        session.Character.GiftAdd(itemVNum: rollitem.ItemGeneratedVNum,
                                            amount: rollitem.ItemGeneratedAmount, rare: (byte)rare,
                                            design: rollitem.ItemGeneratedDesign); //, rollitem.ItemGeneratedUpgrade);
                                        break;
                                    }

                                    session.Character.Inventory.RemoveItemAmount(vnum: VNum);
                                    break;
                            }

                            break;
                    }

                    break;
            }

            session.Character.IncrementQuests(type: QuestType.Use, firstData: inv.ItemVNum);
        }

        #endregion
    }
}