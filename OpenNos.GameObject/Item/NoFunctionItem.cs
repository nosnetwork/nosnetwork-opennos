﻿using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class NoFunctionItem : Item
    {
        #region Instantiation

        public NoFunctionItem(ItemDto item) : base(item: item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte option = 0,
            string[] packetsplit = null)
        {
            if (session.Character.IsVehicled)
            {
                session.SendPacket(
                    packet: session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_DO_VEHICLED"), type: 10));
                return;
            }

            if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance) return;

            switch (Effect)
            {
                case 10:
                    {
                        switch (EffectValue)
                        {
                            case 1:
                                if (session.Character.Inventory.CountItem(itemVNum: 1036) < 1 ||
                                    session.Character.Inventory.CountItem(itemVNum: 1013) < 1) return;
                                session.Character.Inventory.RemoveItemAmount(vnum: 1036);
                                session.Character.Inventory.RemoveItemAmount(vnum: 1013);
                                if (ServerManager.RandomNumber() < 25)
                                    switch (ServerManager.RandomNumber(min: 0, max: 2))
                                    {
                                        case 0:
                                            session.Character.GiftAdd(itemVNum: 1015, amount: 1);
                                            break;
                                        case 1:
                                            session.Character.GiftAdd(itemVNum: 1016, amount: 1);
                                            break;
                                    }

                                break;
                            case 2:
                                if (session.Character.Inventory.CountItem(itemVNum: 1038) < 1 ||
                                    session.Character.Inventory.CountItem(itemVNum: 1013) < 1) return;
                                session.Character.Inventory.RemoveItemAmount(vnum: 1038);
                                session.Character.Inventory.RemoveItemAmount(vnum: 1013);
                                if (ServerManager.RandomNumber() < 25)
                                    switch (ServerManager.RandomNumber(min: 0, max: 4))
                                    {
                                        case 0:
                                            session.Character.GiftAdd(itemVNum: 1031, amount: 1);
                                            break;
                                        case 1:
                                            session.Character.GiftAdd(itemVNum: 1032, amount: 1);
                                            break;
                                        case 2:
                                            session.Character.GiftAdd(itemVNum: 1033, amount: 1);
                                            break;
                                        case 3:
                                            session.Character.GiftAdd(itemVNum: 1034, amount: 1);
                                            break;
                                    }

                                break;
                            case 3:
                                if (session.Character.Inventory.CountItem(itemVNum: 1037) < 1 ||
                                    session.Character.Inventory.CountItem(itemVNum: 1013) < 1) return;
                                session.Character.Inventory.RemoveItemAmount(vnum: 1037);
                                session.Character.Inventory.RemoveItemAmount(vnum: 1013);
                                if (ServerManager.RandomNumber() < 25)
                                    switch (ServerManager.RandomNumber(min: 0, max: 17))
                                    {
                                        case 0:
                                        case 1:
                                        case 2:
                                        case 3:
                                        case 4:
                                            session.Character.GiftAdd(itemVNum: 1017, amount: 1);
                                            break;
                                        case 5:
                                        case 6:
                                        case 7:
                                        case 8:
                                            session.Character.GiftAdd(itemVNum: 1018, amount: 1);
                                            break;
                                        case 9:
                                        case 10:
                                        case 11:
                                            session.Character.GiftAdd(itemVNum: 1019, amount: 1);
                                            break;
                                        case 12:
                                        case 13:
                                            session.Character.GiftAdd(itemVNum: 1020, amount: 1);
                                            break;
                                        case 14:
                                            session.Character.GiftAdd(itemVNum: 1021, amount: 1);
                                            break;
                                        case 15:
                                            session.Character.GiftAdd(itemVNum: 1022, amount: 1);
                                            break;
                                        case 16:
                                            session.Character.GiftAdd(itemVNum: 1023, amount: 1);
                                            break;
                                    }

                                break;
                        }

                        session.Character.GiftAdd(itemVNum: 1014,
                            amount: (byte)ServerManager.RandomNumber(min: 5, max: 11));
                    }
                    break;
                default:
                    Logger.Warn(data: string.Format(format: Language.Instance.GetMessageFromKey(key: "NO_HANDLER_ITEM"),
                        GetType(), VNum,
                        Effect, EffectValue));
                    break;
            }
        }

        #endregion
    }
}