﻿using System.Linq;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class ProduceItem : Item
    {
        #region Instantiation

        public ProduceItem(ItemDto item) : base(item: item)
        {
        }

        #endregion

        #region Methods

        public override void Use(ClientSession session, ref ItemInstance inv, byte option = 0,
            string[] packetsplit = null)
        {
            if (session.Character.IsVehicled)
            {
                session.SendPacket(
                    packet: session.Character.GenerateSay(
                        message: Language.Instance.GetMessageFromKey(key: "CANT_DO_VEHICLED"), type: 10));
                return;
            }

            if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.TalentArenaMapInstance) return;

            switch (Effect)
            {
                case 100:
                    session.Character.LastNRunId = 0;
                    session.Character.LastItemVNum = inv.ItemVNum;
                    session.SendPacket(packet: "wopen 28 0");
                    var recipeList = ServerManager.Instance.GetRecipesByItemVNum(itemVNum: VNum);
                    var list = recipeList.Where(predicate: s => s.Amount > 0)
                        .Aggregate(seed: "m_list 2", func: (current, s) => current + $" {s.ItemVNum}");
                    session.SendPacket(packet: list + (EffectValue <= 110 && EffectValue >= 108 ? " 999" : ""));
                    break;

                default:
                    Logger.Warn(data: string.Format(format: Language.Instance.GetMessageFromKey(key: "NO_HANDLER_ITEM"),
                        GetType(), VNum,
                        Effect, EffectValue));
                    break;
            }
        }

        #endregion
    }
}