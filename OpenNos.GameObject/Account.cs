﻿using System.Collections.Generic;
using System.Linq;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class Account : AccountDto
    {
        public Account(AccountDto input)
        {
            AccountId = input.AccountId;
            Authority = input.Authority;
            Email = input.Email;
            Name = input.Name;
            Password = input.Password;
            ReferrerId = input.ReferrerId;
            RegistrationIp = input.RegistrationIp;
            VerificationToken = input.VerificationToken;
        }

        #region Properties

        public bool IsLimited
        {
            get =>
                Authority >= AuthorityType.Administrator &&
                Authority != AuthorityType.Administrator; //Is Limited Fix
        }

        public List<PenaltyLogDto> PenaltyLogs
        {
            get
            {
                var logs = new PenaltyLogDto[ServerManager.Instance.PenaltyLogs.Count + 10];
                ServerManager.Instance.PenaltyLogs.CopyTo(array: logs);
                return logs.Where(predicate: s => s != null && s.AccountId == AccountId).ToList();
            }
        }

        #endregion
    }
}