﻿using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class Portal : PortalDto
    {
        #region Methods

        public string GenerateGp()
        {
            return
                $"gp {SourceX} {SourceY} {ServerManager.GetMapInstance(id: DestinationMapInstanceId)?.Map.MapId ?? 0} {Type} {PortalId} {(IsDisabled ? 1 : 0)}";
        }

        #endregion

        #region Members

        Guid _destinationMapInstanceId;

        Guid _sourceMapInstanceId;

        #endregion

        #region Instantiation

        public Portal()
        {
            OnTraversalEvents = new List<EventContainer>();
        }

        public Portal(PortalDto input)
        {
            OnTraversalEvents = new List<EventContainer>();
            DestinationMapId = input.DestinationMapId;
            DestinationX = input.DestinationX;
            DestinationY = input.DestinationY;
            IsDisabled = input.IsDisabled;
            PortalId = input.PortalId;
            SourceMapId = input.SourceMapId;
            SourceX = input.SourceX;
            SourceY = input.SourceY;
            Type = input.Type;
        }

        #endregion

        #region Properties

        public Guid DestinationMapInstanceId
        {
            get
            {
                if (_destinationMapInstanceId == default && DestinationMapId != -1)
                    _destinationMapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(mapId: DestinationMapId);
                return _destinationMapInstanceId;
            }
            set => _destinationMapInstanceId = value;
        }

        public List<EventContainer> OnTraversalEvents { get; set; }

        public Guid SourceMapInstanceId
        {
            get
            {
                if (_sourceMapInstanceId == default)
                    _sourceMapInstanceId = ServerManager.GetBaseMapInstanceIdByMapId(mapId: SourceMapId);
                return _sourceMapInstanceId;
            }
            set => _sourceMapInstanceId = value;
        }

        #endregion
    }
}