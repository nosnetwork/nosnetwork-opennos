﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class Shop : ShopDto
    {
        #region Methods

        public void Initialize()
        {
            ShopItems = ServerManager.Instance.GetShopItemsByShopId(shopId: ShopId);
            ShopSkills = ServerManager.Instance.GetShopSkillsByShopId(shopId: ShopId);
        }

        #endregion

        #region Instantiation

        public Shop()
        {
        }

        public Shop(ShopDto input)
        {
            MapNpcId = input.MapNpcId;
            MenuType = input.MenuType;
            Name = input.Name;
            ShopId = input.ShopId;
            ShopType = input.ShopType;
        }

        #endregion

        #region Properties

        public List<ShopItemDto> ShopItems { get; set; }

        public List<ShopSkillDto> ShopSkills { get; set; }

        #endregion
    }
}