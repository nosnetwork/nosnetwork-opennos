﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.Core.Threading;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject
{
    public class Group : IDisposable
    {
        #region Instantiation

        public Group()
        {
            Sessions = new ThreadSafeGenericList<ClientSession>();
            GroupId = ServerManager.Instance.GetNextGroupId();
            _order = 0;
        }

        #endregion

        #region Members

        readonly object _syncObj = new object();
        bool _disposed;
        int _order;

        #endregion

        #region Properties

        public int SessionCount
        {
            get => Sessions.Count;
        }

        public ThreadSafeGenericList<ClientSession> Characters { get; }

        public ThreadSafeGenericList<ClientSession> Sessions { get; }

        public long GroupId { get; set; }

        public GroupType GroupType { get; set; }

        public ScriptedInstance Raid { get; set; }

        public byte SharingMode { get; set; }

        public TalentArenaBattle TalentArenaBattle { get; set; }

        #endregion

        #region Methods

        public void Dispose()
        {
            if (!_disposed)
            {
                Dispose(disposing: true);
                GC.SuppressFinalize(obj: this);
                _disposed = true;
            }
        }

        public List<string> GeneratePst(ClientSession player)
        {
            var str = new List<string>();
            var i = 0;
            str.AddRange(collection: player.Character.Mates.Where(predicate: s => s.IsTeamMember)
                .OrderByDescending(keySelector: s => s.MateType).Select(
                    selector: mate =>
                        $"pst 2 {mate.MateTransportId} {((short)mate.MateType == 1 ? ++i : 0)} {(int)(mate.Hp / mate.MaxHp * 100)} {(int)(mate.Mp / mate.MaxMp * 100)} {mate.Hp} {mate.Mp} 0 0 0 {mate.Buff.GetAllItems().Aggregate(seed: "", func: (current, buff) => current + $" {buff.Card.CardId}")}"));
            Sessions.Where(predicate: s => s != player).ForEach(action: session =>
            {
                str.Add(
                    item:
                    $"pst 1 {session.Character.CharacterId} {++i} {(int)(session.Character.Hp / session.Character.HPLoad() * 100)} {(int)(session.Character.Mp / session.Character.MPLoad() * 100)} {session.Character.HPLoad()} {session.Character.MPLoad()} {(byte)session.Character.Class} {(byte)session.Character.Gender} {(session.Character.UseSp ? session.Character.Morph : 0)}{session.Character.Buff.GetAllItems().Where(predicate: s => !s.StaticBuff || new short[] { 339, 340 }.Contains(value: s.Card.CardId)).Aggregate(seed: "", func: (current, buff) => current + $" {buff.Card.CardId}")}");
            });
            Sessions.Where(predicate: s => s == player).ForEach(action: session =>
            {
                i = session.Character.Mates.Count(predicate: s => s.IsTeamMember);
                str.Add(
                    item:
                    $"pst 1 {session.Character.CharacterId} {++i} {(int)(session.Character.Hp / session.Character.HPLoad() * 100)} {(int)(session.Character.Mp / session.Character.MPLoad() * 100)} {session.Character.HPLoad()} {session.Character.MPLoad()} {(byte)session.Character.Class} {(byte)session.Character.Gender} {(session.Character.UseSp ? session.Character.Morph : 0)}");
            });
            return str;
        }

        public string GenerateRdlst()
        {
            string result;

            if (GroupType != GroupType.GiantTeam)
                result = $"rdlst {Raid?.LevelMinimum ?? 1} {Raid?.LevelMaximum ?? 99} 0";
            else
                result = $"rdlstf {Raid?.LevelMinimum ?? 1} {Raid?.LevelMaximum ?? 99} 0 0";

            try
            {
                Sessions.ForEach(action: session =>
                    result +=
                        $" {session.Character.Level}.{(session.Character.UseSp || session.Character.IsVehicled ? session.Character.Morph : -1)}.{(short)session.Character.Class}.{Raid?.InstanceBag.DeadList.Count(predicate: s => s == session.Character.CharacterId) ?? 0}.{session.Character.Name}.{(short)session.Character.Gender}.{session.Character.CharacterId}.{session.Character.HeroLevel}");
            }
            catch (Exception ex)
            {
                Logger.Error(ex: ex, memberName: nameof(GenerateRdlst));
            }

            return result;
        }

        public string GeneraterRaidmbf(ClientSession session)
        {
            return
                $"raidmbf {session?.CurrentMapInstance?.InstanceBag?.MonsterLocker.Initial} {session?.CurrentMapInstance?.InstanceBag?.MonsterLocker.Current} {session?.CurrentMapInstance?.InstanceBag?.ButtonLocker.Initial} {session?.CurrentMapInstance?.InstanceBag?.ButtonLocker.Current} {Raid?.InstanceBag?.Lives - Raid?.InstanceBag?.DeadList.Count} {Raid?.InstanceBag?.Lives} {(GroupType == GroupType.GiantTeam ? 0 : 25)}";
        }

        public long? GetNextOrderedCharacterId(Character character)
        {
            lock (_syncObj)
            {
                _order++;
                var sessions = Sessions.Where(predicate: s =>
                    Map.GetDistance(character1: s.Character, character2: character) < 50);
                if (_order > sessions.Count - 1) // if order wents out of amount of ppl, reset it -> zero based index
                    _order = 0;

                if (sessions.Count == 0) // group seems to be empty
                    return null;

                return sessions[index: _order].Character.CharacterId;
            }
        }

        public bool IsLeader(ClientSession session)
        {
            if (Sessions.Count > 0)
                return Sessions.FirstOrDefault() == session;
            return false;
        }

        public bool IsMemberOfGroup(long entityId)
        {
            return Sessions?.Any(predicate: s =>
                s?.Character != null && (s.Character.CharacterId == entityId ||
                                         s.Character.Mates.Any(predicate: m =>
                                             m.IsTeamMember && m.MateTransportId == entityId))) == true;
        }

        public bool IsMemberOfGroup(ClientSession session)
        {
            return Sessions?.Any(predicate: s => s?.Character?.CharacterId == session.Character.CharacterId) == true;
        }

        public void JoinGroup(long characterId)
        {
            var session = ServerManager.Instance.GetSessionByCharacterId(characterId: characterId);
            if (session != null) JoinGroup(session: session);
        }

        public bool JoinGroup(ClientSession session)
        {
            if (Sessions.Count > 0 &&
                session.Character.IsBlockedByCharacter(characterId: Sessions.FirstOrDefault().Character.CharacterId))
            {
                session.SendPacket(
                    packet: UserInterfaceHelper.GenerateInfo(
                        message: Language.Instance.GetMessageFromKey(key: "BLACKLIST_BLOCKED")));
                return false;
            }

            if (Raid != null)
            {
                var entries = Raid.DailyEntries - session.Character.GeneralLogs.CountLinq(predicate: s =>
                    s.LogType == "InstanceEntry" && short.Parse(s: s.LogData) == Raid.Id &&
                    s.Timestamp.Date == DateTime.Today);
                if (Raid.DailyEntries > 0 && entries <= 0)
                {
                    session.SendPacket(
                        packet: UserInterfaceHelper.GenerateMsg(
                            message: Language.Instance.GetMessageFromKey(key: "INSTANCE_NO_MORE_ENTRIES"),
                            type: 0));
                    session.SendPacket(
                        packet: session.Character.GenerateSay(
                            message: Language.Instance.GetMessageFromKey(key: "INSTANCE_NO_MORE_ENTRIES"),
                            type: 10));
                    return false;
                }

                if (Raid.RequiredItems != null)
                    foreach (var requiredItem in Raid.RequiredItems)
                        if (ServerManager.GetItem(vnum: requiredItem.VNum).Type == InventoryType.Equipment
                            && !session.Character.Inventory.Any(predicate: s =>
                                s.ItemVNum == requiredItem.VNum && s.Type == InventoryType.Wear))
                        {
                            session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                message: string.Format(
                                    format: Language.Instance.GetMessageFromKey(key: "ITEM_NOT_EQUIPPED"),
                                    arg0: ServerManager.GetItem(vnum: requiredItem.VNum).Name), type: 0));
                            return false;
                        }
            }

            session.Character.Group = this;
            Sessions.Add(value: session);
            if (GroupType == GroupType.Group)
                if (Sessions.Find(predicate: c =>
                        c.Character.IsCoupleOfCharacter(characterId: session.Character.CharacterId)) is ClientSession
                    couple)
                {
                    session.Character.AddStaticBuff(staticBuff: new StaticBuffDto { CardId = 319 }, isPermaBuff: true);
                    couple.Character.AddStaticBuff(staticBuff: new StaticBuffDto { CardId = 319 }, isPermaBuff: true);
                }

            return true;
        }

        public void LeaveGroup(ClientSession session)
        {
            session.Character.Group = null;
            if (Sessions.Find(predicate: c =>
                    c.Character.IsCoupleOfCharacter(characterId: session.Character.CharacterId)) is ClientSession
                couple)
            {
                session.Character.RemoveBuff(cardId: 319, removePermaBuff: true);
                couple.Character.RemoveBuff(cardId: 319, removePermaBuff: true);
            }

            Sessions.RemoveAll(match: s => s?.Character.CharacterId == session.Character.CharacterId);
            if (IsLeader(session: session) && GroupType != GroupType.Group && Sessions.Count > 1)
                Sessions.ForEach(action: s =>
                    s.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                        message: string.Format(format: Language.Instance.GetMessageFromKey(key: "TEAM_LEADER_CHANGE"),
                            arg0: Sessions.ElementAt(v: 0).Character?.Name), type: 0)));
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing) Sessions.Dispose();
        }

        #endregion
    }
}