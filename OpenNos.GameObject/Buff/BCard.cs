using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Windows.Media.Media3D;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Buff;
using OpenNos.GameObject.Event;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using OpenNos.PathFinder;

namespace OpenNos.GameObject
{
    public class BCard : BCardDto
    {
        public BCard()
        {
        }

        public BCard(BCardDto input) : this()
        {
            BCardId = input.BCardId;
            CardId = input.CardId;
            CastType = input.CastType;
            FirstData = input.FirstData;
            IsLevelDivided = input.IsLevelDivided;
            IsLevelScaled = input.IsLevelScaled;
            ItemVNum = input.ItemVNum;
            NpcMonsterVNum = input.NpcMonsterVNum;
            SecondData = input.SecondData;
            SkillVNum = input.SkillVNum;
            SubType = input.SubType;
            ThirdData = input.ThirdData;
            Type = input.Type;
        }

        #region Properties

        public bool IsPartnerSkillBCard { get; set; }

        public int ForceDelay { get; set; }

        #endregion

        #region Methods

        public void ApplyBCards(BattleEntity session, BattleEntity sender, short x = 0, short y = 0)
        {
            /*if (Type != (byte)BCardType.CardType.Buff && (CardId != null || SkillVNum != null))
            {
                Console.WriteLine($"BCardId: {BCardId} Type: {(BCardType.CardType)Type} SubType: {SubType} CardId: {CardId?.ToString() ?? "null"} ItemVNum: {ItemVNum?.ToString() ?? "null"} SkillVNum: {SkillVNum?.ToString() ?? "null"} SessionType: {session?.EntityType.ToString() ?? "null"} SenderType: {sender?.EntityType.ToString() ?? "null"}");
            }*/

            var firstData = FirstData;
            int senderLevel = sender.MapMonster?.Owner?.Level ?? sender.Level;

            Card card = null;
            Skill skill = null;
            var delayTime = 0;
            var duration = 0;

            if (CardId is short cardId2 && ServerManager.Instance.GetCardByCardId(cardId: cardId2) is Card BuffCard)
            {
                card = BuffCard;

                if (CastType == 1) delayTime = card.Delay * 100;

                duration = card.Duration * 100 - delayTime;
            }

            if (SkillVNum is short skillVNum && ServerManager.GetSkill(skillVNum: skillVNum) is Skill Skill)
            {
                skill = Skill;
                if (sender.Character != null)
                {
                    var skills = sender.Character.GetSkills();

                    if (skills != null)
                    {
                        firstData = skills.Find(match: s => s.SkillVNum == skill.SkillVNum)?.GetSkillBCards()
                                        .OrderByDescending(keySelector: s => s.SkillVNum)
                                        .FirstOrDefault(predicate: b => b.Type == Type && b.SubType == SubType)
                                        .FirstData ??
                                    FirstData;
                        //firstData = skills.Where(s => s.SkillVNum == skill.SkillVNum).Sum(s => s.GetSkillBCards().Where(b => b.Type == Type && b.SubType == SubType).Sum(b => b.FirstData));
                        if (firstData == 0) firstData = FirstData;
                    }
                }
            }

            if (ForceDelay > 0) delayTime = ForceDelay * 100;

            if (BCardId > 0)
                session.BCardDisposables[key: skill?.SkillVNum == 1098 ? skill.SkillVNum * 1000 : BCardId]?.Dispose();
            session.BCardDisposables[key: skill?.SkillVNum == 1098 ? skill.SkillVNum * 1000 : BCardId] = Observable
                .Timer(dueTime: TimeSpan.FromMilliseconds(value: delayTime)).Subscribe(onNext: o =>
                {
                    switch ((BCardType.CardType)Type)
                    {
                        case BCardType.CardType.Buff:
                            {
                                var cardId = (short)SecondData;

                                // Memorial should only be applied on 1st Mass Teleport activation

                                if (cardId == 620 && sender?.Character?.SavedLocation != null) return;

                                var buff = new Buff.Buff(id: cardId, level: senderLevel)
                                {
                                    SkillVNum = SkillVNum
                                };

                                var Chance = firstData == 0 ? ThirdData : firstData;
                                var CardsToProtect = new List<short>();
                                if (buff.Card.BuffType == BuffType.Bad &&
                                    session.GetBuff(type: BCardType.CardType.DebuffResistance,
                                            subtype: (byte)AdditionalTypes.DebuffResistance.NeverBadEffectChance) is int[]
                                        NeverBadEffectChance)
                                    if (ServerManager.RandomNumber() < NeverBadEffectChance[1]
                                        && buff.Card.Level <= -NeverBadEffectChance[0])
                                        return;
                                if (session.GetBuff(type: BCardType.CardType.DebuffResistance,
                                        subtype: (byte)AdditionalTypes.DebuffResistance.NeverBadGeneralEffectChance) is int
                                    []
                                    NeverBadGeneralEffectChance)
                                    if (ServerManager.RandomNumber() < NeverBadGeneralEffectChance[1]
                                        && buff.Card.Level <= -NeverBadGeneralEffectChance[0]
                                        && buff.Card.BuffType == BuffType.Bad)
                                        return;
                                if (session.GetBuff(type: BCardType.CardType.Buff,
                                            subtype: (byte)AdditionalTypes.Buff.PreventingBadEffect) is int[]
                                        PreventingBadEffect &&
                                    (PreventingBadEffect[1] > 0 || PreventingBadEffect[2] > 0))
                                {
                                    var Prob = 100 - PreventingBadEffect[1] * 10;
                                    var ProtectType = PreventingBadEffect[0];

                                    if (PreventingBadEffect[2] > 0)
                                    {
                                        Prob = PreventingBadEffect[2];
                                        ProtectType = PreventingBadEffect[1];
                                    }

                                    if (ServerManager.RandomNumber() < Prob && buff.Card.BuffType == BuffType.Bad)
                                        switch (ProtectType)
                                        {
                                            case 0:
                                                //Bleedings
                                                CardsToProtect.Add(item: 1);
                                                CardsToProtect.Add(item: 21);
                                                CardsToProtect.Add(item: 42);
                                                CardsToProtect.Add(item: 82);
                                                CardsToProtect.Add(item: 189);
                                                CardsToProtect.Add(item: 190);
                                                CardsToProtect.Add(item: 191);
                                                CardsToProtect.Add(item: 192);
                                                break;
                                            case 4:
                                                //Blackouts
                                                CardsToProtect.Add(item: 7);
                                                CardsToProtect.Add(item: 66);
                                                CardsToProtect.Add(item: 100);
                                                CardsToProtect.Add(item: 195);
                                                CardsToProtect.Add(item: 196);
                                                CardsToProtect.Add(item: 197);
                                                CardsToProtect.Add(item: 198);
                                                break;
                                            case 32:
                                                //Side-effects of resurrecting
                                                CardsToProtect.Add(item: 44);
                                                break;
                                            case 85:
                                                //Foggy Colossus' poison
                                                break;
                                        }
                                }

                                if (buff.Card.BuffType == BuffType.Bad &&
                                    session.GetBuff(type: BCardType.CardType.SpecialisationBuffResistance,
                                        subtype: (byte)AdditionalTypes.SpecialisationBuffResistance.ResistanceToEffect,
                                        secondData: buff.Card.CardId) is int[] ResistanceToEffect)
                                    if (ServerManager.RandomNumber() < ResistanceToEffect[0])
                                        CardsToProtect.Add(item: (short)ResistanceToEffect[1]);

                                if (CardsToProtect.Contains(item: buff.Card.CardId)) return;

                                if (SubType == 1)
                                {
                                    if (Chance > 0 && ServerManager.RandomNumber() < Chance)
                                    {
                                        if (SkillVNum != null && (buff.Card.CardId == 570 || buff.Card.CardId == 56))
                                            sender.AddBuff(indicator: buff, sender: sender, x: x, y: y, forced: true);
                                        else if (buff.Card?.BuffType == BuffType.Bad
                                                 && session.HasBuff(type: BCardType.CardType.TauntSkill,
                                                     subtype: (byte)AdditionalTypes.TauntSkill.ReflectBadEffect)
                                                 && ServerManager.RandomNumber() < FirstData)
                                            sender.AddBuff(indicator: buff, sender: sender, x: x, y: y);
                                        else
                                            session.AddBuff(indicator: buff, sender: sender, x: x, y: y);
                                    }
                                    else if (Chance < 0 && ServerManager.RandomNumber() < -Chance)
                                    {
                                        session.RemoveBuff(id: cardId);
                                    }
                                }
                            }
                            break;
                        case BCardType.CardType.Move:
                            {
                                if (session.Character != null)
                                {
                                    session.Character.LastSpeedChange = DateTime.Now;
                                    session.Character.LoadSpeed();
                                    session.Character.Session?.SendPacket(packet: session.Character.GenerateCond());
                                }
                            }
                            break;

                        case BCardType.CardType.Summons:
                            if (sender.MapMonster?.MonsterVNum == 154) return;
                            var move = SecondData != 1382;
                            var summonParameters = new List<MonsterToSummon>();
                            var amountToSpawn = firstData;
                            var aliveTime = ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime /
                                (ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime < 2400
                                    ? ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime <
                                      150
                                        ? 1
                                        : 10
                                    : 40) * (ServerManager.GetNpcMonster(npcVNum: (short)SecondData)
                                                 .RespawnTime >=
                                             150
                                    ? 4
                                    : 1);
                            for (var i = 0; i < amountToSpawn; i++)
                            {
                                x = (short)(ServerManager.RandomNumber(min: -1, max: 1) + sender.PositionX);
                                y = (short)(ServerManager.RandomNumber(min: -1, max: 1) + sender.PositionY);
                                if (skill != null && sender.Character == null)
                                {
                                    var randomCell =
                                        sender.MapInstance.Map.GetRandomPositionByDistance(xPos: sender.PositionX,
                                            yPos: sender.PositionY, distance: skill.Range, randomInRange: true);
                                    if (randomCell != null)
                                    {
                                        x = randomCell.X;
                                        y = randomCell.Y;
                                    }
                                }

                                summonParameters.Add(item: new MonsterToSummon(vnum: (short)SecondData,
                                    spawnCell: new MapCell { X = x, Y = y },
                                    target: null, move: move, aliveTime: aliveTime, owner: sender));
                            }

                            if (ServerManager.RandomNumber() <= Math.Abs(value: ThirdData) || ThirdData == 0 ||
                                ThirdData < 0)
                                switch (SubType)
                                {
                                    case 2:
                                        if (CardId == null && SkillVNum == null)
                                        {
                                            if (sender.MapMonster != null)
                                            {
                                                sender.BCardDisposables[key: BCardId]?.Dispose();
                                                IDisposable bcardDisposable = null;
                                                bcardDisposable = Observable
                                                    .Interval(period: TimeSpan.FromSeconds(value: 5))
                                                    .Subscribe(onNext: s =>
                                                    {
                                                        if (sender.BCardDisposables[key: BCardId] != bcardDisposable)
                                                        {
                                                            bcardDisposable.Dispose();
                                                            return;
                                                        }

                                                        summonParameters = new List<MonsterToSummon>();
                                                        for (var i = 0; i < amountToSpawn; i++)
                                                        {
                                                            x = (short)(ServerManager.RandomNumber(min: -1, max: 1) +
                                                                         sender.PositionX);
                                                            y = (short)(ServerManager.RandomNumber(min: -1, max: 1) +
                                                                         sender.PositionY);
                                                            summonParameters.Add(item: new MonsterToSummon(
                                                                vnum: (short)SecondData,
                                                                spawnCell: new MapCell { X = x, Y = y }, target: null,
                                                                move: move,
                                                                aliveTime: aliveTime, owner: sender));
                                                        }

                                                        if (sender.MapMonster.Target != null && sender.MapInstance
                                                            .GetCharactersInRange(mapX: sender.PositionX,
                                                                mapY: sender.PositionY, distance: 5)
                                                            .Any(predicate: c => c.BattleEntity.MapEntityId ==
                                                                                 sender.MapMonster.Target
                                                                                     .MapEntityId))
                                                            EventHelper.Instance.RunEvent(
                                                                evt: new EventContainer(mapInstance: sender.MapInstance,
                                                                    eventActionType: EventActionType.Spawnmonsters,
                                                                    param: summonParameters));
                                                    });
                                                sender.BCardDisposables[key: BCardId] = bcardDisposable;
                                            }
                                        }
                                        else
                                        {
                                            EventHelper.Instance.RunEvent(evt: new EventContainer(
                                                mapInstance: sender.MapInstance,
                                                eventActionType: EventActionType.Spawnmonsters,
                                                param: summonParameters));
                                        }

                                        break;

                                    case 3:
                                        summonParameters = new List<MonsterToSummon>();
                                        for (var i = 0; i < amountToSpawn; i++)
                                        {
                                            x = (short)(ServerManager.RandomNumber(min: -1, max: 1) +
                                                         sender.PositionX);
                                            y = (short)(ServerManager.RandomNumber(min: -1, max: 1) +
                                                         sender.PositionY);
                                            summonParameters.Add(item: new MonsterToSummon(vnum: (short)SecondData,
                                                spawnCell: new MapCell { X = x, Y = y }, target: null, move: move,
                                                aliveTimeMp: aliveTime,
                                                owner: sender));
                                        }

                                        EventHelper.Instance.RunEvent(evt: new EventContainer(
                                            mapInstance: sender.MapInstance,
                                            eventActionType: EventActionType.Spawnmonsters, param: summonParameters));
                                        break;

                                    default:
                                        if (!sender.OnDeathEvents.ToList().Any(predicate: s =>
                                            s.EventActionType == EventActionType.Spawnmonsters))
                                            sender.OnDeathEvents.Add(item: new EventContainer(
                                                mapInstance: sender.MapInstance,
                                                eventActionType: EventActionType.Spawnmonsters,
                                                param: summonParameters));
                                        break;
                                }

                            break;

                        case BCardType.CardType.SpecialAttack:
                            break;

                        case BCardType.CardType.SpecialDefence:
                            break;

                        case BCardType.CardType.AttackPower:
                            if (SubType == (byte)AdditionalTypes.AttackPower.AllAttacksIncreased / 10)
                                if (session.Character != null && sender.Character != null &&
                                    session.Character == sender.Character)
                                    if (skill != null && skill.UpgradeSkill == 0)
                                    {
                                        var skills = session.Character.GetSkills();

                                        session.Character.ChargeValue = skills
                                            .Where(predicate: s => s.SkillVNum == skill.SkillVNum)
                                            .Sum(selector: s => s.GetSkillBCards().Sum(selector: b => b.FirstData));
                                        session.AddBuff(indicator: new Buff.Buff(id: 0, level: session.Level),
                                            sender: session);
                                    }

                            if (SubType == (byte)AdditionalTypes.AttackPower.MeleeAttacksIncreased / 10)
                                if (session.Character != null && sender.Character != null &&
                                    session.Character == sender.Character)
                                    if (skill != null && skill.UpgradeSkill == 0)
                                    {
                                        var skills = session.Character.GetSkills();

                                        session.Character.ChargeValue = skills
                                            .Where(predicate: s => s.SkillVNum == skill.SkillVNum)
                                            .Sum(selector: s => s.GetSkillBCards().Sum(selector: b => b.FirstData));
                                        session.AddBuff(indicator: new Buff.Buff(id: 0, level: session.Level),
                                            sender: session);
                                    }

                            if (SubType == (byte)AdditionalTypes.AttackPower.RangedAttacksIncreased / 10)
                                if (session.Character != null && sender.Character != null &&
                                    session.Character == sender.Character)
                                    if (skill != null && skill.UpgradeSkill == 0)
                                    {
                                        var skills = session.Character.GetSkills();

                                        session.Character.ChargeValue = skills
                                            .Where(predicate: s => s.SkillVNum == skill.SkillVNum)
                                            .Sum(selector: s => s.GetSkillBCards().Sum(selector: b => b.FirstData));
                                        session.AddBuff(indicator: new Buff.Buff(id: 0, level: session.Level),
                                            sender: session);
                                    }

                            if (SubType == (byte)AdditionalTypes.AttackPower.MagicalAttacksIncreased / 10)
                                if (session.Character != null && sender.Character != null &&
                                    session.Character == sender.Character)
                                    if (skill != null && skill.UpgradeSkill == 0)
                                    {
                                        var skills = session.Character.GetSkills();

                                        session.Character.ChargeValue = skills
                                            .Where(predicate: s => s.SkillVNum == skill.SkillVNum)
                                            .Sum(selector: s => s.GetSkillBCards().Sum(selector: b => b.FirstData));
                                        session.AddBuff(indicator: new Buff.Buff(id: 0, level: session.Level),
                                            sender: session);
                                    }

                            break;

                        case BCardType.CardType.Target:
                            break;

                        case BCardType.CardType.Critical:
                            break;

                        case BCardType.CardType.SpecialCritical:
                            break;

                        case BCardType.CardType.Element:
                            break;

                        case BCardType.CardType.IncreaseDamage:
                            break;

                        case BCardType.CardType.Defence:
                            break;

                        case BCardType.CardType.DodgeAndDefencePercent:
                            break;

                        case BCardType.CardType.Block:
                            break;

                        case BCardType.CardType.Absorption:
                            break;

                        case BCardType.CardType.ElementResistance:
                            break;

                        case BCardType.CardType.EnemyElementResistance:
                            break;

                        case BCardType.CardType.Damage:
                            break;

                        case BCardType.CardType.GuarantedDodgeRangedAttack:
                            break;

                        case BCardType.CardType.Morale:
                            break;

                        case BCardType.CardType.Casting:
                            break;

                        case BCardType.CardType.Reflection:
                            if (SubType == (byte)AdditionalTypes.Reflection.EnemyMpDecreased / 10)
                                if (ServerManager.RandomNumber() < -firstData)
                                {
                                    session.DecreaseMp(amount: session.Mp * SecondData / 100);
                                    if (session.Character != null)
                                        session.Character.Session.SendPacket(packet: session.Character.GenerateStat());
                                }

                            break;

                        case BCardType.CardType.DrainAndSteal:
                            if (SubType == (byte)AdditionalTypes.DrainAndSteal.ConvertEnemyHpToMp / 10)
                            {
                                var bonus = 0;
                                if (firstData < 0)
                                {
                                    if (IsLevelScaled)
                                        bonus = senderLevel * (firstData - 1) * -1;
                                    else
                                        bonus = firstData * -1;
                                    bonus = session.GetDamage(damage: bonus, damager: sender, dontKill: true,
                                        fromDebuff: true);
                                    if (bonus > 0)
                                    {
                                        session.MapInstance?.Broadcast(packet: session.GenerateDm(dmg: bonus));
                                        session.Character?.Session?.SendPacket(
                                            packet: session.Character?.GenerateStat());
                                        if (sender.Mp + bonus > sender.MPLoad())
                                            sender.Mp = (int)sender.MPLoad();
                                        else
                                            sender.Mp += bonus;
                                        sender.Character?.Session?.SendPacket(packet: sender.Character?.GenerateStat());
                                    }
                                }
                            }
                            else if (SubType == (byte)AdditionalTypes.DrainAndSteal.LeechEnemyHp / 10)
                            {
                                // FirstData = -1 SecondData = 0 SkillVNum = 400 (Tumble) IsLevelScaled = 1

                                if (SecondData == 0) break; // Wtf !? !? !!!

                                if (ServerManager.RandomNumber() < FirstData * -1)
                                    if (session.Hp > 1
                                        && session.MapInstance != null)
                                    {
                                        var amount = senderLevel * SecondData;

                                        if (amount >= session.Hp) amount = session.Hp - 1;

                                        if (sender.Hp + amount > sender.HpMax) amount = sender.HpMax - sender.Hp;

                                        session.Hp -= amount;
                                        sender.Hp += amount;

                                        sender.MapInstance.Broadcast(
                                            packet: StaticPacketHelper.GenerateEff(effectType: sender.UserType,
                                                callerId: sender.MapEntityId, effectId: 18));
                                        sender.MapInstance.Broadcast(
                                            packet: sender.GenerateRc(characterHealth: amount));
                                        sender.Character?.Session?.SendPacket(packet: sender.Character?.GenerateStat());
                                        session.Character?.Session?.SendPacket(
                                            packet: session.Character?.GenerateStat());
                                    }
                            }
                            else if (SubType == (byte)AdditionalTypes.DrainAndSteal.LeechEnemyMp / 10)
                            {
                                // FirstData = -100 SecondData = 3 CardId = 228 (MAna Drain) ThirdData = 1

                                if (ThirdData != 0)
                                {
#pragma warning disable CS1030 // #warning: "TODO: Mana Drain"
#warning TODO: Mana Drain
                                    break;
#pragma warning restore CS1030 // #warning: "TODO: Mana Drain"
                                }

                                if (ServerManager.RandomNumber() < FirstData * -1)
                                    if (session.Mp > 1
                                        && session.MapInstance != null)
                                    {
                                        var amount = senderLevel * SecondData;

                                        if (amount >= session.Mp) amount = session.Mp - 1;

                                        session.Mp -= amount;
                                        sender.Mp += amount;

                                        if (sender.Mp > sender.MpMax) sender.Mp = sender.MpMax;

                                        sender.Character?.Session?.SendPacket(packet: sender.Character?.GenerateStat());
                                        session.Character?.Session?.SendPacket(
                                            packet: session.Character?.GenerateStat());
                                    }
                            }

                            break;

                        case BCardType.CardType.HealingBurningAndCasting:
                            {
                                if (session.HasBuff(type: BCardType.CardType.RecoveryAndDamagePercent, subtype: 01)) return;

                                void HealingBurningAndCastingAction()
                                {
                                    if (session.Hp < 1
                                        || session.MapInstance == null)
                                        return;

                                    var amount = 0;

                                    if (SubType == (byte)AdditionalTypes.HealingBurningAndCasting.RestoreHp / 10
                                        || SubType == (byte)AdditionalTypes.HealingBurningAndCasting.DecreaseHp / 10)
                                    {
                                        if (firstData > 0)
                                        {
                                            if (IsLevelScaled)
                                                amount = senderLevel * firstData;
                                            else
                                                amount = firstData;

                                            if (session.Hp + amount > session.HpMax) amount = session.HpMax - session.Hp;

                                            if (amount > 0)
                                            {
                                                if (session.HasBuff(type: BCardType.CardType.DarkCloneSummon,
                                                    subtype: (byte)AdditionalTypes.DarkCloneSummon
                                                        .ConvertRecoveryToDamage))
                                                {
                                                    amount = session.GetDamage(damage: amount, damager: sender,
                                                        dontKill: true, fromDebuff: true);

                                                    session.MapInstance.Broadcast(packet: session.GenerateDm(dmg: amount));
                                                }
                                                else
                                                {
                                                    session.Hp += amount;

                                                    session.MapInstance.Broadcast(
                                                        packet: session.GenerateRc(characterHealth: amount));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (IsLevelScaled)
                                                amount = senderLevel * (firstData - 1);
                                            else
                                                amount = firstData;

                                            amount *= -1;

                                            if (session.Hp - amount < 1) amount = session.Hp - 1;

                                            if (amount > 0)
                                            {
                                                amount = session.GetDamage(damage: amount, damager: sender, dontKill: true,
                                                    fromDebuff: true);

                                                session.MapInstance.Broadcast(packet: session.GenerateDm(dmg: amount));
                                            }
                                        }

                                        session?.Character?.Session?.SendPacket(packet: session.Character?.GenerateStat());
                                    }
                                    else if (SubType == (byte)AdditionalTypes.HealingBurningAndCasting.RestoreMp / 10
                                             || SubType == (byte)AdditionalTypes.HealingBurningAndCasting.DecreaseMp / 10)
                                    {
                                        if (firstData > 0)
                                        {
                                            if (IsLevelScaled)
                                                amount = senderLevel * firstData;
                                            else
                                                amount = firstData;

                                            if (session.Mp + amount > session.MpMax) amount = session.MpMax - session.Mp;

                                            session.Mp += amount;
                                        }
                                        else
                                        {
                                            if (IsLevelScaled)
                                                amount = senderLevel * (firstData - 1);
                                            else
                                                amount = firstData;

                                            amount *= -1;

                                            if (session.Mp - amount < 1) amount = session.Mp - 1;

                                            session.DecreaseMp(amount: amount);
                                        }

                                        session?.Character?.Session?.SendPacket(packet: session.Character?.GenerateStat());
                                    }
                                }

                                HealingBurningAndCastingAction();

                                var interval = ThirdData > 0 ? ThirdData * 2 : CastType * 2;

                                if (CardId != null && interval > 0)
                                {
                                    IDisposable bcardDisposable = null;
                                    bcardDisposable = Observable.Interval(period: TimeSpan.FromSeconds(value: interval))
                                        .Subscribe(onNext: s =>
                                        {
                                            if (session.BCardDisposables[key: BCardId] != bcardDisposable)
                                            {
                                                bcardDisposable.Dispose();
                                                return;
                                            }

                                            if (session != null) HealingBurningAndCastingAction();
                                        });
                                    session.BCardDisposables[key: BCardId] = bcardDisposable;
                                }
                            }
                            break;

                        case BCardType.CardType.Hpmp:
                            if (SubType == (byte)AdditionalTypes.Hpmp.DecreaseRemainingMp / 10)
                            {
                                if (firstData < 0)
                                {
                                    var bonus = (int)(session.Mp * -firstData / 100D);
                                    var change = false;
                                    if (session.Mp - bonus > 1)
                                    {
                                        session.DecreaseMp(amount: bonus);
                                        change = true;
                                    }
                                    else
                                    {
                                        if (session.Mp != 1)
                                        {
                                            bonus = session.Mp - 1;
                                            session.Mp = 1;
                                            change = true;
                                        }
                                    }

                                    if (change)
                                        session.Character?.Session?.SendPacket(
                                            packet: session.Character?.GenerateStat());
                                }
                            }
                            else
                            {
                                void HPMPAction()
                                {
                                    if (session.Hp > 0)
                                    {
                                        if (SubType == (byte)AdditionalTypes.Hpmp.HpRestored / 10
                                            || SubType == (byte)AdditionalTypes.Hpmp.HpReduced / 10)
                                        {
                                            var bonus = 0;
                                            var change = false;
                                            if (firstData > 0)
                                            {
                                                if (IsLevelScaled)
                                                    bonus = senderLevel * firstData;
                                                else
                                                    bonus = firstData;
                                                if (session.Hp + bonus <= session.HPLoad())
                                                {
                                                    session.Hp += bonus;
                                                    change = true;
                                                }
                                                else
                                                {
                                                    bonus = (int)session.HPLoad() - session.Hp;
                                                    session.Hp = (int)session.HPLoad();
                                                    change = true;
                                                }

                                                if (change)
                                                {
                                                    session.MapInstance?.Broadcast(
                                                        packet: session.GenerateRc(characterHealth: bonus));
                                                    session.Character?.Session?.SendPacket(packet: session.Character
                                                        ?.GenerateStat());
                                                }
                                            }
                                            else
                                            {
                                                if (IsLevelScaled)
                                                    bonus = senderLevel * (firstData + 1) * -1;
                                                else
                                                    bonus = firstData * -1;
                                                bonus = session.GetDamage(damage: bonus, damager: sender,
                                                    dontKill: true, fromDebuff: true);
                                                if (bonus > 0)
                                                {
                                                    session.MapInstance?.Broadcast(
                                                        packet: session.GenerateDm(dmg: bonus));
                                                    session.Character?.Session?.SendPacket(packet: session.Character
                                                        ?.GenerateStat());
                                                }
                                            }
                                        }

                                        if (SubType == (byte)AdditionalTypes.Hpmp.MpRestored / 10
                                            || SubType == (byte)AdditionalTypes.Hpmp.MpReduced / 10)
                                        {
                                            var bonus = 0;
                                            var change = false;
                                            if (firstData > 0)
                                            {
                                                if (IsLevelScaled)
                                                    bonus = senderLevel * firstData;
                                                else
                                                    bonus = firstData;
                                                if (session.Mp + bonus <= session.MPLoad())
                                                {
                                                    session.Mp += bonus;
                                                    change = true;
                                                }
                                                else
                                                {
                                                    bonus = (int)session.MPLoad() - session.Mp;
                                                    session.Mp = (int)session.MPLoad();
                                                    change = true;
                                                }

                                                if (change)
                                                    session.Character?.Session?.SendPacket(packet: session.Character
                                                        ?.GenerateStat());
                                            }
                                            else
                                            {
                                                if (IsLevelScaled)
                                                    bonus = senderLevel * (firstData + 1) * -1;
                                                else
                                                    bonus = firstData;
                                                if (session.Mp - bonus > 1)
                                                {
                                                    session.DecreaseMp(amount: bonus);
                                                    change = true;
                                                }
                                                else
                                                {
                                                    if (session.Mp != 1)
                                                    {
                                                        bonus = session.Mp - 1;
                                                        session.Mp = 1;
                                                        change = true;
                                                    }
                                                }

                                                if (change)
                                                    session.Character?.Session?.SendPacket(packet: session.Character
                                                        ?.GenerateStat());
                                            }
                                        }
                                    }
                                }

                                HPMPAction();
                                if (ThirdData > 0)
                                {
                                    IDisposable bcardDisposable = null;
                                    bcardDisposable = Observable
                                        .Interval(period: TimeSpan.FromSeconds(value: ThirdData * 2))
                                        .Subscribe(onNext: s =>
                                        {
                                            if (session.BCardDisposables[key: BCardId] != bcardDisposable)
                                            {
                                                bcardDisposable.Dispose();
                                                return;
                                            }

                                            if (session != null) HPMPAction();
                                        });
                                    session.BCardDisposables[key: BCardId] = bcardDisposable;
                                }
                            }

                            break;

                        case BCardType.CardType.SpecialisationBuffResistance:
                            if (SubType.Equals(
                                obj: (byte)AdditionalTypes.SpecialisationBuffResistance.RemoveBadEffects / 10))
                            {
                                if (FirstData > 0)
                                {
                                    if (ServerManager.RandomNumber() < FirstData)
                                        session.DisableBuffs(type: BuffType.Good, level: SecondData + 1);
                                }
                                else
                                {
                                    if (ServerManager.RandomNumber() < -FirstData &&
                                        sender.BCardDisposables[key: BCardId] == null)
                                        session.DisableBuffs(type: BuffType.Bad, level: SecondData + 1);
                                    if (session == sender && skill?.SkillVNum == 1098)
                                    {
                                        if (session.BCardDisposables[key: BCardId] != null)
                                        {
                                            session.BCardDisposables[key: BCardId].Dispose();
                                            session.BCardDisposables[key: BCardId] = null;
                                        }
                                        else
                                        {
                                            int count = 0, count2 = 0;
                                            IDisposable fiveSecs = null;
                                            fiveSecs = Observable.Interval(period: TimeSpan.FromSeconds(value: 2.5))
                                                .Subscribe(onNext: s =>
                                                {
                                                    count++;
                                                    RemoveBadEffectsAction(health: true);
                                                    if (count == 2) fiveSecs.Dispose();
                                                });
                                            IDisposable bcardDisposable = null;
                                            bcardDisposable = Observable
                                                .Interval(period: TimeSpan.FromSeconds(value: 2.5)).Subscribe(
                                                    onNext: s =>
                                                    {
                                                        if (session.BCardDisposables[key: BCardId] != bcardDisposable)
                                                        {
                                                            bcardDisposable.Dispose();
                                                            return;
                                                        }

                                                        if (!session.Character.Session.HasCurrentMapInstance)
                                                        {
                                                            session.BCardDisposables[key: BCardId].Dispose();
                                                            session.BCardDisposables[key: BCardId] = null;
                                                            return;
                                                        }

                                                        count2++;
                                                        if (count2 > 2)
                                                        {
                                                            var health = false;
                                                            if (session.Mp >= 144)
                                                            {
                                                                health = true;
                                                                session.DecreaseMp(amount: 144);
                                                            }

                                                            RemoveBadEffectsAction(health: health);
                                                        }
                                                    });
                                            session.BCardDisposables[key: BCardId] = bcardDisposable;

                                            void RemoveBadEffectsAction(bool health)
                                            {
                                                session.MapInstance
                                                    .GetBattleEntitiesInRange(
                                                        pos: new MapCell { X = session.PositionX, Y = session.PositionY },
                                                        distance: skill.TargetRange)
                                                    .Where(predicate: e => e.MapMonster == null && e.Hp > 0 &&
                                                                           !session.CanAttackEntity(receiver: e))
                                                    .ToList().ForEach(action: b =>
                                                    {
                                                        if (ServerManager.RandomNumber() < 25)
                                                            b.DisableBuffs(type: BuffType.Bad, level: SecondData + 1);
                                                        if (health)
                                                        {
                                                            var healthAmount = senderLevel * 2;
                                                            if (b.Hp + healthAmount > b.HpMax)
                                                                healthAmount = b.HpMax - b.Hp;
                                                            if (healthAmount > 0)
                                                            {
                                                                b.Hp += healthAmount;
                                                                b.MapInstance.Broadcast(
                                                                    packet: b.GenerateRc(
                                                                        characterHealth: healthAmount));
                                                                b.Character?.Session.SendPacket(
                                                                    packet: b.Character.GenerateStat());
                                                            }
                                                        }

                                                        b.MapInstance.Broadcast(
                                                            packet: StaticPacketHelper.GenerateEff(
                                                                effectType: b.UserType, callerId: b.MapEntityId,
                                                                effectId: 4354));
                                                    });
                                            }
                                        }
                                    }
                                }
                            }

                            break;

                        case BCardType.CardType.SpecialEffects:
                            if (SubType.Equals(obj: (byte)AdditionalTypes.SpecialEffects.ShadowAppears / 10))
                                session.MapInstance.Broadcast(
                                    packet:
                                    $"guri 0 {(short)session.UserType} {session.MapEntityId} {firstData} {SecondData}");
                            break;

                        case BCardType.CardType.Capture:
                            if (sender.Character?.Session is ClientSession senderSession)
                            {
                                if (session.MapMonster is MapMonster mapMonster)
                                    if (ServerManager.GetNpcMonster(npcVNum: mapMonster.MonsterVNum) is NpcMonster
                                        mateNpc)
                                    {
                                        if (mapMonster.Monster.Catch &&
                                            (senderSession.Character.MapInstance.MapInstanceType ==
                                             MapInstanceType.BaseMapInstance ||
                                             senderSession.Character.MapInstance.MapInstanceType ==
                                             MapInstanceType.TimeSpaceInstance))
                                        {
                                            if (mapMonster.Monster.Level < senderSession.Character.Level)
                                            {
                                                if (mapMonster.CurrentHp < mapMonster.Monster.MaxHp / 2)
                                                {
                                                    // Algo  
                                                    var capturerate =
                                                        100 - (int)((mapMonster.CurrentHp /
                                                            mapMonster.Monster.MaxHp + 1) * 100 / 3);
                                                    if (ServerManager.RandomNumber() <= capturerate)
                                                    {
                                                        if (senderSession.Character.Quests.Any(predicate: q =>
                                                            q.Quest.QuestType == (int)QuestType.Capture1 &&
                                                            q.Quest.QuestObjectives.Any(predicate: d =>
                                                                d.Data == mapMonster.MonsterVNum &&
                                                                q.GetObjectives()[d.ObjectiveIndex - 1] <
                                                                q.GetObjectiveByIndex(index: d.ObjectiveIndex)
                                                                    .Objective)))
                                                        {
                                                            senderSession.Character.IncrementQuests(
                                                                type: QuestType.Capture1,
                                                                firstData: mapMonster.MonsterVNum);
                                                            mapMonster.SetDeathStatement();
                                                            senderSession.CurrentMapInstance?.Broadcast(
                                                                packet: StaticPacketHelper.Out(type: UserType.Monster,
                                                                    callerId: mapMonster.MapMonsterId));
                                                        }
                                                        else
                                                        {
                                                            senderSession.Character.IncrementQuests(
                                                                type: QuestType.Capture2,
                                                                firstData: mapMonster.MonsterVNum);
                                                            var mate = new Mate(owner: senderSession.Character,
                                                                npcMonster: mateNpc,
                                                                level: mapMonster.Monster.Level,
                                                                matetype: MateType.Pet);
                                                            if (senderSession.Character.CanAddMate(mate: mate))
                                                            {
                                                                mate.RefreshStats();
                                                                senderSession.Character.AddPetWithSkill(mate: mate);
                                                                senderSession.SendPacket(
                                                                    packet: UserInterfaceHelper.GenerateMsg(
                                                                        message: Language.Instance.GetMessageFromKey(
                                                                            key: "CATCH_SUCCESS"), type: 0));
                                                                senderSession.CurrentMapInstance?.Broadcast(
                                                                    packet: StaticPacketHelper.GenerateEff(
                                                                        effectType: UserType.Player,
                                                                        callerId: senderSession.Character.CharacterId,
                                                                        effectId: 197));
                                                                senderSession.CurrentMapInstance?.Broadcast(
                                                                    packet: StaticPacketHelper.SkillUsed(
                                                                        type: UserType.Player,
                                                                        callerId: senderSession.Character.CharacterId,
                                                                        secondaryType: 3,
                                                                        targetId: mapMonster.MapMonsterId,
                                                                        skillVNum: -1, cooldown: 0, attackAnimation: 15,
                                                                        skillEffect: -1, x: -1, y: -1,
                                                                        isAlive: true,
                                                                        health: (int)((float)mapMonster.CurrentHp /
                                                                            (float)mapMonster.MaxHp * 100),
                                                                        damage: 0, hitmode: -1,
                                                                        skillType: 0));
                                                                mapMonster.SetDeathStatement();
                                                                senderSession.CurrentMapInstance?.Broadcast(
                                                                    packet: StaticPacketHelper.Out(
                                                                        type: UserType.Monster,
                                                                        callerId: mapMonster.MapMonsterId));
                                                            }
                                                            else
                                                            {
                                                                senderSession.SendPacket(
                                                                    packet: senderSession.Character.GenerateSay(
                                                                        message: Language.Instance.GetMessageFromKey(
                                                                            key: "PET_SLOT_FULL"), type: 10));
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        senderSession.SendPacket(
                                                            packet: UserInterfaceHelper.GenerateMsg(
                                                                message: Language.Instance.GetMessageFromKey(
                                                                    key: "CATCH_FAIL"), type: 0));
                                                    }
                                                }
                                                else
                                                {
                                                    senderSession.SendPacket(
                                                        packet: UserInterfaceHelper.GenerateMsg(
                                                            message: Language.Instance.GetMessageFromKey(
                                                                key: "CURRENT_HP_TOO_HIGH"),
                                                            type: 0));
                                                }
                                            }
                                            else
                                            {
                                                senderSession.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                    message: Language.Instance.GetMessageFromKey(
                                                        key: "LEVEL_LOWER_THAN_MONSTER"),
                                                    type: 0));
                                            }
                                        }
                                        else
                                        {
                                            senderSession.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(
                                                    key: "MONSTER_CANT_BE_CAPTURED"), type: 0));
                                        }
                                    }

                                //senderSession.CurrentMapInstance?.Broadcast(StaticPacketHelper.SkillUsed(UserType.Player, senderSession.Character.CharacterId, 3, session.MapEntityId, -1, 0, 15, -1, -1, -1, true, (int)((float)session.Hp / (float)session.HpMax * 100), 0, -1, 0));
                                senderSession.SendPacket(
                                    packet: StaticPacketHelper.Cancel(type: 2, callerId: session.MapEntityId));
                            }

                            break;

                        case BCardType.CardType.SpecialDamageAndExplosions:
                            break;

                        case BCardType.CardType.SpecialEffects2:
                            if (SubType.Equals(obj: (byte)AdditionalTypes.SpecialEffects2.TeleportInRadius / 10))
                                if (session.Character != null && session.MapEntityId == sender.MapEntityId)
                                    session.Character.TeleportToDir(Dir: session.Character.Direction,
                                        Distance: firstData);
                            break;

                        case BCardType.CardType.CalculatingLevel:
                            break;

                        case BCardType.CardType.Recovery:
                            break;

                        case BCardType.CardType.MaxHpmp:
                            break;

                        case BCardType.CardType.MultAttack:
                            break;

                        case BCardType.CardType.MultDefence:
                            break;

                        case BCardType.CardType.TimeCircleSkills:
                            break;

                        case BCardType.CardType.RecoveryAndDamagePercent:
                            if (session.HasBuff(type: BCardType.CardType.RecoveryAndDamagePercent, subtype: 01)) return;

                            void RecoveryAndDamagePercentAction()
                            {
                                if (session.Hp > 0)
                                {
                                    var bonus = 0;
                                    var change = false;
                                    if (SubType == (byte)AdditionalTypes.RecoveryAndDamagePercent.HpRecovered / 10
                                        || SubType == (byte)AdditionalTypes.RecoveryAndDamagePercent.HpReduced / 10)
                                    {
                                        if (IsLevelDivided)
                                            bonus = (int)(senderLevel / firstData * (session.HPLoad() / 100));
                                        else
                                            bonus = (int)(firstData * (session.HPLoad() / 100));
                                        if (bonus > 0)
                                        {
                                            if (session.Hp + bonus < session.HPLoad())
                                            {
                                                session.Hp += bonus;
                                                change = true;
                                            }
                                            else
                                            {
                                                if (session.Hp != (int)session.HPLoad())
                                                {
                                                    bonus = (int)session.HPLoad() - session.Hp;
                                                    session.Hp = (int)session.HPLoad();
                                                    change = true;
                                                }
                                            }

                                            if (change)
                                            {
                                                session.MapInstance?.Broadcast(
                                                    packet: session.GenerateRc(characterHealth: bonus));
                                                session.Character?.Session?.SendPacket(
                                                    packet: session.Character?.GenerateStat());
                                            }
                                        }

                                        if (bonus <= 0)
                                        {
                                            bonus *= -1;
                                            bonus = session.GetDamage(damage: bonus, damager: sender, dontKill: true,
                                                fromDebuff: true);
                                            if (bonus > 0)
                                            {
                                                session.MapInstance?.Broadcast(packet: session.GenerateDm(dmg: bonus));
                                                session.Character?.Session?.SendPacket(
                                                    packet: session.Character?.GenerateStat());
                                            }
                                        }
                                    }

                                    if (SubType == (byte)AdditionalTypes.RecoveryAndDamagePercent.MpRecovered / 10
                                        || SubType == (byte)AdditionalTypes.RecoveryAndDamagePercent.MpReduced / 10)
                                    {
                                        if (IsLevelDivided)
                                            bonus = (int)(senderLevel / firstData * (session.MPLoad() / 100));
                                        else
                                            bonus = (int)(firstData * (session.MPLoad() / 100));
                                        if (bonus > 0)
                                        {
                                            if (session.Mp + bonus < session.MPLoad())
                                            {
                                                session.Mp += bonus;
                                                change = true;
                                            }
                                            else
                                            {
                                                if (session.Mp != (int)session.MPLoad())
                                                {
                                                    bonus = (int)session.MPLoad() - session.Mp;
                                                    session.Mp = (int)session.MPLoad();
                                                    change = true;
                                                }
                                            }

                                            if (change)
                                                session.Character?.Session?.SendPacket(
                                                    packet: session.Character?.GenerateStat());
                                        }

                                        if (bonus <= 0)
                                        {
                                            bonus *= -1;
                                            if (session.Mp - bonus > 1)
                                            {
                                                session.DecreaseMp(amount: bonus);
                                                change = true;
                                            }
                                            else
                                            {
                                                if (session.Mp != 1)
                                                {
                                                    bonus = session.Mp - 1;
                                                    session.Mp = 1;
                                                    change = true;
                                                }
                                            }

                                            if (change)
                                                session.Character?.Session?.SendPacket(
                                                    packet: session.Character?.GenerateStat());
                                        }
                                    }
                                }
                            }

                            if (ThirdData > 0 && CastType == 0)
                            {
                                RecoveryAndDamagePercentAction();
                                IDisposable bcardDisposable = null;
                                bcardDisposable = Observable
                                    .Interval(period: TimeSpan.FromSeconds(value: ThirdData * 2))
                                    .Subscribe(onNext: s =>
                                    {
                                        if (session.BCardDisposables[key: BCardId] != bcardDisposable)
                                        {
                                            bcardDisposable.Dispose();
                                            return;
                                        }

                                        if (session != null) RecoveryAndDamagePercentAction();
                                    });
                                session.BCardDisposables[key: BCardId] = bcardDisposable;
                            }

                            break;

                        case BCardType.CardType.Count:
                            break;

                        case BCardType.CardType.NoDefeatAndNoDamage:
                            break;

                        case BCardType.CardType.SpecialActions:
                            if (SubType.Equals(obj: (byte)AdditionalTypes.SpecialActions.PushBack / 10))
                            {
                                if (session.ResistForcedMovement <= 0
                                    || ServerManager.RandomNumber() < session.ResistForcedMovement)
                                    PushBackSession(PushDistance: firstData, receiver: session, point: sender);
                            }
                            else if (SubType.Equals(obj: (byte)AdditionalTypes.SpecialActions.Hide / 10))
                            {
                                if (FirstData >= 0)
                                    if (session.Character is Character charact)
                                    {
                                        if (charact.MapInstance.MapInstanceType != MapInstanceType.NormalInstance ||
                                            charact.MapInstance.Map.MapId != 2004)
                                        {
                                            charact.Invisible = true;
                                            charact.Mates.Where(predicate: s => s.IsTeamMember).ToList().ForEach(
                                                action: s =>
                                                    charact.Session.CurrentMapInstance?.Broadcast(
                                                        packet: s.GenerateOut()));
                                            charact.Session.CurrentMapInstance?.Broadcast(
                                                packet: charact.GenerateInvisible());
                                        }
                                        else if (card != null)
                                        {
                                            charact.RemoveBuff(cardId: card.CardId);
                                        }
                                    }
                            }
                            else if (SubType.Equals(obj: (byte)AdditionalTypes.SpecialActions.FocusEnemies / 10))
                            {
                                if (session.ResistForcedMovement <= 0
                                    || ServerManager.RandomNumber() < session.ResistForcedMovement)
                                    if ((session.MapMonster == null ||
                                         !session.MapMonster.IsBoss &&
                                         !ServerManager.Instance.BossVNums.Contains(
                                             item: session.MapMonster.MonsterVNum) ||
                                         session.MapMonster.Owner?.Character != null ||
                                         session.MapMonster.Owner?.Mate != null)
                                        && (session.MapNpc == null ||
                                            !ServerManager.Instance.BossVNums.Contains(item: session.MapNpc.NpcVNum)))
                                        Observable.Timer(
                                            dueTime: TimeSpan.FromMilliseconds(value: skill.CastTime * 100)).Subscribe(
                                            onNext: s =>
                                            {
                                                if (!session.MapInstance.Map.IsBlockedZone(firstX: session.PositionX,
                                                    firstY: session.PositionY, mapX: sender.PositionX,
                                                    mapY: sender.PositionY))
                                                {
                                                    session.PositionX = sender.PositionX;
                                                    session.PositionY = sender.PositionY;
                                                    session.MapInstance.Broadcast(
                                                        packet:
                                                        $"guri 3 {(short)session.UserType} {session.MapEntityId} {session.PositionX} {session.PositionY} 3 {SecondData} 2 -1");
                                                }
                                            });
                            }
                            else if (SubType.Equals(obj: (byte)AdditionalTypes.SpecialActions.RunAway / 10))
                            {
                                if (session.MapMonster != null && session.MapMonster.IsMoving &&
                                    !session.MapMonster.IsBoss &&
                                    !ServerManager.Instance.BossVNums.Contains(item: session.MapMonster.MonsterVNum) &&
                                    session.MapMonster.Owner?.Character == null &&
                                    session.MapMonster.Owner?.Mate == null)
                                    if (session.MapMonster.Target != null)
                                    {
                                        (session.MapMonster.Path ?? (session.MapMonster.Path = new List<Node>()))
                                            .Clear();
                                        session.MapMonster.Target = null;

                                        var RunToX = session.MapMonster.FirstX;
                                        var RunToY = session.MapMonster.FirstY;

                                        var RunToPos =
                                            session.MapInstance.Map.GetRandomPositionByDistance(xPos: session.PositionX,
                                                yPos: session.PositionY, distance: 20);
                                        if (RunToPos != null)
                                        {
                                            RunToX = RunToPos.X;
                                            RunToY = RunToPos.Y;
                                        }

                                        /*session.MapMonster.Path = BestFirstSearch.FindPathJagged(new Node { X = session.MapMonster.MapX, Y = session.MapMonster.MapY }, new Node { X = RunToX, Y = RunToY },
                                            session.MapMonster.MapInstance.Map.JaggedGrid);*/
                                        session.MapMonster.RunToX = RunToX;
                                        session.MapMonster.RunToY = RunToY;
                                        Observable.Timer(
                                                dueTime: TimeSpan.FromMilliseconds(
                                                    value: card != null ? card.Duration * 100 : 10000))
                                            .Subscribe(onNext: s =>
                                            {
                                                session.MapMonster.RunToX = 0;
                                                session.MapMonster.RunToY = 0;
                                            });
                                    }
                            }

                            break;

                        case BCardType.CardType.Mode:
                            break;

                        case BCardType.CardType.NoCharacteristicValue:
                            break;

                        case BCardType.CardType.LightAndShadow:
                            if (SubType == (byte)AdditionalTypes.LightAndShadow.RemoveBadEffects / 10)
                                session.Buffs.Where(predicate: b =>
                                        b.Card.BuffType == BuffType.Bad && b.Card.Level <= firstData)
                                    .ForEach(action: s => session.RemoveBuff(id: s.Card.CardId));
                            break;

                        case BCardType.CardType.Item:
                            break;

                        case BCardType.CardType.DebuffResistance:
                            break;

                        case BCardType.CardType.SpecialBehaviour:
                            if (SubType == (byte)AdditionalTypes.SpecialBehaviour.TeleportRandom / 10)
                            {
                                if (sender.Character != null)
                                {
                                    var randomCell = session.MapInstance.Map.GetRandomPosition();
                                    session.PositionX = randomCell.X;
                                    session.PositionY = randomCell.Y;
                                    session.MapInstance.Broadcast(packet: session.GenerateTp());
                                }
                                else
                                {
                                    MapCell randomCell = null;

                                    if (SecondData > 0)
                                        randomCell =
                                            sender.MapInstance.Map.GetRandomPositionByDistance(xPos: sender.PositionX,
                                                yPos: sender.PositionY, distance: (short)SecondData,
                                                randomInRange: true);
                                    else
                                        randomCell = sender.MapInstance.Map.GetRandomPosition();

                                    sender.PositionX = randomCell.X;
                                    sender.PositionY = randomCell.Y;
                                    sender.MapInstance.Broadcast(packet: sender.GenerateTp());
                                }
                            }
                            else if (SubType == (byte)AdditionalTypes.SpecialBehaviour.InflictOnTeam / 10)
                            {
                                if (CardId != null && CardId.Value == SecondData) // Checked on official server
                                    return;

                                void InflictOnTeamAction()
                                {
                                    if (session.Hp < 1
                                        || session.MapInstance == null)
                                        return;

                                    session.MapInstance
                                        .GetBattleEntitiesInRange(
                                            pos: new MapCell { X = session.PositionX, Y = session.PositionY },
                                            distance: (byte)FirstData)
                                        .Where(predicate: s => s.EntityType != EntityType.Npc &&
                                                               (s.EntityType != session.EntityType ||
                                                                s.MapEntityId != session.MapEntityId) &&
                                                               !session.CanAttackEntity(receiver: s)).ToList()
                                        .ForEach(action: s =>
                                            s.AddBuff(
                                                indicator: new Buff.Buff(id: (short)SecondData, level: senderLevel),
                                                sender: sender));
                                }

                                if (ThirdData > 0)
                                {
                                    IDisposable bcardDisposable = null;
                                    bcardDisposable = Observable
                                        .Interval(period: TimeSpan.FromSeconds(value: ThirdData * 2))
                                        .Subscribe(onNext: s =>
                                        {
                                            if (session.BCardDisposables[key: BCardId] != bcardDisposable)
                                            {
                                                bcardDisposable.Dispose();
                                                return;
                                            }

                                            InflictOnTeamAction();
                                        });
                                    session.BCardDisposables[key: BCardId] = bcardDisposable;
                                }
                            }

                            break;

                        case BCardType.CardType.Quest:
                            break;

                        case BCardType.CardType.SecondSpCard:
                            var summonParameters2 = new List<MonsterToSummon>();
                            if (session.Character != null)
                            {
                                aliveTime = ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime /
                                    (ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime < 2400
                                        ? ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime <
                                          150
                                            ? 1
                                            : 10
                                        : 40) * (ServerManager.GetNpcMonster(npcVNum: (short)SecondData)
                                                     .RespawnTime >=
                                                 150
                                        ? 4
                                        : 1);
                                for (var i = 0; i < firstData; i++)
                                {
                                    x = SecondData == 945
                                        ? session.PositionX
                                        : (short)(ServerManager.RandomNumber(min: -1, max: 1) + session.PositionX);
                                    y = SecondData == 945
                                        ? session.PositionY
                                        : (short)(ServerManager.RandomNumber(min: -1, max: 1) + session.PositionY);
                                    if (session.MapInstance.Map.IsBlockedZone(x: x, y: y))
                                    {
                                        x = session.PositionX;
                                        y = session.PositionY;
                                    }

                                    summonParameters2.Add(item: new MonsterToSummon(vnum: (short)SecondData,
                                        spawnCell: new MapCell { X = x, Y = y }, target: null, move: true,
                                        isHostile: SecondData != 945,
                                        owner: session, aliveTime: aliveTime));
                                }

                                if (ServerManager.RandomNumber() <= Math.Abs(value: ThirdData) || ThirdData == 0 ||
                                    ThirdData < 0)
                                    switch (SubType)
                                    {
                                        case (byte)AdditionalTypes.SecondSpCard.PlantBomb / 10:
                                            {
                                                if (session.MapInstance.Monsters.Any(predicate: s =>
                                                    s.Owner != null && s.Owner.MapEntityId == session.MapEntityId &&
                                                    s.MonsterVNum == SecondData))
                                                {
                                                    //Attack
                                                    session.MapInstance.Monsters
                                                        .Where(predicate: s =>
                                                            s.Owner?.MapEntityId == session.MapEntityId &&
                                                            s.MonsterVNum == SecondData).ToList().ForEach(action: s =>
                                                        {
                                                            s.Target = s.BattleEntity;
                                                        });

                                                    short NewCooldown = 300;
                                                    Observable.Timer(
                                                            dueTime: TimeSpan.FromMilliseconds(
                                                                value: skill.Cooldown * 100 + 50))
                                                        .Subscribe(onNext: s =>
                                                        {
                                                            session.Character.Session.CurrentMapInstance.Broadcast(
                                                                packet: StaticPacketHelper.SkillUsed(type: UserType.Player,
                                                                    callerId: session.Character.CharacterId,
                                                                    secondaryType: 1,
                                                                    targetId: session.Character.CharacterId,
                                                                    skillVNum: skill.SkillVNum,
                                                                    cooldown: NewCooldown, attackAnimation: 2,
                                                                    skillEffect: skill.Effect,
                                                                    x: session.Character.PositionX,
                                                                    y: session.Character.PositionY, isAlive: true,
                                                                    health: (int)(session.Character.Hp /
                                                                        session.Character.HPLoad() * 100),
                                                                    damage: 0, hitmode: -1,
                                                                    skillType: (byte)(skill.SkillType - 1)));
                                                        });
                                                    var EndCooldownTime =
                                                        DateTime.Now.AddMilliseconds(value: NewCooldown * 100);
                                                    session.Character.SkillsSp
                                                        .FirstOrDefault(predicate: s => s.SkillVNum == SkillVNum)
                                                        .LastUse = EndCooldownTime;
                                                    Observable.Timer(dueTime: EndCooldownTime).Subscribe(onNext: s =>
                                                    {
                                                        if (session.Character.SkillsSp != null &&
                                                            session.Character.SkillsSp.FirstOrDefault(predicate: c =>
                                                                c.SkillVNum == SkillVNum) is CharacterSkill cSkill)
                                                            if (cSkill.LastUse <= EndCooldownTime)
                                                                session.Character.Session.SendPacket(
                                                                    packet: StaticPacketHelper.SkillReset(
                                                                        castId: skill.CastId));
                                                    });
                                                }
                                                else
                                                {
                                                    EventHelper.Instance.RunEvent(evt: new EventContainer(
                                                        mapInstance: session.MapInstance,
                                                        eventActionType: EventActionType.Spawnmonsters,
                                                        param: summonParameters2));
                                                }
                                            }
                                            break;
                                        case (byte)AdditionalTypes.SecondSpCard.PlantSelfDestructionBomb / 10:
                                            {
                                                EventHelper.Instance.RunEvent(evt: new EventContainer(
                                                    mapInstance: session.MapInstance,
                                                    eventActionType: EventActionType.Spawnmonsters,
                                                    param: summonParameters2));
                                            }
                                            break;
                                    }

                                session.Character.Session.SendPacket(packet: session.Character.GenerateStat());
                            }

                            break;

                        case BCardType.CardType.SpCardUpgrade:
                            break;

                        case BCardType.CardType.HugeSnowman:
                            if (SubType == (byte)AdditionalTypes.HugeSnowman.SnowStorm / 10)
                                if (sender.CanAttackEntity(receiver: session))
                                {
                                    session.GetDamage(damage: (int)(session.HpMax * 0.5D), damager: sender,
                                        dontKill: false, fromDebuff: true);
                                    session.Character?.Session.SendPacket(packet: session.Character.GenerateStat());
                                    session.Mate?.Owner.Session.SendPacket(packet: session.Mate.GenerateStatInfo());
                                    if (session.Hp <= 0)
                                    {
                                        session.MapInstance.Broadcast(packet: StaticPacketHelper.Die(
                                            callerType: session.UserType,
                                            callerId: session.MapEntityId, targetType: session.UserType,
                                            targetId: session.MapEntityId));
                                        if (session.Character != null)
                                            Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 1000))
                                                .Subscribe(onNext: obs =>
                                                {
                                                    ServerManager.Instance.AskRevive(
                                                        characterId: session.Character.CharacterId);
                                                });
                                    }
                                }

                            break;

                        case BCardType.CardType.Drain:
                            if (session.HasBuff(type: BCardType.CardType.RecoveryAndDamagePercent, subtype: 01)) return;

                            void DrainAction()
                            {
                                if (session.Hp > 0 && sender.Hp > 0)
                                    if (SubType == (byte)AdditionalTypes.Drain.TransferEnemyHp / 10)
                                    {
                                        var bonus = 0;
                                        var senderChange = false;
                                        if (firstData > 0)
                                        {
                                            if (IsLevelScaled)
                                                bonus = senderLevel * firstData;
                                            else
                                                bonus = firstData;
                                            bonus = session.GetDamage(damage: bonus, damager: sender, dontKill: true,
                                                fromDebuff: true);
                                            if (bonus > 0)
                                            {
                                                session.MapInstance?.Broadcast(packet: session.GenerateDm(dmg: bonus));
                                                session.Character?.Session?.SendPacket(
                                                    packet: session.Character?.GenerateStat());

                                                if (sender.Hp + bonus <= sender.HPLoad())
                                                {
                                                    sender.Hp += bonus;
                                                    senderChange = true;
                                                }
                                                else
                                                {
                                                    bonus = (int)sender.HPLoad() - sender.Hp;
                                                    sender.Hp = (int)sender.HPLoad();
                                                    senderChange = true;
                                                }

                                                if (senderChange)
                                                {
                                                    sender.MapInstance?.Broadcast(
                                                        packet: sender.GenerateRc(characterHealth: bonus));
                                                    sender.Character?.Session?.SendPacket(
                                                        packet: sender.Character?.GenerateStat());
                                                }
                                            }
                                        }
                                    }
                            }

                            DrainAction();
                            if (ThirdData > 0)
                            {
                                IDisposable bcardDisposable = null;
                                bcardDisposable = Observable
                                    .Interval(period: TimeSpan.FromSeconds(value: ThirdData * 2))
                                    .Subscribe(onNext: s =>
                                    {
                                        if (session.BCardDisposables[key: BCardId] != bcardDisposable)
                                        {
                                            bcardDisposable.Dispose();
                                            return;
                                        }

                                        if (session != null) DrainAction();
                                    });
                                session.BCardDisposables[key: BCardId] = bcardDisposable;
                            }

                            break;

                        case BCardType.CardType.BossMonstersSkill:
                            break;

                        case BCardType.CardType.LordHatus:
                            break;

                        case BCardType.CardType.LordCalvinas:
                            break;

                        case BCardType.CardType.SeSpecialist:
                            if (SubType.Equals(obj: (byte)AdditionalTypes.SeSpecialist.LowerHpStrongerEffect / 10))
                            {
                                var hpPercentage = session.Hp / session.HPLoad() * 100;
                                if (hpPercentage < 35)
                                    session.AddBuff(indicator: new Buff.Buff(id: 274, level: senderLevel),
                                        sender: sender);
                                else if (hpPercentage < 67)
                                    session.AddBuff(indicator: new Buff.Buff(id: 273, level: senderLevel),
                                        sender: sender);
                                else
                                    session.AddBuff(indicator: new Buff.Buff(id: 272, level: senderLevel),
                                        sender: sender);
                            }

                            break;

                        case BCardType.CardType.FourthGlacernonFamilyRaid:
                            break;

                        case BCardType.CardType.SummonedMonsterAttack:
                            break;

                        case BCardType.CardType.BearSpirit:
                            break;

                        case BCardType.CardType.SummonSkill:
                            if (SubType.Equals(obj: (byte)AdditionalTypes.SummonSkill.Summon12 / 10) ||
                                SubType.Equals(obj: (byte)AdditionalTypes.SummonSkill.Summon10 / 10))
                            {
                                var amount = 0;

                                if (SubType.Equals(obj: (byte)AdditionalTypes.SummonSkill.Summon12 / 10)) amount = 12;

                                if (SubType.Equals(obj: (byte)AdditionalTypes.SummonSkill.Summon10 / 10)) amount = 10;

                                if (ServerManager.RandomNumber() < firstData)
                                {
                                    aliveTime = ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime /
                                                (ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime <
                                                 2400
                                                    ? ServerManager.GetNpcMonster(npcVNum: (short)SecondData)
                                                          .RespawnTime <
                                                      150 ? 1 : 10
                                                    : 40) *
                                                (ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime >=
                                                 150
                                                    ? 4
                                                    : 1);
                                    var canMove = SecondData != 797 && SecondData != 798 && SecondData != 2013 &&
                                                  SecondData != 2016;
                                    var monstersToSummon = new List<MonsterToSummon>();
                                    for (var i = 0; i < (amount > 0 ? amount : 1); i++)
                                    {
                                        x = (short)(ServerManager.RandomNumber(min: -1, max: 1) + sender.PositionX);
                                        y = (short)(ServerManager.RandomNumber(min: -1, max: 1) + sender.PositionY);

                                        if (skill != null && sender.Character == null)
                                        {
                                            var randomCell =
                                                sender.MapInstance.Map.GetRandomPositionByDistance(
                                                    xPos: sender.PositionX,
                                                    yPos: sender.PositionY, distance: skill.Range, randomInRange: true);

                                            if (randomCell != null)
                                            {
                                                x = randomCell.X;
                                                y = randomCell.Y;
                                            }
                                        }

                                        monstersToSummon.Add(item: new MonsterToSummon(vnum: (short)SecondData,
                                            spawnCell: new MapCell { X = x, Y = y }, target: null, move: canMove,
                                            aliveTimeMp: aliveTime,
                                            owner: sender));
                                    }

                                    EventHelper.Instance.RunEvent(evt: new EventContainer(
                                        mapInstance: sender.MapInstance,
                                        eventActionType: EventActionType.Spawnmonsters, param: monstersToSummon));
                                }
                            }

                            break;

                        case BCardType.CardType.InflictSkill:
                            break;

                        case BCardType.CardType.HideBarrelSkill:
                            break;

                        case BCardType.CardType.FocusEnemyAttentionSkill:
                            break;

                        case BCardType.CardType.TauntSkill:
                            if (SubType.Equals(obj: (byte)AdditionalTypes.TauntSkill.TauntWhenKnockdown / 10) ||
                                SubType.Equals(obj: (byte)AdditionalTypes.TauntSkill.TauntWhenNormal / 10))
                            {
                                if (FirstData > 0)
                                {
                                    if (session.Buffs.Any(predicate: s => s.Card.CardId == 500) &&
                                        ServerManager.RandomNumber() < FirstData)
                                        session.AddBuff(
                                            indicator: new Buff.Buff(id: (short)SecondData, level: senderLevel),
                                            sender: sender);
                                }
                                else
                                {
                                    if (!session.Buffs.Any(predicate: s => s.Card.CardId == 500) &&
                                        ServerManager.RandomNumber() < -FirstData)
                                        session.AddBuff(
                                            indicator: new Buff.Buff(id: (short)SecondData, level: senderLevel),
                                            sender: sender);
                                }
                            }

                            break;

                        case BCardType.CardType.FireCannoneerRangeBuff:
                            break;

                        case BCardType.CardType.VulcanoElementBuff:
                            break;

                        case BCardType.CardType.DamageConvertingSkill:
                            if (SubType.Equals(
                                obj: (byte)AdditionalTypes.DamageConvertingSkill.TransferInflictedDamage / 10))
                                if (sender.Character != null)
                                {
                                    sender.Character.Session.SendPacket(packet: UserInterfaceHelper.GenerateMsg(
                                        message: string.Format(
                                            format: Language.Instance.GetMessageFromKey(key: "SACRIFICES_THEMSELVES"),
                                            arg0: sender.Character.Name,
                                            arg1: session.Character?.Name ?? session.Mate?.Name ??
                                            session.MapNpc?.Name ?? session.MapMonster?.Name), type: 3));
                                    session.Character?.Session.SendPacket(
                                        packet: UserInterfaceHelper.GenerateMsg(
                                            message: string.Format(
                                                format: Language.Instance.GetMessageFromKey(
                                                    key: "SACRIFICES_THEMSELVES"),
                                                arg0: sender.Character.Name, arg1: session.Character.Name), type: 3));

                                    IDisposable bcardDisposable = null;
                                    bcardDisposable = Observable
                                        .Interval(period: TimeSpan.FromMilliseconds(value: 1000)).Subscribe(
                                            onNext: s =>
                                            {
                                                if (session.BCardDisposables[key: BCardId] != bcardDisposable)
                                                {
                                                    bcardDisposable.Dispose();
                                                    return;
                                                }

                                                if (!sender.Character.Session.HasCurrentMapInstance ||
                                                    Map.GetDistance(
                                                        p: new MapCell { X = session.PositionX, Y = session.PositionY },
                                                        q: new MapCell { X = sender.PositionX, Y = sender.PositionY }) >
                                                    10 ||
                                                    session.MapInstance != sender.MapInstance ||
                                                    !sender.Buffs.Any(predicate: c => c.Card.CardId == 546))
                                                {
                                                    sender.Character.Session.SendPacket(
                                                        packet: UserInterfaceHelper.GenerateMsg(
                                                            message: string.Format(
                                                                format: Language.Instance.GetMessageFromKey(
                                                                    key: "SACRIFICE_OUT_OF_RANGE")), type: 3));
                                                    session.Character?.Session.SendPacket(
                                                        packet: UserInterfaceHelper.GenerateMsg(
                                                            message: string.Format(
                                                                format: Language.Instance.GetMessageFromKey(
                                                                    key: "SACRIFICE_OUT_OF_RANGE")), type: 3));
                                                    session.ClearSacrificeBuff();
                                                    session.BCardDisposables[key: BCardId].Dispose();
                                                }
                                            });
                                    session.BCardDisposables[key: BCardId] = bcardDisposable;
                                }

                            break;

                        case BCardType.CardType.MeditationSkill:
                            {
                                if (sender.Character is Character character)
                                {
                                    if (SubType.Equals(obj: (byte)AdditionalTypes.MeditationSkill.Sacrifice / 10))
                                    {
                                        session.AddBuff(
                                            indicator: new Buff.Buff(id: (short)SecondData, level: senderLevel),
                                            sender: sender);
                                    }
                                    else if (character.LastSkillComboUse < DateTime.Now)
                                    {
                                        ItemInstance sp = null;

                                        if (character.Inventory != null)
                                            sp = character.Inventory.LoadBySlotAndType(slot: (byte)EquipmentType.Sp,
                                                type: InventoryType.Wear);

                                        if (sp != null)
                                        {
                                            var newSkillVNum = (short)SecondData;

                                            if (SkillVNum.HasValue
                                                && SubType.Equals(
                                                    obj: (byte)AdditionalTypes.MeditationSkill.CausingChance / 10)
                                                && ServerManager.RandomNumber() < firstData)
                                            {
                                                if (SkillHelper.IsCausingChance(skillVNum: SkillVNum.Value)
                                                    && session != sender)
                                                    return;

                                                var oldSkill = character.GetSkill(skillVNum: SkillVNum.Value);
                                                var newSkill = character.GetSkill(skillVNum: newSkillVNum);

                                                if (oldSkill?.Skill != null && newSkill?.Skill != null)
                                                {
                                                    newSkill.FirstCastId = oldSkill.FirstCastId;

                                                    if (newSkillVNum == 1126 && character.SkillComboCount > 4
                                                        || newSkillVNum == 1140 && character.SkillComboCount > 8)
                                                    {
                                                        character.SkillComboCount = 0;
                                                        character.LastSkillComboUse = DateTime.Now.AddSeconds(value: 3);
                                                    }
                                                    else
                                                    {
                                                        character.SkillComboCount++;
                                                        character.LastSkillComboUse = DateTime.Now;

                                                        Observable.Timer(dueTime: TimeSpan.FromMilliseconds(value: 100))
                                                            .Subscribe(
                                                                onNext: observer =>
                                                                {
                                                                    character.Session.SendPacket(
                                                                        packet: $"mslot {newSkill.Skill.CastId} -1");

                                                                    var quicklistEntries = character.QuicklistEntries.Where(
                                                                        predicate: s =>
                                                                            s.Morph == sp.Item.Morph &&
                                                                            s.Pos.Equals(obj: oldSkill.FirstCastId.Value));

                                                                    foreach (var quicklistEntry in quicklistEntries)
                                                                        character.Session.SendPacket(
                                                                            packet:
                                                                            $"qset {quicklistEntry.Q1} {quicklistEntry.Q2} {quicklistEntry.Type}.{quicklistEntry.Slot}.{newSkill.Skill.CastId}.0");
                                                                });

                                                        if (oldSkill.Skill.CastId > 10)
                                                            Observable.Timer(
                                                                dueTime: TimeSpan.FromMilliseconds(
                                                                    value: oldSkill.Skill.Cooldown * 100 + 500)).Subscribe(
                                                                onNext: observer =>
                                                                {
                                                                    if (character.SkillsSp != null &&
                                                                        character.SkillsSp.Any(predicate: s =>
                                                                            s.Skill.SkillVNum == oldSkill.SkillVNum &&
                                                                            s.CanBeUsed()))
                                                                        character.Session.SendPacket(
                                                                            packet: StaticPacketHelper.SkillReset(
                                                                                castId: oldSkill.Skill
                                                                                    .CastId));
                                                                });
                                                    }
                                                }
                                            }

                                            if (character.MeditationDictionary != null)
                                                lock (character.MeditationDictionary)
                                                {
                                                    switch (SubType)
                                                    {
                                                        case 2:
                                                            character.MeditationDictionary[key: newSkillVNum] =
                                                                DateTime.Now.AddSeconds(value: 4);
                                                            break;

                                                        case 3:
                                                            character.MeditationDictionary[key: newSkillVNum] =
                                                                DateTime.Now.AddSeconds(value: 8);
                                                            break;

                                                        case 4:
                                                            character.MeditationDictionary[key: newSkillVNum] =
                                                                DateTime.Now.AddSeconds(value: 12);
                                                            break;
                                                    }
                                                }
                                        }
                                    }
                                }
                            }
                            break;

                        case BCardType.CardType.FalconSkill:
                            if (SubType.Equals(obj: (byte)AdditionalTypes.FalconSkill.Hide / 10) ||
                                SubType.Equals(obj: (byte)AdditionalTypes.FalconSkill.Ambush / 10))
                            {
                                if (session.Character is Character chara)
                                {
                                    if (chara.MapInstance.MapInstanceType != MapInstanceType.NormalInstance ||
                                        chara.MapInstance.Map.MapId != 2004)
                                    {
                                        if (x != 0 || y != 0)
                                        {
                                            chara.PositionX = x;
                                            chara.PositionY = y;
                                            chara.Session.CurrentMapInstance.Broadcast(packet: chara.GenerateTp());
                                        }

                                        chara.Invisible = true;
                                        chara.Mates.Where(predicate: s => s.IsTeamMember).ToList().ForEach(action: s =>
                                            chara.Session.CurrentMapInstance?.Broadcast(packet: s.GenerateOut()));
                                        chara.Session.CurrentMapInstance?.Broadcast(packet: chara.GenerateInvisible());
                                    }
                                    else if (card != null)
                                    {
                                        chara.RemoveBuff(cardId: card.CardId);
                                    }
                                }
                            }
                            else if (SubType.Equals(obj: (byte)AdditionalTypes.FalconSkill.CausingChanceLocation / 10))
                            {
                                if (session.Character is Character chara)
                                    if (chara.Session.CurrentMapInstance != null)
                                    {
                                        var ownedMonsters = new ConcurrentBag<MapMonster>();
                                        ServerManager.GetAllMapInstances().ForEach(action: s =>
                                            s.Monsters.Where(predicate: m => m.Owner?.MapEntityId == chara.CharacterId)
                                                .ToList()
                                                .ForEach(action: mon => ownedMonsters.Add(item: mon)));
                                        if (ownedMonsters.Count >= 3)
                                            chara.BattleEntity.RemoveOwnedMonsters(OnlyFirst: true);
                                        var monster = new MapMonster
                                        {
                                            MonsterVNum = (short)SecondData,
                                            MapX = x > 0 ? x : chara.Session.Character.PositionX,
                                            MapY = y > 0 ? y : chara.Session.Character.PositionY,
                                            MapId = chara.Session.Character.MapInstance.Map.MapId,
                                            Position = chara.Session.Character.Direction,
                                            MapMonsterId = chara.Session.CurrentMapInstance.GetNextMonsterId(),
                                            ShouldRespawn = false,
                                            Invisible = true,
                                            AliveTime = SecondData == 1436
                                                ? 20
                                                : ServerManager.GetNpcMonster(npcVNum: (short)SecondData)
                                                    .BasicCooldown > 0
                                                    ? ServerManager.GetNpcMonster(npcVNum: (short)SecondData)
                                                        .BasicCooldown
                                                    : ServerManager.GetNpcMonster(npcVNum: (short)SecondData)
                                                        .RespawnTime,
                                            Owner = chara.BattleEntity
                                        };
                                        monster.Initialize(currentMapInstance: chara.Session.CurrentMapInstance);
                                        chara.Session.CurrentMapInstance.AddMonster(monster: monster);
                                        chara.Session.CurrentMapInstance.Broadcast(packet: monster.GenerateIn());

                                        if (monster.MonsterVNum == 1438 || monster.MonsterVNum == 1439)
                                        {
                                            monster.Target = monster.BattleEntity;
                                            monster.StartLife();
                                            var npcMonsterSkill = monster.Skills.FirstOrDefault();
                                            session.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                                type: UserType.Monster,
                                                callerId: monster.MapMonsterId, secondaryType: (byte)UserType.Monster,
                                                targetId: monster.MapMonsterId,
                                                skillVNum: npcMonsterSkill.SkillVNum,
                                                cooldown: npcMonsterSkill.Skill.Cooldown,
                                                attackAnimation: npcMonsterSkill.Skill.AttackAnimation,
                                                skillEffect: npcMonsterSkill.Skill.Effect,
                                                x: monster.MapX, y: monster.MapY,
                                                isAlive: monster.CurrentHp > 0,
                                                health: (int)(monster.CurrentHp / monster.MaxHp * 100), damage: 0,
                                                hitmode: 0, skillType: 0));
                                        }
                                    }
                            }
                            else if (SubType.Equals(obj: (byte)AdditionalTypes.FalconSkill.FalconFollowing / 10))
                            {
                                if (sender.Character != null)
                                {
                                    sender.Character.BattleEntity.ClearOwnFalcon();
                                    sender.Character.BattleEntity.FalconFocusedEntityId = session.MapEntityId;
                                    sender.MapInstance.Broadcast(
                                        packet: $"eff_ob  {(byte)session.UserType} {session.MapEntityId} 1 4269");
                                    if (skill != null)
                                        session.MapInstance.Broadcast(packet: StaticPacketHelper.SkillUsed(
                                            type: sender.UserType,
                                            callerId: sender.MapEntityId, secondaryType: (byte)session.UserType,
                                            targetId: session.MapEntityId,
                                            skillVNum: skill.SkillVNum, cooldown: skill.Cooldown,
                                            attackAnimation: skill.AttackAnimation, skillEffect: skill.Effect, x: x,
                                            y: y,
                                            isAlive: true, health: session.Hp * 100 / session.HpMax, damage: 0,
                                            hitmode: -1, skillType: 0));
                                    sender.Character.BattleEntity.BCardDisposables[key: BCardId]?.Dispose();
                                    IDisposable bcardDisposable = null;
                                    bcardDisposable = Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 60))
                                        .Subscribe(onNext: s =>
                                        {
                                            if (sender.Character.BattleEntity.BCardDisposables[key: BCardId] !=
                                                bcardDisposable)
                                            {
                                                bcardDisposable.Dispose();
                                                return;
                                            }

                                            sender.Character.BattleEntity.ClearOwnFalcon();
                                        });
                                    sender.Character.BattleEntity.BCardDisposables[key: BCardId] = bcardDisposable;
                                }
                            }

                            break;

                        case BCardType.CardType.AbsorptionAndPowerSkill:
                            break;

                        case BCardType.CardType.LeonaPassiveSkill:
                            break;

                        case BCardType.CardType.FearSkill:
                            if (SubType.Equals(obj: (byte)AdditionalTypes.FearSkill.TimesUsed / 10))
                            {
                                if (sender.Character != null)
                                    if (FirstData == 1 && sender.Character.SkillComboCount == 3 ||
                                        FirstData == 2 && sender.Character.SkillComboCount == 5)
                                        sender.AddBuff(
                                            indicator: new Buff.Buff(id: (short)SecondData, level: senderLevel),
                                            sender: sender);
                            }
                            else if (SubType.Equals(obj: (byte)AdditionalTypes.FearSkill.AttackRangedIncreased / 10))
                            {
                                if (session.Character != null)
                                    session.Character.Session.SendPacket(packet: $"bf_d {FirstData} 1");
                            }
                            else if (SubType.Equals(obj: (byte)AdditionalTypes.FearSkill.MoveAgainstWill / 10))
                            {
                                if (session.Character != null)
                                    session.Character.Session.SendPacket(packet: $"rv_m {session.MapEntityId} 1 1");
                            }
                            else if (SubType.Equals(obj: (byte)AdditionalTypes.FearSkill.ProduceWhenAmbushe / 10))
                            {
                                if (sender == session && (x != 0 || y != 0) && SecondData > 0)
                                    if (ServerManager.RandomNumber() < firstData)
                                        sender.AddBuff(
                                            indicator: new Buff.Buff(id: (short)SecondData, level: senderLevel),
                                            sender: sender);
                            }

                            break;

                        case BCardType.CardType.SniperAttack:
                            if (session == sender) return;
                            if (SubType.Equals(obj: (byte)AdditionalTypes.SniperAttack.ChanceCausing / 10))
                            {
                                if (ServerManager.RandomNumber() < firstData)
                                {
                                    var ChanceCausingBuff = new Buff.Buff(id: (short)SecondData, level: senderLevel);
                                    if (ChanceCausingBuff.Card.BuffType == BuffType.Good ||
                                        ChanceCausingBuff.Card.BuffType == BuffType.Neutral)
                                        sender.AddBuff(indicator: ChanceCausingBuff, sender: sender);
                                    else
                                        session.AddBuff(indicator: ChanceCausingBuff, sender: sender);
                                }
                            }
                            else if (SubType.Equals(obj: (byte)AdditionalTypes.SniperAttack.ProduceChance / 10))
                            {
                                if (sender.Buffs.Any(predicate: s => s.Card.BCards.Any(predicate: b =>
                                    b.Type == (byte)BCardType.CardType.FalconSkill &&
                                    (b.SubType == (byte)AdditionalTypes.FalconSkill.Hide / 10 ||
                                     b.SubType == (byte)AdditionalTypes.FalconSkill.Ambush / 10))))
                                    if (ServerManager.RandomNumber() < firstData)
                                    {
                                        var ProduceChanceBuff =
                                            new Buff.Buff(id: (short)SecondData, level: senderLevel);
                                        if (ProduceChanceBuff.Card.BuffType == BuffType.Good ||
                                            ProduceChanceBuff.Card.BuffType == BuffType.Neutral)
                                            sender.AddBuff(indicator: ProduceChanceBuff, sender: sender);
                                        else
                                            session.AddBuff(indicator: ProduceChanceBuff, sender: sender);
                                    }
                            }

                            break;

                        case BCardType.CardType.FrozenDebuff:
                            {
                                if (SubType == (byte)AdditionalTypes.FrozenDebuff.GlacerusSkill / 10)
                                {
                                    var mapInstance = sender.MapInstance;

                                    if (mapInstance?.MapInstanceType == MapInstanceType.RaidInstance)
                                    {
                                        mapInstance.Broadcast(
                                            packet: UserInterfaceHelper.GenerateMsg(
                                                message: Language.Instance.GetMessageFromKey(key: "GLACERUS_GRRR"),
                                                type: 0));

                                        // SafeZone

                                        for (short monsterVNum = 4280; monsterVNum <= 4282; monsterVNum++)
                                            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: mapInstance,
                                                eventActionType: EventActionType.Spawnmonster, param: new MonsterToSummon(
                                                    vnum: monsterVNum,
                                                    spawnCell: new MapCell { X = 0, Y = 0 }, target: null, move: false)
                                                {
                                                    AfterSpawnEvents = new List<EventContainer>
                                                    {
                                                    new EventContainer(mapInstance: mapInstance,
                                                        eventActionType: EventActionType.Effect,
                                                        param: new Tuple<short, int>(item1: monsterVNum, item2: 0)),
                                                    new EventContainer(mapInstance: mapInstance,
                                                        eventActionType: EventActionType.Removeafter, param: 15)
                                                    }
                                                }));

                                        Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 5)).Subscribe(onNext: t =>
                                        {
                                            foreach (var character in mapInstance.Sessions
                                                .Where(predicate: s => s?.Character != null)
                                                .Select(selector: s => s.Character))
                                            {
                                            // Wind

                                            character.Session.SendPacket(packet: StaticPacketHelper.GenerateEff(
                                                    effectType: UserType.Player,
                                                    callerId: character.CharacterId, effectId: 4293));

                                            // Freeze

                                            if (character.Hp < 1
                                                    || character.HasBuff(type: BCardType.CardType.FrozenDebuff,
                                                        subtype: (byte)AdditionalTypes.FrozenDebuff.EternalIce))
                                                    continue;

                                                var safeZoneList = mapInstance
                                                    .GetMonsterInRangeList(mapX: character.PositionX,
                                                        mapY: character.PositionY, distance: 5)
                                                    .Where(predicate: m => m.MonsterVNum >= 4280 && m.MonsterVNum <= 4282);

                                                if (!safeZoneList.Any())
                                                {
                                                    character.AddBuff(
                                                        indicator: new Buff.Buff(id: 569, level: sender.Level),
                                                        sender: sender);

                                                    if (!mapInstance.Sessions.Any(predicate: s => s.Character != null
                                                                                                  && !s.Character.HasBuff(
                                                                                                      type: BCardType
                                                                                                          .CardType
                                                                                                          .FrozenDebuff,
                                                                                                      subtype: (byte)
                                                                                                      AdditionalTypes
                                                                                                          .FrozenDebuff
                                                                                                          .EternalIce)))
                                                    {
                                                        EventHelper.Instance.RunEvent(evt: new EventContainer(
                                                            mapInstance: mapInstance,
                                                            eventActionType: EventActionType.Sendpacket,
                                                            param: UserInterfaceHelper.GenerateMsg(
                                                                message: Language.Instance.GetMessageFromKey(
                                                                    key: "ALL_FROZEN"), type: 0)));
                                                        Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 5)).Subscribe(
                                                            onNext: _ =>
                                                                EventHelper.Instance.RunEvent(evt: new EventContainer(
                                                                    mapInstance: mapInstance,
                                                                    eventActionType: EventActionType.Scriptend,
                                                                    param: (byte)4)));
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                            break;

                        case BCardType.CardType.JumpBackPush:
                            if (session.ResistForcedMovement <= 0
                                || ServerManager.RandomNumber() < session.ResistForcedMovement)
                            {
                                if (SubType.Equals(obj: (byte)AdditionalTypes.JumpBackPush.JumpBackChance / 10))
                                    if (ServerManager.RandomNumber() < firstData)
                                        PushBackSession(PushDistance: SecondData, receiver: sender, point: session);
                                if (SubType.Equals(obj: (byte)AdditionalTypes.JumpBackPush.PushBackChance / 10))
                                    if (ServerManager.RandomNumber() < firstData)
                                        PushBackSession(PushDistance: SecondData, receiver: session, point: sender);
                            }

                            break;

                        case BCardType.CardType.FairyXpIncrease:
                            break;

                        case BCardType.CardType.SummonAndRecoverHp:
                            var summonParameters3 = new List<MonsterToSummon>();
                            if (ServerManager.RandomNumber() <= Math.Abs(value: ThirdData) || ThirdData == 0 ||
                                ThirdData < 0)
                            {
                                aliveTime = ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime /
                                    (ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime < 2400
                                        ? ServerManager.GetNpcMonster(npcVNum: (short)SecondData).RespawnTime <
                                          150
                                            ? 1
                                            : 10
                                        : 40) * (ServerManager.GetNpcMonster(npcVNum: (short)SecondData)
                                                     .RespawnTime >=
                                                 150
                                        ? 4
                                        : 1);
                                switch (SubType)
                                {
                                    case 1:
                                        if (sender.GetOwnedMonsters().Where(predicate: s => s.MonsterVNum == SecondData)
                                                .Count() ==
                                            0)
                                        {
                                            summonParameters3.Add(item: new MonsterToSummon(vnum: (short)SecondData,
                                                spawnCell: new MapCell { X = sender.PositionX, Y = sender.PositionY },
                                                target: null, move: true,
                                                aliveTime: aliveTime, owner: sender));
                                            EventHelper.Instance.RunEvent(evt: new EventContainer(
                                                mapInstance: sender.MapInstance,
                                                eventActionType: EventActionType.Spawnmonsters,
                                                param: summonParameters3));
                                        }

                                        break;

                                    case 2:
                                        Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1)).Subscribe(
                                            onNext: spawn =>
                                            {
                                                if (sender.MapMonster != null && sender.MapMonster.Owner != null &&
                                                    sender.MapMonster.Owner.GetOwnedMonsters().Where(predicate: s =>
                                                        s.MonsterVNum == sender.MapMonster.MonsterVNum).Count() == 1)
                                                {
                                                    for (var i = 0; i < firstData - 1; i++)
                                                    {
                                                        x = (short)(ServerManager.RandomNumber(min: -1, max: 1) +
                                                                     sender.PositionX);
                                                        y = (short)(ServerManager.RandomNumber(min: -1, max: 1) +
                                                                     sender.PositionY);
                                                        summonParameters3.Add(item: new MonsterToSummon(
                                                            vnum: sender.MapMonster.MonsterVNum,
                                                            spawnCell: new MapCell { X = x, Y = y }, target: null,
                                                            move: true, aliveTime: aliveTime,
                                                            owner: sender.MapMonster.Owner));
                                                    }

                                                    EventHelper.Instance.RunEvent(evt: new EventContainer(
                                                        mapInstance: sender.MapInstance,
                                                        eventActionType: EventActionType.Spawnmonsters,
                                                        param: summonParameters3));
                                                }
                                            });
                                        break;
                                }
                            }

                            break;

                        case BCardType.CardType.TeamArenaBuff:
                            break;

                        case BCardType.CardType.ArenaCamera:
                            break;

                        case BCardType.CardType.DarkCloneSummon:
                            {
                                if (SubType == (byte)AdditionalTypes.DarkCloneSummon.SummonDarkCloneChance / 10)
                                    if (ServerManager.RandomNumber() < FirstData)
                                    {
                                        var monstersToSummon = new List<MonsterToSummon>();

                                        var monsterVNums = Enumerable.Range(start: 2112, count: SecondData)
                                            .OrderBy(keySelector: t => ServerManager.RandomNumber())
                                            .Select(selector: t => (short)t).ToList();

                                        for (var i = 0;
                                            i < ServerManager.RandomNumber(min: 1, max: monsterVNums.Count + 1);
                                            i++)
                                        {
                                            var mapX = (short)(sender.PositionX +
                                                                ServerManager.RandomNumber(min: -2, max: 2));
                                            var mapY = (short)(sender.PositionY +
                                                                ServerManager.RandomNumber(min: -2, max: 2));

                                            if (sender.MapInstance.Map.IsBlockedZone(x: mapX, y: mapY))
                                            {
                                                mapX = sender.PositionX;
                                                mapY = sender.PositionY;
                                            }

                                            monstersToSummon.Add(item: new MonsterToSummon(vnum: monsterVNums[index: i],
                                                spawnCell: new MapCell { X = mapX, Y = mapY }, target: session,
                                                move: true, isTarget: false, isBonus: false, isHostile: false,
                                                isBoss: false, owner: sender, aliveTime: 5, aliveTimeMp: 0, noticeRange: 0,
                                                hasDelay: 1 /* second(s) */));
                                        }

                                        EventHelper.Instance.RunEvent(evt: new EventContainer(
                                            mapInstance: sender.MapInstance,
                                            eventActionType: EventActionType.Spawnmonsters, param: monstersToSummon));
                                    }
                            }
                            break;

                        case BCardType.CardType.AbsorbedSpirit:
                            {
                                if (SubType == (byte)AdditionalTypes.AbsorbedSpirit.ApplyEffectIfPresent / 10
                                    || SubType == (byte)AdditionalTypes.AbsorbedSpirit.ApplyEffectIfNotPresent / 10)
                                {
                                    var hasSpiritAbsorption = session.HasBuff(cardId: 596);

                                    if (FirstData > 0)
                                    {
                                        if (hasSpiritAbsorption)
                                        {
                                            session.AddBuff(
                                                indicator: new Buff.Buff(id: (short)SecondData, level: session.Level),
                                                sender: session);
                                            session.RemoveBuff(id: 596);
                                        }
                                    }
                                    else
                                    {
                                        if (!hasSpiritAbsorption && !session.HasBuff(cardId: 599))
                                            session.AddBuff(
                                                indicator: new Buff.Buff(id: (short)SecondData, level: session.Level),
                                                sender: session);
                                    }
                                }
                            }
                            break;

                        case BCardType.CardType.AngerSkill:
                            break;

                        case BCardType.CardType.MeteoriteTeleport:
                            {
                                if (SubType == (byte)AdditionalTypes.MeteoriteTeleport.SummonInVisualRange / 10)
                                {
                                    var mapInstance = session?.MapInstance;

                                    if (mapInstance != null)
                                    {
                                        var monstersToSummon = new List<MonsterToSummon>();

                                        for (var i = 0; i < 73; i++)
                                            monstersToSummon.Add(item: new MonsterToSummon(vnum: 2328,
                                                spawnCell: new MapCell { X = 0, Y = 0 }, target: null,
                                                move: false, hasDelay: (short)ServerManager.RandomNumber(min: 0, max: 5)));

                                        EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: mapInstance,
                                            eventActionType: EventActionType.Spawnmonsters, param: monstersToSummon));
                                    }
                                }
                                else if (SubType == (byte)AdditionalTypes.MeteoriteTeleport.TransformTarget / 10)
                                {
                                    var morphVNums = new[] { 1000099, 1000156 };

                                    if (sender != null
                                        && session?.Character?.MapInstance != null
                                        && !morphVNums.Contains(value: session.Character.Morph)
                                        && ServerManager.RandomNumber() < 50)
                                    {
                                        session.Character.MapInstance.Broadcast(
                                            packet: session.Character.GenerateEff(effectid: 4054));
                                        session.Character.IsMorphed = true;
                                        session.Character.PreviousMorph = session.Character.Morph;
                                        session.Character.Morph =
                                            morphVNums.OrderBy(keySelector: rnd => ServerManager.RandomNumber()).First();

                                        switch (session.Character.Morph)
                                        {
                                            case 1000099: // Hamster
                                                session.AddBuff(
                                                    indicator: new Buff.Buff(id: 478, level: sender.Level,
                                                        isPermaBuff: true),
                                                    sender: sender);
                                                break;
                                            case 1000156: // Bushtail
                                                session.AddBuff(
                                                    indicator: new Buff.Buff(id: 477, level: sender.Level,
                                                        isPermaBuff: true),
                                                    sender: sender);
                                                break;
                                        }

                                        session.Character.MapInstance.Broadcast(packet: session.Character.GenerateCMode());
                                    }
                                }
                                else if (SubType == (byte)AdditionalTypes.MeteoriteTeleport.TeleportForward / 10)
                                {
                                    session.TeleportTo(
                                        mapCell: session.MapInstance.Map.GetRandomPositionByDistance(
                                            xPos: session.PositionX,
                                            yPos: session.PositionY, distance: (short)FirstData));
                                }
                                else if (SubType == (byte)AdditionalTypes.MeteoriteTeleport.CauseMeteoriteFall / 10)
                                {
                                    var mapInstance = session?.MapInstance;

                                    if (mapInstance != null)
                                    {
                                        var monstersToSummon = new List<MonsterToSummon>();

                                        var range = 10;

                                        for (var i = 0; i < 10 + (int)(session.Level / (double)FirstData); i++)
                                        {
                                            var mapCell = new MapCell
                                            {
                                                X = (short)(session.PositionX +
                                                             ServerManager.RandomNumber(min: -range, max: range + 1)),
                                                Y = (short)(session.PositionY +
                                                             ServerManager.RandomNumber(min: -range, max: range + 1))
                                            };

                                            monstersToSummon.Add(
                                                item: new MonsterToSummon(
                                                    vnum: (short)ServerManager.RandomNumber(min: 2352, max: 2353 + 1),
                                                    spawnCell: mapCell, target: null, move: false, owner: session,
                                                    hasDelay: ServerManager.RandomNumber<short>(min: 0, max: 10 + 1))
                                                {
                                                    IsMeteorite = true
                                                });
                                        }

                                        if (monstersToSummon.Any())
                                            EventHelper.Instance.RunEvent(evt: new EventContainer(mapInstance: mapInstance,
                                                eventActionType: EventActionType.Spawnmonsters, param: monstersToSummon));
                                    }
                                }
                                else if (SubType == (byte)AdditionalTypes.MeteoriteTeleport
                                    .TeleportYouAndGroupToSavedLocation / 10)
                                {
                                    if (session.Character is Character character &&
                                        character.CharacterId == sender?.Character?.CharacterId)
                                    {
                                        if (character.SavedLocation == null)
                                        {
                                            character.SavedLocation = new MapCell
                                            {
                                                X = character.PositionX,
                                                Y = character.PositionY
                                            };

                                            session.MapInstance?.Broadcast(
                                                packet:
                                                $"eff_g 4497 {character.CharacterId} {character.SavedLocation.X} {character.SavedLocation.Y} 0");
                                        }
                                        else
                                        {
                                            var mapCellTo = new MapCell
                                            {
                                                X = character.SavedLocation.X,
                                                Y = character.SavedLocation.Y
                                            };

                                            var mapCellFrom = new MapCell
                                            {
                                                X = session.PositionX,
                                                Y = session.PositionY
                                            };

                                            session.MapInstance?.Broadcast(
                                                packet:
                                                $"eff_g 4483 {character.CharacterId} {mapCellFrom.X} {mapCellFrom.Y} 0");

                                            Observable.Timer(dueTime: TimeSpan.FromSeconds(value: 1))
                                                .Subscribe(onNext: t =>
                                                {
                                                    session.TeleportTo(mapCell: mapCellTo);

                                                    session.MapInstance?.Broadcast(
                                                        packet:
                                                        $"eff_g 4497 {character.CharacterId} {mapCellTo.X} {mapCellTo.Y} 1");

                                                    var friendCharacters = new List<Character>();

                                                    if (character.Family?.FamilyCharacters != null)
                                                        friendCharacters.AddRange(collection: character.Family
                                                            .FamilyCharacters
                                                            .Select(selector: fc =>
                                                                ServerManager.Instance.GetCharacterById(
                                                                    characterId: fc.CharacterId))
                                                            .Where(predicate: c => c != null));

                                                    if (character.Group?.Sessions != null)
                                                        friendCharacters.AddRange(collection: character.Group.Sessions
                                                            .Where(predicate: s => s.Character != null)
                                                            .Select(selector: s => s.Character));

                                                    friendCharacters.Where(predicate: c =>
                                                            c.CharacterId != character.CharacterId && c.MapInstanceId ==
                                                                                                   character.MapInstanceId
                                                                                                   && Map.GetDistance(
                                                                                                       p: c.BattleEntity
                                                                                                           .GetPos(),
                                                                                                       q: mapCellFrom) <=
                                                                                                   skill.TargetRange)
                                                        .OrderBy(keySelector: c =>
                                                            Map.GetDistance(p: c.BattleEntity.GetPos(), q: mapCellFrom))
                                                        .Take(count: FirstData).ToList()
                                                        .ForEach(action: c =>
                                                            c.BattleEntity.TeleportTo(mapCell: mapCellTo, distance: 3));
                                                });
                                        }
                                    }
                                }
                            }
                            break;

                        case BCardType.CardType.StealBuff:
                            break;

                        case BCardType.CardType.Unknown:
                            break;

                        case BCardType.CardType.EffectSummon:
                            break;

                        case BCardType.CardType.MartialArts:
                            break;

                        default:
                            Logger.Warn(data: $"Card Type {Type} not defined!");
                            break;
                    }
                });

            void PushBackSession(int PushDistance, BattleEntity receiver, BattleEntity point)
            {
                if (receiver.MapMonster != null
                    && (receiver.MapMonster.IsBoss
                        || ServerManager.Instance.BossVNums.Contains(item: receiver.MapMonster.MonsterVNum)
                        || receiver.MapMonster.Owner?.Character != null || receiver.MapMonster.Owner?.Mate != null))
                    return;

                if (receiver.MapNpc != null
                    && ServerManager.Instance.BossVNums.Contains(item: receiver.MapNpc.NpcVNum))
                    return;

                receiver.Character?.WalkDisposable?.Dispose();

                var NewX = receiver.PositionX;
                var NewY = receiver.PositionY;

                if (point.PositionX == receiver.PositionX && point.PositionY == receiver.PositionY)
                {
                    var randomCell =
                        receiver.MapInstance.Map.GetRandomPositionByDistance(xPos: point.PositionX,
                            yPos: point.PositionY,
                            distance: (short)PushDistance);
                    if (randomCell != null)
                    {
                        NewX = randomCell.X;
                        NewY = randomCell.Y;
                    }
                }
                else
                {
                    var pointPosition = new Point3D(x: point.PositionX, y: point.PositionY, z: 0.0);
                    var receiverPosition = new Point3D(x: receiver.PositionX, y: receiver.PositionY, z: 0.0);
                    var newPosition = new Point3D(x: receiver.PositionX, y: receiver.PositionY, z: 0.0);
                    for (var i = 0;
                        i == 0 || PushDistance - i >= 0 && receiver.MapInstance.Map.IsBlockedZone(
                            firstX: receiver.PositionX,
                            firstY: receiver.PositionY, mapX: (int)Math.Round(a: newPosition.X),
                            mapY: (int)Math.Round(a: newPosition.Y));
                        i++)
                    {
                        var v = receiverPosition - pointPosition;
                        double offset = PushDistance - i;
                        newPosition = ExtendLine(point: receiverPosition, offset: offset, vector: v);
                    }

                    NewX = (short)Math.Round(a: newPosition.X);
                    NewY = (short)Math.Round(a: newPosition.Y);
                }

                receiver.PositionX = NewX;
                receiver.PositionY = NewY;

                if (receiver == sender)
                    receiver.MapInstance.Broadcast(
                        packet:
                        $"guri 3 {(short)receiver.UserType} {receiver.MapEntityId} {receiver.PositionX} {receiver.PositionY} 3 {SecondData / 4} 2 -1");
                else
                    receiver.MapInstance.Broadcast(
                        packet:
                        $"guri 3 {(short)receiver.UserType} {receiver.MapEntityId} {receiver.PositionX} {receiver.PositionY} 3 {SecondData} 2 -1");
            }
        }

        static Point3D ExtendLine(Point3D point, double offset, Vector3D vector)
        {
            vector.Normalize();
            var _offset = offset / vector.Length;
            var _vector = vector * _offset;
            var m = new Matrix3D();
            m.Translate(offset: _vector);
            var result = m.Transform(point: point);
            return result;
        }

        #endregion
    }
}