﻿using System;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Buff
{
    public class Buff
    {
        #region Instantiation

        public Buff(short id, int level, bool isPermaBuff = false)
        {
            Card = ServerManager.GetCard(cardId: id);
            Level = level;
            IsPermaBuff = isPermaBuff;
        }

        #endregion

        #region Members

        public int Level;

        public bool IsPermaBuff { get; set; }

        #endregion

        #region Properties

        public Card Card { get; set; }

        public int RemainingTime { get; set; }

        public DateTime Start { get; set; }

        public bool StaticBuff { get; set; }

        public IDisposable StaticVisualEffect { get; set; }

        public BattleEntity Sender { get; set; }

        public short? SkillVNum { get; set; }

        #endregion
    }
}