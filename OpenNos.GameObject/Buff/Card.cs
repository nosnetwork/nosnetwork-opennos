﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.GameObject.Buff
{
    public class Card : CardDto
    {
        #region Properties

        public List<BCard> BCards { get; set; }

        #endregion

        #region Instantiation

        public Card()
        {
        }

        public Card(CardDto input)
        {
            BuffType = input.BuffType;
            CardId = input.CardId;
            Delay = input.Delay;
            Duration = input.Duration;
            EffectId = input.EffectId;
            Level = input.Level;
            Name = input.Name;
            Propability = input.Propability;
            TimeoutBuff = input.TimeoutBuff;
            TimeoutBuffChance = input.TimeoutBuffChance;
        }

        #endregion
    }
}