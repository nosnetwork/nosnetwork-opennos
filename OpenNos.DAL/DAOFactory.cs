﻿using OpenNos.DAL.DAO;
using OpenNos.DAL.Interface;


namespace OpenNos.DAL
{
    public static class DaoFactory
    {
        #region Members

        static IAccountDao _accountDao;
        static IBazaarItemDao _bazaarItemDao;
        static IBCardDao _bcardDao;
        static ICardDao _cardDao;
        static ICellonOptionDao _cellonOptionDao;
        static ICharacterDao _characterDao;
        static ICharacterRelationDao _characterRelationDao;
        static ICharacterSkillDao _characterSkillDao;
        static ICharacterQuestDao _characterQuestDao;
        static IComboDao _comboDao;
        static IDropDao _dropDao;
        static IFamilyCharacterDao _familyCharacterDao;
        static IFamilyDao _familyDao;
        static IFamilyLogDao _familyLogDao;
        static IGeneralLogDao _generalLogDao;
        static IItemDao _itemDao;
        static IItemInstanceDao _itemInstanceDao;
        static IMailDao _mailDao;
        static IMaintenanceLogDao _maintenanceLogDao;
        static IMapDao _mapDao;
        static IMapMonsterDao _mapMonsterDao;
        static IMapNpcDao _mapNpcDao;
        static IMapTypeDao _mapTypeDao;
        static IMapTypeMapDao _mapTypeMapDao;
        static IMateDao _mateDao;
        static IMinigameLogDao _minigameLogDao;
        static IMinilandObjectDao _minilandObjectDao;
        static INpcMonsterDao _npcMonsterDao;
        static INpcMonsterSkillDao _npcMonsterSkillDao;
        static IPartnerSkillDao _partnerSkillDao;
        static IPenaltyLogDao _penaltyLogDao;
        static IPortalDao _portalDao;
        static IQuestDao _questDao;
        static IQuestLogDao _questLogDao;
        static IQuestRewardDao _questRewardDao;
        static IQuestObjectiveDao _questObjectiveDao;
        static IQuicklistEntryDao _quicklistEntryDao;
        static IRecipeDao _recipeDao;
        static IRecipeItemDao _recipeItemDao;
        static IRecipeListDao _recipeListDao;
        static IRespawnDao _respawnDao;
        static IRespawnMapTypeDao _respawnMapTypeDao;
        static IRollGeneratedItemDao _rollGeneratedItemDao;
        static IScriptedInstanceDao _scriptedInstanceDao;
        static IShellEffectDao _shellEffectDao;
        static IShopDao _shopDao;
        static IShopItemDao _shopItemDao;
        static IShopSkillDao _shopSkillDao;
        static ISkillDao _skillDao;
        static IStaticBonusDao _staticBonusDao;
        static IStaticBuffDao _staticBuffDao;
        static ITeleporterDao _teleporterDao;

        #endregion

        #region Properties

        public static IAccountDao AccountDao
        {
            get => _accountDao ?? (_accountDao = new AccountDao());
        }

        public static IBazaarItemDao BazaarItemDao
        {
            get => _bazaarItemDao ?? (_bazaarItemDao = new BazaarItemDao());
        }

        public static IBCardDao BCardDao
        {
            get => _bcardDao ?? (_bcardDao = new BCardDao());
        }

        public static ICardDao CardDao
        {
            get => _cardDao ?? (_cardDao = new CardDao());
        }

        public static ICellonOptionDao CellonOptionDao
        {
            get => _cellonOptionDao ?? (_cellonOptionDao = new CellonOptionDao());
        }

        public static ICharacterDao CharacterDao
        {
            get => _characterDao ?? (_characterDao = new CharacterDao());
        }

        public static ICharacterRelationDao CharacterRelationDao
        {
            get => _characterRelationDao ?? (_characterRelationDao = new CharacterRelationDao());
        }

        public static ICharacterSkillDao CharacterSkillDao
        {
            get => _characterSkillDao ?? (_characterSkillDao = new CharacterSkillDao());
        }

        public static ICharacterQuestDao CharacterQuestDao
        {
            get => _characterQuestDao ?? (_characterQuestDao = new CharacterQuestDao());
        }

        public static IComboDao ComboDao
        {
            get => _comboDao ?? (_comboDao = new ComboDao());
        }

        public static IDropDao DropDao
        {
            get => _dropDao ?? (_dropDao = new DropDao());
        }

        public static IFamilyCharacterDao FamilyCharacterDao
        {
            get => _familyCharacterDao ?? (_familyCharacterDao = new FamilyCharacterDao());
        }

        public static IFamilyDao FamilyDao
        {
            get => _familyDao ?? (_familyDao = new FamilyDao());
        }

        public static IFamilyLogDao FamilyLogDao
        {
            get => _familyLogDao ?? (_familyLogDao = new FamilyLogDao());
        }

        public static IGeneralLogDao GeneralLogDao
        {
            get => _generalLogDao ?? (_generalLogDao = new GeneralLogDao());
        }

        public static IItemDao ItemDao
        {
            get => _itemDao ?? (_itemDao = new ItemDao());
        }

        public static IItemInstanceDao ItemInstanceDao
        {
            get => _itemInstanceDao ?? (_itemInstanceDao = new ItemInstanceDao());
        }

        public static IMailDao MailDao
        {
            get => _mailDao ?? (_mailDao = new MailDao());
        }

        public static IMaintenanceLogDao MaintenanceLogDao
        {
            get => _maintenanceLogDao ?? (_maintenanceLogDao = new MaintenanceLogDao());
        }

        public static IMapDao MapDao
        {
            get => _mapDao ?? (_mapDao = new MapDao());
        }

        public static IMapMonsterDao MapMonsterDao
        {
            get => _mapMonsterDao ?? (_mapMonsterDao = new MapMonsterDao());
        }

        public static IMapNpcDao MapNpcDao
        {
            get => _mapNpcDao ?? (_mapNpcDao = new MapNpcDao());
        }

        public static IMapTypeDao MapTypeDao
        {
            get => _mapTypeDao ?? (_mapTypeDao = new MapTypeDao());
        }

        public static IMapTypeMapDao MapTypeMapDao
        {
            get => _mapTypeMapDao ?? (_mapTypeMapDao = new MapTypeMapDao());
        }

        public static IMateDao MateDao
        {
            get => _mateDao ?? (_mateDao = new MateDao());
        }

        public static IMinigameLogDao MinigameLogDao
        {
            get => _minigameLogDao ?? (_minigameLogDao = new MinigameLogDao());
        }

        public static IMinilandObjectDao MinilandObjectDao
        {
            get => _minilandObjectDao ?? (_minilandObjectDao = new MinilandObjectDao());
        }

        public static INpcMonsterDao NpcMonsterDao
        {
            get => _npcMonsterDao ?? (_npcMonsterDao = new NpcMonsterDao());
        }

        public static INpcMonsterSkillDao NpcMonsterSkillDao
        {
            get => _npcMonsterSkillDao ?? (_npcMonsterSkillDao = new NpcMonsterSkillDao());
        }

        public static IPartnerSkillDao PartnerSkillDao
        {
            get => _partnerSkillDao ?? (_partnerSkillDao = new PartnerSkillDao());
        }

        public static IPenaltyLogDao PenaltyLogDao
        {
            get => _penaltyLogDao ?? (_penaltyLogDao = new PenaltyLogDao());
        }

        public static IPortalDao PortalDao
        {
            get => _portalDao ?? (_portalDao = new PortalDao());
        }

        public static IQuestDao QuestDao
        {
            get => _questDao ?? (_questDao = new QuestDao());
        }

        public static IQuestLogDao QuestLogDao
        {
            get => _questLogDao ?? (_questLogDao = new QuestLogDao());
        }

        public static IQuestRewardDao QuestRewardDao
        {
            get => _questRewardDao ?? (_questRewardDao = new QuestRewardDao());
        }

        public static IQuestObjectiveDao QuestObjectiveDao
        {
            get => _questObjectiveDao ?? (_questObjectiveDao = new QuestObjectiveDao());
        }

        public static IQuicklistEntryDao QuicklistEntryDao
        {
            get => _quicklistEntryDao ?? (_quicklistEntryDao = new QuicklistEntryDao());
        }

        public static IRecipeDao RecipeDao
        {
            get => _recipeDao ?? (_recipeDao = new RecipeDao());
        }

        public static IRecipeItemDao RecipeItemDao
        {
            get => _recipeItemDao ?? (_recipeItemDao = new RecipeItemDao());
        }

        public static IRecipeListDao RecipeListDao
        {
            get => _recipeListDao ?? (_recipeListDao = new RecipeListDao());
        }

        public static IRespawnDao RespawnDao
        {
            get => _respawnDao ?? (_respawnDao = new RespawnDao());
        }

        public static IRespawnMapTypeDao RespawnMapTypeDao
        {
            get => _respawnMapTypeDao ?? (_respawnMapTypeDao = new RespawnMapTypeDao());
        }

        public static IRollGeneratedItemDao RollGeneratedItemDao
        {
            get => _rollGeneratedItemDao ?? (_rollGeneratedItemDao = new RollGeneratedItemDao());
        }

        public static IScriptedInstanceDao ScriptedInstanceDao
        {
            get => _scriptedInstanceDao ?? (_scriptedInstanceDao = new ScriptedInstanceDao());
        }

        public static IShellEffectDao ShellEffectDao
        {
            get => _shellEffectDao ?? (_shellEffectDao = new ShellEffectDao());
        }

        public static IShopDao ShopDao
        {
            get => _shopDao ?? (_shopDao = new ShopDao());
        }

        public static IShopItemDao ShopItemDao
        {
            get => _shopItemDao ?? (_shopItemDao = new ShopItemDao());
        }

        public static IShopSkillDao ShopSkillDao
        {
            get => _shopSkillDao ?? (_shopSkillDao = new ShopSkillDao());
        }

        public static ISkillDao SkillDao
        {
            get => _skillDao ?? (_skillDao = new SkillDao());
        }

        public static IStaticBonusDao StaticBonusDao
        {
            get => _staticBonusDao ?? (_staticBonusDao = new StaticBonusDao());
        }

        public static IStaticBuffDao StaticBuffDao
        {
            get => _staticBuffDao ?? (_staticBuffDao = new StaticBuffDao());
        }

        public static ITeleporterDao TeleporterDao
        {
            get => _teleporterDao ?? (_teleporterDao = new TeleporterDao());
        }

        #endregion
    }
}