﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace OpenNos.SCS.Threading
{
    public class Timer
    {
        readonly System.Threading.Timer _taskTimer;
        volatile bool _performingTasks;
        volatile bool _running;

        public Timer(int period)
            : this(period: period, runOnStart: false)
        {
        }

        public Timer(int period, bool runOnStart)
        {
            Period = period;
            RunOnStart = runOnStart;
            _taskTimer = new System.Threading.Timer(callback: TimerCallBack, state: null, dueTime: -1, period: -1);
        }

        public int Period { get; set; }

        public bool RunOnStart { get; set; }

        [CompilerGenerated] public event EventHandler Elapsed;

        public void Start()
        {
            _running = true;
            _taskTimer.Change(dueTime: RunOnStart ? 0 : Period, period: -1);
        }

        public void Stop()
        {
            lock (_taskTimer)
            {
                _running = false;
                _taskTimer.Change(dueTime: -1, period: -1);
            }
        }

        public void WaitToStop()
        {
            lock (_taskTimer)
            {
                while (_performingTasks)
                    Monitor.Wait(obj: _taskTimer);
            }
        }

        void TimerCallBack(object state)
        {
            lock (_taskTimer)
            {
                if (!_running || _performingTasks)
                    return;
                _taskTimer.Change(dueTime: -1, period: -1);
                _performingTasks = true;
            }

            try
            {
                if (Elapsed == null)
                    return;
                Elapsed(sender: this, e: new EventArgs());
            }
            catch
            {
            }
            finally
            {
                lock (_taskTimer)
                {
                    _performingTasks = false;
                    if (_running)
                        _taskTimer.Change(dueTime: Period, period: -1);
                    Monitor.Pulse(obj: _taskTimer);
                }
            }
        }
    }
}