﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OpenNos.SCS.Threading
{
    public class SequentialItemProcessor<TItem>
    {
        readonly Action<TItem> _processMethod;
        readonly Queue<TItem> _queue;
        readonly object _syncObj = new object();
        Task _currentProcessTask;
        bool _isProcessing;
        bool _isRunning;

        public SequentialItemProcessor(Action<TItem> processMethod)
        {
            _processMethod = processMethod;
            _queue = new Queue<TItem>();
        }

        public void EnqueueMessage(TItem item)
        {
            lock (_syncObj)
            {
                if (!_isRunning)
                    return;
                _queue.Enqueue(item: item);
                if (_isProcessing)
                    return;
                _currentProcessTask = Task.Factory.StartNew(action: ProcessItem);
            }
        }

        public void Start()
        {
            _isRunning = true;
        }

        public void Stop()
        {
            _isRunning = false;
            lock (_syncObj)
            {
                _queue.Clear();
            }

            if (!_isProcessing)
                return;
            try
            {
                _currentProcessTask.Wait();
            }
            catch
            {
            }
        }

        void ProcessItem()
        {
            TItem obj;
            lock (_syncObj)
            {
                if (!_isRunning || _isProcessing || _queue.Count <= 0)
                    return;
                _isProcessing = true;
                obj = _queue.Dequeue();
            }

            _processMethod(obj: obj);
            lock (_syncObj)
            {
                _isProcessing = false;
                if (!_isRunning || _queue.Count <= 0)
                    return;
                _currentProcessTask = Task.Factory.StartNew(action: ProcessItem);
            }
        }
    }
}