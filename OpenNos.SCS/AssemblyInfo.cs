﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany(company: "Eastmile")]
[assembly: AssemblyConfiguration(configuration: "")]
[assembly: AssemblyCopyright(copyright: "Eastmile")]
[assembly: AssemblyDescription(description: "Simple Client Server Framework - Eastmile Edition")]
[assembly: AssemblyProduct(product: "OpenNos.SCS")]
[assembly: AssemblyTitle(title: "OpenNos.SCS")]
[assembly: AssemblyTrademark(trademark: "")]
[assembly: ComVisible(visibility: false)]
[assembly: Guid(guid: "7b68ba8a-978a-4616-a2ce-47f653923df7")]
[assembly: AssemblyVersion(version: "1.2.6505.26681")]