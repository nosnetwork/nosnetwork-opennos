﻿using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace Properties
{
    [CompilerGenerated]
    [GeneratedCode(tool: "Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator",
        version: "15.3.0.0")]
    internal sealed class Settings : ApplicationSettingsBase
    {
        public static Settings Default { get; } = (Settings)Synchronized(settingsBase: new Settings());
    }
}