﻿using System.Collections.Generic;
using System.Threading;

namespace OpenNos.SCS.Collections
{
    public class ThreadSafeSortedList<TK, TV>
    {
        protected readonly SortedList<TK, TV> _items;
        protected readonly ReaderWriterLockSlim _lock;

        public ThreadSafeSortedList()
        {
            _items = new SortedList<TK, TV>();
            _lock = new ReaderWriterLockSlim(recursionPolicy: LockRecursionPolicy.NoRecursion);
        }

        public TV this[TK key]
        {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.ContainsKey(key: key) ? _items[key: key] : default;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
            set
            {
                _lock.EnterWriteLock();
                try
                {
                    _items[key: key] = value;
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
            }
        }

        public int Count
        {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Count;
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public bool ContainsKey(TK key)
        {
            _lock.EnterReadLock();
            try
            {
                return _items.ContainsKey(key: key);
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        public bool ContainsValue(TV item)
        {
            _lock.EnterReadLock();
            try
            {
                return _items.ContainsValue(value: item);
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        public bool Remove(TK key)
        {
            _lock.EnterWriteLock();
            try
            {
                if (!_items.ContainsKey(key: key))
                    return false;
                _items.Remove(key: key);
                return true;
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public List<TV> GetAllItems()
        {
            _lock.EnterReadLock();
            try
            {
                return new List<TV>(collection: _items.Values);
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        public void ClearAll()
        {
            _lock.EnterWriteLock();
            try
            {
                _items.Clear();
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public List<TV> GetAndClearAllItems()
        {
            _lock.EnterWriteLock();
            try
            {
                var vList = new List<TV>(collection: _items.Values);
                _items.Clear();
                return vList;
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }
    }
}