﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Proxies;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;
using OpenNos.SCS.Communication.Scs.Server;
using OpenNos.SCS.Communication.ScsServices.Communication;

namespace OpenNos.SCS.Communication.ScsServices.Service
{
    internal class ScsServiceClient : IScsServiceClient
    {
        readonly RequestReplyMessenger<IScsServerClient> _requestReplyMessenger;
        readonly IScsServerClient _serverClient;
        RealProxy _realProxy;

        public ScsServiceClient(
            IScsServerClient serverClient,
            RequestReplyMessenger<IScsServerClient> requestReplyMessenger)
        {
            _serverClient = serverClient;
            _serverClient.Disconnected += Client_Disconnected;
            _requestReplyMessenger = requestReplyMessenger;
        }

        [CompilerGenerated] public event EventHandler Disconnected;

        public long ClientId
        {
            get => _serverClient.ClientId;
        }

        public ScsEndPoint RemoteEndPoint
        {
            get => _serverClient.RemoteEndPoint;
        }

        public CommunicationStates CommunicationState
        {
            get => _serverClient.CommunicationState;
        }

        public void Disconnect()
        {
            _serverClient.Disconnect();
        }

        public T GetClientProxy<T>() where T : class
        {
            _realProxy = new RemoteInvokeProxy<T, IScsServerClient>(clientMessenger: _requestReplyMessenger);
            return (T)_realProxy.GetTransparentProxy();
        }

        void Client_Disconnected(object sender, EventArgs e)
        {
            _requestReplyMessenger.Stop();
            OnDisconnected();
        }

        void OnDisconnected()
        {
            var disconnected = Disconnected;
            if (disconnected == null)
                return;
            disconnected(sender: this, e: EventArgs.Empty);
        }
    }
}