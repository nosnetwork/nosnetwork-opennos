﻿using System;

namespace OpenNos.SCS.Communication.ScsServices.Service
{
    public abstract class ScsService
    {
        [ThreadStatic] static IScsServiceClient _currentClient;

        protected internal IScsServiceClient CurrentClient
        {
            get
            {
                if (_currentClient == null)
                    throw new Exception(
                        message:
                        "Client channel can not be obtained. CurrentClient property must be called by the thread which runs the service method.");
                return _currentClient;
            }
            internal set => _currentClient = value;
        }
    }
}