﻿using System;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints;

namespace OpenNos.SCS.Communication.ScsServices.Service
{
    public interface IScsServiceClient
    {
        long ClientId { get; }

        ScsEndPoint RemoteEndPoint { get; }

        CommunicationStates CommunicationState { get; }
        event EventHandler Disconnected;

        void Disconnect();

        T GetClientProxy<T>() where T : class;
    }
}