﻿using System;

namespace OpenNos.SCS.Communication.ScsServices.Service
{
    public class ServiceClientEventArgs : EventArgs
    {
        public ServiceClientEventArgs(IScsServiceClient client)
        {
            Client = client;
        }

        public IScsServiceClient Client { get; }
    }
}