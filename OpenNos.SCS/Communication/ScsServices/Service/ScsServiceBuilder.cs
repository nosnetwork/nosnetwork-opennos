﻿using OpenNos.SCS.Communication.Scs.Communication.EndPoints;
using OpenNos.SCS.Communication.Scs.Server;

namespace OpenNos.SCS.Communication.ScsServices.Service
{
    public static class ScsServiceBuilder
    {
        public static IScsServiceApplication CreateService(ScsEndPoint endPoint)
        {
            if (Management.CheckLicence())
                return new ScsServiceApplication(scsServer: ScsServerFactory.CreateServer(endPoint: endPoint));
            return null;
        }
    }
}