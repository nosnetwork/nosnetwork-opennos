﻿using OpenNos.SCS.Communication.Scs.Communication.Messengers;
using OpenNos.SCS.Communication.Scs.Server;

namespace OpenNos.SCS.Communication.ScsServices.Service
{
    internal static class ScsServiceClientFactory
    {
        public static IScsServiceClient CreateServiceClient(
            IScsServerClient serverClient,
            RequestReplyMessenger<IScsServerClient> requestReplyMessenger)
        {
            return new ScsServiceClient(serverClient: serverClient, requestReplyMessenger: requestReplyMessenger);
        }
    }
}