﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using OpenNos.SCS.Collections;
using OpenNos.SCS.Communication.Scs.Communication.Messages;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;
using OpenNos.SCS.Communication.Scs.Server;
using OpenNos.SCS.Communication.ScsServices.Communication.Messages;

namespace OpenNos.SCS.Communication.ScsServices.Service
{
    internal class ScsServiceApplication : IScsServiceApplication
    {
        readonly IScsServer _scsServer;
        readonly ThreadSafeSortedList<long, IScsServiceClient> _serviceClients;
        readonly ThreadSafeSortedList<string, ServiceObject> _serviceObjects;

        public ScsServiceApplication(IScsServer scsServer)
        {
            _scsServer = scsServer ?? throw new ArgumentNullException(paramName: nameof(scsServer));
            _scsServer.ClientConnected += ScsServer_ClientConnected;
            _scsServer.ClientDisconnected += ScsServer_ClientDisconnected;
            _serviceObjects = new ThreadSafeSortedList<string, ServiceObject>();
            _serviceClients = new ThreadSafeSortedList<long, IScsServiceClient>();
        }

        [CompilerGenerated] public event EventHandler<ServiceClientEventArgs> ClientConnected;

        [CompilerGenerated] public event EventHandler<ServiceClientEventArgs> ClientDisconnected;

        public void Start()
        {
            _scsServer.Start();
        }

        public void Stop()
        {
            _scsServer.Stop();
        }

        public void AddService<TServiceInterface, TServiceClass>(TServiceClass service)
            where TServiceInterface : class
            where TServiceClass : ScsService, TServiceInterface
        {
            if (service == null)
                throw new ArgumentNullException(paramName: nameof(service));
            var serviceInterfaceType = typeof(TServiceInterface);
            if (_serviceObjects[key: serviceInterfaceType.Name] != null)
                throw new Exception(message: "Service '" + serviceInterfaceType.Name + "' is already added before.");
            _serviceObjects[key: serviceInterfaceType.Name] =
                new ServiceObject(serviceInterfaceType: serviceInterfaceType, service: service);
        }

        public bool RemoveService<TServiceInterface>() where TServiceInterface : class
        {
            return _serviceObjects.Remove(key: typeof(TServiceInterface).Name);
        }

        void ScsServer_ClientConnected(object sender, ServerClientEventArgs e)
        {
            var requestReplyMessenger = new RequestReplyMessenger<IScsServerClient>(messenger: e.Client);
            requestReplyMessenger.MessageReceived += Client_MessageReceived;
            requestReplyMessenger.Start();
            var serviceClient = ScsServiceClientFactory.CreateServiceClient(serverClient: e.Client,
                requestReplyMessenger: requestReplyMessenger);
            _serviceClients[key: serviceClient.ClientId] = serviceClient;
            OnClientConnected(client: serviceClient);
        }

        void ScsServer_ClientDisconnected(object sender, ServerClientEventArgs e)
        {
            var serviceClient = _serviceClients[key: e.Client.ClientId];
            if (serviceClient == null)
                return;
            _serviceClients.Remove(key: e.Client.ClientId);
            OnClientDisconnected(client: serviceClient);
        }

        void Client_MessageReceived(object sender, MessageEventArgs e)
        {
            var requestReplyMessenger = (RequestReplyMessenger<IScsServerClient>)sender;
            ScsRemoteInvokeMessage message;
            message = e.Message as ScsRemoteInvokeMessage;
            if (message == null)
                return;
            try
            {
                var serviceClient = _serviceClients[key: requestReplyMessenger.Messenger.ClientId];
                if (serviceClient == null)
                {
                    requestReplyMessenger.Messenger.Disconnect();
                }
                else
                {
                    var serviceObject = _serviceObjects[key: message.ServiceClassName];
                    if (serviceObject == null)
                        SendInvokeResponse(client: requestReplyMessenger, requestMessage: message, returnValue: null,
                            exception: new ScsRemoteException(
                                message: "There is no service with name '" + message.ServiceClassName + "'"));
                    else
                        try
                        {
                            serviceObject.Service.CurrentClient = serviceClient;
                            object returnValue;
                            try
                            {
                                returnValue = serviceObject.InvokeMethod(methodName: message.MethodName,
                                    parameters: message.Parameters);
                            }
                            finally
                            {
                                serviceObject.Service.CurrentClient = null;
                            }

                            SendInvokeResponse(client: requestReplyMessenger, requestMessage: message,
                                returnValue: returnValue, exception: null);
                        }
                        catch (TargetInvocationException ex)
                        {
                            var innerException = ex.InnerException;
                            SendInvokeResponse(client: requestReplyMessenger, requestMessage: message,
                                returnValue: null,
                                exception: new ScsRemoteException(
                                    message: innerException.Message + Environment.NewLine + "Service Version: " +
                                             serviceObject.ServiceAttribute.Version, innerException: innerException));
                        }
                        catch (Exception ex)
                        {
                            SendInvokeResponse(client: requestReplyMessenger, requestMessage: message,
                                returnValue: null,
                                exception: new ScsRemoteException(
                                    message: ex.Message + Environment.NewLine + "Service Version: " +
                                             serviceObject.ServiceAttribute.Version, innerException: ex));
                        }
                }
            }
            catch (Exception ex)
            {
                SendInvokeResponse(client: requestReplyMessenger, requestMessage: message, returnValue: null,
                    exception: new ScsRemoteException(message: "An error occured during remote service method call.",
                        innerException: ex));
            }
        }

        static void SendInvokeResponse(
            IMessenger client,
            IScsMessage requestMessage,
            object returnValue,
            ScsRemoteException exception)
        {
            try
            {
                var messenger = client;
                var invokeReturnMessage1 = new ScsRemoteInvokeReturnMessage
                {
                    RepliedMessageId = requestMessage.MessageId,
                    ReturnValue = returnValue,
                    RemoteException = exception
                };
                var invokeReturnMessage2 = invokeReturnMessage1;
                messenger.SendMessage(message: invokeReturnMessage2);
            }
            catch
            {
            }
        }

        void OnClientConnected(IScsServiceClient client)
        {
            var clientConnected = ClientConnected;
            if (clientConnected == null)
                return;
            clientConnected(sender: this, e: new ServiceClientEventArgs(client: client));
        }

        void OnClientDisconnected(IScsServiceClient client)
        {
            var clientDisconnected = ClientDisconnected;
            if (clientDisconnected == null)
                return;
            clientDisconnected(sender: this, e: new ServiceClientEventArgs(client: client));
        }

        sealed class ServiceObject
        {
            readonly SortedList<string, MethodInfo> _methods;

            public ServiceObject(Type serviceInterfaceType, ScsService service)
            {
                Service = service;
                var customAttributes =
                    serviceInterfaceType.GetCustomAttributes(attributeType: typeof(ScsServiceAttribute), inherit: true);
                if (customAttributes.Length == 0)
                    throw new Exception(message: "Service interface (" + serviceInterfaceType.Name +
                                                 ") must has ScsService attribute.");
                ServiceAttribute = customAttributes[0] as ScsServiceAttribute;
                _methods = new SortedList<string, MethodInfo>();
                foreach (var method in serviceInterfaceType.GetMethods())
                    _methods.Add(key: method.Name, value: method);
            }

            public ScsService Service { get; }

            public ScsServiceAttribute ServiceAttribute { get; }

            public object InvokeMethod(string methodName, params object[] parameters)
            {
                if (!_methods.ContainsKey(key: methodName))
                    throw new Exception(message: "There is not a method with name '" + methodName +
                                                 "' in service class.");
                return _methods[key: methodName].Invoke(obj: Service, parameters: parameters);
            }
        }
    }
}