﻿using System;

namespace OpenNos.SCS.Communication.ScsServices.Service
{
    [AttributeUsage(validOn: AttributeTargets.Class | AttributeTargets.Interface)]
    public class ScsServiceAttribute : Attribute
    {
        public ScsServiceAttribute()
        {
            Version = "NO_VERSION";
        }

        public string Version { get; set; }
    }
}