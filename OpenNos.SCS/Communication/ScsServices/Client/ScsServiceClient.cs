﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using OpenNos.SCS.Communication.Scs.Client;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.Messages;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;
using OpenNos.SCS.Communication.ScsServices.Communication;
using OpenNos.SCS.Communication.ScsServices.Communication.Messages;

namespace OpenNos.SCS.Communication.ScsServices.Client
{
    class ScsServiceClient<T> : IScsServiceClient<T>, IConnectableClient, IDisposable
        where T : class
    {
        readonly IScsClient _client;
        readonly object _clientObject;
        readonly AutoConnectRemoteInvokeProxy<T, IScsClient> _realServiceProxy;
        readonly RequestReplyMessenger<IScsClient> _requestReplyMessenger;

        public ScsServiceClient(IScsClient client, object clientObject)
        {
            _client = client;
            _clientObject = clientObject;
            _client.Connected += Client_Connected;
            _client.Disconnected += Client_Disconnected;
            _requestReplyMessenger = new RequestReplyMessenger<IScsClient>(messenger: client);
            _requestReplyMessenger.MessageReceived += RequestReplyMessenger_MessageReceived;
            _realServiceProxy =
                new AutoConnectRemoteInvokeProxy<T, IScsClient>(clientMessenger: _requestReplyMessenger, client: this);
            ServiceProxy = (T) _realServiceProxy.GetTransparentProxy();
        }

        [CompilerGenerated] public event EventHandler Connected;

        [CompilerGenerated] public event EventHandler Disconnected;

        public int ConnectTimeout
        {
            get => _client.ConnectTimeout;
            set => _client.ConnectTimeout = value;
        }

        public CommunicationStates CommunicationState => _client.CommunicationState;

        public T ServiceProxy { get; }

        public int Timeout
        {
            get => _requestReplyMessenger.Timeout;
            set => _requestReplyMessenger.Timeout = value;
        }

        public void Connect()
        {
            _client.Connect();
        }

        public void Disconnect()
        {
            _client.Disconnect();
        }

        public void Dispose()
        {
            Disconnect();
        }

        void RequestReplyMessenger_MessageReceived(object sender, MessageEventArgs e)
        {
            ScsRemoteInvokeMessage message;
            message = e.Message as ScsRemoteInvokeMessage;
            if (message == null)
                return;
            if (_clientObject == null)
            {
                SendInvokeResponse(requestMessage: message, returnValue: null,
                    exception: new ScsRemoteException(
                        message: "Client does not wait for method invocations by server."));
            }
            else
            {
                object returnValue;
                try
                {
                    returnValue = _clientObject.GetType().GetMethod(name: message.MethodName)
                        .Invoke(obj: _clientObject, parameters: message.Parameters);
                }
                catch (TargetInvocationException ex)
                {
                    var innerException = ex.InnerException;
                    SendInvokeResponse(requestMessage: message, returnValue: null,
                        exception: new ScsRemoteException(message: innerException.Message,
                            innerException: innerException));
                    return;
                }
                catch (Exception ex)
                {
                    SendInvokeResponse(requestMessage: message, returnValue: null,
                        exception: new ScsRemoteException(message: ex.Message, innerException: ex));
                    return;
                }

                SendInvokeResponse(requestMessage: message, returnValue: returnValue, exception: null);
            }
        }

        void SendInvokeResponse(
            IScsMessage requestMessage,
            object returnValue,
            ScsRemoteException exception)
        {
            try
            {
                var requestReplyMessenger = _requestReplyMessenger;
                var invokeReturnMessage1 = new ScsRemoteInvokeReturnMessage
                {
                    RepliedMessageId = requestMessage.MessageId,
                    ReturnValue = returnValue,
                    RemoteException = exception
                };
                var invokeReturnMessage2 = invokeReturnMessage1;
                requestReplyMessenger.SendMessage(message: invokeReturnMessage2);
            }
            catch
            {
            }
        }

        void Client_Connected(object sender, EventArgs e)
        {
            _requestReplyMessenger.Start();
            OnConnected();
        }

        void Client_Disconnected(object sender, EventArgs e)
        {
            _requestReplyMessenger.Stop();
            OnDisconnected();
        }

        void OnConnected()
        {
            var connected = Connected;
            if (connected == null)
                return;
            connected(sender: this, e: EventArgs.Empty);
        }

        void OnDisconnected()
        {
            var disconnected = Disconnected;
            if (disconnected == null)
                return;
            disconnected(sender: this, e: EventArgs.Empty);
        }
    }
}