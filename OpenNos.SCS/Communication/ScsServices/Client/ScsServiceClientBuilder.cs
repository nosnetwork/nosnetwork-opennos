﻿using OpenNos.SCS.Communication.Scs.Communication.EndPoints;

namespace OpenNos.SCS.Communication.ScsServices.Client
{
    public class ScsServiceClientBuilder
    {
        public static IScsServiceClient<T> CreateClient<T>(
            ScsEndPoint endpoint,
            object clientObject = null)
            where T : class
        {
            if (Management.CheckLicence())
                return new ScsServiceClient<T>(client: endpoint.CreateClient(), clientObject: clientObject);
            return null;
        }

        public static IScsServiceClient<T> CreateClient<T>(
            string endpointAddress,
            object clientObject = null)
            where T : class
        {
            if (Management.CheckLicence())
                return CreateClient<T>(endpoint: ScsEndPoint.CreateEndPoint(endPointAddress: endpointAddress),
                    clientObject: clientObject);
            return null;
        }
    }
}