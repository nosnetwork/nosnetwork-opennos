﻿using System;
using OpenNos.SCS.Communication.Scs.Client;

namespace OpenNos.SCS.Communication.ScsServices.Client
{
    public interface IScsServiceClient<out T> : IConnectableClient, IDisposable
        where T : class
    {
        T ServiceProxy { get; }

        int Timeout { get; set; }
    }
}