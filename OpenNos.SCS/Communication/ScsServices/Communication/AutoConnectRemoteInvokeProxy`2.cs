﻿using System.Runtime.Remoting.Messaging;
using OpenNos.SCS.Communication.Scs.Client;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;

namespace OpenNos.SCS.Communication.ScsServices.Communication
{
    internal class AutoConnectRemoteInvokeProxy<TProxy, TMessenger> : RemoteInvokeProxy<TProxy, TMessenger>
        where TMessenger : IMessenger
    {
        readonly IConnectableClient _client;

        public AutoConnectRemoteInvokeProxy(
            RequestReplyMessenger<TMessenger> clientMessenger,
            IConnectableClient client)
            : base(clientMessenger: clientMessenger)
        {
            _client = client;
        }

        public override IMessage Invoke(IMessage msg)
        {
            if (_client.CommunicationState == CommunicationStates.Connected)
                return base.Invoke(msg: msg);
            _client.Connect();
            try
            {
                return base.Invoke(msg: msg);
            }
            finally
            {
                _client.Disconnect();
            }
        }
    }
}