﻿using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;
using OpenNos.SCS.Communication.ScsServices.Communication.Messages;

namespace OpenNos.SCS.Communication.ScsServices.Communication
{
    class RemoteInvokeProxy<TProxy, TMessenger> : RealProxy where TMessenger : IMessenger
    {
        readonly RequestReplyMessenger<TMessenger> _clientMessenger;

        public RemoteInvokeProxy(RequestReplyMessenger<TMessenger> clientMessenger)
            : base(classToProxy: typeof(TProxy))
        {
            _clientMessenger = clientMessenger;
        }

        public override IMessage Invoke(IMessage msg)
        {
            IMethodCallMessage mcm;
            mcm = msg as IMethodCallMessage;
            if (mcm == null)
                return null;
            var remoteInvokeMessage = new ScsRemoteInvokeMessage
            {
                ServiceClassName = typeof(TProxy).Name,
                MethodName = mcm.MethodName,
                Parameters = mcm.InArgs
            };
            var invokeReturnMessage = (ScsRemoteInvokeReturnMessage) null;
            if (remoteInvokeMessage.ServiceClassName.EndsWith(value: "Client"))
            {
                _clientMessenger.SendMessage(message: remoteInvokeMessage);
            }
            else
            {
                invokeReturnMessage =
                    _clientMessenger.SendMessageAndWaitForResponse(message: remoteInvokeMessage) as
                        ScsRemoteInvokeReturnMessage;
                if (invokeReturnMessage == null)
                    return null;
            }

            if (invokeReturnMessage == null)
                return new ReturnMessage(ret: null, outArgs: null, outArgsCount: 0, callCtx: mcm.LogicalCallContext,
                    mcm: mcm);
            if (invokeReturnMessage.RemoteException == null)
                return new ReturnMessage(ret: invokeReturnMessage.ReturnValue, outArgs: null, outArgsCount: 0,
                    callCtx: mcm.LogicalCallContext, mcm: mcm);
            return new ReturnMessage(e: invokeReturnMessage.RemoteException, mcm: mcm);
        }
    }
}