﻿using System;
using OpenNos.SCS.Communication.Scs.Communication.Messages;

namespace OpenNos.SCS.Communication.ScsServices.Communication.Messages
{
    [Serializable]
    public class ScsRemoteInvokeReturnMessage : ScsMessage
    {
        public object ReturnValue { get; set; }

        public ScsRemoteException RemoteException { get; set; }

        public override string ToString()
        {
            return string.Format(format: "ScsRemoteInvokeReturnMessage: Returns {0}, Exception = {1}",
                arg0: ReturnValue,
                arg1: RemoteException);
        }
    }
}