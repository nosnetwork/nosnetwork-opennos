﻿using System;
using System.Runtime.Serialization;

namespace OpenNos.SCS.Communication.ScsServices.Communication.Messages
{
    [Serializable]
    public class ScsRemoteException : Exception
    {
        public ScsRemoteException()
        {
        }

        public ScsRemoteException(SerializationInfo serializationInfo, StreamingContext context)
            : base(info: serializationInfo, context: context)
        {
        }

        public ScsRemoteException(string message)
            : base(message: message)
        {
        }

        public ScsRemoteException(string message, Exception innerException)
            : base(message: message, innerException: innerException)
        {
        }
    }
}