﻿using System;
using OpenNos.SCS.Communication.Scs.Communication.Messages;

namespace OpenNos.SCS.Communication.ScsServices.Communication.Messages
{
    [Serializable]
    public class ScsRemoteInvokeMessage : ScsMessage
    {
        public string ServiceClassName { get; set; }

        public string MethodName { get; set; }

        public object[] Parameters { get; set; }

        public override string ToString()
        {
            return string.Format(format: "ScsRemoteInvokeMessage: {0}.{1}(...)", arg0: ServiceClassName,
                arg1: MethodName);
        }
    }
}