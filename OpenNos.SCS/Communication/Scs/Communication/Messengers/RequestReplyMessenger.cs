﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using OpenNos.SCS.Communication.Scs.Communication.Messages;
using OpenNos.SCS.Communication.Scs.Communication.Protocols;
using OpenNos.SCS.Threading;

namespace OpenNos.SCS.Communication.Scs.Communication.Messengers
{
    public class RequestReplyMessenger<T> : IMessenger, IDisposable where T : IMessenger
    {
        const int DefaultTimeout = 60000;
        readonly SequentialItemProcessor<IScsMessage> _incomingMessageProcessor;
        readonly object _syncObj = new object();
        readonly SortedList<string, WaitingMessage> _waitingMessages;

        public RequestReplyMessenger(T messenger)
        {
            Messenger = messenger;
            messenger.MessageReceived += Messenger_MessageReceived;
            messenger.MessageSent += Messenger_MessageSent;
            _incomingMessageProcessor = new SequentialItemProcessor<IScsMessage>(processMethod: OnMessageReceived);
            _waitingMessages = new SortedList<string, WaitingMessage>();
            Timeout = 60000;
        }

        public T Messenger { get; }

        public int Timeout { get; set; }

        public void Dispose()
        {
            Stop();
        }

        [CompilerGenerated] public event EventHandler<MessageEventArgs> MessageReceived;

        [CompilerGenerated] public event EventHandler<MessageEventArgs> MessageSent;

        public IScsWireProtocol WireProtocol
        {
            get => Messenger.WireProtocol;
            set => Messenger.WireProtocol = value;
        }

        public DateTime LastReceivedMessageTime => Messenger.LastReceivedMessageTime;

        public DateTime LastSentMessageTime => Messenger.LastSentMessageTime;

        public void SendMessage(IScsMessage message)
        {
            Messenger.SendMessage(message: message);
        }

        public virtual void Start()
        {
            _incomingMessageProcessor.Start();
        }

        public virtual void Stop()
        {
            _incomingMessageProcessor.Stop();
            lock (_syncObj)
            {
                foreach (var waitingMessage in _waitingMessages.Values)
                {
                    waitingMessage.State = WaitingMessageStates.Cancelled;
                    waitingMessage.WaitEvent.Set();
                }

                _waitingMessages.Clear();
            }
        }

        public IScsMessage SendMessageAndWaitForResponse(IScsMessage message)
        {
            return SendMessageAndWaitForResponse(message: message, timeoutMilliseconds: Timeout);
        }

        public IScsMessage SendMessageAndWaitForResponse(
            IScsMessage message,
            int timeoutMilliseconds)
        {
            var waitingMessage = new WaitingMessage();
            lock (_syncObj)
            {
                _waitingMessages[key: message.MessageId] = waitingMessage;
            }

            try
            {
                Messenger.SendMessage(message: message);
                waitingMessage.WaitEvent.Wait(millisecondsTimeout: timeoutMilliseconds);
                switch (waitingMessage.State)
                {
                    case WaitingMessageStates.WaitingForResponse:
                        throw new TimeoutException(message: "Timeout occured. Can not received response.");
                    case WaitingMessageStates.Cancelled:
                        throw new CommunicationException(message: "Disconnected before response received.");
                    default:
                        return waitingMessage.ResponseMessage;
                }
            }
            finally
            {
                lock (_syncObj)
                {
                    if (_waitingMessages.ContainsKey(key: message.MessageId))
                        _waitingMessages.Remove(key: message.MessageId);
                }
            }
        }

        void Messenger_MessageReceived(object sender, MessageEventArgs e)
        {
            if (!string.IsNullOrEmpty(value: e.Message.RepliedMessageId))
            {
                var waitingMessage = (WaitingMessage) null;
                lock (_syncObj)
                {
                    if (_waitingMessages.ContainsKey(key: e.Message.RepliedMessageId))
                        waitingMessage = _waitingMessages[key: e.Message.RepliedMessageId];
                }

                if (waitingMessage != null)
                {
                    waitingMessage.ResponseMessage = e.Message;
                    waitingMessage.State = WaitingMessageStates.ResponseReceived;
                    waitingMessage.WaitEvent.Set();
                    return;
                }
            }

            _incomingMessageProcessor.EnqueueMessage(item: e.Message);
        }

        void Messenger_MessageSent(object sender, MessageEventArgs e)
        {
            OnMessageSent(message: e.Message);
        }

        protected virtual void OnMessageReceived(IScsMessage message)
        {
            var messageReceived = MessageReceived;
            if (messageReceived == null)
                return;
            messageReceived(sender: this, e: new MessageEventArgs(message: message));
        }

        protected virtual void OnMessageSent(IScsMessage message)
        {
            var messageSent = MessageSent;
            if (messageSent == null)
                return;
            messageSent(sender: this, e: new MessageEventArgs(message: message));
        }

        sealed class WaitingMessage
        {
            public WaitingMessage()
            {
                WaitEvent = new ManualResetEventSlim(initialState: false);
                State = WaitingMessageStates.WaitingForResponse;
            }

            public IScsMessage ResponseMessage { get; set; }

            public ManualResetEventSlim WaitEvent { get; }

            public WaitingMessageStates State { get; set; }
        }

        enum WaitingMessageStates
        {
            WaitingForResponse,
            Cancelled,
            ResponseReceived
        }
    }
}