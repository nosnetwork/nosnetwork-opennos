﻿using System;
using OpenNos.SCS.Communication.Scs.Communication.Messages;
using OpenNos.SCS.Communication.Scs.Communication.Protocols;

namespace OpenNos.SCS.Communication.Scs.Communication.Messengers
{
    public interface IMessenger
    {
        IScsWireProtocol WireProtocol { get; set; }

        DateTime LastReceivedMessageTime { get; }

        DateTime LastSentMessageTime { get; }
        event EventHandler<MessageEventArgs> MessageReceived;

        event EventHandler<MessageEventArgs> MessageSent;

        void SendMessage(IScsMessage message);
    }
}