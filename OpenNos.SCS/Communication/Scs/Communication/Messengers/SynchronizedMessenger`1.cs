﻿using System;
using System.Collections.Generic;
using System.Threading;
using OpenNos.SCS.Communication.Scs.Communication.Messages;

namespace OpenNos.SCS.Communication.Scs.Communication.Messengers
{
    public class SynchronizedMessenger<T> : RequestReplyMessenger<T> where T : IMessenger
    {
        readonly ManualResetEventSlim _receiveWaiter;
        readonly Queue<IScsMessage> _receivingMessageQueue;
        volatile bool _running;

        public SynchronizedMessenger(T messenger)
            : this(messenger: messenger, incomingMessageQueueCapacity: int.MaxValue)
        {
        }

        public SynchronizedMessenger(T messenger, int incomingMessageQueueCapacity)
            : base(messenger: messenger)
        {
            _receiveWaiter = new ManualResetEventSlim();
            _receivingMessageQueue = new Queue<IScsMessage>();
            IncomingMessageQueueCapacity = incomingMessageQueueCapacity;
        }

        public int IncomingMessageQueueCapacity { get; set; }

        public override void Start()
        {
            lock (_receivingMessageQueue)
            {
                _running = true;
            }

            base.Start();
        }

        public override void Stop()
        {
            base.Stop();
            lock (_receivingMessageQueue)
            {
                _running = false;
                _receiveWaiter.Set();
            }
        }

        public IScsMessage ReceiveMessage()
        {
            return ReceiveMessage(timeout: -1);
        }

        public IScsMessage ReceiveMessage(int timeout)
        {
            while (_running)
            {
                lock (_receivingMessageQueue)
                {
                    if (!_running)
                        throw new Exception(message: "SynchronizedMessenger is stopped. Can not receive message.");
                    if (_receivingMessageQueue.Count > 0)
                        return _receivingMessageQueue.Dequeue();
                    _receiveWaiter.Reset();
                }

                if (!_receiveWaiter.Wait(millisecondsTimeout: timeout))
                    throw new TimeoutException(message: "Timeout occured. Can not received any message");
            }

            throw new Exception(message: "SynchronizedMessenger is stopped. Can not receive message.");
        }

        public TMessage ReceiveMessage<TMessage>() where TMessage : IScsMessage
        {
            return ReceiveMessage<TMessage>(timeout: -1);
        }

        public TMessage ReceiveMessage<TMessage>(int timeout) where TMessage : IScsMessage
        {
            var message = ReceiveMessage(timeout: timeout);
            if (!(message is TMessage))
                throw new Exception(message: "Unexpected message received. Expected type: " + typeof(TMessage).Name +
                                             ". Received message type: " + message.GetType().Name);
            return (TMessage)message;
        }

        protected override void OnMessageReceived(IScsMessage message)
        {
            lock (_receivingMessageQueue)
            {
                if (_receivingMessageQueue.Count < IncomingMessageQueueCapacity)
                    _receivingMessageQueue.Enqueue(item: message);
                _receiveWaiter.Set();
            }
        }
    }
}