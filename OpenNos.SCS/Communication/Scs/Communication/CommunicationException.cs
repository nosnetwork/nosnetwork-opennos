﻿using System;
using System.Runtime.Serialization;

namespace OpenNos.SCS.Communication.Scs.Communication
{
    [Serializable]
    public class CommunicationException : Exception
    {
        public CommunicationException()
        {
        }

        public CommunicationException(SerializationInfo serializationInfo, StreamingContext context)
            : base(info: serializationInfo, context: context)
        {
        }

        public CommunicationException(string message)
            : base(message: message)
        {
        }

        public CommunicationException(string message, Exception innerException)
            : base(message: message, innerException: innerException)
        {
        }
    }
}