﻿using System;
using System.Runtime.CompilerServices;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints;
using OpenNos.SCS.Communication.Scs.Communication.Messages;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;
using OpenNos.SCS.Communication.Scs.Communication.Protocols;

namespace OpenNos.SCS.Communication.Scs.Communication.Channels
{
    internal abstract class CommunicationChannelBase : ICommunicationChannel, IMessenger
    {
        protected CommunicationChannelBase()
        {
            CommunicationState = CommunicationStates.Disconnected;
            LastReceivedMessageTime = DateTime.MinValue;
            LastSentMessageTime = DateTime.MinValue;
        }

        [CompilerGenerated] public event EventHandler<MessageEventArgs> MessageReceived;

        [CompilerGenerated] public event EventHandler<MessageEventArgs> MessageSent;

        [CompilerGenerated] public event EventHandler Disconnected;

        public abstract ScsEndPoint RemoteEndPoint { get; }

        public CommunicationStates CommunicationState { get; protected set; }

        public DateTime LastReceivedMessageTime { get; protected set; }

        public DateTime LastSentMessageTime { get; protected set; }

        public IScsWireProtocol WireProtocol { get; set; }

        public abstract void Disconnect();

        public void Start()
        {
            StartInternal();
            CommunicationState = CommunicationStates.Connected;
        }

        public void SendMessage(IScsMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(paramName: nameof(message));
            SendMessageInternal(message: message);
        }

        protected abstract void StartInternal();

        protected abstract void SendMessageInternal(IScsMessage message);

        protected virtual void OnDisconnected()
        {
            var disconnected = Disconnected;
            if (disconnected == null)
                return;
            disconnected(sender: this, e: EventArgs.Empty);
        }

        protected virtual void OnMessageReceived(IScsMessage message)
        {
            var messageReceived = MessageReceived;
            if (messageReceived == null)
                return;
            messageReceived(sender: this, e: new MessageEventArgs(message: message));
        }

        protected virtual void OnMessageSent(IScsMessage message)
        {
            var messageSent = MessageSent;
            if (messageSent == null)
                return;
            messageSent(sender: this, e: new MessageEventArgs(message: message));
        }
    }
}