﻿using System.Net;
using System.Net.Sockets;
using System.Threading;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;

namespace OpenNos.SCS.Communication.Scs.Communication.Channels.Tcp
{
    internal class TcpConnectionListener : ConnectionListenerBase
    {
        readonly ScsTcpEndPoint _endPoint;
        TcpListener _listenerSocket;
        volatile bool _running;
        Thread _thread;

        public TcpConnectionListener(ScsTcpEndPoint endPoint)
        {
            _endPoint = endPoint;
        }

        public override void Start()
        {
            StartSocket();
            _running = true;
            _thread = new Thread(start: DoListenAsThread);
            _thread.Start();
        }

        public override void Stop()
        {
            _running = false;
            StopSocket();
        }

        void StartSocket()
        {
            _listenerSocket = new TcpListener(localaddr: IPAddress.Any, port: _endPoint.TcpPort);
            _listenerSocket.Start();
        }

        void StopSocket()
        {
            try
            {
                _listenerSocket.Stop();
            }
            catch
            {
            }
        }

        void DoListenAsThread()
        {
            while (_running)
                try
                {
                    var clientSocket = _listenerSocket.AcceptSocket();
                    if (clientSocket.Connected)
                        OnCommunicationChannelConnected(
                            client: new TcpCommunicationChannel(clientSocket: clientSocket));
                }
                catch
                {
                    StopSocket();
                    Thread.Sleep(millisecondsTimeout: 1000);
                    if (!_running)
                        break;
                    try
                    {
                        StartSocket();
                    }
                    catch
                    {
                    }
                }
        }
    }
}