﻿using System;
using System.Net;
using System.Net.Sockets;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.SCS.Communication.Scs.Communication.Messages;

namespace OpenNos.SCS.Communication.Scs.Communication.Channels.Tcp
{
    internal class TcpCommunicationChannel : CommunicationChannelBase
    {
        const int ReceiveBufferSize = 4096;
        readonly byte[] _buffer;
        readonly Socket _clientSocket;
        readonly ScsTcpEndPoint _remoteEndPoint;
        readonly object _syncLock;
        volatile bool _running;

        public TcpCommunicationChannel(Socket clientSocket)
        {
            _clientSocket = clientSocket;
            _clientSocket.NoDelay = true;
            var remoteEndPoint = (IPEndPoint)_clientSocket.RemoteEndPoint;
            _remoteEndPoint =
                new ScsTcpEndPoint(ipAddress: remoteEndPoint.Address.ToString(), port: remoteEndPoint.Port);
            _buffer = new byte[4096];
            _syncLock = new object();
        }

        public override ScsEndPoint RemoteEndPoint
        {
            get => _remoteEndPoint;
        }

        public override void Disconnect()
        {
            if (CommunicationState != CommunicationStates.Connected)
                return;
            _running = false;
            try
            {
                if (_clientSocket.Connected)
                    _clientSocket.Close();
                _clientSocket.Dispose();
            }
            catch
            {
            }

            CommunicationState = CommunicationStates.Disconnected;
            OnDisconnected();
        }

        protected override void StartInternal()
        {
            _running = true;
            _clientSocket.BeginReceive(buffer: _buffer, offset: 0, size: _buffer.Length, socketFlags: SocketFlags.None,
                callback: ReceiveCallback, state: null);
        }

        protected override void SendMessageInternal(IScsMessage message)
        {
            var offset = 0;
            lock (_syncLock)
            {
                int num;
                for (var bytes = WireProtocol.GetBytes(message: message); offset < bytes.Length; offset += num)
                {
                    num = _clientSocket.Send(buffer: bytes, offset: offset, size: bytes.Length - offset,
                        socketFlags: SocketFlags.None);
                    if (num <= 0)
                        throw new CommunicationException(message: "Message could not be sent via TCP socket. Only " +
                                                                  offset +
                                                                  " bytes of " + bytes.Length + " bytes are sent.");
                }

                LastSentMessageTime = DateTime.Now;
                OnMessageSent(message: message);
            }
        }

        void ReceiveCallback(IAsyncResult ar)
        {
            if (!_running)
                return;
            try
            {
                var length = _clientSocket.EndReceive(asyncResult: ar);
                if (length <= 0)
                    throw new CommunicationException(message: "Tcp socket is closed");
                LastReceivedMessageTime = DateTime.Now;
                var receivedBytes = new byte[length];
                Array.Copy(sourceArray: _buffer, sourceIndex: 0, destinationArray: receivedBytes, destinationIndex: 0,
                    length: length);
                foreach (var message in WireProtocol.CreateMessages(receivedBytes: receivedBytes))
                    OnMessageReceived(message: message);
                if (!_running)
                    return;
                _clientSocket.BeginReceive(buffer: _buffer, offset: 0, size: _buffer.Length,
                    socketFlags: SocketFlags.None, callback: ReceiveCallback, state: null);
            }
            catch
            {
                Disconnect();
            }
        }
    }
}