﻿using System;
using System.Runtime.CompilerServices;

namespace OpenNos.SCS.Communication.Scs.Communication.Channels
{
    internal abstract class ConnectionListenerBase : IConnectionListener
    {
        [CompilerGenerated] public event EventHandler<CommunicationChannelEventArgs> CommunicationChannelConnected;

        public abstract void Start();

        public abstract void Stop();

        protected virtual void OnCommunicationChannelConnected(ICommunicationChannel client)
        {
            var channelConnected = CommunicationChannelConnected;
            if (channelConnected == null)
                return;
            channelConnected(sender: this, e: new CommunicationChannelEventArgs(channel: client));
        }
    }
}