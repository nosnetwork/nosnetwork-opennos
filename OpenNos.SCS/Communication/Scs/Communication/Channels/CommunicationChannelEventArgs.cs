﻿using System;

namespace OpenNos.SCS.Communication.Scs.Communication.Channels
{
    internal class CommunicationChannelEventArgs : EventArgs
    {
        public CommunicationChannelEventArgs(ICommunicationChannel channel)
        {
            Channel = channel;
        }

        public ICommunicationChannel Channel { get; }
    }
}