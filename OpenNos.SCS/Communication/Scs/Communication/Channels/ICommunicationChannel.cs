﻿using System;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;

namespace OpenNos.SCS.Communication.Scs.Communication.Channels
{
    internal interface ICommunicationChannel : IMessenger
    {
        ScsEndPoint RemoteEndPoint { get; }

        CommunicationStates CommunicationState { get; }
        event EventHandler Disconnected;

        void Start();

        void Disconnect();
    }
}