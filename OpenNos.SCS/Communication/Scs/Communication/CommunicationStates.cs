﻿namespace OpenNos.SCS.Communication.Scs.Communication
{
    public enum CommunicationStates
    {
        Connected,
        Disconnected
    }
}