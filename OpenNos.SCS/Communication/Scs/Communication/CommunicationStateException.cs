﻿using System;
using System.Runtime.Serialization;

namespace OpenNos.SCS.Communication.Scs.Communication
{
    [Serializable]
    public class CommunicationStateException : CommunicationException
    {
        public CommunicationStateException()
        {
        }

        public CommunicationStateException(
            SerializationInfo serializationInfo,
            StreamingContext context)
            : base(serializationInfo: serializationInfo, context: context)
        {
        }

        public CommunicationStateException(string message)
            : base(message: message)
        {
        }

        public CommunicationStateException(string message, Exception innerException)
            : base(message: message, innerException: innerException)
        {
        }
    }
}