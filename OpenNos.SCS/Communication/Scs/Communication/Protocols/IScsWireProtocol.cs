﻿using System.Collections.Generic;
using OpenNos.SCS.Communication.Scs.Communication.Messages;

namespace OpenNos.SCS.Communication.Scs.Communication.Protocols
{
    public interface IScsWireProtocol
    {
        byte[] GetBytes(IScsMessage message);

        IEnumerable<IScsMessage> CreateMessages(byte[] receivedBytes);

        void Reset();
    }
}