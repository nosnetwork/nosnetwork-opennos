﻿namespace OpenNos.SCS.Communication.Scs.Communication.Protocols
{
    public interface IScsWireProtocolFactory
    {
        IScsWireProtocol CreateWireProtocol();
    }
}