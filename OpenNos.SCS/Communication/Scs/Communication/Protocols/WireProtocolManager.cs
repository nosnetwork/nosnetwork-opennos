﻿using OpenNos.SCS.Communication.Scs.Communication.Protocols.BinarySerialization;

namespace OpenNos.SCS.Communication.Scs.Communication.Protocols
{
    internal static class WireProtocolManager
    {
        public static IScsWireProtocolFactory GetDefaultWireProtocolFactory()
        {
            return new BinarySerializationProtocolFactory();
        }

        public static IScsWireProtocol GetDefaultWireProtocol()
        {
            return new BinarySerializationProtocol();
        }
    }
}