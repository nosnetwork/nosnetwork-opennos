﻿namespace OpenNos.SCS.Communication.Scs.Communication.Protocols.BinarySerialization
{
    public class BinarySerializationProtocolFactory : IScsWireProtocolFactory
    {
        public IScsWireProtocol CreateWireProtocol()
        {
            return new BinarySerializationProtocol();
        }
    }
}