﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using OpenNos.SCS.Communication.Scs.Communication.Messages;

namespace OpenNos.SCS.Communication.Scs.Communication.Protocols.BinarySerialization
{
    public class BinarySerializationProtocol : IScsWireProtocol
    {
        MemoryStream _receiveMemoryStream;

        public BinarySerializationProtocol()
        {
            _receiveMemoryStream = new MemoryStream();
        }

        public byte[] GetBytes(IScsMessage message)
        {
            var numArray = SerializeMessage(message: message);
            var length = numArray.Length;
            if (length > 134217728)
                throw new CommunicationException(message: "Message is too big (" + length +
                                                          " bytes). Max allowed length is " +
                                                          134217728 + " bytes.");
            var buffer = new byte[length + 4];
            WriteInt32(buffer: buffer, startIndex: 0, number: length);
            Array.Copy(sourceArray: numArray, sourceIndex: 0, destinationArray: buffer, destinationIndex: 4,
                length: length);
            return buffer;
        }

        public IEnumerable<IScsMessage> CreateMessages(byte[] receivedBytes)
        {
            _receiveMemoryStream.Write(buffer: receivedBytes, offset: 0, count: receivedBytes.Length);
            var scsMessageList = new List<IScsMessage>();
            do
            {
                ;
            } while (ReadSingleMessage(messages: scsMessageList));

            return scsMessageList;
        }

        public void Reset()
        {
            if (_receiveMemoryStream.Length <= 0L)
                return;
            _receiveMemoryStream = new MemoryStream();
        }

        protected virtual byte[] SerializeMessage(IScsMessage message)
        {
            using (var memoryStream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(serializationStream: memoryStream, graph: message);
                return memoryStream.ToArray();
            }
        }

        protected virtual IScsMessage DeserializeMessage(byte[] bytes)
        {
            using (var memoryStream = new MemoryStream(buffer: bytes))
            {
                memoryStream.Position = 0L;
                return (IScsMessage)new BinaryFormatter
                {
                    AssemblyFormat = FormatterAssemblyStyle.Simple,
                    Binder = new DeserializationAppDomainBinder()
                }.Deserialize(serializationStream: memoryStream);
            }
        }

        bool ReadSingleMessage(ICollection<IScsMessage> messages)
        {
            _receiveMemoryStream.Position = 0L;
            if (_receiveMemoryStream.Length < 4L)
                return false;
            var length = ReadInt32(stream: _receiveMemoryStream);
            if (length > 134217728)
                throw new Exception(message: "Message is too big (" + length + " bytes). Max allowed length is " +
                                             134217728 +
                                             " bytes.");
            if (length == 0)
            {
                if (_receiveMemoryStream.Length == 4L)
                {
                    _receiveMemoryStream = new MemoryStream();
                    return false;
                }

                var array = _receiveMemoryStream.ToArray();
                _receiveMemoryStream = new MemoryStream();
                _receiveMemoryStream.Write(buffer: array, offset: 4, count: array.Length - 4);
                return true;
            }

            if (_receiveMemoryStream.Length < 4 + length)
            {
                _receiveMemoryStream.Position = _receiveMemoryStream.Length;
                return false;
            }

            var bytes = ReadByteArray(stream: _receiveMemoryStream, length: length);
            messages.Add(item: DeserializeMessage(bytes: bytes));
            var buffer = ReadByteArray(stream: _receiveMemoryStream,
                length: (int)(_receiveMemoryStream.Length - (4 + length)));
            _receiveMemoryStream = new MemoryStream();
            _receiveMemoryStream.Write(buffer: buffer, offset: 0, count: buffer.Length);
            return buffer.Length > 4;
        }

        static void WriteInt32(byte[] buffer, int startIndex, int number)
        {
            buffer[startIndex] = (byte)(number >> 24 & byte.MaxValue);
            buffer[startIndex + 1] = (byte)(number >> 16 & byte.MaxValue);
            buffer[startIndex + 2] = (byte)(number >> 8 & byte.MaxValue);
            buffer[startIndex + 3] = (byte)(number & byte.MaxValue);
        }

        static int ReadInt32(Stream stream)
        {
            var numArray = ReadByteArray(stream: stream, length: 4);
            return numArray[0] << 24 | numArray[1] << 16 | numArray[2] << 8 | numArray[3];
        }

        static byte[] ReadByteArray(Stream stream, int length)
        {
            var buffer = new byte[length];
            int num;
            for (var offset = 0; offset < length; offset += num)
            {
                num = stream.Read(buffer: buffer, offset: offset, count: length - offset);
                if (num <= 0)
                    throw new EndOfStreamException(message: "Can not read from stream! Input stream is closed.");
            }

            return buffer;
        }

        protected sealed class DeserializationAppDomainBinder : SerializationBinder
        {
            public override Type BindToType(string assemblyName, string typeName)
            {
                var toAssemblyName = assemblyName.Split(',')[0];
                return AppDomain.CurrentDomain.GetAssemblies()
                    .Where(predicate: assembly => assembly.FullName.Split(',')[0] == toAssemblyName)
                    .Select(selector: assembly => assembly.GetType(name: typeName)).FirstOrDefault();
            }
        }
    }
}