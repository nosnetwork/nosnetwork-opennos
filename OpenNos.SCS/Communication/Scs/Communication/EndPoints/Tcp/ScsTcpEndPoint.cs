﻿using System;
using OpenNos.SCS.Communication.Scs.Client;
using OpenNos.SCS.Communication.Scs.Client.Tcp;
using OpenNos.SCS.Communication.Scs.Server;
using OpenNos.SCS.Communication.Scs.Server.Tcp;

namespace OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp
{
    public sealed class ScsTcpEndPoint : ScsEndPoint
    {
        public ScsTcpEndPoint(int tcpPort)
        {
            TcpPort = tcpPort;
        }

        public ScsTcpEndPoint(string ipAddress, int port)
        {
            IpAddress = ipAddress;
            TcpPort = port;
        }

        public ScsTcpEndPoint(string address)
        {
            var strArray = address.Trim().Split(':');
            IpAddress = strArray[0].Trim();
            TcpPort = Convert.ToInt32(value: strArray[1].Trim());
        }

        public string IpAddress { get; set; }

        public int TcpPort { get; }

        internal override IScsServer CreateServer()
        {
            return new ScsTcpServer(endPoint: this);
        }

        internal override IScsClient CreateClient()
        {
            return new ScsTcpClient(serverEndPoint: this);
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(value: IpAddress))
                return "tcp://" + TcpPort;
            return "tcp://" + IpAddress + ":" + TcpPort;
        }
    }
}