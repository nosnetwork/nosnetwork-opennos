﻿using System;
using OpenNos.SCS.Communication.Scs.Client;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.SCS.Communication.Scs.Server;

namespace OpenNos.SCS.Communication.Scs.Communication.EndPoints
{
    public abstract class ScsEndPoint
    {
        public static ScsEndPoint CreateEndPoint(string endPointAddress)
        {
            if (string.IsNullOrEmpty(value: endPointAddress))
                throw new ArgumentNullException(paramName: nameof(endPointAddress));
            var str = endPointAddress;
            if (!str.Contains(value: "://"))
                str = "tcp://" + str;
            var strArray = str.Split(separator: new string[1] { "://" }, options: StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length != 2)
                throw new ApplicationException(message: endPointAddress + " is not a valid endpoint address.");
            var lower = strArray[0].Trim().ToLower();
            var address = strArray[1].Trim();
            if (lower == "tcp")
                return new ScsTcpEndPoint(address: address);
            throw new ApplicationException(
                message: "Unsupported protocol " + lower + " in end point " + endPointAddress);
        }

        internal abstract IScsServer CreateServer();

        internal abstract IScsClient CreateClient();
    }
}