﻿using System;

namespace OpenNos.SCS.Communication.Scs.Communication.Messages
{
    [Serializable]
    public class ScsTextMessage : ScsMessage
    {
        public ScsTextMessage()
        {
        }

        public ScsTextMessage(string text)
        {
            Text = text;
        }

        public ScsTextMessage(string text, string repliedMessageId)
            : this(text: text)
        {
            RepliedMessageId = repliedMessageId;
        }

        public string Text { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(value: RepliedMessageId))
                return string.Format(format: "ScsTextMessage [{0}] Replied To [{1}]: {2}", arg0: MessageId,
                    arg1: RepliedMessageId, arg2: Text);
            return string.Format(format: "ScsTextMessage [{0}]: {1}", arg0: MessageId, arg1: Text);
        }
    }
}