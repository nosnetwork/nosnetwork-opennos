﻿using System;

namespace OpenNos.SCS.Communication.Scs.Communication.Messages
{
    [Serializable]
    public class ScsRawDataMessage : ScsMessage
    {
        public ScsRawDataMessage()
        {
        }

        public ScsRawDataMessage(byte[] messageData)
        {
            MessageData = messageData;
        }

        public ScsRawDataMessage(byte[] messageData, string repliedMessageId)
            : this(messageData: messageData)
        {
            RepliedMessageId = repliedMessageId;
        }

        public byte[] MessageData { get; set; }

        public override string ToString()
        {
            var num = MessageData == null ? 0 : MessageData.Length;
            if (!string.IsNullOrEmpty(value: RepliedMessageId))
                return string.Format(format: "ScsRawDataMessage [{0}] Replied To [{1}]: {2} bytes", arg0: MessageId,
                    arg1: RepliedMessageId,
                    arg2: num);
            return string.Format(format: "ScsRawDataMessage [{0}]: {1} bytes", arg0: MessageId, arg1: num);
        }
    }
}