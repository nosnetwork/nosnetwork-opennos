﻿using System;

namespace OpenNos.SCS.Communication.Scs.Communication.Messages
{
    [Serializable]
    public sealed class ScsPingMessage : ScsMessage
    {
        public ScsPingMessage()
        {
        }

        public ScsPingMessage(string repliedMessageId)
            : this()
        {
            RepliedMessageId = repliedMessageId;
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(value: RepliedMessageId))
                return string.Format(format: "ScsPingMessage [{0}] Replied To [{1}]", arg0: MessageId,
                    arg1: RepliedMessageId);
            return string.Format(format: "ScsPingMessage [{0}]", arg0: MessageId);
        }
    }
}