﻿using System;

namespace OpenNos.SCS.Communication.Scs.Communication.Messages
{
    public class MessageEventArgs : EventArgs
    {
        public MessageEventArgs(IScsMessage message)
        {
            Message = message;
        }

        public IScsMessage Message { get; }
    }
}