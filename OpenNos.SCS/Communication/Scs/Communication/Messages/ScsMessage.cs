﻿using System;

namespace OpenNos.SCS.Communication.Scs.Communication.Messages
{
    [Serializable]
    public class ScsMessage : IScsMessage
    {
        public ScsMessage()
        {
            MessageId = Guid.NewGuid().ToString();
        }

        public ScsMessage(string repliedMessageId)
            : this()
        {
            RepliedMessageId = repliedMessageId;
        }

        public string MessageId { get; set; }

        public string RepliedMessageId { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(value: RepliedMessageId))
                return string.Format(format: "ScsMessage [{0}] Replied To [{1}]", arg0: MessageId,
                    arg1: RepliedMessageId);
            return string.Format(format: "ScsMessage [{0}]", arg0: MessageId);
        }
    }
}