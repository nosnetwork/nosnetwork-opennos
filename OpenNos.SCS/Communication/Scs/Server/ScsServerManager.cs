﻿using System.Threading;

namespace OpenNos.SCS.Communication.Scs.Server
{
    internal static class ScsServerManager
    {
        static long _lastClientId;

        public static long GetClientId()
        {
            return Interlocked.Increment(location: ref _lastClientId);
        }
    }
}