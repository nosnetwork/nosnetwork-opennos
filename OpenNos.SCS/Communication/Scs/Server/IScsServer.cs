﻿using System;
using OpenNos.SCS.Collections;
using OpenNos.SCS.Communication.Scs.Communication.Protocols;

namespace OpenNos.SCS.Communication.Scs.Server
{
    public interface IScsServer
    {
        IScsWireProtocolFactory WireProtocolFactory { get; set; }

        ThreadSafeSortedList<long, IScsServerClient> Clients { get; }
        event EventHandler<ServerClientEventArgs> ClientConnected;

        event EventHandler<ServerClientEventArgs> ClientDisconnected;

        void Start();

        void Stop();
    }
}