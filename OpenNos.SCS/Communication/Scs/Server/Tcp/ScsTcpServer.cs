﻿using OpenNos.SCS.Communication.Scs.Communication.Channels;
using OpenNos.SCS.Communication.Scs.Communication.Channels.Tcp;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;

namespace OpenNos.SCS.Communication.Scs.Server.Tcp
{
    internal class ScsTcpServer : ScsServerBase
    {
        readonly ScsTcpEndPoint _endPoint;

        public ScsTcpServer(ScsTcpEndPoint endPoint)
        {
            _endPoint = endPoint;
        }

        protected override IConnectionListener CreateConnectionListener()
        {
            return new TcpConnectionListener(endPoint: _endPoint);
        }
    }
}