﻿using System;
using System.Runtime.CompilerServices;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.Channels;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints;
using OpenNos.SCS.Communication.Scs.Communication.Messages;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;
using OpenNos.SCS.Communication.Scs.Communication.Protocols;

namespace OpenNos.SCS.Communication.Scs.Server
{
    internal class ScsServerClient : IScsServerClient, IMessenger
    {
        readonly ICommunicationChannel _communicationChannel;

        public ScsServerClient(ICommunicationChannel communicationChannel)
        {
            _communicationChannel = communicationChannel;
            _communicationChannel.MessageReceived += CommunicationChannel_MessageReceived;
            _communicationChannel.MessageSent += CommunicationChannel_MessageSent;
            _communicationChannel.Disconnected += CommunicationChannel_Disconnected;
        }

        [CompilerGenerated] public event EventHandler<MessageEventArgs> MessageReceived;

        [CompilerGenerated] public event EventHandler<MessageEventArgs> MessageSent;

        [CompilerGenerated] public event EventHandler Disconnected;

        public long ClientId { get; set; }

        public CommunicationStates CommunicationState
        {
            get => _communicationChannel.CommunicationState;
        }

        public IScsWireProtocol WireProtocol
        {
            get => _communicationChannel.WireProtocol;
            set => _communicationChannel.WireProtocol = value;
        }

        public ScsEndPoint RemoteEndPoint
        {
            get => _communicationChannel.RemoteEndPoint;
        }

        public DateTime LastReceivedMessageTime
        {
            get => _communicationChannel.LastReceivedMessageTime;
        }

        public DateTime LastSentMessageTime
        {
            get => _communicationChannel.LastSentMessageTime;
        }

        public void Disconnect()
        {
            _communicationChannel.Disconnect();
        }

        public void SendMessage(IScsMessage message)
        {
            _communicationChannel.SendMessage(message: message);
        }

        void CommunicationChannel_Disconnected(object sender, EventArgs e)
        {
            OnDisconnected();
        }

        void CommunicationChannel_MessageReceived(object sender, MessageEventArgs e)
        {
            var message = e.Message;
            if (message is ScsPingMessage)
            {
                var communicationChannel = _communicationChannel;
                var scsPingMessage1 = new ScsPingMessage();
                scsPingMessage1.RepliedMessageId = message.MessageId;
                var scsPingMessage2 = scsPingMessage1;
                communicationChannel.SendMessage(message: scsPingMessage2);
            }
            else
            {
                OnMessageReceived(message: message);
            }
        }

        void CommunicationChannel_MessageSent(object sender, MessageEventArgs e)
        {
            OnMessageSent(message: e.Message);
        }

        void OnDisconnected()
        {
            var disconnected = Disconnected;
            if (disconnected == null)
                return;
            disconnected(sender: this, e: EventArgs.Empty);
        }

        void OnMessageReceived(IScsMessage message)
        {
            var messageReceived = MessageReceived;
            if (messageReceived == null)
                return;
            messageReceived(sender: this, e: new MessageEventArgs(message: message));
        }

        protected virtual void OnMessageSent(IScsMessage message)
        {
            var messageSent = MessageSent;
            if (messageSent == null)
                return;
            messageSent(sender: this, e: new MessageEventArgs(message: message));
        }
    }
}