﻿using System;

namespace OpenNos.SCS.Communication.Scs.Server
{
    public class ServerClientEventArgs : EventArgs
    {
        public ServerClientEventArgs(IScsServerClient client)
        {
            Client = client;
        }

        public IScsServerClient Client { get; }
    }
}