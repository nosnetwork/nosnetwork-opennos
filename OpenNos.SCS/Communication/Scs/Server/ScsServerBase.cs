﻿using System;
using System.Runtime.CompilerServices;
using OpenNos.SCS.Collections;
using OpenNos.SCS.Communication.Scs.Communication.Channels;
using OpenNos.SCS.Communication.Scs.Communication.Protocols;

namespace OpenNos.SCS.Communication.Scs.Server
{
    internal abstract class ScsServerBase : IScsServer
    {
        IConnectionListener _connectionListener;

        protected ScsServerBase()
        {
            Clients = new ThreadSafeSortedList<long, IScsServerClient>();
            WireProtocolFactory = WireProtocolManager.GetDefaultWireProtocolFactory();
        }

        [CompilerGenerated] public event EventHandler<ServerClientEventArgs> ClientConnected;

        [CompilerGenerated] public event EventHandler<ServerClientEventArgs> ClientDisconnected;

        public IScsWireProtocolFactory WireProtocolFactory { get; set; }

        public ThreadSafeSortedList<long, IScsServerClient> Clients { get; }

        public virtual void Start()
        {
            _connectionListener = CreateConnectionListener();
            _connectionListener.CommunicationChannelConnected += ConnectionListener_CommunicationChannelConnected;
            _connectionListener.Start();
        }

        public virtual void Stop()
        {
            if (_connectionListener != null)
                _connectionListener.Stop();
            foreach (var allItem in Clients.GetAllItems())
                allItem.Disconnect();
        }

        protected abstract IConnectionListener CreateConnectionListener();

        void ConnectionListener_CommunicationChannelConnected(
            object sender,
            CommunicationChannelEventArgs e)
        {
            var scsServerClient = new ScsServerClient(communicationChannel: e.Channel)
            {
                ClientId = ScsServerManager.GetClientId(),
                WireProtocol = WireProtocolFactory.CreateWireProtocol()
            };
            scsServerClient.Disconnected += Client_Disconnected;
            Clients[key: scsServerClient.ClientId] = scsServerClient;
            OnClientConnected(client: scsServerClient);
            e.Channel.Start();
        }

        void Client_Disconnected(object sender, EventArgs e)
        {
            var client = (IScsServerClient)sender;
            Clients.Remove(key: client.ClientId);
            OnClientDisconnected(client: client);
        }

        protected virtual void OnClientConnected(IScsServerClient client)
        {
            var clientConnected = ClientConnected;
            if (clientConnected == null)
                return;
            clientConnected(sender: this, e: new ServerClientEventArgs(client: client));
        }

        protected virtual void OnClientDisconnected(IScsServerClient client)
        {
            var clientDisconnected = ClientDisconnected;
            if (clientDisconnected == null)
                return;
            clientDisconnected(sender: this, e: new ServerClientEventArgs(client: client));
        }
    }
}