﻿using System;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;

namespace OpenNos.SCS.Communication.Scs.Server
{
    public interface IScsServerClient : IMessenger
    {
        long ClientId { get; }

        ScsEndPoint RemoteEndPoint { get; }

        CommunicationStates CommunicationState { get; }
        event EventHandler Disconnected;

        void Disconnect();
    }
}