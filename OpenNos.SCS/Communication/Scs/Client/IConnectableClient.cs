﻿using System;
using OpenNos.SCS.Communication.Scs.Communication;

namespace OpenNos.SCS.Communication.Scs.Client
{
    public interface IConnectableClient : IDisposable
    {
        int ConnectTimeout { get; set; }

        CommunicationStates CommunicationState { get; }
        event EventHandler Connected;

        event EventHandler Disconnected;

        void Connect();

        void Disconnect();
    }
}