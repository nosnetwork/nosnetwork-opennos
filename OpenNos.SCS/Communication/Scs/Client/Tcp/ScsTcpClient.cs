﻿using System.Net;
using OpenNos.SCS.Communication.Scs.Communication.Channels;
using OpenNos.SCS.Communication.Scs.Communication.Channels.Tcp;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;

namespace OpenNos.SCS.Communication.Scs.Client.Tcp
{
    internal class ScsTcpClient : ScsClientBase
    {
        readonly ScsTcpEndPoint _serverEndPoint;

        public ScsTcpClient(ScsTcpEndPoint serverEndPoint)
        {
            _serverEndPoint = serverEndPoint;
        }

        protected override ICommunicationChannel CreateCommunicationChannel()
        {
            return new TcpCommunicationChannel(clientSocket: TcpHelper.ConnectToServer(
                endPoint: !IsStringIp(address: _serverEndPoint.IpAddress)
                    ? new DnsEndPoint(host: _serverEndPoint.IpAddress, port: _serverEndPoint.TcpPort)
                    : (EndPoint)new IPEndPoint(address: IPAddress.Parse(ipString: _serverEndPoint.IpAddress),
                        port: _serverEndPoint.TcpPort),
                timeoutMs: ConnectTimeout));
        }

        bool IsStringIp(string address)
        {
            IPAddress address1 = null;
            return IPAddress.TryParse(ipString: address, address: out address1);
        }
    }
}