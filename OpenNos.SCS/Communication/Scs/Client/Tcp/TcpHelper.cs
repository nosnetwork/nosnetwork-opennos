﻿using System;
using System.Net;
using System.Net.Sockets;

namespace OpenNos.SCS.Communication.Scs.Client.Tcp
{
    internal static class TcpHelper
    {
        public static Socket ConnectToServer(EndPoint endPoint, int timeoutMs)
        {
            var socket = new Socket(addressFamily: AddressFamily.InterNetwork, socketType: SocketType.Stream,
                protocolType: ProtocolType.Tcp);
            try
            {
                socket.Blocking = false;
                socket.Connect(remoteEP: endPoint);
                socket.Blocking = true;
                return socket;
            }
            catch (SocketException ex)
            {
                if (ex.ErrorCode != 10035)
                {
                    socket.Close();
                    throw;
                }

                if (!socket.Poll(microSeconds: timeoutMs * 1000, mode: SelectMode.SelectWrite))
                {
                    socket.Close();
                    throw new TimeoutException(message: "The host failed to connect. Timeout occured.");
                }

                socket.Blocking = true;
                return socket;
            }
        }
    }
}