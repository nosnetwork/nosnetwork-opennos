﻿using System;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;

namespace OpenNos.SCS.Communication.Scs.Client
{
    public interface IScsClient : IMessenger, IConnectableClient, IDisposable
    {
    }
}