﻿using System;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Threading;

namespace OpenNos.SCS.Communication.Scs.Client
{
    public class ClientReConnecter : IDisposable
    {
        readonly IConnectableClient _client;
        readonly Timer _reconnectTimer;
        volatile bool _disposed;

        public ClientReConnecter(IConnectableClient client)
        {
            _client = client ?? throw new ArgumentNullException(paramName: nameof(client));
            _client.Disconnected += Client_Disconnected;
            _reconnectTimer = new Timer(period: 20000);
            _reconnectTimer.Elapsed += ReconnectTimer_Elapsed;
            _reconnectTimer.Start();
        }

        public int ReConnectCheckPeriod
        {
            get => _reconnectTimer.Period;
            set => _reconnectTimer.Period = value;
        }

        public void Dispose()
        {
            if (_disposed)
                return;
            _disposed = true;
            _client.Disconnected -= Client_Disconnected;
            _reconnectTimer.Stop();
        }

        void Client_Disconnected(object sender, EventArgs e)
        {
            _reconnectTimer.Start();
        }

        void ReconnectTimer_Elapsed(object sender, EventArgs e)
        {
            if (!_disposed)
                if (_client.CommunicationState != CommunicationStates.Connected)
                    try
                    {
                        _client.Connect();
                        _reconnectTimer.Stop();
                        return;
                    }
                    catch
                    {
                        return;
                    }

            _reconnectTimer.Stop();
        }
    }
}