﻿using OpenNos.SCS.Communication.Scs.Communication.EndPoints;

namespace OpenNos.SCS.Communication.Scs.Client
{
    public static class ScsClientFactory
    {
        public static IScsClient CreateClient(ScsEndPoint endpoint)
        {
            return endpoint.CreateClient();
        }

        public static IScsClient CreateClient(string endpointAddress)
        {
            return CreateClient(endpoint: ScsEndPoint.CreateEndPoint(endPointAddress: endpointAddress));
        }
    }
}