﻿using System;
using System.Runtime.CompilerServices;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.Channels;
using OpenNos.SCS.Communication.Scs.Communication.Messages;
using OpenNos.SCS.Communication.Scs.Communication.Messengers;
using OpenNos.SCS.Communication.Scs.Communication.Protocols;
using OpenNos.SCS.Threading;

namespace OpenNos.SCS.Communication.Scs.Client
{
    internal abstract class ScsClientBase : IScsClient, IMessenger, IConnectableClient, IDisposable
    {
        const int DefaultConnectionAttemptTimeout = 15000;
        readonly Timer _pingTimer;
        ICommunicationChannel _communicationChannel;
        IScsWireProtocol _wireProtocol;

        protected ScsClientBase()
        {
            _pingTimer = new Timer(period: 30000);
            _pingTimer.Elapsed += PingTimer_Elapsed;
            ConnectTimeout = 15000;
            WireProtocol = WireProtocolManager.GetDefaultWireProtocol();
        }

        [CompilerGenerated] public event EventHandler<MessageEventArgs> MessageReceived;

        [CompilerGenerated] public event EventHandler<MessageEventArgs> MessageSent;

        [CompilerGenerated] public event EventHandler Connected;

        [CompilerGenerated] public event EventHandler Disconnected;

        public int ConnectTimeout { get; set; }

        public IScsWireProtocol WireProtocol
        {
            get => _wireProtocol;
            set
            {
                if (CommunicationState == CommunicationStates.Connected)
                    throw new ApplicationException(
                        message: "Wire protocol can not be changed while connected to server.");
                _wireProtocol = value;
            }
        }

        public CommunicationStates CommunicationState
        {
            get
            {
                if (_communicationChannel == null)
                    return CommunicationStates.Disconnected;
                return _communicationChannel.CommunicationState;
            }
        }

        public DateTime LastReceivedMessageTime
        {
            get
            {
                if (_communicationChannel == null)
                    return DateTime.MinValue;
                return _communicationChannel.LastReceivedMessageTime;
            }
        }

        public DateTime LastSentMessageTime
        {
            get
            {
                if (_communicationChannel == null)
                    return DateTime.MinValue;
                return _communicationChannel.LastSentMessageTime;
            }
        }

        public void Connect()
        {
            WireProtocol.Reset();
            _communicationChannel = CreateCommunicationChannel();
            _communicationChannel.WireProtocol = WireProtocol;
            _communicationChannel.Disconnected += CommunicationChannel_Disconnected;
            _communicationChannel.MessageReceived += CommunicationChannel_MessageReceived;
            _communicationChannel.MessageSent += CommunicationChannel_MessageSent;
            _communicationChannel.Start();
            _pingTimer.Start();
            OnConnected();
        }

        public void Disconnect()
        {
            if (CommunicationState != CommunicationStates.Connected)
                return;
            _communicationChannel.Disconnect();
        }

        public void Dispose()
        {
            Disconnect();
        }

        public void SendMessage(IScsMessage message)
        {
            if (CommunicationState != CommunicationStates.Connected)
                throw new CommunicationStateException(message: "Client is not connected to the server.");
            _communicationChannel.SendMessage(message: message);
        }

        protected abstract ICommunicationChannel CreateCommunicationChannel();

        void CommunicationChannel_MessageReceived(object sender, MessageEventArgs e)
        {
            if (e.Message is ScsPingMessage)
                return;
            OnMessageReceived(message: e.Message);
        }

        void CommunicationChannel_MessageSent(object sender, MessageEventArgs e)
        {
            OnMessageSent(message: e.Message);
        }

        void CommunicationChannel_Disconnected(object sender, EventArgs e)
        {
            _pingTimer.Stop();
            OnDisconnected();
        }

        void PingTimer_Elapsed(object sender, EventArgs e)
        {
            if (CommunicationState != CommunicationStates.Connected)
                return;
            try
            {
                var dateTime = DateTime.Now.AddMinutes(value: -1.0);
                if (_communicationChannel.LastReceivedMessageTime > dateTime ||
                    _communicationChannel.LastSentMessageTime > dateTime)
                    return;
                _communicationChannel.SendMessage(message: new ScsPingMessage());
            }
            catch
            {
            }
        }

        protected virtual void OnConnected()
        {
            var connected = Connected;
            if (connected == null)
                return;
            connected(sender: this, e: EventArgs.Empty);
        }

        protected virtual void OnDisconnected()
        {
            var disconnected = Disconnected;
            if (disconnected == null)
                return;
            disconnected(sender: this, e: EventArgs.Empty);
        }

        protected virtual void OnMessageReceived(IScsMessage message)
        {
            var messageReceived = MessageReceived;
            if (messageReceived == null)
                return;
            messageReceived(sender: this, e: new MessageEventArgs(message: message));
        }

        protected virtual void OnMessageSent(IScsMessage message)
        {
            var messageSent = MessageSent;
            if (messageSent == null)
                return;
            messageSent(sender: this, e: new MessageEventArgs(message: message));
        }
    }
}