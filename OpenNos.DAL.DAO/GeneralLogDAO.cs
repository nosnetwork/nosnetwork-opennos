﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class GeneralLogDao : IGeneralLogDao
    {
        #region Methods

        public bool IdAlreadySet(long id)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                return context.GeneralLog.Any(predicate: gl => gl.LogId == id);
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return false;
            }
        }

        public GeneralLogDto Insert(GeneralLogDto generalLog)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new GeneralLog();
                GeneralLogMapper.ToGeneralLog(input: generalLog, output: entity);
                context.GeneralLog.Add(entity: entity);
                context.SaveChanges();
                if (GeneralLogMapper.ToGeneralLogDto(input: entity, output: generalLog)) return generalLog;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public SaveResult InsertOrUpdate(ref GeneralLogDto generalLog)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var logId = generalLog.LogId;
                var entity = context.GeneralLog.FirstOrDefault(predicate: c => c.LogId.Equals(logId));

                if (entity == null)
                {
                    generalLog = Insert(generalLog: generalLog, context: context);
                    return SaveResult.Inserted;
                }

                generalLog = Update(entity: entity, generalLog: generalLog, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_GeneralLog_ERROR"),
                        arg0: generalLog.LogId,
                        arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        static GeneralLogDto Insert(GeneralLogDto generalLog, OpenNosContext context)
        {
            var entity = new GeneralLog();
            GeneralLogMapper.ToGeneralLog(input: generalLog, output: entity);
            context.GeneralLog.Add(entity: entity);
            context.SaveChanges();
            GeneralLogMapper.ToGeneralLogDto(input: entity, output: generalLog);
            return generalLog;
        }

        static GeneralLogDto
            Update(GeneralLog entity, GeneralLogDto generalLog, OpenNosContext context) //<---- Cleanup is not possible
        {
            if (entity != null)
            {
                GeneralLogMapper.ToGeneralLog(input: generalLog, output: entity);
                context.Entry(entity: entity).State = EntityState.Modified;
                context.SaveChanges();
            }

            if (GeneralLogMapper.ToGeneralLogDto(input: entity, output: generalLog)) return generalLog;

            return null;
        }

        public IEnumerable<GeneralLogDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<GeneralLogDto>();
            foreach (var generalLog in context.GeneralLog)
            {
                var dto = new GeneralLogDto();
                GeneralLogMapper.ToGeneralLogDto(input: generalLog, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<GeneralLogDto> LoadByIp(string ip)
        {
            using var context = DataAccessHelper.CreateContext();
            var cleanIp = ip.Replace(oldValue: "tcp://", newValue: "");
            cleanIp = cleanIp.Substring(startIndex: 0,
                length: cleanIp.LastIndexOf(":", StringComparison.Ordinal) > 0 ? cleanIp.LastIndexOf(":", StringComparison.Ordinal) : cleanIp.Length);
            var result = new List<GeneralLogDto>();
            foreach (var generalLog in context.GeneralLog.Where(predicate: s => s.IpAddress.Contains(cleanIp)))
            {
                var dto = new GeneralLogDto();
                GeneralLogMapper.ToGeneralLogDto(input: generalLog, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<GeneralLogDto> LoadByAccount(long? accountId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<GeneralLogDto>();
            foreach (var generalLog in context.GeneralLog.Where(predicate: s => s.AccountId == accountId))
            {
                var dto = new GeneralLogDto();
                GeneralLogMapper.ToGeneralLogDto(input: generalLog, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<GeneralLogDto> LoadByLogType(string logType, long? characterId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<GeneralLogDto>();
            foreach (var log in context.GeneralLog.Where(predicate: c =>
                c.LogType.Equals(logType) && c.CharacterId == characterId))
            {
                var dto = new GeneralLogDto();
                GeneralLogMapper.ToGeneralLogDto(input: log, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<GeneralLogDto> LoadByLogTypeAndAccountId(string logType, long? logId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<GeneralLogDto>();
            foreach (var log in context.GeneralLog.Where(predicate: c =>
                c.LogType.Equals(logType) && c.LogId == logId))
            {
                var dto = new GeneralLogDto();
                GeneralLogMapper.ToGeneralLogDto(input: log, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public void SetCharIdNull(long? characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                foreach (var log in context.GeneralLog.Where(predicate: c => c.CharacterId == characterId))
                    log.CharacterId = null;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public void WriteGeneralLog(long logId, string ipAddress, long? characterId, string logType, string logData)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var log = new GeneralLog
                {
                    LogId = logId,
                    IpAddress = ipAddress,
                    Timestamp = DateTime.Now,
                    LogType = logType,
                    LogData = logData,
                    CharacterId = characterId
                };

                context.GeneralLog.Add(entity: log);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        #endregion
    }
}