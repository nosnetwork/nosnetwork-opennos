﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class StaticBuffDao : IStaticBuffDao
    {
        #region Methods

        public void Delete(short bonusToDelete, long characterId)
        {
            try
            {
                using (var context = DataAccessHelper.CreateContext())
                {
                    var bon = context.StaticBuff.FirstOrDefault(predicate: c =>
                        c.CardId == bonusToDelete && c.CharacterId == characterId);

                    if (bon != null)
                    {
                        context.StaticBuff.Remove(entity: bon);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_ERROR"),
                        arg0: bonusToDelete, arg1: e.Message), ex: e);
            }
        }

        public SaveResult InsertOrUpdate(ref StaticBuffDto staticBuff)
        {
            try
            {
                using (var context = DataAccessHelper.CreateContext())
                {
                    var id = staticBuff.CharacterId;
                    var cardid = staticBuff.CardId;
                    var entity =
                        context.StaticBuff.FirstOrDefault(predicate: c => c.CardId == cardid && c.CharacterId == id);

                    if (entity == null)
                    {
                        staticBuff = Insert(sb: staticBuff, context: context);
                        return SaveResult.Inserted;
                    }

                    staticBuff.StaticBuffId = entity.StaticBuffId;
                    staticBuff = Update(entity: entity, sb: staticBuff, context: context);
                    return SaveResult.Updated;
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<StaticBuffDto> LoadByCharacterId(long characterId)
        {
            using (var context = DataAccessHelper.CreateContext())
            {
                var result = new List<StaticBuffDto>();
                foreach (var entity in context.StaticBuff.Where(predicate: i => i.CharacterId == characterId))
                {
                    var dto = new StaticBuffDto();
                    StaticBuffMapper.ToStaticBuffDTO(input: entity, output: dto);
                    result.Add(item: dto);
                }

                return result;
            }
        }

        public static StaticBuffDto LoadById(long sbId)
        {
            try
            {
                using (var context = DataAccessHelper.CreateContext())
                {
                    var dto = new StaticBuffDto();
                    if (StaticBuffMapper.ToStaticBuffDTO(
                        input: context.StaticBuff.FirstOrDefault(predicate: s => s.StaticBuffId.Equals(sbId)),
                        output: dto)
                    ) //who the fuck was so retarded and set it to respawn ?!?
                        return dto;

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<short> LoadByTypeCharacterId(long characterId)
        {
            try
            {
                using (var context = DataAccessHelper.CreateContext())
                {
                    return context.StaticBuff.Where(predicate: i => i.CharacterId == characterId)
                        .Select(selector: qle => qle.CardId)
                        .ToList();
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static StaticBuffDto Insert(StaticBuffDto sb, OpenNosContext context)
        {
            try
            {
                var entity = new StaticBuff();
                StaticBuffMapper.ToStaticBuff(input: sb, output: entity);
                context.StaticBuff.Add(entity: entity);
                context.SaveChanges();
                if (StaticBuffMapper.ToStaticBuffDTO(input: entity, output: sb)) return sb;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static StaticBuffDto Update(StaticBuff entity, StaticBuffDto sb, OpenNosContext context)
        {
            if (entity != null)
            {
                StaticBuffMapper.ToStaticBuff(input: sb, output: entity);
                context.SaveChanges();
            }

            if (StaticBuffMapper.ToStaticBuffDTO(input: entity, output: sb)) return sb;

            return null;
        }

        #endregion
    }
}