﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class NpcMonsterDao : INpcMonsterDao
    {
        #region Methods

        public IEnumerable<NpcMonsterDto> FindByName(string name)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<NpcMonsterDto>();
            foreach (var npcMonster in context.NpcMonster.Where(predicate: s =>
                string.IsNullOrEmpty(name) ? s.Name.Equals("") : s.Name.Contains(name)))
            {
                var dto = new NpcMonsterDto();
                NpcMonsterMapper.ToNpcMonsterDto(input: npcMonster, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public void Insert(List<NpcMonsterDto> npcMonsters)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var item in npcMonsters)
                {
                    var entity = new NpcMonster();
                    NpcMonsterMapper.ToNpcMonster(input: item, output: entity);
                    context.NpcMonster.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public NpcMonsterDto Insert(NpcMonsterDto npc)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new NpcMonster();
                NpcMonsterMapper.ToNpcMonster(input: npc, output: entity);
                context.NpcMonster.Add(entity: entity);
                context.SaveChanges();
                if (NpcMonsterMapper.ToNpcMonsterDto(input: entity, output: npc)) return npc;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public SaveResult InsertOrUpdate(ref NpcMonsterDto npcMonster)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var npcMonsterVNum = npcMonster.NpcMonsterVNum;
                var entity =
                    context.NpcMonster.FirstOrDefault(predicate: c => c.NpcMonsterVNum.Equals(npcMonsterVNum));

                if (entity == null)
                {
                    npcMonster = Insert(npcMonster: npcMonster, context: context);
                    return SaveResult.Inserted;
                }

                npcMonster = Update(entity: entity, npcMonster: npcMonster, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_NPCMONSTER_ERROR"),
                        arg0: npcMonster.NpcMonsterVNum, arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<NpcMonsterDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<NpcMonsterDto>();
            foreach (var npcMonster in context.NpcMonster)
            {
                var dto = new NpcMonsterDto();
                NpcMonsterMapper.ToNpcMonsterDto(input: npcMonster, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public NpcMonsterDto LoadByVNum(short npcMonsterVNum)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new NpcMonsterDto();
                if (NpcMonsterMapper.ToNpcMonsterDto(
                    input: context.NpcMonster.FirstOrDefault(
                        predicate: i => i.NpcMonsterVNum.Equals(npcMonsterVNum)), output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static NpcMonsterDto Insert(NpcMonsterDto npcMonster, OpenNosContext context)
        {
            var entity = new NpcMonster();
            NpcMonsterMapper.ToNpcMonster(input: npcMonster, output: entity);
            context.NpcMonster.Add(entity: entity);
            context.SaveChanges();
            if (NpcMonsterMapper.ToNpcMonsterDto(input: entity, output: npcMonster)) return npcMonster;

            return null;
        }

        static NpcMonsterDto Update(NpcMonster entity, NpcMonsterDto npcMonster, OpenNosContext context)
        {
            if (entity != null)
            {
                NpcMonsterMapper.ToNpcMonster(input: npcMonster, output: entity);
                context.SaveChanges();
            }

            if (NpcMonsterMapper.ToNpcMonsterDto(input: entity, output: npcMonster)) return npcMonster;

            return null;
        }

        #endregion
    }
}