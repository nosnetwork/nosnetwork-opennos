﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class QuicklistEntryDao : IQuicklistEntryDao
    {
        #region Methods

        public DeleteResult Delete(Guid id)
        {
            using var context = DataAccessHelper.CreateContext();
            var entity = context.Set<QuicklistEntry>().FirstOrDefault(predicate: i => i.Id == id);
            if (entity != null)
            {
                context.Set<QuicklistEntry>().Remove(entity: entity);
                context.SaveChanges();
            }

            return DeleteResult.Deleted;
        }

        public IEnumerable<QuicklistEntryDto> InsertOrUpdate(IEnumerable<QuicklistEntryDto> dtos)
        {
            try
            {
                IList<QuicklistEntryDto> results = new List<QuicklistEntryDto>();
                using (var context = DataAccessHelper.CreateContext())
                {
                    foreach (var dto in dtos) results.Add(item: InsertOrUpdate(context: context, dto: dto));
                }

                return results;
            }
            catch (Exception e)
            {
                Logger.Error(data: $"Message: {e.Message}", ex: e);
                return Enumerable.Empty<QuicklistEntryDto>();
            }
        }

        public QuicklistEntryDto InsertOrUpdate(QuicklistEntryDto dto)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                return InsertOrUpdate(context: context, dto: dto);
            }
            catch (Exception e)
            {
                Logger.Error(data: $"Message: {e.Message}", ex: e);
                return null;
            }
        }

        public IEnumerable<QuicklistEntryDto> LoadByCharacterId(long characterId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<QuicklistEntryDto>();
            foreach (var quicklistEntryobject in context.QuicklistEntry.Where(predicate: i =>
                i.CharacterId == characterId))
            {
                var quicklistEntryDto = new QuicklistEntryDto();
                QuicklistEntryMapper.ToQuicklistEntryDto(input: quicklistEntryobject, output: quicklistEntryDto);
                result.Add(item: quicklistEntryDto);
            }

            return result;
        }

        public QuicklistEntryDto LoadById(Guid id)
        {
            using var context = DataAccessHelper.CreateContext();
            var quicklistEntryDto = new QuicklistEntryDto();
            if (QuicklistEntryMapper.ToQuicklistEntryDto(
                input: context.QuicklistEntry.FirstOrDefault(predicate: i => i.Id.Equals(id)),
                output: quicklistEntryDto))
                return quicklistEntryDto;

            return null;
        }

        public IEnumerable<Guid> LoadKeysByCharacterId(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                return context.QuicklistEntry.Where(predicate: i => i.CharacterId == characterId)
                    .Select(selector: qle => qle.Id)
                    .ToList();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        protected static QuicklistEntryDto Insert(QuicklistEntryDto dto, OpenNosContext context)
        {
            var entity = new QuicklistEntry();
            QuicklistEntryMapper.ToQuicklistEntry(input: dto, output: entity);
            context.Set<QuicklistEntry>().Add(entity: entity);
            context.SaveChanges();
            if (QuicklistEntryMapper.ToQuicklistEntryDto(input: entity, output: dto)) return dto;

            return null;
        }

        protected static QuicklistEntryDto InsertOrUpdate(OpenNosContext context, QuicklistEntryDto dto)
        {
            var primaryKey = dto.Id;
            var entity = context.Set<QuicklistEntry>().FirstOrDefault(predicate: c => c.Id == primaryKey);
            if (entity == null)
                return Insert(dto: dto, context: context);
            return Update(entity: entity, inventory: dto, context: context);
        }

        protected static QuicklistEntryDto Update(QuicklistEntry entity, QuicklistEntryDto inventory,
            OpenNosContext context)
        {
            if (entity != null)
            {
                QuicklistEntryMapper.ToQuicklistEntry(input: inventory, output: entity);
                context.SaveChanges();
            }

            if (QuicklistEntryMapper.ToQuicklistEntryDto(input: entity, output: inventory)) return inventory;

            return null;
        }

        #endregion
    }
}