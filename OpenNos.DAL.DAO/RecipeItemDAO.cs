﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class RecipeItemDao : IRecipeItemDao
    {
        #region Methods

        public RecipeItemDto Insert(RecipeItemDto recipeItem)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new RecipeItem();
                RecipeItemMapper.ToRecipeItem(input: recipeItem, output: entity);
                context.RecipeItem.Add(entity: entity);
                context.SaveChanges();
                if (RecipeItemMapper.ToRecipeItemDto(input: entity, output: recipeItem)) return recipeItem;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<RecipeItemDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RecipeItemDto>();
            foreach (var recipeItem in context.RecipeItem)
            {
                var dto = new RecipeItemDto();
                RecipeItemMapper.ToRecipeItemDto(input: recipeItem, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public RecipeItemDto LoadById(short recipeItemId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new RecipeItemDto();
                if (RecipeItemMapper.ToRecipeItemDto(
                    input: context.RecipeItem.FirstOrDefault(predicate: s => s.RecipeItemId.Equals(recipeItemId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<RecipeItemDto> LoadByRecipe(short recipeId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RecipeItemDto>();
            foreach (var recipeItem in context.RecipeItem.Where(predicate: s => s.RecipeId.Equals(recipeId)))
            {
                var dto = new RecipeItemDto();
                RecipeItemMapper.ToRecipeItemDto(input: recipeItem, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<RecipeItemDto> LoadByRecipeAndItem(short recipeId, short itemVNum)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RecipeItemDto>();
            foreach (var recipeItem in context.RecipeItem.Where(predicate: s =>
                s.ItemVNum.Equals(itemVNum) && s.RecipeId.Equals(recipeId)))
            {
                var dto = new RecipeItemDto();
                RecipeItemMapper.ToRecipeItemDto(input: recipeItem, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}