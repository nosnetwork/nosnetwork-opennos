﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class FamilyDao : IFamilyDao
    {
        #region Methods

        public DeleteResult Delete(long familyId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var fam = context.Family.FirstOrDefault(predicate: c => c.FamilyId == familyId);

                if (fam != null)
                {
                    context.Family.Remove(entity: fam);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_ERROR"),
                        arg0: familyId, arg1: e.Message),
                    ex: e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref FamilyDto family)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var accountId = family.FamilyId;
                var entity = context.Family.FirstOrDefault(predicate: c => c.FamilyId.Equals(accountId));

                if (entity == null)
                {
                    family = Insert(family: family, context: context);
                    return SaveResult.Inserted;
                }

                family = Update(entity: entity, family: family, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_FAMILY_ERROR"),
                        arg0: family.FamilyId,
                        arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<FamilyDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<FamilyDto>();
            foreach (var entity in context.Family)
            {
                var dto = new FamilyDto();
                FamilyMapper.ToFamilyDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public FamilyDto LoadByCharacterId(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var familyCharacter =
                    context.FamilyCharacter.FirstOrDefault(predicate: fc =>
                        fc.Character.CharacterId.Equals(characterId));
                if (familyCharacter != null)
                {
                    var family =
                        context.Family.FirstOrDefault(predicate: a => a.FamilyId.Equals(familyCharacter.FamilyId));
                    if (family != null)
                    {
                        var dto = new FamilyDto();
                        if (FamilyMapper.ToFamilyDto(input: family, output: dto)) return dto;

                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return null;
        }

        public FamilyDto LoadById(long familyId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var family = context.Family.FirstOrDefault(predicate: a => a.FamilyId.Equals(familyId));
                if (family != null)
                {
                    var dto = new FamilyDto();
                    if (FamilyMapper.ToFamilyDto(input: family, output: dto)) return dto;

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return null;
        }

        public FamilyDto LoadByName(string name)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var family = context.Family.FirstOrDefault(predicate: a => a.Name.Equals(name));
                if (family != null)
                {
                    var dto = new FamilyDto();
                    if (FamilyMapper.ToFamilyDto(input: family, output: dto)) return dto;

                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return null;
        }

        static FamilyDto Insert(FamilyDto family, OpenNosContext context)
        {
            var entity = new Family();
            FamilyMapper.ToFamily(input: family, output: entity);
            context.Family.Add(entity: entity);
            context.SaveChanges();
            if (FamilyMapper.ToFamilyDto(input: entity, output: family)) return family;

            return null;
        }

        static FamilyDto Update(Family entity, FamilyDto family, OpenNosContext context)
        {
            if (entity != null)
            {
                FamilyMapper.ToFamily(input: family, output: entity);
                context.SaveChanges();
            }

            if (FamilyMapper.ToFamilyDto(input: entity, output: family)) return family;

            return null;
        }

        #endregion
    }
}