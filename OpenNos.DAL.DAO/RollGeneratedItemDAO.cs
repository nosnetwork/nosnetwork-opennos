﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class RollGeneratedItemDao : IRollGeneratedItemDao
    {
        #region Methods

        public RollGeneratedItemDto Insert(RollGeneratedItemDto item)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new RollGeneratedItem();
                RollGeneratedItemMapper.ToRollGeneratedItem(input: item, output: entity);
                context.RollGeneratedItem.Add(entity: entity);
                context.SaveChanges();
                if (RollGeneratedItemMapper.ToRollGeneratedItemDto(input: entity, output: item)) return item;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<RollGeneratedItemDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RollGeneratedItemDto>();
            foreach (var item in context.RollGeneratedItem)
            {
                var dto = new RollGeneratedItemDto();
                RollGeneratedItemMapper.ToRollGeneratedItemDto(input: item, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public RollGeneratedItemDto LoadById(short id)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new RollGeneratedItemDto();
                if (RollGeneratedItemMapper.ToRollGeneratedItemDto(
                    input: context.RollGeneratedItem.FirstOrDefault(
                        predicate: i => i.RollGeneratedItemId.Equals(id)), output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<RollGeneratedItemDto> LoadByItemVNum(short vnum)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RollGeneratedItemDto>();
            foreach (var item in context.RollGeneratedItem.Where(predicate: s => s.OriginalItemVNum == vnum))
            {
                var dto = new RollGeneratedItemDto();
                RollGeneratedItemMapper.ToRollGeneratedItemDto(input: item, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}