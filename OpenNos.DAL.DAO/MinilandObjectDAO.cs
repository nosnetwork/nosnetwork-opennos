﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class MinilandObjectDao : IMinilandObjectDao
    {
        #region Methods

        public DeleteResult DeleteById(long id)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var item = context.MinilandObject.First(predicate: i => i.MinilandObjectId.Equals(id));

                if (item != null)
                {
                    context.MinilandObject.Remove(entity: item);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref MinilandObjectDto obj)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var id = obj.MinilandObjectId;
                var entity = context.MinilandObject.FirstOrDefault(predicate: c => c.MinilandObjectId.Equals(id));

                if (entity == null)
                {
                    obj = Insert(obj: obj, context: context);
                    return SaveResult.Inserted;
                }

                obj.MinilandObjectId = entity.MinilandObjectId;
                obj = Update(entity: entity, respawn: obj, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<MinilandObjectDto> LoadByCharacterId(long characterId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MinilandObjectDto>();
            foreach (var obj in context.MinilandObject.Where(predicate: s => s.CharacterId == characterId))
            {
                var dto = new MinilandObjectDto();
                MinilandObjectMapper.ToMinilandObjectDto(input: obj, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        static MinilandObjectDto Insert(MinilandObjectDto obj, OpenNosContext context)
        {
            try
            {
                var entity = new MinilandObject();
                MinilandObjectMapper.ToMinilandObject(input: obj, output: entity);
                context.MinilandObject.Add(entity: entity);
                context.SaveChanges();
                if (MinilandObjectMapper.ToMinilandObjectDto(input: entity, output: obj)) return obj;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static MinilandObjectDto Update(MinilandObject entity, MinilandObjectDto respawn, OpenNosContext context)
        {
            if (entity != null)
            {
                MinilandObjectMapper.ToMinilandObject(input: respawn, output: entity);
                context.SaveChanges();
            }

            if (MinilandObjectMapper.ToMinilandObjectDto(input: entity, output: respawn)) return respawn;

            return null;
        }

        #endregion
    }
}