﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class ScriptedInstanceDao : IScriptedInstanceDao
    {
        #region Methods

        public void Insert(List<ScriptedInstanceDto> scriptedInstances)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var scriptedInstance in scriptedInstances)
                {
                    var entity = new ScriptedInstance();
                    ScriptedInstanceMapper.ToScriptedInstance(input: scriptedInstance, output: entity);
                    context.ScriptedInstance.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public ScriptedInstanceDto Insert(ScriptedInstanceDto scriptedInstance)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new ScriptedInstance();
                ScriptedInstanceMapper.ToScriptedInstance(input: scriptedInstance, output: entity);
                context.ScriptedInstance.Add(entity: entity);
                context.SaveChanges();
                if (ScriptedInstanceMapper.ToScriptedInstanceDto(input: entity, output: scriptedInstance))
                    return scriptedInstance;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<ScriptedInstanceDto> LoadByMap(short mapId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ScriptedInstanceDto>();
            foreach (var timespaceObject in context.ScriptedInstance.Where(predicate: c => c.MapId.Equals(mapId)))
            {
                var dto = new ScriptedInstanceDto();
                ScriptedInstanceMapper.ToScriptedInstanceDto(input: timespaceObject, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}