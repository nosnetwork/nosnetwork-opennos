﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class MinigameLogDao : IMinigameLogDao
    {
        #region Methods

        public SaveResult InsertOrUpdate(ref MinigameLogDto minigameLog)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var minigameLogId = minigameLog.MinigameLogId;
                var entity =
                    context.MinigameLog.FirstOrDefault(predicate: c => c.MinigameLogId.Equals(minigameLogId));

                if (entity == null)
                {
                    minigameLog = Insert(account: minigameLog, context: context);
                    return SaveResult.Inserted;
                }

                minigameLog = Update(entity: entity, account: minigameLog, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<MinigameLogDto> LoadByCharacterId(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                IEnumerable<MinigameLog> minigameLog =
                    context.MinigameLog.Where(predicate: a => a.CharacterId.Equals(characterId)).ToList();
                {
                    var result = new List<MinigameLogDto>();
                    foreach (var input in minigameLog)
                    {
                        var dto = new MinigameLogDto();
                        if (MinigameLogMapper.ToMinigameLogDto(input: input, output: dto)) result.Add(item: dto);
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return null;
        }

        public MinigameLogDto LoadById(long minigameLogId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var minigameLog =
                    context.MinigameLog.FirstOrDefault(predicate: a => a.MinigameLogId.Equals(minigameLogId));
                if (minigameLog != null)
                {
                    var minigameLogDto = new MinigameLogDto();
                    if (MinigameLogMapper.ToMinigameLogDto(input: minigameLog, output: minigameLogDto))
                        return minigameLogDto;
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return null;
        }

        static MinigameLogDto Insert(MinigameLogDto account, OpenNosContext context)
        {
            var entity = new MinigameLog();
            MinigameLogMapper.ToMinigameLog(input: account, output: entity);
            context.MinigameLog.Add(entity: entity);
            context.SaveChanges();
            MinigameLogMapper.ToMinigameLogDto(input: entity, output: account);
            return account;
        }

        static MinigameLogDto Update(MinigameLog entity, MinigameLogDto account, OpenNosContext context)
        {
            if (entity != null)
            {
                MinigameLogMapper.ToMinigameLog(input: account, output: entity);
                context.Entry(entity: entity).State = EntityState.Modified;
                context.SaveChanges();
            }

            if (MinigameLogMapper.ToMinigameLogDto(input: entity, output: account)) return account;

            return null;
        }

        #endregion
    }
}