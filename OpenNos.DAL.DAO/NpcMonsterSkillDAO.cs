﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class NpcMonsterSkillDao : INpcMonsterSkillDao
    {
        #region Methods

        public NpcMonsterSkillDto Insert(ref NpcMonsterSkillDto npcMonsterSkill)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new NpcMonsterSkill();
                NpcMonsterSkillMapper.ToNpcMonsterSkill(input: npcMonsterSkill, output: entity);
                context.NpcMonsterSkill.Add(entity: entity);
                context.SaveChanges();
                if (NpcMonsterSkillMapper.ToNpcMonsterSkillDto(input: entity, output: npcMonsterSkill))
                    return npcMonsterSkill;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public void Insert(List<NpcMonsterSkillDto> skills)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var skill in skills)
                {
                    var entity = new NpcMonsterSkill();
                    NpcMonsterSkillMapper.ToNpcMonsterSkill(input: skill, output: entity);
                    context.NpcMonsterSkill.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public List<NpcMonsterSkillDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<NpcMonsterSkillDto>();
            foreach (var npcMonsterSkillobject in context.NpcMonsterSkill)
            {
                var dto = new NpcMonsterSkillDto();
                NpcMonsterSkillMapper.ToNpcMonsterSkillDto(input: npcMonsterSkillobject, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<NpcMonsterSkillDto> LoadByNpcMonster(short npcId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<NpcMonsterSkillDto>();
            foreach (var npcMonsterSkillobject in context.NpcMonsterSkill.Where(predicate: i =>
                i.NpcMonsterVNum == npcId))
            {
                var dto = new NpcMonsterSkillDto();
                NpcMonsterSkillMapper.ToNpcMonsterSkillDto(input: npcMonsterSkillobject, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}