﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class MaintenanceLogDao : IMaintenanceLogDao
    {
        #region Methods

        public MaintenanceLogDto Insert(MaintenanceLogDto maintenanceLog)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new MaintenanceLog();
                MaintenanceLogMapper.ToMaintenanceLog(input: maintenanceLog, output: entity);
                context.MaintenanceLog.Add(entity: entity);
                context.SaveChanges();
                if (MaintenanceLogMapper.ToMaintenanceLogDto(input: entity, output: maintenanceLog))
                    return maintenanceLog;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<MaintenanceLogDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MaintenanceLogDto>();
            foreach (var maintenanceLog in context.MaintenanceLog)
            {
                var dto = new MaintenanceLogDto();
                MaintenanceLogMapper.ToMaintenanceLogDto(input: maintenanceLog, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public MaintenanceLogDto LoadFirst()
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new MaintenanceLogDto();
                if (MaintenanceLogMapper.ToMaintenanceLogDto(
                    input: context.MaintenanceLog.FirstOrDefault(predicate: m => m.DateEnd > DateTime.Now),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        #endregion
    }
}