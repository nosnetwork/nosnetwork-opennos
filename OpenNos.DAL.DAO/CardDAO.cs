﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class CardDao : ICardDao
    {
        #region Methods

        public CardDto Insert(ref CardDto card)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new Card();
                CardMapper.ToCard(input: card, output: entity);
                context.Card.Add(entity: entity);
                context.SaveChanges();
                if (CardMapper.ToCardDto(input: entity, output: card)) return card;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public void Insert(List<CardDto> cards)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var card in cards) InsertOrUpdate(card: card);
                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public IEnumerable<CardDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CardDto>();
            foreach (var card in context.Card)
            {
                var dto = new CardDto();
                CardMapper.ToCardDto(input: card, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public CardDto LoadById(short cardId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new CardDto();
                if (CardMapper.ToCardDto(
                    input: context.Card.FirstOrDefault(predicate: s => s.CardId.Equals(cardId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public SaveResult InsertOrUpdate(CardDto card)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                long cardId = card.CardId;
                var entity = context.Card.FirstOrDefault(predicate: c => c.CardId == cardId);

                if (entity == null)
                {
                    card = Insert(card: card, context: context);
                    return SaveResult.Inserted;
                }

                card = Update(entity: entity, card: card, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_CARD_ERROR"),
                        arg0: card.CardId, arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        static CardDto Insert(CardDto card, OpenNosContext context)
        {
            var entity = new Card();
            CardMapper.ToCard(input: card, output: entity);
            context.Card.Add(entity: entity);
            context.SaveChanges();
            if (CardMapper.ToCardDto(input: entity, output: card)) return card;

            return null;
        }

        static CardDto Update(Card entity, CardDto card, OpenNosContext context)
        {
            if (entity != null)
            {
                CardMapper.ToCard(input: card, output: entity);
                context.SaveChanges();
            }

            if (CardMapper.ToCardDto(input: entity, output: card)) return card;

            return null;
        }

        #endregion
    }
}