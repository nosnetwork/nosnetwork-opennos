﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class MailDao : IMailDao
    {
        #region Methods

        public DeleteResult DeleteById(long mailId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var mail = context.Mail.First(predicate: i => i.MailId.Equals(mailId));

                if (mail != null)
                {
                    context.Mail.Remove(entity: mail);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref MailDto mail)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var mailId = mail.MailId;
                var entity = context.Mail.FirstOrDefault(predicate: c => c.MailId.Equals(mailId));

                if (entity == null)
                {
                    mail = Insert(mail: mail, context: context);
                    return SaveResult.Inserted;
                }

                mail.MailId = entity.MailId;
                mail = Update(entity: entity, respawn: mail, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<MailDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MailDto>();
            foreach (var mail in context.Mail)
            {
                var dto = new MailDto();
                MailMapper.ToMailDto(input: mail, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public MailDto LoadById(long mailId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new MailDto();
                if (MailMapper.ToMailDto(
                    input: context.Mail.FirstOrDefault(predicate: i => i.MailId.Equals(mailId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<MailDto> LoadSentByCharacter(long characterId)
        {
            //Where(s => s.SenderId == CharacterId && s.IsSenderCopy && MailList.All(m => m.Value.MailId != s.MailId))
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MailDto>();
            foreach (var mail in context.Mail.Where(predicate: s => s.SenderId == characterId && s.IsSenderCopy)
                .Take(count: 40))
            {
                var dto = new MailDto();
                MailMapper.ToMailDto(input: mail, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<MailDto> LoadSentToCharacter(long characterId)
        {
            //s => s.ReceiverId == CharacterId && !s.IsSenderCopy && MailList.All(m => m.Value.MailId != s.MailId)).Take(50)
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MailDto>();
            foreach (var mail in context.Mail.Where(predicate: s => s.ReceiverId == characterId && !s.IsSenderCopy)
                .Take(count: 40))
            {
                var dto = new MailDto();
                MailMapper.ToMailDto(input: mail, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        static MailDto Insert(MailDto mail, OpenNosContext context)
        {
            try
            {
                var entity = new Mail();
                MailMapper.ToMail(input: mail, output: entity);
                context.Mail.Add(entity: entity);
                context.SaveChanges();
                if (MailMapper.ToMailDto(input: entity, output: mail)) return mail;

                return null;
            }
            catch (DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        // raise a new exception nesting the current instance as InnerException
                        Logger.Error(
                            ex: new InvalidOperationException(
                                message: $"{validationErrors.Entry.Entity}:{validationError.ErrorMessage}",
                                innerException: raise));
                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static MailDto Update(Mail entity, MailDto respawn, OpenNosContext context)
        {
            if (entity != null)
            {
                MailMapper.ToMail(input: respawn, output: entity);
                context.SaveChanges();
            }

            if (MailMapper.ToMailDto(input: entity, output: respawn)) return respawn;

            return null;
        }

        #endregion
    }
}