﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Domain;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class ItemInstanceDao : IItemInstanceDao
    {
        #region Methods

        public virtual DeleteResult Delete(Guid id)
        {
            using var context = DataAccessHelper.CreateContext();
            var entity = context.Set<ItemInstance>().FirstOrDefault(predicate: i => i.Id == id);
            if (entity != null)
            {
                context.Set<ItemInstance>().Remove(entity: entity);
                context.SaveChanges();
            }

            return DeleteResult.Deleted;
        }

        public DeleteResult DeleteFromSlotAndType(long characterId, short slot, InventoryType type)
        {
            try
            {
                var dto = LoadBySlotAndType(characterId: characterId, slot: slot, type: type);
                if (dto != null) return Delete(id: dto.Id);

                return DeleteResult.Unknown;
            }
            catch (Exception e)
            {
                Logger.Error(data: $"characterId: {characterId} slot: {slot} type: {type}", ex: e);
                return DeleteResult.Error;
            }
        }

        public DeleteResult DeleteGuidList(IEnumerable<Guid> guids)
        {
            using var context = DataAccessHelper.CreateContext();
            var enumerable = guids as Guid[] ?? guids.ToArray();
            try
            {
                foreach (var id in enumerable)
                {
                    var entity = context.ItemInstance.FirstOrDefault(predicate: i => i.Id == id);
                    if (entity != null && entity.Type != InventoryType.FamilyWareHouse)
                        context.ItemInstance.Remove(entity: entity);
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.LogUserEventError(logEvent: "DELETEGUIDLIST_EXCEPTION", caller: "Saving Process",
                    data: "Items were not deleted!",
                    ex: ex);
                foreach (var id in enumerable)
                    try
                    {
                        Delete(id: id);
                    }
                    catch (Exception exc)
                    {
                        // TODO: Work on: statement conflicted with the REFERENCE constraint
                        //       "FK_dbo.BazaarItem_dbo.ItemInstance_ItemInstanceId". The
                        //       conflict occurred in database "opennos", table "dbo.BazaarItem",
                        //       column 'ItemInstanceId'.
                        Logger.LogUserEventError(logEvent: "ONSAVEDELETION_EXCEPTION", caller: "Saving Process",
                            data: $"FALLBACK FUNCTION FAILED! Detailed Item Information: Item ID = {id}", ex: exc);
                    }
            }

            return DeleteResult.Deleted;
        }

        public IEnumerable<ItemInstanceDto> InsertOrUpdate(IEnumerable<ItemInstanceDto> dtos)
        {
            try
            {
                IList<ItemInstanceDto> results = new List<ItemInstanceDto>();
                using (var context = DataAccessHelper.CreateContext())
                {
                    foreach (var dto in dtos) results.Add(item: InsertOrUpdate(context: context, dto: dto));
                }

                return results;
            }
            catch (Exception e)
            {
                Logger.Error(data: $"Message: {e.Message}", ex: e);
                return Enumerable.Empty<ItemInstanceDto>();
            }
        }

        public ItemInstanceDto InsertOrUpdate(ItemInstanceDto dto)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                return InsertOrUpdate(context: context, dto: dto);
            }
            catch (Exception e)
            {
                Logger.Error(data: $"Message: {e.Message}", ex: e);
                return null;
            }
        }

        public SaveResult InsertOrUpdateFromList(IEnumerable<ItemInstanceDto> items)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();

                void Insert(ItemInstanceDto iteminstance)
                {
                    var entity = new ItemInstance();
                    Map(input: iteminstance, output: entity);
                    context.ItemInstance.Add(entity: entity);
                    context.SaveChanges();
                    iteminstance.Id = entity.Id;
                }

                static void Update(ItemInstance entity, ItemInstanceDto iteminstance)
                {
                    if (entity != null) Map(input: iteminstance, output: entity);
                }

                foreach (var item in items)
                {
                    var entity = context.ItemInstance.FirstOrDefault(predicate: c => c.Id == item.Id);

                    if (entity == null)
                        Insert(iteminstance: item);
                    else
                        Update(entity: entity, iteminstance: item);
                }

                context.SaveChanges();
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<ItemInstanceDto> LoadByCharacterId(long characterId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ItemInstanceDto>();
            foreach (var itemInstance in context.ItemInstance.Where(predicate: i =>
                i.CharacterId.Equals(characterId)))
            {
                var output = new ItemInstanceDto();
                Map(input: itemInstance, output: output);
                result.Add(item: output);
            }

            return result;
        }

        public ItemInstanceDto LoadById(Guid id)
        {
            using var context = DataAccessHelper.CreateContext();
            var itemInstanceDto = new ItemInstanceDto();
            if (Map(input: context.ItemInstance.FirstOrDefault(predicate: i => i.Id.Equals(id)),
                output: itemInstanceDto))
                return itemInstanceDto;

            return null;
        }

        public ItemInstanceDto LoadBySlotAndType(long characterId, short slot, InventoryType type)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = context.ItemInstance.FirstOrDefault(predicate: i =>
                    i.CharacterId == characterId && i.Slot == slot && i.Type == type);
                var output = new ItemInstanceDto();
                if (Map(input: entity, output: output)) return output;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<ItemInstanceDto> LoadByType(long characterId, InventoryType type)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ItemInstanceDto>();
            foreach (var itemInstance in context.ItemInstance.Where(predicate: i =>
                i.CharacterId == characterId && i.Type == type))
            {
                var output = new ItemInstanceDto();
                Map(input: itemInstance, output: output);
                result.Add(item: output);
            }

            return result;
        }

        public IList<Guid> LoadSlotAndTypeByCharacterId(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                return context.ItemInstance.Where(predicate: i => i.CharacterId.Equals(characterId))
                    .Select(selector: i => i.Id)
                    .ToList();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        protected static ItemInstanceDto Insert(ItemInstanceDto dto, OpenNosContext context)
        {
            var entity = new ItemInstance();
            Map(input: dto, output: entity);
            context.Set<ItemInstance>().Add(entity: entity);
            context.SaveChanges();
            if (Map(input: entity, output: dto)) return dto;

            return null;
        }

        protected static ItemInstanceDto InsertOrUpdate(OpenNosContext context, ItemInstanceDto dto)
        {
            try
            {
                var entity = context.ItemInstance.FirstOrDefault(predicate: c => c.Id == dto.Id);
                return entity == null
                    ? Insert(dto: dto, context: context)
                    : Update(entity: entity, inventory: dto, context: context);
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        protected static ItemInstanceDto Update(ItemInstance entity, ItemInstanceDto inventory, OpenNosContext context)
        {
            if (entity != null)
            {
                Map(input: inventory, output: entity);
                context.SaveChanges();
            }

            if (Map(input: entity, output: inventory)) return inventory;
            return null;
        }

        // TODO: Implement Exists
        static bool Map(ItemInstance input, ItemInstanceDto output)
        {
            if (input == null) return false;

            ItemInstanceMapper.ToItemInstanceDto(input: input, output: output);
            if (output.EquipmentSerialId == Guid.Empty) output.EquipmentSerialId = Guid.NewGuid();
            return true;
        }

        static void Map(ItemInstanceDto input, ItemInstance output)
        {
            if (input == null) return;

            ItemInstanceMapper.ToItemInstance(input: input, output: output);
            if (output.EquipmentSerialId == Guid.Empty) output.EquipmentSerialId = Guid.NewGuid();
        }

        #endregion
    }
}