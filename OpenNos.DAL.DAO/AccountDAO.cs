﻿using System;
using System.Data.Entity;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Domain;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class AccountDao : IAccountDao
    {
        #region Methods

        public DeleteResult Delete(long accountId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var account = context.Account.FirstOrDefault(predicate: c => c.AccountId.Equals(accountId));

                if (account != null)
                {
                    context.Account.Remove(entity: account);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_ACCOUNT_ERROR"),
                        arg0: accountId, arg1: e.Message),
                    ex: e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref AccountDto account)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var accountId = account.AccountId;
                var entity = context.Account.FirstOrDefault(predicate: c => c.AccountId.Equals(accountId));

                if (entity == null)
                {
                    account = Insert(account: account, context: context);
                    return SaveResult.Inserted;
                }

                account = Update(entity: entity, account: account, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_ACCOUNT_ERROR"),
                        arg0: account.AccountId,
                        arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        public AccountDto LoadById(long accountId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var account = context.Account.FirstOrDefault(predicate: a => a.AccountId.Equals(accountId));
                if (account != null)
                {
                    var accountDto = new AccountDto();
                    if (AccountMapper.ToAccountDto(input: account, output: accountDto)) return accountDto;
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return null;
        }

        public AccountDto LoadByName(string name)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var account = context.Account.FirstOrDefault(predicate: a => a.Name.Equals(name));
                if (account != null)
                {
                    var accountDto = new AccountDto();
                    if (AccountMapper.ToAccountDto(input: account, output: accountDto)) return accountDto;
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return null;
        }

        public void WriteGeneralLog(long accountId, string ipAddress, long? characterId, GeneralLogType logType,
            string logData)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var log = new GeneralLog
                {
                    AccountId = accountId,
                    IpAddress = ipAddress,
                    Timestamp = DateTime.Now,
                    LogType = logType.ToString(),
                    LogData = logData,
                    CharacterId = characterId
                };

                context.GeneralLog.Add(entity: log);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        static AccountDto Insert(AccountDto account, OpenNosContext context)
        {
            var entity = new Account();
            AccountMapper.ToAccount(input: account, output: entity);
            context.Account.Add(entity: entity);
            context.SaveChanges();
            AccountMapper.ToAccountDto(input: entity, output: account);
            return account;
        }

        static AccountDto Update(Account entity, AccountDto account, OpenNosContext context)
        {
            if (entity != null)
            {
                AccountMapper.ToAccount(input: account, output: entity);
                context.Entry(entity: entity).State = EntityState.Modified;
                context.SaveChanges();
            }

            if (AccountMapper.ToAccountDto(input: entity, output: account)) return account;

            return null;
        }

        #endregion
    }
}