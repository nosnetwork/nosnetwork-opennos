﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class MateDao : IMateDao
    {
        #region Methods

        public DeleteResult Delete(long id)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var mate = context.Mate.FirstOrDefault(predicate: c => c.MateId.Equals(id));
                if (mate != null)
                {
                    context.Mate.Remove(entity: mate);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_MATE_ERROR"),
                        arg0: e.Message), ex: e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref MateDto mate)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var mateId = mate.MateId;
                var entity = context.Mate.FirstOrDefault(predicate: c => c.MateId.Equals(mateId));

                if (entity == null)
                {
                    mate = Insert(mate: mate, context: context);
                    return SaveResult.Inserted;
                }

                mate = Update(entity: entity, character: mate, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSERT_ERROR"), arg0: mate,
                        arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<MateDto> LoadByCharacterId(long characterId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MateDto>();
            foreach (var mate in context.Mate.Where(predicate: s => s.CharacterId == characterId))
            {
                var dto = new MateDto();
                MateMapper.ToMateDto(input: mate, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        static MateDto Insert(MateDto mate, OpenNosContext context)
        {
            var entity = new Mate();
            MateMapper.ToMate(input: mate, output: entity);
            context.Mate.Add(entity: entity);
            context.SaveChanges();
            if (MateMapper.ToMateDto(input: entity, output: mate)) return mate;

            return null;
        }

        static MateDto Update(Mate entity, MateDto character, OpenNosContext context)
        {
            if (entity != null)
            {
                MateMapper.ToMate(input: character, output: entity);
                context.SaveChanges();
            }

            if (MateMapper.ToMateDto(input: entity, output: character)) return character;

            return null;
        }

        #endregion
    }
}