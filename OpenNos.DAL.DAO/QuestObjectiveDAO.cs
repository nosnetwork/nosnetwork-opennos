﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class QuestObjectiveDao : IQuestObjectiveDao
    {
        #region Methods

        public void Insert(List<QuestObjectiveDto> quests)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var quest in quests)
                {
                    var entity = new QuestObjective();
                    QuestObjectiveMapper.ToQuestObjective(input: quest, output: entity);
                    context.QuestObjective.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public QuestObjectiveDto Insert(QuestObjectiveDto questObjective)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new QuestObjective();
                QuestObjectiveMapper.ToQuestObjective(input: questObjective, output: entity);
                context.QuestObjective.Add(entity: entity);
                context.SaveChanges();
                if (QuestObjectiveMapper.ToQuestObjectiveDto(input: entity, output: questObjective))
                    return questObjective;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public List<QuestObjectiveDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<QuestObjectiveDto>();
            foreach (var questObjective in context.QuestObjective)
            {
                var dto = new QuestObjectiveDto();
                QuestObjectiveMapper.ToQuestObjectiveDto(input: questObjective, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<QuestObjectiveDto> LoadByQuestId(long questId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<QuestObjectiveDto>();
            foreach (var questObjective in context.QuestObjective.Where(predicate: s => s.QuestId == questId))
            {
                var dto = new QuestObjectiveDto();
                QuestObjectiveMapper.ToQuestObjectiveDto(input: questObjective, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}