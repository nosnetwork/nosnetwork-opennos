﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class CellonOptionDao : ICellonOptionDao
    {
        #region Methods

        public DeleteResult DeleteByEquipmentSerialId(Guid id)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var deleteentities = context.CellonOption.Where(predicate: s => s.EquipmentSerialId == id).ToList();
                if (deleteentities.Count != 0)
                {
                    context.CellonOption.RemoveRange(entities: deleteentities);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_ERROR"), arg0: id,
                        arg1: e.Message), ex: e);
                return DeleteResult.Error;
            }
        }

        public IEnumerable<CellonOptionDto> GetOptionsByWearableInstanceId(Guid wearableInstanceId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CellonOptionDto>();
            foreach (var entity in context.CellonOption.Where(predicate: c =>
                c.EquipmentSerialId == wearableInstanceId))
            {
                var dto = new CellonOptionDto();
                CellonOptionMapper.ToCellonOptionDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public CellonOptionDto InsertOrUpdate(CellonOptionDto cellonOption)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var cellonOptionId = cellonOption.CellonOptionId;
                var entity =
                    context.CellonOption.FirstOrDefault(predicate: c => c.CellonOptionId.Equals(cellonOptionId));

                if (entity == null) return Insert(cellonOption: cellonOption, context: context);
                return Update(entity: entity, cellonOption: cellonOption, context: context);
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSERT_ERROR"),
                        arg0: cellonOption, arg1: e.Message), ex: e);
                return cellonOption;
            }
        }

        public void InsertOrUpdateFromList(List<CellonOptionDto> cellonOption, Guid equipmentSerialId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();

                void Insert(CellonOptionDto cellonoption)
                {
                    var entity = new CellonOption();
                    CellonOptionMapper.ToCellonOption(input: cellonoption, output: entity);
                    context.CellonOption.Add(entity: entity);
                    context.SaveChanges();
                    cellonoption.CellonOptionId = entity.CellonOptionId;
                }

                static void Update(CellonOption entity, CellonOptionDto cellonoption)
                {
                    if (entity != null) CellonOptionMapper.ToCellonOption(input: cellonoption, output: entity);
                }

                foreach (var item in cellonOption)
                {
                    item.EquipmentSerialId = equipmentSerialId;
                    var entity =
                        context.CellonOption.FirstOrDefault(predicate: c =>
                            c.CellonOptionId == item.CellonOptionId);

                    if (entity == null)
                        Insert(cellonoption: item);
                    else
                        Update(entity: entity, cellonoption: item);
                }

                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        static CellonOptionDto Insert(CellonOptionDto cellonOption, OpenNosContext context)
        {
            var entity = new CellonOption();
            CellonOptionMapper.ToCellonOption(input: cellonOption, output: entity);
            context.CellonOption.Add(entity: entity);
            context.SaveChanges();
            if (CellonOptionMapper.ToCellonOptionDto(input: entity, output: cellonOption)) return cellonOption;

            return null;
        }

        static CellonOptionDto Update(CellonOption entity, CellonOptionDto cellonOption, OpenNosContext context)
        {
            if (entity != null)
            {
                CellonOptionMapper.ToCellonOption(input: cellonOption, output: entity);
                context.SaveChanges();
            }

            if (CellonOptionMapper.ToCellonOptionDto(input: entity, output: cellonOption)) return cellonOption;

            return null;
        }

        #endregion
    }
}