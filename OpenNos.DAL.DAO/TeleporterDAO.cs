﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class TeleporterDao : ITeleporterDao
    {
        #region Methods

        public TeleporterDto Insert(TeleporterDto teleporter)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new Teleporter();
                TeleporterMapper.ToTeleporter(input: teleporter, output: entity);
                context.Teleporter.Add(entity: entity);
                context.SaveChanges();
                if (TeleporterMapper.ToTeleporterDto(input: entity, output: teleporter)) return teleporter;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<TeleporterDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<TeleporterDto>();
            foreach (var entity in context.Teleporter)
            {
                var dto = new TeleporterDto();
                TeleporterMapper.ToTeleporterDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public TeleporterDto LoadById(short teleporterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new TeleporterDto();
                if (TeleporterMapper.ToTeleporterDto(
                    input: context.Teleporter.FirstOrDefault(predicate: i => i.TeleporterId.Equals(teleporterId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<TeleporterDto> LoadFromNpc(int npcId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<TeleporterDto>();
            foreach (var entity in context.Teleporter.Where(predicate: c => c.MapNpcId.Equals(npcId)))
            {
                var dto = new TeleporterDto();
                TeleporterMapper.ToTeleporterDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}