﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class PortalDao : IPortalDao
    {
        #region Methods

        public void Insert(List<PortalDto> portals)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var item in portals)
                {
                    var entity = new Portal();
                    PortalMapper.ToPortal(input: item, output: entity);
                    context.Portal.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public PortalDto Insert(PortalDto portal)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new Portal();
                PortalMapper.ToPortal(input: portal, output: entity);
                context.Portal.Add(entity: entity);
                context.SaveChanges();
                if (PortalMapper.ToPortalDto(input: entity, output: portal)) return portal;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<PortalDto> LoadByMap(short mapId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<PortalDto>();
            foreach (var portalobject in context.Portal.Where(predicate: c => c.SourceMapId.Equals(mapId)))
            {
                var dto = new PortalDto();
                PortalMapper.ToPortalDto(input: portalobject, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}