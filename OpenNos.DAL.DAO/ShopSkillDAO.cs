﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class ShopSkillDao : IShopSkillDao
    {
        #region Methods

        public ShopSkillDto Insert(ShopSkillDto shopSkill)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new ShopSkill();
                ShopSkillMapper.ToShopSkill(input: shopSkill, output: entity);
                context.ShopSkill.Add(entity: entity);
                context.SaveChanges();
                if (ShopSkillMapper.ToShopSkillDto(input: entity, output: shopSkill)) return shopSkill;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public void Insert(List<ShopSkillDto> skills)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var skill in skills)
                {
                    var entity = new ShopSkill();
                    ShopSkillMapper.ToShopSkill(input: skill, output: entity);
                    context.ShopSkill.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public IEnumerable<ShopSkillDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ShopSkillDto>();
            foreach (var entity in context.ShopSkill)
            {
                var dto = new ShopSkillDto();
                ShopSkillMapper.ToShopSkillDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<ShopSkillDto> LoadByShopId(int shopId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ShopSkillDto>();
            foreach (var shopSkill in context.ShopSkill.Where(predicate: s => s.ShopId.Equals(shopId)))
            {
                var dto = new ShopSkillDto();
                ShopSkillMapper.ToShopSkillDto(input: shopSkill, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}