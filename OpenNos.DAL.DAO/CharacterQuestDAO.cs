﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class CharacterQuestDao : ICharacterQuestDao
    {
        #region Methods

        public DeleteResult Delete(long characterId, long questId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var charQuest =
                    context.CharacterQuest.FirstOrDefault(predicate: i =>
                        i.CharacterId == characterId && i.QuestId == questId);
                if (charQuest != null)
                {
                    context.CharacterQuest.Remove(entity: charQuest);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public CharacterQuestDto InsertOrUpdate(CharacterQuestDto charQuest)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                return InsertOrUpdate(context: context, dto: charQuest);
            }
            catch (Exception e)
            {
                Logger.Error(data: $"Message: {e.Message}", ex: e);
                return null;
            }
        }

        protected static CharacterQuestDto InsertOrUpdate(OpenNosContext context, CharacterQuestDto dto)
        {
            var primaryKey = dto.Id;
            var entity = context.Set<CharacterQuest>().FirstOrDefault(predicate: c => c.Id == primaryKey);
            if (entity == null)
                return Insert(charQuest: dto, context: context);
            return Update(entity: entity, charQuest: dto, context: context);
        }

        static CharacterQuestDto Insert(CharacterQuestDto charQuest, OpenNosContext context)
        {
            var entity = new CharacterQuest();
            CharacterQuestMapper.ToCharacterQuest(input: charQuest, output: entity);
            context.CharacterQuest.Add(entity: entity);
            context.SaveChanges();
            if (CharacterQuestMapper.ToCharacterQuestDto(input: entity, output: charQuest)) return charQuest;

            return null;
        }

        static CharacterQuestDto Update(CharacterQuest entity, CharacterQuestDto charQuest, OpenNosContext context)
        {
            if (entity != null)
            {
                CharacterQuestMapper.ToCharacterQuest(input: charQuest, output: entity);
                context.SaveChanges();
            }

            if (CharacterQuestMapper.ToCharacterQuestDto(input: entity, output: charQuest)) return charQuest;

            return null;
        }

        public IEnumerable<CharacterQuestDto> LoadByCharacterId(long characterId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CharacterQuestDto>();
            foreach (var charQuest in context.CharacterQuest.Where(predicate: s => s.CharacterId == characterId))
            {
                var dto = new CharacterQuestDto();
                CharacterQuestMapper.ToCharacterQuestDto(input: charQuest, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<Guid> LoadKeysByCharacterId(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                return context.CharacterQuest.Where(predicate: i => i.CharacterId == characterId)
                    .Select(selector: c => c.Id).ToList();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        #endregion
    }
}