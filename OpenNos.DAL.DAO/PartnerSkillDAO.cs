﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class PartnerSkillDao : IPartnerSkillDao
    {
        public PartnerSkillDto Insert(PartnerSkillDto partnerSkillDto)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var partnerSkill = new PartnerSkill();

                if (PartnerSkillMapper.ToPartnerSkill(input: partnerSkillDto, output: partnerSkill))
                {
                    context.PartnerSkill.Add(entity: partnerSkill);
                    context.SaveChanges();

                    var dto = new PartnerSkillDto();

                    if (PartnerSkillMapper.ToPartnerSkillDto(input: partnerSkill, output: dto)) return dto;
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return null;
        }

        public List<PartnerSkillDto> LoadByEquipmentSerialId(Guid equipmentSerialId)
        {
            var partnerSkillDtOs = new List<PartnerSkillDto>();

            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.PartnerSkill.Where(predicate: s => s.EquipmentSerialId == equipmentSerialId).ToList()
                    .ForEach(action: partnerSkill =>
                    {
                        var partnerSkillDto = new PartnerSkillDto();

                        if (PartnerSkillMapper.ToPartnerSkillDto(input: partnerSkill, output: partnerSkillDto))
                            partnerSkillDtOs.Add(item: partnerSkillDto);
                    });
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return partnerSkillDtOs;
        }

        public DeleteResult Remove(long partnerSkillId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var partnerSkill =
                    context.PartnerSkill.FirstOrDefault(predicate: s => s.PartnerSkillId == partnerSkillId);

                if (partnerSkill == null) return DeleteResult.NotFound;

                context.PartnerSkill.Remove(entity: partnerSkill);
                context.SaveChanges();

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return DeleteResult.Error;
        }
    }
}