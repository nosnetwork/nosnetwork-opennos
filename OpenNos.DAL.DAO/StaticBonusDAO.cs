﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Domain;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class StaticBonusDao : IStaticBonusDao
    {
        #region Methods

        public void Delete(short bonusToDelete, long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var bon = context.StaticBonus.FirstOrDefault(predicate: c =>
                    c.StaticBonusType == (StaticBonusType)bonusToDelete && c.CharacterId == characterId);

                if (bon != null)
                {
                    context.StaticBonus.Remove(entity: bon);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_ERROR"),
                        arg0: bonusToDelete, arg1: e.Message), ex: e);
            }
        }

        public SaveResult InsertOrUpdate(ref StaticBonusDto staticBonus)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var id = staticBonus.CharacterId;
                var cardid = staticBonus.StaticBonusType;
                var entity =
                    context.StaticBonus.FirstOrDefault(predicate: c =>
                        c.StaticBonusType == cardid && c.CharacterId == id);

                if (entity == null)
                {
                    staticBonus = Insert(sb: staticBonus, context: context);
                    return SaveResult.Inserted;
                }

                staticBonus.StaticBonusId = entity.StaticBonusId;
                staticBonus = Update(entity: entity, sb: staticBonus, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<StaticBonusDto> LoadByCharacterId(long characterId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<StaticBonusDto>();
            foreach (var entity in context.StaticBonus.Where(predicate: i =>
                i.CharacterId == characterId && i.DateEnd > DateTime.Now))
            {
                var dto = new StaticBonusDto();
                StaticBonusMapper.ToStaticBonusDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public static StaticBonusDto LoadById(long sbId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new StaticBonusDto();
                if (StaticBonusMapper.ToStaticBonusDto(
                    input: context.StaticBonus.FirstOrDefault(predicate: s => s.StaticBonusId.Equals(sbId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<short> LoadTypeByCharacterId(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                return context.StaticBonus.Where(predicate: i => i.CharacterId == characterId)
                    .Select(selector: qle => (short)qle.StaticBonusType).ToList();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static StaticBonusDto Insert(StaticBonusDto sb, OpenNosContext context)
        {
            try
            {
                var entity = new StaticBonus();
                StaticBonusMapper.ToStaticBonus(input: sb, output: entity);
                context.StaticBonus.Add(entity: entity);
                context.SaveChanges();
                if (StaticBonusMapper.ToStaticBonusDto(input: entity, output: sb)) return sb;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static StaticBonusDto Update(StaticBonus entity, StaticBonusDto sb, OpenNosContext context)
        {
            if (entity != null)
            {
                StaticBonusMapper.ToStaticBonus(input: sb, output: entity);
                context.SaveChanges();
            }

            if (StaticBonusMapper.ToStaticBonusDto(input: entity, output: sb)) return sb;

            return null;
        }

        #endregion
    }
}