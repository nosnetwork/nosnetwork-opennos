﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class ComboDao : IComboDao
    {
        #region Methods

        public void Insert(List<ComboDto> combos)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var combo in combos)
                {
                    var entity = new Combo();
                    ComboMapper.ToCombo(input: combo, output: entity);
                    context.Combo.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public ComboDto Insert(ComboDto combo)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new Combo();
                ComboMapper.ToCombo(input: combo, output: entity);
                context.Combo.Add(entity: entity);
                context.SaveChanges();
                if (ComboMapper.ToComboDto(input: entity, output: combo)) return combo;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<ComboDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ComboDto>();
            foreach (var combo in context.Combo)
            {
                var dto = new ComboDto();
                ComboMapper.ToComboDto(input: combo, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public ComboDto LoadById(short comboId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new ComboDto();
                if (ComboMapper.ToComboDto(
                    input: context.Combo.FirstOrDefault(predicate: s => s.SkillVNum.Equals(comboId)), output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<ComboDto> LoadBySkillVnum(short skillVNum)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ComboDto>();
            foreach (var combo in context.Combo.Where(predicate: c => c.SkillVNum == skillVNum))
            {
                var dto = new ComboDto();
                ComboMapper.ToComboDto(input: combo, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<ComboDto> LoadByVNumHitAndEffect(short skillVNum, short hit, short effect)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ComboDto>();
            foreach (var combo in context.Combo.Where(predicate: s =>
                s.SkillVNum == skillVNum && s.Hit == hit && s.Effect == effect))
            {
                var dto = new ComboDto();
                ComboMapper.ToComboDto(input: combo, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}