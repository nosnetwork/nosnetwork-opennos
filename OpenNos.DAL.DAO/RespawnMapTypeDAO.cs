﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class RespawnMapTypeDao : IRespawnMapTypeDao
    {
        #region Methods

        public void Insert(List<RespawnMapTypeDto> respawnMapTypes)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var respawnMapType in respawnMapTypes)
                {
                    var entity = new RespawnMapType();
                    RespawnMapTypeMapper.ToRespawnMapType(input: respawnMapType, output: entity);
                    context.RespawnMapType.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public SaveResult InsertOrUpdate(ref RespawnMapTypeDto respawnMapType)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var mapId = respawnMapType.DefaultMapId;
                var entity = context.RespawnMapType.FirstOrDefault(predicate: c => c.DefaultMapId.Equals(mapId));

                if (entity == null)
                {
                    respawnMapType = Insert(respawnMapType: respawnMapType, context: context);
                    return SaveResult.Inserted;
                }

                respawnMapType.RespawnMapTypeId = entity.RespawnMapTypeId;
                respawnMapType = Update(entity: entity, respawnMapType: respawnMapType, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return SaveResult.Error;
            }
        }

        public RespawnMapTypeDto LoadById(long respawnMapTypeId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new RespawnMapTypeDto();
                if (RespawnMapTypeMapper.ToRespawnMapTypeDto(
                    input: context.RespawnMapType.FirstOrDefault(predicate: s =>
                        s.RespawnMapTypeId.Equals(respawnMapTypeId)), output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public RespawnMapTypeDto LoadByMapId(short mapId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new RespawnMapTypeDto();
                if (RespawnMapTypeMapper.ToRespawnMapTypeDto(
                    input: context.RespawnMapType.FirstOrDefault(predicate: s => s.DefaultMapId.Equals(mapId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static RespawnMapTypeDto Insert(RespawnMapTypeDto respawnMapType, OpenNosContext context)
        {
            try
            {
                var entity = new RespawnMapType();
                RespawnMapTypeMapper.ToRespawnMapType(input: respawnMapType, output: entity);
                context.RespawnMapType.Add(entity: entity);
                context.SaveChanges();
                if (RespawnMapTypeMapper.ToRespawnMapTypeDto(input: entity, output: respawnMapType))
                    return respawnMapType;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static RespawnMapTypeDto Update(RespawnMapType entity, RespawnMapTypeDto respawnMapType, OpenNosContext context)
        {
            if (entity != null)
            {
                RespawnMapTypeMapper.ToRespawnMapType(input: respawnMapType, output: entity);
                context.SaveChanges();
            }

            if (RespawnMapTypeMapper.ToRespawnMapTypeDto(input: entity, output: respawnMapType)) return respawnMapType;

            return null;
        }

        #endregion
    }
}