﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class QuestRewardDao : IQuestRewardDao
    {
        #region Methods

        public void Insert(List<QuestRewardDto> questRewards)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var rewards in questRewards)
                {
                    var entity = new QuestReward();
                    QuestRewardMapper.ToQuestReward(input: rewards, output: entity);
                    context.QuestReward.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public QuestRewardDto Insert(QuestRewardDto questReward)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new QuestReward();
                QuestRewardMapper.ToQuestReward(input: questReward, output: entity);
                context.QuestReward.Add(entity: entity);
                context.SaveChanges();
                if (QuestRewardMapper.ToQuestRewardDto(input: entity, output: questReward)) return questReward;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public List<QuestRewardDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<QuestRewardDto>();
            foreach (var entity in context.QuestReward)
            {
                var dto = new QuestRewardDto();
                QuestRewardMapper.ToQuestRewardDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<QuestRewardDto> LoadByQuestId(long questId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<QuestRewardDto>();
            foreach (var reward in context.QuestReward.Where(predicate: s => s.QuestId == questId))
            {
                var dto = new QuestRewardDto();
                QuestRewardMapper.ToQuestRewardDto(input: reward, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}