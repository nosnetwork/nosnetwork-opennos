﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class RespawnDao : IRespawnDao
    {
        #region Methods

        public SaveResult InsertOrUpdate(ref RespawnDto respawn)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var characterId = respawn.CharacterId;
                var respawnMapTypeId = respawn.RespawnMapTypeId;
                var entity = context.Respawn.FirstOrDefault(predicate: c =>
                    c.RespawnMapTypeId.Equals(respawnMapTypeId) && c.CharacterId.Equals(characterId));

                if (entity == null)
                {
                    respawn = Insert(respawn: respawn, context: context);
                    return SaveResult.Inserted;
                }

                respawn.RespawnId = entity.RespawnId;
                respawn = Update(entity: entity, respawn: respawn, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<RespawnDto> LoadByCharacter(long characterId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RespawnDto>();
            foreach (var respawnobject in context.Respawn.Where(predicate: i => i.CharacterId.Equals(characterId)))
            {
                var dto = new RespawnDto();
                RespawnMapper.ToRespawnDto(input: respawnobject, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public RespawnDto LoadById(long respawnId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new RespawnDto();
                if (RespawnMapper.ToRespawnDto(
                    input: context.Respawn.FirstOrDefault(predicate: s => s.RespawnId.Equals(respawnId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static RespawnDto Insert(RespawnDto respawn, OpenNosContext context)
        {
            try
            {
                var entity = new Respawn();
                RespawnMapper.ToRespawn(input: respawn, output: entity);
                context.Respawn.Add(entity: entity);
                context.SaveChanges();
                if (RespawnMapper.ToRespawnDto(input: entity, output: respawn)) return respawn;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static RespawnDto Update(Respawn entity, RespawnDto respawn, OpenNosContext context)
        {
            if (entity != null)
            {
                RespawnMapper.ToRespawn(input: respawn, output: entity);
                context.SaveChanges();
            }

            if (RespawnMapper.ToRespawnDto(input: entity, output: respawn)) return respawn;

            return null;
        }

        #endregion
    }
}