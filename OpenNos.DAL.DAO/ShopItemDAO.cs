﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class ShopItemDao : IShopItemDao
    {
        #region Methods

        public DeleteResult DeleteById(int itemId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var item = context.ShopItem.FirstOrDefault(predicate: i => i.ShopItemId.Equals(itemId));

                if (item != null)
                {
                    context.ShopItem.Remove(entity: item);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public ShopItemDto Insert(ShopItemDto item)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new ShopItem();
                ShopItemMapper.ToShopItem(input: item, output: entity);
                context.ShopItem.Add(entity: entity);
                context.SaveChanges();
                if (ShopItemMapper.ToShopItemDto(input: entity, output: item)) return item;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public void Insert(List<ShopItemDto> items)
        {
            foreach (var item in items) Insert(item: item);
        }

        public IEnumerable<ShopItemDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ShopItemDto>();
            foreach (var entity in context.ShopItem)
            {
                var dto = new ShopItemDto();
                ShopItemMapper.ToShopItemDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public ShopItemDto LoadById(int itemId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new ShopItemDto();
                if (ShopItemMapper.ToShopItemDto(
                    input: context.ShopItem.FirstOrDefault(predicate: i => i.ShopItemId.Equals(itemId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<ShopItemDto> LoadByShopId(int shopId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ShopItemDto>();
            foreach (var shopItem in context.ShopItem.Where(predicate: i => i.ShopId.Equals(shopId)))
            {
                var dto = new ShopItemDto();
                ShopItemMapper.ToShopItemDto(input: shopItem, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}