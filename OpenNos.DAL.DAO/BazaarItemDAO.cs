﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class BazaarItemDao : IBazaarItemDao
    {
        #region Methods

        public DeleteResult Delete(long bazaarItemId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var bazaarItem =
                    context.BazaarItem.FirstOrDefault(predicate: c => c.BazaarItemId.Equals(bazaarItemId));

                if (bazaarItem != null)
                {
                    context.BazaarItem.Remove(entity: bazaarItem);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_ERROR"),
                        arg0: bazaarItemId, arg1: e.Message), ex: e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref BazaarItemDto bazaarItem)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var bazaarItemId = bazaarItem.BazaarItemId;
                var entity = context.BazaarItem.FirstOrDefault(predicate: c => c.BazaarItemId.Equals(bazaarItemId));

                if (entity == null)
                {
                    bazaarItem = Insert(bazaarItem: bazaarItem, context: context);
                    return SaveResult.Inserted;
                }

                bazaarItem = Update(entity: entity, bazaarItem: bazaarItem, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(data: $"BazaarItemId: {bazaarItem.BazaarItemId} Message: {e.Message}", ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<BazaarItemDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<BazaarItemDto>();
            foreach (var bazaarItem in context.BazaarItem)
            {
                var dto = new BazaarItemDto();
                BazaarItemMapper.ToBazaarItemDto(input: bazaarItem, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public BazaarItemDto LoadById(long bazaarItemId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new BazaarItemDto();
                if (BazaarItemMapper.ToBazaarItemDto(
                    input: context.BazaarItem.FirstOrDefault(predicate: i => i.BazaarItemId.Equals(bazaarItemId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public void RemoveOutDated()
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                foreach (var entity in context.BazaarItem.Where(predicate: e =>
                    DbFunctions.AddDays(DbFunctions.AddHours(e.DateStart, e.Duration), e.MedalUsed ? 30 : 7) <
                    DateTime.Now)) context.BazaarItem.Remove(entity: entity);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        static BazaarItemDto Insert(BazaarItemDto bazaarItem, OpenNosContext context)
        {
            var entity = new BazaarItem();
            BazaarItemMapper.ToBazaarItem(input: bazaarItem, output: entity);
            context.BazaarItem.Add(entity: entity);
            context.SaveChanges();
            if (BazaarItemMapper.ToBazaarItemDto(input: entity, output: bazaarItem)) return bazaarItem;

            return null;
        }

        static BazaarItemDto Update(BazaarItem entity, BazaarItemDto bazaarItem, OpenNosContext context)
        {
            if (entity != null)
            {
                BazaarItemMapper.ToBazaarItem(input: bazaarItem, output: entity);
                context.SaveChanges();
            }

            if (BazaarItemMapper.ToBazaarItemDto(input: entity, output: bazaarItem)) return bazaarItem;

            return null;
        }

        #endregion
    }
}