﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class MapNpcDao : IMapNpcDao
    {
        #region Methods

        public DeleteResult DeleteById(int mapNpcId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var npc = context.MapNpc.First(predicate: i => i.MapNpcId.Equals(mapNpcId));

                if (npc != null)
                {
                    context.MapNpc.Remove(entity: npc);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public bool DoesNpcExist(int mapNpcId)
        {
            using var context = DataAccessHelper.CreateContext();
            return context.MapNpc.Any(predicate: i => i.MapNpcId.Equals(mapNpcId));
        }

        public void Insert(List<MapNpcDto> npcs)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var item in npcs)
                {
                    var entity = new MapNpc();
                    MapNpcMapper.ToMapNpc(input: item, output: entity);
                    context.MapNpc.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public MapNpcDto Insert(MapNpcDto npc)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new MapNpc();
                MapNpcMapper.ToMapNpc(input: npc, output: entity);
                context.MapNpc.Add(entity: entity);
                context.SaveChanges();
                if (MapNpcMapper.ToMapNpcDto(input: entity, output: npc)) return npc;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public SaveResult Update(ref MapNpcDto mapNpc)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var mapNpcId = mapNpc.MapNpcId;
                var entity = context.MapNpc.FirstOrDefault(predicate: c => c.MapNpcId.Equals(mapNpcId));

                mapNpc = Update(entity: entity, mapNpc: mapNpc, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_MAPNPC_ERROR"),
                        arg0: mapNpc.MapNpcId,
                        arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        static MapNpcDto Update(MapNpc entity, MapNpcDto mapNpc, OpenNosContext context)
        {
            if (entity != null)
            {
                MapNpcMapper.ToMapNpc(input: mapNpc, output: entity);
                context.Entry(entity: entity).State = EntityState.Modified;
                context.SaveChanges();
            }

            if (MapNpcMapper.ToMapNpcDto(input: entity, output: mapNpc)) return mapNpc;

            return null;
        }

        public IEnumerable<MapNpcDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MapNpcDto>();
            foreach (var entity in context.MapNpc)
            {
                var dto = new MapNpcDto();
                MapNpcMapper.ToMapNpcDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public MapNpcDto LoadById(int mapNpcId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new MapNpcDto();
                if (MapNpcMapper.ToMapNpcDto(
                    input: context.MapNpc.FirstOrDefault(predicate: i => i.MapNpcId.Equals(mapNpcId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<MapNpcDto> LoadFromMap(short mapId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MapNpcDto>();
            foreach (var npcobject in context.MapNpc.Where(predicate: c => c.MapId.Equals(mapId)))
            {
                var dto = new MapNpcDto();
                MapNpcMapper.ToMapNpcDto(input: npcobject, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}