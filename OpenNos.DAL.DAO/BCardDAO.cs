﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class BCardDao : IBCardDao
    {
        #region Methods

        public DeleteResult DeleteByItemVNum(short itemVNum)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                IEnumerable<BCard> bCards = context.BCard.Where(predicate: s => s.ItemVNum == itemVNum);

                foreach (var bcard in bCards) context.BCard.Remove(entity: bcard);
                context.SaveChanges();

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public DeleteResult DeleteBySkillVNum(short skillVNum)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                IEnumerable<BCard> bCards = context.BCard.Where(predicate: s => s.SkillVNum == skillVNum);

                foreach (var bcard in bCards) context.BCard.Remove(entity: bcard);
                context.SaveChanges();

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public DeleteResult DeleteByMonsterVNum(short monsterVNum)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                IEnumerable<BCard> bCards = context.BCard.Where(predicate: s => s.NpcMonsterVNum == monsterVNum);

                foreach (var bcard in bCards) context.BCard.Remove(entity: bcard);
                context.SaveChanges();

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public DeleteResult DeleteByCardId(short cardId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                IEnumerable<BCard> bCards = context.BCard.Where(predicate: s => s.CardId == cardId);

                foreach (var bcard in bCards) context.BCard.Remove(entity: bcard);
                context.SaveChanges();

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public BCardDto Insert(ref BCardDto cardObject)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new BCard();
                BCardMapper.ToBCard(input: cardObject, output: entity);
                context.BCard.Add(entity: entity);
                context.SaveChanges();
                if (BCardMapper.ToBCardDto(input: entity, output: cardObject)) return cardObject;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public void Insert(List<BCardDto> cards)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var card in cards)
                {
                    var entity = new BCard();
                    BCardMapper.ToBCard(input: card, output: entity);
                    context.BCard.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public IEnumerable<BCardDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<BCardDto>();
            foreach (var card in context.BCard)
            {
                var dto = new BCardDto();
                BCardMapper.ToBCardDto(input: card, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<BCardDto> LoadByCardId(short cardId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<BCardDto>();
            foreach (var card in context.BCard.Where(predicate: s => s.CardId == cardId))
            {
                var dto = new BCardDto();
                BCardMapper.ToBCardDto(input: card, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public BCardDto LoadById(short cardId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new BCardDto();
                if (BCardMapper.ToBCardDto(
                    input: context.BCard.FirstOrDefault(predicate: s => s.BCardId.Equals(cardId)), output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<BCardDto> LoadByItemVNum(short vNum)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<BCardDto>();
            foreach (var card in context.BCard.Where(predicate: s => s.ItemVNum == vNum))
            {
                var dto = new BCardDto();
                BCardMapper.ToBCardDto(input: card, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<BCardDto> LoadByNpcMonsterVNum(short vNum)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<BCardDto>();
            foreach (var card in context.BCard.Where(predicate: s => s.NpcMonsterVNum == vNum))
            {
                var dto = new BCardDto();
                BCardMapper.ToBCardDto(input: card, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<BCardDto> LoadBySkillVNum(short vNum)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<BCardDto>();
            foreach (var card in context.BCard.Where(predicate: s => s.SkillVNum == vNum))
            {
                var dto = new BCardDto();
                BCardMapper.ToBCardDto(input: card, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}