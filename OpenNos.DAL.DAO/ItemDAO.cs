﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class ItemDao : IItemDao
    {
        #region Methods

        public IEnumerable<ItemDto> FindByName(string name)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ItemDto>();
            foreach (var item in context.Item.Where(predicate: s =>
                string.IsNullOrEmpty(name) ? s.Name.Equals("") : s.Name.Contains(name)))
            {
                var dto = new ItemDto();
                ItemMapper.ToItemDto(input: item, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public void Insert(IEnumerable<ItemDto> items)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                foreach (var item in items)
                {
                    var entity = new Item();
                    ItemMapper.ToItem(input: item, output: entity);
                    context.Item.Add(entity: entity);
                }

                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public ItemDto Insert(ItemDto item)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new Item();
                ItemMapper.ToItem(input: item, output: entity);
                context.Item.Add(entity: entity);
                context.SaveChanges();
                if (ItemMapper.ToItemDto(input: entity, output: item)) return item;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<ItemDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ItemDto>();
            foreach (var item in context.Item)
            {
                var dto = new ItemDto();
                ItemMapper.ToItemDto(input: item, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public ItemDto LoadById(short vNum)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new ItemDto();
                if (ItemMapper.ToItemDto(input: context.Item.FirstOrDefault(predicate: i => i.VNum.Equals(vNum)),
                    output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        #endregion
    }
}