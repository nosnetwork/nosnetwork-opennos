﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class CharacterSkillDao : ICharacterSkillDao
    {
        #region Methods

        public DeleteResult Delete(long characterId, short skillVNum)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var invItem = context.CharacterSkill.FirstOrDefault(predicate: i =>
                    i.CharacterId == characterId && i.SkillVNum == skillVNum);
                if (invItem != null)
                {
                    context.CharacterSkill.Remove(entity: invItem);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public DeleteResult Delete(Guid id)
        {
            using var context = DataAccessHelper.CreateContext();
            var entity = context.Set<CharacterSkill>().FirstOrDefault(predicate: i => i.Id == id);
            if (entity != null)
            {
                context.Set<CharacterSkill>().Remove(entity: entity);
                context.SaveChanges();
            }

            return DeleteResult.Deleted;
        }

        public IEnumerable<CharacterSkillDto> InsertOrUpdate(IEnumerable<CharacterSkillDto> dtos)
        {
            try
            {
                IList<CharacterSkillDto> results = new List<CharacterSkillDto>();
                using (var context = DataAccessHelper.CreateContext())
                {
                    foreach (var dto in dtos) results.Add(item: InsertOrUpdate(context: context, dto: dto));
                }

                return results;
            }
            catch (Exception e)
            {
                Logger.Error(data: $"Message: {e.Message}", ex: e);
                return Enumerable.Empty<CharacterSkillDto>();
            }
        }

        public CharacterSkillDto InsertOrUpdate(CharacterSkillDto dto)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                return InsertOrUpdate(context: context, dto: dto);
            }
            catch (Exception e)
            {
                Logger.Error(data: $"Message: {e.Message}", ex: e);
                return null;
            }
        }

        public IEnumerable<CharacterSkillDto> LoadByCharacterId(long characterId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CharacterSkillDto>();
            foreach (var entity in context.CharacterSkill.Where(predicate: i => i.CharacterId == characterId))
            {
                var output = new CharacterSkillDto();
                CharacterSkillMapper.ToCharacterSkillDto(input: entity, output: output);
                result.Add(item: output);
            }

            return result;
        }

        public CharacterSkillDto LoadById(Guid id)
        {
            using var context = DataAccessHelper.CreateContext();
            var characterSkillDto = new CharacterSkillDto();
            if (CharacterSkillMapper.ToCharacterSkillDto(
                input: context.CharacterSkill.FirstOrDefault(predicate: i => i.Id.Equals(id)),
                output: characterSkillDto))
                return characterSkillDto;

            return null;
        }

        public IEnumerable<Guid> LoadKeysByCharacterId(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                return context.CharacterSkill.Where(predicate: i => i.CharacterId == characterId)
                    .Select(selector: c => c.Id).ToList();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        protected static CharacterSkillDto Insert(CharacterSkillDto dto, OpenNosContext context)
        {
            var entity = new CharacterSkill();
            CharacterSkillMapper.ToCharacterSkill(input: dto, output: entity);
            context.Set<CharacterSkill>().Add(entity: entity);
            context.SaveChanges();
            if (CharacterSkillMapper.ToCharacterSkillDto(input: entity, output: dto)) return dto;

            return null;
        }

        protected static CharacterSkillDto InsertOrUpdate(OpenNosContext context, CharacterSkillDto dto)
        {
            var primaryKey = dto.Id;
            var entity = context.Set<CharacterSkill>().FirstOrDefault(predicate: c => c.Id == primaryKey);
            if (entity == null)
                return Insert(dto: dto, context: context);
            return Update(entity: entity, inventory: dto, context: context);
        }

        protected static CharacterSkillDto Update(CharacterSkill entity, CharacterSkillDto inventory,
            OpenNosContext context)
        {
            if (entity != null)
            {
                CharacterSkillMapper.ToCharacterSkill(input: inventory, output: entity);
                context.SaveChanges();
            }

            if (CharacterSkillMapper.ToCharacterSkillDto(input: entity, output: inventory)) return inventory;

            return null;
        }

        #endregion
    }
}