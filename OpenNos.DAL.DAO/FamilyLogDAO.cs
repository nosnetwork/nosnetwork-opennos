﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class FamilyLogDao : IFamilyLogDao
    {
        #region Methods

        public DeleteResult Delete(long familyLogId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var famlog = context.FamilyLog.FirstOrDefault(predicate: c => c.FamilyLogId.Equals(familyLogId));

                if (famlog != null)
                {
                    context.FamilyLog.Remove(entity: famlog);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_ERROR"),
                        arg0: familyLogId, arg1: e.Message),
                    ex: e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref FamilyLogDto familyLog)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var FamilyLog = familyLog.FamilyLogId; //Change FamilyLog to familylog is not possible
                var entity = context.FamilyLog.FirstOrDefault(predicate: c => c.FamilyLogId.Equals(FamilyLog));

                if (entity == null)
                {
                    familyLog = Insert(famlog: familyLog, context: context);
                    return SaveResult.Inserted;
                }

                familyLog = Update(entity: entity, famlog: familyLog, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_FAMILYLOG_ERROR"),
                        arg0: familyLog.FamilyLogId,
                        arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<FamilyLogDto> LoadByFamilyId(long familyId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<FamilyLogDto>();
            foreach (var familylog in context.FamilyLog.Where(predicate: fc => fc.FamilyId.Equals(familyId)))
            {
                var dto = new FamilyLogDto();
                FamilyLogMapper.ToFamilyLogDto(input: familylog, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        static FamilyLogDto Insert(FamilyLogDto famlog, OpenNosContext context)
        {
            var entity = new FamilyLog();
            FamilyLogMapper.ToFamilyLog(input: famlog, output: entity);
            context.FamilyLog.Add(entity: entity);
            context.SaveChanges();
            if (FamilyLogMapper.ToFamilyLogDto(input: entity, output: famlog)) return famlog;

            return null;
        }

        static FamilyLogDto Update(FamilyLog entity, FamilyLogDto famlog, OpenNosContext context)
        {
            if (entity != null)
            {
                FamilyLogMapper.ToFamilyLog(input: famlog, output: entity);
                context.SaveChanges();
            }

            if (FamilyLogMapper.ToFamilyLogDto(input: entity, output: famlog)) return famlog;

            return null;
        }

        #endregion
    }
}