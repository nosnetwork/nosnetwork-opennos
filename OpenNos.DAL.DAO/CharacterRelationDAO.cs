﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class CharacterRelationDao : ICharacterRelationDao
    {
        #region Methods

        public DeleteResult Delete(long characterRelationId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var relation =
                    context.CharacterRelation.SingleOrDefault(
                        predicate: c => c.CharacterRelationId.Equals(characterRelationId));

                if (relation != null)
                {
                    context.CharacterRelation.Remove(entity: relation);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_CHARACTER_ERROR"),
                        arg0: characterRelationId,
                        arg1: e.Message), ex: e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref CharacterRelationDto characterRelation)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var characterId = characterRelation.CharacterId;
                var relatedCharacterId = characterRelation.RelatedCharacterId;
                var entity = context.CharacterRelation.FirstOrDefault(predicate: c =>
                    c.CharacterId.Equals(characterId) && c.RelatedCharacterId.Equals(relatedCharacterId));

                if (entity == null)
                {
                    characterRelation = Insert(relation: characterRelation, context: context);
                    return SaveResult.Inserted;
                }

                characterRelation.CharacterRelationId = entity.CharacterRelationId;
                characterRelation = Update(entity: entity, relation: characterRelation, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(
                        format: Language.Instance.GetMessageFromKey(key: "UPDATE_CHARACTERRELATION_ERROR"),
                        arg0: characterRelation.CharacterRelationId, arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<CharacterRelationDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CharacterRelationDto>();
            foreach (var entity in context.CharacterRelation)
            {
                var dto = new CharacterRelationDto();
                CharacterRelationMapper.ToCharacterRelationDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public CharacterRelationDto LoadById(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new CharacterRelationDto();
                if (CharacterRelationMapper.ToCharacterRelationDto(
                    input: context.CharacterRelation.FirstOrDefault(predicate: s =>
                        s.CharacterRelationId.Equals(characterId)), output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static CharacterRelationDto Insert(CharacterRelationDto relation, OpenNosContext context)
        {
            var entity = new CharacterRelation();
            CharacterRelationMapper.ToCharacterRelation(input: relation, output: entity);
            context.CharacterRelation.Add(entity: entity);
            context.SaveChanges();
            if (CharacterRelationMapper.ToCharacterRelationDto(input: entity, output: relation)) return relation;

            return null;
        }

        static CharacterRelationDto Update(CharacterRelation entity, CharacterRelationDto relation,
            OpenNosContext context)
        {
            if (entity != null)
            {
                CharacterRelationMapper.ToCharacterRelation(input: relation, output: entity);
                context.SaveChanges();
            }

            if (CharacterRelationMapper.ToCharacterRelationDto(input: entity, output: relation)) return relation;

            return null;
        }

        #endregion
    }
}