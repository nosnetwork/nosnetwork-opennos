﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class MapTypeMapDao : IMapTypeMapDao
    {
        #region Methods

        public void Insert(List<MapTypeMapDto> mapTypeMaps)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var mapTypeMap in mapTypeMaps)
                {
                    var entity = new MapTypeMap();
                    MapTypeMapMapper.ToMapTypeMap(input: mapTypeMap, output: entity);
                    context.MapTypeMap.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public IEnumerable<MapTypeMapDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MapTypeMapDto>();
            foreach (var mapTypeMap in context.MapTypeMap)
            {
                var dto = new MapTypeMapDto();
                MapTypeMapMapper.ToMapTypeMapDto(input: mapTypeMap, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public MapTypeMapDto LoadByMapAndMapType(short mapId, short maptypeId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new MapTypeMapDto();
                if (MapTypeMapMapper.ToMapTypeMapDto(
                    input: context.MapTypeMap.FirstOrDefault(predicate: i =>
                        i.MapId.Equals(mapId) && i.MapTypeId.Equals(maptypeId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<MapTypeMapDto> LoadByMapId(short mapId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MapTypeMapDto>();
            foreach (var mapTypeMap in context.MapTypeMap.Where(predicate: c => c.MapId.Equals(mapId)))
            {
                var dto = new MapTypeMapDto();
                MapTypeMapMapper.ToMapTypeMapDto(input: mapTypeMap, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<MapTypeMapDto> LoadByMapTypeId(short maptypeId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MapTypeMapDto>();
            foreach (var mapTypeMap in context.MapTypeMap.Where(predicate: c => c.MapTypeId.Equals(maptypeId)))
            {
                var dto = new MapTypeMapDto();
                MapTypeMapMapper.ToMapTypeMapDto(input: mapTypeMap, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}