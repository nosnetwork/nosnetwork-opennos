﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class MapTypeDao : IMapTypeDao
    {
        #region Methods

        public MapTypeDto Insert(ref MapTypeDto mapType)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new MapType();
                MapTypeMapper.ToMapType(input: mapType, output: entity);
                context.MapType.Add(entity: entity);
                context.SaveChanges();
                if (MapTypeMapper.ToMapTypeDto(input: entity, output: mapType)) return mapType;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<MapTypeDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MapTypeDto>();
            foreach (var mapType in context.MapType)
            {
                var dto = new MapTypeDto();
                MapTypeMapper.ToMapTypeDto(input: mapType, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public MapTypeDto LoadById(short maptypeId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new MapTypeDto();
                if (MapTypeMapper.ToMapTypeDto(
                    input: context.MapType.FirstOrDefault(predicate: s => s.MapTypeId.Equals(maptypeId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        #endregion
    }
}