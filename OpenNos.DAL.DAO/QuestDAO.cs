﻿using System;
using System.Collections.Generic;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class QuestDao : IQuestDao
    {
        #region Methods

        public DeleteResult DeleteById(long id)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var deleteEntity = context.Quest.Find(id);
                if (deleteEntity != null)
                {
                    context.Quest.Remove(entity: deleteEntity);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_ERROR"), arg0: id,
                        arg1: e.Message), ex: e);
                return DeleteResult.Error;
            }
        }

        public QuestDto InsertOrUpdate(QuestDto quest)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = context.Quest.Find(quest.QuestId);

                if (entity == null) return Insert(quest: quest, context: context);
                return Update(entity: entity, quest: quest, context: context);
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSERT_ERROR"), arg0: quest,
                        arg1: e.Message), ex: e);
                return quest;
            }
        }

        public void Insert(List<QuestDto> questList)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var quest in questList)
                {
                    var entity = new Quest();
                    QuestMapper.ToQuest(input: quest, output: entity);
                    context.Quest.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public IEnumerable<QuestDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<QuestDto>();
            foreach (var entity in context.Quest)
            {
                var dto = new QuestDto();
                QuestMapper.ToQuestDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public QuestDto LoadById(long id)
        {
            using var context = DataAccessHelper.CreateContext();
            var dto = new QuestDto();
            if (QuestMapper.ToQuestDto(input: context.Quest.Find(id), output: dto)) return dto;

            return null;
        }

        static QuestDto Insert(QuestDto quest, OpenNosContext context)
        {
            var entity = new Quest();
            QuestMapper.ToQuest(input: quest, output: entity);
            context.Quest.Add(entity: entity);
            context.SaveChanges();
            if (QuestMapper.ToQuestDto(input: entity, output: quest)) return quest;

            return null;
        }

        static QuestDto Update(Quest entity, QuestDto quest, OpenNosContext context)
        {
            if (entity != null)
            {
                QuestMapper.ToQuest(input: quest, output: entity);
                context.SaveChanges();
            }

            if (QuestMapper.ToQuestDto(input: entity, output: quest)) return quest;

            return null;
        }

        #endregion
    }
}