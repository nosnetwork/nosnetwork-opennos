﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class FamilyCharacterDao : IFamilyCharacterDao
    {
        #region Methods

        public DeleteResult Delete(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var familyCharacter =
                    context.FamilyCharacter.FirstOrDefault(predicate: c => c.CharacterId == characterId);

                if (familyCharacter == null) return DeleteResult.NotFound;

                context.FamilyCharacter.Remove(entity: familyCharacter);
                context.SaveChanges();

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(
                        format: Language.Instance.GetMessageFromKey(key: "DELETE_FAMILYCHARACTER_ERROR"),
                        arg0: e.Message), ex: e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref FamilyCharacterDto character)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var familyCharacterId = character.FamilyCharacterId;
                var entity =
                    context.FamilyCharacter.FirstOrDefault(predicate: c =>
                        c.FamilyCharacterId.Equals(familyCharacterId));

                if (entity == null)
                {
                    character = Insert(character: character, context: context);
                    return SaveResult.Inserted;
                }

                character = Update(entity: entity, character: character, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSERT_ERROR"),
                        arg0: character, arg1: e.Message),
                    ex: e);
                return SaveResult.Error;
            }
        }

        public FamilyCharacterDto LoadByCharacterId(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new FamilyCharacterDto();
                if (FamilyCharacterMapper.ToFamilyCharacterDto(
                    input: context.FamilyCharacter.FirstOrDefault(predicate: c => c.CharacterId == characterId),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IList<FamilyCharacterDto> LoadByFamilyId(long familyId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<FamilyCharacterDto>();
            foreach (var entity in context.FamilyCharacter.Where(predicate: fc => fc.FamilyId.Equals(familyId)))
            {
                var dto = new FamilyCharacterDto();
                FamilyCharacterMapper.ToFamilyCharacterDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public FamilyCharacterDto LoadById(long familyCharacterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new FamilyCharacterDto();
                if (FamilyCharacterMapper.ToFamilyCharacterDto(
                    input: context.FamilyCharacter.FirstOrDefault(predicate: c =>
                        c.FamilyCharacterId.Equals(familyCharacterId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static FamilyCharacterDto Insert(FamilyCharacterDto character, OpenNosContext context)
        {
            var entity = new FamilyCharacter();
            FamilyCharacterMapper.ToFamilyCharacter(input: character, output: entity);
            context.FamilyCharacter.Add(entity: entity);
            context.SaveChanges();
            if (FamilyCharacterMapper.ToFamilyCharacterDto(input: entity, output: character)) return character;

            return null;
        }

        static FamilyCharacterDto Update(FamilyCharacter entity, FamilyCharacterDto character, OpenNosContext context)
        {
            if (entity != null)
            {
                FamilyCharacterMapper.ToFamilyCharacter(input: character, output: entity);
                context.SaveChanges();
            }

            if (FamilyCharacterMapper.ToFamilyCharacterDto(input: entity, output: character)) return character;

            return null;
        }

        #endregion
    }
}