﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following set of attributes.
// Change these attribute values to modify the information associated with an assembly.
[assembly: AssemblyTitle(title: "OpenNos.DAL.DAO")]
[assembly: AssemblyDescription(description: "")]
[assembly: AssemblyConfiguration(configuration: "")]
[assembly: AssemblyCompany(company: "")]
[assembly: AssemblyProduct(product: "OpenNos.DAL.DAO")]
[assembly: AssemblyCopyright(copyright: "Copyright ©  2017")]
[assembly: AssemblyTrademark(trademark: "")]
[assembly: AssemblyCulture(culture: "")]

// Setting ComVisible to false makes the types in this assembly not visible to COM components. If you
// need to access a type in this assembly from COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(visibility: false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid(guid: "b438a1df-0cfb-4854-82b5-2751793279bf")]

// Version information for an assembly consists of the following four values:
//
// Major Version Minor Version Build Number Revision
//
// You can specify all the values or you can default the Build and Revision Numbers by using the '*'
// as shown below: [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(version: "1.0.0.0")]
[assembly: AssemblyFileVersion(version: "1.0.0.0")]