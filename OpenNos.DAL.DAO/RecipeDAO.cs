﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class RecipeDao : IRecipeDao
    {
        #region Methods

        public RecipeDto Insert(RecipeDto recipe)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new Recipe();
                RecipeMapper.ToRecipe(input: recipe, output: entity);
                context.Recipe.Add(entity: entity);
                context.SaveChanges();
                if (RecipeMapper.ToRecipeDto(input: entity, output: recipe)) return recipe;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<RecipeDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RecipeDto>();
            foreach (var recipe in context.Recipe)
            {
                var dto = new RecipeDto();
                RecipeMapper.ToRecipeDto(input: recipe, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public RecipeDto LoadById(short recipeId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new RecipeDto();
                if (RecipeMapper.ToRecipeDto(
                    input: context.Recipe.SingleOrDefault(predicate: s => s.RecipeId.Equals(recipeId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public RecipeDto LoadByItemVNum(short itemVNum)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new RecipeDto();
                if (RecipeMapper.ToRecipeDto(
                    input: context.Recipe.SingleOrDefault(predicate: s => s.ItemVNum.Equals(itemVNum)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public void Update(RecipeDto recipe)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var result = context.Recipe.FirstOrDefault(predicate: c => c.ItemVNum == recipe.ItemVNum);
                if (result != null)
                {
                    recipe.RecipeId = result.RecipeId;
                    RecipeMapper.ToRecipe(input: recipe, output: result);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        #endregion
    }
}