﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class DropDao : IDropDao
    {
        #region Methods

        public void Insert(List<DropDto> drops)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var drop in drops)
                {
                    var entity = new Drop();
                    DropMapper.ToDrop(input: drop, output: entity);
                    context.Drop.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public DropDto Insert(DropDto drop)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new Drop();
                context.Drop.Add(entity: entity);
                context.SaveChanges();
                if (DropMapper.ToDropDto(input: entity, output: drop)) return drop;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public List<DropDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<DropDto>();
            foreach (var entity in context.Drop)
            {
                var dto = new DropDto();
                DropMapper.ToDropDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<DropDto> LoadByMonster(short monsterVNum)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<DropDto>();

            foreach (var drop in context.Drop.Where(predicate: s =>
                s.MonsterVNum == monsterVNum || s.MonsterVNum == null))
            {
                var dto = new DropDto();
                DropMapper.ToDropDto(input: drop, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}