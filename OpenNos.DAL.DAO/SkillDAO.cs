﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class SkillDao : ISkillDao
    {
        #region Methods

        public void Insert(List<SkillDto> skills)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var skill in skills) InsertOrUpdate(skill: skill);
                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public SkillDto Insert(SkillDto skill)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new Skill();
                SkillMapper.ToSkill(input: skill, output: entity);
                context.Skill.Add(entity: entity);
                context.SaveChanges();
                if (SkillMapper.ToSkillDto(input: entity, output: skill)) return skill;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<SkillDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<SkillDto>();
            foreach (var skill in context.Skill)
            {
                var dto = new SkillDto();
                SkillMapper.ToSkillDto(input: skill, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public SkillDto LoadById(short skillId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new SkillDto();
                if (SkillMapper.ToSkillDto(
                    input: context.Skill.FirstOrDefault(predicate: s => s.SkillVNum.Equals(skillId)), output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public SaveResult InsertOrUpdate(SkillDto skill)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                long skillVNum = skill.SkillVNum;
                var entity = context.Skill.FirstOrDefault(predicate: c => c.SkillVNum == skillVNum);

                if (entity == null)
                {
                    skill = Insert(skill: skill, context: context);
                    return SaveResult.Inserted;
                }

                skill = Update(entity: entity, skill: skill, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_SKILL_ERROR"),
                        arg0: skill.SkillVNum,
                        arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        static SkillDto Insert(SkillDto skill, OpenNosContext context)
        {
            var entity = new Skill();
            SkillMapper.ToSkill(input: skill, output: entity);
            context.Skill.Add(entity: entity);
            context.SaveChanges();
            if (SkillMapper.ToSkillDto(input: entity, output: skill)) return skill;

            return null;
        }

        static SkillDto Update(Skill entity, SkillDto skill, OpenNosContext context)
        {
            if (entity != null)
            {
                SkillMapper.ToSkill(input: skill, output: entity);
                context.SaveChanges();
            }

            if (SkillMapper.ToSkillDto(input: entity, output: skill)) return skill;

            return null;
        }

        #endregion
    }
}