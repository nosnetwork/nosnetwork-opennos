﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class ShopDao : IShopDao
    {
        #region Methods

        public DeleteResult DeleteByNpcId(int mapNpcId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var shop = context.Shop.First(predicate: i => i.MapNpcId.Equals(mapNpcId));
                IEnumerable<ShopItem> shopItem =
                    context.ShopItem.Where(predicate: s => s.ShopId.Equals(shop.ShopId));
                IEnumerable<ShopSkill> shopSkill =
                    context.ShopSkill.Where(predicate: s => s.ShopId.Equals(shop.ShopId));

                if (shop != null)
                {
                    foreach (var item in shopItem) context.ShopItem.Remove(entity: item);
                    foreach (var skill in shopSkill) context.ShopSkill.Remove(entity: skill);
                    context.Shop.Remove(entity: shop);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public void Insert(List<ShopDto> shops)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var item in shops)
                {
                    var entity = new Shop();
                    ShopMapper.ToShop(input: item, output: entity);
                    context.Shop.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public ShopDto Insert(ShopDto shop)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                if (context.Shop.FirstOrDefault(predicate: c => c.MapNpcId.Equals(shop.MapNpcId)) == null)
                {
                    var entity = new Shop();
                    ShopMapper.ToShop(input: shop, output: entity);
                    context.Shop.Add(entity: entity);
                    context.SaveChanges();
                    if (ShopMapper.ToShopDto(input: entity, output: shop)) return shop;

                    return null;
                }

                return new ShopDto();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        static ShopDto Update(Shop entity, ShopDto shop, OpenNosContext context)
        {
            if (entity != null)
            {
                ShopMapper.ToShop(input: shop, output: entity);
                context.Entry(entity: entity).State = EntityState.Modified;
                context.SaveChanges();
            }

            if (ShopMapper.ToShopDto(input: entity, output: shop)) return shop;

            return null;
        }

        public SaveResult Update(ref ShopDto shop)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var shopId = shop.ShopId;
                var entity = context.Shop.FirstOrDefault(predicate: c => c.ShopId.Equals(shopId));

                shop = Update(entity: entity, shop: shop, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_SHOP_ERROR"),
                        arg0: shop.ShopId, arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<ShopDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ShopDto>();
            foreach (var entity in context.Shop)
            {
                var dto = new ShopDto();
                ShopMapper.ToShopDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public ShopDto LoadById(int shopId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new ShopDto();
                if (ShopMapper.ToShopDto(
                    input: context.Shop.FirstOrDefault(predicate: s => s.ShopId.Equals(shopId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public ShopDto LoadByNpc(int mapNpcId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new ShopDto();
                if (ShopMapper.ToShopDto(
                    input: context.Shop.FirstOrDefault(predicate: s => s.MapNpcId.Equals(mapNpcId)), output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        #endregion
    }
}