﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class ShellEffectDao : IShellEffectDao
    {
        #region Methods

        public DeleteResult DeleteByEquipmentSerialId(Guid id)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var deleteentities = context.ShellEffect.Where(predicate: s => s.EquipmentSerialId == id).ToList();
                if (deleteentities.Count != 0)
                {
                    context.ShellEffect.RemoveRange(entities: deleteentities);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_ERROR"), arg0: id,
                        arg1: e.Message), ex: e);
                return DeleteResult.Error;
            }
        }

        public ShellEffectDto InsertOrUpdate(ShellEffectDto shelleffect)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var shelleffectId = shelleffect.ShellEffectId;
                var entity =
                    context.ShellEffect.FirstOrDefault(predicate: c => c.ShellEffectId.Equals(shelleffectId));

                if (entity == null) return Insert(shelleffect: shelleffect, context: context);
                return Update(entity: entity, shelleffect: shelleffect, context: context);
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSERT_ERROR"),
                        arg0: shelleffect, arg1: e.Message),
                    ex: e);
                return shelleffect;
            }
        }

        public void InsertOrUpdateFromList(List<ShellEffectDto> shellEffects, Guid equipmentSerialId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();

                void Insert(ShellEffectDto shelleffect)
                {
                    var entity = new ShellEffect();
                    ShellEffectMapper.ToShellEffect(input: shelleffect, output: entity);
                    context.ShellEffect.Add(entity: entity);
                    context.SaveChanges();
                    shelleffect.ShellEffectId = entity.ShellEffectId;
                }

                static void Update(ShellEffect entity, ShellEffectDto shelleffect)
                {
                    if (entity != null) ShellEffectMapper.ToShellEffect(input: shelleffect, output: entity);
                }

                foreach (var item in shellEffects)
                {
                    item.EquipmentSerialId = equipmentSerialId;
                    var entity =
                        context.ShellEffect.FirstOrDefault(predicate: c => c.ShellEffectId == item.ShellEffectId);

                    if (entity == null)
                        Insert(shelleffect: item);
                    else
                        Update(entity: entity, shelleffect: item);
                }

                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public IEnumerable<ShellEffectDto> LoadByEquipmentSerialId(Guid id)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<ShellEffectDto>();
            foreach (var entity in context.ShellEffect.Where(predicate: c => c.EquipmentSerialId == id))
            {
                var dto = new ShellEffectDto();
                ShellEffectMapper.ToShellEffectDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        static ShellEffectDto Insert(ShellEffectDto shelleffect, OpenNosContext context)
        {
            var entity = new ShellEffect();
            ShellEffectMapper.ToShellEffect(input: shelleffect, output: entity);
            context.ShellEffect.Add(entity: entity);
            context.SaveChanges();
            if (ShellEffectMapper.ToShellEffectDto(input: entity, output: shelleffect)) return shelleffect;

            return null;
        }

        static ShellEffectDto Update(ShellEffect entity, ShellEffectDto shelleffect, OpenNosContext context)
        {
            if (entity != null)
            {
                ShellEffectMapper.ToShellEffect(input: shelleffect, output: entity);
                context.SaveChanges();
            }

            if (ShellEffectMapper.ToShellEffectDto(input: entity, output: shelleffect)) return shelleffect;

            return null;
        }

        #endregion
    }
}