﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class QuestLogDao : IQuestLogDao
    {
        public SaveResult InsertOrUpdate(ref QuestLogDto quest)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var questId = quest.QuestId;
                var characterId = quest.CharacterId;
                var entity = context.QuestLog.FirstOrDefault(predicate: c =>
                    c.QuestId.Equals(questId) && c.CharacterId.Equals(characterId));

                if (entity == null)
                {
                    quest = Insert(questLog: quest, context: context);
                    return SaveResult.Inserted;
                }

                quest.QuestId = entity.QuestId;
                quest = Update(old: entity, replace: quest, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return SaveResult.Error;
            }
        }

        public QuestLogDto LoadById(long id)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new QuestLogDto();
                if (QuestLogMapper.ToQuestLogDto(
                    input: context.QuestLog.FirstOrDefault(predicate: i => i.Id.Equals(id)), output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<QuestLogDto> LoadByCharacterId(long characterId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<QuestLogDto>();
            foreach (var questLog in context.QuestLog.Where(predicate: s => s.CharacterId == characterId))
            {
                var dto = new QuestLogDto();
                QuestLogMapper.ToQuestLogDto(input: questLog, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public QuestLogDto Insert(QuestLogDto questLog, OpenNosContext context)
        {
            try
            {
                var entity = new QuestLog();
                QuestLogMapper.ToQuestLog(input: questLog, output: entity);
                context.QuestLog.Add(entity: entity);
                context.SaveChanges();
                if (QuestLogMapper.ToQuestLogDto(input: entity, output: questLog)) return questLog;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public QuestLogDto Update(QuestLog old, QuestLogDto replace, OpenNosContext context)
        {
            if (old != null)
            {
                QuestLogMapper.ToQuestLog(input: replace, output: old);
                context.Entry(entity: old).State = EntityState.Modified;
                context.SaveChanges();
            }

            if (QuestLogMapper.ToQuestLogDto(input: old, output: replace)) return replace;

            return null;
        }
    }
}