﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class MapDao : IMapDao
    {
        #region Methods

        public void Insert(List<MapDto> maps)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var item in maps)
                {
                    var entity = new Map();
                    MapMapper.ToMap(input: item, output: entity);
                    context.Map.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public MapDto Insert(MapDto map)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                if (context.Map.FirstOrDefault(predicate: c => c.MapId.Equals(map.MapId)) == null)
                {
                    var entity = new Map();
                    MapMapper.ToMap(input: map, output: entity);
                    context.Map.Add(entity: entity);
                    context.SaveChanges();
                    if (MapMapper.ToMapDto(input: entity, output: map)) return map;

                    return null;
                }

                return new MapDto();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<MapDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MapDto>();
            foreach (var map in context.Map)
            {
                var dto = new MapDto();
                MapMapper.ToMapDto(input: map, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public MapDto LoadById(short mapId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new MapDto();
                if (MapMapper.ToMapDto(input: context.Map.FirstOrDefault(predicate: c => c.MapId.Equals(mapId)),
                    output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        #endregion
    }
}