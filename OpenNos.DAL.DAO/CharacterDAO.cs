﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Domain;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class CharacterDao : ICharacterDao
    {
        #region Methods

        public DeleteResult DeleteByPrimaryKey(long accountId, byte characterSlot)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                // actually a Character wont be deleted, it just will be disabled for future traces
                var character = context.Character.SingleOrDefault(predicate: c =>
                    c.AccountId.Equals(accountId) && c.Slot.Equals(characterSlot) &&
                    c.State.Equals((byte)CharacterState.Active));

                if (character != null)
                {
                    character.State = (byte)CharacterState.Inactive;
                    character.Name = $"[DELETED]{character.Name}";
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_CHARACTER_ERROR"),
                        arg0: characterSlot,
                        arg1: e.Message), ex: e);
                return DeleteResult.Error;
            }
        }

        /// <summary>
        ///     Returns first 30 occurences of highest Compliment
        /// </summary>
        /// <returns></returns>
        public List<CharacterDto> GetTopCompliment()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CharacterDto>();
            foreach (var entity in context.Character
                .Where(predicate: c => c.State == (byte)CharacterState.Active &&
                                       c.Account.Authority == AuthorityType.User &&
                                       !c.Account.PenaltyLog.Any(l =>
                                           l.Penalty == PenaltyType.Banned && l.DateEnd > DateTime.Now))
                .OrderByDescending(keySelector: c => c.Compliment).Take(count: 30))
            {
                var dto = new CharacterDto();
                CharacterMapper.ToCharacterDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        /// <summary>
        ///     Returns first 30 occurences of highest Act4Points
        /// </summary>
        /// <returns></returns>
        public List<CharacterDto> GetTopPoints()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CharacterDto>();
            foreach (var entity in context.Character
                .Where(predicate: c => c.State == (byte)CharacterState.Active &&
                                       c.Account.Authority == AuthorityType.User &&
                                       !c.Account.PenaltyLog.Any(l =>
                                           l.Penalty == PenaltyType.Banned && l.DateEnd > DateTime.Now))
                .OrderByDescending(keySelector: c => c.Act4Points).Take(count: 30))
            {
                var dto = new CharacterDto();
                CharacterMapper.ToCharacterDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        /// <summary>
        ///     Returns first 30 occurences of highest Reputation
        /// </summary>
        /// <returns></returns>
        public List<CharacterDto> GetTopReputation()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CharacterDto>();
            foreach (var entity in context.Character
                .Where(predicate: c => c.State == (byte)CharacterState.Active &&
                                       c.Account.Authority == AuthorityType.User &&
                                       !c.Account.PenaltyLog.Any(l =>
                                           l.Penalty == PenaltyType.Banned && l.DateEnd > DateTime.Now))
                .OrderByDescending(keySelector: c => c.Reputation).Take(count: 43))
            {
                var dto = new CharacterDto();
                CharacterMapper.ToCharacterDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public SaveResult InsertOrUpdate(ref CharacterDto character)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var characterId = character.CharacterId;
                var entity = context.Character.FirstOrDefault(predicate: c => c.CharacterId.Equals(characterId));
                if (entity == null)
                {
                    character = Insert(character: character, context: context);
                    return SaveResult.Inserted;
                }

                character = Update(entity: entity, character: character, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "INSERT_ERROR"),
                        arg0: character, arg1: e.Message),
                    ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<CharacterDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CharacterDto>();
            foreach (var chara in context.Character)
            {
                var dto = new CharacterDto();
                CharacterMapper.ToCharacterDto(input: chara, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<CharacterDto> LoadAllByAccount(long accountId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CharacterDto>();
            foreach (var entity in context.Character.Where(predicate: c => c.AccountId.Equals(accountId))
                .OrderByDescending(keySelector: c => c.Slot))
            {
                var dto = new CharacterDto();
                CharacterMapper.ToCharacterDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<CharacterDto> LoadByAccount(long accountId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<CharacterDto>();
            foreach (var entity in context.Character
                .Where(predicate: c =>
                    c.AccountId.Equals(accountId) && c.State.Equals((byte)CharacterState.Active))
                .OrderByDescending(keySelector: c => c.Slot))
            {
                var dto = new CharacterDto();
                CharacterMapper.ToCharacterDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public CharacterDto LoadById(long characterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new CharacterDto();
                if (CharacterMapper.ToCharacterDto(
                    input: context.Character.FirstOrDefault(predicate: c => c.CharacterId.Equals(characterId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public CharacterDto LoadByName(string name)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new CharacterDto();
                if (CharacterMapper.ToCharacterDto(
                    input: context.Character.SingleOrDefault(predicate: c => c.Name.Equals(name)), output: dto))
                    return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }

            return null;
        }

        public CharacterDto LoadBySlot(long accountId, byte slot)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new CharacterDto();
                if (CharacterMapper.ToCharacterDto(
                    input: context.Character.SingleOrDefault(predicate: c =>
                        c.AccountId.Equals(accountId) && c.Slot.Equals(slot) &&
                        c.State.Equals((byte)CharacterState.Active)), output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(data: $"There should be only 1 character per slot, AccountId: {accountId} Slot: {slot}",
                    ex: e);
                return null;
            }
        }

        static CharacterDto Insert(CharacterDto character, OpenNosContext context)
        {
            var entity = new Character();
            CharacterMapper.ToCharacter(input: character, output: entity);
            context.Character.Add(entity: entity);
            context.SaveChanges();
            if (CharacterMapper.ToCharacterDto(input: entity, output: character)) return character;
            return null;
        }

        static CharacterDto Update(Character entity, CharacterDto character, OpenNosContext context)
        {
            if (entity != null)
            {
                // State Updates should only occur upon deleting character, so outside of this method.
                var state = entity.State;
                CharacterMapper.ToCharacter(input: character, output: entity);
                entity.State = state;

                context.SaveChanges();
            }

            if (CharacterMapper.ToCharacterDto(input: entity, output: character)) return character;

            return null;
        }

        #endregion
    }
}