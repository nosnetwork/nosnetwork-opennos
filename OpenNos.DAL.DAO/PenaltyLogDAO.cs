﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class PenaltyLogDao : IPenaltyLogDao
    {
        #region Methods

        public DeleteResult Delete(int penaltyLogId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var penaltyLog =
                    context.PenaltyLog.FirstOrDefault(predicate: c => c.PenaltyLogId.Equals(penaltyLogId));

                if (penaltyLog != null)
                {
                    context.PenaltyLog.Remove(entity: penaltyLog);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "DELETE_PENALTYLOG_ERROR"),
                        arg0: penaltyLogId,
                        arg1: e.Message), ex: e);
                return DeleteResult.Error;
            }
        }

        public SaveResult InsertOrUpdate(ref PenaltyLogDto log)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var id = log.PenaltyLogId;
                var entity = context.PenaltyLog.FirstOrDefault(predicate: c => c.PenaltyLogId.Equals(id));

                if (entity == null)
                {
                    log = Insert(penaltylog: log, context: context);
                    return SaveResult.Inserted;
                }

                log = Update(entity: entity, penaltylog: log, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_PENALTYLOG_ERROR"),
                        arg0: log.PenaltyLogId,
                        arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        public IEnumerable<PenaltyLogDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<PenaltyLogDto>();
            foreach (var entity in context.PenaltyLog)
            {
                var dto = new PenaltyLogDto();
                PenaltyLogMapper.ToPenaltyLogDto(input: entity, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<PenaltyLogDto> LoadByAccount(long accountId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<PenaltyLogDto>();
            foreach (var penaltyLog in context.PenaltyLog.Where(predicate: s => s.AccountId.Equals(accountId)))
            {
                var dto = new PenaltyLogDto();
                PenaltyLogMapper.ToPenaltyLogDto(input: penaltyLog, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public PenaltyLogDto LoadById(int penaltyLogId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new PenaltyLogDto();
                if (PenaltyLogMapper.ToPenaltyLogDto(
                    input: context.PenaltyLog.FirstOrDefault(predicate: s => s.PenaltyLogId.Equals(penaltyLogId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<PenaltyLogDto> LoadByIp(string ip)
        {
            using var context = DataAccessHelper.CreateContext();
            var cleanIp = ip.Replace(oldValue: "tcp://", newValue: "");
            cleanIp = cleanIp.Substring(startIndex: 0,
                length: cleanIp.LastIndexOf(value: ":", comparisonType: StringComparison.Ordinal) > 0
                    ? cleanIp.LastIndexOf(value: ":", comparisonType: StringComparison.Ordinal)
                    : cleanIp.Length);
            var result = new List<PenaltyLogDto>();
            foreach (var penaltyLog in context.PenaltyLog.Where(predicate: s => s.Ip.Contains(cleanIp)))
            {
                var dto = new PenaltyLogDto();
                PenaltyLogMapper.ToPenaltyLogDto(input: penaltyLog, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        static PenaltyLogDto Insert(PenaltyLogDto penaltylog, OpenNosContext context)
        {
            var entity = new PenaltyLog();
            PenaltyLogMapper.ToPenaltyLog(input: penaltylog, output: entity);
            context.PenaltyLog.Add(entity: entity);
            context.SaveChanges();
            if (PenaltyLogMapper.ToPenaltyLogDto(input: entity, output: penaltylog)) return penaltylog;

            return null;
        }

        static PenaltyLogDto Update(PenaltyLog entity, PenaltyLogDto penaltylog, OpenNosContext context)
        {
            if (entity != null)
            {
                PenaltyLogMapper.ToPenaltyLog(input: penaltylog, output: entity);
                context.SaveChanges();
            }

            if (PenaltyLogMapper.ToPenaltyLogDto(input: entity, output: penaltylog)) return penaltylog;

            return null;
        }

        #endregion
    }
}