﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Context;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class MapMonsterDao : IMapMonsterDao
    {
        #region Methods

        public DeleteResult DeleteById(int mapMonsterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var monster = context.MapMonster.First(predicate: i => i.MapMonsterId.Equals(mapMonsterId));

                if (monster != null)
                {
                    context.MapMonster.Remove(entity: monster);
                    context.SaveChanges();
                }

                return DeleteResult.Deleted;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return DeleteResult.Error;
            }
        }

        public bool DoesMonsterExist(int mapMonsterId)
        {
            using var context = DataAccessHelper.CreateContext();
            return context.MapMonster.Any(predicate: i => i.MapMonsterId.Equals(mapMonsterId));
        }

        public void Insert(IEnumerable<MapMonsterDto> mapMonsters)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                context.Configuration.AutoDetectChangesEnabled = false;
                foreach (var monster in mapMonsters)
                {
                    var entity = new MapMonster();
                    MapMonsterMapper.ToMapMonster(input: monster, output: entity);
                    context.MapMonster.Add(entity: entity);
                }

                context.Configuration.AutoDetectChangesEnabled = true;
                context.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        public MapMonsterDto Insert(MapMonsterDto mapMonster)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new MapMonster();
                MapMonsterMapper.ToMapMonster(input: mapMonster, output: entity);
                context.MapMonster.Add(entity: entity);
                context.SaveChanges();
                if (MapMonsterMapper.ToMapMonsterDto(input: entity, output: mapMonster)) return mapMonster;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public SaveResult Update(ref MapMonsterDto mapMonster)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var mapMonsterId = mapMonster.MapMonsterId;
                var entity = context.MapMonster.FirstOrDefault(predicate: c => c.MapMonsterId.Equals(mapMonsterId));

                mapMonster = Update(entity: entity, mapMonster: mapMonster, context: context);
                return SaveResult.Updated;
            }
            catch (Exception e)
            {
                Logger.Error(
                    data: string.Format(format: Language.Instance.GetMessageFromKey(key: "UPDATE_MAPMONSTER_ERROR"),
                        arg0: mapMonster.MapMonsterId, arg1: e.Message), ex: e);
                return SaveResult.Error;
            }
        }

        static MapMonsterDto Update(MapMonster entity, MapMonsterDto mapMonster, OpenNosContext context)
        {
            if (entity != null)
            {
                MapMonsterMapper.ToMapMonster(input: mapMonster, output: entity);
                context.Entry(entity: entity).State = EntityState.Modified;
                context.SaveChanges();
            }

            if (MapMonsterMapper.ToMapMonsterDto(input: entity, output: mapMonster)) return mapMonster;

            return null;
        }

        public MapMonsterDto LoadById(int mapMonsterId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new MapMonsterDto();
                if (MapMonsterMapper.ToMapMonsterDto(
                    input: context.MapMonster.FirstOrDefault(predicate: i => i.MapMonsterId.Equals(mapMonsterId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<MapMonsterDto> LoadFromMap(short mapId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<MapMonsterDto>();
            foreach (var mapMonsterobject in context.MapMonster.Where(predicate: c => c.MapId.Equals(mapId)))
            {
                var dto = new MapMonsterDto();
                MapMonsterMapper.ToMapMonsterDto(input: mapMonsterobject, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        #endregion
    }
}