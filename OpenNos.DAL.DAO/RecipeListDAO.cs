﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Mapper.Mappers;

namespace OpenNos.DAL.DAO
{
    public class RecipeListDao : IRecipeListDao
    {
        #region Methods

        public RecipeListDto Insert(RecipeListDto recipeList)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var entity = new RecipeList();
                RecipeListMapper.ToRecipeList(input: recipeList, output: entity);
                context.RecipeList.Add(entity: entity);
                context.SaveChanges();
                if (RecipeListMapper.ToRecipeListDto(input: entity, output: recipeList)) return recipeList;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<RecipeListDto> LoadAll()
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RecipeListDto>();
            foreach (var recipeList in context.RecipeList)
            {
                var dto = new RecipeListDto();
                RecipeListMapper.ToRecipeListDto(input: recipeList, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public RecipeListDto LoadById(int recipeListId)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var dto = new RecipeListDto();
                if (RecipeListMapper.ToRecipeListDto(
                    input: context.RecipeList.SingleOrDefault(predicate: s => s.RecipeListId.Equals(recipeListId)),
                    output: dto)) return dto;

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
                return null;
            }
        }

        public IEnumerable<RecipeListDto> LoadByItemVNum(short itemVNum)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RecipeListDto>();
            foreach (var recipeList in context.RecipeList.Where(predicate: r => r.ItemVNum == itemVNum))
            {
                var dto = new RecipeListDto();
                RecipeListMapper.ToRecipeListDto(input: recipeList, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<RecipeListDto> LoadByMapNpcId(int mapNpcId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RecipeListDto>();
            foreach (var recipeList in context.RecipeList.Where(predicate: r => r.MapNpcId == mapNpcId))
            {
                var dto = new RecipeListDto();
                RecipeListMapper.ToRecipeListDto(input: recipeList, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public IEnumerable<RecipeListDto> LoadByRecipeId(short recipeId)
        {
            using var context = DataAccessHelper.CreateContext();
            var result = new List<RecipeListDto>();
            foreach (var recipeList in context.RecipeList.Where(predicate: r => r.RecipeId.Equals(recipeId)))
            {
                var dto = new RecipeListDto();
                RecipeListMapper.ToRecipeListDto(input: recipeList, output: dto);
                result.Add(item: dto);
            }

            return result;
        }

        public void Update(RecipeListDto recipe)
        {
            try
            {
                using var context = DataAccessHelper.CreateContext();
                var result =
                    context.RecipeList.FirstOrDefault(predicate: r => r.RecipeListId.Equals(recipe.RecipeListId));
                if (result != null)
                {
                    RecipeListMapper.ToRecipeList(input: recipe, output: result);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Logger.Error(ex: e);
            }
        }

        #endregion
    }
}