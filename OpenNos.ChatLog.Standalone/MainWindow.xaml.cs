﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using OpenNos.Log.Shared;

namespace OpenNos.Log.Standalone
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region Instantiation

        public MainWindow()
        {
            InitializeComponent();
            _logs = new List<LogEntry>();
            _reader = new LogFileReader();
            RecursiveFileOpen(dir: "chatlogs");
            _logs = _logs.OrderBy(keySelector: s => s.Timestamp).ToList();
            Resultlistbox.ItemsSource = _logs;
            Typedropdown.Items.Add(newItem: "All");
            Typedropdown.Items.Add(newItem: LogType.Map);
            Typedropdown.Items.Add(newItem: LogType.Speaker);
            Typedropdown.Items.Add(newItem: LogType.Whisper);
            Typedropdown.Items.Add(newItem: LogType.Group);
            Typedropdown.Items.Add(newItem: LogType.Family);
            Typedropdown.Items.Add(newItem: LogType.BuddyTalk);
            Typedropdown.SelectedIndex = 0;

            var rmbMenu = new ContextMenu();

            var copySender = new MenuItem
            {
                Header = "Copy Sender"
            };
            copySender.Click += CopySenderOnClick;
            rmbMenu.Items.Add(newItem: copySender);

            var copySenderId = new MenuItem
            {
                Header = "Copy SenderId"
            };
            copySenderId.Click += CopySenderIdOnClick;
            rmbMenu.Items.Add(newItem: copySenderId);

            var copyReceiver = new MenuItem
            {
                Header = "Copy Receiver"
            };
            copyReceiver.Click += CopyReceiverOnClick;
            rmbMenu.Items.Add(newItem: copyReceiver);

            var copyReceiverId = new MenuItem
            {
                Header = "Copy ReceiverId"
            };
            copyReceiverId.Click += CopyReceiverIdOnClick;
            rmbMenu.Items.Add(newItem: copyReceiverId);

            var copyMessage = new MenuItem
            {
                Header = "Copy Message"
            };
            copyMessage.Click += CopyMessageOnClick;
            rmbMenu.Items.Add(newItem: copyMessage);

            var copyLogEntry = new MenuItem
            {
                Header = "Copy LogEntry"
            };
            copyLogEntry.Click += CopyLogEntryOnClick;
            rmbMenu.Items.Add(newItem: copyLogEntry);

            var searchBidirectionally = new MenuItem
            {
                Header = "Search Bidirectionally"
            };
            searchBidirectionally.Click += SearchBidirectionallyOnClick;
            rmbMenu.Items.Add(newItem: searchBidirectionally);

            Resultlistbox.ContextMenu = rmbMenu;
        }

        #endregion

        #region Members

        readonly List<LogEntry> _logs;
        readonly LogFileReader _reader;
        bool _orderDesc;

        #endregion

        #region Methods

        void CloseFile(object sender, RoutedEventArgs e)
        {
        }

        void CopyLogEntryOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (Resultlistbox.SelectedItem is LogEntry entry) Clipboard.SetText(text: entry.ToString());
        }

        void CopyMessageOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (Resultlistbox.SelectedItem is LogEntry entry) Clipboard.SetText(text: entry.Message);
        }

        void CopyReceiverIdOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (Resultlistbox.SelectedItem is LogEntry entry) Clipboard.SetText(text: entry.ReceiverId.ToString());
        }

        void CopyReceiverOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (Resultlistbox.SelectedItem is LogEntry entry) Clipboard.SetText(text: entry.Receiver);
        }

        void CopySenderIdOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (Resultlistbox.SelectedItem is LogEntry entry) Clipboard.SetText(text: entry.SenderId.ToString());
        }

        void CopySenderOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (Resultlistbox.SelectedItem is LogEntry entry) Clipboard.SetText(text: entry.Sender);
        }

        void OpenFile(object sender, RoutedEventArgs e)
        {
        }

        void RecursiveFileOpen(string dir)
        {
            try
            {
                foreach (var d in Directory.GetDirectories(path: dir))
                {
                    foreach (var s in Directory.GetFiles(path: d).Where(predicate: s => s.EndsWith(value: ".onc")))
                        _logs.AddRange(collection: _reader.ReadChatLogFile(path: s));

                    RecursiveFileOpen(dir: d);
                }
            }
            catch
            {
                MessageBox.Show(messageBoxText: "Something went wrong while opening Chat Log Files. Exiting...",
                    caption: "Error",
                    button: MessageBoxButton.OK, icon: MessageBoxImage.Error);
                Environment.Exit(exitCode: -1);
            }
        }

        //PacketLog not implemented on ChatLog.Standalone

        void SearchBidirectionallyOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (Resultlistbox.SelectedItem is LogEntry entry)
            {
                if (entry.MessageType == LogType.Whisper || entry.MessageType == LogType.BuddyTalk)
                {
                    var tmp = _logs;
                    tmp = tmp.Where(predicate: s =>
                        s.MessageType == entry.MessageType
                        && (s.Sender == entry.Sender && s.Receiver == entry.Receiver
                            || s.Sender == entry.Receiver && s.Receiver == entry.Sender)).ToList();

                    if (_orderDesc)
                        Resultlistbox.ItemsSource = tmp.OrderByDescending(keySelector: s => s.Timestamp);
                    else
                        Resultlistbox.ItemsSource = tmp.OrderBy(keySelector: s => s.Timestamp);
                }
                else
                {
                    MessageBox.Show(
                        messageBoxText: "You can ony search Bidirectionally for Whisper and BuddyTalk messages",
                        caption: "",
                        button: MessageBoxButton.OK, icon: MessageBoxImage.Information);
                }
            }
        }

        void SearchButton(object sender, RoutedEventArgs e)
        {
            var tmp = _logs;
            if (!string.IsNullOrWhiteSpace(value: Senderbox.Text))
                tmp = tmp.Where(predicate: s =>
                        s.Sender.IndexOf(value: Senderbox.Text,
                            comparisonType: StringComparison.CurrentCultureIgnoreCase) >= 0)
                    .ToList();

            if (!string.IsNullOrWhiteSpace(value: Senderidbox.Text) &&
                long.TryParse(s: Senderidbox.Text, result: out var senderid))
                tmp = tmp.Where(predicate: s => s.SenderId == senderid).ToList();

            if (!string.IsNullOrWhiteSpace(value: Receiverbox.Text))
                tmp = tmp.Where(predicate: s =>
                    s.Receiver.IndexOf(value: Receiverbox.Text,
                        comparisonType: StringComparison.CurrentCultureIgnoreCase) >= 0).ToList();

            if (!string.IsNullOrWhiteSpace(value: Receiveridbox.Text)
                && long.TryParse(s: Receiveridbox.Text, result: out var receiverid))
                tmp = tmp.Where(predicate: s => s.ReceiverId == receiverid).ToList();

            if (!string.IsNullOrWhiteSpace(value: Messagebox.Text))
                tmp = tmp.Where(predicate: s =>
                        s.Message.IndexOf(value: Messagebox.Text,
                            comparisonType: StringComparison.CurrentCultureIgnoreCase) >= 0)
                    .ToList();

            if (Datestartpicker.Value != null)
                tmp = tmp.Where(predicate: s => s.Timestamp >= Datestartpicker.Value).ToList();

            if (Dateendpicker.Value != null)
                tmp = tmp.Where(predicate: s => s.Timestamp <= Dateendpicker.Value).ToList();

            if (Typedropdown.SelectedIndex != 0)
                tmp = tmp.Where(predicate: s => s.MessageType == (LogType)Typedropdown.SelectedValue).ToList();

            if (_orderDesc)
                Resultlistbox.ItemsSource = tmp.OrderByDescending(keySelector: s => s.Timestamp);
            else
                Resultlistbox.ItemsSource = tmp.OrderBy(keySelector: s => s.Timestamp);
        }

        void SettingsOrderAscending(object sender, RoutedEventArgs e)
        {
            _orderDesc = false;
            Orderdesc.IsChecked = true;
            Orderasc.IsChecked = false;
        }

        void SettingsOrderDescending(object sender, RoutedEventArgs e)
        {
            _orderDesc = true;
            Orderdesc.IsChecked = true;
            Orderasc.IsChecked = false;
        }

        #endregion
    }
}