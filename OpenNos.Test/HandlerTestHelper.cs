﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using log4net;
using NUnit.Framework;
using OpenNos.Core;
using OpenNos.Core.Serializing;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Mock;
using OpenNos.GameObject.Networking;
using OpenNos.Handler;
using OpenNos.Master.Library.Client;

namespace OpenNos.Test
{
    public static class HandlerTestHelper
    {
        #region Members

        static SessionManager _sessionManager;

        #endregion

        #region Methods

        public static FakeNetworkClient CreateFakeNetworkClient()
        {
            var client = new FakeNetworkClient();
            _sessionManager.AddSession(customClient: client);

            long id = ServerManager.RandomNumber(min: 0, max: 999999);
            var account = new AccountDto
            {
                AccountId = id,
                Authority = AuthorityType.Gm,
                Name = "test" + id,
                Password =
                    "ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff"
            };
            DaoFactory.AccountDao.InsertOrUpdate(account: ref account);

            // register for account login
            CommunicationServiceClient.Instance.RegisterAccountLogin(accountId: account.AccountId, sessionId: 12345,
                ipAddress: "127.0.0.1");

            // OpenNosEntryPoint -> LoadCharacterList
            client.ReceivePacket(packet: "12345");
            client.ReceivePacket(packet: account.Name);
            client.ReceivePacket(packet: "test");

            WaitForPacket(client: client);

            WaitForPacket(client: client);

            // creation of character
            client.ReceivePacket(packet: $"Char_NEW {account.Name} 2 1 0 9");

            var clistAfterCreate = WaitForPackets(client: client, amount: 3);
            var cListPacket = PacketFactory.Deserialize<CListPacket>(packetContent: clistAfterCreate[index: 1]);

            // select character
            client.ReceivePacket(packet: $"select {cListPacket.Slot}");
            WaitForPacket(client: client);

            // start game
            client.ReceivePacket(packet: "game_start");
            WaitForPackets(client: client, lastPacketHeader: "p_clear");
            WaitForPackets(client: client, lastPacketHeader: "p_clear");

            // wait 100 milliseconds to be sure initialization has been finished
            Thread.Sleep(millisecondsTimeout: 100);

            return client;
        }

        public static FakeNetworkClient InitializeTestEnvironment()
        {
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo(name: "en-US");

            // initialize Logger
            Logger.InitializeLogger(log: LogManager.GetLogger(type: typeof(BasicPacketHandlerTest)));

            // register mappings for items
            RegisterMappings();

            // create server entities (this values would have been imported)
            CreateServerItems();
            CreateServerMaps();
            CreateServerSkills();

            // initialize servermanager
            ServerManager.Instance.Initialize();

            // initialize PacketSerialization
            PacketFactory.Initialize<WalkPacket>();

            // initialize new manager
            _sessionManager = new NetworkManager<TestEncryption>(ipAddress: "127.0.0.1", port: 1234,
                packetHandler: typeof(CharacterScreenPacketHandler), fallbackEncryptor: typeof(TestEncryption),
                isWorldServer: true);

            return CreateFakeNetworkClient();
        }

        public static void ShutdownTestingEnvironment()
        {
            _sessionManager.StopServer();
        }

        public static string WaitForPacket(FakeNetworkClient client)
        {
            var startTime = DateTime.Now;

            while (true)
            {
                if (client.SentPackets.Count > 0)
                {
                    var packet = client.SentPackets.Dequeue();
                    Debug.WriteLine(message: $"Dequeued {packet}");
                    return packet;
                }

                // exit token
                if (startTime.AddSeconds(value: 10) < DateTime.Now)
                {
                    Assert.Fail(message: "Timed out while waiting for a Packet.");
                    return "";
                }
            }
        }

        public static string WaitForPacket(FakeNetworkClient client, string packetHeader)
        {
            var startTime = DateTime.Now;

            while (true)
            {
                if (client.SentPackets.Count > 0)
                {
                    var packet = client.SentPackets.Dequeue();
                    Debug.WriteLine(message: $"Dequeued {packet}");
                    if (packet?.StartsWith(value: packetHeader) == true) return packet;
                }

                // exit token
                if (startTime.AddSeconds(value: 10) < DateTime.Now)
                {
                    Assert.Fail(message: $"Timed out while waiting for {packetHeader}");
                    return "";
                }
            }
        }

        public static List<string> WaitForPackets(FakeNetworkClient client, int amount)
        {
            var startTime = DateTime.Now;

            var receivedPackets = 0;
            var packets = new List<string>();
            while (receivedPackets < amount)
            {
                if (client.SentPackets.Count > 0)
                {
                    packets.Add(item: client.SentPackets.Dequeue());
                    receivedPackets++;
                }

                // exit token
                if (startTime.AddSeconds(value: 10) < DateTime.Now)
                {
                    Assert.Fail(message: $"Timed out while waiting for {amount} Packets");
                    return new List<string>();
                }
            }

            return packets;
        }

        public static List<string> WaitForPackets(FakeNetworkClient client, string lastPacketHeader, int timeout = 5)
        {
            var startTime = DateTime.Now;
            var packets = new List<string>();

            while (true)
            {
                if (client.SentPackets.Count > 0)
                {
                    var packet = client.SentPackets.Dequeue();
                    if (packet != null)
                    {
                        packets.Add(item: packet);
                        if (packet.StartsWith(value: lastPacketHeader, comparisonType: StringComparison.CurrentCulture))
                            return packets;
                    }
                }

                if (startTime.AddSeconds(value: timeout) <= DateTime.Now) return packets; // timing out
            }
        }

        static void CreateServerItems()
        {
            DaoFactory.ItemDao.Insert(item: new ItemDto
            {
                VNum = 1,
                Class = 1,
                CriticalLuckRate = 4,
                CriticalRate = 70,
                DamageMaximum = 28,
                DamageMinimum = 20,
                HitRate = 20,
                IsDroppable = true,
                IsSoldable = true,
                IsTradable = true,
                LevelMinimum = 1,
                MaximumAmmo = 100,
                Name = "Wooden Stick",
                Price = 70
            });
            DaoFactory.ItemDao.Insert(item: new ItemDto
            {
                VNum = 8,
                EquipmentSlot = EquipmentType.SecondaryWeapon
            });
            DaoFactory.ItemDao.Insert(item: new ItemDto
            {
                VNum = 12,
                EquipmentSlot = EquipmentType.Armor
            });
        }

        static void CreateServerMaps()
        {
            var testingMap = new MapDto
            {
                MapId = 1,
                Music = 1,
                Name = "Testing-Map",
                ShopAllowed = true
            };
            var mapData = new List<byte>
            {
                255, // x length
                0, // x length
                255, // y length
                0 // y length
            };

            // create map grid
            for (var i = 0; i < 255; i++)
                for (var j = 0; j < 255; j++)
                    // we can go everywhere
                    mapData.Add(item: 0);
            testingMap.Data = mapData.ToArray();
            DaoFactory.MapDao.Insert(map: testingMap);
        }

        static void CreateServerSkills()
        {
            DaoFactory.SkillDao.Insert(skill: new SkillDto
            {
                SkillVNum = 200,
                CastId = 0
            });
            DaoFactory.SkillDao.Insert(skill: new SkillDto
            {
                SkillVNum = 201,
                CastId = 1
            });
            DaoFactory.SkillDao.Insert(skill: new SkillDto
            {
                SkillVNum = 209,
                CastId = 2
            });
        }

        static void RegisterMappings()
        {
            // entities
            //DAOFactory.AccountDAO.RegisterMapping(typeof(Account)).InitializeMapper();
            //DAOFactory.CellonOptionDAO.RegisterMapping(typeof(CellonOptionDTO)).InitializeMapper();
            //DAOFactory.CharacterDAO.RegisterMapping(typeof(Character)).InitializeMapper();
            //DAOFactory.ComboDAO.RegisterMapping(typeof(ComboDTO)).InitializeMapper();
            //DAOFactory.DropDAO.RegisterMapping(typeof(DropDTO)).InitializeMapper();
            //DAOFactory.GeneralLogDAO.RegisterMapping(typeof(GeneralLogDTO)).InitializeMapper();
            //DAOFactory.ItemDAO.RegisterMapping(typeof(ItemDTO)).InitializeMapper();
            //DAOFactory.MailDAO.RegisterMapping(typeof(MailDTO)).InitializeMapper();
            //DAOFactory.MapDAO.RegisterMapping(typeof(MapDTO)).InitializeMapper();
            //DAOFactory.MapMonsterDAO.RegisterMapping(typeof(MapMonster)).InitializeMapper();
            //DAOFactory.MapNpcDAO.RegisterMapping(typeof(MapNpc)).InitializeMapper();
            //DAOFactory.MapTypeDAO.RegisterMapping(typeof(MapTypeDTO)).InitializeMapper();
            //DAOFactory.MapTypeMapDAO.RegisterMapping(typeof(MapTypeMapDTO)).InitializeMapper();
            //DAOFactory.NpcMonsterDAO.RegisterMapping(typeof(NpcMonster)).InitializeMapper();
            //DAOFactory.NpcMonsterSkillDAO.RegisterMapping(typeof(NpcMonsterSkill)).InitializeMapper();
            //DAOFactory.PenaltyLogDAO.RegisterMapping(typeof(PenaltyLogDTO)).InitializeMapper();
            //DAOFactory.PortalDAO.RegisterMapping(typeof(PortalDTO)).InitializeMapper();
            //DAOFactory.RecipeDAO.RegisterMapping(typeof(Recipe)).InitializeMapper();
            //DAOFactory.RecipeItemDAO.RegisterMapping(typeof(RecipeItemDTO)).InitializeMapper();
            //DAOFactory.RespawnDAO.RegisterMapping(typeof(RespawnDTO)).InitializeMapper();
            //DAOFactory.ShopDAO.RegisterMapping(typeof(Shop)).InitializeMapper();
            //DAOFactory.ShopItemDAO.RegisterMapping(typeof(ShopItemDTO)).InitializeMapper();
            //DAOFactory.ShopSkillDAO.RegisterMapping(typeof(ShopSkillDTO)).InitializeMapper();
            //DAOFactory.SkillDAO.RegisterMapping(typeof(Skill)).InitializeMapper();
            //DAOFactory.TeleporterDAO.RegisterMapping(typeof(TeleporterDTO)).InitializeMapper();
        }

        #endregion
    }
}