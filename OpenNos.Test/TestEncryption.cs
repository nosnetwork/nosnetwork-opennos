﻿using System.Text;
using OpenNos.Core.Cryptography;

namespace OpenNos.Test
{
    public class TestEncryption : CryptographyBase
    {
        #region Instantiation

        public TestEncryption() : base(hasCustomParameter: true)
        {
        }

        public TestEncryption(bool hasCustomParameter) : base(hasCustomParameter: hasCustomParameter)
        {
        }

        #endregion

        #region Methods

        public override string Decrypt(byte[] data, int sessionId = 0)
        {
            var encoding = new UTF8Encoding();
            return encoding.GetString(bytes: data);
        }

        public override string DecryptCustomParameter(byte[] data)
        {
            var encoding = new UTF8Encoding();
            return encoding.GetString(bytes: data);
        }

        public override byte[] Encrypt(string data)
        {
            var encoding = new UTF8Encoding();
            return encoding.GetBytes(s: data);
        }

        #endregion
    }
}