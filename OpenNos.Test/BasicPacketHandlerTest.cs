﻿using System.Threading;
using NUnit.Framework;
using OpenNos.Core.Serializing;
using OpenNos.Domain;
using OpenNos.GameObject;
using OpenNos.GameObject.Packets.ServerPackets;

namespace OpenNos.Test
{
    [TestFixture]
    public class BasicPacketHandlerTest
    {
        // [Test]
        public void InitializeTestEnvironmentTest()
        {
            // login, create character, start game
            HandlerTestHelper.InitializeTestEnvironment();
            HandlerTestHelper.ShutdownTestingEnvironment();
            Assert.Pass();
        }

        [Test]
        [MaxTime(milliseconds: 10000)]
        public void GroupTest()
        {
            // login, create character, start game
            var clientA = HandlerTestHelper.InitializeTestEnvironment();
            var clientB = HandlerTestHelper.CreateFakeNetworkClient();

            Thread.Sleep(millisecondsTimeout: 1000);

            // client A asks client B for group
            var pjoinPacketRequest = new PJoinPacket
            {
                CharacterId = clientB.Session.Character.CharacterId,
                RequestType = GroupRequestType.Invited
            };

            clientA.ReceivePacket(packet: pjoinPacketRequest);
            HandlerTestHelper.WaitForPackets(client: clientA, amount: 1);

            // client B accepts group request
            var pjoinPacketAccept = new PJoinPacket
            {
                CharacterId = clientA.Session.Character.CharacterId,
                RequestType = GroupRequestType.Accepted
            };

            clientB.ReceivePacket(packet: pjoinPacketAccept);
            HandlerTestHelper.WaitForPackets(client: clientA, amount: 1);

            // check if group has been created successfully
            Assert.IsNotNull(anObject: clientA.Session.Character.Group);
            Assert.IsNotNull(anObject: clientB.Session.Character.Group);
            Assert.AreEqual(expected: 2, actual: clientA.Session.Character.Group.SessionCount);
        }

        [Test]
        [MaxTime(milliseconds: 10000)]
        [RequiresThread(apartment: ApartmentState.STA)]
        public void TestCharacterOption()
        {
            // login, create character, start game
            var client = HandlerTestHelper.InitializeTestEnvironment();

            var optionPacket = new CharacterOptionPacket
            { IsActive = false, Option = CharacterOption.FamilyRequestBlocked };

            // check family request
            client.ReceivePacket(packet: optionPacket);
            HandlerTestHelper.WaitForPacket(client: client, packetHeader: "msg");
            Assert.IsTrue(condition: client.Session.Character.FamilyRequestBlocked);

            HandlerTestHelper.ShutdownTestingEnvironment();
            Assert.Pass();
        }

        [Test]
        [MaxTime(milliseconds: 10000)]
        [RequiresThread(apartment: ApartmentState.STA)]
        public void TestWalkMove()
        {
            // login, create character, start game
            var client = HandlerTestHelper.InitializeTestEnvironment();

            var walkPacket = new WalkPacket { Speed = 11, XCoordinate = 89, YCoordinate = 126 };

            // send walkpacket to client
            client.ReceivePacket(packet: walkPacket);

            var mvPacket = HandlerTestHelper.WaitForPacket(client: client, packetHeader: "mv");
            var movePacket = PacketFactory.Deserialize<MovePacket>(packetContent: mvPacket);

            Assert.AreEqual(expected: walkPacket.XCoordinate, actual: movePacket.PositionX);
            Assert.AreEqual(expected: walkPacket.YCoordinate, actual: movePacket.PositionY);
            Assert.AreEqual(expected: walkPacket.Speed, actual: movePacket.Speed);

            HandlerTestHelper.ShutdownTestingEnvironment();
            Assert.Pass();
        }
    }
}