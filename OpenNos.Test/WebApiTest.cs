﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenNos.Master.Library.Client;

namespace OpenNos.Test
{
    [TestClass]
    public class WebApiTest
    {
        #region Methods

        [TestMethod]
        public async Task TestParelellConnectionsAsync()
        {
            CommunicationServiceClient.Instance.Cleanup();

            foreach (var x in Enumerable.Range(start: 1, count: 50000))
                await Task.Factory.StartNew(action: () =>
                {
                    CommunicationServiceClient.Instance.RegisterAccountLogin(accountId: x, sessionId: x,
                        ipAddress: "127.0.0.1");
                    var hasRegisteredAccountLogin =
                        CommunicationServiceClient.Instance.IsLoginPermitted(accountId: x, sessionId: x);
                    Assert.IsTrue(condition: hasRegisteredAccountLogin);
                }).ConfigureAwait(continueOnCapturedContext: false);
        }

        #endregion
    }
}