﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.Core.ConcurrencyExtensions
{
    public static class ConcurrentBagExtensions
    {
        #region Methods

        public static void AddRange<T>(this ConcurrentBag<T> bag, List<T> list)
        {
            foreach (var item in list) bag.Add(item);
        }

        /*public static void Clear<T>(this ConcurrentBag<T> bag)
            {
                while (bag.Count > 0)
                {
                    bag.TryTake(out T item);
                }
            }*/

        public static ConcurrentBag<T> Where<T>(this ConcurrentBag<T> bag, Func<T, bool> predicate)
        {
            var newBag = new ConcurrentBag<T>();
            foreach (var item in bag.ToList().Where(predicate)) newBag.Add(item);
            return newBag;
        }

        #endregion
    }
}