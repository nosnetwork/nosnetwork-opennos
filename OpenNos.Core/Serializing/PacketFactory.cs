﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace OpenNos.Core.Serializing
{
    public static class PacketFactory
    {
        #region Members

        static Dictionary<Tuple<Type, string>, Dictionary<PacketIndexAttribute, PropertyInfo>>
            _packetSerializationInformations;

        #endregion

        #region Properties

        public static bool IsInitialized { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Deserializes a string into a PacketDefinition
        /// </summary>
        /// <param name="packetContent">The content to deseralize</param>
        /// <param name="packetType">The type of the packet to deserialize to</param>
        /// <param name="includesKeepAliveIdentity">
        ///     Include the keep alive identity or exclude it
        /// </param>
        /// <returns>The deserialized packet.</returns>
        public static PacketDefinition Deserialize(string packetContent, Type packetType,
            bool includesKeepAliveIdentity = false)
        {
            try
            {
                var serializationInformation = GetSerializationInformation(serializationType: packetType);
                var deserializedPacket = (PacketDefinition)Activator.CreateInstance(type: packetType);
                SetDeserializationInformations(packetDefinition: deserializedPacket, packetContent: packetContent,
                    packetHeader: serializationInformation.Key.Item2);
                return Deserialize(packetContent: packetContent, deserializedPacket: deserializedPacket,
                    serializationInformation: serializationInformation,
                    includesKeepAliveIdentity: includesKeepAliveIdentity);
            }
            catch (Exception ex)
            {
                Logger.Warn(data: $"The serialized packet has the wrong format. Packet: {packetContent}",
                    innerException: ex);
                return null;
            }
        }

        /// <summary>
        ///     Deserializes a string into a PacketDefinition
        /// </summary>
        /// <typeparam name="TPacket"></typeparam>
        /// <param name="packetContent">The content to deseralize</param>
        /// <param name="includesKeepAliveIdentity">
        ///     Include the keep alive identity or exclude it
        /// </param>
        /// <returns>The deserialized packet.</returns>
        public static TPacket Deserialize<TPacket>(string packetContent, bool includesKeepAliveIdentity = false)
            where TPacket : PacketDefinition
        {
            try
            {
                var serializationInformation = GetSerializationInformation(serializationType: typeof(TPacket));
                var deserializedPacket = Activator.CreateInstance<TPacket>(); // reflection is bad, improve?
                SetDeserializationInformations(packetDefinition: deserializedPacket, packetContent: packetContent,
                    packetHeader: serializationInformation.Key.Item2);
                return (TPacket)Deserialize(packetContent: packetContent, deserializedPacket: deserializedPacket,
                    serializationInformation: serializationInformation,
                    includesKeepAliveIdentity: includesKeepAliveIdentity);
            }
            catch (Exception e)
            {
                Logger.Warn(data: $"The serialized packet has the wrong format. Packet: {packetContent}",
                    innerException: e);
                return null;
            }
        }

        /// <summary>
        ///     Initializes the PacketFactory and generates the serialization informations based on the
        ///     given BaseType.
        /// </summary>
        /// <typeparam name="TBaseType">The BaseType to generate serialization informations</typeparam>
        public static void Initialize<TBaseType>() where TBaseType : PacketDefinition
        {
            if (!IsInitialized)
            {
                GenerateSerializationInformations<TBaseType>();
                IsInitialized = true;
            }
        }

        /// <summary>
        ///     Serializes a PacketDefinition to string.
        /// </summary>
        /// <typeparam name="TPacket">The type of the PacketDefinition</typeparam>
        /// <param name="packet">The object reference of the PacketDefinition</param>
        /// <returns>The serialized string.</returns>
        public static string Serialize<TPacket>(TPacket packet) where TPacket : PacketDefinition
        {
            try
            {
                // load pregenerated serialization information
                var serializationInformation = GetSerializationInformation(serializationType: packet.GetType());
                var deserializedPacket = serializationInformation.Key.Item2; // set header
                var lastIndex = 0;
                foreach (var packetBasePropertyInfo in serializationInformation.Value)
                {
                    // check if we need to add a non mapped values (pseudovalues)
                    if (packetBasePropertyInfo.Key.Index > lastIndex + 1)
                    {
                        var amountOfEmptyValuesToAdd = packetBasePropertyInfo.Key.Index - (lastIndex + 1);

                        for (var i = 0; i < amountOfEmptyValuesToAdd; i++) deserializedPacket += " 0";
                    }

                    // add value for current configuration
                    deserializedPacket += SerializeValue(propertyType: packetBasePropertyInfo.Value.PropertyType,
                        value: packetBasePropertyInfo.Value.GetValue(obj: packet),
                        packetIndexAttribute: packetBasePropertyInfo.Key);

                    // check if the value should be serialized to end
                    if (packetBasePropertyInfo.Key.SerializeToEnd)
                        // we reached the end
                        break;

                    // set new index
                    lastIndex = packetBasePropertyInfo.Key.Index;
                }

                return deserializedPacket;
            }
            catch (Exception e)
            {
                Logger.Warn(data: "Wrong Packet Format!", innerException: e);
                return "";
            }
        }

        static PacketDefinition Deserialize(string packetContent, PacketDefinition deserializedPacket,
            KeyValuePair<Tuple<Type, string>,
                Dictionary<PacketIndexAttribute, PropertyInfo>> serializationInformation,
            bool includesKeepAliveIdentity)
        {
            var matches = Regex.Matches(input: packetContent,
                pattern: @"([^\s]+[\.][^\s]+[\s]?)+((?=\s)|$)|([^\s]+)((?=\s)|$)");

            if (matches.Count > 0)
                foreach (var packetBasePropertyInfo in serializationInformation.Value)
                {
                    var currentIndex =
                        packetBasePropertyInfo.Key.Index +
                        (includesKeepAliveIdentity
                            ? 2
                            : 1); // adding 2 because we need to skip incrementing number and packet header

                    if (currentIndex < matches.Count)
                    {
                        if (packetBasePropertyInfo.Key.SerializeToEnd)
                        {
                            // get the value to the end and stop deserialization
                            var valueToEnd = packetContent.Substring(startIndex: matches[i: currentIndex].Index,
                                length: packetContent.Length - matches[i: currentIndex].Index);
                            packetBasePropertyInfo.Value.SetValue(obj: deserializedPacket,
                                value: DeserializeValue(packetPropertyType: packetBasePropertyInfo.Value.PropertyType,
                                    currentValue: valueToEnd,
                                    packetIndexAttribute: packetBasePropertyInfo.Key, packetMatches: matches,
                                    includesKeepAliveIdentity: includesKeepAliveIdentity));
                            break;
                        }

                        var currentValue = matches[i: currentIndex].Value;

                        // set the value & convert currentValue
                        packetBasePropertyInfo.Value.SetValue(obj: deserializedPacket,
                            value: DeserializeValue(packetPropertyType: packetBasePropertyInfo.Value.PropertyType,
                                currentValue: currentValue,
                                packetIndexAttribute: packetBasePropertyInfo.Key, packetMatches: matches,
                                includesKeepAliveIdentity: includesKeepAliveIdentity));
                    }
                    else
                    {
                        break;
                    }
                }

            return deserializedPacket;
        }

        /// <summary>
        ///     Converts simple list to List of Bytes
        /// </summary>
        /// <param name="currentValues">String to convert</param>
        /// <param name="genericListType">Type of the property to convert</param>
        /// <returns>The string as converted List</returns>
        static IList DeserializeSimpleList(string currentValues, Type genericListType)
        {
            var subpackets = (IList)Convert.ChangeType(value: Activator.CreateInstance(type: genericListType),
                conversionType: genericListType);
            foreach (var currentValue in currentValues.Split('.'))
            {
                var value = DeserializeValue(packetPropertyType: genericListType.GenericTypeArguments[0],
                    currentValue: currentValue, packetIndexAttribute: null, packetMatches: null);
                subpackets.Add(value: value);
            }

            return subpackets;
        }

        static object DeserializeSubpacket(string currentSubValues, Type packetBasePropertyType,
            KeyValuePair<Tuple<Type, string>, Dictionary<PacketIndexAttribute, PropertyInfo>>
                subpacketSerializationInfo, bool isReturnPacket = false)
        {
            var subpacketValues = currentSubValues.Split(isReturnPacket ? '^' : '.');
            var newSubpacket = Activator.CreateInstance(type: packetBasePropertyType);

            foreach (var subpacketPropertyInfo in subpacketSerializationInfo.Value)
            {
                var currentSubIndex =
                    isReturnPacket
                        ? subpacketPropertyInfo.Key.Index + 1
                        : subpacketPropertyInfo.Key.Index; // return packets do include header
                var currentSubValue = subpacketValues[currentSubIndex];

                subpacketPropertyInfo.Value.SetValue(obj: newSubpacket,
                    value: DeserializeValue(packetPropertyType: subpacketPropertyInfo.Value.PropertyType,
                        currentValue: currentSubValue,
                        packetIndexAttribute: subpacketPropertyInfo.Key, packetMatches: null));
            }

            return newSubpacket;
        }

        /// <summary>
        ///     Converts a Sublist of Packets to List of Subpackets
        /// </summary>
        /// <param name="currentValue">The value as String</param>
        /// <param name="packetBasePropertyType">Type of the Property to convert to</param>
        /// <param name="shouldRemoveSeparator"></param>
        /// <param name="packetMatchCollections"></param>
        /// <param name="currentIndex"></param>
        /// <param name="includesKeepAliveIdentity"></param>
        /// <returns>List of Deserialized subpackets</returns>
        static IList DeserializeSubpackets(string currentValue, Type packetBasePropertyType,
            bool shouldRemoveSeparator, MatchCollection packetMatchCollections, int? currentIndex,
            bool includesKeepAliveIdentity)
        {
            // split into single values
            var splittedSubpackets = currentValue.Split(' ').ToList();

            // generate new list
            var subpackets = (IList)Convert.ChangeType(value: Activator.CreateInstance(type: packetBasePropertyType),
                conversionType: packetBasePropertyType);

            var subPacketType = packetBasePropertyType.GetGenericArguments()[0];
            var subpacketSerializationInfo = GetSerializationInformation(serializationType: subPacketType);

            // handle subpackets with separator
            if (shouldRemoveSeparator)
            {
                if (!currentIndex.HasValue || packetMatchCollections == null) return subpackets;

                var splittedSubpacketParts =
                    packetMatchCollections.Cast<Match>().Select(selector: m => m.Value).ToList();
                splittedSubpackets = new List<string>();

                var generatedPseudoDelimitedString = "";
                var subPacketTypePropertiesCount = subpacketSerializationInfo.Value.Count;

                // check if the amount of properties can be serialized properly
                if ((splittedSubpacketParts.Count + (includesKeepAliveIdentity ? 1 : 0))
                    % subPacketTypePropertiesCount ==
                    0) // amount of properties per subpacket does match the given value amount in %
                    for (var i = currentIndex.Value + 1 + (includesKeepAliveIdentity ? 1 : 0);
                        i < splittedSubpacketParts.Count;
                        i++)
                    {
                        int j;
                        for (j = i; j < i + subPacketTypePropertiesCount; j++)
                            // add delimited value
                            generatedPseudoDelimitedString += splittedSubpacketParts[index: j] + ".";
                        i = j - 1;

                        //remove last added separator
                        generatedPseudoDelimitedString =
                            generatedPseudoDelimitedString.Substring(startIndex: 0,
                                length: generatedPseudoDelimitedString.Length - 1);

                        // add delimited values to list of values to serialize
                        splittedSubpackets.Add(item: generatedPseudoDelimitedString);
                        generatedPseudoDelimitedString = "";
                    }
                else
                    throw new Exception(
                        message:
                        "The amount of splitted subpacket values without delimiter do not match the % property amount of the serialized type.");
            }

            foreach (var subpacket in splittedSubpackets)
                subpackets.Add(value: DeserializeSubpacket(currentSubValues: subpacket,
                    packetBasePropertyType: subPacketType, subpacketSerializationInfo: subpacketSerializationInfo));

            return subpackets;
        }

        static object DeserializeValue(Type packetPropertyType, string currentValue,
            PacketIndexAttribute packetIndexAttribute, MatchCollection packetMatches,
            bool includesKeepAliveIdentity = false)
        {
            // check for empty value and cast it to null
            if (currentValue == "-1" || currentValue == "-") currentValue = null;

            // enum should be casted to number
            if (packetPropertyType.BaseType?.Equals(o: typeof(Enum)) == true)
            {
                object convertedValue = null;
                try
                {
                    if (currentValue != null &&
                        packetPropertyType.IsEnumDefined(value: Enum.Parse(enumType: packetPropertyType,
                            value: currentValue)))
                        convertedValue = Enum.Parse(enumType: packetPropertyType, value: currentValue);
                }
                catch (Exception)
                {
                    Logger.Warn(data: $"Could not convert value {currentValue} to type {packetPropertyType.Name}");
                }

                return convertedValue;
            }

            if (packetPropertyType.Equals(o: typeof(bool))) // handle boolean values
                return !currentValue.Equals(value: "0");
            if (packetPropertyType.BaseType?.Equals(o: typeof(PacketDefinition)) == true) // subpacket
            {
                var subpacketSerializationInfo = GetSerializationInformation(serializationType: packetPropertyType);
                return DeserializeSubpacket(currentSubValues: currentValue, packetBasePropertyType: packetPropertyType,
                    subpacketSerializationInfo: subpacketSerializationInfo,
                    isReturnPacket: packetIndexAttribute?.IsReturnPacket ?? false);
            }

            if (packetPropertyType.IsGenericType && packetPropertyType.GetGenericTypeDefinition()
                                                     .IsAssignableFrom(c: typeof(List<>)) // subpacket list
                                                 && packetPropertyType.GenericTypeArguments[0].BaseType
                                                     .Equals(o: typeof(PacketDefinition)))
                return DeserializeSubpackets(currentValue: currentValue, packetBasePropertyType: packetPropertyType,
                    shouldRemoveSeparator: packetIndexAttribute?.RemoveSeparator ?? false,
                    packetMatchCollections: packetMatches, currentIndex: packetIndexAttribute?.Index,
                    includesKeepAliveIdentity: includesKeepAliveIdentity);
            if (packetPropertyType.IsGenericType &&
                packetPropertyType.GetGenericTypeDefinition().IsAssignableFrom(c: typeof(List<>))) // simple list
                return DeserializeSimpleList(currentValues: currentValue, genericListType: packetPropertyType);
            if (Nullable.GetUnderlyingType(nullableType: packetPropertyType) != null &&
                string.IsNullOrEmpty(value: currentValue)
            ) // empty nullable value
                return null;
            if (Nullable.GetUnderlyingType(nullableType: packetPropertyType) != null) // nullable value
            {
                if (packetPropertyType.GenericTypeArguments[0]?.BaseType.Equals(o: typeof(Enum)) == true)
                    return Enum.Parse(enumType: packetPropertyType.GenericTypeArguments[0], value: currentValue);
                return Convert.ChangeType(value: currentValue,
                    conversionType: packetPropertyType.GenericTypeArguments[0]);
            }

            return Convert.ChangeType(value: currentValue,
                conversionType: packetPropertyType); // cast to specified type
        }

        static void GenerateSerializationInformations<TPacketDefinition>()
            where TPacketDefinition : PacketDefinition
        {
            _packetSerializationInformations =
                new Dictionary<Tuple<Type, string>, Dictionary<PacketIndexAttribute, PropertyInfo>>();

            // Iterate thru all PacketDefinition implementations
            foreach (var packetBaseType in typeof(TPacketDefinition).Assembly.GetTypes().Where(predicate: p =>
                !p.IsInterface && typeof(TPacketDefinition).BaseType.IsAssignableFrom(c: p)))
            {
                // add to serialization informations
                var serializationInformations = GenerateSerializationInformations(serializationType: packetBaseType);
            }
        }

        static KeyValuePair<Tuple<Type, string>, Dictionary<PacketIndexAttribute, PropertyInfo>>
            GenerateSerializationInformations(Type serializationType)
        {
            var header = serializationType.GetCustomAttribute<PacketHeaderAttribute>()?.Identification;

            if (header == null)
                throw new Exception(message: $"Packet header cannot be empty. PacketType: {serializationType.Name}");

            var packetsForPacketDefinition = new Dictionary<PacketIndexAttribute, PropertyInfo>();

            foreach (var packetBasePropertyInfo in serializationType.GetProperties()
                .Where(predicate: x => x.GetCustomAttributes(inherit: false).OfType<PacketIndexAttribute>().Any()))
            {
                var indexAttribute = packetBasePropertyInfo.GetCustomAttributes(inherit: false)
                    .OfType<PacketIndexAttribute>()
                    .FirstOrDefault();

                if (indexAttribute != null)
                    packetsForPacketDefinition.Add(key: indexAttribute, value: packetBasePropertyInfo);
            }

            // order by index
            var keyValuePairs = packetsForPacketDefinition.OrderBy(keySelector: p => p.Key.Index);

            var serializationInformation =
                new KeyValuePair<Tuple<Type, string>, Dictionary<PacketIndexAttribute, PropertyInfo>>(
                    key: new Tuple<Type, string>(item1: null, item2: ""), value: null);
            foreach (var str in header)
            {
                if (string.IsNullOrEmpty(value: str))
                    throw new Exception(
                        message: $"Packet header cannot be empty. PacketType: {serializationType.Name}");
                serializationInformation =
                    new KeyValuePair<Tuple<Type, string>, Dictionary<PacketIndexAttribute, PropertyInfo>>(
                        key: new Tuple<Type, string>(item1: serializationType, item2: str),
                        value: packetsForPacketDefinition);
                _packetSerializationInformations.Add(key: serializationInformation.Key,
                    value: serializationInformation.Value);
            }

            return serializationInformation;
        }

        static KeyValuePair<Tuple<Type, string>, Dictionary<PacketIndexAttribute, PropertyInfo>>
            GetSerializationInformation(Type serializationType)
        {
            return _packetSerializationInformations.Any(predicate: si => si.Key.Item1 == serializationType)
                ? _packetSerializationInformations.FirstOrDefault(predicate: si => si.Key.Item1 == serializationType)
                : GenerateSerializationInformations(
                    serializationType: serializationType); // generic runtime serialization parameter generation
        }

        /// <summary>
        ///     Converts List of Bytes to Simple list
        /// </summary>
        /// <param name="listValues">Values in List of simple type.</param>
        /// <param name="propertyType">The simple type.</param>
        /// <returns>String of serialized bytes</returns>
        static string SerializeSimpleList(IList listValues, Type propertyType)
        {
            var resultListPacket = "";
            var listValueCount = listValues.Count;
            if (listValueCount > 0)
            {
                resultListPacket += SerializeValue(propertyType: propertyType.GenericTypeArguments[0],
                    value: listValues[index: 0]);

                for (var i = 1; i < listValueCount; i++)
                    resultListPacket +=
                        $".{SerializeValue(propertyType: propertyType.GenericTypeArguments[0], value: listValues[index: i]).Replace(oldValue: " ", newValue: "")}";
            }

            return resultListPacket;
        }

        static string SerializeSubpacket(object value,
            KeyValuePair<Tuple<Type, string>, Dictionary<PacketIndexAttribute, PropertyInfo>>
                subpacketSerializationInfo, bool isReturnPacket, bool shouldRemoveSeparator)
        {
            var serializedSubpacket = isReturnPacket ? $" #{subpacketSerializationInfo.Key.Item2}^" : " ";

            // iterate thru configure subpacket properties
            foreach (var subpacketPropertyInfo in subpacketSerializationInfo.Value)
            {
                // first element
                if (subpacketPropertyInfo.Key.Index != 0)
                    serializedSubpacket += isReturnPacket ? "^" : shouldRemoveSeparator ? " " : ".";

                serializedSubpacket += SerializeValue(propertyType: subpacketPropertyInfo.Value.PropertyType,
                    value: subpacketPropertyInfo.Value.GetValue(obj: value)).Replace(oldValue: " ", newValue: "");
            }

            return serializedSubpacket;
        }

        static string SerializeSubpackets(IList listValues, Type packetBasePropertyType,
            bool shouldRemoveSeparator)
        {
            var serializedSubPacket = "";
            var subpacketSerializationInfo =
                GetSerializationInformation(serializationType: packetBasePropertyType.GetGenericArguments()[0]);

            if (listValues.Count > 0)
                foreach (var listValue in listValues)
                    serializedSubPacket += SerializeSubpacket(value: listValue,
                        subpacketSerializationInfo: subpacketSerializationInfo, isReturnPacket: false,
                        shouldRemoveSeparator: shouldRemoveSeparator);

            return serializedSubPacket;
        }

        static string SerializeValue(Type propertyType, object value,
            PacketIndexAttribute packetIndexAttribute = null)
        {
            if (propertyType != null)
            {
                // check for nullable without value or string
                if (propertyType.Equals(o: typeof(string)) &&
                    string.IsNullOrEmpty(value: Convert.ToString(value: value))) return " -";
                if (Nullable.GetUnderlyingType(nullableType: propertyType) != null &&
                    string.IsNullOrEmpty(value: Convert.ToString(value: value)))
                    return " -1";

                // enum should be casted to number
                if (propertyType.BaseType?.Equals(o: typeof(Enum)) == true) return $" {Convert.ToInt16(value: value)}";
                if (propertyType.Equals(o: typeof(bool)))
                    // bool is 0 or 1 not True or False
                    return Convert.ToBoolean(value: value) ? " 1" : " 0";
                if (propertyType.BaseType?.Equals(o: typeof(PacketDefinition)) == true)
                {
                    var subpacketSerializationInfo = GetSerializationInformation(serializationType: propertyType);
                    return SerializeSubpacket(value: value, subpacketSerializationInfo: subpacketSerializationInfo,
                        isReturnPacket: packetIndexAttribute?.IsReturnPacket ?? false,
                        shouldRemoveSeparator: packetIndexAttribute?.RemoveSeparator ?? false);
                }

                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition()
                                                   .IsAssignableFrom(c: typeof(List<>))
                                               && propertyType.GenericTypeArguments[0].BaseType
                                                   .Equals(o: typeof(PacketDefinition)))
                    return SerializeSubpackets(listValues: (IList)value, packetBasePropertyType: propertyType,
                        shouldRemoveSeparator: packetIndexAttribute?.RemoveSeparator ?? false);
                if (propertyType.IsGenericType &&
                    propertyType.GetGenericTypeDefinition().IsAssignableFrom(c: typeof(List<>))) //simple list
                    return SerializeSimpleList(listValues: (IList)value, propertyType: propertyType);
                return $" {value}";
            }

            return "";
        }

        static PacketDefinition SetDeserializationInformations(PacketDefinition packetDefinition,
            string packetContent, string packetHeader)
        {
            packetDefinition.OriginalContent = packetContent;
            packetDefinition.OriginalHeader = packetHeader;
            return packetDefinition;
        }

        #endregion
    }
}