﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace OpenNos.Core.Extensions
{
    public static class ConcurrentBagExtensions
    {
        #region Methods

        public static void Clear<T>(this ConcurrentBag<T> queue)
        {
            while (queue.Count > 0)
                queue.TryTake(result: out var _);
        }

        public static ConcurrentBag<T> Replace<T>(this ConcurrentBag<T> queue, Func<T, bool> predicate)
        {
            return new ConcurrentBag<T>(collection: queue.ToList().Where(predicate: predicate));
        }

        public static void RemoveWhere<T>(this ConcurrentBag<T> queue, Func<T, bool> predicate,
            out ConcurrentBag<T> queueReturned)
        {
            queueReturned = new ConcurrentBag<T>(collection: queue.Where(predicate: predicate));
        }

        #endregion
    }
}