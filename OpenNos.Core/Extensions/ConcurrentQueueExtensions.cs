﻿using System.Collections.Concurrent;

namespace OpenNos.Core.Extensions
{
    public static class ConcurrentQueueExtensions
    {
        #region Methods

        public static void Clear<T>(this ConcurrentQueue<T> queue)
        {
            while (queue.Count > 0)
                queue.TryDequeue(result: out var _);
        }

        #endregion
    }
}