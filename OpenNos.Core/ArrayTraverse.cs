﻿using System;

namespace OpenNos.Core.ArrayExtensions
{
    class ArrayTraverse
    {
        #region Instantiation

        public ArrayTraverse(Array array)
        {
            _maxLengths = new int[array.Rank];
            for (var i = 0; i < array.Rank; ++i) _maxLengths[i] = array.GetLength(i) - 1;
            _position = new int[array.Rank];
        }

        #endregion

        #region Methods

        public bool Step()
        {
            for (var i = 0; i < _position.Length; ++i)
                if (_position[i] < _maxLengths[i])
                {
                    _position[i]++;
                    for (var j = 0; j < i; j++) _position[j] = 0;
                    return true;
                }

            return false;
        }

        #endregion

        #region Members

        public readonly int[] _position;

        readonly int[] _maxLengths;

        #endregion
    }
}