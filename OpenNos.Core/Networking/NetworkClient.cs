﻿using System.Collections.Generic;
using OpenNos.Core.Cryptography;
using OpenNos.Core.Networking.Communication.Scs.Communication;
using OpenNos.Core.Networking.Communication.Scs.Communication.Channels;
using OpenNos.Core.Networking.Communication.Scs.Communication.Messages;
using OpenNos.Core.Networking.Communication.Scs.Server;

namespace OpenNos.Core.Networking
{
    public class NetworkClient : ScsServerClient, INetworkClient
    {
        #region Members

        CryptographyBase _encryptor;

        #endregion

        #region Instantiation

        public NetworkClient(ICommunicationChannel communicationChannel) : base(
            communicationChannel: communicationChannel)
        {
        }

        #endregion

        #region Properties

        public string IpAddress
        {
            get => RemoteEndPoint.ToString();
        }

        public bool IsConnected
        {
            get => CommunicationState == CommunicationStates.Connected;
        }

        public bool IsDisposing { get; set; }

        #endregion

        #region Methods

        public void Initialize(CryptographyBase encryptor)
        {
            _encryptor = encryptor;
        }

        public void SendPacket(string packet, byte priority = 10)
        {
            if (!IsDisposing && packet != null && packet != "")
            {
                var rawMessage = new ScsRawDataMessage(messageData: _encryptor.Encrypt(data: packet));
                SendMessage(message: rawMessage, priority: priority);
            }
        }

        public void SendPacketFormat(string packet, params object[] param)
        {
            SendPacket(packet: string.Format(format: packet, args: param));
        }

        public void SendPackets(IEnumerable<string> packets, byte priority = 10)
        {
            foreach (var packet in packets) SendPacket(packet: packet, priority: priority);
        }

        public void SetClientSession(object clientSession)
        {
        }

        #endregion
    }
}