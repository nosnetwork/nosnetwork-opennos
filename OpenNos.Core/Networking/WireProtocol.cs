﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using OpenNos.Core.ExceptionExtensions;
using OpenNos.Core.Networking.Communication.Scs.Communication.Messages;
using OpenNos.Core.Networking.Communication.Scs.Communication.Protocols;

namespace OpenNos.Core.Networking
{
    public class WireProtocol : IScsWireProtocol, IDisposable
    {
        #region Instantiation

        public WireProtocol()
        {
            _receiveMemoryStream = new MemoryStream();
            new Dictionary<string, DateTime>();
        }

        #endregion

        #region Members

        /// <summary>
        ///     Maximum length of a message.
        /// </summary>
        const short MaxMessageLength = 4096;

        bool _disposed;

        /// <summary>
        ///     This MemoryStream object is used to collect receiving bytes to build messages.
        /// </summary>
        MemoryStream _receiveMemoryStream;

        #endregion

        #region Methods

        public IEnumerable<IScsMessage> CreateMessages(byte[] receivedBytes)
        {
            // Write all received bytes to the _receiveMemoryStream
            _receiveMemoryStream.Write(buffer: receivedBytes, offset: 0, count: receivedBytes.Length);

            // Create a list to collect messages
            var messages = new List<IScsMessage>();

            // Read all available messages and add to messages collection
            while (ReadSingleMessage(messages: messages))
            {
            }

            // Return message list
            return messages;
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                Dispose(disposing: true);
                GC.SuppressFinalize(obj: this);
                _disposed = true;
            }
        }

        public byte[] GetBytes(IScsMessage message)
        {
            // Serialize the message to a byte array
            ScsTextMessage textMessage;
            textMessage = message as ScsTextMessage;
            return textMessage != null
                ? Encoding.Default.GetBytes(s: textMessage.Text)
                : ((ScsRawDataMessage)message).MessageData;
        }

        public void Reset()
        {
            if (_receiveMemoryStream.Length > 0) _receiveMemoryStream = new MemoryStream();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing) _receiveMemoryStream.Dispose();
        }

        /// <summary>
        ///     Reads a byte array with specified length.
        /// </summary>
        /// <param name="stream">Stream to read from</param>
        /// <param name="length">Length of the byte array to read</param>
        /// <returns>Read byte array</returns>
        /// <exception cref="EndOfStreamException">
        ///     Throws EndOfStreamException if can not read from stream.
        /// </exception>
        static byte[] ReadByteArray(Stream stream, short length)
        {
            var buffer = new byte[length];
            var read = stream.Read(buffer: buffer, offset: 0, count: length);
            if (read <= 0) throw new EndOfStreamException(message: "Can not read from stream! Input stream is closed.");
            return buffer;
        }

        /// <summary>
        ///     This method tries to read a single message and add to the messages collection.
        /// </summary>
        /// <param name="messages">Messages collection to collect messages</param>
        /// <returns>
        ///     Returns a boolean value indicates that if there is a need to re-call this method.
        /// </returns>
        /// <exception cref="CommunicationException">
        ///     Throws CommunicationException if message is bigger than maximum allowed message length.
        /// </exception>
        bool ReadSingleMessage(ICollection<IScsMessage> messages)
        {
            // Go to the beginning of the stream
            _receiveMemoryStream.Position = 0;

            // check if message length is 0
            if (_receiveMemoryStream.Length == 0) return false;

            // get length of frame
            var frameLength = (short)_receiveMemoryStream.Length;

            // Read length of the message
            if (frameLength > MaxMessageLength)
                throw new CommunicationException(message: "Message is too big (" + frameLength +
                                                          " bytes). Max allowed length is " + MaxMessageLength +
                                                          " bytes.");

            // Read bytes of serialized message and deserialize it
            var serializedMessageBytes = ReadByteArray(stream: _receiveMemoryStream, length: frameLength);
            messages.Add(item: new ScsRawDataMessage(messageData: serializedMessageBytes));

            // Read remaining bytes to an array
            if (_receiveMemoryStream.Length > frameLength)
            {
                var remainingBytes = ReadByteArray(stream: _receiveMemoryStream,
                    length: (short)(_receiveMemoryStream.Length - frameLength));

                // Re-create the receive memory stream and write remaining bytes
                _receiveMemoryStream = new MemoryStream();
                _receiveMemoryStream.Write(buffer: remainingBytes, offset: 0, count: remainingBytes.Length);
            }
            else
            {
                // nothing left, just recreate
                _receiveMemoryStream = new MemoryStream();
            }

            // Return true to re-call this method to try to read next message
            return _receiveMemoryStream.Length > 0;
        }

        #endregion
    }
}