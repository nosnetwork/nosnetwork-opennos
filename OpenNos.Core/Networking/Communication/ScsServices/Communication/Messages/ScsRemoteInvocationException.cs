﻿using System;
using System.Runtime.Serialization;

namespace OpenNos.Core.Networking.Communication.ScsServices.Communication.Messages
{
    /// <summary>
    ///     Exception thrown when service invocation target errors.
    /// </summary>
    [Serializable]
    public class ScsRemoteInvocationException : ScsRemoteException
    {
        #region Overrides of Exception

        /// <inheritdoc />
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info: info, context: context);

            info.AddValue(name: nameof(MethodName), value: MethodName);
            info.AddValue(name: nameof(ServiceType), value: ServiceType);
            info.AddValue(name: nameof(ServiceVersion), value: ServiceVersion);
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ScsRemoteInvocationException" /> class.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="serviceVersion">The service version.</param>
        /// <param name="methodName">Name of the method.</param>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public ScsRemoteInvocationException(string serviceType, string serviceVersion, string methodName,
            string message, Exception innerException)
            : base(message: message, innerException: innerException)
        {
            MethodName = methodName;
            ServiceType = serviceType;
            ServiceVersion = serviceVersion;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ScsRemoteInvocationException" /> class.
        /// </summary>
        /// <param name="serializationInfo"></param>
        /// <param name="context"></param>
        protected ScsRemoteInvocationException(SerializationInfo serializationInfo, StreamingContext context)
            : base(serializationInfo: serializationInfo, context: context)
        {
            MethodName = serializationInfo.GetString(name: nameof(MethodName));
            ServiceType = serializationInfo.GetString(name: nameof(ServiceType));
            ServiceVersion = serializationInfo.GetString(name: nameof(ServiceVersion));
        }

        public ScsRemoteInvocationException()
        {
        }

        public ScsRemoteInvocationException(string message) : base(message: message)
        {
        }

        public ScsRemoteInvocationException(string message, Exception innerException) : base(message: message,
            innerException: innerException)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the name of the invoked method.
        /// </summary>
        public string MethodName { get; }

        /// <summary>
        ///     Gets the type of the service class.
        /// </summary>
        public string ServiceType { get; }

        /// <summary>
        ///     Gets the service version.
        /// </summary>
        public string ServiceVersion { get; }

        #endregion
    }
}