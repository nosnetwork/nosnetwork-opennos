﻿using System;
using System.Linq;
using System.Reflection;
using OpenNos.Core.Networking.Communication.Scs.Client;
using OpenNos.Core.Networking.Communication.Scs.Communication;
using OpenNos.Core.Networking.Communication.Scs.Communication.Channels;
using OpenNos.Core.Networking.Communication.Scs.Communication.Messages;
using OpenNos.Core.Networking.Communication.Scs.Communication.Messengers;
using OpenNos.Core.Networking.Communication.ScsServices.Communication;
using OpenNos.Core.Networking.Communication.ScsServices.Communication.Messages;

namespace OpenNos.Core.Networking.Communication.ScsServices.Client
{
    /// <summary>
    ///     Represents a service client that consumes a SCS service.
    /// </summary>
    /// <typeparam name="T">Type of service interface</typeparam>
    public class ScsServiceClient<T> : IScsServiceClient<T> where T : class
    {
        #region Instantiation

        /// <summary>
        ///     Creates a new ScsServiceClient object.
        /// </summary>
        /// <param name="client">Underlying IScsClient object to communicate with server</param>
        /// <param name="clientObject">
        ///     The client object that is used to call method invokes in client side. May be null if
        ///     client has no methods to be invoked by server.
        /// </param>
        public ScsServiceClient(IScsClient client, object clientObject)
        {
            _client = client;
            _clientObject = clientObject;

            _client.Connected += Client_Connected;
            _client.Disconnected += Client_Disconnected;

            _requestReplyMessenger = new RequestReplyMessenger<IScsClient>(messenger: client);
            _requestReplyMessenger.MessageReceived += RequestReplyMessenger_MessageReceived;

            var realServiceProxy =
                new AutoConnectRemoteInvokeProxy<T, IScsClient>(clientMessenger: _requestReplyMessenger, client: this);
            ServiceProxy = (T)realServiceProxy.GetTransparentProxy();
        }

        #endregion

        #region Members

        /// <summary>
        ///     Underlying IScsClient object to communicate with server.
        /// </summary>
        readonly IScsClient _client;

        /// <summary>
        ///     The client object that is used to call method invokes in client side. May be null if
        ///     client has no methods to be invoked by server.
        /// </summary>
        readonly object _clientObject;

        /// <summary>
        ///     Messenger object to send/receive messages over _client.
        /// </summary>
        readonly RequestReplyMessenger<IScsClient> _requestReplyMessenger;

        bool _disposed;

        #endregion

        #region Events

        /// <summary>
        ///     This event is raised when client connected to server.
        /// </summary>
        public event EventHandler Connected;

        /// <summary>
        ///     This event is raised when client disconnected from server.
        /// </summary>
        public event EventHandler Disconnected;

        #endregion

        #region Properties

        /// <inheritdoc />
        public ICommunicationChannel CommunicationChannel
        {
            get => _client.CommunicationChannel;
        }

        /// <summary>
        ///     Gets the current communication state.
        /// </summary>
        public CommunicationStates CommunicationState
        {
            get => _client.CommunicationState;
        }

        /// <summary>
        ///     Timeout for connecting to a server (as milliseconds). Default value: 15 seconds (15000 ms).
        /// </summary>
        public int ConnectTimeout
        {
            get => _client.ConnectTimeout;
            set => _client.ConnectTimeout = value;
        }

        /// <summary>
        ///     Reference to the service proxy to invoke remote service methods.
        /// </summary>
        public T ServiceProxy { get; }

        /// <summary>
        ///     Timeout value when invoking a service method. If timeout occurs before end of remote
        ///     method call, an exception is thrown. Use -1 for no timeout (wait indefinite). Default
        ///     value: 60000 (1 minute).
        /// </summary>
        public int Timeout
        {
            get => _requestReplyMessenger.Timeout;
            set => _requestReplyMessenger.Timeout = value;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Connects to server.
        /// </summary>
        public void Connect()
        {
            _client.Connect();
        }

        /// <summary>
        ///     Disconnects from server. Does nothing if already disconnected.
        /// </summary>
        public void Disconnect()
        {
            _client.Disconnect();
        }

        /// <summary>
        ///     Calls Disconnect method.
        /// </summary>
        public void Dispose()
        {
            if (!_disposed)
            {
                Dispose(disposing: true);
                GC.SuppressFinalize(obj: this);
                _disposed = true;
            }
        }

        /// <summary>
        ///     Gets a service proxy for the specified <typeparamref name="TServiceInterface" />.
        /// </summary>
        /// <typeparam name="TServiceInterface">the service interface type</typeparam>
        /// <returns></returns>
        public TServiceInterface GetServiceProxy<TServiceInterface>()
        {
            return (TServiceInterface)new AutoConnectRemoteInvokeProxy<TServiceInterface, IScsClient>(
                clientMessenger: _requestReplyMessenger, client: this).GetTransparentProxy();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing) Disconnect();
        }

        /// <summary>
        ///     Handles Connected event of _client object.
        /// </summary>
        /// <param name="sender">Source of object</param>
        /// <param name="e">Event arguments</param>
        void Client_Connected(object sender, EventArgs e)
        {
            _requestReplyMessenger.Start();
            OnConnected();
        }

        /// <summary>
        ///     Handles Disconnected event of _client object.
        /// </summary>
        /// <param name="sender">Source of object</param>
        /// <param name="e">Event arguments</param>
        void Client_Disconnected(object sender, EventArgs e)
        {
            _requestReplyMessenger.Stop();
            OnDisconnected();
        }

        /// <summary>
        ///     Raises Connected event.
        /// </summary>
        void OnConnected()
        {
            var handler = Connected;
            handler?.Invoke(sender: this, e: EventArgs.Empty);
        }

        /// <summary>
        ///     Raises Disconnected event.
        /// </summary>
        void OnDisconnected()
        {
            var handler = Disconnected;
            handler?.Invoke(sender: this, e: EventArgs.Empty);
        }

        /// <summary>
        ///     Handles MessageReceived event of messenger. It gets messages from server and invokes
        ///     appropriate method.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        void RequestReplyMessenger_MessageReceived(object sender, MessageEventArgs e)
        {
            // Cast message to ScsRemoteInvokeMessage and check it
            ScsRemoteInvokeMessage invokeMessage;
            invokeMessage = e.Message as ScsRemoteInvokeMessage;
            if (invokeMessage == null) return;

            // Check client object.
            if (_clientObject == null)
            {
                SendInvokeResponse(requestMessage: invokeMessage, returnValue: null,
                    exception: new ScsRemoteException(
                        message: "Client does not wait for method invocations by server."));
                return;
            }

            // Invoke method
            object returnValue;
            try
            {
                // reflection?
                var type = _clientObject.GetType();
                var method = type.GetMethod(name: invokeMessage.MethodName) ?? type.GetInterfaces()
                    .Select(selector: t => t.GetMethod(name: invokeMessage.MethodName))
                    .FirstOrDefault(predicate: m => m != null);
                returnValue = method?.Invoke(obj: _clientObject, parameters: invokeMessage.Parameters);
            }
            catch (TargetInvocationException ex)
            {
                var innerEx = ex.InnerException;
                if (innerEx != null)
                    SendInvokeResponse(requestMessage: invokeMessage, returnValue: null,
                        exception: new ScsRemoteException(message: innerEx.Message, innerException: innerEx));
                return;
            }
            catch (Exception ex)
            {
                SendInvokeResponse(requestMessage: invokeMessage, returnValue: null,
                    exception: new ScsRemoteException(message: ex.Message, innerException: ex));
                return;
            }

            // Send return value
            SendInvokeResponse(requestMessage: invokeMessage, returnValue: returnValue, exception: null);
        }

        /// <summary>
        ///     Sends response to the remote application that invoked a service method.
        /// </summary>
        /// <param name="requestMessage">Request message</param>
        /// <param name="returnValue">Return value to send</param>
        /// <param name="exception">Exception to send</param>
        void SendInvokeResponse(IScsMessage requestMessage, object returnValue, ScsRemoteException exception)
        {
            try
            {
                _requestReplyMessenger.SendMessage(message: new ScsRemoteInvokeReturnMessage
                {
                    RepliedMessageId = requestMessage.MessageId,
                    ReturnValue = returnValue,
                    RemoteException = exception
                }, priority: 10);
            }
            catch (Exception ex)
            {
                Logger.Error(data: "Invoke response failed to send", ex: ex);
            }
        }

        #endregion
    }
}