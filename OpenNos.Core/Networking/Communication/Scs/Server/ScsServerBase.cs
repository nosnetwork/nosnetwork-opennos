﻿using System;
using OpenNos.Core.Networking.Communication.Scs.Communication.Channels;
using OpenNos.Core.Networking.Communication.Scs.Communication.Protocols;
using OpenNos.Core.Threading;

namespace OpenNos.Core.Networking.Communication.Scs.Server
{
    /// <summary>
    ///     This class provides base functionality for server Classs.
    /// </summary>
    public abstract class ScsServerBase : IScsServer, IDisposable
    {
        #region Instantiation

        /// <summary>
        ///     Constructor.
        /// </summary>
        protected ScsServerBase()
        {
            Clients = new ThreadSafeSortedList<long, IScsServerClient>();
            WireProtocolFactory = WireProtocolManager.GetDefaultWireProtocolFactory();
        }

        #endregion

        #region Members

        /// <summary>
        ///     This object is used to listen incoming connections.
        /// </summary>
        IConnectionListener _connectionListener;

        bool _disposed;

        #endregion

        #region Events

        /// <summary>
        ///     This event is raised when a new client is connected.
        /// </summary>
        public event EventHandler<ServerClientEventArgs> ClientConnected;

        /// <summary>
        ///     This event is raised when a client disconnected from the server.
        /// </summary>
        public event EventHandler<ServerClientEventArgs> ClientDisconnected;

        #endregion

        #region Properties

        /// <summary>
        ///     A collection of clients that are connected to the server.
        /// </summary>
        public ThreadSafeSortedList<long, IScsServerClient> Clients { get; }

        /// <summary>
        ///     Gets/sets wire protocol that is used while reading and writing messages.
        /// </summary>
        public IScsWireProtocolFactory WireProtocolFactory { get; set; }

        #endregion

        #region Methods

        public void Dispose()
        {
            if (!_disposed)
            {
                Dispose(disposing: true);
                GC.SuppressFinalize(obj: this);
                _disposed = true;
            }
        }

        /// <summary>
        ///     Starts the server.
        /// </summary>
        public virtual void Start()
        {
            _connectionListener = CreateConnectionListener();
            _connectionListener.CommunicationChannelConnected += ConnectionListener_CommunicationChannelConnected;
            _connectionListener.Start();
        }

        /// <summary>
        ///     Stops the server.
        /// </summary>
        public virtual void Stop()
        {
            _connectionListener?.Stop();
            foreach (var client in Clients.GetAllItems()) client.Disconnect();
        }

        /// <summary>
        ///     This method is implemented by derived Classs to create appropriate connection listener to
        ///     listen incoming connection requets.
        /// </summary>
        /// <returns></returns>
        protected abstract IConnectionListener CreateConnectionListener();

        protected virtual void Dispose(bool disposing)
        {
            if (disposing) Clients.Dispose();
        }

        /// <summary>
        ///     Raises ClientConnected event.
        /// </summary>
        /// <param name="client">Connected client</param>
        protected virtual void OnClientConnected(IScsServerClient client)
        {
            ClientConnected?.Invoke(sender: this, e: new ServerClientEventArgs(client: client));
        }

        /// <summary>
        ///     Raises ClientDisconnected event.
        /// </summary>
        /// <param name="client">Disconnected client</param>
        protected virtual void OnClientDisconnected(IScsServerClient client)
        {
            ClientDisconnected?.Invoke(sender: this, e: new ServerClientEventArgs(client: client));
        }

        /// <summary>
        ///     Handles Disconnected events of all connected clients.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        void Client_Disconnected(object sender, EventArgs e)
        {
            var client = (IScsServerClient)sender;
            Clients.Remove(key: client.ClientId);
            OnClientDisconnected(client: client);
        }

        /// <summary>
        ///     Handles CommunicationChannelConnected event of _connectionListener object.
        /// </summary>
        /// <param name="sender">Source of event</param>
        /// <param name="e">Event arguments</param>
        void ConnectionListener_CommunicationChannelConnected(object sender, CommunicationChannelEventArgs e)
        {
            var client = new NetworkClient(communicationChannel: e.Channel)
            {
                ClientId = ScsServerManager.GetClientId(),
                WireProtocol = WireProtocolFactory.CreateWireProtocol()
            };

            client.Disconnected += Client_Disconnected;
            Clients[key: client.ClientId] = client;
            OnClientConnected(client: client);
            e.Channel.Start();
        }

        #endregion
    }
}