﻿using System.Net;
using System.Net.Sockets;
using OpenNos.Core.Networking.Communication.Scs.Communication.Channels;
using OpenNos.Core.Networking.Communication.Scs.Communication.Channels.Tcp;
using OpenNos.Core.Networking.Communication.Scs.Communication.EndPoints.Tcp;

namespace OpenNos.Core.Networking.Communication.Scs.Client.Tcp
{
    /// <summary>
    ///     This class is used to communicate with server over TCP/IP protocol.
    /// </summary>
    public class ScsTcpClient : ScsClientBase
    {
        #region Instantiation

        /// <summary>
        ///     Creates a new ScsTcpClient object.
        /// </summary>
        /// <param name="serverEndPoint">The endpoint address to connect to the server</param>
        /// <param name="existingSocketInformation">The existing socket information.</param>
        public ScsTcpClient(ScsTcpEndPoint serverEndPoint, SocketInformation? existingSocketInformation)
        {
            _serverEndPoint = serverEndPoint;
            _existingSocketInformation = existingSocketInformation;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Creates a communication channel using ServerIpAddress and ServerPort.
        /// </summary>
        /// <returns>Ready communication channel to communicate</returns>
        protected override ICommunicationChannel CreateCommunicationChannel()
        {
            Socket socket;

            if (_existingSocketInformation.HasValue)
            {
                socket = new Socket(socketInformation: _existingSocketInformation.Value);
                _existingSocketInformation = null;
            }
            else
            {
                socket = TcpHelper.ConnectToServer(
                    endPoint: new IPEndPoint(address: _serverEndPoint.IpAddress, port: _serverEndPoint.TcpPort),
                    timeoutMs: ConnectTimeout);
            }

            return new TcpCommunicationChannel(clientSocket: socket);
        }

        #endregion

        #region Members

        /// <summary>
        ///     The endpoint address of the server.
        /// </summary>
        readonly ScsTcpEndPoint _serverEndPoint;

        /// <summary>
        ///     The existing socket information or <c>null</c>.
        /// </summary>
        SocketInformation? _existingSocketInformation;

        #endregion
    }
}