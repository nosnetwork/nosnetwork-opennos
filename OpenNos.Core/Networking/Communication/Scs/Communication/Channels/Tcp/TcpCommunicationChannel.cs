﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using OpenNos.Core.Extensions;
using OpenNos.Core.Networking.Communication.Scs.Communication.EndPoints;
using OpenNos.Core.Networking.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.Core.Networking.Communication.Scs.Communication.Messages;

namespace OpenNos.Core.Networking.Communication.Scs.Communication.Channels.Tcp
{
    /// <summary>
    ///     This class is used to communicate with a remote application over TCP/IP protocol.
    /// </summary>
    public class TcpCommunicationChannel : CommunicationChannelBase, IDisposable
    {
        #region Instantiation

        /// <summary>
        ///     Creates a new TcpCommunicationChannel object.
        /// </summary>
        /// <param name="clientSocket">
        ///     A connected Socket object that is used to communicate over network
        /// </param>
        public TcpCommunicationChannel(Socket clientSocket)
        {
            _clientSocket = clientSocket;
            _clientSocket.NoDelay = true;
            var ipEndPoint = (IPEndPoint)_clientSocket.RemoteEndPoint;
            _remoteEndPoint = new ScsTcpEndPoint(ipAddress: ipEndPoint.Address, port: ipEndPoint.Port);
            _buffer = new byte[ReceiveBufferSize];
            var o = new object();
            _highPriorityBuffer = new ConcurrentQueue<byte[]>();
            _lowPriorityBuffer = new ConcurrentQueue<byte[]>();
            var cancellationToken = _sendCancellationToken.Token;

            var unused = StartSendingAsync(action: SendInterval,
                period: new TimeSpan(days: 0, hours: 0, minutes: 0, seconds: 0, milliseconds: 10),
                _sendCancellationToken: cancellationToken); //changed :)
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the endpoint of remote application.
        /// </summary>
        public override ScsEndPoint RemoteEndPoint
        {
            get => _remoteEndPoint;
        }

        #endregion

        #region Members

        /// <summary>
        ///     Size of the buffer that is used to receive bytes from TCP socket.
        /// </summary>
        const int ReceiveBufferSize = 4 * 1024; // 4KB

        const ushort PingRequest = 0x0779;

        const ushort PingResponse = 0x0988;

        /// <summary>
        ///     This buffer is used to receive bytes
        /// </summary>
        readonly byte[] _buffer;

        /// <summary>
        ///     Socket object to send/reveice messages.
        /// </summary>
        readonly Socket _clientSocket;

        readonly ConcurrentQueue<byte[]> _highPriorityBuffer;

        readonly ConcurrentQueue<byte[]> _lowPriorityBuffer;

        readonly ScsTcpEndPoint _remoteEndPoint;

        readonly CancellationTokenSource _sendCancellationToken = new CancellationTokenSource();

        bool _disposed;

        /// <summary>
        ///     A flag to control thread's running
        /// </summary>
        volatile bool _running;

        #endregion

        #region Methods

        /// <summary>
        ///     Duplicates the client socket and closes.
        /// </summary>
        /// <param name="processId">The process identifier.</param>
        /// <returns></returns>
        /// <summary>The callee should dispose anything relying on this channel immediately.</summary>
        public SocketInformation DuplicateSocketAndClose(int processId)
        {
            // request ping from host to kill our async BeginReceive
            _clientSocket.Send(buffer: BitConverter.GetBytes(value: PingRequest));

            // wait for response
            while (_running) Thread.Sleep(millisecondsTimeout: 20);

            return _clientSocket.DuplicateAndClose(targetProcessId: processId);
        }

        public static async Task StartSendingAsync(Action action, TimeSpan period,
            CancellationToken _sendCancellationToken)
        {
            while (!_sendCancellationToken.IsCancellationRequested)
            {
                await Task.Delay(delay: period, cancellationToken: _sendCancellationToken)
                    .ConfigureAwait(continueOnCapturedContext: false);
                if (!_sendCancellationToken.IsCancellationRequested) action?.Invoke();
            }
        }

        public override Task ClearLowPriorityQueueAsync()
        {
            _lowPriorityBuffer.Clear();
            return Task.CompletedTask;
        }

        /// <summary>
        ///     Disconnects from remote application and closes channel.
        /// </summary>
        public override void Disconnect()
        {
            if (CommunicationState != CommunicationStates.Connected) return;

            _running = false;
            try
            {
                _sendCancellationToken.Cancel();
                if (_clientSocket.Connected) _clientSocket.Close();

                _clientSocket.Dispose();
            }
            catch (Exception)
            {
                // do nothing
            }
            finally
            {
                _sendCancellationToken.Dispose();
            }

            CommunicationState = CommunicationStates.Disconnected;
            OnDisconnected();
        }

        /// <summary>
        ///     Calls Disconnect method.
        /// </summary>
        public void Dispose()
        {
            if (!_disposed)
            {
                Dispose(disposing: true);
                GC.SuppressFinalize(obj: this);
                _disposed = true;
            }
        }

        public void SendInterval()
        {
            try
            {
                if (WireProtocol != null)
                {
                    SendByPriority(buffer: _highPriorityBuffer);
                    SendByPriority(buffer: _lowPriorityBuffer);
                }
            }
            catch (Exception)
            {
                // disconnect
            }

            if (!_clientSocket.Connected)
            {
                // do nothing
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Disconnect();
                _sendCancellationToken.Dispose();
            }
        }

        /// <summary>
        ///     Sends a message to the remote application.
        /// </summary>
        /// <param name="message">Message to be sent</param>
        /// <param name="priority">Priority of message to send</param>
        protected override void SendMessagePublic(IScsMessage message, byte priority)
        {
            if (priority > 5)
                _highPriorityBuffer.Enqueue(item: WireProtocol.GetBytes(message: message));
            else
                _lowPriorityBuffer.Enqueue(item: WireProtocol.GetBytes(message: message));
        }

        /// <summary>
        ///     Starts the thread to receive messages from socket.
        /// </summary>
        protected override void StartPublic()
        {
            _running = true;
            _clientSocket.BeginReceive(buffer: _buffer, offset: 0, size: _buffer.Length, socketFlags: 0,
                callback: ReceiveCallback, state: null);
        }

        static void SendCallback(IAsyncResult result)
        {
            try
            {
                // Retrieve the socket from the state object.
                var client = (Socket)result.AsyncState;

                if (!client.Connected) return;

                // Complete sending the data to the remote device.
                var bytesSent = client.EndSend(asyncResult: result);
            }
            catch (Exception)
            {
                // disconnect
            }
        }

        /// <summary>
        ///     This method is used as callback method in _clientSocket's BeginReceive method. It
        ///     reveives bytes from socker.
        /// </summary>
        /// <param name="result">Asyncronous call result</param>
        void ReceiveCallback(IAsyncResult result)
        {
            if (!_running) return;

            try
            {
                var bytesRead = -1;

                // Get received bytes count
                bytesRead = _clientSocket.EndReceive(asyncResult: result);

                if (bytesRead > 0)
                {
                    switch (BitConverter.ToUInt16(value: _buffer, startIndex: 0))
                    {
                        case PingRequest:
                            _clientSocket.Send(buffer: BitConverter.GetBytes(value: PingResponse));
                            goto CONT_RECEIVE;

                        case PingResponse:
                            _running = false;
                            return;
                    }

                    LastReceivedMessageTime = DateTime.Now;

                    // Copy received bytes to a new byte array
                    var receivedBytes = new byte[bytesRead];
                    Array.Copy(sourceArray: _buffer, destinationArray: receivedBytes, length: bytesRead);

                    // Read messages according to current wire protocol and raise MessageReceived
                    // event for all received messages
                    foreach (var message in WireProtocol.CreateMessages(receivedBytes: receivedBytes))
                        OnMessageReceived(message: message, receivedTimestamp: DateTime.Now);
                }
                else
                {
                    Logger.Warn(data: Language.Instance.GetMessageFromKey(key: "CLIENT_DISCONNECTED"));
                    Disconnect();
                }

            CONT_RECEIVE:
                // Read more bytes if still running
                if (_running)
                    _clientSocket.BeginReceive(buffer: _buffer, offset: 0, size: _buffer.Length, socketFlags: 0,
                        callback: ReceiveCallback, state: null);
            }
            catch (Exception)
            {
                Disconnect();
            }
        }

        void SendByPriority(ConcurrentQueue<byte[]> buffer)
        {
            IEnumerable<byte> outgoingPacket = new List<byte>();

            // send max 30 packets at once
            for (var i = 0; i < 30; i++)
                if (buffer.TryDequeue(result: out var message) && message != null)
                    outgoingPacket = outgoingPacket.Concat(second: message);
                else
                    break;

            if (outgoingPacket.Any())
                _clientSocket.BeginSend(buffer: outgoingPacket.ToArray(), offset: 0, size: outgoingPacket.Count(),
                    socketFlags: SocketFlags.None,
                    callback: SendCallback, state: _clientSocket);
        }

        #endregion
    }
}