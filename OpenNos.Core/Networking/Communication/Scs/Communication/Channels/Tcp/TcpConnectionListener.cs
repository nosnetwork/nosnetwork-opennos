﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using OpenNos.Core.Networking.Communication.Scs.Communication.EndPoints.Tcp;

namespace OpenNos.Core.Networking.Communication.Scs.Communication.Channels.Tcp
{
    /// <summary>
    ///     This class is used to listen and accept incoming TCP connection requests on a TCP port.
    /// </summary>
    public class TcpConnectionListener : ConnectionListenerBase
    {
        #region Instantiation

        /// <summary>
        ///     Creates a new TcpConnectionListener for given endpoint.
        /// </summary>
        /// <param name="endPoint">The endpoint address of the server to listen incoming connections</param>
        public TcpConnectionListener(ScsTcpEndPoint endPoint)
        {
            _endPoint = endPoint;
        }

        #endregion

        #region Members

        /// <summary>
        ///     The endpoint address of the server to listen incoming connections.
        /// </summary>
        readonly ScsTcpEndPoint _endPoint;

        /// <summary>
        ///     Server socket to listen incoming connection requests.
        /// </summary>
        TcpListener _listenerSocket;

        /// <summary>
        ///     A flag to control thread's running
        /// </summary>
        volatile bool _running;

        /// <summary>
        ///     The thread to listen socket
        /// </summary>
        Thread _thread;

        #endregion

        #region Methods

        /// <summary>
        ///     Starts listening incoming connections.
        /// </summary>
        public override void Start()
        {
            StartSocket();
            _running = true;
            _thread = new Thread(start: ListenThread);
            _thread.Start();
        }

        /// <summary>
        ///     Stops listening incoming connections.
        /// </summary>
        public override void Stop()
        {
            _running = false;
            StopSocket();
        }

        /// <summary>
        ///     Entrance point of the thread. This method is used by the thread to listen incoming requests.
        /// </summary>
        void ListenThread()
        {
            TcpCommunicationChannel tcpChannel = null;
            while (_running)
                try
                {
                    var clientSocket = _listenerSocket.AcceptSocket();
                    if (clientSocket.Connected)
                    {
                        tcpChannel = new TcpCommunicationChannel(clientSocket: clientSocket);
                        OnCommunicationChannelConnected(client: tcpChannel);
                    }
                }
                catch (Exception)
                {
                    // Disconnect, wait for a while and connect again.
                    StopSocket();
                    Thread.Sleep(millisecondsTimeout: 1000);
                    if (!_running) return;
                    try
                    {
                        StartSocket();
                    }
                    catch (Exception)
                    {
                        // do nothing
                    }
                }

            if (tcpChannel != null) tcpChannel.Dispose();
        }

        /// <summary>
        ///     Starts listening socket.
        /// </summary>
        void StartSocket()
        {
            _listenerSocket = new TcpListener(localaddr: _endPoint.IpAddress ?? IPAddress.Any, port: _endPoint.TcpPort);
            _listenerSocket.Start();
        }

        /// <summary>
        ///     Stops listening socket.
        /// </summary>
        void StopSocket()
        {
            try
            {
                _listenerSocket.Stop();
            }
            catch (Exception)
            {
                // do nothing
            }
        }

        #endregion
    }
}