﻿using OpenNos.Core.Networking.Communication.Scs.Communication.Protocols;

namespace OpenNos.Core.Networking
{
    public class WireProtocolFactory : IScsWireProtocolFactory
    {
        #region Methods

        public IScsWireProtocol CreateWireProtocol()
        {
            return new WireProtocol();
        }

        #endregion
    }
}