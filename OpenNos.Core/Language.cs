﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;

namespace OpenNos.Core
{
    public class Language
    {
        #region Instantiation

        Language()
        {
            try
            {
                _streamWriter = new StreamWriter(path: "MissingLanguage.txt", append: true)
                {
                    AutoFlush = true
                };
            }
            catch
            {
                // ignored
            }

            _resourceCulture = new CultureInfo(name: ConfigurationManager.AppSettings[name: nameof(Language)]);

            if (Assembly.GetEntryAssembly() != null)
                _manager = new ResourceManager(
                    baseName: Assembly.GetEntryAssembly()?.GetName().Name + ".Resource.LocalizedResources",
                    assembly: Assembly.GetEntryAssembly() ?? throw new InvalidOperationException());
        }

        #endregion

        #region Properties

        public static Language Instance
        {
            get => _instance ?? (_instance = new Language());
        }

        #endregion

        #region Methods

        public string GetMessageFromKey(string key)
        {
            return _language.GetOrAdd(key: key, valueFactory: name =>
            {
                var value = _manager?.GetString(name: name, culture: _resourceCulture);

                if (string.IsNullOrEmpty(value: value))
                {
                    _streamWriter?.WriteLine(value: name);
                    return "none";
                }

                return value;
            });
        }

        /*public string GetMessageFromergenisString(string v, string ergebnisString)
        {
            return _language.GetOrAdd(ergebnisString, name =>
            {
                string value = _manager?.GetString(name, _resourceCulture);

                if (string.IsNullOrEmpty(value))
                {
                    _streamWriter?.WriteLine(name);
                    return "none";
                }

                return value;
            });
        }*/

        #endregion

        #region Members

        static Language _instance;

        readonly CultureInfo _resourceCulture;

        readonly ResourceManager _manager;

        readonly StreamWriter _streamWriter;

        readonly ConcurrentDictionary<string, string> _language = new ConcurrentDictionary<string, string>();

        #endregion
    }
}