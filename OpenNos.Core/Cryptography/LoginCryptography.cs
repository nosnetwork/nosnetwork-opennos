using System;
using System.Text;

namespace OpenNos.Core.Cryptography
{
    public class LoginCryptography : CryptographyBase
    {
        #region Instantiation

        public LoginCryptography() : base(hasCustomParameter: false)
        {
        }

        #endregion

        #region Methods

        public static string GetPassword(string password)
        {
            var equal = password.Length % 2 == 0;
            var str = equal ? password.Remove(startIndex: 0, count: 3) : password.Remove(startIndex: 0, count: 4);
            var decryptpass = new StringBuilder();

            for (var i = 0; i < str.Length; i += 2) decryptpass.Append(value: str[index: i]);
            if (decryptpass.Length % 2 != 0)
            {
                str = password.Remove(startIndex: 0, count: 2);
                decryptpass = decryptpass.Clear();
                for (var i = 0; i < str.Length; i += 2) decryptpass.Append(value: str[index: i]);
            }

            var passwd = new StringBuilder();
            for (var i = 0; i < decryptpass.Length; i += 2)
                passwd.Append(value: Convert.ToChar(
                    value: Convert.ToUInt32(value: decryptpass.ToString().Substring(startIndex: i, length: 2),
                        fromBase: 16)));
            return passwd.ToString();
        }

        public override string Decrypt(byte[] data, int sessionId = 0)
        {
            try
            {
                var builder = new StringBuilder();
                foreach (var character in data)
                    if (character > 14)
                        builder.Append(value: Convert.ToChar(value: character - 15 ^ 195));
                    else
                        builder.Append(value: Convert.ToChar(value: 256 - (15 - character) ^ 195));
                return builder.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        public override string DecryptCustomParameter(byte[] data)
        {
            throw new NotImplementedException();
        }

        public override byte[] Encrypt(string data)
        {
            try
            {
                data += " ";
                var tmp = Encoding.Default.GetBytes(s: data);
                for (var i = 0; i < data.Length; i++) tmp[i] = Convert.ToByte(value: tmp[i] + 15);
                tmp[tmp.Length - 1] = 25;
                return tmp;
            }
            catch
            {
                return new byte[0];
            }
        }

        #endregion
    }
}