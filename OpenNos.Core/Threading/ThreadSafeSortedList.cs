﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace OpenNos.Core.Threading
{
    /// <summary>
    ///     This class is used to store key-value based items in a thread safe manner. It uses
    ///     <see cref="SortedList" /> publicly.
    /// </summary>
    /// <typeparam name="Tk">Key type</typeparam>
    /// <typeparam name="Tv">Value type</typeparam>
    public class ThreadSafeSortedList<Tk, Tv> : IDisposable
    {
        #region Instantiation

        /// <summary>
        ///     Creates a new ThreadSafeSortedList object.
        /// </summary>
        public ThreadSafeSortedList()
        {
            _items = new SortedList<Tk, Tv>();
            _lock = new ReaderWriterLockSlim(recursionPolicy: LockRecursionPolicy.NoRecursion);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets count of items in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                if (!_disposed)
                {
                    _lock.EnterReadLock();
                    try
                    {
                        return _items.Count;
                    }
                    finally
                    {
                        _lock.ExitReadLock();
                    }
                }

                return 0;
            }
        }

        #endregion

        #region Indexers

        /// <summary>
        ///     Gets/adds/replaces an item by key.
        /// </summary>
        /// <param name="key">Key to get/set value</param>
        /// <returns>Item associated with this key</returns>
        public Tv this[Tk key]
        {
            get
            {
                if (!_disposed)
                {
                    _lock.EnterReadLock();
                    try
                    {
                        return _items.ContainsKey(key: key) ? _items[key: key] : default;
                    }
                    finally
                    {
                        _lock.ExitReadLock();
                    }
                }

                return default;
            }

            set
            {
                if (!_disposed)
                {
                    _lock.EnterWriteLock();
                    try
                    {
                        _items[key: key] = value;
                    }
                    finally
                    {
                        _lock.ExitWriteLock();
                    }
                }
            }
        }

        #endregion

        #region Members

        /// <summary>
        ///     private collection to store _items.
        /// </summary>
        readonly SortedList<Tk, Tv> _items;

        /// <summary>
        ///     Used to synchronize access to _items list.
        /// </summary>
        readonly ReaderWriterLockSlim _lock;

        bool _disposed;

        #endregion

        #region Methods

        /// <summary>
        ///     Determines whether all elements of a sequence satisfy a condition.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns><see cref="bool" /> True; if elements satisgy the condition</returns>
        public bool All(Func<Tv, bool> predicate)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.All(predicate: predicate);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return false;
        }

        /// <summary>
        ///     Determines whether any element of a sequence satisfies a condition.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>
        ///     <see cref="bool" />
        /// </returns>
        public bool Any(Func<Tv, bool> predicate)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Any(predicate: predicate);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return false;
        }

        /// <summary>
        ///     Removes all items from list.
        /// </summary>
        public void ClearAll()
        {
            if (!_disposed)
            {
                _lock.EnterWriteLock();
                try
                {
                    _items.Clear();
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
            }
        }

        /// <summary>
        ///     Checks if collection contains spesified key.
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns><see cref="bool" /> True; if collection contains given key</returns>
        public bool ContainsKey(Tk key)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.ContainsKey(key: key);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return false;
        }

        /// <summary>
        ///     Checks if collection contains spesified item.
        /// </summary>
        /// <param name="item">Item to check</param>
        /// <returns><see cref="bool" /> True; if collection contains given item</returns>
        public bool ContainsValue(Tv item)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.ContainsValue(value: item);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return false;
        }

        /// <summary>
        ///     Returns a number that represents how many elements in the specified sequence satisfy a condition.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns><see cref="int" /> number of found elements</returns>
        public int CountLinq(Func<Tv, bool> predicate)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Count(predicate: predicate);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return 0;
        }

        /// <summary>
        ///     Disposes the current object.
        /// </summary>
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                Dispose(disposing: true);
                GC.SuppressFinalize(obj: this);
            }
        }

        /// <summary>
        ///     Returns the first element of the sequence that satisfies a condition or a default value
        ///     if no such element is found.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns><see cref="Tv" /> object</returns>
        public Tv FirstOrDefault(Func<Tv, bool> predicate)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.FirstOrDefault(predicate: predicate);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return default;
        }

        /// <summary>
        ///     Performs the specified action on each element of the <see cref="List{T}" />.
        /// </summary>
        /// <param name="action"></param>
        public void ForEach(Action<Tv> action)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    _items.Values.ToList().ForEach(action: action);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        /// <summary>
        ///     Gets all items in collection.
        /// </summary>
        /// <returns>
        ///     <see cref="List{TV}" />
        /// </returns>
        public List<Tv> GetAllItems()
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return new List<Tv>(collection: _items.Values);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return new List<Tv>();
        }

        /// <summary>
        ///     Gets then removes all items in collection.
        /// </summary>
        /// <returns>
        ///     <see cref="List{TV}" />
        /// </returns>
        public List<Tv> GetAndClearAllItems()
        {
            if (!_disposed)
            {
                _lock.EnterWriteLock();
                try
                {
                    var list = new List<Tv>(collection: _items.Values);
                    _items.Clear();
                    return list;
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
            }

            return new List<Tv>();
        }

        /// <summary>
        ///     Returns the last element of a sequence that satisfies a specified condition.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns><see cref="Tv" /> object</returns>
        public Tv Last(Func<Tv, bool> predicate)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Last(predicate: predicate);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return default;
        }

        /// <summary>
        ///     Returns the last element of a sequence.
        /// </summary>
        /// <returns><see cref="Tv" /> object</returns>
        public Tv Last()
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Last();
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return default;
        }

        /// <summary>
        ///     Returns the last element of a sequence that satisfies a condition or a default value if
        ///     no such element is found.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns><see cref="Tv" /> object</returns>
        public Tv LastOrDefault(Func<Tv, bool> predicate)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.LastOrDefault(predicate: predicate);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return default;
        }

        /// <summary>
        ///     Returns the last element of a sequence, or a default value if the sequence contains no elements.
        /// </summary>
        /// <returns><see cref="Tv" /> object</returns>
        public Tv LastOrDefault()
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.LastOrDefault();
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return default;
        }

        /// <summary>
        ///     Removes an item from collection.
        /// </summary>
        /// <param name="key">Key of item to remove</param>
        /// <returns><see cref="bool" /> if removed</returns>
        public bool Remove(Tk key)
        {
            if (!_disposed)
            {
                _lock.EnterWriteLock();
                try
                {
                    if (!_items.ContainsKey(key: key)) return false;

                    _items.Remove(key: key);
                    return true;
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
            }

            return false;
        }

        /// <summary>
        ///     Removes an item from collection.
        /// </summary>
        /// <param name="value">Value of item to remove</param>
        /// <returns><see cref="bool" /> if removed</returns>
        public bool Remove(Tv value)
        {
            if (!_disposed)
            {
                _lock.EnterWriteLock();
                try
                {
                    if (!_items.ContainsValue(value: value)) return false;

                    _items.RemoveAt(index: _items.IndexOfValue(value: value));
                    return true;
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
            }

            return false;
        }

        /// <summary>
        ///     Projects each element of a sequence into a new form.
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="selector"></param>
        public IEnumerable<TResult> Select<TResult>(Func<Tv, TResult> selector)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Select(selector: selector);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return default;
        }

        /// <summary>
        ///     Returns the only element of a sequence that satisfies a specified condition, and throws
        ///     an exception if more than one such element exists.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns><see cref="Tv" /> object</returns>
        public Tv Single(Func<Tv, bool> predicate)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Single(predicate: predicate);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return default;
        }

        /// <summary>
        ///     Returns the only element of a sequence that satisfies a specified condition or a default
        ///     value if no such element exists; this method throws an exception if more than one element
        ///     satisfies the condition.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns><see cref="Tv" /> object</returns>
        public Tv SingleOrDefault(Func<Tv, bool> predicate)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.SingleOrDefault(predicate: predicate);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return default;
        }

        /// <summary>
        ///     Returns a number that represents how many elements in the specified sequence satisfy a condition.
        /// </summary>
        /// <param name="selector"></param>
        /// <returns><see cref="int" /> number of found elements</returns>
        public int Sum(Func<Tv, int> selector)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Sum(selector: selector);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return 0;
        }

        /// <summary>
        ///     Returns a number that represents how many elements in the specified sequence satisfy a condition.
        /// </summary>
        /// <param name="selector"></param>
        /// <returns>int? number of found elements</returns>
        public int? Sum(Func<Tv, int?> selector)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Sum(selector: selector);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return 0;
        }

        /// <summary>
        ///     Returns a number that represents how many elements in the specified sequence satisfy a condition.
        /// </summary>
        /// <param name="selector"></param>
        /// <returns><see cref="long" /> number of found elements</returns>
        public long Sum(Func<Tv, long> selector)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Sum(selector: selector);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return 0;
        }

        /// <summary>
        ///     Returns a number that represents how many elements in the specified sequence satisfy a condition.
        /// </summary>
        /// <param name="selector"></param>
        /// <returns>long? number of found elements</returns>
        public long? Sum(Func<Tv, long?> selector)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Sum(selector: selector);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return 0;
        }

        /// <summary>
        ///     Returns a number that represents how many elements in the specified sequence satisfy a condition.
        /// </summary>
        /// <param name="selector"></param>
        /// <returns><see cref="double" /> number of found elements</returns>
        public double Sum(Func<Tv, double> selector)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Sum(selector: selector);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return 0;
        }

        /// <summary>
        ///     Returns a number that represents how many elements in the specified sequence satisfy a condition.
        /// </summary>
        /// <param name="selector"></param>
        /// <returns>double? number of found elements</returns>
        public double? Sum(Func<Tv, double?> selector)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return _items.Values.Sum(selector: selector);
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return 0;
        }

        /// <summary>
        ///     Filters a sequence of values based on a predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns>
        ///     <see cref="List{TV}" />
        /// </returns>
        public List<Tv> Where(Func<Tv, bool> predicate)
        {
            if (!_disposed)
            {
                _lock.EnterReadLock();
                try
                {
                    return new List<Tv>(collection: _items.Values.Where(predicate: predicate));
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }

            return new List<Tv>();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                ClearAll();
                _lock.Dispose();
            }
        }

        #endregion
    }
}