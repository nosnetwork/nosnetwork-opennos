﻿using System;
using System.Threading;

namespace OpenNos.Core.Threading
{
    /// <summary>
    ///     This class is a timer that performs some tasks periodically.
    /// </summary>
    public class Timer : IDisposable
    {
        #region Events

        /// <summary>
        ///     This event is raised periodically according to Period of Timer.
        /// </summary>
        public event EventHandler Elapsed;

        #endregion

        #region Members

        readonly object _lock = new object();

        /// <summary>
        ///     This timer is used to perfom the task at spesified intervals.
        /// </summary>
        readonly System.Threading.Timer _taskTimer;

        bool _disposed;

        /// <summary>
        ///     Indicates that whether performing the task or _taskTimer is in sleep mode. This field is
        ///     used to wait executing tasks when stopping Timer.
        /// </summary>
        volatile bool _performingTasks;

        /// <summary>
        ///     Indicates that whether timer is running or stopped.
        /// </summary>
        volatile bool _running;

        #endregion

        #region Instantiation

        /// <summary>
        ///     Creates a new Timer.
        /// </summary>
        /// <param name="period">Task period of timer (as milliseconds)</param>
        public Timer(int period) : this(period: period, runOnStart: false)
        {
        }

        /// <summary>
        ///     Creates a new Timer.
        /// </summary>
        /// <param name="period">Task period of timer (as milliseconds)</param>
        /// <param name="runOnStart">
        ///     Indicates whether timer raises Elapsed event on Start method of Timer for once
        /// </param>
        public Timer(int period, bool runOnStart)
        {
            Period = period;
            RunOnStart = runOnStart;
            _taskTimer = new System.Threading.Timer(callback: TimerCallBack, state: null, dueTime: Timeout.Infinite,
                period: Timeout.Infinite);
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Task period of timer (as milliseconds).
        /// </summary>
        public int Period { get; set; }

        /// <summary>
        ///     Indicates whether timer raises Elapsed event on Start method of Timer for once.
        ///     Default: False.
        /// </summary>
        public bool RunOnStart { get; set; }

        #endregion

        #region Methods

        public void Dispose()
        {
            if (!_disposed)
            {
                Dispose(disposing: true);
                GC.SuppressFinalize(obj: this);
                _disposed = true;
            }
        }

        /// <summary>
        ///     Starts the timer.
        /// </summary>
        public void Start()
        {
            _running = true;
            _taskTimer.Change(dueTime: RunOnStart ? 0 : Period, period: Timeout.Infinite);
        }

        /// <summary>
        ///     Stops the timer.
        /// </summary>
        public void Stop()
        {
            lock (_lock)
            {
                _running = false;
                _taskTimer.Change(dueTime: Timeout.Infinite, period: Timeout.Infinite);
            }
        }

        /// <summary>
        ///     Waits the service to stop.
        /// </summary>
        public void WaitToStop()
        {
            lock (_lock)
            {
                while (_performingTasks) Monitor.Wait(obj: _taskTimer);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Stop();
                _taskTimer.Dispose();
            }
        }

        /// <summary>
        ///     This method is called by _taskTimer.
        /// </summary>
        /// <param name="state">Not used argument</param>
        void TimerCallBack(object state)
        {
            lock (_lock)
            {
                if (!_running || _performingTasks) return;

                _taskTimer.Change(dueTime: Timeout.Infinite, period: Timeout.Infinite);
                _performingTasks = true;
            }

            try
            {
                Elapsed?.Invoke(sender: this, e: EventArgs.Empty);
            }
            finally
            {
                lock (_lock)
                {
                    _performingTasks = false;
                    if (_running) _taskTimer.Change(dueTime: Period, period: Timeout.Infinite);

                    Monitor.Pulse(obj: _taskTimer);
                }
            }
        }

        #endregion
    }
}