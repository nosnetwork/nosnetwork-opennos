﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.Core.Threading
{
    public class ThreadSafeGenericLockedList<T>
    {
        #region Members

        readonly List<T> _internalList;

        #endregion

        #region Instantiation

        public ThreadSafeGenericLockedList()
        {
            _internalList = new List<T>();
        }

        public ThreadSafeGenericLockedList(List<T> other)
        {
            _internalList = other.ToList();
        }

        #endregion

        #region Interface Implementation

        public T this[int index]
        {
            get
            {
                lock (_internalList)
                {
                    return _internalList[index: index];
                }
            }

            set
            {
                lock (_internalList)
                {
                    _internalList[index: index] = value;
                }
            }
        }

        public int Count
        {
            get
            {
                lock (_internalList)
                {
                    return _internalList.Count;
                }
            }
        }

        public bool IsReadOnly
        {
            get => false;
        }

        public void Add(T item)
        {
            lock (_internalList)
            {
                _internalList.Add(item: item);
            }
        }

        public void Clear()
        {
            lock (_internalList)
            {
                _internalList.Clear();
            }
        }

        public bool Contains(T item)
        {
            lock (_internalList)
            {
                return _internalList.Contains(item: item);
            }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            lock (_internalList)
            {
                _internalList.CopyTo(array: array, arrayIndex: arrayIndex);
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ToList().GetEnumerator();
        }

        public int IndexOf(T item)
        {
            lock (_internalList)
            {
                return _internalList.IndexOf(item: item);
            }
        }

        public void Insert(int index, T item)
        {
            lock (_internalList)
            {
                _internalList.Insert(index: index, item: item);
            }
        }

        public bool Remove(T item)
        {
            lock (_internalList)
            {
                return _internalList.Remove(item: item);
            }
        }

        public void RemoveAt(int index)
        {
            lock (_internalList)
            {
                _internalList.RemoveAt(index: index);
            }
        }

        #endregion

        #region Methods

        public void AddRange(IEnumerable<T> collection)
        {
            lock (_internalList)
            {
                _internalList.AddRange(collection: collection);
            }
        }

        public bool Any(Func<T, bool> predicate)
        {
            lock (_internalList)
            {
                return _internalList.Any(predicate: predicate);
            }
        }

        public bool Any()
        {
            lock (_internalList)
            {
                return _internalList.Any();
            }
        }

        public ThreadSafeGenericLockedList<T> Clone()
        {
            lock (_internalList)
            {
                return new ThreadSafeGenericLockedList<T>(other: _internalList);
            }
        }

        public void Lock(Action action)
        {
            lock (_internalList)
            {
                action();
            }
        }

        public void ForEach(Action<T> action)
        {
            lock (_internalList)
            {
                _internalList.ForEach(action: action);
            }
        }

        public int RemoveAll(Predicate<T> match)
        {
            lock (_internalList)
            {
                return _internalList.RemoveAll(match: match);
            }
        }

        public List<T> ToList()
        {
            lock (_internalList)
            {
                return _internalList.ToList();
            }
        }

        public List<T> Where(Func<T, bool> predicate)
        {
            lock (_internalList)
            {
                return _internalList.Where(predicate: predicate).ToList();
            }
        }

        #endregion
    }
}