﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace OpenNos.Core.Threading
{
    /// <summary>
    ///     This class is used to process items sequentially in a multithreaded manner.
    /// </summary>
    /// <typeparam name="TItem">Type of item to process</typeparam>
    public class SequentialItemProcessor<TItem>
    {
        #region Instantiation

        /// <summary>
        ///     Creates a new SequentialItemProcessor object.
        /// </summary>
        /// <param name="processMethod">
        ///     The method delegate that is called to actually process items
        /// </param>
        public SequentialItemProcessor(Action<TItem> processMethod)
        {
            _processMethod = processMethod;
            _queue = new Queue<TItem>();
        }

        #endregion

        #region Members

        /// <summary>
        ///     The method delegate that is called to actually process items.
        /// </summary>
        readonly Action<TItem> _processMethod;

        /// <summary>
        ///     Item queue. Used to process items sequentially.
        /// </summary>
        readonly Queue<TItem> _queue;

        /// <summary>
        ///     An object to synchronize threads.
        /// </summary>
        readonly object _syncObj = new object();

        /// <summary>
        ///     Indicates state of the item processing.
        /// </summary>
        bool _isProcessing;

        /// <summary>
        ///     A boolean value to control running of SequentialItemProcessor.
        /// </summary>
        bool _isRunning;

        #endregion

        #region Methods

        public void ClearQueue()
        {
            _queue.Clear();
        }

        /// <summary>
        ///     Adds an item to queue to process the item.
        /// </summary>
        /// <param name="item">Item to add to the queue</param>
        public void EnqueueMessage(TItem item)
        {
            // Add the item to the queue and start a new Task if needed
            lock (_syncObj)
            {
                if (!_isRunning) return;

                _queue.Enqueue(item: item);

                if (!_isProcessing) ThreadPool.QueueUserWorkItem(callBack: ProcessItem);
            }
        }

        /// <summary>
        ///     Starts processing of items.
        /// </summary>
        public void Start()
        {
            _isRunning = true;
        }

        /// <summary>
        ///     Stops processing of items and waits stopping of current item.
        /// </summary>
        public void Stop()
        {
            _isRunning = false;

            //Clear all incoming messages
            lock (_syncObj)
            {
                _queue.Clear();
            }
        }

        /// <summary>
        ///     This method runs on a new seperated Task (thread) to process items on the queue.
        /// </summary>
        /// <param name="state">todo: describe state parameter on processItem</param>
        void ProcessItem(object state)
        {
            //Try to get an item from queue to process it.
            TItem itemToProcess;
            lock (_syncObj)
            {
                if (!_isRunning || _isProcessing) return;

                if (_queue.Count <= 0) return;

                _isProcessing = true;
                itemToProcess = _queue.Dequeue();
            }

            try
            {
                //Process the item (by calling the _processMethod delegate)
                _processMethod(obj: itemToProcess);
            }
            catch (Exception)
            {
                // do nothing
            }

            //Process next item if available
            lock (_syncObj)
            {
                _isProcessing = false;
                if (!_isRunning || _queue.Count <= 0) return;

                //Start a new task
                ThreadPool.QueueUserWorkItem(callBack: ProcessItem);
            }
        }

        #endregion
    }
}