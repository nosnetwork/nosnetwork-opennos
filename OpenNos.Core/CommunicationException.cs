﻿using System;
using System.Runtime.Serialization;

namespace OpenNos.Core.ExceptionExtensions
{
    /// <summary>
    ///     Defines a CommunicationException thrown mostly by TcpConnections
    /// </summary>
    public class CommunicationException : Exception
    {
        #region Instantiation

        public CommunicationException()
        {
        }

        public CommunicationException(string message) : base(nameof(CommunicationException) + message)
        {
        }

        public CommunicationException(string message, Exception innerException) : base(
            nameof(CommunicationException) + message, innerException)
        {
        }

        protected CommunicationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        #endregion
    }
}