﻿using System.Collections.Generic;

namespace OpenNos.Core.Extensions
{
    public class ReferenceEqualityComparer : EqualityComparer<object>
    {
        #region Methods

        public override bool Equals(object x, object y)
        {
            return ReferenceEquals(x, y);
        }

        public override int GetHashCode(object obj)
        {
            if (obj == null) return 0;
            return obj.GetHashCode();
        }

        #endregion
    }
}