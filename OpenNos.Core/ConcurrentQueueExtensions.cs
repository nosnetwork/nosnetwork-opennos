﻿using System.Collections.Concurrent;

namespace OpenNos.Core.ConcurrencyExtensions
{
    static class ConcurrentQueueExtensions
    {
        #region Methods

        public static void Clear<T>(this ConcurrentQueue<T> queue)
        {
            while (queue.TryDequeue(out _))
            {
                // do nothing
            }
        }

        #endregion
    }
}