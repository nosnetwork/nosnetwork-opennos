﻿namespace OpenNos.Core
{
    public class Singleton<T> where T : class, new()
    {
        #region Members

        static T _instance;

        #endregion

        #region Properties

        public static T Instance
        {
            get => _instance ?? (_instance = new T());
        }

        #endregion
    }
}