﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace OpenNos.Core
{
    public static class DelegateBuilder
    {
        #region Methods

        public static T BuildDelegate<T>(MethodInfo method, params object[] missingParamValues)
        {
            var queueMissingParams = new Queue<object>(collection: missingParamValues);
            var dgtMi = typeof(T).GetMethod(name: "Invoke");
            Debug.Assert(condition: dgtMi != null, message: nameof(dgtMi) + " != null");
            var dgtParams = dgtMi.GetParameters();
            var paramsOfDelegate = dgtParams
                .Select(selector: tp => Expression.Parameter(type: tp.ParameterType, name: tp.Name)).ToArray();
            var methodParams = method.GetParameters();
            if (method.IsStatic)
            {
                var paramsToPass = methodParams
                    .Select(selector: (p, i) => CreateParam(paramsOfDelegate: paramsOfDelegate, index: i,
                        callParamType: p, queueMissingParams: queueMissingParams)).ToArray();
                var expr = Expression.Lambda<T>(body: Expression.Call(method: method, arguments: paramsToPass),
                    parameters: paramsOfDelegate);
                return expr.Compile();
            }
            else
            {
                var paramThis = Expression.Convert(expression: paramsOfDelegate[0],
                    type: method.DeclaringType ?? throw new InvalidOperationException());
                var paramsToPass = methodParams
                    .Select(selector: (p, i) => CreateParam(paramsOfDelegate: paramsOfDelegate, index: i + 1,
                        callParamType: p, queueMissingParams: queueMissingParams)).ToArray();
                var expr = Expression.Lambda<T>(
                    body: Expression.Call(instance: paramThis, method: method, arguments: paramsToPass),
                    parameters: paramsOfDelegate);
                return expr.Compile();
            }
        }

        static Expression CreateParam(ParameterExpression[] paramsOfDelegate, int index,
            ParameterInfo callParamType, Queue<object> queueMissingParams)
        {
            if (index < paramsOfDelegate.Length)
                return Expression.Convert(expression: paramsOfDelegate[index], type: callParamType.ParameterType);

            if (queueMissingParams.Count > 0) return Expression.Constant(value: queueMissingParams.Dequeue());

            if (callParamType.ParameterType.IsValueType)
                return Expression.Constant(value: Activator.CreateInstance(type: callParamType.ParameterType));

            return Expression.Constant(value: null);
        }

        #endregion
    }
}