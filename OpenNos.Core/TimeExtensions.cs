﻿using System;

namespace OpenNos.Core.Extensions
{
    public static class TimeExtensions
    {
        #region Methods

        public static DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime((dt.Ticks + d.Ticks - 1) / d.Ticks * d.Ticks);
        }

        #endregion
    }
}