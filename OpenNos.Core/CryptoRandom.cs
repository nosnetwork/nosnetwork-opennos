﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;

namespace OpenNos.Core
{
    /// <summary>
    ///     A random number generator based on the RNGCryptoServiceProvider. Adapted from the "Tales from
    ///     the CryptoRandom" article in MSDN Magazine (September 2007) but with explicit guarantee to be
    ///     thread safe. Note that this implementation also includes an optional (enabled by default)
    ///     random buffer which provides a significant speed boost as it greatly reduces the amount of
    ///     calls into unmanaged land.
    /// </summary>
    public class CryptoRandom : Random, IDisposable
    {
        #region Members

        readonly object _lockObject = new object();

        readonly object _lockObject2 = new object();

        readonly RNGCryptoServiceProvider _rng = new RNGCryptoServiceProvider();

        byte[] _buffer;

        int _bufferPosition;

        bool _disposed;

        #endregion

        #region Instantiation

        /// <summary>
        ///     Initializes a new instance of the <see cref="CryptoRandom" /> class with. Using this
        ///     overload will enable the random buffer pool.
        /// </summary>
        public CryptoRandom() : this(enableRandomPool: true)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CryptoRandom" /> class. This method will
        ///     disregard whatever value is passed as seed and it's only implemented in order to be fully
        ///     backwards compatible with <see cref="System.Random" />. Using this overload will enable
        ///     the random buffer pool.
        /// </summary>
        /// <param name="ignoredSeed">The ignored seed.</param>
        [SuppressMessage(category: "Microsoft.Usage", checkId: "CA1801:ReviewUnusedParameters",
            MessageId = "ignoredSeed",
            Justification = "Cannot remove this parameter as we implement the full API of System.Random")]
        public CryptoRandom(int ignoredSeed) : this(enableRandomPool: true)
        {
            IgnoredSeed = ignoredSeed;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="CryptoRandom" /> class with optional random buffer.
        /// </summary>
        /// <param name="enableRandomPool">
        ///     set to <c>true</c> to enable the random pool buffer for increased performance.
        /// </param>
        public CryptoRandom(bool enableRandomPool)
        {
            IsRandomPoolEnabled = enableRandomPool;
        }

        #endregion

        #region Properties

        public int IgnoredSeed { get; }

        /// <summary>
        ///     Gets a value indicating whether this instance has random pool enabled.
        /// </summary>
        /// <value><c>true</c> if this instance has random pool enabled; otherwise, <c>false</c>.</value>
        public bool IsRandomPoolEnabled { get; }

        #endregion

        #region Methods

        public void Dispose()
        {
            if (!_disposed)
            {
                Dispose(disposing: true);
                GC.SuppressFinalize(obj: this);
                _disposed = true;
            }
        }

        /// <summary>
        ///     Returns a nonnegative random number.
        /// </summary>
        /// <returns>
        ///     A 32-bit signed integer greater than or equal to zero and less than <see cref="F:System.Int32.MaxValue" />.
        /// </returns>
        public override int Next()
        {
            // Mask away the sign bit so that we always return nonnegative integers
            return (int)GetRandomUInt32() & 0x7FFFFFFF;
        }

        /// <summary>
        ///     Returns a nonnegative random number less than the specified maximum.
        /// </summary>
        /// <param name="maxValue">
        ///     The exclusive upper bound of the random number to be generated.
        ///     <paramref name="maxValue" />
        ///     must be greater than or equal to zero.
        /// </param>
        /// <returns>
        ///     A 32-bit signed integer greater than or equal to zero, and less than
        ///     <paramref name="maxValue" />; that is, the range of return values ordinarily includes zero but not
        ///     <paramref name="maxValue" />. However, if <paramref name="maxValue" /> equals zero,
        ///     <paramref name="maxValue" /> is returned.
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///     <paramref name="maxValue" /> is less than zero.
        /// </exception>
        public override int Next(int maxValue)
        {
            if (maxValue < 0) throw new ArgumentOutOfRangeException(paramName: nameof(maxValue));
            return Next(minValue: 0, maxValue: maxValue);
        }

        /// <summary>
        ///     Returns a random number within a specified range.
        /// </summary>
        /// <param name="minValue">The inclusive lower bound of the random number returned.</param>
        /// <param name="maxValue">
        ///     The exclusive upper bound of the random number returned. <paramref name="maxValue" /> must
        ///     be greater than or equal to <paramref name="minValue" />.
        /// </param>
        /// <returns>
        ///     A 32-bit signed integer greater than or equal to <paramref name="minValue" /> and less
        ///     than <paramref name="maxValue" />; that is, the range of return values includes
        ///     <paramref name="minValue" /> but not <paramref name="maxValue" />. If <paramref name="minValue" />
        ///     equals <paramref name="maxValue" />, <paramref name="minValue" /> is returned.
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        ///     <paramref name="minValue" /> is greater than <paramref name="maxValue" />.
        /// </exception>
        public override int Next(int minValue, int maxValue)
        {
            if (minValue > maxValue) throw new ArgumentOutOfRangeException(paramName: nameof(minValue));
            if (minValue == maxValue) return minValue;
            long diff = maxValue - minValue;
            while (true)
            {
                var rand = GetRandomUInt32();
                const long max = 1 + (long)uint.MaxValue;
                var remainder = max % diff;
                if (rand < max - remainder) return (int)(minValue + rand % diff);
            }
        }

        /// <summary>
        ///     Fills the elements of a specified array of bytes with random numbers.
        /// </summary>
        /// <param name="buffer">An array of bytes to contain random numbers.</param>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="buffer" /> is null.</exception>
        public override void NextBytes(byte[] buffer)
        {
            if (buffer == null) throw new ArgumentNullException(paramName: nameof(buffer));

            lock (_lockObject)
            {
                if (IsRandomPoolEnabled && _buffer == null) InitBuffer();

                // Can we fit the requested number of bytes in the buffer?
                if (IsRandomPoolEnabled && _buffer.Length <= buffer.Length)
                {
                    var count = buffer.Length;
                    EnsureRandomBuffer(requiredBytes: count);
                    Buffer.BlockCopy(src: _buffer, srcOffset: _bufferPosition, dst: buffer, dstOffset: 0, count: count);
                    _bufferPosition += count;
                }
                else
                {
                    // Draw bytes directly from the RNGCryptoProvider
                    _rng.GetBytes(data: buffer);
                }
            }
        }

        /// <summary>
        ///     Returns a random number between 0.0 and 1.0.
        /// </summary>
        /// <returns>
        ///     A double-precision floating point number greater than or equal to 0.0, and less than 1.0.
        /// </returns>
        public override double NextDouble()
        {
            return GetRandomUInt32() / (1.0 + uint.MaxValue);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing) _rng.Dispose();
        }

        /// <summary>
        ///     Ensures that we have enough bytes in the random buffer.
        /// </summary>
        /// <param name="requiredBytes">The number of required bytes.</param>
        void EnsureRandomBuffer(int requiredBytes)
        {
            if (_buffer == null) InitBuffer();
            if (requiredBytes > _buffer.Length)
                throw new ArgumentOutOfRangeException(paramName: nameof(requiredBytes),
                    message: "cannot be greater than random buffer");
            if (_buffer.Length - _bufferPosition < requiredBytes) InitBuffer();
        }

        /// <summary>
        ///     Gets one random unsigned 32bit integer in a thread safe manner.
        /// </summary>
        uint GetRandomUInt32()
        {
            lock (_lockObject2)
            {
                EnsureRandomBuffer(requiredBytes: 4);
                var rand = BitConverter.ToUInt32(value: _buffer, startIndex: _bufferPosition);
                _bufferPosition += 4;
                return rand;
            }
        }

        void InitBuffer()
        {
            if (IsRandomPoolEnabled)
            {
                if (_buffer == null || _buffer.Length != 512) _buffer = new byte[512];
            }
            else if (_buffer == null || _buffer.Length != 4)
            {
                _buffer = new byte[4];
            }

            _rng.GetBytes(data: _buffer);
            _bufferPosition = 0;
        }

        #endregion
    }
}