﻿using System;
using System.Runtime.CompilerServices;
using log4net;

namespace OpenNos.Core
{
    public static class Logger
    {
        #region Properties

        public static ILog Log { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Wraps up the error message with the CallerMemberName
        /// </summary>
        /// <param name="data"></param>
        /// <param name="memberName"></param>
        public static void Debug(string data, [CallerMemberName] string memberName = "")
        {
            Log?.Debug(message: $"[{memberName}]: {data}");
        }

        /// <summary>
        ///     Wraps up the error message with the CallerMemberName
        /// </summary>
        /// <param name="memberName"></param>
        /// <param name="ex"></param>
        public static void Error(Exception ex, [CallerMemberName] string memberName = "")
        {
            Log?.Error(message: $"[{memberName}]: {ex.Message}", exception: ex);
        }

        /// <summary>
        ///     Wraps up the error message with the CallerMemberName
        /// </summary>
        /// <param name="data"></param>
        /// <param name="ex"></param>
        /// <param name="memberName"></param>
        public static void Error(string data, Exception ex = null, [CallerMemberName] string memberName = "")
        {
            if (ex != null)
                Log?.Error(message: $"[{memberName}]: {data} {ex.InnerException}", exception: ex);
            else
                Log?.Error(message: $"[{memberName}]: {data}");
        }

        /// <summary>
        ///     Wraps up the fatal message with the CallerMemberName
        /// </summary>
        /// <param name="data"></param>
        /// <param name="ex"></param>
        /// <param name="memberName"></param>
        public static void Fatal(string data, Exception ex = null, [CallerMemberName] string memberName = "")
        {
            if (ex != null)
                Log?.Fatal(message: $"[{memberName}]: {data} {ex.InnerException}", exception: ex);
            else
                Log?.Fatal(message: $"[{memberName}]: {data}");
        }

        /// <summary>
        ///     Wraps up the info message with the CallerMemberName
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        /// <param name="memberName"></param>
        public static void Info(string message, Exception ex = null, [CallerMemberName] string memberName = "")
        {
            if (ex != null)
                Log?.Info(message: $"[{memberName}]: {message}", exception: ex);
            else
                Log?.Info(message: $"[{memberName}]: {message}");
        }

        public static void InitializeLogger(ILog log)
        {
            Log = log;
        }

        /// <summary>
        ///     Wraps up the error message with the Logging Event
        /// </summary>
        /// <param name="logEvent"></param>
        /// <param name="data"></param>
        /// <param name="ex"></param>
        /// <param name="memberName"></param>
        public static void LogEvent(string logEvent, string data, Exception ex = null,
            [CallerMemberName] string memberName = "")
        {
            if (ex != null)
                Log?.Info(message: $"[{memberName}]: [{logEvent}]{data}");
            else
                Log?.Info(message: $"[{memberName}]: [{logEvent}]{data}", exception: null);
        }

        /// <summary>
        ///     Wraps up the error message with the Logging Event
        /// </summary>
        /// <param name="logEvent"></param>
        /// <param name="data"></param>
        /// <param name="ex"></param>
        /// <param name="memberName"></param>
        public static void LogEventError(string logEvent, string data, Exception ex = null,
            [CallerMemberName] string memberName = "")
        {
            if (ex != null)
                Log?.Error(message: $"[{memberName}]: [{logEvent}]{data}", exception: ex);
            else
                Log?.Error(message: $"[{memberName}]: [{logEvent}]{data}");
        }

        /// <summary>
        ///     Wraps up the error message with the Logging Event
        /// </summary>
        /// <param name="logEvent"></param>
        /// <param name="caller"></param>
        /// <param name="data"></param>
        public static void LogUserEvent(string logEvent, string caller, string data)
        {
            Log?.Info(message: $"[{logEvent}][{caller}]{data}");
        }

        /// <summary>
        ///     Wraps up the message with the CallerMemberName
        /// </summary>
        /// <param name="logEvent"></param>
        /// <param name="caller"></param>
        /// <param name="data"></param>
        public static void LogUserEventDebug(string logEvent, string caller, string data)
        {
            Log?.Debug(message: $"[{logEvent}][{caller}]{data}");
        }

        /// <summary>
        ///     Wraps up the error message with the Logging Event
        /// </summary>
        /// <param name="logEvent"></param>
        /// <param name="caller"></param>
        /// <param name="data"></param>
        /// <param name="ex"></param>
        public static void LogUserEventError(string logEvent, string caller, string data, Exception ex)
        {
            Log?.Error(message: $"[{logEvent}][{caller}]{data}", exception: ex);
        }

        /// <summary>
        ///     Wraps up the warn message with the CallerMemberName
        /// </summary>
        /// <param name="data"></param>
        /// <param name="innerException"></param>
        /// <param name="memberName"></param>
        public static void Warn(string data, Exception innerException = null, [CallerMemberName] string memberName = "")
        {
            if (innerException != null)
                Log?.Warn(message: $"[{memberName}]: {data} {innerException.InnerException}",
                    exception: innerException);
            else
                Log?.Warn(message: $"[{memberName}]: {data}");
        }

        #endregion
    }
}