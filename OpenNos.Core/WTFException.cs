﻿using System;
using System.Runtime.Serialization;

namespace OpenNos.Core.ExceptionExtensions
{
    /// <summary>
    ///     Defines a What a Terrible Fault Exception, which should actually never be thrown. "For
    ///     those times when: if (true == false) { Console.WriteLine("Logic is dead!"); } actually executes.."
    /// </summary>
    public class WTFException : Exception
    {
        #region Instantiation

        public WTFException()
        {
        }

        public WTFException(string message) : base("WTF!!?!??!!1one11! " + message)
        {
        }

        public WTFException(string message, Exception innerException) : base("WTF!!?!??!!1one11! " + message,
            innerException)
        {
        }

        protected WTFException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        #endregion
    }
}