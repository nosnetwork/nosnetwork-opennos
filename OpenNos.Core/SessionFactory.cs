﻿namespace OpenNos.Core
{
    public class SessionFactory
    {
        #region Instantiation

        SessionFactory()
        {
        }

        #endregion

        #region Properties

        public static SessionFactory Instance
        {
            get => _instance ?? (_instance = new SessionFactory());
        }

        #endregion

        #region Methods

        public int GenerateSessionId()
        {
            _sessionCounter += 2;
            return _sessionCounter;
        }

        #endregion

        #region Members

        static SessionFactory _instance;

        int _sessionCounter;

        #endregion
    }
}