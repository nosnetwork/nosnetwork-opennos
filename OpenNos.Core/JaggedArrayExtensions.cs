﻿namespace OpenNos.Core.ArrayExtensions
{
    public static class JaggedArrayExtensions
    {
        #region Methods

        public static T[][] CreateJaggedArray<T>(int xLenght, int yLength)
        {
            var array = new T[xLenght][];
            for (var i = 0; i < xLenght; i++) array[i] = new T[yLength];
            return array;
        }

        #endregion
    }
}