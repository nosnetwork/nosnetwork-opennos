﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using log4net;
using OpenNos.Core;
using OpenNos.DAL.EF.Helpers;

namespace OpenNos.Import.Console
{
    public static class Program
    {
        #region Methods

        public static void Main(string[] args)
        {
            var isDebug = false;
#if DEBUG
            isDebug = true;
#endif
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo(name: "en-US");
            System.Console.Title = $"OpenNos Import Console{(isDebug ? " Development Environment" : "")}";

            var ignoreStartupMessages = false;
            foreach (var arg in args) ignoreStartupMessages |= arg == "--nomsg";

            // initialize logger
            Logger.InitializeLogger(log: LogManager.GetLogger(type: typeof(Program)));

            if (!ignoreStartupMessages)
            {
                var assembly = Assembly.GetExecutingAssembly();
                var fileVersionInfo = FileVersionInfo.GetVersionInfo(fileName: assembly.Location);
                var text = $"IMPORT CONSOLE VERSION {fileVersionInfo.ProductVersion} by NN Team";
                var offset = System.Console.WindowWidth / 2 + text.Length / 2;
                var separator = new string(c: '=', count: System.Console.WindowWidth);
                System.Console.WriteLine(value: separator + string.Format(format: "{0," + offset + "}\n", arg0: text) +
                                                separator);
            }

            DataAccessHelper.Initialize();
            Logger.Warn(data: Language.Instance.GetMessageFromKey(key: "NEED_TREE"));
            System.Console.BackgroundColor = ConsoleColor.Blue;
            System.Console.WriteLine(value: "Root");
            System.Console.ResetColor();
            System.Console.WriteLine(
                value: $"-----_code_{ConfigurationManager.AppSettings[name: nameof(Language)]}_Card.txt");
            System.Console.WriteLine(
                value: $"-----_code_{ConfigurationManager.AppSettings[name: nameof(Language)]}_Item.txt");
            System.Console.WriteLine(
                value: $"-----_code_{ConfigurationManager.AppSettings[name: nameof(Language)]}_MapIDData.txt");
            System.Console.WriteLine(
                value: $"-----_code_{ConfigurationManager.AppSettings[name: nameof(Language)]}_monster.txt");
            System.Console.WriteLine(
                value: $"-----_code_{ConfigurationManager.AppSettings[name: nameof(Language)]}_Skill.txt");
            System.Console.WriteLine(value: "-----packet.txt");
            System.Console.WriteLine(value: "-----Card.dat");
            System.Console.WriteLine(value: "-----Item.dat");
            System.Console.WriteLine(value: "-----MapIDData.dat");
            System.Console.WriteLine(value: "-----monster.dat");
            System.Console.WriteLine(value: "-----Skill.dat");
            System.Console.BackgroundColor = ConsoleColor.Blue;
            System.Console.WriteLine(value: "-----map");
            System.Console.ResetColor();
            System.Console.WriteLine(value: "----------0");
            System.Console.WriteLine(value: "----------1");
            System.Console.WriteLine(value: "----------...");

            try
            {
                Logger.Warn(data: Language.Instance.GetMessageFromKey(key: "ENTER_PATH"));

                var folder = System.Console.ReadLine();
                System.Console.WriteLine(value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_ALL")} [Y/n]");
                var key = System.Console.ReadKey(intercept: true);

                ImportFactory factory = null;
                if (!string.IsNullOrWhiteSpace(value: folder))
                    factory = new ImportFactory(folder: folder);
                else
                    factory = new ImportFactory(folder: Directory.GetCurrentDirectory() + "/parser");

                factory.ImportPackets();
                if (key.KeyChar != 'n')
                {
                    factory.ImportMaps();
                    factory.LoadMaps();
                    ImportFactory.ImportRespawnMapType();
                    ImportFactory.ImportMapType();
                    ImportFactory.ImportMapTypeMap();
                    ImportFactory.ImportAccounts();
                    factory.ImportPortals();
                    factory.ImportScriptedInstances();
                    factory.ImportItems();
                    factory.ImportSkills();
                    factory.ImportCards();
                    factory.ImportNpcMonsters();
                    factory.ImportNpcMonsterData();
                    factory.ImportMapNpcs();
                    factory.ImportMonsters();
                    factory.ImportShops();
                    factory.ImportTeleporters();
                    factory.ImportShopItems();
                    factory.ImportShopSkills();
                    factory.ImportRecipe();
                    factory.ImportHardcodedItemRecipes();
                    factory.ImportQuests();
                }
                else
                {
                    System.Console.WriteLine(value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_MAPS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n')
                    {
                        factory.ImportMaps();
                        factory.LoadMaps();
                    }

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_MAPTYPES")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n')
                    {
                        ImportFactory.ImportMapType();
                        ImportFactory.ImportMapTypeMap();
                    }

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_ACCOUNTS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') ImportFactory.ImportAccounts();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_PORTALS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportPortals();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_TIMESPACES")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportScriptedInstances();

                    System.Console.WriteLine(value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_ITEMS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportItems();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_SKILLS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportSkills();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_MONSTERS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportNpcMonsters();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_NPCMONSTERDATA")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportNpcMonsterData();

                    System.Console.WriteLine(value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_CARDS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportCards();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_MAPNPCS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportMapNpcs();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_MAPMONSTERS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportMonsters();

                    System.Console.WriteLine(value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_SHOPS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportShops();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_TELEPORTERS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportTeleporters();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_SHOPITEMS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportShopItems();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_SHOPSKILLS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportShopSkills();

                    System.Console.WriteLine(
                        value: $"{Language.Instance.GetMessageFromKey(key: "PARSE_RECIPES")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n')
                    {
                        factory.ImportRecipe();
                        factory.ImportHardcodedItemRecipes();
                    }

                    System.Console.WriteLine(
                        value: $@"{Language.Instance.GetMessageFromKey(key: "PARSE_QUESTS")} [Y/n]");
                    key = System.Console.ReadKey(intercept: true);
                    if (key.KeyChar != 'n') factory.ImportQuests();
                }

                System.Console.WriteLine(value: Language.Instance.GetMessageFromKey(key: "DONE"));
                System.Console.ReadKey();
            }
            catch (FileNotFoundException ex)
            {
                Logger.Error(data: Language.Instance.GetMessageFromKey(key: "AT_LEAST_ONE_FILE_MISSING"), ex: ex);
                System.Console.ReadKey();
            }
        }

        #endregion
    }
}