﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Cryptography;
using OpenNos.Core.Threading;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;

namespace OpenNos.Import.Console
{
    public class ImportFactory
    {
        #region Instantiation

        public ImportFactory(string folder)
        {
            _folder = folder;
        }

        #endregion

        #region Members

        readonly string _folder;

        readonly List<string[]> _packetList = new List<string[]>();

        IEnumerable<MapDto> _maps;

        #endregion

        #region Methods

        public static void ImportAccounts()
        {
            var acc1 = new AccountDto
            {
                AccountId = 1,
                Authority = AuthorityType.Administrator,
                Name = "administrator",
                Password = CryptographyBase.Sha512(inputString: "test")
            };
            DaoFactory.AccountDao.InsertOrUpdate(account: ref acc1);

            var acc2 = new AccountDto
            {
                AccountId = 2,
                Authority = AuthorityType.Gm,
                Name = "gamemaster",
                Password = CryptographyBase.Sha512(inputString: "test")
            };
            DaoFactory.AccountDao.InsertOrUpdate(account: ref acc2);

            var acc3 = new AccountDto
            {
                AccountId = 3,
                Authority = AuthorityType.User,
                Name = "user",
                Password = CryptographyBase.Sha512(inputString: "test")
            };
            DaoFactory.AccountDao.InsertOrUpdate(account: ref acc3);
        }

        public void ImportQuests()
        {
            var fileQuestDat = $"{_folder}\\quest.dat";
            var fileRewardsDat = $"{_folder}\\qstprize.dat";
            var qstCounter = 0;

            var dictionaryRewards = new Dictionary<long, QuestRewardDto>();
            var reward = new QuestRewardDto();
            string line;

            using (var questRewardStream =
                new StreamReader(path: fileRewardsDat, encoding: Encoding.GetEncoding(codepage: 1252)))
            {
                while ((line = questRewardStream.ReadLine()) != null)
                {
                    var currentLine = line.Split('\t');
                    if (currentLine.Length <= 1 && currentLine[0] != "END") continue;
                    switch (currentLine[0])
                    {
                        case "VNUM":
                            reward = new QuestRewardDto
                            {
                                QuestRewardId = long.Parse(s: currentLine[1]),
                                RewardType = byte.Parse(s: currentLine[2])
                            };
                            break;

                        case "DATA":
                            if (currentLine.Length < 3) return;
                            switch ((QuestRewardType)reward.RewardType)
                            {
                                case QuestRewardType.Exp:
                                case QuestRewardType.SecondExp:
                                case QuestRewardType.JobExp:
                                case QuestRewardType.SecondJobExp:
                                    reward.Data = int.Parse(s: currentLine[2]) == -1 ? 0 : int.Parse(s: currentLine[2]);
                                    reward.Amount = int.Parse(s: currentLine[1]);
                                    break;

                                case QuestRewardType.WearItem:
                                    reward.Data = int.Parse(s: currentLine[1]);
                                    reward.Amount = 1;
                                    break;

                                case QuestRewardType.EtcMainItem:
                                    reward.Data = int.Parse(s: currentLine[1]);
                                    reward.Amount = int.Parse(s: currentLine[5]) == -1
                                        ? 1
                                        : int.Parse(s: currentLine[5]);
                                    break;

                                case QuestRewardType.Gold:
                                case QuestRewardType.SecondGold:
                                case QuestRewardType.ThirdGold:
                                case QuestRewardType.FourthGold:
                                case QuestRewardType.Reput:
                                    reward.Data = 0;
                                    reward.Amount = int.Parse(s: currentLine[1]);
                                    break;

                                default:
                                    reward.Data = int.Parse(s: currentLine[1]);
                                    reward.Amount = int.Parse(s: currentLine[2]);
                                    break;
                            }

                            break;

                        case "END":
                            dictionaryRewards[key: reward.QuestRewardId] = reward;
                            break;
                    }
                }

                questRewardStream.Close();
            }


            // Final List
            var quests = new List<QuestDto>();
            var rewards = new List<QuestRewardDto>();
            var questObjectives = new List<QuestObjectiveDto>();

            // Current
            var quest = new QuestDto();
            var currentRewards = new List<QuestRewardDto>();
            var currentObjectives = new List<QuestObjectiveDto>();

            byte objectiveIndex = 0;
            using var questStream =
                new StreamReader(path: fileQuestDat, encoding: Encoding.GetEncoding(codepage: 1252));
            while ((line = questStream.ReadLine()) != null)
            {
                var currentLine = line.Split('\t');
                if (currentLine.Length > 1 || currentLine[0] == "END")
                    switch (currentLine[0])
                    {
                        case "VNUM":
                            quest = new QuestDto
                            {
                                QuestId = long.Parse(s: currentLine[1]),
                                QuestType = int.Parse(s: currentLine[2]),
                                InfoId = int.Parse(s: currentLine[1])
                            };
                            switch (quest.QuestId)
                            {
                                //TODO: Legendary Hunter quests will be markes ad daily, but should be in the "secondary" slot and be daily nevertheless
                                case 6057: // John the adventurer
                                case 7519: // Legendary Hunter 1 time Kertos
                                case 7520: // Legendary Hunter 1 time Valakus
                                case 7521: // Legendary Hunter Grenigas
                                case 7522: // Legendary Hunter Draco
                                case 7523: // Legendary Hunter Glacerus
                                case 7524: // Legendary Hunter Laurena
                                case 5514: // Sherazade ice flower (n_run 65)
                                case 5919: // Akamur's military engineer (n_run 68)
                                case 5908: // John (n_run 67)
                                case 5914: // Alchemist (n_run 66)
                                    quest.IsDaily = true;
                                    break;
                            }

                            objectiveIndex = 0;
                            currentRewards.Clear();
                            currentObjectives.Clear();
                            break;

                        case "LINK":
                            if (int.Parse(s: currentLine[1]) != -1) // Base Quest Order (ex: SpQuest)
                            {
                                quest.NextQuestId = int.Parse(s: currentLine[1]);
                                continue;
                            }

                            // Main Quest Order
                            switch (quest.QuestId)
                            {
                                case 1997:
                                    quest.NextQuestId = 1500;
                                    break;
                                case 1523:
                                case 1532:
                                case 1580:
                                case 1610:
                                case 1618:
                                case 1636:
                                case 1647:
                                case 1664:
                                case 3075:
                                    quest.NextQuestId = quest.QuestId + 2;
                                    break;
                                case 1527:
                                case 1553:
                                    quest.NextQuestId = quest.QuestId + 3;
                                    break;
                                case 1690:
                                    quest.NextQuestId = 1694;
                                    break;
                                case 1737:
                                    quest.NextQuestId = 1982;
                                    break;
                                case 1751:
                                    quest.NextQuestId = 3000;
                                    break;
                                case 1982:
                                    quest.NextQuestId = 1738;
                                    break;
                                case 3101:
                                    quest.NextQuestId = 3200;
                                    break;
                                case 3331:
                                    quest.NextQuestId = 3340;
                                    break;
                                case 3374:
                                    quest.NextQuestId = 6179; // First desert quest
                                    break;

                                default:
                                    if (quest.QuestId < 1500 || quest.QuestId >= 1751 && quest.QuestId < 3000 ||
                                        quest.QuestId >= 3374 && (quest.QuestId < 5478 || quest.QuestId >= 5513))
                                        continue;
                                    quest.NextQuestId = quest.QuestId + 1;
                                    break;
                            }

                            break;

                        case "LEVEL":
                            quest.LevelMin = byte.Parse(s: currentLine[1]);
                            quest.LevelMax = byte.Parse(s: currentLine[2]);
                            break;

                        case "TALK":
                            if (int.Parse(s: currentLine[1]) > 0) quest.StartDialogId = int.Parse(s: currentLine[1]);
                            if (int.Parse(s: currentLine[2]) > 0) quest.EndDialogId = int.Parse(s: currentLine[2]);
                            if (int.Parse(s: currentLine[3]) > 0) quest.DialogNpcVNum = int.Parse(s: currentLine[3]);
                            if (int.Parse(s: currentLine[4]) > 0) quest.DialogNpcId = int.Parse(s: currentLine[4]);
                            break;

                        case "TARGET":
                            if (int.Parse(s: currentLine[3]) > 0)
                            {
                                quest.TargetMap = short.Parse(s: currentLine[3]);
                                quest.TargetX = short.Parse(s: currentLine[1]);
                                quest.TargetY = short.Parse(s: currentLine[2]);
                            }

                            switch (quest.QuestId)
                            {
                                case 1708:
                                    quest.TargetMap = 76;
                                    quest.TargetX = 62;
                                    quest.TargetY = 68;
                                    break;
                            }

                            break;

                        case "DATA":
                            if (currentLine.Length < 3) return;
                            objectiveIndex++;
                            int? data = null, objective = null, specialData = null, secondSpecialData = null;
                            switch ((QuestType)quest.QuestType)
                            {
                                case QuestType.Hunt:
                                case QuestType.Capture1:
                                case QuestType.Capture2:
                                case QuestType.Collect1:
                                case QuestType.Product:
                                    data = int.Parse(s: currentLine[1]);
                                    objective = int.Parse(s: currentLine[2]);
                                    if (objective == -1) objective = int.Parse(s: currentLine[3]);
                                    break;

                                case QuestType.Brings: // npcVNum - ItemCount - ItemVNum //
                                case QuestType.Collect3: // ItemVNum - Objective - TsId //
                                case QuestType.Needed: // ItemVNum - Objective - npcVNum //
                                case QuestType.Collect5: // ItemVNum - Objective - npcVNum //
                                    data = int.Parse(s: currentLine[2]);
                                    objective = int.Parse(s: currentLine[3]);
                                    specialData = int.Parse(s: currentLine[1]);
                                    break;

                                case QuestType.Collect4: // ItemVNum - Objective - MonsterVNum - DropRate // 
                                case QuestType.Collect2: // ItemVNum - Objective - MonsterVNum - DropRate // 
                                    data = int.Parse(s: currentLine[2]);
                                    objective = int.Parse(s: currentLine[3]);
                                    specialData = int.Parse(s: currentLine[1]);
                                    secondSpecialData = int.Parse(s: currentLine[4]);
                                    break;

                                case QuestType.TimesSpace: // TS Lvl - Objective - TS Id //
                                case QuestType.TsPoint:
                                    data = int.Parse(s: currentLine[4]);
                                    objective = int.Parse(s: currentLine[2]);
                                    specialData = int.Parse(s: currentLine[1]);
                                    break;

                                case QuestType.Wear: // Item VNum - * - NpcVNum //
                                    data = int.Parse(s: currentLine[2]);
                                    specialData = int.Parse(s: currentLine[1]);
                                    break;

                                case QuestType.TransmitGold: // NpcVNum - Gold x10K - * //
                                    data = int.Parse(s: currentLine[1]);
                                    objective = int.Parse(s: currentLine[2]) * 10000;
                                    break;

                                case QuestType.GoTo: // Map - PosX - PosY //
                                    data = int.Parse(s: currentLine[1]);
                                    objective = int.Parse(s: currentLine[2]);
                                    specialData = int.Parse(s: currentLine[3]);
                                    break;

                                case QuestType.WinRaid: // Design - Objective - ? //
                                    data = int.Parse(s: currentLine[1]);
                                    objective = int.Parse(s: currentLine[2]);
                                    specialData = int.Parse(s: currentLine[3]);
                                    break;

                                case QuestType.Use: // Item to use - * - mateVnum //
                                    data = int.Parse(s: currentLine[1]);
                                    specialData = int.Parse(s: currentLine[2]);
                                    break;

                                case QuestType.Dialog1: // npcVNum - * - * //
                                case QuestType.Dialog2: // npcVNum - * - * //
                                    data = int.Parse(s: currentLine[1]);
                                    break;

                                case QuestType.FlowerQuest:
                                    objective = 10;
                                    break;

                                case QuestType.Inspect: // NpcVNum - Objective - ItemVNum //
                                case QuestType.Required: // npcVNum - Objective - ItemVNum //
                                    data = int.Parse(s: currentLine[1]);
                                    objective = int.Parse(s: currentLine[3]);
                                    specialData = int.Parse(s: currentLine[2]);
                                    break;

                                default:
                                    data = int.Parse(s: currentLine[1]);
                                    objective = int.Parse(s: currentLine[2]);
                                    specialData = int.Parse(s: currentLine[3]);
                                    break;
                            }

                            currentObjectives.Add(item: new QuestObjectiveDto
                            {
                                Data = data,
                                Objective = objective ?? 1,
                                SpecialData = specialData < 0 ? null : specialData,
                                DropRate = secondSpecialData < 0 ? null : secondSpecialData,
                                ObjectiveIndex = objectiveIndex,
                                QuestId = (int)quest.QuestId
                            });
                            break;

                        case "PRIZE":
                            for (var a = 1; a < 5; a++)
                            {
                                if (!dictionaryRewards.ContainsKey(key: long.Parse(s: currentLine[a]))) continue;
                                var currentReward = dictionaryRewards[key: long.Parse(s: currentLine[a])];
                                currentRewards.Add(item: new QuestRewardDto
                                {
                                    RewardType = currentReward.RewardType,
                                    Data = currentReward.Data,
                                    Amount = currentReward.Amount,
                                    QuestId = quest.QuestId
                                });
                            }

                            break;

                        case "END":
                            if (DaoFactory.QuestDao.LoadById(id: quest.QuestId) == null)
                            {
                                quest.IsDaily = true;
                                questObjectives.AddRange(collection: currentObjectives);
                                rewards.AddRange(collection: currentRewards);
                                quests.Add(item: quest);
                                qstCounter++;
                            }

                            break;
                    }
            }

            DaoFactory.QuestDao.Insert(questList: quests);
            DaoFactory.QuestRewardDao.Insert(questRewards: rewards);
            DaoFactory.QuestObjectiveDao.Insert(questObjectives: questObjectives);
            Logger.Log.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "QUEST_PARSED"),
                arg0: qstCounter));
            Logger.Log.Info(
                message: string.Format(format: Language.Instance.GetMessageFromKey(key: "QUEST_REWARD_PARSED"),
                    arg0: rewards.Count));
            Logger.Log.Info(message: string.Format(
                format: Language.Instance.GetMessageFromKey(key: "QUEST_OBJECTIVE_PARSED"),
                arg0: questObjectives.Count));

            questStream.Close();
        }

        public void ImportCards()
        {
            var fileCardDat = $"{_folder}\\Card.dat";
            var fileCardLang = $"{_folder}\\_code_{ConfigurationManager.AppSettings[name: nameof(Language)]}_Card.txt";
            var cards = new List<CardDto>();
            var dictionaryIdLang = new Dictionary<string, string>();
            var card = new CardDto();
            BCardDto bcard;
            var bcards = new List<BCardDto>();
            string line;
            var itemAreaBegin = false;

            using (var npcIdLangStream =
                new StreamReader(path: fileCardLang, encoding: Encoding.GetEncoding(codepage: 1252)))
            {
                while ((line = npcIdLangStream.ReadLine()) != null)
                {
                    var linesave = line.Split('\t');
                    if (linesave.Length > 1 && !dictionaryIdLang.ContainsKey(key: linesave[0]))
                        dictionaryIdLang.Add(key: linesave[0], value: linesave[1]);
                }
            }

            using var npcIdStream = new StreamReader(path: fileCardDat, encoding: Encoding.GetEncoding(codepage: 1252));
            while ((line = npcIdStream.ReadLine()) != null)
            {
                var currentLine = line.Split('\t');

                if (currentLine.Length > 2 && currentLine[1] == "VNUM")
                {
                    card = new CardDto
                    {
                        CardId = short.Parse(s: currentLine[2])
                    };
                    itemAreaBegin = true;
                    DaoFactory.BCardDao.DeleteByCardId(cardId: card.CardId);
                }
                else if (currentLine.Length > 2 && currentLine[1] == "NAME")
                {
                    card.Name = dictionaryIdLang.ContainsKey(key: currentLine[2])
                        ? dictionaryIdLang[key: currentLine[2]]
                        : "";
                }
                else if (currentLine.Length > 3 && currentLine[1] == "GROUP")
                {
                    if (!itemAreaBegin) continue;
                    card.Level = byte.Parse(s: currentLine[3]);
                }
                else if (currentLine.Length > 3 && currentLine[1] == "EFFECT")
                {
                    card.EffectId = int.Parse(s: currentLine[3]);
                }
                else if (currentLine.Length > 3 && currentLine[1] == "STYLE")
                {
                    card.BuffType = (BuffType)byte.Parse(s: currentLine[3]);
                    if (card.CardId == 106) card.BuffType = BuffType.Bad;
                }
                else if (currentLine.Length > 3 && currentLine[1] == "TIME")
                {
                    card.Duration = int.Parse(s: currentLine[2]);
                    card.Delay = int.Parse(s: currentLine[3]);
                }
                else if (currentLine.Length > 3 && currentLine[1] == "1ST")
                {
                    for (var i = 0; i < 3; i++)
                        if (currentLine[2 + i * 6] != "-1" && currentLine[2 + i * 6] != "0")
                        {
                            var first = int.Parse(s: currentLine[6 + i * 6]);

                            bcard = new BCardDto
                            {
                                CardId = card.CardId,
                                Type = byte.Parse(s: currentLine[2 + i * 6]),
                                SubType = (byte)(byte.Parse(s: currentLine[3 + i * 6]) + 1),

                                IsLevelScaled = Convert.ToBoolean(value: first % 4),
                                IsLevelDivided = Math.Abs(value: first % 4) == 2,
                                FirstData = first / 4,
                                SecondData = int.Parse(s: currentLine[7 + i * 6]) / 4,
                                ThirdData = int.Parse(s: currentLine[5 + i * 6])
                            };
                            bcards.Add(item: bcard);
                        }
                }
                else if (currentLine.Length > 3 && currentLine[1] == "2ST")
                {
                    for (var i = 0; i < 2; i++)
                    {
                        var first = int.Parse(s: currentLine[6 + i * 6]);
                        if (currentLine[2 + i * 6] != "-1" && currentLine[2 + i * 6] != "0")
                        {
                            bcard = new BCardDto
                            {
                                CastType = 1,
                                CardId = card.CardId,
                                Type = byte.Parse(s: currentLine[2 + i * 6]),
                                SubType = (byte)(byte.Parse(s: currentLine[3 + i * 6]) + 1),

                                ThirdData = int.Parse(s: currentLine[5 + i * 6]) / 4,
                                IsLevelScaled = Convert.ToBoolean(value: first % 4),
                                IsLevelDivided = Math.Abs(value: first % 4) == 2,
                                FirstData = first / 4,
                                SecondData = int.Parse(s: currentLine[7 + i * 6]) / 4
                            };
                            bcards.Add(item: bcard);
                        }
                    }
                }
                else if (currentLine.Length > 3 && currentLine[1] == "LAST")
                {
                    card.TimeoutBuff = short.Parse(s: currentLine[2]);
                    card.TimeoutBuffChance = byte.Parse(s: currentLine[3]);

                    // investigate
                    cards.Add(item: card);
                    itemAreaBegin = false;
                }
            }

            static BCardDto ReturnBCard(short cardId, byte type, byte subType, int firstData, int secondData = 0,
                int thirdData = 0, byte castType = 0, bool isLevelScaled = false, bool isLevelDivided = false)
            {
                return new BCardDto
                {
                    CardId = cardId,
                    Type = type,
                    SubType = subType,
                    FirstData = firstData,
                    SecondData = secondData,
                    ThirdData = thirdData,
                    CastType = castType,
                    IsLevelScaled = isLevelScaled,
                    IsLevelDivided = isLevelDivided
                };
            }

            bcards.Add(item: ReturnBCard(cardId: 146, type: 44, subType: 6, firstData: 50));
            bcards.Add(item: ReturnBCard(cardId: 131, type: 8, subType: 2, firstData: 30));
            bcards.Add(item: ReturnBCard(cardId: 131, type: 8, subType: 3, firstData: 30));
            bcards.Add(item: ReturnBCard(cardId: 131, type: 8, subType: 4, firstData: 30));
            bcards.Add(item: ReturnBCard(cardId: 131, type: 8, subType: 5, firstData: 30));

            cards = cards.Where(predicate: s => DaoFactory.CardDao.LoadById(cardId: s.CardId) == null).ToList();
            DaoFactory.CardDao.Insert(cards: cards);
            DaoFactory.BCardDao.Insert(cards: bcards);
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "CARDS_PARSED"),
                arg0: cards.Count));
        }

        public void ImportHardcodedItemRecipes()
        {
            // Production Tools for Adventurers
            InsertRecipe(itemVNum: 6, triggerVNum: 1035, amount: 1,
                recipeItems: new short[] { 1027, 10, 2038, 8, 1035, 1 });
            InsertRecipe(itemVNum: 16, triggerVNum: 1035, amount: 1,
                recipeItems: new short[] { 1027, 8, 2042, 6, 1035, 1 });
            InsertRecipe(itemVNum: 204, triggerVNum: 1035, amount: 1,
                recipeItems: new short[] { 1027, 15, 2042, 10, 1035, 1 });
            InsertRecipe(itemVNum: 206, triggerVNum: 1035, amount: 1,
                recipeItems: new short[] { 1027, 8, 2046, 7, 1035, 1 });
            InsertRecipe(itemVNum: 501, triggerVNum: 1035, amount: 1,
                recipeItems: new short[] { 1027, 14, 500, 1, 1035, 1 });

            // Production Tools for Swordsmen
            InsertRecipe(itemVNum: 22, triggerVNum: 1039, amount: 1,
                recipeItems: new short[] { 1027, 30, 2035, 32, 1039, 1 });
            InsertRecipe(itemVNum: 26, triggerVNum: 1039, amount: 1,
                recipeItems: new short[] { 1027, 43, 2035, 44, 1039, 1 });
            InsertRecipe(itemVNum: 30, triggerVNum: 1039, amount: 1,
                recipeItems: new short[] { 1027, 54, 2036, 56, 1039, 1 });
            InsertRecipe(itemVNum: 73, triggerVNum: 1039, amount: 1,
                recipeItems: new short[] { 1027, 33, 2035, 10, 2039, 23, 1039, 1 });
            InsertRecipe(itemVNum: 76, triggerVNum: 1039, amount: 1,
                recipeItems: new short[] { 1027, 53, 2036, 14, 2040, 39, 1039, 1 });
            InsertRecipe(itemVNum: 96, triggerVNum: 1039, amount: 1,
                recipeItems: new short[] { 1027, 20, 2034, 6, 2046, 14, 1039, 1 });
            InsertRecipe(itemVNum: 100, triggerVNum: 1039, amount: 1,
                recipeItems: new short[] { 1027, 35, 2035, 12, 2047, 23, 1039, 1 });
            InsertRecipe(itemVNum: 104, triggerVNum: 1039, amount: 1,
                recipeItems: new short[] { 1027, 53, 2036, 18, 2048, 35, 1039, 1 });

            // Production Tools for Archers
            InsertRecipe(itemVNum: 36, triggerVNum: 1040, amount: 1,
                recipeItems: new short[] { 1027, 30, 2039, 32, 1040, 1 });
            InsertRecipe(itemVNum: 40, triggerVNum: 1040, amount: 1,
                recipeItems: new short[] { 1027, 43, 2039, 35, 1040, 1 });
            InsertRecipe(itemVNum: 44, triggerVNum: 1040, amount: 1,
                recipeItems: new short[] { 1027, 54, 2040, 56, 1040, 1 });
            InsertRecipe(itemVNum: 81, triggerVNum: 1040, amount: 1,
                recipeItems: new short[] { 1027, 33, 2035, 32, 1040, 1 });
            InsertRecipe(itemVNum: 84, triggerVNum: 1040, amount: 1,
                recipeItems: new short[] { 1027, 53, 2036, 54, 1040, 1 });
            InsertRecipe(itemVNum: 109, triggerVNum: 1040, amount: 1,
                recipeItems: new short[] { 1027, 20, 2042, 8, 2046, 12, 1040, 1 });
            InsertRecipe(itemVNum: 113, triggerVNum: 1040, amount: 1,
                recipeItems: new short[] { 1027, 35, 2043, 13, 2047, 22, 1040, 1 });
            InsertRecipe(itemVNum: 117, triggerVNum: 1040, amount: 1,
                recipeItems: new short[] { 1027, 53, 2044, 20, 2048, 33, 1040, 1 });

            // Production Tools for Sorcerers
            InsertRecipe(itemVNum: 50, triggerVNum: 1041, amount: 1,
                recipeItems: new short[] { 1027, 30, 2039, 32, 1041, 1 });
            InsertRecipe(itemVNum: 54, triggerVNum: 1041, amount: 1,
                recipeItems: new short[] { 1027, 43, 2039, 45, 1041, 1 });
            InsertRecipe(itemVNum: 58, triggerVNum: 1041, amount: 1,
                recipeItems: new short[] { 1027, 54, 2040, 56, 1041, 1 });
            InsertRecipe(itemVNum: 89, triggerVNum: 1041, amount: 1,
                recipeItems: new short[] { 1027, 33, 2035, 34, 1041, 1 });
            InsertRecipe(itemVNum: 92, triggerVNum: 1041, amount: 1,
                recipeItems: new short[] { 1027, 53, 2036, 54, 1041, 1 });
            InsertRecipe(itemVNum: 122, triggerVNum: 1041, amount: 1,
                recipeItems: new short[] { 1027, 20, 2042, 14, 2046, 6, 1041, 1 });
            InsertRecipe(itemVNum: 126, triggerVNum: 1041, amount: 1,
                recipeItems: new short[] { 1027, 35, 2043, 28, 2047, 7, 1041, 1 });
            InsertRecipe(itemVNum: 130, triggerVNum: 1041, amount: 1,
                recipeItems: new short[] { 1027, 53, 2044, 42, 2048, 11, 1041, 1 });

            // Production Tools for Accessories
            InsertRecipe(itemVNum: 508, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 24, 1032, 5, 1047, 1 });
            InsertRecipe(itemVNum: 509, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 25, 1031, 5, 1047, 1 });
            InsertRecipe(itemVNum: 510, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 26, 1031, 7, 1047, 1 });
            InsertRecipe(itemVNum: 514, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 33, 1033, 10, 1047, 1 });
            InsertRecipe(itemVNum: 516, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 35, 1032, 12, 1047, 1 });
            InsertRecipe(itemVNum: 517, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 36, 1034, 15, 1047, 1 });
            InsertRecipe(itemVNum: 522, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 43, 1033, 20, 1047, 1 });
            InsertRecipe(itemVNum: 523, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 44, 1031, 24, 1047, 1 });
            InsertRecipe(itemVNum: 525, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 46, 1034, 28, 1047, 1 });
            InsertRecipe(itemVNum: 531, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 54, 1032, 36, 1047, 1 });
            InsertRecipe(itemVNum: 534, triggerVNum: 1047, amount: 1,
                recipeItems: new short[] { 1027, 57, 1033, 42, 1047, 1 });

            // Production Tools for Gems, Cellons and Crystals
            InsertRecipe(itemVNum: 1016, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 99, 1015, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1018, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 5, 1017, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1019, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 10, 1018, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1020, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 17, 1019, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1021, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 25, 1020, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1022, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 35, 1021, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1023, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 50, 1022, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1024, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 75, 1023, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1025, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 110, 1024, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1026, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 160, 1025, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1029, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 20, 1028, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1030, triggerVNum: 1072, amount: 1,
                recipeItems: new short[] { 1014, 50, 1029, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1031, triggerVNum: 1072, amount: 4,
                recipeItems: new short[] { 1028, 1, 2097, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1032, triggerVNum: 1072, amount: 4,
                recipeItems: new short[] { 1028, 1, 2097, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1033, triggerVNum: 1072, amount: 4,
                recipeItems: new short[] { 1028, 1, 2097, 5, 1072, 1 });
            InsertRecipe(itemVNum: 1034, triggerVNum: 1072, amount: 4,
                recipeItems: new short[] { 1028, 1, 2097, 5, 1072, 1 });

            // Production Tools for Raw Materials
            InsertRecipe(itemVNum: 2035, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 5, 2034, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2036, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 10, 2035, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2037, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 20, 2036, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2039, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 5, 2038, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2040, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 10, 2039, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2041, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 20, 2040, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2043, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 5, 2042, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2044, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 10, 2043, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2045, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 20, 2044, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2047, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 5, 2046, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2048, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 10, 2047, 5, 1073, 1 });
            InsertRecipe(itemVNum: 2049, triggerVNum: 1073, amount: 1,
                recipeItems: new short[] { 1014, 20, 2048, 5, 1073, 1 });

            // Production Tools for Gloves and Shoes
            InsertRecipe(itemVNum: 718, triggerVNum: 1083, amount: 1,
                recipeItems: new short[] { 1027, 5, 1028, 1, 2042, 4, 1083, 1 });
            InsertRecipe(itemVNum: 703, triggerVNum: 1083, amount: 1,
                recipeItems: new short[] { 1027, 7, 1028, 2, 2034, 5, 1083, 1 });
            InsertRecipe(itemVNum: 705, triggerVNum: 1083, amount: 1,
                recipeItems: new short[] { 1027, 9, 1028, 3, 2035, 3, 1083, 1 });
            InsertRecipe(itemVNum: 719, triggerVNum: 1083, amount: 1,
                recipeItems: new short[] { 1027, 12, 1028, 4, 2047, 5, 1083, 1 });
            InsertRecipe(itemVNum: 722, triggerVNum: 1083, amount: 1,
                recipeItems: new short[] { 1027, 5, 1028, 1, 2046, 5, 1083, 1 });
            InsertRecipe(itemVNum: 723, triggerVNum: 1083, amount: 1,
                recipeItems: new short[] { 1027, 7, 1028, 2, 2046, 7, 1083, 1 });
            InsertRecipe(itemVNum: 724, triggerVNum: 1083, amount: 1,
                recipeItems: new short[] { 1027, 9, 1028, 3, 2047, 4, 1083, 1 });
            InsertRecipe(itemVNum: 725, triggerVNum: 1083, amount: 1,
                recipeItems: new short[] { 1027, 14, 1028, 4, 2047, 7, 1083, 1 });
            InsertRecipe(itemVNum: 325, triggerVNum: 1083, amount: 1,
                recipeItems: new short[] { 2044, 10, 2048, 10, 2093, 50, 1083, 1 });

            // Construction Plan (Level 1)
            InsertRecipe(itemVNum: 3121, triggerVNum: 1235, amount: 1,
                recipeItems: new short[] { 2036, 50, 2037, 30, 2040, 20, 2105, 10, 2189, 20, 2205, 20, 1, 1235 });
            InsertRecipe(itemVNum: 3122, triggerVNum: 1235, amount: 1,
                recipeItems: new short[] { 2040, 50, 2041, 30, 2048, 20, 2109, 10, 2190, 20, 2206, 20, 1, 1235 });
            InsertRecipe(itemVNum: 3123, triggerVNum: 1235, amount: 1,
                recipeItems: new short[] { 2044, 20, 2048, 50, 2049, 30, 2117, 10, 2191, 20, 2207, 20, 1, 1235 });
            InsertRecipe(itemVNum: 3124, triggerVNum: 1235, amount: 1,
                recipeItems: new short[] { 2036, 20, 2044, 50, 2045, 30, 2118, 10, 2192, 20, 2208, 20, 1, 1235 });

            // Construction Plan (Level 2)
            InsertRecipe(itemVNum: 3125, triggerVNum: 1236, amount: 1,
                recipeItems: new short[]
                    {2037, 70, 2041, 40, 2048, 20, 2105, 20, 2189, 30, 2193, 30, 2197, 20, 2205, 40, 1236, 1});
            InsertRecipe(itemVNum: 3126, triggerVNum: 1236, amount: 1,
                recipeItems: new short[]
                    {2041, 70, 2044, 20, 2049, 40, 2109, 20, 2190, 30, 2194, 30, 2198, 20, 2206, 40, 1236, 1});
            InsertRecipe(itemVNum: 3127, triggerVNum: 1236, amount: 1,
                recipeItems: new short[]
                    {2036, 20, 2045, 40, 2049, 70, 2117, 20, 2191, 30, 2195, 30, 2199, 20, 2207, 40, 1236, 1});
            InsertRecipe(itemVNum: 3128, triggerVNum: 1236, amount: 1,
                recipeItems: new short[]
                    {2037, 40, 2040, 20, 2045, 70, 2118, 20, 2192, 30, 2196, 30, 2200, 20, 2208, 40, 1236, 1});

            // Boot Combination Recipe A
            InsertRecipe(itemVNum: 384, triggerVNum: 1237, amount: 1,
                recipeItems: new short[] { 1027, 30, 1032, 10, 2010, 10, 2044, 30, 2208, 10, 1237, 1 });
            InsertRecipe(itemVNum: 385, triggerVNum: 1237, amount: 1,
                recipeItems: new short[] { 1027, 30, 1031, 10, 2010, 10, 2036, 30, 2205, 10, 1237, 1 });
            InsertRecipe(itemVNum: 386, triggerVNum: 1237, amount: 1,
                recipeItems: new short[] { 1027, 30, 1033, 10, 2010, 10, 2040, 30, 2206, 10, 1237, 1 });
            InsertRecipe(itemVNum: 387, triggerVNum: 1237, amount: 1,
                recipeItems: new short[] { 1027, 30, 1034, 10, 2010, 10, 2048, 30, 2207, 10, 1237, 1 });

            // Boot Combination Recipe B
            InsertRecipe(itemVNum: 388, triggerVNum: 1238, amount: 1,
                recipeItems: new short[] { 1027, 50, 1030, 5, 2010, 20, 2204, 10, 2210, 5, 1238, 1 });
            InsertRecipe(itemVNum: 389, triggerVNum: 1238, amount: 1,
                recipeItems: new short[] { 1027, 50, 1030, 5, 2010, 20, 2201, 10, 2209, 5, 1238, 1 });
            InsertRecipe(itemVNum: 390, triggerVNum: 1238, amount: 1,
                recipeItems: new short[] { 1027, 50, 1030, 5, 2010, 20, 2202, 10, 2211, 5, 1238, 1 });
            InsertRecipe(itemVNum: 391, triggerVNum: 1238, amount: 1,
                recipeItems: new short[] { 1027, 50, 1030, 5, 2010, 20, 2203, 10, 2212, 5, 1238, 1 });

            // Glove Combination Recipe A
            InsertRecipe(itemVNum: 376, triggerVNum: 1239, amount: 1,
                recipeItems: new short[] { 1027, 30, 1032, 10, 2010, 10, 2044, 30, 2208, 10, 1239, 1 });
            InsertRecipe(itemVNum: 377, triggerVNum: 1239, amount: 1,
                recipeItems: new short[] { 1027, 30, 1031, 10, 2010, 10, 2036, 30, 2205, 10, 1239, 1 });
            InsertRecipe(itemVNum: 378, triggerVNum: 1239, amount: 1,
                recipeItems: new short[] { 1027, 30, 1033, 10, 2010, 10, 2040, 30, 2206, 10, 1239, 1 });
            InsertRecipe(itemVNum: 379, triggerVNum: 1239, amount: 1,
                recipeItems: new short[] { 1027, 30, 1034, 10, 2010, 10, 2048, 30, 2207, 10, 1239, 1 });

            // Glove Combination Recipe B
            InsertRecipe(itemVNum: 380, triggerVNum: 1240, amount: 1,
                recipeItems: new short[] { 1027, 50, 1030, 5, 2010, 20, 2204, 10, 2210, 5, 1240, 1 });
            InsertRecipe(itemVNum: 381, triggerVNum: 1240, amount: 1,
                recipeItems: new short[] { 1027, 50, 1030, 5, 2010, 20, 2201, 10, 2209, 5, 1240, 1 });
            InsertRecipe(itemVNum: 382, triggerVNum: 1240, amount: 1,
                recipeItems: new short[] { 1027, 50, 1030, 5, 2010, 20, 2202, 10, 2211, 5, 1240, 1 });
            InsertRecipe(itemVNum: 383, triggerVNum: 1240, amount: 1,
                recipeItems: new short[] { 1027, 50, 1030, 5, 2010, 20, 2203, 10, 2212, 5, 1240, 1 });

            // Consumables Recipe
            InsertRecipe(itemVNum: 1245, triggerVNum: 1241, amount: 1,
                recipeItems: new short[] { 2029, 5, 2097, 5, 2196, 5, 2208, 5, 2215, 1, 1241, 1 });
            InsertRecipe(itemVNum: 1246, triggerVNum: 1241, amount: 1,
                recipeItems: new short[] { 2029, 5, 2097, 5, 2193, 5, 2206, 5, 1241, 1 });
            InsertRecipe(itemVNum: 1247, triggerVNum: 1241, amount: 1,
                recipeItems: new short[] { 2029, 5, 2097, 5, 2194, 5, 2207, 5, 1241, 1 });
            InsertRecipe(itemVNum: 1248, triggerVNum: 1241, amount: 1,
                recipeItems: new short[] { 2029, 5, 2097, 5, 2195, 5, 2205, 5, 1241, 1 });
            InsertRecipe(itemVNum: 1249, triggerVNum: 1241, amount: 1,
                recipeItems: new short[] { 2029, 5, 2097, 5, 2195, 5, 2205, 5, 1241, 1 });

            // Amir's Armour Parchment
            InsertRecipe(itemVNum: 409, triggerVNum: 1312, amount: 1,
                recipeItems: new short[] { 298, 1, 2049, 70, 2227, 80, 2254, 5, 2265, 80, 1312, 1 });
            InsertRecipe(itemVNum: 410, triggerVNum: 1312, amount: 1,
                recipeItems: new short[] { 296, 1, 2037, 70, 2246, 80, 2255, 5, 2271, 80, 1312, 1 });
            InsertRecipe(itemVNum: 411, triggerVNum: 1312, amount: 1,
                recipeItems: new short[] { 272, 1, 2041, 70, 2252, 5, 2253, 80, 2270, 80, 1312, 1 });

            // Amir's Weapon Parchment A
            InsertRecipe(itemVNum: 400, triggerVNum: 1313, amount: 1,
                recipeItems: new short[] { 263, 1, 2036, 60, 2218, 40, 2250, 10, 1313, 1 });
            InsertRecipe(itemVNum: 402, triggerVNum: 1313, amount: 1,
                recipeItems: new short[] { 292, 1, 2040, 60, 2217, 50, 2249, 5, 2263, 30, 2279, 3, 1313, 1 });
            InsertRecipe(itemVNum: 403, triggerVNum: 1313, amount: 1,
                recipeItems: new short[] { 266, 1, 2040, 60, 2217, 40, 2249, 10, 1313, 1 });
            InsertRecipe(itemVNum: 405, triggerVNum: 1313, amount: 1,
                recipeItems: new short[] { 290, 1, 2044, 60, 2224, 50, 2251, 5, 2262, 3, 2275, 30, 1313, 1 });
            InsertRecipe(itemVNum: 406, triggerVNum: 1313, amount: 1,
                recipeItems: new short[] { 269, 1, 2048, 60, 2224, 40, 2251, 10, 1313, 1 });
            InsertRecipe(itemVNum: 408, triggerVNum: 1313, amount: 1,
                recipeItems: new short[] { 264, 1, 2036, 60, 2218, 50, 2222, 3, 2250, 5, 2276, 30, 1313, 1 });

            // Amir's Weapon Parchment B
            InsertRecipe(itemVNum: 401, triggerVNum: 1314, amount: 1,
                recipeItems: new short[] { 400, 1, 2037, 99, 2222, 3, 2231, 70, 2257, 99, 1314, 1 });
            InsertRecipe(itemVNum: 404, triggerVNum: 1314, amount: 1,
                recipeItems: new short[] { 403, 1, 2041, 99, 2219, 3, 2226, 70, 2277, 99, 1314, 1 });
            InsertRecipe(itemVNum: 407, triggerVNum: 1314, amount: 1,
                recipeItems: new short[] { 406, 1, 2049, 99, 2245, 3, 2261, 70, 2269, 99, 1314, 1 });

            // Amir's Weapon Specification Book Cover
            InsertRecipe(itemVNum: 1315, triggerVNum: 1316, amount: 1,
                recipeItems: new short[] { 1312, 10, 1313, 10, 1314, 10, 1316, 1 });

            // Ancelloan's Accessory Production Scroll
            InsertRecipe(itemVNum: 4942, triggerVNum: 5884, amount: 1,
                recipeItems: new short[] { 4940, 1, 2805, 15, 2816, 5, 5881, 5, 2811, 30, 5884, 1 });
            InsertRecipe(itemVNum: 4943, triggerVNum: 5884, amount: 1,
                recipeItems: new short[] { 4938, 1, 2805, 10, 2816, 3, 5881, 3, 2811, 20, 5884, 1 });
            InsertRecipe(itemVNum: 4944, triggerVNum: 5884, amount: 1,
                recipeItems: new short[] { 4936, 1, 2805, 12, 2816, 4, 5881, 4, 2811, 25, 5884, 1 });
            InsertRecipe(itemVNum: 4946, triggerVNum: 5884, amount: 1,
                recipeItems: new short[] { 4940, 1, 2805, 15, 2816, 5, 5880, 5, 2811, 30, 5884, 1 });
            InsertRecipe(itemVNum: 4947, triggerVNum: 5884, amount: 1,
                recipeItems: new short[] { 4938, 1, 2805, 10, 2816, 3, 5880, 3, 2811, 20, 5884, 1 });
            InsertRecipe(itemVNum: 4948, triggerVNum: 5884, amount: 1,
                recipeItems: new short[] { 4936, 1, 2805, 12, 2816, 4, 5880, 4, 2811, 25, 5884, 1 });

            // Ancelloan's Weapon Production Scroll
            InsertRecipe(itemVNum: 4958, triggerVNum: 5885, amount: 1,
                recipeItems: new short[] { 4901, 1, 2805, 80, 2816, 60, 5880, 70, 2812, 35, 5885, 1 });
            InsertRecipe(itemVNum: 4959, triggerVNum: 5885, amount: 1,
                recipeItems: new short[] { 4907, 1, 2805, 80, 2816, 60, 5880, 70, 2812, 35, 5885, 1 });
            InsertRecipe(itemVNum: 4960, triggerVNum: 5885, amount: 1,
                recipeItems: new short[] { 4904, 1, 2805, 80, 2816, 60, 5880, 70, 2812, 35, 5885, 1 });
            InsertRecipe(itemVNum: 4964, triggerVNum: 5885, amount: 1,
                recipeItems: new short[] { 4901, 1, 2805, 80, 2816, 60, 5881, 70, 2812, 35, 5885, 1 });
            InsertRecipe(itemVNum: 4965, triggerVNum: 5885, amount: 1,
                recipeItems: new short[] { 4907, 1, 2805, 80, 2816, 60, 5881, 70, 2812, 35, 5885, 1 });
            InsertRecipe(itemVNum: 4966, triggerVNum: 5885, amount: 1,
                recipeItems: new short[] { 4904, 1, 2805, 80, 2816, 60, 5881, 70, 2812, 35, 5885, 1 });

            // Ancelloan's Secondary Weapon Production Scroll
            InsertRecipe(itemVNum: 4955, triggerVNum: 5886, amount: 1,
                recipeItems: new short[] { 4913, 1, 2805, 80, 2816, 60, 5880, 70, 2812, 35, 5886, 1 });
            InsertRecipe(itemVNum: 4956, triggerVNum: 5886, amount: 1,
                recipeItems: new short[] { 4910, 1, 2805, 80, 2816, 60, 5880, 70, 2812, 35, 5886, 1 });
            InsertRecipe(itemVNum: 4957, triggerVNum: 5886, amount: 1,
                recipeItems: new short[] { 4916, 1, 2805, 80, 2816, 60, 5880, 70, 2812, 35, 5886, 1 });
            InsertRecipe(itemVNum: 4961, triggerVNum: 5886, amount: 1,
                recipeItems: new short[] { 4913, 1, 2805, 80, 2816, 60, 5881, 70, 2812, 35, 5886, 1 });
            InsertRecipe(itemVNum: 4962, triggerVNum: 5886, amount: 1,
                recipeItems: new short[] { 4910, 1, 2805, 80, 2816, 60, 5881, 70, 2812, 35, 5886, 1 });
            InsertRecipe(itemVNum: 4963, triggerVNum: 5886, amount: 1,
                recipeItems: new short[] { 4916, 1, 2805, 80, 2816, 60, 5881, 70, 2812, 35, 5886, 1 });

            // Ancelloan's Armour Production Scroll
            InsertRecipe(itemVNum: 4949, triggerVNum: 5887, amount: 1,
                recipeItems: new short[]
                    {4919, 1, 2805, 80, 2816, 40, 5880, 10, 2818, 20, 2819, 10, 2811, 70, 5887, 1});
            InsertRecipe(itemVNum: 4950, triggerVNum: 5887, amount: 1,
                recipeItems: new short[]
                    {4925, 1, 2805, 60, 2816, 15, 5880, 10, 2814, 70, 2818, 10, 2819, 20, 5887, 1});
            InsertRecipe(itemVNum: 4951, triggerVNum: 5887, amount: 1,
                recipeItems: new short[]
                    {4922, 1, 2805, 70, 2816, 30, 5880, 70, 2814, 35, 2818, 15, 2819, 15, 2811, 35, 5887, 1});
            InsertRecipe(itemVNum: 4952, triggerVNum: 5887, amount: 1,
                recipeItems: new short[]
                    {4919, 1, 2805, 80, 2816, 40, 5881, 10, 2818, 20, 2819, 10, 2811, 90, 5887, 1});
            InsertRecipe(itemVNum: 4953, triggerVNum: 5887, amount: 1,
                recipeItems: new short[]
                    {4925, 1, 2805, 60, 2816, 15, 5881, 10, 2814, 70, 2818, 10, 2819, 20, 5887, 1});
            InsertRecipe(itemVNum: 4954, triggerVNum: 5887, amount: 1,
                recipeItems: new short[]
                    {4922, 1, 2805, 70, 2816, 30, 5881, 70, 2814, 35, 2818, 15, 2819, 15, 2811, 35, 5887, 1});

            // Charred Mask Parchment
            InsertRecipe(itemVNum: 4927, triggerVNum: 5900, amount: 1,
                recipeItems: new short[] { 2505, 3, 2506, 2, 2353, 30, 2355, 20, 5900, 1 });
            InsertRecipe(itemVNum: 4928, triggerVNum: 5900, amount: 1,
                recipeItems: new short[] { 2505, 10, 2506, 8, 2507, 1, 2353, 90, 2356, 60, 5900, 3 });

            // Grenigas Accessories Parchment
            InsertRecipe(itemVNum: 4936, triggerVNum: 5901, amount: 1,
                recipeItems: new short[] { 4935, 1, 2505, 4, 2506, 4, 2359, 20, 2360, 20, 2509, 5, 5901, 1 });
            InsertRecipe(itemVNum: 4938, triggerVNum: 5901, amount: 1,
                recipeItems: new short[] { 4937, 1, 2505, 6, 2506, 2, 2359, 20, 2360, 20, 2510, 5, 5901, 1 });
            InsertRecipe(itemVNum: 4940, triggerVNum: 5901, amount: 1,
                recipeItems: new short[] { 4939, 1, 2505, 2, 2506, 6, 2359, 20, 2360, 20, 2508, 5, 5901, 1 });

            // this implementation takes a FUCKTON of hardcoding, for fucks sake ENTWELL why u suck
            // soo much -_-
        }

        public void ImportMapNpcs()
        {
            short map = 0;
            var npcs = new List<MapNpcDto>();
            var effPacketsDictionary = new ThreadSafeSortedList<int, short>();
            var npcMvPacketsList = new ConcurrentBag<int>();
            Parallel.ForEach(
                source: _packetList.Where(predicate: o => o[0].Equals(value: "mv") && o[1].Equals(value: "2")),
                body: currentPacket =>
                {
                    if (long.Parse(s: currentPacket[2]) > 20000) return;
                    if (!npcMvPacketsList.Contains(value: int.Parse(s: currentPacket[2])))
                        npcMvPacketsList.Add(item: int.Parse(s: currentPacket[2]));
                });

            Parallel.ForEach(
                source: _packetList.Where(predicate: o => o[0].Equals(value: "eff") && o[1].Equals(value: "2")),
                body: currentPacket =>
                {
                    if (long.Parse(s: currentPacket[2]) > 20000) return;
                    // ReSharper disable once AccessToDisposedClosure
                    if (!effPacketsDictionary.ContainsKey(key: int.Parse(s: currentPacket[2])))
                        // ReSharper disable once AccessToDisposedClosure
                        effPacketsDictionary[key: int.Parse(s: currentPacket[2])] = short.Parse(s: currentPacket[3]);
                });

            foreach (var currentPacket in _packetList.Where(predicate: o =>
                o[0].Equals(value: "in") || o[0].Equals(value: "at")))
            {
                if (currentPacket.Length > 5 && currentPacket[0] == "at")
                {
                    map = short.Parse(s: currentPacket[2]);
                    continue;
                }

                if (currentPacket.Length > 7 && currentPacket[0] == "in" && currentPacket[1] == "2")
                {
                    var npctest = new MapNpcDto
                    {
                        MapX = short.Parse(s: currentPacket[4]),
                        MapY = short.Parse(s: currentPacket[5]),
                        MapId = map,
                        NpcVNum = short.Parse(s: currentPacket[2])
                    };
                    if (long.Parse(s: currentPacket[3]) > 20000) continue;
                    npctest.MapNpcId = int.Parse(s: currentPacket[3]);
                    if (effPacketsDictionary.ContainsKey(key: npctest.MapNpcId))
                        npctest.Effect = effPacketsDictionary[key: npctest.MapNpcId];
                    npctest.EffectDelay = 4750;
                    npctest.IsMoving = npcMvPacketsList.Contains(value: npctest.MapNpcId);
                    npctest.Position = byte.Parse(s: currentPacket[6]);
                    npctest.Dialog = short.Parse(s: currentPacket[9]);
                    npctest.IsSitting = currentPacket[13] != "1";
                    npctest.IsDisabled = false;

                    // Levers teleporters
                    TeleporterDto teleporter = null;
                    if (npctest.MapId == 51 && npctest.MapX == 90 && npctest.MapY == 9)
                    {
                        teleporter = new TeleporterDto
                        {
                            Index = 0,
                            Type = 0,
                            MapNpcId = npctest.MapNpcId,
                            MapId = npctest.MapId,
                            MapX = 18,
                            MapY = 11
                        };
                        DaoFactory.TeleporterDao.Insert(teleporter: teleporter);
                    }

                    if (npctest.MapId == 51 && npctest.MapX == 18 && npctest.MapY == 10)
                    {
                        teleporter = new TeleporterDto
                        {
                            Index = 0,
                            Type = 0,
                            MapNpcId = npctest.MapNpcId,
                            MapId = npctest.MapId,
                            MapX = 90,
                            MapY = 10
                        };
                        DaoFactory.TeleporterDao.Insert(teleporter: teleporter);
                    }

                    if (npctest.MapId == 51 && npctest.MapX == 38 && npctest.MapY == 19)
                    {
                        teleporter = new TeleporterDto
                        {
                            Index = 0,
                            Type = 0,
                            MapNpcId = npctest.MapNpcId,
                            MapId = npctest.MapId,
                            MapX = 29,
                            MapY = 45
                        };
                        DaoFactory.TeleporterDao.Insert(teleporter: teleporter);
                    }

                    if (npctest.MapId == 51 && npctest.MapX == 29 && npctest.MapY == 43)
                        teleporter = new TeleporterDto
                        {
                            Index = 0,
                            Type = 0,
                            MapNpcId = npctest.MapNpcId,
                            MapId = npctest.MapId,
                            MapX = 38,
                            MapY = 20
                        };
                    if (npctest.MapId == 85 && npctest.MapX == 7 && npctest.MapY == 18)
                        teleporter = new TeleporterDto
                        {
                            Index = 0,
                            Type = 0,
                            MapNpcId = npctest.MapNpcId,
                            MapId = npctest.MapId,
                            MapX = 41,
                            MapY = 33
                        };
                    if (npctest.MapId == 85 && npctest.MapX == 40 && npctest.MapY == 32)
                        teleporter = new TeleporterDto
                        {
                            Index = 0,
                            Type = 0,
                            MapNpcId = npctest.MapNpcId,
                            MapId = npctest.MapId,
                            MapX = 7,
                            MapY = 20
                        };
                    if (npctest.MapId == 85 && npctest.MapX == 45 && npctest.MapY == 44)
                        teleporter = new TeleporterDto
                        {
                            Index = 0,
                            Type = 0,
                            MapNpcId = npctest.MapNpcId,
                            MapId = npctest.MapId,
                            MapX = 6,
                            MapY = 56
                        };
                    if (npctest.MapId == 85 && npctest.MapX == 5 && npctest.MapY == 55)
                        teleporter = new TeleporterDto
                        {
                            Index = 0,
                            Type = 0,
                            MapNpcId = npctest.MapNpcId,
                            MapId = npctest.MapId,
                            MapX = 44,
                            MapY = 45
                        };
                    if (npctest.MapId == 85 && npctest.MapX == 10 && npctest.MapY == 69)
                        teleporter = new TeleporterDto
                        {
                            Index = 0,
                            Type = 0,
                            MapNpcId = npctest.MapNpcId,
                            MapId = npctest.MapId,
                            MapX = 44,
                            MapY = 78
                        };
                    if (npctest.MapId == 85 && npctest.MapX == 43 && npctest.MapY == 77)
                        teleporter = new TeleporterDto
                        {
                            Index = 0,
                            Type = 0,
                            MapNpcId = npctest.MapNpcId,
                            MapId = npctest.MapId,
                            MapX = 10,
                            MapY = 70
                        };
                    if (teleporter != null) DaoFactory.TeleporterDao.Insert(teleporter: teleporter);

                    if (DaoFactory.NpcMonsterDao.LoadByVNum(npcMonsterVNum: npctest.NpcVNum) == null ||
                        DaoFactory.MapNpcDao.LoadById(mapNpcId: npctest.MapNpcId) != null ||
                        npcs.Any(predicate: i => i.MapNpcId == npctest.MapNpcId)
                        )
                        // Skip duplicated npcs
                        //|| DAOFactory.MapNpcDAO.LoadFromMap(map).Any(s => s.NpcVNum == npctest.NpcVNum && s.MapX == npctest.MapX && s.MapY == npctest.MapY) 
                        //|| npcs.Any(s => s.NpcVNum == npctest.NpcVNum && s.MapId == npctest.MapId && s.MapX == npctest.MapX && s.MapY == npctest.MapY))
                        continue;
                    npcs.Add(item: npctest);
                }
            }

            DaoFactory.MapNpcDao.Insert(npcs: npcs);
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "NPCS_PARSED"),
                arg0: npcs.Count));
            effPacketsDictionary.Dispose();
        }

        public void ImportMaps()
        {
            var fileMapIdDat = $"{_folder}\\MapIDData.dat";
            var fileMapIdLang =
                $"{_folder}\\_code_{ConfigurationManager.AppSettings[name: nameof(Language)]}_MapIDData.txt";
            var folderMap = $"{_folder}\\map";
            var maps = new ThreadSafeSortedList<short, MapDto>();
            var dictionaryZts = new Dictionary<int, string>();
            var dictionaryIdLang = new Dictionary<string, string>();
            var dictionaryMusic = new ThreadSafeSortedList<int, int>();
            var dictionaryData = new Dictionary<short, byte[]>();
            var dictionaryMap = new Dictionary<short, short>();

            string line;
            using (var mapIdStream =
                new StreamReader(path: fileMapIdDat, encoding: Encoding.GetEncoding(codepage: 1252)))
            {
                while ((line = mapIdStream.ReadLine()) != null)
                {
                    var linesave = line.Split(' ');
                    if (linesave.Length <= 2) continue;
                    if (!int.TryParse(s: linesave[0], result: out var mapid)) continue;
                    if (!dictionaryZts.ContainsKey(key: mapid)) dictionaryZts.Add(key: mapid, value: linesave[4]);
                }
            }

            using (var mapIdLangStream =
                new StreamReader(path: fileMapIdLang, encoding: Encoding.GetEncoding(codepage: 1252)))
            {
                while ((line = mapIdLangStream.ReadLine()) != null)
                {
                    var linesave = line.Split('\t');
                    if (linesave.Length <= 1 || dictionaryIdLang.ContainsKey(key: linesave[0])) continue;
                    dictionaryIdLang.Add(key: linesave[0], value: linesave[1]);
                }
            }

            Parallel.ForEach(source: _packetList.Where(predicate: o => o[0].Equals(value: "at")), body: linesave =>
            {
                // ReSharper disable once AccessToDisposedClosure
                if (linesave.Length <= 10 || dictionaryMusic.ContainsKey(key: int.Parse(s: linesave[2]))) return;
                // ReSharper disable once AccessToDisposedClosure
                dictionaryMusic[key: int.Parse(s: linesave[2])] = int.Parse(s: linesave[7]);
                if (_packetList.FirstOrDefault(predicate: s =>
                    s[0].Equals(value: "c_map") &&
                    _packetList.FindIndex(match: b => b == s) >
                    _packetList.FindIndex(match: b => b == linesave)) is { } cmap)
                    dictionaryMap[key: short.Parse(s: cmap[2])] = short.Parse(s: linesave[2]);
            });

            var mapPartitioner = Partitioner.Create(source: new DirectoryInfo(path: folderMap).GetFiles(),
                partitionerOptions: EnumerablePartitionerOptions.NoBuffering);
            Parallel.ForEach(source: mapPartitioner, parallelOptions: new ParallelOptions { MaxDegreeOfParallelism = 1 },
                body: file => dictionaryData[key: short.Parse(s: file.Name)] = File.ReadAllBytes(path: file.FullName));

            // Found in packet.txt maps 
            Parallel.ForEach(source: dictionaryMap, body: map =>
            {
                if (dictionaryData[key: map.Value] == null) return; // Map grid data doesnt exists 
                AddMap(mapId: map.Key, originalMapId: map.Value, mapData: dictionaryData[key: map.Value]);
            });

            // Nostale data maps didnt found in packet.txt 
            Parallel.ForEach(source: dictionaryData,
                body: map => { AddMap(mapId: map.Key, originalMapId: map.Key, mapData: map.Value); });

            void AddMap(short mapId, short originalMapId, byte[] mapData)
            {
                var name = "";
                var music = 0;

                if (dictionaryZts.ContainsKey(key: mapId) &&
                    dictionaryIdLang.ContainsKey(key: dictionaryZts[key: mapId]))
                    name = dictionaryIdLang[key: dictionaryZts[key: mapId]];
                // ReSharper disable once AccessToDisposedClosure
                if (dictionaryMusic.ContainsKey(key: mapId))
                    music =
                        // ReSharper disable once AccessToDisposedClosure
                        dictionaryMusic[key: mapId];

                var map = new MapDto
                {
                    Name = name,
                    Music = music,
                    MapId = mapId,
                    GridMapId = originalMapId,
                    Data = mapData,
                    ShopAllowed = mapId == 147
                };
                // ReSharper disable once AccessToDisposedClosure
                if (DaoFactory.MapDao.LoadById(mapId: map.MapId) == null && !maps.ContainsKey(key: map.MapId))
                    // ReSharper disable once AccessToDisposedClosure
                    maps[key: map.MapId] = map;
            }

            var mapsToInsert = maps.GetAllItems()
                .Where(predicate: s => DaoFactory.MapDao.LoadById(mapId: s.MapId) == null).ToList();
            DaoFactory.MapDao.Insert(maps: mapsToInsert);
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MAPS_PARSED"),
                arg0: mapsToInsert.Count));
            dictionaryMusic.Dispose();
            maps.Dispose();
        }

        public static void ImportMapType()
        {
            var list = DaoFactory.MapTypeDao.LoadAll().ToList();
            var mt1 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act1,
                MapTypeName = "Act1",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt1.MapTypeId)) DaoFactory.MapTypeDao.Insert(mapType: ref mt1);
            var mt2 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act2,
                MapTypeName = "Act2",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt2.MapTypeId)) DaoFactory.MapTypeDao.Insert(mapType: ref mt2);
            var mt3 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act3,
                MapTypeName = "Act3",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt3.MapTypeId)) DaoFactory.MapTypeDao.Insert(mapType: ref mt3);
            var mt4 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act4,
                MapTypeName = "Act4",
                PotionDelay = 5000
            };
            if (list.All(predicate: s => s.MapTypeId != mt4.MapTypeId)) DaoFactory.MapTypeDao.Insert(mapType: ref mt4);
            var mt5 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act51,
                MapTypeName = "Act5.1",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct5,
                ReturnMapTypeId = (long)RespawnType.ReturnAct5
            };
            if (list.All(predicate: s => s.MapTypeId != mt5.MapTypeId)) DaoFactory.MapTypeDao.Insert(mapType: ref mt5);
            var mt6 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act52,
                MapTypeName = "Act5.2",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct5,
                ReturnMapTypeId = (long)RespawnType.ReturnAct5
            };
            if (list.All(predicate: s => s.MapTypeId != mt6.MapTypeId)) DaoFactory.MapTypeDao.Insert(mapType: ref mt6);
            var mt7 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act61,
                MapTypeName = "Act6.1",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct6,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt7.MapTypeId)) DaoFactory.MapTypeDao.Insert(mapType: ref mt7);
            var mt8 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act62,
                MapTypeName = "Act6.2",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt8.MapTypeId)) DaoFactory.MapTypeDao.Insert(mapType: ref mt8);
            var mt9 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act61A,
                MapTypeName = "Act6.1a", // angel camp
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct6,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt9.MapTypeId)) DaoFactory.MapTypeDao.Insert(mapType: ref mt9);
            var mt10 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act61d,
                MapTypeName = "Act6.1d", // demon camp
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct6,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt10.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt10);
            var mt11 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.CometPlain,
                MapTypeName = "CometPlain",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt11.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt11);
            var mt12 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Mine1,
                MapTypeName = "Mine1",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt12.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt12);
            var mt13 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Mine2,
                MapTypeName = "Mine2",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt13.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt13);
            var mt14 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.MeadowOfMine,
                MapTypeName = "MeadownOfPlain",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt14.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt14);
            var mt15 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.SunnyPlain,
                MapTypeName = "SunnyPlain",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt15.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt15);
            var mt16 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Fernon,
                MapTypeName = "Fernon",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt16.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt16);
            var mt17 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.FernonF,
                MapTypeName = "FernonF",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt17.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt17);
            var mt18 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Cliff,
                MapTypeName = "Cliff",
                PotionDelay = 300,
                RespawnMapTypeId = (long)RespawnType.DefaultAct1,
                ReturnMapTypeId = (long)RespawnType.ReturnAct1
            };
            if (list.All(predicate: s => s.MapTypeId != mt18.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt18);
            var mt19 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.LandOfTheDead,
                MapTypeName = "LandOfTheDead",
                PotionDelay = 300
            };
            if (list.All(predicate: s => s.MapTypeId != mt19.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt19);
            var mt20 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Act32,
                MapTypeName = "Act 3.2",
                PotionDelay = 300
            };
            if (list.All(predicate: s => s.MapTypeId != mt20.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt20);
            var mt21 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.CleftOfDarkness,
                MapTypeName = "Cleft of Darkness",
                PotionDelay = 300
            };
            if (list.All(predicate: s => s.MapTypeId != mt21.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt21);
            var mt22 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.PvpMap,
                MapTypeName = "PVPMap",
                PotionDelay = 300
            };
            if (list.All(predicate: s => s.MapTypeId != mt22.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt22);
            var mt23 = new MapTypeDto
            {
                MapTypeId = (short)MapTypeEnum.Citadel,
                MapTypeName = "Citadel",
                PotionDelay = 300
            };
            if (list.All(predicate: s => s.MapTypeId != mt23.MapTypeId))
                DaoFactory.MapTypeDao.Insert(mapType: ref mt23);
            Logger.Info(message: Language.Instance.GetMessageFromKey(key: "MAPTYPES_PARSED"));
        }

        public static void ImportMapTypeMap()
        {
            var maptypemaps = new List<MapTypeMapDto>();
            short mapTypeId = 1;
            for (var i = 1; i < 300; i++)
            {
                var objectset = false;
                if (i < 3 || i > 48 && i < 53 || i > 67 && i < 76 || i == 102 || i > 103 && i < 105 ||
                    i > 144 && i < 149)
                {
                    // "act1"
                    mapTypeId = (short)MapTypeEnum.Act1;
                    objectset = true;
                }
                else if (i > 19 && i < 34 || i > 52 && i < 68 || i > 84 && i < 101)
                {
                    // "act2"
                    mapTypeId = (short)MapTypeEnum.Act2;
                    objectset = true;
                }
                else if (i > 40 && i < 45 || i > 45 && i < 48 || i > 99 && i < 102 || i > 104 && i < 128)
                {
                    // "act3"
                    mapTypeId = (short)MapTypeEnum.Act3;
                    objectset = true;
                }
                else if (i == 260)
                {
                    // "act3.2"
                    mapTypeId = (short)MapTypeEnum.Act32;
                    objectset = true;
                }
                else if (i > 129 && i <= 134 || i == 135 || i == 137 || i == 139 || i == 141 || i > 150 && i < 155)
                {
                    // "act4"
                    mapTypeId = (short)MapTypeEnum.Act4;
                    objectset = true;
                }
                else if (i > 169 && i < 205)
                {
                    // "act5.1"
                    mapTypeId = (short)MapTypeEnum.Act51;
                    objectset = true;
                }
                else if (i > 204 && i < 221)
                {
                    // "act5.2"
                    mapTypeId = (short)MapTypeEnum.Act52;
                    objectset = true;
                }
                else if (i > 227 && i < 241)
                {
                    // "act6.1"
                    mapTypeId = (short)MapTypeEnum.Act61;
                    objectset = true;
                }
                else if (i > 239 && i < 251 || i == 299)
                {
                    // "act6.2"
                    mapTypeId = (short)MapTypeEnum.Act62;
                    objectset = true;
                }
                else if (i == 103)
                {
                    // "Comet plain"
                    mapTypeId = (short)MapTypeEnum.CometPlain;
                    objectset = true;
                }
                else if (i == 6)
                {
                    // "Mine1"
                    mapTypeId = (short)MapTypeEnum.Mine1;
                    objectset = true;
                }
                else if (i > 6 && i < 9)
                {
                    // "Mine2"
                    mapTypeId = (short)MapTypeEnum.Mine2;
                    objectset = true;
                }
                else if (i == 3)
                {
                    // "Meadown of mine"
                    mapTypeId = (short)MapTypeEnum.MeadowOfMine;
                    objectset = true;
                }
                else if (i == 4)
                {
                    // "Sunny plain"
                    mapTypeId = (short)MapTypeEnum.SunnyPlain;
                    objectset = true;
                }
                else if (i == 5)
                {
                    // "Fernon"
                    mapTypeId = (short)MapTypeEnum.Fernon;
                    objectset = true;
                }
                else if (i > 9 && i < 19 || i > 79 && i < 85)
                {
                    // "FernonF"
                    mapTypeId = (short)MapTypeEnum.FernonF;
                    objectset = true;
                }
                else if (i > 75 && i < 79)
                {
                    // "Cliff"
                    mapTypeId = (short)MapTypeEnum.Cliff;
                    objectset = true;
                }
                else if (i == 150)
                {
                    // "Land of the dead"
                    mapTypeId = (short)MapTypeEnum.LandOfTheDead;
                    objectset = true;
                }
                else if (i == 138)
                {
                    // "Cleft of Darkness"
                    mapTypeId = (short)MapTypeEnum.CleftOfDarkness;
                    objectset = true;
                }
                else if (i == 9305)
                {
                    // "PVPMap"
                    mapTypeId = (short)MapTypeEnum.PvpMap;
                    objectset = true;
                }
                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                else if (i == 130 && i == 131)
                {
                    // "Citadel"
                    mapTypeId = (short)MapTypeEnum.Citadel;
                    objectset = true;
                }

                // add "act6.1a" and "act6.1d" when ids found
                if (objectset && DaoFactory.MapDao.LoadById(mapId: (short)i) != null &&
                    DaoFactory.MapTypeMapDao.LoadByMapAndMapType(mapId: (short)i, maptypeId: mapTypeId) == null)
                    maptypemaps.Add(item: new MapTypeMapDto { MapId = (short)i, MapTypeId = mapTypeId });
            }

            DaoFactory.MapTypeMapDao.Insert(mapTypeMaps: maptypemaps);
        }

        public void ImportMonsters()
        {
            short map = 0;
            var mobMvPacketsList = new ConcurrentBag<int>();
            var monsters = new List<MapMonsterDto>();

            Parallel.ForEach(
                source: _packetList.Where(predicate: o => o[0].Equals(value: "mv") && o[1].Equals(value: "3")),
                body: currentPacket =>
                {
                    if (!mobMvPacketsList.Contains(value: int.Parse(s: currentPacket[2])))
                        mobMvPacketsList.Add(item: int.Parse(s: currentPacket[2]));
                });
            foreach (var currentPacket in _packetList.Where(predicate: o =>
                o[0].Equals(value: "in") || o[0].Equals(value: "c_map")))
            {
                if (currentPacket.Length > 3 && currentPacket[0] == "c_map")
                {
                    map = short.Parse(s: currentPacket[2]);
                    continue;
                }

                if (currentPacket.Length > 7 && currentPacket[0] == "in" && currentPacket[1] == "3")
                {
                    var monster = new MapMonsterDto
                    {
                        MapId = map,
                        MonsterVNum = short.Parse(s: currentPacket[2]),
                        MapMonsterId = int.Parse(s: currentPacket[3]),
                        MapX = short.Parse(s: currentPacket[4]),
                        MapY = short.Parse(s: currentPacket[5]),
                        Position = (byte)(currentPacket[6]?.Length == 0 ? 0 : byte.Parse(s: currentPacket[6])),
                        IsDisabled = false
                    };
                    monster.IsMoving = mobMvPacketsList.Contains(value: monster.MapMonsterId);
                    if (DaoFactory.NpcMonsterDao.LoadByVNum(npcMonsterVNum: monster.MonsterVNum) == null ||
                        DaoFactory.MapMonsterDao.LoadById(mapMonsterId: monster.MapMonsterId) != null ||
                        monsters.Any(predicate: i => i.MapMonsterId == monster.MapMonsterId)) continue;
                    monsters.Add(item: monster);
                }
            }

            DaoFactory.MapMonsterDao.Insert(mapMonsters: monsters);
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "MONSTERS_PARSED"),
                arg0: monsters.Count));
        }

        public void ImportNpcMonsterData()
        {
            var npcMonsterDataPartitioner =
                Partitioner.Create(
                    source: _packetList.Where(predicate: o => o[0].Equals(value: "e_info") && o[1].Equals(value: "10")),
                    partitionerOptions: EnumerablePartitionerOptions.NoBuffering);
            Parallel.ForEach(source: npcMonsterDataPartitioner,
                parallelOptions: new ParallelOptions { MaxDegreeOfParallelism = 8 },
                body: currentPacket =>
                {
                    if (currentPacket.Length > 25)
                    {
                        var npcMonster =
                            DaoFactory.NpcMonsterDao.LoadByVNum(npcMonsterVNum: short.Parse(s: currentPacket[2]));
                        if (npcMonster == null) return;
                        npcMonster.AttackClass = byte.Parse(s: currentPacket[5]);
                        npcMonster.AttackUpgrade = byte.Parse(s: currentPacket[7]);
                        npcMonster.DamageMinimum = short.Parse(s: currentPacket[8]);
                        npcMonster.DamageMaximum = short.Parse(s: currentPacket[9]);
                        npcMonster.Concentrate = short.Parse(s: currentPacket[10]);
                        npcMonster.CriticalChance = byte.Parse(s: currentPacket[11]);
                        npcMonster.CriticalRate = short.Parse(s: currentPacket[12]);
                        npcMonster.DefenceUpgrade = byte.Parse(s: currentPacket[13]);
                        npcMonster.CloseDefence = short.Parse(s: currentPacket[14]);
                        npcMonster.DefenceDodge = short.Parse(s: currentPacket[15]);
                        npcMonster.DistanceDefence = short.Parse(s: currentPacket[16]);
                        npcMonster.DistanceDefenceDodge = short.Parse(s: currentPacket[17]);
                        npcMonster.MagicDefence = short.Parse(s: currentPacket[18]);
                        npcMonster.FireResistance = sbyte.Parse(s: currentPacket[19]);
                        npcMonster.WaterResistance = sbyte.Parse(s: currentPacket[20]);
                        npcMonster.LightResistance = sbyte.Parse(s: currentPacket[21]);
                        npcMonster.DarkResistance = sbyte.Parse(s: currentPacket[22]);

                        // TODO: BCard Buff parsing
                        DaoFactory.NpcMonsterDao.InsertOrUpdate(npcMonster: ref npcMonster);
                    }
                });
        }

        public void ImportNpcMonsters()
        {
            var basicHp = new int[101];
            var basicMp = new int[101];
            var basicXp = new int[101];
            var basicJXp = new int[101];

            // basicHpLoad
            var baseHp = 138;
            var basup = 17;
            for (var i = 0; i < 100; i++)
            {
                basicHp[i] = baseHp;
                basup++;
                baseHp += basup;

                if (i == 37)
                {
                    baseHp = 1765;
                    basup = 65;
                }

                if (i >= 41 && (99 - i) % 8 == 0) basup++;
            }

            const int baseMp = 10;
            var baseMpup = 5;
            // basicMpLoad
            var x = 0;
            for (var i = 0; i < 100; i++)
            {
                basicMp[i] = i == 0 ? baseMp : basicMp[i - 1];

                if (i == 1) continue;
                if (i > 3)
                {
                    if (x != 3)
                        x++;
                    else
                        x = 0;
                }

                if (x > 1) baseMpup++;

                basicMp[i] += baseMpup;
            }

            // basicXPLoad
            for (var i = 0; i < 100; i++) basicXp[i] = i * 180;

            // basicJXpLoad
            for (var i = 0; i < 100; i++) basicJXp[i] = 360;

            var fileNpcId = $"{_folder}\\monster.dat";
            var fileNpcLang =
                $"{_folder}\\_code_{ConfigurationManager.AppSettings[name: nameof(Language)]}_monster.txt";
            var npcs = new List<NpcMonsterDto>();

            // Store like this: (vnum, (name, level))
            var dictionaryIdLang = new Dictionary<string, string>();
            var npc = new NpcMonsterDto();
            var drops = new List<DropDto>();
            var monstercards = new List<BCardDto>();
            var skills = new List<NpcMonsterSkillDto>();
            string line;
            var itemAreaBegin = false;
            long unknownData = 0;
            using (var npcIdLangStream =
                new StreamReader(path: fileNpcLang, encoding: Encoding.GetEncoding(codepage: 1252)))
            {
                while ((line = npcIdLangStream.ReadLine()) != null)
                {
                    var linesave = line.Split('\t');
                    if (linesave.Length > 1 && !dictionaryIdLang.ContainsKey(key: linesave[0]))
                        dictionaryIdLang.Add(key: linesave[0], value: linesave[1]);
                }
            }

            using (var npcIdStream = new StreamReader(path: fileNpcId, encoding: Encoding.GetEncoding(codepage: 1252)))
            {
                while ((line = npcIdStream.ReadLine()) != null)
                {
                    var currentLine = line.Split('\t');

                    if (currentLine.Length > 2 && currentLine[1] == "VNUM")
                    {
                        npc = new NpcMonsterDto
                        {
                            NpcMonsterVNum = short.Parse(s: currentLine[2])
                        };
                        itemAreaBegin = true;
                        unknownData = 0;
                        DaoFactory.BCardDao.DeleteByMonsterVNum(monsterVNum: npc.NpcMonsterVNum);
                    }
                    else if (currentLine.Length > 2 && currentLine[1] == "NAME")
                    {
                        npc.Name = dictionaryIdLang.ContainsKey(key: currentLine[2])
                            ? dictionaryIdLang[key: currentLine[2]]
                            : "";
                    }
                    else if (currentLine.Length > 2 && currentLine[1] == "LEVEL")
                    {
                        if (!itemAreaBegin) continue;
                        npc.Level = byte.Parse(s: currentLine[2]);
                    }
                    else if (currentLine.Length > 3 && currentLine[1] == "RACE")
                    {
                        npc.Race = byte.Parse(s: currentLine[2]);
                        npc.RaceType = byte.Parse(s: currentLine[3]);
                    }
                    else if (currentLine.Length > 7 && currentLine[1] == "ATTRIB")
                    {
                        npc.Element = byte.Parse(s: currentLine[2]);
                        npc.ElementRate = short.Parse(s: currentLine[3]);
                        npc.FireResistance = Convert.ToInt16(value: currentLine[4]);
                        npc.WaterResistance = Convert.ToInt16(value: currentLine[5]);
                        npc.LightResistance = Convert.ToInt16(value: currentLine[6]);
                        npc.DarkResistance = Convert.ToInt16(value: currentLine[7]);
                    }
                    else if (currentLine.Length > 3 && currentLine[1] == "HP/MP") //Default = 3
                    {
                        npc.MaxHp = int.Parse(s: currentLine[2]) + basicHp[npc.Level];
                        npc.MaxMp = int.Parse(s: currentLine[3]) + basicMp[npc.Level];
                        switch (npc.Race)
                        {
                            // TODO: Race Types 1, 2, 4, 5 and 7 are either missing or not 100% correct - test it.
                            case 2:
                            case 3:
                                npc.MaxMp += npc.Level * 4 + 46;
                                break;

                            case 6:
                                npc.MaxMp += 715;
                                break;

                            case 8:
                                npc.MaxMp = 4;
                                break;
                        }
                    }
                    else if (currentLine.Length > 3 && currentLine[1] == "EXP")
                    {
                        npc.Xp = Math.Abs(value: int.Parse(s: currentLine[2]) + basicXp[npc.Level]);
                        npc.JobXp = int.Parse(s: currentLine[3]) + basicJXp[npc.Level];
                        npc.HeroXp = npc.NpcMonsterVNum switch
                        {
                            2500 => 533,
                            2501 => 534,
                            2502 => 535,
                            2503 => 614,
                            2510 => 534,
                            2511 => 533,
                            2512 => 535,
                            2513 => 651,
                            2521 => 170,
                            2522 => 286,
                            2523 => 328,
                            2525 => 261,
                            var _ => 0
                        };
                    }
                    else if (currentLine.Length > 6 && currentLine[1] == "PREATT")
                    {
                        npc.IsHostile = currentLine[2] != "0";
                        npc.NoticeRange = byte.Parse(s: currentLine[4]);
                        npc.Speed = byte.Parse(s: currentLine[5]);
                        npc.RespawnTime = int.Parse(s: currentLine[6]);
                    }
                    else if (currentLine.Length > 6 && currentLine[1] == "WEAPON")
                    {
                        if (currentLine[3] == "1")
                        {
                            var line2 = (short)(short.Parse(s: currentLine[2]) - 1);
                            npc.DamageMinimum = (short)(line2 * 4 + 32 + short.Parse(s: currentLine[4]) +
                                                         Math.Round(d: Convert.ToDecimal(value: (npc.Level - 1) / 5)));
                            npc.DamageMaximum = (short)(line2 * 6 + 40 + short.Parse(s: currentLine[5]) -
                                                         Math.Round(d: Convert.ToDecimal(value: (npc.Level - 1) / 5)));
                            npc.Concentrate = (short)(line2 * 5 + 27 + short.Parse(s: currentLine[6]));
                            npc.CriticalChance = (byte)(4 + short.Parse(s: currentLine[7]));
                            npc.CriticalRate = (short)(70 + short.Parse(s: currentLine[8]));
                        }
                        else if (currentLine[3] == "2")
                        {
                            var line2 = short.Parse(s: currentLine[2]);
                            npc.DamageMinimum = (short)(line2 * 6.5f + 23 + short.Parse(s: currentLine[4]));
                            npc.DamageMaximum = (short)((line2 - 1) * 8 + 38 + short.Parse(s: currentLine[5]));
                            npc.Concentrate = (short)(70 + short.Parse(s: currentLine[6]));
                        }
                    }
                    else if (currentLine.Length > 6 && currentLine[1] == "ARMOR")
                    {
                        var line2 = (short)(short.Parse(s: currentLine[2]) - 1);
                        npc.CloseDefence = (short)(line2 * 2 + 18);
                        npc.DistanceDefence = (short)(line2 * 3 + 17);
                        npc.MagicDefence = (short)(line2 * 2 + 13);
                        npc.DefenceDodge = (short)(line2 * 5 + 31);
                        npc.DistanceDefenceDodge = (short)(line2 * 5 + 31);
                    }
                    else if (currentLine.Length > 7 && currentLine[1] == "ETC")
                    {
                        unknownData = Convert.ToInt64(value: currentLine[2]);
                        npc.Catch = currentLine[2] == "8";
                        if (unknownData == -2147481593) npc.MonsterType = MonsterType.Special;
                        if (unknownData == -2147483616 || unknownData == -2147483647 || unknownData == -2147483646)
                            npc.NoAggresiveIcon = npc.Race == 8 && npc.RaceType == 0;
                        if (npc.NpcMonsterVNum >= 588 && npc.NpcMonsterVNum <= 607) npc.MonsterType = MonsterType.Elite;
                    }
                    else if (currentLine.Length > 6 && currentLine[1] == "SETTING")
                    {
                        if (currentLine[4] != "0")
                        {
                            npc.VNumRequired = short.Parse(s: currentLine[4]);
                            npc.AmountRequired = 1;
                        }
                    }
                    else if (currentLine.Length > 4 && currentLine[1] == "PETINFO")
                    {
                        if (npc.VNumRequired == 0 && (unknownData == -2147481593 || unknownData == -2147481599 ||
                                                      unknownData == -1610610681))
                        {
                            npc.VNumRequired = short.Parse(s: currentLine[2]);
                            npc.AmountRequired = byte.Parse(s: currentLine[3]);
                        }
                    }
                    else if (currentLine.Length > 2 && currentLine[1] == "EFF")
                    {
                        npc.BasicSkill = short.Parse(s: currentLine[2]);
                    }
                    else if (currentLine.Length > 8 && currentLine[1] == "ZSKILL")
                    {
                        npc.AttackClass = byte.Parse(s: currentLine[2]);
                        switch (npc.NpcMonsterVNum)
                        {
                            case 45:
                            case 46:
                            case 47:
                            case 48:
                            case 49:
                            case 50:
                            case 51:
                            case 52:
                            case 53:
                            case 195:
                            case 208:
                            case 209:
                                npc.BasicRange = 0;
                                break;
                            case var _:
                                npc.BasicRange = byte.Parse(s: currentLine[3]);
                                break;
                        }

                        npc.BasicArea = byte.Parse(s: currentLine[5]);
                        npc.BasicCooldown = short.Parse(s: currentLine[6]);
                    }
                    else if (currentLine.Length > 4 && currentLine[1] == "WINFO")
                    {
                        npc.AttackUpgrade = byte.Parse(s: unknownData == 1 ? currentLine[2] : currentLine[4]);
                    }
                    else if (currentLine.Length > 3 && currentLine[1] == "AINFO")
                    {
                        npc.DefenceUpgrade = byte.Parse(s: unknownData == 1 ? currentLine[2] : currentLine[3]);
                    }
                    else if (currentLine.Length > 1 && currentLine[1] == "SKILL")
                    {
                        for (var i = 2; i < currentLine.Length - 3; i += 3)
                        {
                            var vnum = short.Parse(s: currentLine[i]);
                            if (vnum == -1 || vnum == 0) continue;
                            if (DaoFactory.SkillDao.LoadById(skillId: vnum) == null || DaoFactory.NpcMonsterSkillDao
                                .LoadByNpcMonster(npcId: npc.NpcMonsterVNum)
                                .Any(predicate: s => s.SkillVNum == vnum)) continue;
                            skills.Add(item: new NpcMonsterSkillDto
                            {
                                SkillVNum = vnum,
                                Rate = short.Parse(s: currentLine[i + 1]),
                                NpcMonsterVNum = npc.NpcMonsterVNum
                            });
                        }
                    }
                    else if (currentLine.Length > 1 && currentLine[1] == "CARD")
                    {
                        for (var i = 0; i < 4; i++)
                        {
                            var type = (byte)int.Parse(s: currentLine[2 + 5 * i]);
                            if (type != 0 && type != 255)
                            {
                                var first = int.Parse(s: currentLine[3 + 5 * i]);
                                var itemCard = new BCardDto
                                {
                                    NpcMonsterVNum = npc.NpcMonsterVNum,
                                    Type = type,
                                    SubType = (byte)(int.Parse(s: currentLine[5 + 5 * i]) + 1),
                                    IsLevelScaled = Convert.ToBoolean(value: first % 4),
                                    IsLevelDivided = Math.Abs(value: first % 4) == 2,
                                    FirstData = (short)(first / 4),
                                    SecondData = (short)(int.Parse(s: currentLine[4 + 5 * i]) / 4),
                                    ThirdData = (short)(int.Parse(s: currentLine[6 + 5 * i]) / 4)
                                };
                                monstercards.Add(item: itemCard);
                            }
                        }
                    }
                    else if (currentLine.Length > 1 && currentLine[1] == "BASIC")
                    {
                        for (var i = 0; i < 10; i++)
                        {
                            var type = (byte)int.Parse(s: currentLine[2 + 5 * i]);
                            if (type != 0)
                            {
                                var subType = (byte)int.Parse(s: currentLine[6 + 5 * i]);
                                int firstData = (short)int.Parse(s: currentLine[5 + 5 * i]);
                                int thirdData = (short)(int.Parse(s: currentLine[3 + 5 * i]) / 4);
                                var itemCard = new BCardDto
                                {
                                    NpcMonsterVNum = npc.NpcMonsterVNum,
                                    Type = type,
                                    SubType = subType > 0 ? subType : (byte)(firstData + 1),
                                    FirstData = subType > 0 ? firstData : thirdData,
                                    SecondData = (short)(int.Parse(s: currentLine[4 + 5 * i]) / 4),
                                    ThirdData = subType > 0 ? thirdData : subType,
                                    CastType = 1,
                                    IsLevelScaled = false,
                                    IsLevelDivided = false
                                };
                                monstercards.Add(item: itemCard);
                            }
                        }
                    }
                    else if (currentLine.Length > 3 && currentLine[1] == "ITEM")
                    {
                        if (DaoFactory.NpcMonsterDao.LoadByVNum(npcMonsterVNum: npc.NpcMonsterVNum) == null)
                            npcs.Add(item: npc);
                        for (var i = 2; i < currentLine.Length - 3; i += 3)
                        {
                            var vnum = short.Parse(s: currentLine[i]);
                            if (vnum == -1) break;
                            if (DaoFactory.DropDao.LoadByMonster(monsterVNum: npc.NpcMonsterVNum)
                                .Any(predicate: s => s.ItemVNum == vnum))
                                continue;
                            drops.Add(item: new DropDto
                            {
                                ItemVNum = vnum,
                                Amount = int.Parse(s: currentLine[i + 2]),
                                MonsterVNum = npc.NpcMonsterVNum,
                                DropChance = int.Parse(s: currentLine[i + 1])
                            });
                        }

                        itemAreaBegin = false;
                    }
                }

                npcs = npcs.Where(predicate: s =>
                    DaoFactory.NpcMonsterDao.LoadByVNum(npcMonsterVNum: s.NpcMonsterVNum) == null).ToList();
                DaoFactory.NpcMonsterDao.Insert(npcMonsters: npcs);
                DaoFactory.NpcMonsterSkillDao.Insert(skills: skills);
                DaoFactory.BCardDao.Insert(cards: monstercards);
                Logger.Info(message: string.Format(
                    format: Language.Instance.GetMessageFromKey(key: "NPCMONSTERS_PARSED"), arg0: npcs.Count));
            }

            if (DaoFactory.DropDao.LoadAll().Count == 0)
            {
                // Act 1
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 12000,
                    MapTypeId = (short)MapTypeEnum.Act1
                });

                // Act2
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1004,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1007,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 7000,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1028,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1086,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1237,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1239,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1241,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2098,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2100,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2101,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 900,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 900,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 900,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2117,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 900,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2118,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 900,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2129,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2205,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2206,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2207,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2208,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2282,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 2500,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2283,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2284,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2296,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 250,
                    MapTypeId = (short)MapTypeEnum.Act2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.Act2
                });

                // Act3
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1004,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1007,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 8000,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1086,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1078,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1235,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1237,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1238,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1239,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1240,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1241,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2098,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2100,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2101,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2117,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2118,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2129,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2205,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2206,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2207,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2208,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2282,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 4500,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2283,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2284,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 350,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2285,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2296,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act3
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.Act3
                });

                // Act3.2
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1004,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1007,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 8000,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1086,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1078,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1235,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1237,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1238,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1239,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1240,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1241,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2098,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2100,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2101,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2117,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2118,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2129,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2205,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2206,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2207,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2208,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2282,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 4500,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2283,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2284,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 350,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2285,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2296,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act32
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.Act32
                });

                // Act4
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1004,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1007,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1010,
                    Amount = 3,
                    MonsterVNum = null,
                    DropChance = 1500,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 2,
                    MonsterVNum = null,
                    DropChance = 3000,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1241,
                    Amount = 3,
                    MonsterVNum = null,
                    DropChance = 3000,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1078,
                    Amount = 3,
                    MonsterVNum = null,
                    DropChance = 1500,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1246,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 2500,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1247,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 2500,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1248,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 2500,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1429,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 2500,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2296,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2307,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1500,
                    MapTypeId = (short)MapTypeEnum.Act4
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2308,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1500,
                    MapTypeId = (short)MapTypeEnum.Act4
                });

                // Act5
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1004,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1007,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 6000,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1086,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1872,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1873,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1874,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2117,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2129,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2206,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2207,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2282,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 2500,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2283,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 800,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2284,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2285,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2351,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 800,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2379,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1000,
                    MapTypeId = (short)MapTypeEnum.Act51
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.Act51
                });

                // Act5.2
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1004,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1007,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 5000,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1086,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1092,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1093,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1500,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1094,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2098,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1500,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2117,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2206,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2379,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 3000,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2380,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 6000,
                    MapTypeId = (short)MapTypeEnum.Act52
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act52
                });

                // Act6.1
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1004,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1007,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1010,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 5000,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1028,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1078,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1086,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1092,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1093,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1094,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2098,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2117,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 800,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2129,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2206,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2803,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2804,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2805,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2806,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2807,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Act61
                });

                // drops.Add(new DropDTO { ItemVNum = 2815, Amount = 1, MonsterVNum = null, DropChance =
                // 450, MapTypeId = 9 }); //Only for angel camp need group act6.1 angel
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2816,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 350,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2818,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2819,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 350,
                    MapTypeId = (short)MapTypeEnum.Act61
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 150,
                    MapTypeId = (short)MapTypeEnum.Act61
                });

                // drops.Add(new DropDTO { ItemVNum = 5881, Amount = 1, MonsterVNum = null, DropChance =
                // 450, MapTypeId = 10 }); //Only for demon camp need group act6.1 demon

                // Act6.2 (need some information) > soon )

                // Comet plain
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1004,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1007,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 7000,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2098,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2100,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2101,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2117,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2205,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2206,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2207,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2208,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2296,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.CometPlain
                });

                // Mine1
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1002,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Mine1
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1005,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Mine1
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 11000,
                    MapTypeId = (short)MapTypeEnum.Mine1
                });

                // Mine2
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1002,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1005,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 11000,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1241,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2100,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2101,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2205,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Mine2
                });

                // MeadownOfMine
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1002,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.MeadowOfMine
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1005,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.MeadowOfMine
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 10000,
                    MapTypeId = (short)MapTypeEnum.MeadowOfMine
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2016,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.MeadowOfMine
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2023,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.MeadowOfMine
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.MeadowOfMine
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2118,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.MeadowOfMine
                });

                // SunnyPlain
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1003,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1006,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 8000,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1078,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1092,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1093,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1094,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2098,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2100,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2101,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2118,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2205,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2206,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2207,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2208,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2296,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.SunnyPlain
                });

                // Fernon
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1003,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1006,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 9000,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1092,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1093,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1094,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2098,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2100,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2101,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2117,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2296,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.Fernon
                });

                // FernonF
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1004,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1007,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 9000,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1078,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1092,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1093,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1094,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 500,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2098,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2100,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2101,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 200,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2115,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2116,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2117,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 700,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2205,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2206,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2207,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2208,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2296,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.FernonF
                });

                // Cliff
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 8000,
                    MapTypeId = (short)MapTypeEnum.Cliff
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2098,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Cliff
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2099,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Cliff
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2100,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Cliff
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2101,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Cliff
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2102,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Cliff
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 2296,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.Cliff
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 30,
                    MapTypeId = (short)MapTypeEnum.Cliff
                });

                // LandOfTheDead
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1007,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 800,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1010,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 800,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1012,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 8000,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1015,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1016,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1078,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1114,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 400,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1019,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 2000,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1020,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 1200,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1021,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 600,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1022,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 300,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 1211,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 250,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
                drops.Add(item: new DropDto
                {
                    ItemVNum = 5119,
                    Amount = 1,
                    MonsterVNum = null,
                    DropChance = 100,
                    MapTypeId = (short)MapTypeEnum.LandOfTheDead
                });
            }

            DaoFactory.DropDao.Insert(drops: drops);
        }

        public void ImportPackets()
        {
            var filePacket = $"{_folder}\\packet.txt";
            using var packetTxtStream =
                new StreamReader(path: filePacket, encoding: Encoding.GetEncoding(codepage: 1252));
            string line;
            while ((line = packetTxtStream.ReadLine()) != null)
            {
                var linesave = line.Split(' ');
                _packetList.Add(item: linesave);
            }
        }

        public void ImportPortals()
        {
            var listPortals1 = new List<PortalDto>();
            var listPortals2 = new List<PortalDto>();
            short map = 0;

            var lodPortal = new PortalDto
            {
                SourceMapId = 150,
                SourceX = 172,
                SourceY = 171,
                DestinationMapId = 98,
                Type = -1,
                DestinationX = 6,
                DestinationY = 36,
                IsDisabled = false
            };
            DaoFactory.PortalDao.Insert(portal: lodPortal);

            var minilandPortal = new PortalDto
            {
                SourceMapId = 20001,
                SourceX = 3,
                SourceY = 8,
                DestinationMapId = 1,
                Type = -1,
                DestinationX = 48,
                DestinationY = 132,
                IsDisabled = false
            };
            DaoFactory.PortalDao.Insert(portal: minilandPortal);

            var weddingPortal = new PortalDto
            {
                SourceMapId = 2586,
                SourceX = 34,
                SourceY = 54,
                DestinationMapId = 145,
                Type = -1,
                DestinationX = 61,
                DestinationY = 165,
                IsDisabled = false
            };
            DaoFactory.PortalDao.Insert(portal: weddingPortal);

            var glacerusCavernPortal = new PortalDto
            {
                SourceMapId = 2587,
                SourceX = 42,
                SourceY = 3,
                DestinationMapId = 189,
                Type = -1,
                DestinationX = 48,
                DestinationY = 156,
                IsDisabled = false
            };
            DaoFactory.PortalDao.Insert(portal: glacerusCavernPortal);

            foreach (var currentPacket in _packetList.Where(predicate: o =>
                o[0].Equals(value: "c_map") || o[0].Equals(value: "gp")))
            {
                if (currentPacket.Length > 3 && currentPacket[0] == "c_map")
                {
                    map = short.Parse(s: currentPacket[2]);
                    continue;
                }

                if (currentPacket.Length > 4 && currentPacket[0] == "gp")
                {
                    var portal = new PortalDto
                    {
                        SourceMapId = map,
                        SourceX = short.Parse(s: currentPacket[1]),
                        SourceY = short.Parse(s: currentPacket[2]),
                        DestinationMapId = short.Parse(s: currentPacket[3]),
                        Type = sbyte.Parse(s: currentPacket[4]),
                        DestinationX = -1,
                        DestinationY = -1,
                        IsDisabled = false
                    };

                    if (listPortals1.Any(predicate: s =>
                            s.SourceMapId == map && s.SourceX == portal.SourceX && s.SourceY == portal.SourceY &&
                            s.DestinationMapId == portal.DestinationMapId) ||
                        _maps.All(predicate: s => s.MapId != portal.SourceMapId) ||
                        _maps.All(predicate: s => s.MapId != portal.DestinationMapId))
                        // Portal already in list
                        continue;

                    listPortals1.Add(item: portal);
                }
            }

            listPortals1 = listPortals1.OrderBy(keySelector: s => s.SourceMapId)
                .ThenBy(keySelector: s => s.DestinationMapId)
                .ThenBy(keySelector: s => s.SourceY).ThenBy(keySelector: s => s.SourceX).ToList();
            foreach (var portal in listPortals1)
            {
                var p = listPortals1.Except(second: listPortals2).FirstOrDefault(predicate: s =>
                    s.SourceMapId == portal.DestinationMapId && s.DestinationMapId == portal.SourceMapId);
                if (p == null) continue;

                portal.DestinationX = p.SourceX;
                portal.DestinationY = p.SourceY;
                p.DestinationY = portal.SourceY;
                p.DestinationX = portal.SourceX;
                listPortals2.Add(item: p);
                listPortals2.Add(item: portal);
            }

            // foreach portal in the new list of Portals where none (=> !Any()) are found in the existing
            var portalCounter = listPortals2.Count(predicate: portal => !DaoFactory.PortalDao
                .LoadByMap(mapId: portal.SourceMapId).Any(
                    predicate: s => s.DestinationMapId == portal.DestinationMapId && s.SourceX == portal.SourceX &&
                                    s.SourceY == portal.SourceY));

            // so this dude doesnt exist yet in DAOFactory -> insert it
            DaoFactory.PortalDao.Insert(portals: listPortals2.Where(predicate: portal => !DaoFactory.PortalDao
                .LoadByMap(mapId: portal.SourceMapId)
                .Any(
                    predicate: s => s.DestinationMapId == portal.DestinationMapId && s.SourceX == portal.SourceX &&
                                    s.SourceY == portal.SourceY)).ToList());

            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "PORTALS_PARSED"),
                arg0: portalCounter));
        }

        public void ImportRecipe()
        {
            var count = 0;
            var mapNpcId = 0;
            short itemVNum = 0;
            RecipeDto recipe;
            RecipeListDto recipeListDTO;

            foreach (var currentPacket in _packetList.Where(predicate: o =>
                o[0].Equals(value: "n_run") || o[0].Equals(value: "pdtse") || o[0].Equals(value: "m_list")))
            {
                if (currentPacket.Length > 4 && currentPacket[0] == "n_run")
                {
                    int.TryParse(s: currentPacket[4], result: out mapNpcId);
                    continue;
                }

                if (currentPacket.Length > 1 && currentPacket[0] == "m_list" &&
                    (currentPacket[1] == "2" || currentPacket[1] == "4"))
                {
                    for (var i = 2; i < currentPacket.Length - 1; i++)
                    {
                        var vNum = short.Parse(s: currentPacket[i]);
                        if (DaoFactory.RecipeDao.LoadByItemVNum(itemVNum: vNum) == null)
                        {
                            recipe = new RecipeDto
                            {
                                ItemVNum = vNum
                            };
                            DaoFactory.RecipeDao.Insert(recipe: recipe);
                        }

                        var recipeForId = DaoFactory.RecipeDao.LoadByItemVNum(itemVNum: vNum);
                        if (DaoFactory.MapNpcDao.LoadById(mapNpcId: mapNpcId) != null && !DaoFactory.RecipeListDao
                            .LoadByMapNpcId(mapNpcId: mapNpcId)
                            .Any(predicate: r => r.RecipeId.Equals(obj: recipeForId.RecipeId)))
                        {
                            recipeListDTO = new RecipeListDto
                            {
                                MapNpcId = mapNpcId,
                                RecipeId = recipeForId.RecipeId
                            };

                            DaoFactory.RecipeListDao.Insert(recipeList: recipeListDTO);
                            count++;
                        }
                    }

                    continue;
                }

                if (currentPacket.Length > 2 && currentPacket[0] == "pdtse")
                {
                    itemVNum = short.Parse(s: currentPacket[2]);
                    continue;
                }

                if (currentPacket.Length > 1 && currentPacket[0] == "m_list" &&
                    (currentPacket[1] == "3" || currentPacket[1] == "5"))
                {
                    for (var i = 3; i < currentPacket.Length - 1; i += 2)
                    {
                        var rec = DaoFactory.RecipeDao.LoadByItemVNum(itemVNum: itemVNum);
                        if (rec != null)
                        {
                            rec.Amount = byte.Parse(s: currentPacket[2]);
                            DaoFactory.RecipeDao.Update(recipe: rec);
                            var recipeitem = new RecipeItemDto
                            {
                                ItemVNum = short.Parse(s: currentPacket[i]),
                                Amount = byte.Parse(s: currentPacket[i + 1]),
                                RecipeId = rec.RecipeId
                            };
                            if (!DaoFactory.RecipeItemDao
                                .LoadByRecipeAndItem(recipeId: rec.RecipeId, itemVNum: recipeitem.ItemVNum).Any())
                                DaoFactory.RecipeItemDao.Insert(recipeItem: recipeitem);
                        }
                    }

                    itemVNum = -1;
                }
            }

            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "RECIPES_PARSED"),
                arg0: count));
        }

        public static void ImportRespawnMapType()
        {
            var respawnmaptypemaps = new List<RespawnMapTypeDto>
            {
                new RespawnMapTypeDto
                {
                    RespawnMapTypeId = (long) RespawnType.DefaultAct1,
                    DefaultMapId = 1,
                    DefaultX = 80,
                    DefaultY = 116,
                    Name = "Default"
                },
                new RespawnMapTypeDto
                {
                    RespawnMapTypeId = (long) RespawnType.ReturnAct1,
                    DefaultMapId = 0,
                    DefaultX = 0,
                    DefaultY = 0,
                    Name = "Return"
                },
                new RespawnMapTypeDto
                {
                    RespawnMapTypeId = (long) RespawnType.DefaultAct5,
                    DefaultMapId = 170,
                    DefaultX = 86,
                    DefaultY = 48,
                    Name = "DefaultAct5"
                },
                new RespawnMapTypeDto
                {
                    RespawnMapTypeId = (long) RespawnType.ReturnAct5,
                    DefaultMapId = 0,
                    DefaultX = 0,
                    DefaultY = 0,
                    Name = "ReturnAct5"
                },
                new RespawnMapTypeDto
                {
                    RespawnMapTypeId = (long) RespawnType.DefaultAct6,
                    DefaultMapId = 228,
                    DefaultX = 72,
                    DefaultY = 102,
                    Name = "DefaultAct6"
                }
            };
            DaoFactory.RespawnMapTypeDao.Insert(respawnMapTypes: respawnmaptypemaps);
            Logger.Info(message: Language.Instance.GetMessageFromKey(key: "RESPAWNTYPE_PARSED"));
        }

        public void ImportScriptedInstances()
        {
            short map = 0;
            var listtimespace = new List<ScriptedInstanceDto>();
            var bddlist = new List<ScriptedInstanceDto>();
            foreach (var currentPacket in _packetList.Where(predicate: o =>
                o[0].Equals(value: "at") || o[0].Equals(value: "wp") || o[0].Equals(value: "gp") ||
                o[0].Equals(value: "rbr")))
                if (currentPacket.Length > 5 && currentPacket[0] == "at")
                {
                    map = short.Parse(s: currentPacket[2]);
                    bddlist = DaoFactory.ScriptedInstanceDao.LoadByMap(mapId: map).ToList();
                }
                else if (currentPacket.Length > 6 && currentPacket[0] == "wp")
                {
                    var ts = new ScriptedInstanceDto
                    {
                        PositionX = short.Parse(s: currentPacket[1]),
                        PositionY = short.Parse(s: currentPacket[2]),
                        MapId = map
                    };

                    if (!bddlist.Concat(second: listtimespace).Any(predicate: s =>
                        s.MapId == ts.MapId && s.PositionX == ts.PositionX && s.PositionY == ts.PositionY))
                        listtimespace.Add(item: ts);
                }
                else if (currentPacket[0] == "gp")
                {
                    if (sbyte.Parse(s: currentPacket[4]) == (byte)PortalType.Raid)
                    {
                        var ts = new ScriptedInstanceDto
                        {
                            PositionX = short.Parse(s: currentPacket[1]),
                            PositionY = short.Parse(s: currentPacket[2]),
                            MapId = map,
                            Type = ScriptedInstanceType.Raid
                        };

                        if (!bddlist.Concat(second: listtimespace).Any(predicate: s =>
                            s.MapId == ts.MapId && s.PositionX == ts.PositionX && s.PositionY == ts.PositionY))
                            listtimespace.Add(item: ts);
                    }
                }
                else if (currentPacket[0] == "rbr")
                {
                    //some info
                }

            DaoFactory.ScriptedInstanceDao.Insert(scriptedInstances: listtimespace);
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "TIMESPACES_PARSED"),
                arg0: listtimespace.Count));
        }

        public void ImportShopItems()
        {
            var shopItems = new List<ShopItemDto>();
            byte type = 0;
            foreach (var currentPacket in _packetList.Where(predicate: o =>
                o[0].Equals(value: "n_inv") || o[0].Equals(value: "shopping")))
                if (currentPacket[0].Equals(value: "n_inv"))
                {
                    if (DaoFactory.ShopDao.LoadByNpc(mapNpcId: short.Parse(s: currentPacket[2])) != null)
                        for (var i = 5; i < currentPacket.Length; i++)
                        {
                            var item = currentPacket[i].Split('.');
                            ShopItemDto shopItem = null;
                            if (item.Length == 5)
                                shopItem = new ShopItemDto
                                {
                                    ShopId = DaoFactory.ShopDao.LoadByNpc(mapNpcId: short.Parse(s: currentPacket[2]))
                                        .ShopId,
                                    Type = type,
                                    Slot = byte.Parse(s: item[1]),
                                    ItemVNum = short.Parse(s: item[2])
                                };
                            else if (item.Length == 6)
                                shopItem = new ShopItemDto
                                {
                                    ShopId = DaoFactory.ShopDao.LoadByNpc(mapNpcId: short.Parse(s: currentPacket[2]))
                                        .ShopId,
                                    Type = type,
                                    Slot = byte.Parse(s: item[1]),
                                    ItemVNum = short.Parse(s: item[2]),
                                    Rare = sbyte.Parse(s: item[3]),
                                    Upgrade = byte.Parse(s: item[4])
                                };
                            if (shopItem == null ||
                                shopItems.Any(predicate: s =>
                                    s.ItemVNum.Equals(obj: shopItem.ItemVNum) &&
                                    s.ShopId.Equals(obj: shopItem.ShopId)) ||
                                DaoFactory.ShopItemDao.LoadByShopId(shopId: shopItem.ShopId)
                                    .Any(predicate: s => s.ItemVNum.Equals(obj: shopItem.ItemVNum))) continue;
                            shopItems.Add(item: shopItem);
                        }
                }
                else if (currentPacket.Length > 3)
                {
                    type = byte.Parse(s: currentPacket[1]);
                }

            DaoFactory.ShopItemDao.Insert(items: shopItems);
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHOPITEMS_PARSED"),
                arg0: shopItems.Count));
        }

        public void ImportShops()
        {
            using var shops = new ThreadSafeSortedList<int, ShopDto>();
            Parallel.ForEach(
                source: _packetList.Where(predicate: o =>
                    o.Length > 6 && o[0].Equals(value: "shop") && o[1].Equals(value: "2")),
                body: currentPacket =>
                {
                    var npc = DaoFactory.MapNpcDao.LoadById(mapNpcId: short.Parse(s: currentPacket[2]));
                    if (npc != null)
                    {
                        // ReSharper disable once RedundantAssignment
                        var name = "";
                        var builder = new StringBuilder();
                        for (var j = 6; j < currentPacket.Length; j++)
                            builder.Append(value: currentPacket[j]).Append(value: " ");
                        name = builder.ToString();
                        name = name.Trim();
                        var shop = new ShopDto
                        {
                            Name = name,
                            MapNpcId = npc.MapNpcId,
                            MenuType = byte.Parse(s: currentPacket[4]),
                            ShopType = byte.Parse(s: currentPacket[5])
                        };
                        if (DaoFactory.ShopDao.LoadByNpc(mapNpcId: npc.MapNpcId) == null &&
                            // ReSharper disable once AccessToDisposedClosure
                            !shops.ContainsKey(key: npc.MapNpcId))
                            // ReSharper disable once AccessToDisposedClosure
                            shops[key: shop.MapNpcId] = shop;
                    }
                });
            DaoFactory.ShopDao.Insert(shops: shops.GetAllItems());
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHOPS_PARSED"),
                arg0: shops.Count));
        }

        public void ImportShopSkills()
        {
            var shopSkills = new List<ShopSkillDto>();
            byte type = 0;
            foreach (var currentPacket in _packetList.Where(predicate: o =>
                o[0].Equals(value: "n_inv") || o[0].Equals(value: "shopping")))
                if (currentPacket[0].Equals(value: "n_inv"))
                {
                    if (DaoFactory.ShopDao.LoadByNpc(mapNpcId: short.Parse(s: currentPacket[2])) != null)
                        for (var i = 5; i < currentPacket.Length; i++)
                        {
                            ShopSkillDto shopSkill;
                            if (!currentPacket[i].Contains(value: ".") && !currentPacket[i].Contains(value: "|"))
                            {
                                shopSkill = new ShopSkillDto
                                {
                                    ShopId = DaoFactory.ShopDao.LoadByNpc(mapNpcId: short.Parse(s: currentPacket[2]))
                                        .ShopId,
                                    Type = type,
                                    Slot = (byte)(i - 5),
                                    SkillVNum = short.Parse(s: currentPacket[i])
                                };
                                if (shopSkills.Any(predicate: s =>
                                        s.SkillVNum.Equals(obj: shopSkill.SkillVNum) &&
                                        s.ShopId.Equals(obj: shopSkill.ShopId)) ||
                                    DaoFactory.ShopSkillDao.LoadByShopId(shopId: shopSkill.ShopId)
                                        .Any(predicate: s => s.SkillVNum.Equals(obj: shopSkill.SkillVNum))) continue;
                                shopSkills.Add(item: shopSkill);
                            }
                        }
                }
                else if (currentPacket.Length > 3)
                {
                    type = byte.Parse(s: currentPacket[1]);
                }

            DaoFactory.ShopSkillDao.Insert(skills: shopSkills);
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SHOPSKILLS_PARSED"),
                arg0: shopSkills.Count));
        }

        public void ImportSkills()
        {
            var fileSkillId = $"{_folder}\\Skill.dat";
            var fileSkillLang =
                $"{_folder}\\_code_{ConfigurationManager.AppSettings[name: nameof(Language)]}_Skill.txt";
            var skills = new List<SkillDto>();

            var dictionaryIdLang = new Dictionary<string, string>();
            var skill = new SkillDto();
            var combos = new List<ComboDto>();
            var skillCards = new List<BCardDto>();
            string line;
            using (var skillIdLangStream =
                new StreamReader(path: fileSkillLang, encoding: Encoding.GetEncoding(codepage: 1252)))
            {
                while ((line = skillIdLangStream.ReadLine()) != null)
                {
                    var linesave = line.Split('\t');
                    if (linesave.Length > 1 && !dictionaryIdLang.ContainsKey(key: linesave[0]))
                        dictionaryIdLang.Add(key: linesave[0], value: linesave[1]);
                }
            }

            using var skillIdStream =
                new StreamReader(path: fileSkillId, encoding: Encoding.GetEncoding(codepage: 1252));
            while ((line = skillIdStream.ReadLine()) != null)
            {
                var currentLine = line.Split('\t');

                if (currentLine.Length > 2 && currentLine[1] == "VNUM")
                {
                    skill = new SkillDto
                    {
                        SkillVNum = short.Parse(s: currentLine[2])
                    };
                    DaoFactory.BCardDao.DeleteBySkillVNum(skillVNum: skill.SkillVNum);
                }
                else if (currentLine.Length > 2 && currentLine[1] == "NAME")
                {
                    skill.Name = dictionaryIdLang.TryGetValue(key: currentLine[2], value: out var name) ? name : "";
                }
                else if (currentLine.Length > 2 && currentLine[1] == "TYPE")
                {
                    skill.SkillType = byte.Parse(s: currentLine[2]);
                    skill.CastId = short.Parse(s: currentLine[3]);
                    skill.Class = byte.Parse(s: currentLine[4]);
                    skill.Type = byte.Parse(s: currentLine[5]);
                    skill.Element = byte.Parse(s: currentLine[7]);
                }
                else if (currentLine.Length > 2 && currentLine[1] == "FCOMBO")
                {
                    for (var i = 3; i < currentLine.Length - 4; i += 3)
                    {
                        var combo = new ComboDto
                        {
                            SkillVNum = skill.SkillVNum,
                            Hit = short.Parse(s: currentLine[i]),
                            Animation = short.Parse(s: currentLine[i + 1]),
                            Effect = short.Parse(s: currentLine[i + 2])
                        };

                        if (combo.Hit != 0 || combo.Animation != 0 || combo.Effect != 0)
                            if (!DaoFactory.ComboDao
                                .LoadByVNumHitAndEffect(skillVNum: combo.SkillVNum, hit: combo.Hit,
                                    effect: combo.Effect).Any())
                                combos.Add(item: combo);
                    }
                }
                else if (currentLine.Length > 3 && currentLine[1] == "COST")
                {
                    skill.CpCost = currentLine[2] == "-1" ? (byte)0 : byte.Parse(s: currentLine[2]);
                    skill.Price = int.Parse(s: currentLine[3]);
                }
                else if (currentLine.Length > 2 && currentLine[1] == "LEVEL")
                {
                    skill.LevelMinimum = currentLine[2] != "-1" ? byte.Parse(s: currentLine[2]) : (byte)0;
                    if (skill.Class > 31)
                    {
                        var firstskill = skills.Find(match: s => s.Class == skill.Class);
                        if (firstskill == null || skill.SkillVNum <= firstskill.SkillVNum + 10)
                            if (skill.Class == 8)
                                switch (skills.Count(predicate: s => s.Class == skill.Class))
                                {
                                    case 3:
                                        skill.LevelMinimum = 20;
                                        break;
                                    case 2:
                                        skill.LevelMinimum = 10;
                                        break;
                                    case var _:
                                        skill.LevelMinimum = 0;
                                        break;
                                    default:
                                        break;
                                }
                            else if (skill.Class == 9)
                                switch (skills.Count(predicate: s => s.Class == skill.Class))
                                {
                                    case 9:
                                        skill.LevelMinimum = 20;
                                        break;
                                    case 8:
                                        skill.LevelMinimum = 16;
                                        break;
                                    case 7:
                                        skill.LevelMinimum = 12;
                                        break;
                                    case 6:
                                        skill.LevelMinimum = 8;
                                        break;
                                    case 5:
                                        skill.LevelMinimum = 4;
                                        break;
                                    case var _:
                                        skill.LevelMinimum = 0;
                                        break;
                                    default:
                                        break;
                                }
                            else if (skill.Class == 16)
                                switch (skills.Count(predicate: s => s.Class == skill.Class))
                                {
                                    case 6:
                                        skill.LevelMinimum = 20;
                                        break;
                                    case 5:
                                        skill.LevelMinimum = 15;
                                        break;
                                    case 4:
                                        skill.LevelMinimum = 10;
                                        break;
                                    case 3:
                                        skill.LevelMinimum = 5;
                                        break;
                                    case 2:
                                        skill.LevelMinimum = 3;
                                        break;
                                    case var _:
                                        skill.LevelMinimum = 0;
                                        break;
                                    default:
                                        break;
                                }
                            else
                                switch (skills.Count(predicate: s => s.Class == skill.Class))
                                {
                                    case 10:
                                        skill.LevelMinimum = 20;
                                        break;
                                    case 9:
                                        skill.LevelMinimum = 16;
                                        break;
                                    case 8:
                                        skill.LevelMinimum = 12;
                                        break;
                                    case 7:
                                        skill.LevelMinimum = 8;
                                        break;
                                    case 6:
                                        skill.LevelMinimum = 4;
                                        break;
                                    case var _:
                                        skill.LevelMinimum = 0;
                                        break;
                                    default:
                                        break;
                                }
                    }

                    skill.MinimumAdventurerLevel = currentLine[3] != "-1" ? byte.Parse(s: currentLine[3]) : (byte)0;
                    skill.MinimumSwordmanLevel = currentLine[4] != "-1" ? byte.Parse(s: currentLine[4]) : (byte)0;
                    skill.MinimumArcherLevel = currentLine[5] != "-1" ? byte.Parse(s: currentLine[5]) : (byte)0;
                    skill.MinimumMagicianLevel = currentLine[6] != "-1" ? byte.Parse(s: currentLine[6]) : (byte)0;
                }
                else if (currentLine.Length > 2 && currentLine[1] == "EFFECT")
                {
                    skill.CastEffect = short.Parse(s: currentLine[3]);
                    skill.CastAnimation = short.Parse(s: currentLine[4]);
                    skill.Effect = short.Parse(s: currentLine[5]);
                    skill.AttackAnimation = short.Parse(s: currentLine[6]);
                }
                else if (currentLine.Length > 2 && currentLine[1] == "TARGET")
                {
                    skill.TargetType = byte.Parse(s: currentLine[2]);
                    skill.HitType = byte.Parse(s: currentLine[3]);
                    skill.Range = byte.Parse(s: currentLine[4]);
                    skill.TargetRange = byte.Parse(s: currentLine[5]);
                }
                else if (currentLine.Length > 2 && currentLine[1] == "DATA")
                {
                    skill.UpgradeSkill = short.Parse(s: currentLine[2]);
                    skill.UpgradeType = short.Parse(s: currentLine[3]);
                    skill.CastTime = short.Parse(s: currentLine[6]);
                    skill.Cooldown = short.Parse(s: currentLine[7]);
                    skill.MpCost = short.Parse(s: currentLine[10]);
                    skill.ItemVNum = short.Parse(s: currentLine[12]);
                }
                else if (currentLine.Length > 2 && currentLine[1] == "BASIC")
                {
                    var type = (byte)int.Parse(s: currentLine[3]);
                    if (type != 0 && type != 255)
                    {
                        var first = int.Parse(s: currentLine[5]);
                        var itemCard = new BCardDto
                        {
                            SkillVNum = skill.SkillVNum,
                            Type = type,
                            SubType = (byte)(int.Parse(s: currentLine[4]) + 1),
                            IsLevelScaled = Convert.ToBoolean(value: first % 4),
                            IsLevelDivided = Math.Abs(value: first % 4) == 2,
                            FirstData = (short)(first / 4),
                            SecondData = (short)(int.Parse(s: currentLine[6]) / 4),
                            ThirdData = (short)(int.Parse(s: currentLine[7]) / 4)
                        };
                        skillCards.Add(item: itemCard);
                    }
                }
                else if (currentLine.Length > 2 && currentLine[1] == "FCOMBO")
                {
                    // investigate
                    /*
                        if (currentLine[2] == "1")
                        {
                            combo.FirstActivationHit = byte.Parse(currentLine[3]);
                            combo.FirstComboAttackAnimation = short.Parse(currentLine[4]);
                            combo.FirstComboEffect = short.Parse(currentLine[5]);
                            combo.SecondActivationHit = byte.Parse(currentLine[3]);
                            combo.SecondComboAttackAnimation = short.Parse(currentLine[4]);
                            combo.SecondComboEffect = short.Parse(currentLine[5]);
                            combo.ThirdActivationHit = byte.Parse(currentLine[3]);
                            combo.ThirdComboAttackAnimation = short.Parse(currentLine[4]);
                            combo.ThirdComboEffect = short.Parse(currentLine[5]);
                            combo.FourthActivationHit = byte.Parse(currentLine[3]);
                            combo.FourthComboAttackAnimation = short.Parse(currentLine[4]);
                            combo.FourthComboEffect = short.Parse(currentLine[5]);
                            combo.FifthActivationHit = byte.Parse(currentLine[3]);
                            combo.FifthComboAttackAnimation = short.Parse(currentLine[4]);
                            combo.FifthComboEffect = short.Parse(currentLine[5]);
                        }
                        */
                }
                else if (currentLine.Length > 2 && currentLine[1] == "CELL")
                {
                    // investigate
                }
                else if (currentLine.Length > 1 && currentLine[1] == "Z_DESC")
                {
                    // investigate
                    skills.Add(item: skill);
                }
            }

            skills = skills.Where(predicate: s => DaoFactory.SkillDao.LoadById(skillId: s.SkillVNum) == null).ToList();
            DaoFactory.SkillDao.Insert(skills: skills);
            DaoFactory.ComboDao.Insert(combos: combos);
            DaoFactory.BCardDao.Insert(cards: skillCards);
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "SKILLS_PARSED"),
                arg0: skills.Count));
        }

        public void ImportTeleporters()
        {
            var teleporterCounter = 0;
            TeleporterDto teleporter = null;
            foreach (var currentPacket in _packetList.Where(predicate: o =>
                o[0].Equals(value: "at") || o[0].Equals(value: "n_run") &&
                (o[1].Equals(value: "16") || o[1].Equals(value: "26") || o[1].Equals(value: "45") ||
                 o[1].Equals(value: "301") ||
                 o[1].Equals(value: "132") || o[1].Equals(value: "5002") || o[1].Equals(value: "5012"))))
            {
                if (currentPacket.Length > 4 && currentPacket[0] == "n_run")
                {
                    if (DaoFactory.MapNpcDao.LoadById(mapNpcId: int.Parse(s: currentPacket[4])) == null) continue;
                    teleporter = new TeleporterDto
                    {
                        MapNpcId = int.Parse(s: currentPacket[4]),
                        Index = short.Parse(s: currentPacket[2])
                    };
                    continue;
                }

                if (currentPacket.Length > 5 && currentPacket[0] == "at")
                {
                    if (teleporter == null) continue;
                    teleporter.MapId = short.Parse(s: currentPacket[2]);
                    teleporter.MapX = short.Parse(s: currentPacket[3]);
                    teleporter.MapY = short.Parse(s: currentPacket[4]);

                    if (DaoFactory.TeleporterDao.LoadFromNpc(npcId: teleporter.MapNpcId)
                        .Any(predicate: s => s.Index == teleporter.Index)) continue;
                    DaoFactory.TeleporterDao.Insert(teleporter: teleporter);
                    teleporterCounter++;
                    teleporter = null;
                }
            }

            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "TELEPORTERS_PARSED"),
                arg0: teleporterCounter));
        }

        public void LoadMaps()
        {
            _maps = DaoFactory.MapDao.LoadAll();
        }

        internal void ImportItems()
        {
            var fileId = $"{_folder}\\Item.dat";
            var fileLang = $"{_folder}\\_code_{ConfigurationManager.AppSettings[name: nameof(Language)]}_Item.txt";
            var dictionaryName = new Dictionary<string, string>();
            string line;
            var items = new List<ItemDto>();
            var itemCards = new List<BCardDto>();
            using (var mapIdLangStream =
                new StreamReader(path: fileLang, encoding: Encoding.GetEncoding(codepage: 1252)))
            {
                while ((line = mapIdLangStream.ReadLine()) != null)
                {
                    var linesave = line.Split('\t');
                    if (linesave.Length <= 1 || dictionaryName.ContainsKey(key: linesave[0])) continue;
                    dictionaryName.Add(key: linesave[0], value: linesave[1]);
                }
            }

            using var npcIdStream = new StreamReader(path: fileId, encoding: Encoding.GetEncoding(codepage: 1252));
            var item = new ItemDto();
            var itemAreaBegin = false;
            while ((line = npcIdStream.ReadLine()) != null)
            {
                var currentLine = line.Split('\t');

                if (currentLine.Length > 3 && currentLine[1] == "VNUM")
                {
                    itemAreaBegin = true;
                    item.VNum = short.Parse(s: currentLine[2]);
                    item.Price = long.Parse(s: currentLine[3]);
                    item.SellToNpcPrice = item.Price / 20;
                }
                else if (currentLine.Length > 1 && currentLine[1] == "END")
                {
                    if (!itemAreaBegin) continue;
                    if (DaoFactory.ItemDao.LoadById(vNum: item.VNum) == null) items.Add(item: item);
                    item = new ItemDto();
                    itemAreaBegin = false;
                }
                else if (currentLine.Length > 2 && currentLine[1] == "NAME")
                {
                    item.Name = dictionaryName.TryGetValue(key: currentLine[2], value: out var name) ? name : "";
                }
                else if (currentLine.Length > 7 && currentLine[1] == "INDEX")
                {
                    item.Type = byte.Parse(s: currentLine[2]) switch
                    {
                        4 => InventoryType.Equipment,
                        8 => InventoryType.Equipment,
                        9 => InventoryType.Main,
                        10 => InventoryType.Etc,
                        var _ => (InventoryType)Enum.Parse(enumType: typeof(InventoryType), value: currentLine[2])
                    };

                    item.ItemType = currentLine[3] != "-1"
                        ? (ItemType)Enum.Parse(enumType: typeof(ItemType),
                            value: $"{(byte)item.Type}{currentLine[3]}")
                        : ItemType.Weapon;
                    item.ItemSubType = byte.Parse(s: currentLine[4]);
                    item.EquipmentSlot = (EquipmentType)Enum.Parse(enumType: typeof(EquipmentType),
                        value: currentLine[5] != "-1" && item.Type == InventoryType.Equipment ? currentLine[5] : "0");

                    if (item.ItemType == ItemType.Special)
                    {
                        // add a value for design here design id might also come in handy
                    }

                    // item.DesignId = short.Parse(currentLine[6]);
                    switch (item.VNum)
                    {
                        case 1906:
                            item.Morph = 2368;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 1907:
                            item.Morph = 2370;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 1965:
                            item.Morph = 2406;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 5008:
                            item.Morph = 2411;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 5117:
                            item.Morph = 2429;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5152:
                            item.Morph = 2432;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5173:
                            item.Morph = 2511;
                            item.Speed = 16;
                            item.WaitDelay = 3000;
                            break;

                        case 5196:
                        case 9076:
                            item.Morph = 2517;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5226: // Invisible locomotion, only 5 seconds with booster
                            item.Morph = 1817;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 5228: // Invisible locoomotion, only 5 seconds with booster
                            item.Morph = 1819;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 5232:
                            item.Morph = 2520;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5234:
                            item.Morph = 2522;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 5236:
                            item.Morph = 2524;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 5238:
                            item.Morph = 1817;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 5240:
                            item.Morph = 1819;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 5319:
                            item.Morph = 2526;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        case 5321:
                            item.Morph = 2528;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5323:
                            item.Morph = 2530;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        case 5330:
                            item.Morph = 2928;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        case 5332:
                            item.Morph = 2930;
                            item.Speed = 14;
                            item.WaitDelay = 3000;
                            break;

                        case 5360:
                            item.Morph = 2932;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        case 5386:
                            item.Morph = 2934;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5387:
                            item.Morph = 2936;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5388:
                            item.Morph = 2938;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5389:
                            item.Morph = 2940;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5390:
                            item.Morph = 2942;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5391:
                            item.Morph = 2944;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5914:
                            item.Morph = 2513;
                            item.Speed = 14;
                            item.WaitDelay = 3000;
                            break;

                        case 5997:
                            item.Morph = 3679;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9054:
                            item.Morph = 2368;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 9055:
                            item.Morph = 2370;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 9058:
                            item.Morph = 2406;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 9065:
                            item.Morph = 2411;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 9070:
                            item.Morph = 2429;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9073:
                            item.Morph = 2432;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9078:
                            item.Morph = 2520;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9079:
                            item.Morph = 2522;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9080:
                            item.Morph = 2524;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9081:
                            item.Morph = 1817;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9082:
                            item.Morph = 1819;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9083:
                            item.Morph = 2526;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        case 9084:
                            item.Morph = 2528;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        case 9085:
                            item.Morph = 2930;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        case 9086:
                            item.Morph = 2928;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        case 9087:
                            item.Morph = 2930;
                            item.Speed = 14;
                            item.WaitDelay = 3000;
                            break;

                        case 9088:
                            item.Morph = 2932;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        case 9090:
                            item.Morph = 2934;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9091:
                            item.Morph = 2936;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9092:
                            item.Morph = 2938;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9093:
                            item.Morph = 2940;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9094:
                            item.Morph = 2942;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 9115:
                            item.Morph = 3679;
                            item.Speed = 21;
                            item.WaitDelay = 3000;
                            break;

                        case 5834:
                            item.Morph = 3693;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 9121:
                            item.Morph = 3693;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 5712:
                        case 9138:
                            item.Morph = 2440;
                            item.Speed = 20;
                            item.WaitDelay = 3000;
                            break;

                        case 5714:
                        case 9140:
                            item.Morph = 2442;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        case 9141:
                            item.Morph = 2444;
                            item.Speed = 22;
                            item.WaitDelay = 3000;
                            break;

                        default:
                            if (item.EquipmentSlot.Equals(obj: EquipmentType.Amulet))
                                switch (item.VNum)
                                {
                                    case 4503:
                                        item.EffectValue = 4544;
                                        break;

                                    case 4504:
                                        item.EffectValue = 4294;
                                        break;

                                    case 282: // Red amulet
                                        item.Effect = 791;
                                        item.EffectValue = 3;
                                        break;

                                    case 283: // Blue amulet
                                        item.Effect = 792;
                                        item.EffectValue = 3;
                                        break;

                                    case 284: // Reinforcement amulet
                                        item.Effect = 793;
                                        item.EffectValue = 3;
                                        break;

                                    case 4264: // Heroic
                                        item.Effect = 794;
                                        item.EffectValue = 3;
                                        break;

                                    case 4262: // Random heroic
                                        item.Effect = 795;
                                        item.EffectValue = 3;
                                        break;

                                    default:
                                        item.EffectValue = Convert.ToInt16(value: currentLine[7]);
                                        break;
                                }
                            else
                                item.Morph = short.Parse(s: currentLine[7]);

                            break;
                    }
                }
                else if (currentLine.Length > 3 && currentLine[1] == "TYPE")
                {
                    // currentLine[2] 0-range 2-range 3-magic
                    item.Class = item.EquipmentSlot == EquipmentType.Fairy ? (byte)15 : byte.Parse(s: currentLine[3]);
                }
                else if (currentLine.Length > 3 && currentLine[1] == "FLAG")
                {
                    item.IsSoldable = currentLine[5] == "0";
                    item.IsDroppable = currentLine[6] == "0";
                    item.IsTradable = currentLine[7] == "0";
                    item.IsBlocked = currentLine[8] == "1";
                    item.IsMinilandObject = currentLine[9] == "1";
                    item.IsHolder = currentLine[10] == "1";
                    item.IsColored = currentLine[16] == "1";
                    item.Sex = currentLine[18] == "1" ? (byte)1 : currentLine[17] == "1" ? (byte)2 : (byte)0;
                    if (currentLine[21] == "1") item.ReputPrice = item.Price;
                    item.IsHeroic = currentLine[22] == "1";
                    /*
                        item.IsVehicle = currentLine[11] == "1" ? true : false; // (?)
                        item.BoxedVehicle = currentLine[12] == "1" ? true : false; // (?)
                        linesave[4]  unknown
                        linesave[11] unknown
                        linesave[12] unknown
                        linesave[13] unknown
                        linesave[14] unknown
                        linesave[15] unknown
                        linesave[19] unknown
                        linesave[20] unknown
                        */
                }
                else if (currentLine.Length > 1 && currentLine[1] == "DATA")
                {
                    switch (item.ItemType)
                    {
                        case ItemType.Weapon:
                            item.LevelMinimum = byte.Parse(s: currentLine[2]);
                            item.DamageMinimum = short.Parse(s: currentLine[3]);
                            item.DamageMaximum = short.Parse(s: currentLine[4]);
                            item.HitRate = short.Parse(s: currentLine[5]);
                            item.CriticalLuckRate = byte.Parse(s: currentLine[6]);
                            item.CriticalRate = short.Parse(s: currentLine[7]);
                            item.BasicUpgrade = byte.Parse(s: currentLine[10]);
                            item.MaximumAmmo = 100;
                            break;

                        case ItemType.Armor:
                            item.LevelMinimum = byte.Parse(s: currentLine[2]);
                            item.CloseDefence = short.Parse(s: currentLine[3]);
                            item.DistanceDefence = short.Parse(s: currentLine[4]);
                            item.MagicDefence = short.Parse(s: currentLine[5]);
                            item.DefenceDodge = short.Parse(s: currentLine[6]);
                            item.DistanceDefenceDodge = short.Parse(s: currentLine[6]);
                            item.BasicUpgrade = byte.Parse(s: currentLine[10]);
                            break;

                        case ItemType.Box:
                            switch (item.VNum)
                            {
                                // add here your custom effect/effectvalue for box item, make
                                // sure its unique for boxitems

                                case 287:
                                    item.Effect = 69;
                                    item.EffectValue = 1;
                                    break;

                                case 4240:
                                    item.Effect = 69;
                                    item.EffectValue = 2;
                                    break;

                                case 4194:
                                    item.Effect = 69;
                                    item.EffectValue = 3;
                                    break;

                                case 4106:
                                    item.Effect = 69;
                                    item.EffectValue = 4;
                                    break;

                                default:
                                    item.Effect = short.Parse(s: currentLine[2]);
                                    item.EffectValue = int.Parse(s: currentLine[3]);
                                    item.LevelMinimum = byte.Parse(s: currentLine[4]);
                                    break;
                            }

                            break;

                        case ItemType.Fashion:
                            item.LevelMinimum = byte.Parse(s: currentLine[2]);
                            item.CloseDefence = short.Parse(s: currentLine[3]);
                            item.DistanceDefence = short.Parse(s: currentLine[4]);
                            item.MagicDefence = short.Parse(s: currentLine[5]);
                            item.DefenceDodge = short.Parse(s: currentLine[6]);
                            if (item.EquipmentSlot.Equals(obj: EquipmentType.CostumeHat) ||
                                item.EquipmentSlot.Equals(obj: EquipmentType.CostumeSuit))
                                item.ItemValidTime = int.Parse(s: currentLine[13]) * 3600;
                            break;

                        case ItemType.Food:
                            item.Hp = short.Parse(s: currentLine[2]);
                            item.Mp = short.Parse(s: currentLine[4]);
                            break;

                        case ItemType.Jewelery:
                            if (item.EquipmentSlot.Equals(obj: EquipmentType.Amulet))
                            {
                                item.LevelMinimum = byte.Parse(s: currentLine[2]);
                                if (item.VNum > 4055 && item.VNum < 4061 || item.VNum > 4172 && item.VNum < 4176 ||
                                    item.VNum > 4045 && item.VNum < 4056
                                    || item.VNum > 8104 && item.VNum < 8115 || item.VNum == 967 || item.VNum == 968)
                                    item.ItemValidTime = 10800;
                                else
                                    item.ItemValidTime = int.Parse(s: currentLine[3]) / 10;
                            }
                            else if (item.EquipmentSlot.Equals(obj: EquipmentType.Fairy))
                            {
                                item.Element = byte.Parse(s: currentLine[2]);
                                item.ElementRate = short.Parse(s: currentLine[3]);
                                if (item.VNum <= 256)
                                    item.MaxElementRate = 50;
                                else
                                    switch (item.ElementRate)
                                    {
                                        case 0:
                                            if (item.VNum >= 800 && item.VNum <= 804)
                                                item.MaxElementRate = 50;
                                            else
                                                item.MaxElementRate = 70;
                                            break;

                                        case 30:
                                            if (item.VNum >= 884 && item.VNum <= 887)
                                                item.MaxElementRate = 50;
                                            else
                                                item.MaxElementRate = 30;
                                            break;

                                        case 35:
                                            item.MaxElementRate = 35;
                                            break;

                                        case 40:
                                            item.MaxElementRate = 70;
                                            break;

                                        case 50:
                                            item.MaxElementRate = 80;
                                            break;
                                    }
                            }
                            else
                            {
                                item.LevelMinimum = byte.Parse(s: currentLine[2]);
                                item.MaxCellonLvl = byte.Parse(s: currentLine[3]);
                                item.MaxCellon = byte.Parse(s: currentLine[4]);
                            }

                            break;

                        case ItemType.Event:
                            item.EffectValue = item.VNum switch
                            {
                                1332 => 5108,
                                1333 => 5109,
                                1334 => 5111,
                                1335 => 5107,
                                1336 => 5106,
                                1337 => 5110,
                                1339 => 5114,
                                9031 => 5108,
                                9032 => 5109,
                                9033 => 5011,
                                9034 => 5107,
                                9035 => 5106,
                                9036 => 5110,
                                9038 => 5114,
                                // EffectItems aka. fireworks
                                1581 => 860,
                                1582 => 861,
                                1585 => 859,
                                1983 => 875,
                                1984 => 876,
                                1985 => 877,
                                1986 => 878,
                                1987 => 879,
                                1988 => 880,
                                9044 => 859,
                                9059 => 875,
                                9060 => 876,
                                9061 => 877,
                                9062 => 878,
                                9063 => 879,
                                9064 => 880,
                                var _ => short.Parse(s: currentLine[7])
                            };

                            break;

                        case ItemType.Special:
                            switch (item.VNum)
                            {
                                case 1246:
                                case 9020:
                                    item.Effect = 6600;
                                    item.EffectValue = 1;
                                    break;

                                case 1247:
                                case 9021:
                                    item.Effect = 6600;
                                    item.EffectValue = 2;
                                    break;

                                case 1248:
                                case 9022:
                                    item.Effect = 6600;
                                    item.EffectValue = 3;
                                    break;

                                case 1249:
                                case 9023:
                                    item.Effect = 6600;
                                    item.EffectValue = 4;
                                    break;

                                case 5130:
                                case 9072:
                                    item.Effect = 1006;
                                    break;

                                case 1272:
                                case 1858:
                                case 9047:
                                    item.Effect = 1009;
                                    item.EffectValue = 10;
                                    break;

                                case 1273:
                                case 9024:
                                    item.Effect = 1009;
                                    item.EffectValue = 30;
                                    break;

                                case 1274:
                                case 9025:
                                    item.Effect = 1009;
                                    item.EffectValue = 60;
                                    break;

                                case 1279:
                                case 9029:
                                    item.Effect = 1007;
                                    item.EffectValue = 30;
                                    break;

                                case 1280:
                                case 9030:
                                    item.Effect = 1007;
                                    item.EffectValue = 60;
                                    break;

                                case 1923:
                                case 9056:
                                    item.Effect = 1007;
                                    item.EffectValue = 10;
                                    break;

                                case 1275:
                                case 1886:
                                case 9026:
                                    item.Effect = 1008;
                                    item.EffectValue = 10;
                                    break;

                                case 1276:
                                case 9027:
                                    item.Effect = 1008;
                                    item.EffectValue = 30;
                                    break;

                                case 1277:
                                case 9028:
                                    item.Effect = 1008;
                                    item.EffectValue = 60;
                                    break;

                                case 5060:
                                case 9066:
                                    item.Effect = 1003;
                                    item.EffectValue = 30;
                                    break;

                                case 5061:
                                case 9067:
                                    item.Effect = 1004;
                                    item.EffectValue = 7;
                                    break;

                                case 5062:
                                case 9068:
                                    item.Effect = 1004;
                                    item.EffectValue = 1;
                                    break;

                                case 5105:
                                    item.Effect = 651;
                                    break;

                                case 5115:
                                    item.Effect = 652;
                                    break;

                                case 1981:
                                    item.Effect = 34; // imagined number as for I = √(-1), complex z = a + bi
                                    break;

                                case 1982:
                                    item.Effect = 6969; // imagined number as for I = √(-1), complex z = a + bi
                                    break;

                                case 1904:
                                    item.Effect = 1894;
                                    break;

                                case 1429:
                                    item.Effect = 666;
                                    break;

                                case 1430:
                                    item.Effect = 666;
                                    item.EffectValue = 1;
                                    break;

                                case 5107:
                                    item.EffectValue = 47;
                                    break;

                                case 5207:
                                    item.EffectValue = 50;
                                    break;

                                case 5519:
                                    item.EffectValue = 60;
                                    break;

                                default:
                                    if (item.VNum > 5891 && item.VNum < 5900 ||
                                        item.VNum > 9100 && item.VNum < 9109)
                                        item.Effect = 69; // imagined number as for I = √(-1), complex z = a + bi
                                    else if (item.VNum > 1893 && item.VNum < 1904)
                                        item.Effect = 2152;
                                    else
                                        item.Effect = short.Parse(s: currentLine[2]);
                                    break;
                            }

                            switch (item.Effect)
                            {
                                case 150:
                                case 151:
                                    item.EffectValue = int.Parse(s: currentLine[4]) switch
                                    {
                                        1 => 30000,
                                        2 => 70000,
                                        3 => 180000,
                                        var _ => int.Parse(s: currentLine[4])
                                    };

                                    break;

                                case 204:
                                    item.EffectValue = 10000;
                                    break;

                                case 305:
                                    item.EffectValue = int.Parse(s: currentLine[5]);
                                    item.Morph = short.Parse(s: currentLine[4]);
                                    break;

                                default:
                                    item.EffectValue = item.EffectValue == 0
                                        ? int.Parse(s: currentLine[4])
                                        : item.EffectValue;
                                    break;
                            }

                            item.WaitDelay = 5000;
                            break;

                        case ItemType.Magical:
                            if (item.VNum > 2059 && item.VNum < 2070)
                                item.Effect = 10;
                            else
                                item.Effect = short.Parse(s: currentLine[2]);
                            item.EffectValue = int.Parse(s: currentLine[4]);
                            if (byte.TryParse(s: currentLine[5], result: out var sex) && sex > 0)
                                item.Sex = (byte)(sex - 1);
                            break;

                        case ItemType.Specialist:

                            // item.isSpecialist = byte.Parse(currentLine[2]); item.Unknown = short.Parse(currentLine[3]);
                            item.ElementRate = short.Parse(s: currentLine[4]);
                            item.Speed = byte.Parse(s: currentLine[5]);
                            item.SpType = byte.Parse(s: currentLine[13]);

                            // item.Morph = short.Parse(currentLine[14]) + 1;
                            item.FireResistance = byte.Parse(s: currentLine[15]);
                            item.WaterResistance = byte.Parse(s: currentLine[16]);
                            item.LightResistance = byte.Parse(s: currentLine[17]);
                            item.DarkResistance = byte.Parse(s: currentLine[18]);

                            // item.PartnerClass = short.Parse(currentLine[19]);
                            item.LevelJobMinimum = byte.Parse(s: currentLine[20]);
                            item.ReputationMinimum = byte.Parse(s: currentLine[21]);

                            var elementdic = new Dictionary<int, int> { [key: 0] = 0 };
                            if (item.FireResistance != 0) elementdic.Add(key: 1, value: item.FireResistance);
                            if (item.WaterResistance != 0) elementdic.Add(key: 2, value: item.WaterResistance);
                            if (item.LightResistance != 0) elementdic.Add(key: 3, value: item.LightResistance);
                            if (item.DarkResistance != 0) elementdic.Add(key: 4, value: item.DarkResistance);

                            item.Element = (byte)elementdic.OrderByDescending(keySelector: s => s.Value).First().Key;
                            if (elementdic.Count > 1 &&
                                elementdic.OrderByDescending(keySelector: s => s.Value).First().Value ==
                                elementdic.OrderByDescending(keySelector: s => s.Value).ElementAt(index: 1).Value)
                                item.SecondaryElement =
                                    (byte)elementdic.OrderByDescending(keySelector: s => s.Value).ElementAt(index: 1)
                                        .Key;

                            // needs to be hardcoded
                            switch (item.VNum)
                            {
                                case 901:
                                    item.Element = 1;
                                    break;
                                case 903:
                                    item.Element = 2;
                                    break;
                                case 906:
                                case 909:
                                    item.Element = 3;
                                    break;
                                case var _:
                                    item.Element = item.Element;
                                    break;
                            }

                            break;

                        case ItemType.Shell:

                            // item.ShellMinimumLevel = short.Parse(linesave[3]);
                            // item.ShellMaximumLevel = short.Parse(linesave[4]); item.ShellType
                            // = byte.Parse(linesave[5]); // 3 shells of each type
                            break;

                        case ItemType.Main:
                            item.Effect = short.Parse(s: currentLine[2]);
                            item.EffectValue = int.Parse(s: currentLine[4]);
                            break;

                        case ItemType.Upgrade:
                            item.Effect = short.Parse(s: currentLine[2]);
                            item.EffectValue = item.VNum switch
                            {
                                // UpgradeItems (needed to be hardcoded)
                                1218 => 26,
                                1363 => 27,
                                1364 => 28,
                                5107 => 47,
                                5207 => 50,
                                5369 => 61,
                                5519 => 60,
                                var _ => int.Parse(s: currentLine[4])
                            };

                            break;

                        case ItemType.Production:
                            item.Effect = short.Parse(s: currentLine[2]);
                            item.EffectValue = int.Parse(s: currentLine[4]);
                            break;

                        case ItemType.Map:
                            item.Effect = short.Parse(s: currentLine[2]);
                            item.EffectValue = int.Parse(s: currentLine[4]);
                            break;

                        case ItemType.Potion:
                            item.Hp = short.Parse(s: currentLine[2]);
                            item.Mp = short.Parse(s: currentLine[4]);
                            break;

                        case ItemType.Snack:
                            item.Hp = short.Parse(s: currentLine[2]);
                            item.Mp = short.Parse(s: currentLine[4]);
                            break;

                        case ItemType.Teacher:
                            item.Effect = short.Parse(s: currentLine[2]);
                            item.EffectValue = int.Parse(s: currentLine[4]);

                            // item.PetLoyality = short.Parse(linesave[4]); item.PetFood = short.Parse(linesave[7]);
                            break;

                        case ItemType.Part:

                            // nothing to parse
                            break;

                        case ItemType.Sell:
                            item.SellToNpcPrice = item.Price;
                            // nothing to parse
                            break;

                        case ItemType.Quest2:

                            // nothing to parse
                            break;

                        case ItemType.Quest1:

                            // nothing to parse
                            break;

                        case ItemType.Ammo:

                            // nothing to parse
                            break;
                    }

                    if (item.Type == InventoryType.Miniland)
                    {
                        item.MinilandObjectPoint = int.Parse(s: currentLine[2]);
                        item.EffectValue = short.Parse(s: currentLine[8]);
                        item.Width = byte.Parse(s: currentLine[9]);
                        item.Height = byte.Parse(s: currentLine[10]);
                    }

                    if ((item.EquipmentSlot == EquipmentType.Boots || item.EquipmentSlot == EquipmentType.Gloves) &&
                        item.Type == 0)
                    {
                        item.FireResistance = byte.Parse(s: currentLine[7]);
                        item.WaterResistance = byte.Parse(s: currentLine[8]);
                        item.LightResistance = byte.Parse(s: currentLine[9]);
                        item.DarkResistance = byte.Parse(s: currentLine[11]);
                    }
                }
                else if (currentLine.Length > 1 && currentLine[1] == "BUFF")
                {
                    DaoFactory.BCardDao.DeleteByItemVNum(itemVNum: item.VNum);
                    for (var i = 0; i < 5; i++)
                    {
                        var type = (byte)int.Parse(s: currentLine[2 + 5 * i]);
                        if (type != 0 && type != 255)
                        {
                            var first = int.Parse(s: currentLine[3 + 5 * i]);
                            var itemCard = new BCardDto
                            {
                                ItemVNum = item.VNum,
                                Type = type,
                                SubType = (byte)(int.Parse(s: currentLine[5 + 5 * i]) + 1),
                                IsLevelScaled = Convert.ToBoolean(value: first % 4),
                                IsLevelDivided = Math.Abs(value: first % 4) == 2,
                                FirstData = (short)(first / 4),
                                SecondData = (short)(int.Parse(s: currentLine[4 + 5 * i]) / 4),
                                ThirdData = (short)(int.Parse(s: currentLine[6 + 5 * i]) / 4)
                            };
                            itemCards.Add(item: itemCard);
                        }
                    }
                }
            }

            items = items.Where(predicate: s => DaoFactory.ItemDao.LoadById(vNum: s.VNum) == null).ToList();
            DaoFactory.ItemDao.Insert(items: items);
            DaoFactory.BCardDao.Insert(cards: itemCards);
            Logger.Info(message: string.Format(format: Language.Instance.GetMessageFromKey(key: "ITEMS_PARSED"),
                arg0: items.Count));
        }

        void InsertRecipe(short itemVNum, short triggerVNum, short amount = 1, short[] recipeItems = null)
        {
            void RecipeAdd(RecipeDto recipeDto)
            {
                var recipeList = DaoFactory.RecipeListDao.LoadByRecipeId(recipeId: recipeDto.RecipeId)
                    .FirstOrDefault(predicate: r => r.ItemVNum == null);
                if (recipeList != null)
                {
                    recipeList.ItemVNum = triggerVNum;
                    DaoFactory.RecipeListDao.Update(recipe: recipeList);
                }
                else
                {
                    recipeList = new RecipeListDto
                    {
                        ItemVNum = triggerVNum,
                        RecipeId = recipeDto.RecipeId
                    };
                    DaoFactory.RecipeListDao.Insert(recipeList: recipeList);
                }
            }

            var recipe = DaoFactory.RecipeDao.LoadByItemVNum(itemVNum: itemVNum);
            if (recipe != null)
            {
                RecipeAdd(recipeDto: recipe);
            }
            else
            {
                recipe = new RecipeDto
                {
                    ItemVNum = itemVNum,
                    Amount = amount
                };
                DaoFactory.RecipeDao.Insert(recipe: recipe);
                recipe = DaoFactory.RecipeDao.LoadByItemVNum(itemVNum: itemVNum);
                if (recipe != null && recipeItems != null)
                {
                    for (var i = 0; i < recipeItems.Length; i += 2)
                    {
                        var recipeItem = new RecipeItemDto
                        {
                            ItemVNum = recipeItems[i],
                            Amount = recipeItems[i + 1],
                            RecipeId = recipe.RecipeId
                        };
                        if (DaoFactory.RecipeItemDao.LoadByRecipeAndItem(recipeId: recipe.RecipeId,
                            itemVNum: recipeItem.ItemVNum) == null)
                            DaoFactory.RecipeItemDao.Insert(recipeItem: recipeItem);
                    }

                    RecipeAdd(recipeDto: recipe);
                }
            }
        }

        #endregion
    }
}