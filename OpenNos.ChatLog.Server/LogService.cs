﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Hik.Communication.ScsServices.Service;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.Log.Networking;
using OpenNos.Log.Shared;
using OpenNos.Master.Library.Client;

namespace OpenNos.Log.Server
{
    class LogService : ScsService, ILogService
    {
        public bool AuthenticateAdmin(string user, string passHash)
        {
            if (string.IsNullOrWhiteSpace(value: user) || string.IsNullOrWhiteSpace(value: passHash)) return false;

            if (AuthentificationServiceClient.Instance.ValidateAccount(userName: user, passHash: passHash) is AccountDto
                    account &&
                account.Authority > AuthorityType.User)
            {
                LogManager.Instance.AuthentificatedClients.Add(item: CurrentClient.ClientId);
                return true;
            }

            return false;
        }

        public bool Authenticate(string authKey)
        {
            if (string.IsNullOrWhiteSpace(value: authKey)) return false;

            if (authKey == ConfigurationManager.AppSettings[name: "LogKey"])
            {
                LogManager.Instance.AuthentificatedClients.Add(item: CurrentClient.ClientId);
                return true;
            }

            return false;
        }

        public List<LogEntry> GetChatLogEntries(string[] sender, string[] senderid, string[] receiver,
            string[] receiverid, string[] message, DateTime? start, DateTime? end, LogType? logType)
        {
            Logger.Info(
                message:
                $"Received Log Request - Sender: {sender} SenderId: {senderid} Receiver: {receiver} ReceiverId: {receiverid} Message: {message} DateStart: {start} DateEnd: {end} ChatLogType: {logType}");
            var tmp = LogManager.Instance.AllChatLogs.GetAllItems();
            if (sender != null)
                tmp = tmp.Where(predicate: s =>
                        sender.Any(predicate: x =>
                            s.Sender.IndexOf(value: x, comparisonType: StringComparison.CurrentCultureIgnoreCase) >= 0))
                    .ToList();
            if (senderid != null)
                tmp = tmp.Where(predicate: s => senderid.Any(predicate: x => s.SenderId == long.Parse(s: x))).ToList();
            if (receiver != null)
                tmp = tmp.Where(predicate: s =>
                        receiver.Any(predicate: x =>
                            s.Receiver?.IndexOf(value: x, comparisonType: StringComparison.CurrentCultureIgnoreCase) >=
                            0))
                    .ToList();
            if (receiverid != null)
                tmp = tmp.Where(predicate: s => receiverid.Any(predicate: x => s.ReceiverId == long.Parse(s: x)))
                    .ToList();
            if (message != null)
                tmp = tmp.Where(predicate: s =>
                        message.Any(predicate: x =>
                            s.Message.IndexOf(value: x, comparisonType: StringComparison.CurrentCultureIgnoreCase) >=
                            0))
                    .ToList();
            if (start.HasValue) tmp = tmp.Where(predicate: s => s.Timestamp >= start).ToList();
            if (end.HasValue) tmp = tmp.Where(predicate: s => s.Timestamp <= end).ToList();
            if (logType.HasValue) tmp = tmp.Where(predicate: s => s.MessageType == logType).ToList();
            return tmp;
        }

        public List<PacketLogEntry> GetPacketLogEntries(string[] sender, string[] senderid, string[] receiver,
            string[] receiverid, string[] packet, DateTime? start, DateTime? end, LogType? logType,
            AuthorityType authority)
        {
            Logger.Info(
                message:
                $"Received Log Request - Sender: {sender} SenderId: {senderid} Receiver: {receiver} ReceiverId: {receiverid} Packet: {packet} DateStart: {start} DateEnd: {end} ChatLogType: {logType}");
            var tmp = LogManager.Instance.AllPacketLogs.GetAllItems();
            if (sender != null)
                tmp = tmp.Where(predicate: s =>
                        sender.Any(predicate: x =>
                            s.Sender.IndexOf(value: x, comparisonType: StringComparison.CurrentCultureIgnoreCase) >= 0))
                    .ToList();
            if (senderid != null)
                tmp = tmp.Where(predicate: s => senderid.Any(predicate: x => s.SenderId == long.Parse(s: x))).ToList();
            if (receiver != null)
                tmp = tmp.Where(predicate: s =>
                        receiver.Any(predicate: x =>
                            s.Receiver.IndexOf(value: x, comparisonType: StringComparison.CurrentCultureIgnoreCase) >=
                            0))
                    .ToList();
            if (receiverid != null)
                tmp = tmp.Where(predicate: s => receiverid.Any(predicate: x => s.ReceiverId == long.Parse(s: x)))
                    .ToList();
            if (packet != null)
                tmp = tmp.Where(predicate: s =>
                        packet.Any(predicate: x =>
                            s.Packet.IndexOf(value: x, comparisonType: StringComparison.CurrentCultureIgnoreCase) >= 0))
                    .ToList();
            if (start.HasValue) tmp = tmp.Where(predicate: s => s.Timestamp >= start).ToList();
            if (end.HasValue) tmp = tmp.Where(predicate: s => s.Timestamp <= end).ToList();
            if (logType.HasValue) tmp = tmp.Where(predicate: s => s.PacketType == logType).ToList();
            if (authority <= AuthorityType.Gm)
                tmp = tmp.Where(predicate: s => s.PacketType <= LogType.ItemCreate).ToList();
            if (authority <= AuthorityType.Gm)
                tmp = tmp.Where(predicate: s => s.PacketType <= LogType.GMCommand).ToList();
            if (authority <= AuthorityType.Gm) tmp = tmp.Where(predicate: s => s.PacketType <= LogType.Packet).ToList();
            return tmp;
        }

        public void LogChatMessage(LogEntry logEntry)
        {
            if (!LogManager.Instance.AuthentificatedClients.Any(predicate: s =>
                    s.Equals(obj: CurrentClient.ClientId)) ||
                logEntry == null) return;

            logEntry.Timestamp = DateTime.Now;
            LogManager.Instance.ChatLogs.Add(value: logEntry);
            LogManager.Instance.AllChatLogs.Add(value: logEntry);
        }

        public void LogPacket(PacketLogEntry logEntry)
        {
            if (!LogManager.Instance.AuthentificatedClients.Any(predicate: s =>
                    s.Equals(obj: CurrentClient.ClientId)) ||
                logEntry == null) return;

            logEntry.Timestamp = DateTime.Now;
            LogManager.Instance.PacketLogs.Add(value: logEntry);
            LogManager.Instance.AllPacketLogs.Add(value: logEntry);
        }

        public AccountDto GetAccount(string user, string passHash)
        {
            if (string.IsNullOrWhiteSpace(value: user) || string.IsNullOrWhiteSpace(value: passHash)) return null;

            if (AuthentificationServiceClient.Instance.ValidateAccount(userName: user, passHash: passHash) is AccountDto
                    account &&
                account.Authority > AuthorityType.User) return account;

            return null;
        }
    }
}