﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using Hik.Communication.Scs.Communication.EndPoints.Tcp;
using Hik.Communication.ScsServices.Service;
using OpenNos.Core;
using OpenNos.Log.Networking;

namespace OpenNos.Log.Server
{
    static class Program
    {
        #region Enums

        public enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        #endregion

        #region Members

        static bool _isDebug;

        #endregion

        #region Methods

        public static void Main(string[] args)
        {
            try
            {
#if DEBUG
                _isDebug = true;
#endif
                CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo(name: "en-US");
                Console.Title = $"OpenNos Log Server{(_isDebug ? " Development Environment" : "")}";

                var ignoreStartupMessages = false;
                foreach (var arg in args)
                    switch (arg)
                    {
                        case "--nomsg":
                            ignoreStartupMessages = true;
                            break;
                    }

                //initialize Logger
                Logger.InitializeLogger(log: log4net.LogManager.GetLogger(type: typeof(Program)));

                var port = Convert.ToInt32(value: ConfigurationManager.AppSettings[name: "LogPort"]);
                if (!ignoreStartupMessages)
                {
                    var assembly = Assembly.GetExecutingAssembly();
                    var fileVersionInfo = FileVersionInfo.GetVersionInfo(fileName: assembly.Location);
                    var text = $"CHAT LOG SERVER v{fileVersionInfo.ProductVersion}dev - PORT : {port} by NN Team";
                    var offset = Console.WindowWidth / 2 + text.Length / 2;
                    var separator = new string(c: '=', count: Console.WindowWidth);
                    Console.WriteLine(value: separator + string.Format(format: "{0," + offset + "}\n", arg0: text) +
                                             separator);
                }

                Logger.Info(message: Language.Instance.GetMessageFromKey(key: "CONFIG_LOADED"));

                try
                {
                    //configure Services and Service Host
                    var ipAddress = ConfigurationManager.AppSettings[name: "LogIP"];
                    var _server =
                        ScsServiceBuilder.CreateService(endPoint: new ScsTcpEndPoint(ipAddress: ipAddress, port: port));

                    _server.AddService<ILogService, LogService>(service: new LogService());
                    _server.ClientConnected += OnClientConnected;
                    _server.ClientDisconnected += OnClientDisconnected;

                    _server.Start();
                    LogManager.Initialize();
                    Logger.Info(message: Language.Instance.GetMessageFromKey(key: "STARTED"));
                }
                catch (Exception ex)
                {
                    Logger.Error(data: "General Error Server", ex: ex);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(data: "General Error", ex: ex);
                Console.ReadKey();
            }
        }

        static void OnClientConnected(object sender, ServiceClientEventArgs e)
        {
            Logger.Info(message: Language.Instance.GetMessageFromKey(key: "NEW_CONNECT") + e.Client.ClientId);
        }

        static void OnClientDisconnected(object sender, ServiceClientEventArgs e)
        {
            Logger.Info(message: Language.Instance.GetMessageFromKey(key: "DISCONNECT") + e.Client.ClientId);
        }

        #endregion
    }
}