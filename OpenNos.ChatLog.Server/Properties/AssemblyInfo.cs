﻿using System.Reflection;
using System.Runtime.InteropServices;
using log4net.Config;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle(title: "OpenNos.Log.Server")]
[assembly: AssemblyDescription(description: "")]
[assembly: AssemblyConfiguration(configuration: "")]
[assembly: AssemblyCompany(company: "")]
[assembly: AssemblyProduct(product: "OpenNos.Log.Server")]
[assembly: AssemblyCopyright(copyright: "Copyright ©  2018")]
[assembly: AssemblyTrademark(trademark: "")]
[assembly: AssemblyCulture(culture: "")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(visibility: false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid(guid: "6fbc7b9f-1f77-48bb-8064-f56ccdfb41e3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion(version: "1.0.*")]
[assembly: XmlConfigurator(Watch = true)]