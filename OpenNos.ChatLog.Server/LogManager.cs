﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using OpenNos.Core;
using OpenNos.Core.Threading;
using OpenNos.Log.Shared;
using OpenNos.Master.Library.Client;

namespace OpenNos.Log.Server
{
    class LogManager
    {
        #region Members

        readonly LogFileReader _reader;

        #endregion

        #region Instantiation

        public LogManager()
        {
            _reader = new LogFileReader();
            AuthentificatedClients = new List<long>();
            ChatLogs = new ThreadSafeGenericList<LogEntry>();
            AllChatLogs = new ThreadSafeGenericList<LogEntry>();
            PacketLogs = new ThreadSafeGenericList<PacketLogEntry>();
            AllPacketLogs = new ThreadSafeGenericList<PacketLogEntry>();
            RecursiveChatFileOpen(dir: "chatlogs");
            RecursivePacketFileOpen(dir: "packetlogs");
            AuthentificationServiceClient.Instance.Authenticate(
                authKey: ConfigurationManager.AppSettings[name: "AuthentificationServiceAuthKey"]);
            Observable.Interval(period: TimeSpan.FromMinutes(value: 1)).Subscribe(onNext: observer => SaveChatLogs());
            Observable.Interval(period: TimeSpan.FromMinutes(value: 1)).Subscribe(onNext: observer => SavePacketLogs());
        }

        #endregion

        #region Properties

        public static LogManager Instance { get; set; }

        public List<long> AuthentificatedClients { get; set; }

        public ThreadSafeGenericList<LogEntry> ChatLogs { get; set; }

        public ThreadSafeGenericList<LogEntry> AllChatLogs { get; set; }

        public ThreadSafeGenericList<PacketLogEntry> PacketLogs { get; set; }

        public ThreadSafeGenericList<PacketLogEntry> AllPacketLogs { get; set; }

        #endregion

        #region Methods

        public static void Initialize()
        {
            Instance = new LogManager();
        }

        public void SaveChatLogs()
        {
            try
            {
                var writer = new LogFileWriter();
                Logger.Info(message: Language.Instance.GetMessageFromKey(key: "SAVE_CHATLOGS"));
                var tmp = ChatLogs.GetAllItems();
                ChatLogs.Clear();
                var current = DateTime.Now;

                var path = "chatlogs";
                if (!Directory.Exists(path: path)) Directory.CreateDirectory(path: path);
                path = Path.Combine(path1: path, path2: current.Year.ToString());
                if (!Directory.Exists(path: path)) Directory.CreateDirectory(path: path);
                path = Path.Combine(path1: path, path2: current.Month.ToString());
                if (!Directory.Exists(path: path)) Directory.CreateDirectory(path: path);
                path = Path.Combine(path1: path, path2: current.Day.ToString());
                if (!Directory.Exists(path: path)) Directory.CreateDirectory(path: path);

                writer.WriteChatLogFile(
                    path: Path.Combine(path1: path,
                        path2:
                        $"{(current.Hour < 10 ? $"0{current.Hour}" : $"{current.Hour}")}.{(current.Minute < 10 ? $"0{current.Minute}" : $"{current.Minute}")}.onc"),
                    logs: tmp);
            }
            catch (Exception ex)
            {
                Logger.Error(ex: ex);
            }
        }

        public void SavePacketLogs()
        {
            try
            {
                var writer = new LogFileWriter();
                Logger.Info(message: Language.Instance.GetMessageFromKey(key: "SAVE_PACKETLOGS"));
                var tmp = PacketLogs.GetAllItems();
                PacketLogs.Clear();
                var current = DateTime.Now;

                var path = "packetlogs";
                if (!Directory.Exists(path: path)) Directory.CreateDirectory(path: path);
                path = Path.Combine(path1: path, path2: current.Year.ToString());
                if (!Directory.Exists(path: path)) Directory.CreateDirectory(path: path);
                path = Path.Combine(path1: path, path2: current.Month.ToString());
                if (!Directory.Exists(path: path)) Directory.CreateDirectory(path: path);
                path = Path.Combine(path1: path, path2: current.Day.ToString());
                if (!Directory.Exists(path: path)) Directory.CreateDirectory(path: path);

                writer.WritePacketLogFile(
                    path: Path.Combine(path1: path,
                        path2:
                        $"{(current.Hour < 10 ? $"0{current.Hour}" : $"{current.Hour}")}.{(current.Minute < 10 ? $"0{current.Minute}" : $"{current.Minute}")}.onc"),
                    logs: tmp);
            }
            catch (Exception ex)
            {
                Logger.Error(ex: ex);
            }
        }

        void RecursiveChatFileOpen(string dir)
        {
            try
            {
                if (!Directory.Exists(path: dir)) Directory.CreateDirectory(path: dir);
                foreach (var d in Directory.GetDirectories(path: dir))
                {
                    Parallel.ForEach(
                        source: Directory.GetFiles(path: d).Where(predicate: s => s.EndsWith(value: ".onc")), body: s =>
                        {
                            try
                            {
                                AllChatLogs.AddRange(value: _reader.ReadChatLogFile(path: s));
                            }
                            catch (Exception e)
                            {
                                Logger.LogEventError(logEvent: "LogFileRead",
                                    data: $"Something went wrong while opening Chat Log File {s}\n{e}");
                            }
                        });
                    RecursiveChatFileOpen(dir: d);
                }
            }
            catch (Exception e)
            {
                Logger.LogEventError(logEvent: "LogFileRead",
                    data: $"Something went wrong while opening Chat Log Files. Exiting...\n{e}");
                Environment.Exit(exitCode: -1);
            }
        }

        void RecursivePacketFileOpen(string dir)
        {
            try
            {
                if (!Directory.Exists(path: dir)) Directory.CreateDirectory(path: dir);
                foreach (var d in Directory.GetDirectories(path: dir))
                {
                    Parallel.ForEach(
                        source: Directory.GetFiles(path: d).Where(predicate: s => s.EndsWith(value: ".onc")), body: s =>
                        {
                            try
                            {
                                AllPacketLogs.AddRange(value: _reader.ReadPacketLogFile(path: s));
                            }
                            catch (Exception e)
                            {
                                Logger.LogEventError(logEvent: "LogFileRead",
                                    data: $"Something went wrong while opening Packet Log File {s}\n{e}");
                            }
                        });
                    RecursivePacketFileOpen(dir: d);
                }
            }
            catch (Exception e)
            {
                Logger.LogEventError(logEvent: "LogFileRead",
                    data: $"Something went wrong while opening Packet Log Files. Exiting...\n{e}");
                Environment.Exit(exitCode: -1);
            }
        }

        #endregion
    }
}