﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenNos.Core.ArrayExtensions;

namespace OpenNos.PathFinder
{
    public static class BestFirstSearch
    {
        #region Methods

        public static List<Node> Backtrace(Node end)
        {
            var path = new List<Node>();
            while (end.Parent != null)
            {
                end = end.Parent;
                path.Add(item: end);
            }

            path.Reverse();
            return path;
        }

        public static List<Node> FindPathJagged(GridPos start, GridPos end, GridPos[][] Grid)
        {
            var gridX = Grid.Length;
            var gridY = Grid[0].Length;
            if (gridX <= start.X || gridY <= start.Y || start.X < 0 || start.Y < 0) return new List<Node>();
            Node node;

            var grid = JaggedArrayExtensions.CreateJaggedArray<Node>(xLenght: gridX, yLength: gridY);
            if (grid[start.X][start.Y] == null) grid[start.X][start.Y] = new Node(node: Grid[start.X][start.Y]);
            var startingNode = grid[start.X][start.Y];
            var path = new MinHeap();

            // push the start node into the open list
            path.Push(element: startingNode);
            startingNode.Opened = true;

            // while the open list is not empty
            while (path.Count > 0)
            {
                // pop the position of node which has the minimum `f` value.
                node = path.Pop();
                if (grid[node.X][node.Y] == null) grid[node.X][node.Y] = new Node(node: Grid[node.X][node.Y]);
                grid[node.X][node.Y].Closed = true;

                //if reached the end position, construct the path and return it
                if (node.X == end.X && node.Y == end.Y) return Backtrace(end: node);

                // get neigbours of the current node
                var neighbors = GetNeighborsJagged(grid: grid, node: node, mapGrid: Grid);
                for (int i = 0, l = neighbors.Count; i < l; ++i)
                {
                    var neighbor = neighbors[index: i];

                    if (neighbor.Closed) continue;

                    // check if the neighbor has not been inspected yet, or can be reached with
                    // smaller cost from the current node
                    if (!neighbor.Opened)
                    {
                        // ReSharper disable once CompareOfFloatsByEqualityOperator
                        if (neighbor.F == 0)
                            neighbor.F = Heuristic.Octile(iDx: Math.Abs(value: neighbor.X - end.X),
                                iDy: Math.Abs(value: neighbor.Y - end.Y));

                        neighbor.Parent = node;

                        if (!neighbor.Opened)
                        {
                            path.Push(element: neighbor);
                            neighbor.Opened = true;
                        }
                        else
                        {
                            neighbor.Parent = node;
                        }
                    }
                }
            }

            return new List<Node>();
        }

        public static List<Node> GetNeighborsJagged(Node[][] grid, Node node, GridPos[][] mapGrid)
        {
            var gridX = grid.Length;
            var gridY = grid[0].Length;
            short x = node.X, y = node.Y;
            var neighbors = new List<Node>();
            var s0 = false;
            var s1 = false;
            var s2 = false;
            var s3 = false;
            int IndexX;
            int IndexY;

            // ↑
            IndexX = x;
            IndexY = y - 1;
            if (gridX > IndexX && gridY > IndexY && IndexX >= 0 && IndexY >= 0 && mapGrid[IndexX][IndexY].IsWalkable())
            {
                if (grid[IndexX][IndexY] == null) grid[IndexX][IndexY] = new Node(node: mapGrid[IndexX][IndexY]);
                neighbors.Add(item: grid[IndexX][IndexY]);
                s0 = true;
            }

            // →
            IndexX = x + 1;
            IndexY = y;
            if (gridX > IndexX && gridY > IndexY && IndexX >= 0 && IndexY >= 0 && mapGrid[IndexX][IndexY].IsWalkable())
            {
                if (grid[IndexX][IndexY] == null) grid[IndexX][IndexY] = new Node(node: mapGrid[IndexX][IndexY]);
                neighbors.Add(item: grid[IndexX][IndexY]);
                s1 = true;
            }

            // ↓
            IndexX = x;
            IndexY = y + 1;
            if (gridX > IndexX && gridY > IndexY && IndexX >= 0 && IndexY >= 0 && mapGrid[IndexX][IndexY].IsWalkable())
            {
                if (grid[IndexX][IndexY] == null) grid[IndexX][IndexY] = new Node(node: mapGrid[IndexX][IndexY]);
                neighbors.Add(item: grid[IndexX][IndexY]);
                s2 = true;
            }

            // ←
            IndexX = x - 1;
            IndexY = y;
            if (gridX > IndexX && gridY > IndexY && IndexX >= 0 && IndexY >= 0 && mapGrid[IndexX][IndexY].IsWalkable())
            {
                if (grid[IndexX][IndexY] == null) grid[IndexX][IndexY] = new Node(node: mapGrid[IndexX][IndexY]);
                neighbors.Add(item: grid[IndexX][IndexY]);
                s3 = true;
            }

            var d0 = s3 || s0;
            var d1 = s0 || s1;
            var d2 = s1 || s2;
            var d3 = s2 || s3;

            // ↖
            IndexX = x - 1;
            IndexY = y - 1;
            if (gridX > IndexX && gridY > IndexY && IndexX >= 0 && IndexY >= 0 && d0 &&
                mapGrid[IndexX][IndexY].IsWalkable())
            {
                if (grid[IndexX][IndexY] == null) grid[IndexX][IndexY] = new Node(node: mapGrid[IndexX][IndexY]);
                neighbors.Add(item: grid[IndexX][IndexY]);
            }

            // ↗
            IndexX = x + 1;
            IndexY = y - 1;
            if (gridX > IndexX && gridY > IndexY && IndexX >= 0 && IndexY >= 0 && d1 &&
                mapGrid[IndexX][IndexY].IsWalkable())
            {
                if (grid[IndexX][IndexY] == null) grid[IndexX][IndexY] = new Node(node: mapGrid[IndexX][IndexY]);
                neighbors.Add(item: grid[IndexX][IndexY]);
            }

            // ↘
            IndexX = x + 1;
            IndexY = y + 1;
            if (gridX > IndexX && gridY > IndexY && IndexX >= 0 && IndexY >= 0 && d2 &&
                mapGrid[IndexX][IndexY].IsWalkable())
            {
                if (grid[IndexX][IndexY] == null) grid[IndexX][IndexY] = new Node(node: mapGrid[IndexX][IndexY]);
                neighbors.Add(item: grid[IndexX][IndexY]);
            }

            // ↙
            IndexX = x - 1;
            IndexY = y + 1;
            if (gridX > IndexX && gridY > IndexY && IndexX >= 0 && IndexY >= 0 && d3 &&
                mapGrid[IndexX][IndexY].IsWalkable())
            {
                if (grid[IndexX][IndexY] == null) grid[IndexX][IndexY] = new Node(node: mapGrid[IndexX][IndexY]);
                neighbors.Add(item: grid[IndexX][IndexY]);
            }

            return neighbors;
        }

        public static Node[][]
            LoadBrushFireJagged(GridPos user, GridPos[][] Grid, short maxDistance = 22) //Conflict with Grid
        {
            var gridX = Grid.Length;
            var gridY = Grid[0].Length;
            var grid = JaggedArrayExtensions.CreateJaggedArray<Node>(xLenght: gridX, yLength: gridY);

            Node node;
            if (grid[user.X][user.Y] == null) grid[user.X][user.Y] = new Node(node: Grid[user.X][user.Y]);
            var start = grid[user.X][user.Y];
            var path = new MinHeap();

            // push the start node into the open list
            path.Push(element: start);
            start.Opened = true;

            // while the open list is not empty
            while (path.Count > 0)
            {
                // pop the position of node which has the minimum `f` value.
                node = path.Pop();
                if (grid[node.X][node.Y] == null) grid[node.X][node.Y] = new Node(node: Grid[node.X][node.Y]);

                grid[node.X][node.Y].Closed = true;

                // get neighbors of the current node
                var neighbors = GetNeighborsJagged(grid: grid, node: node, mapGrid: Grid);

                for (int i = 0, l = neighbors.Count; i < l; ++i)
                {
                    var neighbor = neighbors[index: i];

                    if (neighbor.Closed) continue;

                    // check if the neighbor has not been inspected yet, or can be reached with
                    // smaller cost from the current node
                    if (!neighbor.Opened)
                    {
                        // ReSharper disable once CompareOfFloatsByEqualityOperator
                        if (neighbor.F == 0)
                        {
                            var distance =
                                Heuristic.Octile(iDx: Math.Abs(value: neighbor.X - node.X),
                                    iDy: Math.Abs(value: neighbor.Y - node.Y)) + node.F;
                            if (distance > maxDistance)
                            {
                                neighbor.Value = 1;
                                continue;
                            }

                            neighbor.F = distance;
                            grid[neighbor.X][neighbor.Y].F = neighbor.F;
                        }

                        neighbor.Parent = node;

                        if (!neighbor.Opened)
                        {
                            path.Push(element: neighbor);
                            neighbor.Opened = true;
                        }
                        else
                        {
                            neighbor.Parent = node;
                        }
                    }
                }
            }

            return grid;
        }

        public static List<Node> TracePathJagged(Node node, Node[][] grid, GridPos[][] mapGrid)
        {
            var list = new List<Node>();
            if (mapGrid == null || grid == null || node.X >= grid.Length || node.Y >= grid[0].Length || node.X < 0 ||
                node.Y < 0 || grid[node.X][node.Y] == null)
            {
                node.F = 100;
                list.Add(item: node);
                return list;
            }

            var currentnode = grid[node.X][node.Y];
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            while (currentnode.F != 1 && currentnode.F != 0)
            {
                var newnode = GetNeighborsJagged(grid: grid, node: currentnode, mapGrid: mapGrid)
                    ?.OrderBy(keySelector: s => s.F).FirstOrDefault();
                if (newnode != null)
                {
                    list.Add(item: newnode);
                    currentnode = newnode;
                }
            }

            return list;
        }

        #endregion
    }
}