﻿using System.Collections.Generic;

namespace OpenNos.PathFinder
{
    internal class MinHeap
    {
        #region Members

        readonly List<Node> _array = new List<Node>();

        #endregion

        #region Properties

        public int Count
        {
            get => _array.Count;
        }

        #endregion

        #region Methods

        public Node Pop()
        {
            var ret = _array[index: 0];
            _array[index: 0] = _array[index: _array.Count - 1];
            _array.RemoveAt(index: _array.Count - 1);

            var len = 0;
            while (len < _array.Count)
            {
                var min = len;
                if (2 * len + 1 < _array.Count && _array[index: 2 * len + 1].CompareTo(other: _array[index: min]) == -1)
                    min = 2 * len + 1;
                if (2 * len + 2 < _array.Count && _array[index: 2 * len + 2].CompareTo(other: _array[index: min]) == -1)
                    min = 2 * len + 2;

                if (min == len) break;
                var tmp = _array[index: len];
                _array[index: len] = _array[index: min];
                _array[index: min] = tmp;
                len = min;
            }

            return ret;
        }

        public void Push(Node element)
        {
            _array.Add(item: element);
            var len = _array.Count - 1;
            var parent = len - 1 >> 1;
            while (len > 0 && _array[index: len].CompareTo(other: _array[index: parent]) < 0)
            {
                var tmp = _array[index: len];
                _array[index: len] = _array[index: parent];
                _array[index: parent] = tmp;
                len = parent;
                parent = len - 1 >> 1;
            }
        }

        #endregion
    }
}