﻿using System;

namespace OpenNos.PathFinder
{
    public static class Heuristic
    {
        #region Members

        public static readonly double Sqrt2 = Math.Sqrt(d: 2);

        #endregion

        #region Methods

        public static double Chebyshev(int iDx, int iDy)
        {
            return Math.Max(val1: iDx, val2: iDy);
        }

        public static double Euclidean(int iDx, int iDy)
        {
            float tFdx = iDx;
            float tFdy = iDy;
            return Math.Sqrt(d: tFdx * tFdx + tFdy * tFdy);
        }

        public static double Manhattan(int iDx, int iDy)
        {
            return iDx + iDy;
        }

        public static double Octile(int iDx, int iDy)
        {
            var min = Math.Min(val1: iDx, val2: iDy);
            var max = Math.Max(val1: iDx, val2: iDy);
            return min * Sqrt2 + max - min;
        }

        #endregion
    }
}