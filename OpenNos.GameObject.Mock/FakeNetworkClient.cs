﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using OpenNos.Core.Cryptography;
using OpenNos.Core.Networking;
using OpenNos.Core.Networking.Communication.Scs.Communication.Messages;
using OpenNos.Core.Serializing;

namespace OpenNos.GameObject.Mock
{
    public class FakeNetworkClient : INetworkClient
    {
        #region Instantiation

        public FakeNetworkClient()
        {
            _clientId = 0;
            SentPackets = new Queue<string>();
            ReceivedPackets = new Queue<string>();
            _lastPacketId = 1;
            IsConnected = true;
        }

        #endregion

        #region Events

        public event EventHandler<MessageEventArgs> MessageReceived;

        #endregion

        #region Members

        long _clientId;

        ClientSession _clientSession;

        long _lastPacketId;

        #endregion

        #region Properties

        public long ClientId
        {
            get
            {
                if (_clientId == 0) _clientId = GameObjectMockHelper.Instance.GetNextClientId();

                return _clientId;
            }

            set => _clientId = value;
        }

        public string IpAddress
        {
            get => "127.0.0.1";
        }

        public bool IsConnected { get; private set; }

        public bool IsDisposing { get; set; }

        public Queue<string> ReceivedPackets { get; }

        public Queue<string> SentPackets { get; }

        public ClientSession Session
        {
            get => GetClientSession();
        }

        #endregion

        #region Methods

        public async Task ClearLowPriorityQueueAsync()
        {
            await Task.CompletedTask;
        }

        public void Disconnect()
        {
            IsConnected = false;
        }

        public ClientSession GetClientSession()
        {
            return _clientSession;
        }

        public void Initialize(CryptographyBase encryptor)
        {
            // nothing to do here
        }

        /// <summary>
        ///     Send a Packet to the Server as the Fake client receives it and triggers a Handler method.
        /// </summary>
        /// <param name="packet"></param>
        public void ReceivePacket(string packet)
        {
            Debug.WriteLine(message: $"Enqueued {packet}");
            var encoding = new UTF8Encoding();
            var buf = encoding.GetBytes(s: $"{_lastPacketId} {packet}");
            MessageReceived?.Invoke(sender: this,
                e: new MessageEventArgs(message: new ScsRawDataMessage(messageData: buf),
                    receivedTimestamp: DateTime.Now));
            _lastPacketId++;
        }

        /// <summary>
        ///     Send a packet to the Server as the Fake client receives it and triggers a Handler method.
        /// </summary>
        /// <param name="packet">Packet created thru PacketFactory.</param>
        public void ReceivePacket(PacketDefinition packet)
        {
            ReceivePacket(packet: PacketFactory.Serialize(packet: packet));
        }

        public void SendPacket(string packet, byte priority = 10)
        {
            SentPackets.Enqueue(item: packet);
        }

        public void SendPacketFormat(string packet, params object[] param)
        {
            SentPackets.Enqueue(item: string.Format(format: packet, args: param));
        }

        public void SendPackets(IEnumerable<string> packets, byte priority = 10)
        {
            foreach (var packet in packets) SendPacket(packet: packet, priority: priority);
        }

        public void SetClientSession(object clientSession)
        {
            _clientSession = (ClientSession)clientSession;
        }

        #endregion
    }
}