﻿namespace OpenNos.GameObject.Mock
{
    public class GameObjectMockHelper
    {
        #region Properties

        public static GameObjectMockHelper Instance
        {
            get => _instance ?? (_instance = new GameObjectMockHelper());
        }

        #endregion

        #region Methods

        public long GetNextClientId()
        {
            return ++_nextClientId;
        }

        #endregion

        #region Members

        static GameObjectMockHelper _instance;

        long _nextClientId;

        #endregion
    }
}