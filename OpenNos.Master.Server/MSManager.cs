﻿using System.Collections.Generic;
using System.Configuration;
using OpenNos.Core.Threading;
using OpenNos.Master.Library.Data;
using OpenNos.SCS.Communication.ScsServices.Service;

namespace OpenNos.Master.Server
{
    internal class MsManager
    {
        #region Instantiation

        public MsManager()
        {
            WorldServers = new List<WorldServer>();
            LoginServers = new List<IScsServiceClient>();
            ConnectedAccounts = new ThreadSafeGenericList<AccountConnection>();
            AuthentificatedClients = new ThreadSafeGenericLockedList<long>();
            ConfigurationObject = new ConfigurationObject
            {
                RateXp = int.Parse(s: ConfigurationManager.AppSettings[name: "RateXp"]),
                RateHeroicXp = int.Parse(s: ConfigurationManager.AppSettings[name: "RateHeroicXp"]),
                RateDrop = int.Parse(s: ConfigurationManager.AppSettings[name: "RateDrop"]),
                MaxGold = long.Parse(s: ConfigurationManager.AppSettings[name: "MaxGold"]),
                RateGoldDrop = int.Parse(s: ConfigurationManager.AppSettings[name: "GoldRateDrop"]),
                RateGold = int.Parse(s: ConfigurationManager.AppSettings[name: "RateGold"]),
                RateReputation = int.Parse(s: ConfigurationManager.AppSettings[name: "RateReputation"]),
                RateFairyXp = int.Parse(s: ConfigurationManager.AppSettings[name: "RateFairyXp"]),
                PartnerSpXp = long.Parse(s: ConfigurationManager.AppSettings[name: "PartnerSpXp"]),
                MaxLevel = byte.Parse(s: ConfigurationManager.AppSettings[name: "MaxLevel"]),
                MaxJobLevel = byte.Parse(s: ConfigurationManager.AppSettings[name: "MaxJobLevel"]),
                MaxSpLevel = byte.Parse(s: ConfigurationManager.AppSettings[name: "MaxSPLevel"]),
                MaxHeroLevel = byte.Parse(s: ConfigurationManager.AppSettings[name: "MaxHeroLevel"]),
                HeroicStartLevel = byte.Parse(s: ConfigurationManager.AppSettings[name: "HeroicStartLevel"]),
                MaxUpgrade = byte.Parse(s: ConfigurationManager.AppSettings[name: "MaxUpgrade"]),
                SceneOnCreate = bool.Parse(value: ConfigurationManager.AppSettings[name: "SceneOnCreate"]),
                SessionLimit = int.Parse(s: ConfigurationManager.AppSettings[name: "SessionLimit"]),
                WorldInformation = bool.Parse(value: ConfigurationManager.AppSettings[name: "WorldInformation"]),
                IsAntiCheatEnabled = bool.Parse(value: ConfigurationManager.AppSettings[name: "IsAntiCheatEnabled"]),
                AntiCheatClientKey = ConfigurationManager.AppSettings[name: "AntiCheatClientKey"],
                AntiCheatServerKey = ConfigurationManager.AppSettings[name: "AntiCheatServerKey"],
                Act4Ip = ConfigurationManager.AppSettings[name: "Act4IP"],
                Act4Port = int.Parse(s: ConfigurationManager.AppSettings[name: "Act4Port"]),
                MallBaseUrl = ConfigurationManager.AppSettings[name: "MallBaseURL"],
                MallApiKey = ConfigurationManager.AppSettings[name: "MallAPIKey"],
                UseLogService = bool.Parse(value: ConfigurationManager.AppSettings[name: "UseLogService"]),
                QuestDropRate = int.Parse(s: ConfigurationManager.AppSettings[name: "QuestDropRate"]),
                HalloweenEvent = bool.Parse(value: ConfigurationManager.AppSettings[name: "HalloweenEvent"]),
                ChristmasEvent = bool.Parse(value: ConfigurationManager.AppSettings[name: "ChristmasEvent"])
            };
        }

        #endregion

        #region Members

        static MsManager _instance;

        #endregion

        #region Properties

        public static MsManager Instance
        {
            get => _instance ?? (_instance = new MsManager());
        }

        public ThreadSafeGenericLockedList<long> AuthentificatedClients { get; set; }

        public ConfigurationObject ConfigurationObject { get; set; }

        public ThreadSafeGenericList<AccountConnection> ConnectedAccounts { get; set; }

        public List<IScsServiceClient> LoginServers { get; set; }

        public List<WorldServer> WorldServers { get; set; }

        #endregion
    }
}