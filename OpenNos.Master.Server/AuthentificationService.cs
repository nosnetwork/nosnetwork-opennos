﻿using System.Configuration;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.ScsServices.Service;

namespace OpenNos.Master.Server
{
    internal class AuthentificationService : ScsService, IAuthentificationService
    {
        public bool Authenticate(string authKey)
        {
            if (string.IsNullOrWhiteSpace(value: authKey)) return false;

            if (authKey == ConfigurationManager.AppSettings[name: "AuthentificationServiceAuthKey"])
            {
                MsManager.Instance.AuthentificatedClients.Add(item: CurrentClient.ClientId);
                return true;
            }

            return false;
        }

        public AccountDto ValidateAccount(string userName, string passHash)
        {
            if ( /*!MSManager.Instance.AuthentificatedClients.Any(s => s.Equals(CurrentClient.ClientId)) || */
                string.IsNullOrEmpty(value: userName) || string.IsNullOrEmpty(value: passHash)) return null;

            var account = DaoFactory.AccountDao.LoadByName(name: userName);

            if (account?.Password == passHash) return account;
            return null;
        }

        public CharacterDto ValidateAccountAndCharacter(string userName, string characterName, string passHash)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId)) ||
                string.IsNullOrEmpty(value: userName) || string.IsNullOrEmpty(value: characterName) ||
                string.IsNullOrEmpty(value: passHash)) return null;

            var account = DaoFactory.AccountDao.LoadByName(name: userName);

            if (account?.Password == passHash)
            {
                var character = DaoFactory.CharacterDao.LoadByName(name: characterName);
                if (character?.AccountId == account.AccountId) return character;
                return null;
            }

            return null;
        }
    }
}