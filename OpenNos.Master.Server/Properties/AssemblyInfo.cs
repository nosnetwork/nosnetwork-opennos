﻿using System.Reflection;
using System.Runtime.InteropServices;
using log4net.Config;

// Allgemeine Informationen über eine Assembly werden über die folgenden Attribute gesteuert. Ändern
// Sie diese Attributwerte, um die Informationen zu ändern, die einer Assembly zugeordnet sind.
[assembly: AssemblyTitle(title: "OpenNos Master Server")]
[assembly: AssemblyDescription(description: "")]
[assembly: AssemblyConfiguration(configuration: "")]
[assembly: AssemblyCompany(company: "")]
[assembly: AssemblyProduct(product: "OpenNos.Master.Server")]
[assembly: AssemblyCopyright(copyright: "Copyright ©  2017")]
[assembly: AssemblyTrademark(trademark: "")]
[assembly: AssemblyCulture(culture: "")]

// Durch Festlegen von ComVisible auf FALSE werden die Typen in dieser Assembly für COM-Komponenten
// unsichtbar. Wenn Sie auf einen Typ in dieser Assembly von COM aus zugreifen müssen, sollten Sie
// das ComVisible-Attribut für diesen Typ auf "True" festlegen.
[assembly: ComVisible(visibility: false)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
[assembly: Guid(guid: "9aa91bf5-88e7-4130-9f42-73ae206e2916")]

// Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
//
// Hauptversion Nebenversion Buildnummer Revision
//
// Sie können alle Werte angeben oder Standardwerte für die Build- und Revisionsnummern verwenden,
// übernehmen, indem Sie "*" eingeben: [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(version: "1.1.*")]
[assembly: XmlConfigurator(Watch = true)]