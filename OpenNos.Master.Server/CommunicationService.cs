﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using OpenNos.Core;
using OpenNos.DAL;
using OpenNos.Domain;
using OpenNos.Master.Library.Data;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.SCS.Communication.ScsServices.Service;

namespace OpenNos.Master.Server
{
    internal class CommunicationService : ScsService, ICommunicationService
    {
        #region Methods

        public bool Authenticate(string authKey)
        {
            if (string.IsNullOrWhiteSpace(value: authKey)) return false;

            if (authKey == ConfigurationManager.AppSettings[name: "MasterAuthKey"])
            {
                MsManager.Instance.AuthentificatedClients.Add(item: CurrentClient.ClientId);
                return true;
            }

            return false;
        }

        public void Cleanup()
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            MsManager.Instance.ConnectedAccounts.Clear();
            MsManager.Instance.WorldServers.Clear();
        }

        public void CleanupOutdatedSession()
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            var tmp = new AccountConnection[MsManager.Instance.ConnectedAccounts.Count + 20];
            lock (MsManager.Instance.ConnectedAccounts)
            {
                MsManager.Instance.ConnectedAccounts.CopyTo(grpmembers: tmp);
            }

            foreach (var account in tmp.Where(predicate: a => a?.LastPulse.AddMinutes(value: 5) <= DateTime.Now))
                KickSession(accountId: account.AccountId, sessionId: null);
        }

        public bool ConnectAccount(Guid worldId, long accountId, int sessionId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId)))
                return false;

            var account =
                MsManager.Instance.ConnectedAccounts.Find(predicate: a =>
                    a.AccountId.Equals(obj: accountId) && a.SessionId.Equals(obj: sessionId));
            if (account != null)
                account.ConnectedWorld = MsManager.Instance.WorldServers.Find(match: w => w.Id.Equals(g: worldId));
            return account != null && account.ConnectedWorld != null;
        }

        public bool ConnectAccountCrossServer(Guid worldId, long accountId, int sessionId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId)))
                return false;
            var account = MsManager.Instance.ConnectedAccounts
                .Where(predicate: a => a.AccountId.Equals(obj: accountId) && a.SessionId.Equals(obj: sessionId))
                .FirstOrDefault();
            if (account != null)
            {
                account.CanLoginCrossServer = false;
                account.OriginWorld = account.ConnectedWorld;
                account.ConnectedWorld = MsManager.Instance.WorldServers.Find(match: s => s.Id.Equals(g: worldId));
                if (account.ConnectedWorld != null) return true;
            }

            return false;
        }

        public bool ConnectCharacter(Guid worldId, long characterId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId)))
                return false;

            //Multiple WorldGroups not yet supported by DAOFactory
            var accountId = DaoFactory.CharacterDao.LoadById(characterId: characterId)?.AccountId ?? 0;

            var account = MsManager.Instance.ConnectedAccounts.Find(predicate: a =>
                a.AccountId.Equals(obj: accountId) && a.ConnectedWorld.Id.Equals(g: worldId));
            if (account != null)
            {
                account.CharacterId = characterId;
                foreach (var world in MsManager.Instance.WorldServers.Where(predicate: w =>
                    w.WorldGroup.Equals(value: account.ConnectedWorld.WorldGroup)))
                    world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                        .CharacterConnected(characterId: characterId);
                return true;
            }

            return false;
        }

        public void DisconnectAccount(long accountId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;
            if (MsManager.Instance.ConnectedAccounts.Any(predicate: s =>
                s.AccountId.Equals(obj: accountId) && s.CanLoginCrossServer))
            {
            }
            else
            {
                MsManager.Instance.ConnectedAccounts.RemoveAll(match: c => c.AccountId.Equals(obj: accountId));
            }
        }

        public void DisconnectCharacter(Guid worldId, long characterId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            foreach (var account in MsManager.Instance.ConnectedAccounts.Where(predicate: c =>
                c.CharacterId.Equals(obj: characterId) && c.ConnectedWorld.Id.Equals(g: worldId)))
            {
                foreach (var world in MsManager.Instance.WorldServers.Where(predicate: w =>
                    w.WorldGroup.Equals(value: account.ConnectedWorld.WorldGroup)))
                    world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                        .CharacterDisconnected(characterId: characterId);
                if (!account.CanLoginCrossServer)
                {
                    account.CharacterId = 0;
                    account.ConnectedWorld = null;
                }
            }
        }

        public int? GetChannelIdByWorldId(Guid worldId)
        {
            return MsManager.Instance.WorldServers.Find(match: w => w.Id == worldId)?.ChannelId;
        }

        public long[][] GetOnlineCharacters()
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return null;

            var connections = MsManager.Instance.ConnectedAccounts.Where(predicate: s => s.CharacterId != 0);

            var result = new long[connections.Count][];

            var i = 0;
            foreach (var acc in connections)
            {
                result[i] = new long[3];
                result[i][0] = acc.CharacterId;
                result[i][1] = acc.ConnectedWorld?.ChannelId ?? 0;
                result[i][2] = acc.SessionId;
                i++;
            }

            return result;
        }

        public bool IsAccountConnected(long accountId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId)))
                return false;

            return MsManager.Instance.ConnectedAccounts.Any(predicate: c =>
                c.AccountId == accountId && c.ConnectedWorld != null &&
                c.LastPulse.AddSeconds(value: 90) >= DateTime.Now);
        }

        public bool IsAct4Online(string worldGroup)
        {
            return MsManager.Instance.WorldServers.Any(predicate: w => w.WorldGroup.Equals(value: worldGroup)
                                                                       && w.Endpoint.IpAddress ==
                                                                       MsManager.Instance.ConfigurationObject.Act4Ip &&
                                                                       w.Endpoint.TcpPort == MsManager.Instance
                                                                           .ConfigurationObject
                                                                           .Act4Port);
        }

        public bool IsCharacterConnected(string worldGroup, long characterId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId)))
                return false;

            return MsManager.Instance.ConnectedAccounts.Any(predicate: c =>
                c.ConnectedWorld != null && c.ConnectedWorld.WorldGroup == worldGroup && c.CharacterId == characterId);
        }

        public bool IsCrossServerLoginPermitted(long accountId, int sessionId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId)))
                return false;

            return MsManager.Instance.ConnectedAccounts.Any(predicate: s =>
                s.AccountId.Equals(obj: accountId) && s.SessionId.Equals(obj: sessionId) && s.CanLoginCrossServer);
        }

        public bool IsLoginPermitted(long accountId, int sessionId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId)))
                return false;

            return MsManager.Instance.ConnectedAccounts.Any(predicate: s =>
                s.AccountId.Equals(obj: accountId) && s.SessionId.Equals(obj: sessionId) && s.ConnectedWorld == null);
        }

        public void KickSession(long? accountId, int? sessionId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            foreach (var world in MsManager.Instance.WorldServers)
                world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                    .KickSession(accountId: accountId, sessionId: sessionId);
            if (accountId.HasValue)
                MsManager.Instance.ConnectedAccounts.RemoveAll(match: s => s.AccountId.Equals(obj: accountId.Value));
            else if (sessionId.HasValue)
                MsManager.Instance.ConnectedAccounts.RemoveAll(match: s => s.SessionId.Equals(obj: sessionId.Value));
        }

        public void PulseAccount(long accountId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;
            var account = MsManager.Instance.ConnectedAccounts.Find(predicate: a => a.AccountId.Equals(obj: accountId));
            if (account != null) account.LastPulse = DateTime.Now;
        }

        public void RefreshPenalty(int penaltyId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            foreach (var world in MsManager.Instance.WorldServers)
                world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                    .UpdatePenaltyLog(penaltyLogId: penaltyId);
            foreach (var login in MsManager.Instance.LoginServers)
                login.GetClientProxy<ICommunicationClient>().UpdatePenaltyLog(penaltyLogId: penaltyId);
        }

        public void RegisterAccountLogin(long accountId, int sessionId, string ipAddress)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;
            MsManager.Instance.ConnectedAccounts.RemoveAll(match: a => a.AccountId.Equals(obj: accountId));
            MsManager.Instance.ConnectedAccounts.Add(value: new AccountConnection(accountId: accountId,
                sessionId: sessionId, ipAddress: ipAddress));
        }

        public void RegisterCrossServerAccountLogin(long accountId, int sessionId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;
            var account = MsManager.Instance.ConnectedAccounts
                .Where(predicate: a => a.AccountId.Equals(obj: accountId) && a.SessionId.Equals(obj: sessionId))
                .FirstOrDefault();

            if (account != null) account.CanLoginCrossServer = true;
        }

        public int? RegisterWorldServer(SerializableWorldServer worldServer)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return null;
            var ws = new WorldServer(id: worldServer.Id,
                endpoint: new ScsTcpEndPoint(ipAddress: worldServer.EndPointIp, port: worldServer.EndPointPort),
                accountLimit: worldServer.AccountLimit,
                worldGroup: worldServer.WorldGroup)
            {
                CommunicationServiceClient = CurrentClient,
                ChannelId = Enumerable.Range(start: 1, count: 30).Except(second: MsManager.Instance.WorldServers
                    .Where(predicate: w => w.WorldGroup.Equals(value: worldServer.WorldGroup))
                    .OrderBy(keySelector: w => w.ChannelId)
                    .Select(selector: w => w.ChannelId)).First()
            };
            if (worldServer.EndPointPort == MsManager.Instance.ConfigurationObject.Act4Port) ws.ChannelId = 51;
            MsManager.Instance.WorldServers.Add(item: ws);
            return ws.ChannelId;
        }

        public void Restart(string worldGroup, int time = 5)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            if (worldGroup == "*")
                foreach (var world in MsManager.Instance.WorldServers)
                    world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>().Restart(time: time);
            else
                foreach (var world in MsManager.Instance.WorldServers.Where(predicate: w =>
                    w.WorldGroup.Equals(value: worldGroup)))
                    world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>().Restart(time: time);
        }

        public long[][] RetrieveOnlineCharacters(long characterId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return null;

            var connections = MsManager.Instance.ConnectedAccounts.Where(predicate: s =>
                s.IpAddress == MsManager.Instance.ConnectedAccounts.Find(predicate: f => f.CharacterId == characterId)
                    ?.IpAddress && s.CharacterId != 0);

            var result = new long[connections.Count][];

            var i = 0;
            foreach (var acc in connections)
            {
                result[i] = new long[2];
                result[i][0] = acc.CharacterId;
                result[i][1] = acc.ConnectedWorld?.ChannelId ?? 0;
                i++;
            }

            return result;
        }

        public string RetrieveOriginWorld(long accountId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return null;

            var account = MsManager.Instance.ConnectedAccounts.Find(predicate: s => s.AccountId.Equals(obj: accountId));
            if (account?.OriginWorld != null)
                return $"{account.OriginWorld.Endpoint.IpAddress}:{account.OriginWorld.Endpoint.TcpPort}";
            return null;
        }

        public string RetrieveRegisteredWorldServers(string username, int sessionId, bool ignoreUserName)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return null;

            var lastGroup = "";
            byte worldCount = 0;
            var channelPacket = "NsTeST" + (ignoreUserName ? "" : " " + username) + $" {sessionId} ";

            foreach (var world in MsManager.Instance.WorldServers.OrderBy(keySelector: w => w.WorldGroup))
            {
                if (lastGroup != world.WorldGroup) worldCount++;
                lastGroup = world.WorldGroup;

                var currentlyConnectedAccounts =
                    MsManager.Instance.ConnectedAccounts.CountLinq(predicate: a =>
                        a.ConnectedWorld?.ChannelId == world.ChannelId);
                var channelcolor = (int)Math.Round(a: (double)currentlyConnectedAccounts / world.AccountLimit * 20) +
                                   1;

                if (world.ChannelId == 51) continue;

                channelPacket +=
                    $"{world.Endpoint.IpAddress}:{world.Endpoint.TcpPort}:{channelcolor}:{worldCount}.{world.ChannelId}.{world.WorldGroup} ";
            }

            return channelPacket;
        }

        public IEnumerable<string> RetrieveServerStatistics()
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return null;

            var result = new List<string>();

            try
            {
                var groups = new List<string>();
                foreach (var s in MsManager.Instance.WorldServers.Select(selector: s => s.WorldGroup))
                    if (!groups.Contains(item: s))
                        groups.Add(item: s);
                var totalsessions = 0;
                foreach (var message in groups)
                {
                    result.Add(item: $"==={message}===");
                    var groupsessions = 0;
                    foreach (var world in MsManager.Instance.WorldServers.Where(predicate: w =>
                        w.WorldGroup.Equals(value: message)))
                    {
                        var sessions =
                            MsManager.Instance.ConnectedAccounts.CountLinq(predicate: a =>
                                a.ConnectedWorld?.Id.Equals(g: world.Id) == true);
                        result.Add(item: $"Channel {world.ChannelId}: {sessions} Sessions");
                        groupsessions += sessions;
                    }

                    result.Add(item: $"Group Total: {groupsessions} Sessions");
                    totalsessions += groupsessions;
                }

                result.Add(item: $"Environment Total: {totalsessions} Sessions");
            }
            catch (Exception ex)
            {
                Logger.LogEventError(logEvent: "RETRIEVE_EXCEPTION", data: "Error while retreiving server Statistics:",
                    ex: ex);
            }

            return result;
        }

        public void RunGlobalEvent(EventType eventType)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            foreach (var world in MsManager.Instance.WorldServers)
                world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                    .RunGlobalEvent(eventType: eventType);
        }

        public int? SendMessageToCharacter(ScsCharacterMessage message)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return null;

            var sourceWorld = MsManager.Instance.WorldServers.Find(match: s => s.Id.Equals(g: message.SourceWorldId));
            if (message == null || message.Message == null || sourceWorld == null) return null;

            switch (message.Type)
            {
                case MessageType.Family:
                case MessageType.FamilyChat:
                case MessageType.Broadcast:
                    foreach (var world in MsManager.Instance.WorldServers.Where(predicate: w =>
                        w.WorldGroup.Equals(value: sourceWorld.WorldGroup)))
                        world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                            .SendMessageToCharacter(message: message);
                    return -1;

                case MessageType.PrivateChat:
                    if (message.DestinationCharacterId.HasValue)
                    {
                        var receiverAccount = MsManager.Instance.ConnectedAccounts.Find(predicate: a =>
                            a.CharacterId.Equals(obj: message.DestinationCharacterId.Value));
                        if (receiverAccount?.ConnectedWorld != null)
                        {
                            if (sourceWorld.ChannelId == 51 && receiverAccount.ConnectedWorld.ChannelId == 51
                                                            && DaoFactory.CharacterDao
                                                                .LoadById(characterId: message.SourceCharacterId)
                                                                .Faction
                                                            != DaoFactory.CharacterDao
                                                                .LoadById(characterId: (long)message
                                                                    .DestinationCharacterId)
                                                                .Faction)
                            {
                                var senderAccount = MsManager.Instance.ConnectedAccounts.Find(predicate: a =>
                                    a.CharacterId.Equals(obj: message.SourceCharacterId));
                                message.Message = $"talk {message.DestinationCharacterId} " +
                                                  Language.Instance.GetMessageFromKey(
                                                      key: "CANT_TALK_OPPOSITE_FACTION");
                                message.DestinationCharacterId = message.SourceCharacterId;
                                senderAccount.ConnectedWorld.CommunicationServiceClient
                                    .GetClientProxy<ICommunicationClient>().SendMessageToCharacter(message: message);
                                return -1;
                            }

                            receiverAccount.ConnectedWorld.CommunicationServiceClient
                                .GetClientProxy<ICommunicationClient>().SendMessageToCharacter(message: message);
                            return receiverAccount.ConnectedWorld.ChannelId;
                        }
                    }

                    break;
                case MessageType.Whisper:
                    if (message.DestinationCharacterId.HasValue)
                    {
                        var receiverAccount = MsManager.Instance.ConnectedAccounts.Find(predicate: a =>
                            a.CharacterId.Equals(obj: message.DestinationCharacterId.Value));
                        if (receiverAccount?.ConnectedWorld != null)
                        {
                            if (sourceWorld.ChannelId == 51 && receiverAccount.ConnectedWorld.ChannelId == 51
                                                            && DaoFactory.CharacterDao
                                                                .LoadById(characterId: message.SourceCharacterId)
                                                                .Faction
                                                            != DaoFactory.CharacterDao
                                                                .LoadById(characterId: (long)message
                                                                    .DestinationCharacterId)
                                                                .Faction)
                            {
                                MsManager.Instance.ConnectedAccounts.Find(predicate: a =>
                                    a.CharacterId.Equals(obj: message.SourceCharacterId));
                                message.Message =
                                    $"say 1 {message.SourceCharacterId} 11 {Language.Instance.GetMessageFromKey(key: "CANT_TALK_OPPOSITE_FACTION")}";
                                message.DestinationCharacterId = message.SourceCharacterId;
                                message.Type = MessageType.Other;
                                receiverAccount.ConnectedWorld.CommunicationServiceClient
                                    .GetClientProxy<ICommunicationClient>().SendMessageToCharacter(message: message);
                                return -1;
                            }

                            receiverAccount.ConnectedWorld.CommunicationServiceClient
                                .GetClientProxy<ICommunicationClient>().SendMessageToCharacter(message: message);
                            return receiverAccount.ConnectedWorld.ChannelId;
                        }
                    }

                    break;
                case MessageType.WhisperSupport:
                case MessageType.WhisperGm:
                    if (message.DestinationCharacterId.HasValue)
                    {
                        var account = MsManager.Instance.ConnectedAccounts.Find(predicate: a =>
                            a.CharacterId.Equals(obj: message.DestinationCharacterId.Value));
                        if (account?.ConnectedWorld != null)
                        {
                            account.ConnectedWorld.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                                .SendMessageToCharacter(message: message);
                            return account.ConnectedWorld.ChannelId;
                        }
                    }

                    break;

                case MessageType.Shout:
                    foreach (var world in MsManager.Instance.WorldServers)
                        world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                            .SendMessageToCharacter(message: message);
                    return -1;

                case MessageType.Other:
                    var receiverAcc = MsManager.Instance.ConnectedAccounts.Find(predicate: a =>
                        message.DestinationCharacterId != null &&
                        a.CharacterId.Equals(obj: message.DestinationCharacterId.Value));
                    if (receiverAcc?.ConnectedWorld != null)
                    {
                        receiverAcc.ConnectedWorld.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                            .SendMessageToCharacter(message: message);
                        return receiverAcc.ConnectedWorld.ChannelId;
                    }

                    break;
            }

            return null;
        }

        public void Shutdown(string worldGroup)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            if (worldGroup == "*")
                foreach (var world in MsManager.Instance.WorldServers)
                    world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>().Shutdown();
            else
                foreach (var world in MsManager.Instance.WorldServers.Where(predicate: w =>
                    w.WorldGroup.Equals(value: worldGroup)))
                    world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>().Shutdown();
        }

        public void UnregisterWorldServer(Guid worldId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            MsManager.Instance.ConnectedAccounts.RemoveAll(match: a =>
                a?.ConnectedWorld?.Id.Equals(g: worldId) == true);
            MsManager.Instance.WorldServers.RemoveAll(match: w => w.Id.Equals(g: worldId));
        }

        public void UpdateBazaar(string worldGroup, long bazaarItemId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            foreach (var world in MsManager.Instance.WorldServers.Where(predicate: w =>
                w.WorldGroup.Equals(value: worldGroup)))
                world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                    .UpdateBazaar(bazaarItemId: bazaarItemId);
        }

        public void UpdateFamily(string worldGroup, long familyId, bool changeFaction)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            foreach (var world in MsManager.Instance.WorldServers.Where(predicate: w =>
                w.WorldGroup.Equals(value: worldGroup)))
                world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                    .UpdateFamily(familyId: familyId, changeFaction: changeFaction);
        }

        public void UpdateRelation(string worldGroup, long relationId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            foreach (var world in MsManager.Instance.WorldServers.Where(predicate: w =>
                w.WorldGroup.Equals(value: worldGroup)))
                world.CommunicationServiceClient.GetClientProxy<ICommunicationClient>()
                    .UpdateRelation(relationId: relationId);
        }

        #endregion
    }
}