﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using log4net;
using OpenNos.Core;
using OpenNos.DAL.EF.Helpers;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.SCS.Communication.ScsServices.Service;

namespace OpenNos.Master.Server
{
    internal static class Program
    {
        #region Members

        static bool IsDebug;

        #endregion

        #region Methods

        public static void Main(string[] args)
        {
            try
            {
#if DEBUG
                IsDebug = true;
#endif
                CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo(name: "en-US");
                // ReSharper disable once LocalizableElement
                Console.Title = $"OpenNos Master Server{(IsDebug ? " Development Environment" : "")}";

                var ignoreStartupMessages = false;
                var ignoreTelemetry = false;
                foreach (var arg in args)
                    switch (arg)
                    {
                        case "--nomsg":
                            ignoreStartupMessages = true;
                            break;

                        case "--notelemetry":
                            ignoreTelemetry = true;
                            break;
                    }

                // initialize Logger
                Logger.InitializeLogger(log: LogManager.GetLogger(type: typeof(Program)));

                var port = Convert.ToInt32(value: ConfigurationManager.AppSettings[name: "MasterPort"]);
                if (!ignoreStartupMessages)
                {
                    var assembly = Assembly.GetExecutingAssembly();
                    FileVersionInfo.GetVersionInfo(fileName: assembly.Location);
                    var text = "MASTER SERVER by NN Team";
                    var offset = Console.WindowWidth / 2 + text.Length / 2;
                    var separator = new string(c: '=', count: Console.WindowWidth);
                    Console.WriteLine(value: separator + string.Format(format: "{0," + offset + "}\n", arg0: text) +
                                             separator);
                }

                // initialize DB
                if (!DataAccessHelper.Initialize())
                {
                    Console.ReadLine();
                    return;
                }

                Logger.Info(message: Language.Instance.GetMessageFromKey(key: "CONFIG_LOADED"));

                try
                {
                    // configure Services and Service Host
                    var ipAddress = ConfigurationManager.AppSettings[name: "MasterIP"];
                    var server =
                        ScsServiceBuilder.CreateService(endPoint: new ScsTcpEndPoint(ipAddress: ipAddress, port: port));

                    server.AddService<ICommunicationService, CommunicationService>(
                        service: new CommunicationService());
                    server.AddService<IConfigurationService, ConfigurationService>(
                        service: new ConfigurationService());
                    server.AddService<IMailService, MailService>(service: new MailService());
                    server.AddService<IMallService, MallService>(service: new MallService());
                    server.AddService<IAuthentificationService, AuthentificationService>(
                        service: new AuthentificationService());
                    server.ClientConnected += OnClientConnected;
                    server.ClientDisconnected += OnClientDisconnected;

                    server.Start();
                    Logger.Info(message: Language.Instance.GetMessageFromKey(key: "STARTED"));
                    if (!ignoreTelemetry)
                    {
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(data: "General Error Server", ex: ex);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(data: "General Error", ex: ex);
                Console.ReadKey();
            }
        }

        static void OnClientConnected(object sender, ServiceClientEventArgs e)
        {
            Logger.Info(message: Language.Instance.GetMessageFromKey(key: "NEW_CONNECT") + e.Client.ClientId);
        }

        static void OnClientDisconnected(object sender, ServiceClientEventArgs e)
        {
            Logger.Info(message: Language.Instance.GetMessageFromKey(key: "DISCONNECT") + e.Client.ClientId);
        }

        #endregion
    }
}