﻿using System;
using System.Configuration;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.ScsServices.Service;

namespace OpenNos.Master.Server
{
    internal class MailService : ScsService, IMailService
    {
        public bool Authenticate(string authKey, Guid serverId)
        {
            if (string.IsNullOrWhiteSpace(value: authKey)) return false;

            if (authKey == ConfigurationManager.AppSettings[name: "MasterAuthKey"])
            {
                MsManager.Instance.AuthentificatedClients.Add(item: CurrentClient.ClientId);

                var ws = MsManager.Instance.WorldServers.Find(match: s => s.Id == serverId);
                if (ws != null) ws.MailServiceClient = CurrentClient;
                return true;
            }

            return false;
        }

        public void SendMail(MailDto mail)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            DaoFactory.MailDao.InsertOrUpdate(mail: ref mail);

            if (mail.IsSenderCopy)
            {
                var account =
                    MsManager.Instance.ConnectedAccounts.Find(predicate: a => a.CharacterId.Equals(obj: mail.SenderId));
                if (account?.ConnectedWorld != null)
                    account.ConnectedWorld.MailServiceClient.GetClientProxy<IMailClient>().MailSent(mail: mail);
            }
            else
            {
                var account =
                    MsManager.Instance.ConnectedAccounts.Find(
                        predicate: a => a.CharacterId.Equals(obj: mail.ReceiverId));
                if (account?.ConnectedWorld != null)
                    account.ConnectedWorld.MailServiceClient.GetClientProxy<IMailClient>().MailSent(mail: mail);
            }
        }
    }
}