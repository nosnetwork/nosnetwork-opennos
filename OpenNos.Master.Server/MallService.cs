﻿using System;
using System.Collections.Generic;
using System.Configuration;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Master.Library.Data;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.ScsServices.Service;

namespace OpenNos.Master.Server
{
    internal class MallService : ScsService, IMallService
    {
        public bool Authenticate(string authKey)
        {
            if (string.IsNullOrWhiteSpace(value: authKey)) return false;

            if (authKey == ConfigurationManager.AppSettings[name: "MasterAuthKey"])
            {
                MsManager.Instance.AuthentificatedClients.Add(item: CurrentClient.ClientId);
                return true;
            }

            return false;
        }

        public IEnumerable<CharacterDto> GetCharacters(long accountId)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return null;

            return DaoFactory.CharacterDao.LoadByAccount(accountId: accountId);
        }

        public void SendItem(long characterId, MallItem item)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;

            var mailDto = new MailDto
            {
                AttachmentAmount = (short)item.Amount,
                AttachmentRarity = item.Rare,
                AttachmentUpgrade = item.Upgrade,
                AttachmentDesign = item.Design,
                AttachmentVNum = item.ItemVNum,
                Date = DateTime.Now,
                EqPacket = "",
                IsOpened = false,
                IsSenderCopy = false,
                Message = "",
                ReceiverId = characterId,
                SenderId = characterId,
                Title = "ItemMall"
            };

            DaoFactory.MailDao.InsertOrUpdate(mail: ref mailDto);

            var mail = new MailDto
            {
                AttachmentAmount = mailDto.AttachmentAmount,
                AttachmentRarity = mailDto.AttachmentRarity,
                AttachmentUpgrade = mailDto.AttachmentUpgrade,
                AttachmentDesign = mailDto.AttachmentDesign,
                AttachmentVNum = mailDto.AttachmentVNum,
                Date = mailDto.Date,
                EqPacket = mailDto.EqPacket,
                IsOpened = mailDto.IsOpened,
                IsSenderCopy = mailDto.IsSenderCopy,
                MailId = mailDto.MailId,
                Message = mailDto.Message,
                ReceiverId = mailDto.ReceiverId,
                SenderClass = mailDto.SenderClass,
                SenderGender = mailDto.SenderGender,
                SenderHairColor = mailDto.SenderHairColor,
                SenderHairStyle = mailDto.SenderHairStyle,
                SenderId = mailDto.SenderId,
                SenderMorphId = mailDto.SenderMorphId,
                Title = mailDto.Title
            };

            var account =
                MsManager.Instance.ConnectedAccounts.Find(predicate: a => a.CharacterId.Equals(obj: mail.ReceiverId));
            if (account?.ConnectedWorld != null)
                account.ConnectedWorld.MailServiceClient.GetClientProxy<IMailClient>().MailSent(mail: mail);
        }

        public void SendStaticBonus(long characterId, MallStaticBonus item)
        {
            throw new NotImplementedException();
        }

        public AccountDto ValidateAccount(string userName, string passHash)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId)) ||
                string.IsNullOrEmpty(value: userName) || string.IsNullOrEmpty(value: passHash)) return null;

            var account = DaoFactory.AccountDao.LoadByName(name: userName);

            if (account?.Password == passHash) return account;
            return null;
        }
    }
}