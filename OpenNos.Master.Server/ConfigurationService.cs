﻿using System;
using System.Configuration;
using OpenNos.Master.Library.Data;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.ScsServices.Service;

namespace OpenNos.Master.Server
{
    internal class ConfigurationService : ScsService, IConfigurationService
    {
        public bool Authenticate(string authKey, Guid serverId)
        {
            if (string.IsNullOrWhiteSpace(value: authKey)) return false;

            if (authKey == ConfigurationManager.AppSettings[name: "MasterAuthKey"])
            {
                MsManager.Instance.AuthentificatedClients.Add(item: CurrentClient.ClientId);

                var ws = MsManager.Instance.WorldServers.Find(match: s => s.Id == serverId);
                if (ws != null) ws.ConfigurationServiceClient = CurrentClient;
                return true;
            }

            return false;
        }

        public ConfigurationObject GetConfigurationObject()
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return null;
            return MsManager.Instance.ConfigurationObject;
        }

        public void UpdateConfigurationObject(ConfigurationObject configurationObject)
        {
            if (!MsManager.Instance.AuthentificatedClients.Any(predicate: s => s.Equals(obj: CurrentClient.ClientId))
            ) return;
            MsManager.Instance.ConfigurationObject = configurationObject;

            foreach (var ws in MsManager.Instance.WorldServers)
                ws.ConfigurationServiceClient.GetClientProxy<IConfigurationClient>()
                    .ConfigurationUpdated(configurationObject: MsManager.Instance.ConfigurationObject);
        }
    }
}