﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.Log.Networking;
using OpenNos.Log.Shared;

namespace OpenNos.Log.Client
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region Instantiation

        public MainWindow(AccountDto accountDTO)
        {
            account = accountDTO;
            InitializeComponent();
            var logs = LogServiceClient.Instance.GetChatLogEntries(sender: null, senderid: null, receiver: null,
                receiverid: null, message: null,
                start: DateTime.UtcNow.AddMinutes(value: -15), end: null, logType: null);
            resultlistbox.ItemsSource = logs;
            typedropdown.Items.Add(newItem: "All");
            typedropdown.Items.Add(newItem: LogType.Map);
            typedropdown.Items.Add(newItem: LogType.Speaker);
            typedropdown.Items.Add(newItem: LogType.Whisper);
            typedropdown.Items.Add(newItem: LogType.Group);
            typedropdown.Items.Add(newItem: LogType.Family);
            typedropdown.Items.Add(newItem: LogType.BuddyTalk);
            typedropdown.Items.Add(newItem: LogType.UserCommand);
            typedropdown.Items.Add(newItem: LogType.ModeratorCommand);
            if (account.Authority >= AuthorityType.Gm)
            {
                typedropdown.Items.Add(newItem: LogType.Trade);
                typedropdown.Items.Add(newItem: LogType.BazaarSell);
                typedropdown.Items.Add(newItem: LogType.PrivateShopSell);
                typedropdown.Items.Add(newItem: LogType.FamilyStorage);
                typedropdown.Items.Add(newItem: LogType.Drop);
                typedropdown.Items.Add(newItem: LogType.ItemCreate);
            }

            if (account.Authority >= AuthorityType.Gm) typedropdown.Items.Add(newItem: LogType.GMCommand);
            if (account.Authority >= AuthorityType.Gm)
            {
                typedropdown.Items.Add(newItem: LogType.MallItemBuy);
                typedropdown.Items.Add(newItem: LogType.Packet);
            }

            typedropdown.SelectedIndex = 0;

            var rmbMenu = new ContextMenu();

            var copySender = new MenuItem
            {
                Header = "Copy Sender"
            };
            copySender.Click += CopySenderOnClick;
            rmbMenu.Items.Add(newItem: copySender);

            var copySenderId = new MenuItem
            {
                Header = "Copy SenderId"
            };
            copySenderId.Click += CopySenderIdOnClick;
            rmbMenu.Items.Add(newItem: copySenderId);

            var copyReceiver = new MenuItem
            {
                Header = "Copy Receiver"
            };
            copyReceiver.Click += CopyReceiverOnClick;
            rmbMenu.Items.Add(newItem: copyReceiver);

            var copyReceiverId = new MenuItem
            {
                Header = "Copy ReceiverId"
            };
            copyReceiverId.Click += CopyReceiverIdOnClick;
            rmbMenu.Items.Add(newItem: copyReceiverId);

            var copyMessage = new MenuItem
            {
                Header = "Copy Message"
            };
            copyMessage.Click += CopyMessageOnClick;
            rmbMenu.Items.Add(newItem: copyMessage);

            var copyLogEntry = new MenuItem
            {
                Header = "Copy LogEntry"
            };
            copyLogEntry.Click += CopyLogEntryOnClick;
            rmbMenu.Items.Add(newItem: copyLogEntry);

            var searchBidirectionally = new MenuItem
            {
                Header = "Search Bidirectionally"
            };
            searchBidirectionally.Click += SearchBidirectionallyOnClick;
            rmbMenu.Items.Add(newItem: searchBidirectionally);

            resultlistbox.ContextMenu = rmbMenu;
        }

        #endregion

        #region Members

        bool _orderDesc;

        public AccountDto account;

        #endregion

        #region Methods

        void CloseFile(object sender, RoutedEventArgs e)
        {
        }

        void CopyLogEntryOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (resultlistbox.SelectedItem is LogEntry entry)
                Clipboard.SetText(text: entry.ToString());
            else if (resultlistbox.SelectedItem is PacketLogEntry entry2) Clipboard.SetText(text: entry2.ToString());
        }

        void CopyMessageOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (resultlistbox.SelectedItem is LogEntry entry)
                Clipboard.SetText(text: entry.Message);
            else if (resultlistbox.SelectedItem is PacketLogEntry entry2) Clipboard.SetText(text: entry2.Packet);
        }

        void CopyReceiverIdOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (resultlistbox.SelectedItem is LogEntry entry) Clipboard.SetText(text: entry.ReceiverId.ToString());
        }

        void CopyReceiverOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (resultlistbox.SelectedItem is LogEntry entry) Clipboard.SetText(text: entry.Receiver);
        }

        void CopySenderIdOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (resultlistbox.SelectedItem is LogEntry entry)
                Clipboard.SetText(text: entry.SenderId.ToString());
            else if (resultlistbox.SelectedItem is PacketLogEntry entry2)
                Clipboard.SetText(text: entry2.SenderId.ToString());
        }

        void CopySenderOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (resultlistbox.SelectedItem is LogEntry entry)
                Clipboard.SetText(text: entry.Sender);
            else if (resultlistbox.SelectedItem is PacketLogEntry entry2) Clipboard.SetText(text: entry2.Sender);
        }

        void OpenFile(object sender, RoutedEventArgs e)
        {
        }

        void SearchBidirectionallyOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (resultlistbox.SelectedItem is LogEntry entry)
            {
                if (entry.MessageType == LogType.Whisper || entry.MessageType == LogType.BuddyTalk)
                {
                    var tmp = LogServiceClient.Instance.GetChatLogEntries(
                            sender: new[] {entry.Sender}, senderid: null, receiver: new[] {entry.Receiver},
                            receiverid: null, message: null, start: null, end: null,
                            logType: entry.MessageType)
                        .Concat(second: LogServiceClient.Instance.GetChatLogEntries(
                            sender: new[] {entry.Receiver}, senderid: null, receiver: new[] {entry.Sender},
                            receiverid: null, message: null, start: null, end: null,
                            logType: entry.MessageType));
                    if (_orderDesc)
                        resultlistbox.ItemsSource = tmp.OrderByDescending(keySelector: s => s.Timestamp);
                    else
                        resultlistbox.ItemsSource = tmp.OrderBy(keySelector: s => s.Timestamp);
                }
                else
                {
                    MessageBox.Show(
                        messageBoxText: "You can ony search Bidirectionally for Whisper and BuddyTalk messages",
                        caption: "",
                        button: MessageBoxButton.OK, icon: MessageBoxImage.Information);
                }
            }
        }

        void SearchButton(object sender, RoutedEventArgs e)
        {
            string[] _sender = null;
            string[] _senderid = null;
            string[] _receiver = null;
            string[] _receiverid = null;
            string[] _textfilter = null;
            DateTime? _start = null;
            DateTime? _end = null;
            LogType? _logType = null;

            if (!string.IsNullOrWhiteSpace(value: senderbox.Text)) _sender = senderbox.Text.Split(',');
            if (!string.IsNullOrWhiteSpace(value: senderidbox.Text)
            ) // && long.TryParse(senderidbox.Text, out long senderid))
                _senderid = senderidbox.Text.Split(',');
            if (!string.IsNullOrWhiteSpace(value: receiverbox.Text)) _receiver = receiverbox.Text.Split(',');
            if (!string.IsNullOrWhiteSpace(value: receiveridbox.Text)
            ) // && long.TryParse(receiveridbox.Text, out long receiverid))
                _receiverid = receiveridbox.Text.Split(',');
            if (!string.IsNullOrWhiteSpace(value: messagebox.Text)) _textfilter = messagebox.Text.Split(',');
            _start = datestartpicker.Value;
            _end = dateendpicker.Value;
            if (typedropdown.SelectedIndex != 0) _logType = (LogType) typedropdown.SelectedValue;

            if (_logType.HasValue && _logType > LogType.Family)
            {
                IEnumerable<PacketLogEntry> tmp = LogServiceClient.Instance.GetPacketLogEntries(sender: _sender,
                    senderid: _senderid,
                    receiver: _receiver, receiverid: _receiverid, message: _textfilter, start: _start, end: _end,
                    logType: _logType, authority: account.Authority);
                if (_orderDesc)
                    resultlistbox.ItemsSource = tmp.OrderByDescending(keySelector: s => s.Timestamp);
                else
                    resultlistbox.ItemsSource = tmp.OrderBy(keySelector: s => s.Timestamp);
            }
            else
            {
                IEnumerable<LogEntry> tmp = LogServiceClient.Instance.GetChatLogEntries(sender: _sender,
                    senderid: _senderid, receiver: _receiver,
                    receiverid: _receiverid, message: _textfilter, start: _start, end: _end, logType: _logType);
                if (_orderDesc)
                    resultlistbox.ItemsSource = tmp.OrderByDescending(keySelector: s => s.Timestamp);
                else
                    resultlistbox.ItemsSource = tmp.OrderBy(keySelector: s => s.Timestamp);
            }
        }

        void SettingsOrderAscending(object sender, RoutedEventArgs e)
        {
            _orderDesc = false;
            Orderdesc.IsChecked = true;
            Orderasc.IsChecked = false;
        }

        void SettingsOrderDescending(object sender, RoutedEventArgs e)
        {
            _orderDesc = true;
            Orderdesc.IsChecked = true;
            Orderasc.IsChecked = false;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e: e);

            Application.Current.Shutdown();
        }

        #endregion
    }
}