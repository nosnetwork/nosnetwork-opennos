﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using OpenNos.Log.Networking;

namespace OpenNos.Log.Client
{
    /// <summary>
    ///     Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        void loginButton_Click(object sender, RoutedEventArgs e) //<---- Cleanup is not possible
        {
            string Sha512(string inputString)
            {
                using (var hash = SHA512.Create())
                {
                    return string.Concat(values: hash.ComputeHash(buffer: Encoding.UTF8.GetBytes(s: inputString))
                        .Select(selector: item => item.ToString(format: "x2")));
                }
            }

            if (LogServiceClient.Instance.AuthenticateAdmin(user: AccBox.Text,
                passHash: Sha512(inputString: PassBox.Password)))
            {
                var accountDTO = LogServiceClient.Instance.GetAccount(user: AccBox.Text,
                    passHash: Sha512(inputString: PassBox.Password));
                Hide();
                var mw = new MainWindow(accountDTO: accountDTO);
                mw.Show();
            }
            else
            {
                MessageBox.Show(messageBoxText: "Credentials invalid or not permitted to use the Service.",
                    caption: "Login failed.",
                    button: MessageBoxButton.OK, icon: MessageBoxImage.Error);
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e: e);

            Application.Current.Shutdown();
        }
    }
}