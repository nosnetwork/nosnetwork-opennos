﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace OpenNos.Log.Client
{
    public class TextInputToVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
            CultureInfo culture)
        {
            // Always test MultiValueConverter inputs for non-null
            // (to avoid crash bugs for views in the designer)
            if (values[0] is string && values[1] is bool)
            {
                var hasText = ((string) values[0]).Length > 0;
                var hasFocus = (bool) values[1];

                if (hasFocus || hasText) return Visibility.Collapsed;
            }

            return Visibility.Visible;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}