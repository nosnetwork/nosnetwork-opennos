using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class RecipeListMapper
    {
        #region Methods

        public static bool ToRecipeList(RecipeListDto input, RecipeList output)
        {
            if (input == null) return false;

            output.ItemVNum = input.ItemVNum;
            output.MapNpcId = input.MapNpcId;
            output.RecipeId = input.RecipeId;
            output.RecipeListId = input.RecipeListId;

            return true;
        }

        public static bool ToRecipeListDto(RecipeList input, RecipeListDto output)
        {
            if (input == null) return false;

            output.ItemVNum = input.ItemVNum;
            output.MapNpcId = input.MapNpcId;
            output.RecipeId = input.RecipeId;
            output.RecipeListId = input.RecipeListId;

            return true;
        }

        #endregion
    }
}