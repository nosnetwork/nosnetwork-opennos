﻿using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class CharacterQuestMapper
    {
        #region Methods

        public static bool ToCharacterQuest(CharacterQuestDto input, CharacterQuest output)
        {
            if (input == null) return false;

            output.Id = input.Id;
            output.CharacterId = input.CharacterId;
            output.QuestId = input.QuestId;
            output.FirstObjective = input.FirstObjective;
            output.SecondObjective = input.SecondObjective;
            output.ThirdObjective = input.ThirdObjective;
            output.FourthObjective = input.FourthObjective;
            output.FifthObjective = input.FifthObjective;
            output.IsMainQuest = input.IsMainQuest;

            return true;
        }

        public static bool ToCharacterQuestDto(CharacterQuest input, CharacterQuestDto output)
        {
            if (input == null) return false;

            output.Id = input.Id;
            output.CharacterId = input.CharacterId;
            output.QuestId = input.QuestId;
            output.FirstObjective = input.FirstObjective;
            output.SecondObjective = input.SecondObjective;
            output.ThirdObjective = input.ThirdObjective;
            output.FourthObjective = input.FourthObjective;
            output.FifthObjective = input.FifthObjective;
            output.IsMainQuest = input.IsMainQuest;

            return true;
        }

        #endregion
    }
}