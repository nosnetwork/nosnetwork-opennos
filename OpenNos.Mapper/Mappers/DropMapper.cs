using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class DropMapper
    {
        #region Methods

        public static bool ToDrop(DropDto input, Drop output)
        {
            if (input == null) return false;

            output.Amount = input.Amount;
            output.DropChance = input.DropChance;
            output.DropId = input.DropId;
            output.ItemVNum = input.ItemVNum;
            output.MapTypeId = input.MapTypeId;
            output.MonsterVNum = input.MonsterVNum;

            return true;
        }

        public static bool ToDropDto(Drop input, DropDto output)
        {
            if (input == null) return false;

            output.Amount = input.Amount;
            output.DropChance = input.DropChance;
            output.DropId = input.DropId;
            output.ItemVNum = input.ItemVNum;
            output.MapTypeId = input.MapTypeId;
            output.MonsterVNum = input.MonsterVNum;

            return true;
        }

        #endregion
    }
}