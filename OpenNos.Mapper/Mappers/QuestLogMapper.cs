﻿using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class QuestLogMapper
    {
        #region Methods

        public static bool ToQuestLog(QuestLogDto input, QuestLog output)
        {
            if (input == null) return false;

            output.CharacterId = input.CharacterId;
            output.QuestId = input.QuestId;
            output.IpAddress = input.IpAddress;
            output.LastDaily = input.LastDaily;

            return true;
        }

        public static bool ToQuestLogDto(QuestLog input, QuestLogDto output)
        {
            if (input == null) return false;

            output.CharacterId = input.CharacterId;
            output.QuestId = input.QuestId;
            output.IpAddress = input.IpAddress;
            output.LastDaily = input.LastDaily;

            return true;
        }

        #endregion
    }
}