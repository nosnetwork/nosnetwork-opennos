using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class RecipeItemMapper
    {
        #region Methods

        public static bool ToRecipeItem(RecipeItemDto input, RecipeItem output)
        {
            if (input == null) return false;

            output.Amount = input.Amount;
            output.ItemVNum = input.ItemVNum;
            output.RecipeId = input.RecipeId;
            output.RecipeItemId = input.RecipeItemId;

            return true;
        }

        public static bool ToRecipeItemDto(RecipeItem input, RecipeItemDto output)
        {
            if (input == null) return false;

            output.Amount = input.Amount;
            output.ItemVNum = input.ItemVNum;
            output.RecipeId = input.RecipeId;
            output.RecipeItemId = input.RecipeItemId;

            return true;
        }

        #endregion
    }
}