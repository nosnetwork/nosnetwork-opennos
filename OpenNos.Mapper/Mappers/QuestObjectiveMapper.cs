﻿using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class QuestObjectiveMapper
    {
        #region Methods

        public static bool ToQuestObjective(QuestObjectiveDto input, QuestObjective output)
        {
            if (input == null) return false;

            output.QuestObjectiveId = input.QuestObjectiveId;
            output.QuestId = input.QuestId;
            output.Data = input.Data;
            output.Objective = input.Objective;
            output.SpecialData = input.SpecialData;
            output.DropRate = input.DropRate;
            output.ObjectiveIndex = input.ObjectiveIndex;

            return true;
        }

        public static bool ToQuestObjectiveDto(QuestObjective input, QuestObjectiveDto output)
        {
            if (input == null) return false;

            output.QuestObjectiveId = input.QuestObjectiveId;
            output.QuestId = input.QuestId;
            output.Data = input.Data;
            output.Objective = input.Objective;
            output.SpecialData = input.SpecialData;
            output.DropRate = input.DropRate;
            output.ObjectiveIndex = input.ObjectiveIndex;

            return true;
        }

        #endregion
    }
}