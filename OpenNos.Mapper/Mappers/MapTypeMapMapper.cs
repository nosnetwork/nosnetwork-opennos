using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class MapTypeMapMapper
    {
        #region Methods

        public static bool ToMapTypeMap(MapTypeMapDto input, MapTypeMap output)
        {
            if (input == null) return false;

            output.MapId = input.MapId;
            output.MapTypeId = input.MapTypeId;

            return true;
        }

        public static bool ToMapTypeMapDto(MapTypeMap input, MapTypeMapDto output)
        {
            if (input == null) return false;

            output.MapId = input.MapId;
            output.MapTypeId = input.MapTypeId;

            return true;
        }

        #endregion
    }
}