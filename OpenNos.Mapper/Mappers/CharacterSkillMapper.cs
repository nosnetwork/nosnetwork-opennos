using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class CharacterSkillMapper
    {
        #region Methods

        public static bool ToCharacterSkill(CharacterSkillDto input, CharacterSkill output)
        {
            if (input == null) return false;

            output.CharacterId = input.CharacterId;
            output.Id = input.Id;
            output.SkillVNum = input.SkillVNum;
            return true;
        }

        public static bool ToCharacterSkillDto(CharacterSkill input, CharacterSkillDto output)
        {
            if (input == null) return false;

            output.CharacterId = input.CharacterId;
            output.Id = input.Id;
            output.SkillVNum = input.SkillVNum;

            return true;
        }

        #endregion
    }
}