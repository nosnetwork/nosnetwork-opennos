using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class AccountMapper
    {
        #region Methods

        public static bool ToAccount(AccountDto input, Account output)
        {
            if (input == null) return false;

            output.AccountId = input.AccountId;
            output.Authority = input.Authority;
            output.Email = input.Email;
            output.Name = input.Name;
            output.Password = input.Password;
            output.ReferrerId = input.ReferrerId;
            output.RegistrationIp = input.RegistrationIp;
            output.VerificationToken = input.VerificationToken;

            return true;
        }

        public static bool ToAccountDto(Account input, AccountDto output)
        {
            if (input == null) return false;

            output.AccountId = input.AccountId;
            output.Authority = input.Authority;
            output.Email = input.Email;
            output.Name = input.Name;
            output.Password = input.Password;
            output.ReferrerId = input.ReferrerId;
            output.RegistrationIp = input.RegistrationIp;
            output.VerificationToken = input.VerificationToken;

            return true;
        }

        #endregion
    }
}