using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class MapTypeMapper
    {
        #region Methods

        public static bool ToMapType(MapTypeDto input, MapType output)
        {
            if (input == null) return false;

            output.MapTypeId = input.MapTypeId;
            output.MapTypeName = input.MapTypeName;
            output.PotionDelay = input.PotionDelay;
            output.RespawnMapTypeId = input.RespawnMapTypeId;
            output.ReturnMapTypeId = input.ReturnMapTypeId;

            return true;
        }

        public static bool ToMapTypeDto(MapType input, MapTypeDto output)
        {
            if (input == null) return false;

            output.MapTypeId = input.MapTypeId;
            output.MapTypeName = input.MapTypeName;
            output.PotionDelay = input.PotionDelay;
            output.RespawnMapTypeId = input.RespawnMapTypeId;
            output.ReturnMapTypeId = input.ReturnMapTypeId;

            return true;
        }

        #endregion
    }
}