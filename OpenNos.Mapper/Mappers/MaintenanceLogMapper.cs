using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class MaintenanceLogMapper
    {
        #region Methods

        public static bool ToMaintenanceLog(MaintenanceLogDto input, MaintenanceLog output)
        {
            if (input == null) return false;

            output.DateEnd = input.DateEnd;
            output.DateStart = input.DateStart;
            output.LogId = input.LogId;
            output.Reason = input.Reason;

            return true;
        }

        public static bool ToMaintenanceLogDto(MaintenanceLog input, MaintenanceLogDto output)
        {
            if (input == null) return false;

            output.DateEnd = input.DateEnd;
            output.DateStart = input.DateStart;
            output.LogId = input.LogId;
            output.Reason = input.Reason;

            return true;
        }

        #endregion
    }
}