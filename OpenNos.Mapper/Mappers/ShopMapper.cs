using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class ShopMapper
    {
        #region Methods

        public static bool ToShop(ShopDto input, Shop output)
        {
            if (input == null) return false;

            output.MapNpcId = input.MapNpcId;
            output.MenuType = input.MenuType;
            output.Name = input.Name;
            output.ShopId = input.ShopId;
            output.ShopType = input.ShopType;

            return true;
        }

        public static bool ToShopDto(Shop input, ShopDto output)
        {
            if (input == null) return false;

            output.MapNpcId = input.MapNpcId;
            output.MenuType = input.MenuType;
            output.Name = input.Name;
            output.ShopId = input.ShopId;
            output.ShopType = input.ShopType;

            return true;
        }

        #endregion
    }
}