using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class ScriptedInstanceMapper
    {
        #region Methods

        public static bool ToScriptedInstance(ScriptedInstanceDto input, ScriptedInstance output)
        {
            if (input == null) return false;

            output.MapId = input.MapId;
            output.PositionX = input.PositionX;
            output.PositionY = input.PositionY;
            output.Script = input.Script;
            output.ScriptedInstanceId = input.ScriptedInstanceId;
            output.Type = input.Type;
            output.QuestTimeSpaceId = input.QuestTimeSpaceId;

            return true;
        }

        public static bool ToScriptedInstanceDto(ScriptedInstance input, ScriptedInstanceDto output)
        {
            if (input == null) return false;

            output.MapId = input.MapId;
            output.PositionX = input.PositionX;
            output.PositionY = input.PositionY;
            output.Script = input.Script;
            output.ScriptedInstanceId = input.ScriptedInstanceId;
            output.Type = input.Type;
            output.QuestTimeSpaceId = input.QuestTimeSpaceId;

            return true;
        }

        #endregion
    }
}