﻿using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class StaticBuffMapper
    {
        #region Methods

        public static bool ToStaticBuff(StaticBuffDto input, StaticBuff output)
        {
            if (input == null) return false;

            output.CardId = input.CardId;
            output.CharacterId = input.CharacterId;
            output.RemainingTime = input.RemainingTime;
            output.StaticBuffId = input.StaticBuffId;

            return true;
        }

        public static bool ToStaticBuffDTO(StaticBuff input, StaticBuffDto output)
        {
            if (input == null) return false;

            output.CardId = input.CardId;
            output.CharacterId = input.CharacterId;
            output.RemainingTime = input.RemainingTime;
            output.StaticBuffId = input.StaticBuffId;

            return true;
        }

        #endregion
    }
}