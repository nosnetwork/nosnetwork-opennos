using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class NpcMonsterSkillMapper
    {
        #region Methods

        public static bool ToNpcMonsterSkill(NpcMonsterSkillDto input, NpcMonsterSkill output)
        {
            if (input == null) return false;

            output.NpcMonsterSkillId = input.NpcMonsterSkillId;
            output.NpcMonsterVNum = input.NpcMonsterVNum;
            output.Rate = input.Rate;
            output.SkillVNum = input.SkillVNum;
            return true;
        }

        public static bool ToNpcMonsterSkillDto(NpcMonsterSkill input, NpcMonsterSkillDto output)
        {
            if (input == null) return false;

            output.NpcMonsterSkillId = input.NpcMonsterSkillId;
            output.NpcMonsterVNum = input.NpcMonsterVNum;
            output.Rate = input.Rate;
            output.SkillVNum = input.SkillVNum;

            return true;
        }

        #endregion
    }
}