﻿using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class StaticBonusMapper
    {
        #region Methods

        public static bool ToStaticBonus(StaticBonusDto input, StaticBonus output)
        {
            if (input == null) return false;

            output.CharacterId = input.CharacterId;
            output.DateEnd = input.DateEnd;
            output.StaticBonusId = input.StaticBonusId;
            output.StaticBonusType = input.StaticBonusType;

            return true;
        }

        public static bool ToStaticBonusDto(StaticBonus input, StaticBonusDto output)
        {
            if (input == null) return false;

            output.CharacterId = input.CharacterId;
            output.DateEnd = input.DateEnd;
            output.StaticBonusId = input.StaticBonusId;
            output.StaticBonusType = input.StaticBonusType;

            return true;
        }

        #endregion
    }
}