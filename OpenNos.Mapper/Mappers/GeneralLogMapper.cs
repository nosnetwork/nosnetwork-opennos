using OpenNos.DAL.EF;
using OpenNos.Data;

namespace OpenNos.Mapper.Mappers
{
    public static class GeneralLogMapper
    {
        #region Methods

        public static bool ToGeneralLog(GeneralLogDto input, GeneralLog output)
        {
            if (input == null) return false;

            output.AccountId = input.AccountId;
            output.CharacterId = input.CharacterId;
            output.IpAddress = input.IpAddress;
            output.LogData = input.LogData;
            output.LogId = input.LogId;
            output.LogType = input.LogType;
            output.Timestamp = input.Timestamp;

            return true;
        }

        public static bool ToGeneralLogDto(GeneralLog input, GeneralLogDto output)
        {
            if (input == null) return false;

            output.AccountId = input.AccountId;
            output.CharacterId = input.CharacterId;
            output.IpAddress = input.IpAddress;
            output.LogData = input.LogData;
            output.LogId = input.LogId;
            output.LogType = input.LogType;
            output.Timestamp = input.Timestamp;

            return true;
        }

        #endregion
    }
}