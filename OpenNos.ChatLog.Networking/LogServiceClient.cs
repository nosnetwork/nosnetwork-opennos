﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using Hik.Communication.Scs.Communication;
using Hik.Communication.Scs.Communication.EndPoints.Tcp;
using Hik.Communication.ScsServices.Client;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.Log.Shared;

namespace OpenNos.Log.Networking
{
    public class LogServiceClient : ILogService
    {
        #region Instantiation

        public LogServiceClient()
        {
            var ip = ConfigurationManager.AppSettings[name: "LogIP"];
            var port = Convert.ToInt32(value: ConfigurationManager.AppSettings[name: "LogPort"]);
            _client = ScsServiceClientBuilder.CreateClient<ILogService>(
                endpoint: new ScsTcpEndPoint(ipAddress: ip, port: port));
            Thread.Sleep(millisecondsTimeout: 1000);
            while (_client.CommunicationState != CommunicationStates.Connected)
                try
                {
                    _client.Connect();
                }
                catch (Exception)
                {
                    Logger.Error(data: Language.Instance.GetMessageFromKey(key: "RETRY_CONNECTION"),
                        memberName: nameof(LogServiceClient));
                    Thread.Sleep(millisecondsTimeout: 1000);
                }
        }

        #endregion

        #region Members

        static LogServiceClient _instance;

        readonly IScsServiceClient<ILogService> _client;

        #endregion

        #region Properties

        public static LogServiceClient Instance
        {
            get => _instance ?? (_instance = new LogServiceClient());
        }

        public CommunicationStates CommunicationState
        {
            get => _client.CommunicationState;
        }

        #endregion

        #region Methods

        public void LogChatMessage(LogEntry logEntry)
        {
            _client.ServiceProxy.LogChatMessage(logEntry: logEntry);
        }

        public void LogPacket(PacketLogEntry logEntry)
        {
            _client.ServiceProxy.LogPacket(logEntry: logEntry);
        }

        public bool AuthenticateAdmin(string user, string passHash)
        {
            return _client.ServiceProxy.AuthenticateAdmin(user: user, passHash: passHash);
        }

        public AccountDto GetAccount(string user, string passHash)
        {
            return _client.ServiceProxy.GetAccount(user: user, passHash: passHash);
        }

        public List<LogEntry> GetChatLogEntries(string[] sender, string[] senderid, string[] receiver,
            string[] receiverid, string[] message, DateTime? start, DateTime? end, LogType? logType)
        {
            return _client.ServiceProxy.GetChatLogEntries(sender: sender, senderid: senderid, receiver: receiver,
                receiverid: receiverid, message: message, start: start, end: end,
                logType: logType);
        }

        public List<PacketLogEntry> GetPacketLogEntries(string[] sender, string[] senderid, string[] receiver,
            string[] receiverid, string[] message, DateTime? start, DateTime? end, LogType? logType,
            AuthorityType authority)
        {
            return _client.ServiceProxy.GetPacketLogEntries(sender: sender, senderid: senderid, receiver: receiver,
                receiverid: receiverid, message: message, start: start, end: end,
                logType: logType, authority: authority);
        }

        public bool Authenticate(string authKey)
        {
            return _client.ServiceProxy.Authenticate(authKey: authKey);
        }

        #endregion
    }
}