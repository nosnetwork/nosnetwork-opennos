﻿using System;
using System.Configuration;
using System.Threading;
using OpenNos.Core;
using OpenNos.Master.Library.Data;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.SCS.Communication.ScsServices.Client;

namespace OpenNos.Master.Library.Client
{
    public class ConfigurationServiceClient : IConfigurationService
    {
        #region Instantiation

        public ConfigurationServiceClient()
        {
            var ip = ConfigurationManager.AppSettings[name: "MasterIP"];
            var port = Convert.ToInt32(value: ConfigurationManager.AppSettings[name: "MasterPort"]);
            var confClient = new ConfigurationClient();
            _client = ScsServiceClientBuilder.CreateClient<IConfigurationService>(
                endpoint: new ScsTcpEndPoint(ipAddress: ip, port: port),
                clientObject: confClient);
            Thread.Sleep(millisecondsTimeout: 1000);
            while (_client.CommunicationState != CommunicationStates.Connected)
                try
                {
                    _client.Connect();
                }
                catch (Exception)
                {
                    Logger.Error(data: Language.Instance.GetMessageFromKey(key: "RETRY_CONNECTION"),
                        memberName: nameof(CommunicationServiceClient));
                    Thread.Sleep(millisecondsTimeout: 1000);
                }
        }

        #endregion

        #region Events

        public event EventHandler ConfigurationUpdate;

        #endregion

        #region Members

        static ConfigurationServiceClient _instance;

        readonly IScsServiceClient<IConfigurationService> _client;

        #endregion

        #region Properties

        public static ConfigurationServiceClient Instance
        {
            get => _instance ?? (_instance = new ConfigurationServiceClient());
        }

        public CommunicationStates CommunicationState
        {
            get => _client.CommunicationState;
        }

        #endregion

        #region Methods

        public bool Authenticate(string authKey, Guid serverId)
        {
            return _client.ServiceProxy.Authenticate(authKey: authKey, serverId: serverId);
        }

        public void UpdateConfigurationObject(ConfigurationObject configurationObject)
        {
            _client.ServiceProxy.UpdateConfigurationObject(configurationObject: configurationObject);
        }

        public ConfigurationObject GetConfigurationObject()
        {
            return _client.ServiceProxy.GetConfigurationObject();
        }

        internal void OnConfigurationUpdated(ConfigurationObject configurationObject)
        {
            ConfigurationUpdate?.Invoke(sender: configurationObject, e: null);
        }

        #endregion
    }
}