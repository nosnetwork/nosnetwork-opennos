﻿using System;
using System.Configuration;
using System.Threading;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.SCS.Communication.ScsServices.Client;

namespace OpenNos.Master.Library.Client
{
    public class MailServiceClient : IMailService
    {
        #region Instantiation

        public MailServiceClient()
        {
            var ip = ConfigurationManager.AppSettings[name: "MasterIP"];
            var port = Convert.ToInt32(value: ConfigurationManager.AppSettings[name: "MasterPort"]);
            var mailClient = new MailClient();
            _client = ScsServiceClientBuilder.CreateClient<IMailService>(
                endpoint: new ScsTcpEndPoint(ipAddress: ip, port: port), clientObject: mailClient);

            Thread.Sleep(millisecondsTimeout: 1000);
            while (_client.CommunicationState != CommunicationStates.Connected)
                try
                {
                    _client.Connect();
                }
                catch (Exception)
                {
                    Logger.Error(data: Language.Instance.GetMessageFromKey(key: "RETRY_CONNECTION"),
                        memberName: nameof(MailServiceClient));
                    Thread.Sleep(millisecondsTimeout: 1000);
                }
        }

        #endregion

        #region Events

        public event EventHandler MailSent;

        #endregion

        #region Members

        static MailServiceClient _instance;

        readonly IScsServiceClient<IMailService> _client;

        #endregion

        #region Properties

        public static MailServiceClient Instance
        {
            get => _instance ?? (_instance = new MailServiceClient());
        }

        public CommunicationStates CommunicationState
        {
            get => _client.CommunicationState;
        }

        #endregion

        #region Methods

        public bool Authenticate(string authKey, Guid serverId)
        {
            return _client.ServiceProxy.Authenticate(authKey: authKey, serverId: serverId);
        }

        public void SendMail(MailDto mail)
        {
            _client.ServiceProxy.SendMail(mail: mail);
        }

        internal void OnMailSent(MailDto mail)
        {
            MailSent?.Invoke(sender: mail, e: null);
        }

        #endregion
    }
}