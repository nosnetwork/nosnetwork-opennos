﻿using System;
using System.Configuration;
using System.Threading;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.SCS.Communication.ScsServices.Client;

namespace OpenNos.Master.Library.Client
{
    public class AuthentificationServiceClient : IAuthentificationService
    {
        #region Instantiation

        public AuthentificationServiceClient()
        {
            var ip = ConfigurationManager.AppSettings[name: "MasterIP"];
            var port = Convert.ToInt32(value: ConfigurationManager.AppSettings[name: "MasterPort"]);
            _client = ScsServiceClientBuilder.CreateClient<IAuthentificationService>(
                endpoint: new ScsTcpEndPoint(ipAddress: ip, port: port));
            Thread.Sleep(millisecondsTimeout: 1000);
            while (_client.CommunicationState != CommunicationStates.Connected)
                try
                {
                    _client.Connect();
                }
                catch (Exception)
                {
                    Logger.Error(data: Language.Instance.GetMessageFromKey(key: "RETRY_CONNECTION"),
                        memberName: nameof(AuthentificationServiceClient));
                    Thread.Sleep(millisecondsTimeout: 1000);
                }
        }

        #endregion

        #region Members

        static AuthentificationServiceClient _instance;

        readonly IScsServiceClient<IAuthentificationService> _client;

        #endregion

        #region Properties

        public static AuthentificationServiceClient Instance
        {
            get => _instance ?? (_instance = new AuthentificationServiceClient());
        }

        public CommunicationStates CommunicationState
        {
            get => _client.CommunicationState;
        }

        #endregion

        #region Methods

        public bool Authenticate(string authKey)
        {
            return _client.ServiceProxy.Authenticate(authKey: authKey);
        }

        public AccountDto ValidateAccount(string userName, string passHash)
        {
            return _client.ServiceProxy.ValidateAccount(userName: userName, passHash: passHash);
        }

        public CharacterDto ValidateAccountAndCharacter(string userName, string characterName, string passHash)
        {
            return _client.ServiceProxy.ValidateAccountAndCharacter(userName: userName, characterName: characterName,
                passHash: passHash);
        }

        #endregion
    }
}