﻿using System.Threading.Tasks;
using OpenNos.Domain;
using OpenNos.Master.Library.Data;
using OpenNos.Master.Library.Interface;

namespace OpenNos.Master.Library.Client
{
    internal class CommunicationClient : ICommunicationClient
    {
        #region Methods

        public void CharacterConnected(long characterId)
        {
            Task.Run(action: () => CommunicationServiceClient.Instance.OnCharacterConnected(characterId: characterId));
        }

        public void CharacterDisconnected(long characterId)
        {
            Task.Run(
                action: () => CommunicationServiceClient.Instance.OnCharacterDisconnected(characterId: characterId));
        }

        public void KickSession(long? accountId, int? sessionId)
        {
            Task.Run(action: () =>
                CommunicationServiceClient.Instance.OnKickSession(accountId: accountId, sessionId: sessionId));
        }

        public void Restart(int time = 5)
        {
            Task.Run(action: () => CommunicationServiceClient.Instance.OnRestart(time: time));
        }

        public void RunGlobalEvent(EventType eventType)
        {
            Task.Run(action: () => CommunicationServiceClient.Instance.OnRunGlobalEvent(eventType: eventType));
        }

        public void SendMessageToCharacter(ScsCharacterMessage message)
        {
            Task.Run(action: () => CommunicationServiceClient.Instance.OnSendMessageToCharacter(message: message));
        }

        public void Shutdown()
        {
            Task.Run(action: () => CommunicationServiceClient.Instance.OnShutdown());
        }

        public void UpdateBazaar(long bazaarItemId)
        {
            Task.Run(action: () => CommunicationServiceClient.Instance.OnUpdateBazaar(bazaarItemId: bazaarItemId));
        }

        public void UpdateFamily(long familyId, bool changeFaction)
        {
            Task.Run(action: () =>
                CommunicationServiceClient.Instance.OnUpdateFamily(familyId: familyId, changeFaction: changeFaction));
        }

        public void UpdatePenaltyLog(int penaltyLogId)
        {
            Task.Run(action: () => CommunicationServiceClient.Instance.OnUpdatePenaltyLog(penaltyLogId: penaltyLogId));
        }

        public void UpdateRelation(long relationId)
        {
            Task.Run(action: () => CommunicationServiceClient.Instance.OnUpdateRelation(relationId: relationId));
        }

        public void UpdateStaticBonus(long characterId)
        {
            Task.Run(action: () => CommunicationServiceClient.Instance.OnUpdateStaticBonus(characterId: characterId));
        }

        #endregion
    }
}