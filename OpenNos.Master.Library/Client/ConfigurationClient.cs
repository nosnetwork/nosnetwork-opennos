﻿using System.Threading.Tasks;
using OpenNos.Master.Library.Data;
using OpenNos.Master.Library.Interface;

namespace OpenNos.Master.Library.Client
{
    internal class ConfigurationClient : IConfigurationClient
    {
        public void ConfigurationUpdated(ConfigurationObject configurationObject)
        {
            Task.Run(action: () =>
                ConfigurationServiceClient.Instance.OnConfigurationUpdated(configurationObject: configurationObject));
        }
    }
}