﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using OpenNos.Core;
using OpenNos.Data;
using OpenNos.Master.Library.Data;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.SCS.Communication.ScsServices.Client;

namespace OpenNos.Master.Library.Client
{
    public class MallServiceClient : IMallService
    {
        #region Instantiation

        public MallServiceClient()
        {
            var ip = ConfigurationManager.AppSettings[name: "MasterIP"];
            var port = Convert.ToInt32(value: ConfigurationManager.AppSettings[name: "MasterPort"]);
            _client = ScsServiceClientBuilder.CreateClient<IMallService>(
                endpoint: new ScsTcpEndPoint(ipAddress: ip, port: port));
            Thread.Sleep(millisecondsTimeout: 5000);
            while (_client.CommunicationState != CommunicationStates.Connected)
                try
                {
                    _client.Connect();
                }
                catch
                {
                    Logger.Error(data: Language.Instance.GetMessageFromKey(key: "RETRY_CONNECTION"),
                        // ReSharper disable once ExplicitCallerInfoArgument
                        memberName: "MallServiceClient");
                    Thread.Sleep(millisecondsTimeout: 1000);
                }
        }

        #endregion

        #region Members

        static MallServiceClient _instance;

        readonly IScsServiceClient<IMallService> _client;

        #endregion

        #region Properties

        public static MallServiceClient Instance
        {
            get => _instance ?? (_instance = new MallServiceClient());
        }

        public CommunicationStates CommunicationState
        {
            get => _client.CommunicationState;
        }

        #endregion

        #region Methods

        public bool Authenticate(string authKey)
        {
            return _client.ServiceProxy.Authenticate(authKey: authKey);
        }

        public AccountDto ValidateAccount(string userName, string passHash)
        {
            return _client.ServiceProxy.ValidateAccount(userName: userName, passHash: passHash);
        }

        public IEnumerable<CharacterDto> GetCharacters(long accountId)
        {
            return _client.ServiceProxy.GetCharacters(accountId: accountId);
        }

        public void SendItem(long characterId, MallItem item)
        {
            _client.ServiceProxy.SendItem(characterId: characterId, item: item);
        }

        public void SendStaticBonus(long characterId, MallStaticBonus item)
        {
            _client.ServiceProxy.SendStaticBonus(characterId: characterId, item: item);
        }

        #endregion
    }
}