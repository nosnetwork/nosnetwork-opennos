﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using OpenNos.Core;
using OpenNos.DAL;
using OpenNos.Domain;
using OpenNos.Master.Library.Data;
using OpenNos.Master.Library.Interface;
using OpenNos.SCS.Communication.Scs.Communication;
using OpenNos.SCS.Communication.Scs.Communication.EndPoints.Tcp;
using OpenNos.SCS.Communication.ScsServices.Client;

namespace OpenNos.Master.Library.Client
{
    public class CommunicationServiceClient : ICommunicationService
    {
        #region Instantiation

        public CommunicationServiceClient()
        {
            var ip = ConfigurationManager.AppSettings[name: "MasterIP"];
            var port = Convert.ToInt32(value: ConfigurationManager.AppSettings[name: "MasterPort"]);
            var commClient = new CommunicationClient();
            _client = ScsServiceClientBuilder.CreateClient<ICommunicationService>(
                endpoint: new ScsTcpEndPoint(ipAddress: ip, port: port),
                clientObject: commClient);
            Thread.Sleep(millisecondsTimeout: 1000);
            while (_client.CommunicationState != CommunicationStates.Connected)
                try
                {
                    _client.Connect();
                }
                catch (Exception)
                {
                    Logger.Error(data: Language.Instance.GetMessageFromKey(key: "RETRY_CONNECTION"),
                        memberName: nameof(CommunicationServiceClient));
                    Thread.Sleep(millisecondsTimeout: 1000);
                }
        }

        #endregion

        #region Members

        static CommunicationServiceClient _instance;

        readonly IScsServiceClient<ICommunicationService> _client;

        #endregion

        #region Events

        public event EventHandler BazaarRefresh;

        public event EventHandler CharacterConnectedEvent;

        public event EventHandler CharacterDisconnectedEvent;

        public event EventHandler FamilyRefresh;

        public event EventHandler GlobalEvent;

        public event EventHandler MessageSentToCharacter;

        public event EventHandler PenaltyLogRefresh;

        public event EventHandler RelationRefresh;

        public event EventHandler SessionKickedEvent;

        public event EventHandler ShutdownEvent;

        public event EventHandler RestartEvent;

        public event EventHandler StaticBonusRefresh;

        #endregion

        #region Properties

        public static CommunicationServiceClient Instance
        {
            get => _instance ?? (_instance = new CommunicationServiceClient());
        }

        public CommunicationStates CommunicationState
        {
            get => _client.CommunicationState;
        }

        #endregion

        #region Methods

        public bool Authenticate(string authKey)
        {
            return _client.ServiceProxy.Authenticate(authKey: authKey);
        }

        public void Cleanup()
        {
            _client.ServiceProxy.Cleanup();
        }

        public void CleanupOutdatedSession()
        {
            _client.ServiceProxy.CleanupOutdatedSession();
        }

        public bool ConnectAccount(Guid worldId, long accountId, int sessionId)
        {
            return _client.ServiceProxy.ConnectAccount(worldId: worldId, accountId: accountId, sessionId: sessionId);
        }

        public bool ConnectAccountCrossServer(Guid worldId, long accountId, int sessionId)
        {
            return _client.ServiceProxy.ConnectAccountCrossServer(worldId: worldId, accountId: accountId,
                sessionId: sessionId);
        }

        public bool ConnectCharacter(Guid worldId, long characterId)
        {
            return _client.ServiceProxy.ConnectCharacter(worldId: worldId, characterId: characterId);
        }

        public void DisconnectAccount(long accountId)
        {
            _client.ServiceProxy.DisconnectAccount(accountId: accountId);
        }

        public void DisconnectCharacter(Guid worldId, long characterId)
        {
            _client.ServiceProxy.DisconnectCharacter(worldId: worldId, characterId: characterId);
        }

        public int? GetChannelIdByWorldId(Guid worldId)
        {
            return _client.ServiceProxy.GetChannelIdByWorldId(worldId: worldId);
        }

        public long[][] GetOnlineCharacters()
        {
            return _client.ServiceProxy.GetOnlineCharacters();
        }

        public bool IsAccountConnected(long accountId)
        {
            return _client.ServiceProxy.IsAccountConnected(accountId: accountId);
        }

        public bool IsAct4Online(string worldGroup)
        {
            return _client.ServiceProxy.IsAct4Online(worldGroup: worldGroup);
        }

        public bool IsCharacterConnected(string worldGroup, long characterId)
        {
            return _client.ServiceProxy.IsCharacterConnected(worldGroup: worldGroup, characterId: characterId);
        }

        public bool IsCrossServerLoginPermitted(long accountId, int sessionId)
        {
            return _client.ServiceProxy.IsCrossServerLoginPermitted(accountId: accountId, sessionId: sessionId);
        }

        public bool IsLoginPermitted(long accountId, int sessionId)
        {
            return _client.ServiceProxy.IsLoginPermitted(accountId: accountId, sessionId: sessionId);
        }

        public void KickSession(long? accountId, int? sessionId)
        {
            _client.ServiceProxy.KickSession(accountId: accountId, sessionId: sessionId);
        }

        public void PulseAccount(long accountId)
        {
            _client.ServiceProxy.PulseAccount(accountId: accountId);
        }

        public void RefreshPenalty(int penaltyId)
        {
            _client.ServiceProxy.RefreshPenalty(penaltyId: penaltyId);
        }

        public void RegisterAccountLogin(long accountId, int sessionId, string ipAddress)
        {
            _client.ServiceProxy.RegisterAccountLogin(accountId: accountId, sessionId: sessionId, ipAddress: ipAddress);
        }

        public void RegisterCrossServerAccountLogin(long accountId, int sessionId)
        {
            _client.ServiceProxy.RegisterCrossServerAccountLogin(accountId: accountId, sessionId: sessionId);
        }

        public int? RegisterWorldServer(SerializableWorldServer worldServer)
        {
            return _client.ServiceProxy.RegisterWorldServer(worldServer: worldServer);
        }

        public void Restart(string worldGroup, int time = 5)
        {
            _client.ServiceProxy.Restart(worldGroup: worldGroup, time: time);
        }

        public long[][] RetrieveOnlineCharacters(long characterId)
        {
            return _client.ServiceProxy.RetrieveOnlineCharacters(characterId: characterId);
        }

        public string RetrieveOriginWorld(long accountId)
        {
            return _client.ServiceProxy.RetrieveOriginWorld(accountId: accountId);
        }

        public string RetrieveRegisteredWorldServers(string username, int sessionId, bool ignoreUserName)
        {
            return _client.ServiceProxy.RetrieveRegisteredWorldServers(username: username, sessionId: sessionId,
                ignoreUserName: ignoreUserName);
        }

        public IEnumerable<string> RetrieveServerStatistics()
        {
            return _client.ServiceProxy.RetrieveServerStatistics();
        }

        public void RunGlobalEvent(EventType eventType)
        {
            _client.ServiceProxy.RunGlobalEvent(eventType: eventType);
        }

        public int? SendMessageToCharacter(ScsCharacterMessage message)
        {
            return _client.ServiceProxy.SendMessageToCharacter(message: message);
        }

        public void Shutdown(string worldGroup)
        {
            _client.ServiceProxy.Shutdown(worldGroup: worldGroup);
        }

        public void UnregisterWorldServer(Guid worldId)
        {
            _client.ServiceProxy.UnregisterWorldServer(worldId: worldId);
        }

        public void UpdateBazaar(string worldGroup, long bazaarItemId)
        {
            _client.ServiceProxy.UpdateBazaar(worldGroup: worldGroup, bazaarItemId: bazaarItemId);
        }

        public void UpdateFamily(string worldGroup, long familyId, bool changeFaction)
        {
            _client.ServiceProxy.UpdateFamily(worldGroup: worldGroup, familyId: familyId, changeFaction: changeFaction);
        }

        public void UpdateRelation(string worldGroup, long relationId)
        {
            _client.ServiceProxy.UpdateRelation(worldGroup: worldGroup, relationId: relationId);
        }

        internal void OnCharacterConnected(long characterId)
        {
            var characterName = DaoFactory.CharacterDao.LoadById(characterId: characterId)?.Name;
            CharacterConnectedEvent?.Invoke(sender: new Tuple<long, string>(item1: characterId, item2: characterName),
                e: null);
        }

        internal void OnCharacterDisconnected(long characterId)
        {
            var characterName = DaoFactory.CharacterDao.LoadById(characterId: characterId)?.Name;
            CharacterDisconnectedEvent?.Invoke(
                sender: new Tuple<long, string>(item1: characterId, item2: characterName), e: null);
        }

        internal void OnKickSession(long? accountId, int? sessionId)
        {
            SessionKickedEvent?.Invoke(sender: new Tuple<long?, long?>(item1: accountId, item2: sessionId), e: null);
        }

        internal void OnRunGlobalEvent(EventType eventType)
        {
            GlobalEvent?.Invoke(sender: eventType, e: null);
        }

        internal void OnSendMessageToCharacter(ScsCharacterMessage message)
        {
            MessageSentToCharacter?.Invoke(sender: message, e: null);
        }

        internal void OnShutdown()
        {
            ShutdownEvent?.Invoke(sender: null, e: null);
        }

        internal void OnRestart(int time = 5)
        {
            RestartEvent?.Invoke(sender: time, e: null);
        }

        internal void OnUpdateBazaar(long bazaarItemId)
        {
            BazaarRefresh?.Invoke(sender: bazaarItemId, e: null);
        }

        internal void OnUpdateFamily(long familyId, bool changeFaction)
        {
            FamilyRefresh?.Invoke(sender: new Tuple<long, bool>(item1: familyId, item2: changeFaction), e: null);
        }

        internal void OnUpdatePenaltyLog(int penaltyLogId)
        {
            PenaltyLogRefresh?.Invoke(sender: penaltyLogId, e: null);
        }

        internal void OnUpdateRelation(long relationId)
        {
            RelationRefresh?.Invoke(sender: relationId, e: null);
        }

        internal void OnUpdateStaticBonus(long characterId)
        {
            StaticBonusRefresh?.Invoke(sender: characterId, e: null);
        }

        #endregion
    }
}