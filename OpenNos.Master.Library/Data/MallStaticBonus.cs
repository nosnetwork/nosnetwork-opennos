﻿using System;
using OpenNos.Domain;

namespace OpenNos.Master.Library.Data
{
    [Serializable]
    public class MallStaticBonus
    {
        public StaticBonusType StaticBonus { get; set; }

        public int Seconds { get; set; }
    }
}