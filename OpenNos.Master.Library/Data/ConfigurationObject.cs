﻿using System;

namespace OpenNos.Master.Library.Data
{
    [Serializable]
    public class ConfigurationObject
    {
        public bool IsAntiCheatEnabled { get; set; }

        public string AntiCheatClientKey { get; set; }

        public string AntiCheatServerKey { get; set; }

        public string Act4Ip { get; set; }

        public int Act4Port { get; set; }

        public int SessionLimit { get; set; }

        public bool SceneOnCreate { get; set; }

        public bool WorldInformation { get; set; }

        public int RateXp { get; set; }

        public int RateHeroicXp { get; set; }

        public int RateGold { get; set; }

        public int RateGoldDrop { get; set; }

        public int RateReputation { get; set; }

        public long MaxGold { get; set; }

        public int RateDrop { get; set; }

        public byte MaxLevel { get; set; }

        public byte MaxJobLevel { get; set; }

        public byte MaxHeroLevel { get; set; }

        public byte HeroicStartLevel { get; set; }

        public byte MaxSpLevel { get; set; }

        public int RateFairyXp { get; set; }

        public long PartnerSpXp { get; set; }

        public byte MaxUpgrade { get; set; }

        public string MallBaseUrl { get; set; }

        public string MallApiKey { get; set; }

        public int QuestDropRate { get; set; }

        public bool UseLogService { get; set; }

        public bool HalloweenEvent { get; set; }

        public bool ChristmasEvent { get; set; }
    }
}