﻿using System;

namespace OpenNos.Master.Library.Data
{
    public class AccountConnection
    {
        public AccountConnection(long accountId, int sessionId, string ipAddress)
        {
            AccountId = accountId;
            SessionId = sessionId;
            IpAddress = ipAddress;
            LastPulse = DateTime.Now;
        }

        public long AccountId { get; }

        public bool CanLoginCrossServer { get; set; }

        public long CharacterId { get; set; }

        public WorldServer ConnectedWorld { get; set; }

        public string IpAddress { get; }

        public DateTime LastPulse { get; set; }

        public WorldServer OriginWorld { get; set; }

        public int SessionId { get; }
    }
}