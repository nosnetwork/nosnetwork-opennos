﻿namespace OpenNos.Domain
{
    public enum JewelOptionType : byte
    {
        MaximumAugmentationHp = 0,
        MaximumAugmentationMp = 1,
        MaximumRegenerationHp = 2,
        MaximumRegenerationMp = 3,
        MinimiseUsedMp = 4,
        MinimiseCriticalHit = 5
    }
}