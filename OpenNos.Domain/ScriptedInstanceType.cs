﻿namespace OpenNos.Domain
{
    public enum ScriptedInstanceType : byte
    {
        TimeSpace = 0,
        Raid = 1,
        QuestTimeSpace = 2
    }
}