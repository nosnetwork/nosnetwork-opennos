﻿namespace OpenNos.Domain
{
    public enum MapTypeEnum : short
    {
        // Act
        Act1 = 1,

        Act2 = 2,
        Act3 = 3,
        Act4 = 4,
        Act51 = 5,
        Act52 = 6,
        Act61 = 7,
        Act62 = 8,
        Act61A = 9,
        Act61d = 10,
        CometPlain = 11,
        Mine1 = 12,
        Mine2 = 13,
        MeadowOfMine = 14,
        SunnyPlain = 15,
        Fernon = 16,
        FernonF = 17,
        Cliff = 18,
        LandOfTheDead = 19,
        Act32 = 20,
        CleftOfDarkness = 21,
        PvpMap = 22,
        Citadel = 23
    }
}