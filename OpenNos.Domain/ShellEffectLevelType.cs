﻿namespace OpenNos.Domain
{
    public enum ShellEffectLevelType : byte
    {
        CNormal = 1,
        BNormal = 2,
        ANormal = 3,
        SNormal = 4,
        CBonus = 5,
        BBonus = 6,
        ABonus = 7,
        SBonus = 8,
        Cpvp = 9,
        Bpvp = 10,
        Apvp = 11,
        Spvp = 12
    }
}