﻿namespace OpenNos.Domain
{
    public enum RarifyProtection
    {
        None,
        BlueAmulet,
        RedAmulet,
        Scroll,
        HeroicAmulet,
        RandomHeroicAmulet
    }
}