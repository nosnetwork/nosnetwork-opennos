﻿namespace OpenNos.Domain
{
    public enum MapInstanceType
    {
        BaseMapInstance,
        NormalInstance,
        LodInstance,
        TimeSpaceInstance,
        RaidInstance,
        FamilyRaidInstance,
        Act4ShipAngel,
        Act4ShipDemon,
        Act4Morcos,
        Act4Hatus,
        Act4Calvina,
        Act4Berios,
        EventGameInstance,
        CaligorInstance,
        IceBreakerInstance,
        TalentArenaMapInstance
    }
}