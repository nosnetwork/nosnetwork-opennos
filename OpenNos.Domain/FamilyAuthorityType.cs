﻿namespace OpenNos.Domain
{
    public enum FamilyAuthorityType : byte
    {
        None = 0,
        Put = 1,
        All = 2
    }
}