﻿namespace OpenNos.Domain
{
    public enum CellonOptionType : byte
    {
        HpMax = 0,
        MpMax = 1,
        HpRestore = 2,
        MpRestore = 3,
        MpUsage = 4,
        CritReduce = 5
    }
}