﻿namespace OpenNos.Domain
{
    public enum TargetHitType : byte
    {
        SingleTargetHit = 0,
        SingleTargetHitCombo = 1,
        SingleAoeTargetHit = 2,
        AoeTargetHit = 3,
        ZoneHit = 4,
        SpecialZoneHit = 5
    }
}