﻿namespace OpenNos.Domain
{
    public enum AuthorityType : short
    {
        Closed = -3,
        Banned = -2,
        Unconfirmed = -1,
        User = 0,
        Gs = 1,
        Mod = 2,
        Gm = 3,
        Em = 4,
        Administrator = 5
    }
}