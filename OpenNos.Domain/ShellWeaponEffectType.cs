﻿namespace OpenNos.Domain
{
    public enum ShellWeaponEffectType : byte
    {
        DamageImproved = 1,
        PercentageTotalDamage = 2,
        MinorBleeding = 3,
        Bleeding = 4,
        HeavyBleeding = 5,
        Blackout = 6,
        Freeze = 7,
        DeadlyBlackout = 8,
        DamageIncreasedtothePlant = 9, //Not implemented
        DamageIncreasedtotheAnimal = 10, //Not implemented
        DamageIncreasedtotheEnemy = 11, //Not implemented
        DamageIncreasedtotheUnDead = 12, //Not implemented
        DamageincreasedtotheSmallMonster = 13, //Not implemented
        DamageincreasedtotheBigMonster = 14, //Not implemented
        CriticalChance = 15, //Except Sticks
        CriticalDamage = 16, //Except Sticks
        AntiMagicDisorder = 17, //Only Sticks  //Not implemented
        IncreasedFireProperties = 18,
        IncreasedWaterProperties = 19,
        IncreasedLightProperties = 20,
        IncreasedDarkProperties = 21,
        IncreasedElementalProperties = 22,
        ReducedMpConsume = 23, //Not implemented
        HpRecoveryForKilling = 24, //Not implemented
        MpRecoveryForKilling = 25, //Not implemented
        SlDamage = 26,
        SlDefence = 27,
        SlElement = 28,
        Slhp = 29,
        SlGlobal = 30,
        GainMoreGold = 31,
        GainMoreXp = 32, //Not implemented
        GainMoreCxp = 33, //Not implemented
        PercentageDamageInPvp = 34,
        ReducesPercentageEnemyDefenceInPvp = 35,
        ReducesEnemyFireResistanceInPvp = 36,
        ReducesEnemyWaterResistanceInPvp = 37,
        ReducesEnemyLightResistanceInPvp = 38,
        ReducesEnemyDarkResistanceInPvp = 39,
        ReducesEnemyAllResistancesInPvp = 40,
        NeverMissInPvp = 41, //Not implemented
        PvpDamageAt15Percent = 42, //Not implemented
        ReducesEnemyMpInPvp = 43, //Not implemented
        InspireFireResistanceWithPercentage = 44, //Not implemented
        InspireWaterResistanceWithPercentage = 45, //Not implemented
        InspireLightResistanceWithPercentage = 46, //Not implemented
        InspireDarkResistanceWithPercentage = 47, //Not implemented
        GainSpForKilling = 48, //Not implemented
        IncreasedPrecision = 49, //Not implemented
        IncreasedFocus = 50 //Not implemented
    }
}