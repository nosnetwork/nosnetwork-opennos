﻿namespace OpenNos.Domain
{
    public enum QuestGiverType : byte
    {
        InitialQuest = 0,
        Npc = 1,
        NpcDialog = 2,
        ItemLoot = 3,
        ItemUse = 4
    }
}