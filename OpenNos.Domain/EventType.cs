﻿namespace OpenNos.Domain
{
    public enum EventType
    {
        Instantbattle,
        Lod,
        Minilandrefreshevent,
        Rankingrefresh,
        Act4Ship,
        Act4Raid,
        Meteoritegame,
        Talentarena,
        Caligor,
        Icebreaker
    }
}