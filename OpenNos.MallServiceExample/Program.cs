﻿using System;
using System.Configuration;
using System.Linq;
using OpenNos.Domain;
using OpenNos.Master.Library.Client;
using OpenNos.Master.Library.Data;

namespace OpenNos.MallServiceExample
{
    public class Program
    {
        protected static void Main(string[] args)
        {
            MallServiceClient.Instance.Authenticate(authKey: ConfigurationManager.AppSettings[name: "MasterAuthKey"]);

            Console.WriteLine(value: "UserName:");
            var user = "";
            var userArgIndex = Array.FindIndex(array: args, match: s => s == "--user");
            if (userArgIndex != -1
                && args.Length >= userArgIndex + 1
                && args[userArgIndex + 1] is string User)
                user = User;
            Console.WriteLine(value: "Password:");
            var pass = "";
            var passArgIndex = Array.FindIndex(array: args, match: s => s == "--pass");
            if (passArgIndex != -1
                && args.Length >= passArgIndex + 1
                && args[passArgIndex + 1] is string Pass)
                pass = Pass;

            //pass = Sha512(pass);
            var account = MallServiceClient.Instance.ValidateAccount(userName: user, passHash: pass);
            if (account != null && account.Authority > AuthorityType.Unconfirmed)
            {
                var characters = MallServiceClient.Instance.GetCharacters(accountId: account.AccountId);

                foreach (var character in characters)
                    Console.WriteLine(
                        value:
                        $"ID: {character.CharacterId} Name: {character.Name} Level: {character.Level} Class: {character.Class}");

                Console.WriteLine(value: "CharacterID:");
                long charId = 0;
                var charIdArgIndex = Array.FindIndex(array: args, match: s => s == "--charid");
                if (charIdArgIndex != -1
                    && args.Length >= charIdArgIndex + 1
                    && long.Parse(s: args[charIdArgIndex + 1]) is long CharId)
                    charId = CharId;

                if (characters.Any(predicate: s => s.CharacterId == charId))
                {
                    Console.WriteLine(value: "ItemVNum:");
                    short vnum = 0;
                    var vnumArgIndex = Array.FindIndex(array: args, match: s => s == "--vnum");
                    if (vnumArgIndex != -1
                        && args.Length >= vnumArgIndex + 1
                        && short.Parse(s: args[vnumArgIndex + 1]) is short Vnum)
                        vnum = Vnum;
                    Console.WriteLine(value: "Amount:");
                    short amount = 0;
                    var amountArgIndex = Array.FindIndex(array: args, match: s => s == "--amount");
                    if (amountArgIndex != -1
                        && args.Length >= amountArgIndex + 1
                        && short.Parse(s: args[amountArgIndex + 1]) is short Amount)
                        amount = Amount;
                    Console.WriteLine(value: "Rare:");
                    byte rare = 0;
                    var rareArgIndex = Array.FindIndex(array: args, match: s => s == "--rare");
                    if (rareArgIndex != -1
                        && args.Length >= rareArgIndex + 1
                        && byte.Parse(s: args[rareArgIndex + 1]) is byte Rare)
                        rare = Rare;
                    Console.WriteLine(value: "Upgrade:");
                    byte upgrade = 0;
                    var upgradeArgIndex = Array.FindIndex(array: args, match: s => s == "--upgrade");
                    if (upgradeArgIndex != -1
                        && args.Length >= upgradeArgIndex + 1
                        && byte.Parse(s: args[upgradeArgIndex + 1]) is byte Upgrade)
                        upgrade = Upgrade;
                    Console.WriteLine(value: "Upgrade:");
                    short design = 0;
                    var designArgIndex = Array.FindIndex(array: args, match: s => s == "--design");
                    if (designArgIndex != -1
                        && args.Length >= designArgIndex + 1
                        && byte.Parse(s: args[designArgIndex + 1]) is byte Design)
                        design = Design;
                    Console.WriteLine(value: "Count:");
                    byte count = 0;
                    var countArgIndex = Array.FindIndex(array: args, match: s => s == "--count");
                    if (countArgIndex != -1
                        && args.Length >= countArgIndex + 1
                        && byte.Parse(s: args[countArgIndex + 1]) is byte Count)
                        count = Count;
                    while (count != 0)
                    {
                        MallServiceClient.Instance.SendItem(characterId: charId, item: new MallItem
                        {
                            ItemVNum = vnum,
                            Amount = amount,
                            Rare = rare,
                            Upgrade = upgrade,
                            Design = design
                        });
                        count--;
                    }
                }
            }
        }
    }
}