using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OpenNos.DAL.EF
{
    public sealed class Shop
    {
        #region Instantiation

        public Shop()
        {
            ShopItem = new HashSet<ShopItem>();
            ShopSkill = new HashSet<ShopSkill>();
        }

        #endregion

        #region Properties

        public MapNpc MapNpc { get; set; }

        public int MapNpcId { get; set; }

        public byte MenuType { get; set; }

        [MaxLength(length: 255)] public string Name { get; set; }

        public int ShopId { get; set; }

        public ICollection<ShopItem> ShopItem { get; set; }

        public ICollection<ShopSkill> ShopSkill { get; set; }

        public byte ShopType { get; set; }

        #endregion
    }
}