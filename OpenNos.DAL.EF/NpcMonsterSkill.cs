using System.ComponentModel.DataAnnotations;

namespace OpenNos.DAL.EF
{
    public sealed class NpcMonsterSkill
    {
        #region Properties

        public NpcMonster NpcMonster { get; set; }

        [Key] public long NpcMonsterSkillId { get; set; }

        public short NpcMonsterVNum { get; set; }

        public short Rate { get; set; }

        public Skill Skill { get; set; }

        public short SkillVNum { get; set; }

        #endregion
    }
}