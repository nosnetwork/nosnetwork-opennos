namespace OpenNos.DAL.EF
{
    public sealed class ShopSkill
    {
        #region Properties

        public Shop Shop { get; set; }

        public int ShopId { get; set; }

        public int ShopSkillId { get; set; }

        public Skill Skill { get; set; }

        public short SkillVNum { get; set; }

        public byte Slot { get; set; }

        public byte Type { get; set; }

        #endregion
    }
}