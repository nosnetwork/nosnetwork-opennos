﻿namespace OpenNos.DAL.EF
{
    public sealed class StaticBuff
    {
        #region Properties

        public Card Card { get; set; }

        public short CardId { get; set; }

        public Character Character { get; set; }

        public long CharacterId { get; set; }

        public int RemainingTime { get; set; }

        public long StaticBuffId { get; set; }

        #endregion
    }
}