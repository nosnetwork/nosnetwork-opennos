using System;
using System.ComponentModel.DataAnnotations;

namespace OpenNos.DAL.EF
{
    public sealed class GeneralLog
    {
        #region Properties

        public Account Account { get; set; }

        public long? AccountId { get; set; }

        public Character Character { get; set; }

        public long? CharacterId { get; set; }

        [MaxLength(length: 255)] public string IpAddress { get; set; }

        [MaxLength(length: 255)] public string LogData { get; set; }

        [Key] public long LogId { get; set; }

        public string LogType { get; set; }

        public DateTime Timestamp { get; set; }

        #endregion
    }
}