namespace OpenNos.DAL.EF
{
    public sealed class Combo
    {
        #region Properties

        public short Animation { get; set; }

        public int ComboId { get; set; }

        public short Effect { get; set; }

        public short Hit { get; set; }

        public Skill Skill { get; set; }

        public short SkillVNum { get; set; }

        #endregion
    }
}