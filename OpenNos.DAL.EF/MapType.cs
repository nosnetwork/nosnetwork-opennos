﻿using System.Collections.Generic;

namespace OpenNos.DAL.EF
{
    public sealed class MapType
    {
        #region Instantiation

        public MapType()
        {
            MapTypeMap = new HashSet<MapTypeMap>();
            Drops = new HashSet<Drop>();
        }

        #endregion

        #region Properties

        public ICollection<Drop> Drops { get; set; }

        public short MapTypeId { get; set; }

        public ICollection<MapTypeMap> MapTypeMap { get; set; }

        public string MapTypeName { get; set; }

        public short PotionDelay { get; set; }

        public RespawnMapType RespawnMapType { get; set; }

        public long? RespawnMapTypeId { get; set; }

        public RespawnMapType ReturnMapType { get; set; }

        public long? ReturnMapTypeId { get; set; }

        #endregion
    }
}