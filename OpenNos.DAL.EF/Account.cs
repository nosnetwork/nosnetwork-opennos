using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OpenNos.Domain;

namespace OpenNos.DAL.EF
{
    public sealed class Account
    {
        #region Instantiation

        public Account()
        {
            Character = new HashSet<Character>();
            GeneralLog = new HashSet<GeneralLog>();
            PenaltyLog = new HashSet<PenaltyLog>();
        }

        #endregion

        #region Properties

        public long AccountId { get; set; }

        public AuthorityType Authority { get; set; }

        public ICollection<Character> Character { get; set; }

        [MaxLength(length: 255)] public string Email { get; set; }

        public ICollection<GeneralLog> GeneralLog { get; set; }

        [MaxLength(length: 255)] public string Name { get; set; }

        [MaxLength(length: 255)] public string Password { get; set; }

        public ICollection<PenaltyLog> PenaltyLog { get; set; }

        public long ReferrerId { get; set; }

        [MaxLength(length: 45)] public string RegistrationIp { get; set; }

        [MaxLength(length: 32)] public string VerificationToken { get; set; }

        public bool DailyRewardSent { get; set; }

        #endregion
    }
}