﻿using System;
using System.ComponentModel.DataAnnotations;
using OpenNos.Domain;

namespace OpenNos.DAL.EF
{
    public sealed class PenaltyLog
    {
        #region Properties

        public Account Account { get; set; }

        public long AccountId { get; set; }

        public string Ip { get; set; }

        public string AdminName { get; set; }

        public DateTime DateEnd { get; set; }

        public DateTime DateStart { get; set; }

        public PenaltyType Penalty { get; set; }

        [Key] public int PenaltyLogId { get; set; }

        [MaxLength(length: 255)] public string Reason { get; set; }

        #endregion
    }
}