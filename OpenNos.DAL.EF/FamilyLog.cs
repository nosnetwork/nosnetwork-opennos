﻿using System;
using System.ComponentModel.DataAnnotations;
using OpenNos.Domain;

namespace OpenNos.DAL.EF
{
    public sealed class FamilyLog
    {
        #region Properties

        public Family Family { get; set; }

        public long FamilyId { get; set; }

        [MaxLength(length: 255)] public string FamilyLogData { get; set; }

        public long FamilyLogId { get; set; }

        public FamilyLogType FamilyLogType { get; set; }

        public DateTime Timestamp { get; set; }

        #endregion
    }
}