using OpenNos.DAL.EF.Base;

namespace OpenNos.DAL.EF
{
    public sealed class CharacterSkill : SynchronizableBaseEntity
    {
        #region Properties

        public Character Character { get; set; }

        public long CharacterId { get; set; }

        public Skill Skill { get; set; }

        public short SkillVNum { get; set; }

        #endregion
    }
}