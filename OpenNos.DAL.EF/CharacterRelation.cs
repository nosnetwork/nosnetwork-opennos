﻿using OpenNos.Domain;

namespace OpenNos.DAL.EF
{
    public sealed class CharacterRelation
    {
        #region Properties

        public Character Character1 { get; set; }

        public Character Character2 { get; set; }

        public long CharacterId { get; set; }

        public long CharacterRelationId { get; set; }

        public long RelatedCharacterId { get; set; }

        public CharacterRelationType RelationType { get; set; }

        #endregion
    }
}