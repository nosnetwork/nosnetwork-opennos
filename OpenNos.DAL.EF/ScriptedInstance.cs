﻿using System.ComponentModel.DataAnnotations;
using OpenNos.Domain;

namespace OpenNos.DAL.EF
{
    public sealed class ScriptedInstance
    {
        #region Properties

        public Map Map { get; set; }

        public short MapId { get; set; }

        public short PositionX { get; set; }

        public short PositionY { get; set; }

        [MaxLength(length: int.MaxValue)] public string Script { get; set; }

        public short ScriptedInstanceId { get; set; }

        public ScriptedInstanceType Type { get; set; }

        public string Label { get; set; }

        public int QuestTimeSpaceId { get; set; }

        #endregion
    }
}