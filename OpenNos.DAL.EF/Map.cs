using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OpenNos.DAL.EF
{
    public sealed class Map
    {
        #region Instantiation

        public Map()
        {
            Character = new HashSet<Character>();
            MapMonster = new HashSet<MapMonster>();
            MapNpc = new HashSet<MapNpc>();
            Portal = new HashSet<Portal>();
            Portal1 = new HashSet<Portal>();
            ScriptedInstance = new HashSet<ScriptedInstance>();
            Teleporter = new HashSet<Teleporter>();
            MapTypeMap = new HashSet<MapTypeMap>();
            Respawn = new HashSet<Respawn>();
            RespawnMapType = new HashSet<RespawnMapType>();
        }

        #endregion

        #region Properties

        public ICollection<Character> Character { get; set; }

        public byte[] Data { get; set; }

        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.None)]
        public short MapId { get; set; }

        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.None)]
        public short GridMapId { get; set; }

        public ICollection<MapMonster> MapMonster { get; set; }

        public ICollection<MapNpc> MapNpc { get; set; }

        public ICollection<MapTypeMap> MapTypeMap { get; set; }

        public int Music { get; set; }

        [MaxLength(length: 255)] public string Name { get; set; }

        public ICollection<Portal> Portal { get; set; }

        public ICollection<Portal> Portal1 { get; set; }

        public ICollection<Respawn> Respawn { get; set; }

        public ICollection<RespawnMapType> RespawnMapType { get; set; }

        public ICollection<ScriptedInstance> ScriptedInstance { get; set; }

        public bool ShopAllowed { get; set; }

        public ICollection<Teleporter> Teleporter { get; set; }

        public byte XpRate { get; set; }

        #endregion
    }
}