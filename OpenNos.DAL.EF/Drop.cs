namespace OpenNos.DAL.EF
{
    public sealed class Drop
    {
        #region Properties

        public int Amount { get; set; }

        public int DropChance { get; set; }

        public short DropId { get; set; }

        public Item Item { get; set; }

        public short ItemVNum { get; set; }

        public MapType MapType { get; set; }

        public short? MapTypeId { get; set; }

        public short? MonsterVNum { get; set; }

        public NpcMonster NpcMonster { get; set; }

        #endregion
    }
}