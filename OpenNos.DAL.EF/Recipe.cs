using System.Collections.Generic;

namespace OpenNos.DAL.EF
{
    public sealed class Recipe
    {
        #region Instantiation

        public Recipe()
        {
            RecipeItem = new HashSet<RecipeItem>();
            RecipeList = new HashSet<RecipeList>();
        }

        #endregion

        #region Properties

        public short Amount { get; set; }

        public Item Item { get; set; }

        public short ItemVNum { get; set; }

        public short RecipeId { get; set; }

        public ICollection<RecipeItem> RecipeItem { get; set; }

        public ICollection<RecipeList> RecipeList { get; set; }

        #endregion
    }
}