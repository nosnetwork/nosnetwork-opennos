using System.Data.Entity.Migrations;

namespace OpenNos.DAL.EF.Migrations
{
    public partial class MaxPartnerCount : DbMigration
    {
        public override void Up()
        {
            AddColumn(table: "dbo.Character", name: "MaxPartnerCount", columnAction: c => c.Byte(nullable: false));
        }

        public override void Down()
        {
            DropColumn(table: "dbo.Character", name: "MaxPartnerCount");
        }
    }
}