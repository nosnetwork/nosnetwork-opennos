using System.Data.Entity.Migrations;

namespace OpenNos.DAL.EF.Migrations
{
    public partial class BoxItem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    name: "dbo.BoxItem",
                    columnsAction: c => new
                    {
                        BoxItemId = c.Long(nullable: false, identity: true),
                        OriginalItemVNum = c.Short(nullable: false),
                        OriginalItemDesign = c.Short(nullable: false),
                        ItemGeneratedAmount = c.Short(nullable: false),
                        ItemGeneratedVNum = c.Short(nullable: false),
                        ItemGeneratedDesign = c.Short(nullable: false),
                        ItemGeneratedRare = c.Byte(nullable: false),
                        ItemGeneratedUpgrade = c.Byte(nullable: false),
                        Probability = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.BoxItemId);
        }

        public override void Down()
        {
            DropTable(name: "dbo.BoxItem");
        }
    }
}