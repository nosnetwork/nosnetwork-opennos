using System.Data.Entity.Migrations;

namespace OpenNos.DAL.EF.Migrations
{
    public partial class Migration : DbMigration
    {
        public override void Up()
        {
            AddColumn(table: "dbo.Character", name: "PrestigeLevel", columnAction: c => c.Int(nullable: false));
        }

        public override void Down()
        {
            DropColumn(table: "dbo.Character", name: "PrestigeLevel");
        }
    }
}