using System.Data.Entity.Migrations;

namespace OpenNos.DAL.EF.Migrations
{
    public partial class OpenNos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    name: "dbo.Account",
                    columnsAction: c => new
                    {
                        AccountId = c.Long(nullable: false, identity: true),
                        Authority = c.Short(nullable: false),
                        Email = c.String(maxLength: 255),
                        Name = c.String(maxLength: 255),
                        Password = c.String(maxLength: 255, unicode: false),
                        ReferrerId = c.Long(nullable: false),
                        RegistrationIP = c.String(maxLength: 45),
                        VerificationToken = c.String(maxLength: 32),
                        DailyRewardSent = c.Boolean(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.AccountId);

            CreateTable(
                    name: "dbo.Character",
                    columnsAction: c => new
                    {
                        CharacterId = c.Long(nullable: false, identity: true),
                        AccountId = c.Long(nullable: false),
                        Act4Dead = c.Int(nullable: false),
                        Act4Kill = c.Int(nullable: false),
                        Act4Points = c.Int(nullable: false),
                        ArenaWinner = c.Int(nullable: false),
                        Biography = c.String(maxLength: 255),
                        BuffBlocked = c.Boolean(nullable: false),
                        Class = c.Byte(nullable: false),
                        Compliment = c.Short(nullable: false),
                        Dignity = c.Single(nullable: false),
                        EmoticonsBlocked = c.Boolean(nullable: false),
                        ExchangeBlocked = c.Boolean(nullable: false),
                        Faction = c.Byte(nullable: false),
                        FamilyRequestBlocked = c.Boolean(nullable: false),
                        FriendRequestBlocked = c.Boolean(nullable: false),
                        Gender = c.Byte(nullable: false),
                        Gold = c.Long(nullable: false),
                        GoldBank = c.Long(nullable: false),
                        GroupRequestBlocked = c.Boolean(nullable: false),
                        HairColor = c.Byte(nullable: false),
                        HairStyle = c.Byte(nullable: false),
                        HeroChatBlocked = c.Boolean(nullable: false),
                        HeroLevel = c.Byte(nullable: false),
                        HeroXp = c.Long(nullable: false),
                        Hp = c.Int(nullable: false),
                        HpBlocked = c.Boolean(nullable: false),
                        IsPetAutoRelive = c.Boolean(nullable: false),
                        IsPartnerAutoRelive = c.Boolean(nullable: false),
                        IsSeal = c.Boolean(nullable: false),
                        JobLevel = c.Byte(nullable: false),
                        JobLevelXp = c.Long(nullable: false),
                        LastFamilyLeave = c.Long(nullable: false),
                        Level = c.Byte(nullable: false),
                        LevelXp = c.Long(nullable: false),
                        MapId = c.Short(nullable: false),
                        MapX = c.Short(nullable: false),
                        MapY = c.Short(nullable: false),
                        MasterPoints = c.Int(nullable: false),
                        MasterTicket = c.Int(nullable: false),
                        MaxMateCount = c.Byte(nullable: false),
                        MinilandInviteBlocked = c.Boolean(nullable: false),
                        MinilandMessage = c.String(maxLength: 255),
                        MinilandPoint = c.Short(nullable: false),
                        MinilandState = c.Byte(nullable: false),
                        MouseAimLock = c.Boolean(nullable: false),
                        Mp = c.Int(nullable: false),
                        Name = c.String(maxLength: 255, unicode: false),
                        QuickGetUp = c.Boolean(nullable: false),
                        RagePoint = c.Long(nullable: false),
                        Reputation = c.Long(nullable: false),
                        Slot = c.Byte(nullable: false),
                        SpAdditionPoint = c.Int(nullable: false),
                        SpPoint = c.Int(nullable: false),
                        State = c.Byte(nullable: false),
                        TalentLose = c.Int(nullable: false),
                        TalentSurrender = c.Int(nullable: false),
                        TalentWin = c.Int(nullable: false),
                        WhisperBlocked = c.Boolean(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.CharacterId)
                .ForeignKey(principalTable: "dbo.Map", dependentKeyExpression: t => t.MapId)
                .ForeignKey(principalTable: "dbo.Account", dependentKeyExpression: t => t.AccountId)
                .Index(indexExpression: t => t.AccountId)
                .Index(indexExpression: t => t.MapId);

            CreateTable(
                    name: "dbo.BazaarItem",
                    columnsAction: c => new
                    {
                        BazaarItemId = c.Long(nullable: false, identity: true),
                        Amount = c.Short(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        Duration = c.Short(nullable: false),
                        IsPackage = c.Boolean(nullable: false),
                        ItemInstanceId = c.Guid(nullable: false),
                        MedalUsed = c.Boolean(nullable: false),
                        Price = c.Long(nullable: false),
                        SellerId = c.Long(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.BazaarItemId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.SellerId)
                .ForeignKey(principalTable: "dbo.ItemInstance", dependentKeyExpression: t => t.ItemInstanceId)
                .Index(indexExpression: t => t.ItemInstanceId)
                .Index(indexExpression: t => t.SellerId);

            CreateTable(
                    name: "dbo.ItemInstance",
                    columnsAction: c => new
                    {
                        Id = c.Guid(nullable: false),
                        Amount = c.Int(nullable: false),
                        BazaarItemId = c.Long(),
                        BoundCharacterId = c.Long(),
                        CharacterId = c.Long(nullable: false),
                        Design = c.Short(nullable: false),
                        DurabilityPoint = c.Int(nullable: false),
                        ItemDeleteTime = c.DateTime(),
                        ItemVNum = c.Short(nullable: false),
                        Rare = c.Short(nullable: false),
                        Slot = c.Short(nullable: false),
                        Type = c.Byte(nullable: false),
                        Upgrade = c.Byte(nullable: false),
                        HoldingVNum = c.Short(),
                        ShellRarity = c.Short(),
                        SlDamage = c.Short(),
                        SlDefence = c.Short(),
                        SlElement = c.Short(),
                        SlHP = c.Short(),
                        SpDamage = c.Byte(),
                        SpDark = c.Byte(),
                        SpDefence = c.Byte(),
                        SpElement = c.Byte(),
                        SpFire = c.Byte(),
                        SpHP = c.Byte(),
                        SpLevel = c.Byte(),
                        SpLight = c.Byte(),
                        SpStoneUpgrade = c.Byte(),
                        SpWater = c.Byte(),
                        Ammo = c.Byte(),
                        Cellon = c.Byte(),
                        CloseDefence = c.Short(),
                        Concentrate = c.Short(),
                        CriticalDodge = c.Short(),
                        CriticalLuckRate = c.Byte(),
                        CriticalRate = c.Short(),
                        DamageMaximum = c.Short(),
                        DamageMinimum = c.Short(),
                        DarkElement = c.Byte(),
                        DarkResistance = c.Short(),
                        DefenceDodge = c.Short(),
                        DistanceDefence = c.Short(),
                        DistanceDefenceDodge = c.Short(),
                        ElementRate = c.Short(),
                        EquipmentSerialId = c.Guid(),
                        FireElement = c.Byte(),
                        FireResistance = c.Short(),
                        HitRate = c.Short(),
                        HP = c.Short(),
                        IsEmpty = c.Boolean(),
                        IsFixed = c.Boolean(),
                        IsPartnerEquipment = c.Boolean(),
                        LightElement = c.Byte(),
                        LightResistance = c.Short(),
                        MagicDefence = c.Short(),
                        MaxElementRate = c.Short(),
                        MP = c.Short(),
                        WaterElement = c.Byte(),
                        WaterResistance = c.Short(),
                        XP = c.Long()
                    })
                .PrimaryKey(keyExpression: t => t.Id)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.BoundCharacterId)
                .ForeignKey(principalTable: "dbo.Item", dependentKeyExpression: t => t.ItemVNum)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.BoundCharacterId)
                .Index(indexExpression: t => new { t.CharacterId, t.Slot, t.Type }, name: "IX_SlotAndType")
                .Index(indexExpression: t => t.ItemVNum);

            CreateTable(
                    name: "dbo.Item",
                    columnsAction: c => new
                    {
                        VNum = c.Short(nullable: false),
                        BasicUpgrade = c.Byte(nullable: false),
                        CellonLvl = c.Byte(nullable: false),
                        Class = c.Byte(nullable: false),
                        CloseDefence = c.Short(nullable: false),
                        Color = c.Byte(nullable: false),
                        Concentrate = c.Short(nullable: false),
                        CriticalLuckRate = c.Byte(nullable: false),
                        CriticalRate = c.Short(nullable: false),
                        DamageMaximum = c.Short(nullable: false),
                        DamageMinimum = c.Short(nullable: false),
                        DarkElement = c.Byte(nullable: false),
                        DarkResistance = c.Short(nullable: false),
                        DefenceDodge = c.Short(nullable: false),
                        DistanceDefence = c.Short(nullable: false),
                        DistanceDefenceDodge = c.Short(nullable: false),
                        Effect = c.Short(nullable: false),
                        EffectValue = c.Int(nullable: false),
                        Element = c.Byte(nullable: false),
                        ElementRate = c.Short(nullable: false),
                        EquipmentSlot = c.Byte(nullable: false),
                        FireElement = c.Byte(nullable: false),
                        FireResistance = c.Short(nullable: false),
                        Height = c.Byte(nullable: false),
                        HitRate = c.Short(nullable: false),
                        Hp = c.Short(nullable: false),
                        HpRegeneration = c.Short(nullable: false),
                        IsBlocked = c.Boolean(nullable: false),
                        IsColored = c.Boolean(nullable: false),
                        IsConsumable = c.Boolean(nullable: false),
                        IsDroppable = c.Boolean(nullable: false),
                        IsHeroic = c.Boolean(nullable: false),
                        IsHolder = c.Boolean(nullable: false),
                        IsMinilandObject = c.Boolean(nullable: false),
                        IsSoldable = c.Boolean(nullable: false),
                        IsTradable = c.Boolean(nullable: false),
                        ItemSubType = c.Byte(nullable: false),
                        ItemType = c.Byte(nullable: false),
                        ItemValidTime = c.Long(nullable: false),
                        LevelJobMinimum = c.Byte(nullable: false),
                        LevelMinimum = c.Byte(nullable: false),
                        LightElement = c.Byte(nullable: false),
                        LightResistance = c.Short(nullable: false),
                        MagicDefence = c.Short(nullable: false),
                        MaxCellon = c.Byte(nullable: false),
                        MaxCellonLvl = c.Byte(nullable: false),
                        MaxElementRate = c.Short(nullable: false),
                        MaximumAmmo = c.Byte(nullable: false),
                        MinilandObjectPoint = c.Int(nullable: false),
                        MoreHp = c.Short(nullable: false),
                        MoreMp = c.Short(nullable: false),
                        Morph = c.Short(nullable: false),
                        Mp = c.Short(nullable: false),
                        MpRegeneration = c.Short(nullable: false),
                        Name = c.String(maxLength: 255),
                        Price = c.Long(nullable: false),
                        SellToNpcPrice = c.Long(nullable: false),
                        PvpDefence = c.Short(nullable: false),
                        PvpStrength = c.Byte(nullable: false),
                        ReduceOposantResistance = c.Short(nullable: false),
                        ReputationMinimum = c.Byte(nullable: false),
                        ReputPrice = c.Long(nullable: false),
                        SecondaryElement = c.Byte(nullable: false),
                        Sex = c.Byte(nullable: false),
                        Speed = c.Byte(nullable: false),
                        SpType = c.Byte(nullable: false),
                        Type = c.Byte(nullable: false),
                        WaitDelay = c.Short(nullable: false),
                        WaterElement = c.Byte(nullable: false),
                        WaterResistance = c.Short(nullable: false),
                        Width = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.VNum);

            CreateTable(
                    name: "dbo.BCard",
                    columnsAction: c => new
                    {
                        BCardId = c.Int(nullable: false, identity: true),
                        CardId = c.Short(),
                        CastType = c.Byte(nullable: false),
                        FirstData = c.Int(nullable: false),
                        IsLevelDivided = c.Boolean(nullable: false),
                        IsLevelScaled = c.Boolean(nullable: false),
                        ItemVNum = c.Short(),
                        NpcMonsterVNum = c.Short(),
                        SecondData = c.Int(nullable: false),
                        SkillVNum = c.Short(),
                        SubType = c.Byte(nullable: false),
                        ThirdData = c.Int(nullable: false),
                        Type = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.BCardId)
                .ForeignKey(principalTable: "dbo.Card", dependentKeyExpression: t => t.CardId)
                .ForeignKey(principalTable: "dbo.Item", dependentKeyExpression: t => t.ItemVNum)
                .ForeignKey(principalTable: "dbo.NpcMonster", dependentKeyExpression: t => t.NpcMonsterVNum)
                .ForeignKey(principalTable: "dbo.Skill", dependentKeyExpression: t => t.SkillVNum)
                .Index(indexExpression: t => t.CardId)
                .Index(indexExpression: t => t.ItemVNum)
                .Index(indexExpression: t => t.NpcMonsterVNum)
                .Index(indexExpression: t => t.SkillVNum);

            CreateTable(
                    name: "dbo.Card",
                    columnsAction: c => new
                    {
                        CardId = c.Short(nullable: false),
                        BuffType = c.Byte(nullable: false),
                        Delay = c.Int(nullable: false),
                        Duration = c.Int(nullable: false),
                        EffectId = c.Int(nullable: false),
                        Level = c.Byte(nullable: false),
                        Name = c.String(maxLength: 255),
                        Propability = c.Byte(nullable: false),
                        TimeoutBuff = c.Short(nullable: false),
                        TimeoutBuffChance = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.CardId);

            CreateTable(
                    name: "dbo.StaticBuff",
                    columnsAction: c => new
                    {
                        StaticBuffId = c.Long(nullable: false, identity: true),
                        CardId = c.Short(nullable: false),
                        CharacterId = c.Long(nullable: false),
                        RemainingTime = c.Int(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.StaticBuffId)
                .ForeignKey(principalTable: "dbo.Card", dependentKeyExpression: t => t.CardId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.CardId)
                .Index(indexExpression: t => t.CharacterId);

            CreateTable(
                    name: "dbo.NpcMonster",
                    columnsAction: c => new
                    {
                        NpcMonsterVNum = c.Short(nullable: false),
                        AmountRequired = c.Short(nullable: false),
                        AttackClass = c.Byte(nullable: false),
                        AttackUpgrade = c.Byte(nullable: false),
                        BasicArea = c.Byte(nullable: false),
                        BasicCooldown = c.Short(nullable: false),
                        BasicRange = c.Byte(nullable: false),
                        BasicSkill = c.Short(nullable: false),
                        Catch = c.Boolean(nullable: false),
                        CloseDefence = c.Short(nullable: false),
                        Concentrate = c.Short(nullable: false),
                        CriticalChance = c.Byte(nullable: false),
                        CriticalRate = c.Short(nullable: false),
                        DamageMaximum = c.Short(nullable: false),
                        DamageMinimum = c.Short(nullable: false),
                        DarkResistance = c.Short(nullable: false),
                        DefenceDodge = c.Short(nullable: false),
                        DefenceUpgrade = c.Byte(nullable: false),
                        DistanceDefence = c.Short(nullable: false),
                        DistanceDefenceDodge = c.Short(nullable: false),
                        Element = c.Byte(nullable: false),
                        ElementRate = c.Short(nullable: false),
                        FireResistance = c.Short(nullable: false),
                        HeroLevel = c.Byte(nullable: false),
                        HeroXP = c.Int(nullable: false),
                        IsHostile = c.Boolean(nullable: false),
                        JobXP = c.Int(nullable: false),
                        Level = c.Byte(nullable: false),
                        LightResistance = c.Short(nullable: false),
                        MagicDefence = c.Short(nullable: false),
                        MaxHP = c.Int(nullable: false),
                        MaxMP = c.Int(nullable: false),
                        MonsterType = c.Byte(nullable: false),
                        Name = c.String(maxLength: 255),
                        NoAggresiveIcon = c.Boolean(nullable: false),
                        NoticeRange = c.Byte(nullable: false),
                        OriginalNpcMonsterVNum = c.Short(nullable: false),
                        Race = c.Byte(nullable: false),
                        RaceType = c.Byte(nullable: false),
                        RespawnTime = c.Int(nullable: false),
                        Speed = c.Byte(nullable: false),
                        VNumRequired = c.Short(nullable: false),
                        WaterResistance = c.Short(nullable: false),
                        XP = c.Int(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.NpcMonsterVNum);

            CreateTable(
                    name: "dbo.Drop",
                    columnsAction: c => new
                    {
                        DropId = c.Short(nullable: false, identity: true),
                        Amount = c.Int(nullable: false),
                        DropChance = c.Int(nullable: false),
                        ItemVNum = c.Short(nullable: false),
                        MapTypeId = c.Short(),
                        MonsterVNum = c.Short()
                    })
                .PrimaryKey(keyExpression: t => t.DropId)
                .ForeignKey(principalTable: "dbo.MapType", dependentKeyExpression: t => t.MapTypeId)
                .ForeignKey(principalTable: "dbo.NpcMonster", dependentKeyExpression: t => t.MonsterVNum)
                .ForeignKey(principalTable: "dbo.Item", dependentKeyExpression: t => t.ItemVNum)
                .Index(indexExpression: t => t.ItemVNum)
                .Index(indexExpression: t => t.MapTypeId)
                .Index(indexExpression: t => t.MonsterVNum);

            CreateTable(
                    name: "dbo.MapType",
                    columnsAction: c => new
                    {
                        MapTypeId = c.Short(nullable: false, identity: true),
                        MapTypeName = c.String(),
                        PotionDelay = c.Short(nullable: false),
                        RespawnMapTypeId = c.Long(),
                        ReturnMapTypeId = c.Long()
                    })
                .PrimaryKey(keyExpression: t => t.MapTypeId)
                .ForeignKey(principalTable: "dbo.RespawnMapType", dependentKeyExpression: t => t.RespawnMapTypeId)
                .ForeignKey(principalTable: "dbo.RespawnMapType", dependentKeyExpression: t => t.ReturnMapTypeId)
                .Index(indexExpression: t => t.RespawnMapTypeId)
                .Index(indexExpression: t => t.ReturnMapTypeId);

            CreateTable(
                    name: "dbo.MapTypeMap",
                    columnsAction: c => new
                    {
                        MapId = c.Short(nullable: false),
                        MapTypeId = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => new { t.MapId, t.MapTypeId })
                .ForeignKey(principalTable: "dbo.Map", dependentKeyExpression: t => t.MapId)
                .ForeignKey(principalTable: "dbo.MapType", dependentKeyExpression: t => t.MapTypeId)
                .Index(indexExpression: t => t.MapId)
                .Index(indexExpression: t => t.MapTypeId);

            CreateTable(
                    name: "dbo.Map",
                    columnsAction: c => new
                    {
                        MapId = c.Short(nullable: false),
                        Data = c.Binary(),
                        GridMapId = c.Short(nullable: false),
                        Music = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        ShopAllowed = c.Boolean(nullable: false),
                        XpRate = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.MapId);

            CreateTable(
                    name: "dbo.MapMonster",
                    columnsAction: c => new
                    {
                        MapMonsterId = c.Int(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        IsMoving = c.Boolean(nullable: false),
                        MapId = c.Short(nullable: false),
                        MapX = c.Short(nullable: false),
                        MapY = c.Short(nullable: false),
                        MonsterVNum = c.Short(nullable: false),
                        Name = c.String(),
                        Position = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.MapMonsterId)
                //.ForeignKey("dbo.Map", t => t.MapId) //ForeignKey MapId disabled for Error
                .ForeignKey(principalTable: "dbo.NpcMonster", dependentKeyExpression: t => t.MonsterVNum)
                .Index(indexExpression: t => t.MapId)
                .Index(indexExpression: t => t.MonsterVNum);

            CreateTable(
                    name: "dbo.MapNpc",
                    columnsAction: c => new
                    {
                        MapNpcId = c.Int(nullable: false),
                        Dialog = c.Short(nullable: false),
                        Effect = c.Short(nullable: false),
                        EffectDelay = c.Short(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        IsMoving = c.Boolean(nullable: false),
                        IsSitting = c.Boolean(nullable: false),
                        MapId = c.Short(nullable: false),
                        MapX = c.Short(nullable: false),
                        MapY = c.Short(nullable: false),
                        Name = c.String(),
                        NpcVNum = c.Short(nullable: false),
                        Position = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.MapNpcId)
                .ForeignKey(principalTable: "dbo.Map", dependentKeyExpression: t => t.MapId)
                //.ForeignKey("dbo.NpcMonster", t => t.NpcVNum) //ForeignKey NpcVNum disabled for Error
                .Index(indexExpression: t => t.MapId)
                .Index(indexExpression: t => t.NpcVNum);

            CreateTable(
                    name: "dbo.RecipeList",
                    columnsAction: c => new
                    {
                        RecipeListId = c.Int(nullable: false, identity: true),
                        ItemVNum = c.Short(),
                        MapNpcId = c.Int(),
                        RecipeId = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.RecipeListId)
                .ForeignKey(principalTable: "dbo.Item", dependentKeyExpression: t => t.ItemVNum)
                .ForeignKey(principalTable: "dbo.MapNpc", dependentKeyExpression: t => t.MapNpcId)
                .ForeignKey(principalTable: "dbo.Recipe", dependentKeyExpression: t => t.RecipeId)
                .Index(indexExpression: t => t.ItemVNum)
                .Index(indexExpression: t => t.MapNpcId)
                .Index(indexExpression: t => t.RecipeId);

            CreateTable(
                    name: "dbo.Recipe",
                    columnsAction: c => new
                    {
                        RecipeId = c.Short(nullable: false, identity: true),
                        Amount = c.Short(nullable: false),
                        ItemVNum = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.RecipeId)
                .ForeignKey(principalTable: "dbo.Item", dependentKeyExpression: t => t.ItemVNum)
                .Index(indexExpression: t => t.ItemVNum);

            CreateTable(
                    name: "dbo.RecipeItem",
                    columnsAction: c => new
                    {
                        RecipeItemId = c.Short(nullable: false, identity: true),
                        Amount = c.Short(nullable: false),
                        ItemVNum = c.Short(nullable: false),
                        RecipeId = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.RecipeItemId)
                .ForeignKey(principalTable: "dbo.Recipe", dependentKeyExpression: t => t.RecipeId)
                .ForeignKey(principalTable: "dbo.Item", dependentKeyExpression: t => t.ItemVNum)
                .Index(indexExpression: t => t.ItemVNum)
                .Index(indexExpression: t => t.RecipeId);

            CreateTable(
                    name: "dbo.Shop",
                    columnsAction: c => new
                    {
                        ShopId = c.Int(nullable: false, identity: true),
                        MapNpcId = c.Int(nullable: false),
                        MenuType = c.Byte(nullable: false),
                        Name = c.String(maxLength: 255),
                        ShopType = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.ShopId)
                .ForeignKey(principalTable: "dbo.MapNpc", dependentKeyExpression: t => t.MapNpcId)
                .Index(indexExpression: t => t.MapNpcId);

            CreateTable(
                    name: "dbo.ShopItem",
                    columnsAction: c => new
                    {
                        ShopItemId = c.Int(nullable: false, identity: true),
                        Color = c.Byte(nullable: false),
                        ItemVNum = c.Short(nullable: false),
                        Rare = c.Short(nullable: false),
                        ShopId = c.Int(nullable: false),
                        Slot = c.Byte(nullable: false),
                        Type = c.Byte(nullable: false),
                        Upgrade = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.ShopItemId)
                .ForeignKey(principalTable: "dbo.Shop", dependentKeyExpression: t => t.ShopId)
                .ForeignKey(principalTable: "dbo.Item", dependentKeyExpression: t => t.ItemVNum)
                .Index(indexExpression: t => t.ItemVNum)
                .Index(indexExpression: t => t.ShopId);

            CreateTable(
                    name: "dbo.ShopSkill",
                    columnsAction: c => new
                    {
                        ShopSkillId = c.Int(nullable: false, identity: true),
                        ShopId = c.Int(nullable: false),
                        SkillVNum = c.Short(nullable: false),
                        Slot = c.Byte(nullable: false),
                        Type = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.ShopSkillId)
                .ForeignKey(principalTable: "dbo.Skill", dependentKeyExpression: t => t.SkillVNum)
                .ForeignKey(principalTable: "dbo.Shop", dependentKeyExpression: t => t.ShopId)
                .Index(indexExpression: t => t.ShopId)
                .Index(indexExpression: t => t.SkillVNum);

            CreateTable(
                    name: "dbo.Skill",
                    columnsAction: c => new
                    {
                        SkillVNum = c.Short(nullable: false),
                        AttackAnimation = c.Short(nullable: false),
                        CastAnimation = c.Short(nullable: false),
                        CastEffect = c.Short(nullable: false),
                        CastId = c.Short(nullable: false),
                        CastTime = c.Short(nullable: false),
                        Class = c.Byte(nullable: false),
                        Cooldown = c.Short(nullable: false),
                        CPCost = c.Byte(nullable: false),
                        Duration = c.Short(nullable: false),
                        Effect = c.Short(nullable: false),
                        Element = c.Byte(nullable: false),
                        HitType = c.Byte(nullable: false),
                        ItemVNum = c.Short(nullable: false),
                        Level = c.Byte(nullable: false),
                        LevelMinimum = c.Byte(nullable: false),
                        MinimumAdventurerLevel = c.Byte(nullable: false),
                        MinimumArcherLevel = c.Byte(nullable: false),
                        MinimumMagicianLevel = c.Byte(nullable: false),
                        MinimumSwordmanLevel = c.Byte(nullable: false),
                        MpCost = c.Short(nullable: false),
                        Name = c.String(maxLength: 255),
                        Price = c.Int(nullable: false),
                        Range = c.Byte(nullable: false),
                        SkillType = c.Byte(nullable: false),
                        TargetRange = c.Byte(nullable: false),
                        TargetType = c.Byte(nullable: false),
                        Type = c.Byte(nullable: false),
                        UpgradeSkill = c.Short(nullable: false),
                        UpgradeType = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.SkillVNum);

            CreateTable(
                    name: "dbo.CharacterSkill",
                    columnsAction: c => new
                    {
                        Id = c.Guid(nullable: false),
                        CharacterId = c.Long(nullable: false),
                        SkillVNum = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.Id)
                .ForeignKey(principalTable: "dbo.Skill", dependentKeyExpression: t => t.SkillVNum)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.SkillVNum);

            CreateTable(
                    name: "dbo.Combo",
                    columnsAction: c => new
                    {
                        ComboId = c.Int(nullable: false, identity: true),
                        Animation = c.Short(nullable: false),
                        Effect = c.Short(nullable: false),
                        Hit = c.Short(nullable: false),
                        SkillVNum = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.ComboId)
                .ForeignKey(principalTable: "dbo.Skill", dependentKeyExpression: t => t.SkillVNum)
                .Index(indexExpression: t => t.SkillVNum);

            CreateTable(
                    name: "dbo.NpcMonsterSkill",
                    columnsAction: c => new
                    {
                        NpcMonsterSkillId = c.Long(nullable: false, identity: true),
                        NpcMonsterVNum = c.Short(nullable: false),
                        Rate = c.Short(nullable: false),
                        SkillVNum = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.NpcMonsterSkillId)
                .ForeignKey(principalTable: "dbo.Skill", dependentKeyExpression: t => t.SkillVNum)
                .ForeignKey(principalTable: "dbo.NpcMonster", dependentKeyExpression: t => t.NpcMonsterVNum)
                .Index(indexExpression: t => t.NpcMonsterVNum)
                .Index(indexExpression: t => t.SkillVNum);

            CreateTable(
                    name: "dbo.Teleporter",
                    columnsAction: c => new
                    {
                        TeleporterId = c.Short(nullable: false, identity: true),
                        Index = c.Short(nullable: false),
                        Type = c.Byte(nullable: false),
                        MapId = c.Short(nullable: false),
                        MapNpcId = c.Int(nullable: false),
                        MapX = c.Short(nullable: false),
                        MapY = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.TeleporterId)
                //.ForeignKey("dbo.MapNpc", t => t.MapNpcId) Disabled Test
                .ForeignKey(principalTable: "dbo.Map", dependentKeyExpression: t => t.MapId)
                .Index(indexExpression: t => t.MapId)
                .Index(indexExpression: t => t.MapNpcId);

            CreateTable(
                    name: "dbo.Portal",
                    columnsAction: c => new
                    {
                        PortalId = c.Int(nullable: false, identity: true),
                        DestinationMapId = c.Short(nullable: false),
                        DestinationX = c.Short(nullable: false),
                        DestinationY = c.Short(nullable: false),
                        IsDisabled = c.Boolean(nullable: false),
                        SourceMapId = c.Short(nullable: false),
                        SourceX = c.Short(nullable: false),
                        SourceY = c.Short(nullable: false),
                        Type = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.PortalId)
                .ForeignKey(principalTable: "dbo.Map", dependentKeyExpression: t => t.DestinationMapId)
                .ForeignKey(principalTable: "dbo.Map", dependentKeyExpression: t => t.SourceMapId)
                .Index(indexExpression: t => t.DestinationMapId)
                .Index(indexExpression: t => t.SourceMapId);

            CreateTable(
                    name: "dbo.Respawn",
                    columnsAction: c => new
                    {
                        RespawnId = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(nullable: false),
                        MapId = c.Short(nullable: false),
                        RespawnMapTypeId = c.Long(nullable: false),
                        X = c.Short(nullable: false),
                        Y = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.RespawnId)
                .ForeignKey(principalTable: "dbo.Map", dependentKeyExpression: t => t.MapId)
                .ForeignKey(principalTable: "dbo.RespawnMapType", dependentKeyExpression: t => t.RespawnMapTypeId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.MapId)
                .Index(indexExpression: t => t.RespawnMapTypeId);

            CreateTable(
                    name: "dbo.RespawnMapType",
                    columnsAction: c => new
                    {
                        RespawnMapTypeId = c.Long(nullable: false),
                        DefaultMapId = c.Short(nullable: false),
                        DefaultX = c.Short(nullable: false),
                        DefaultY = c.Short(nullable: false),
                        Name = c.String(maxLength: 255)
                    })
                .PrimaryKey(keyExpression: t => t.RespawnMapTypeId)
                .ForeignKey(principalTable: "dbo.Map", dependentKeyExpression: t => t.DefaultMapId)
                .Index(indexExpression: t => t.DefaultMapId);

            CreateTable(
                    name: "dbo.ScriptedInstance",
                    columnsAction: c => new
                    {
                        ScriptedInstanceId = c.Short(nullable: false, identity: true),
                        MapId = c.Short(nullable: false),
                        PositionX = c.Short(nullable: false),
                        PositionY = c.Short(nullable: false),
                        Script = c.String(),
                        Type = c.Byte(nullable: false),
                        Label = c.String()
                    })
                .PrimaryKey(keyExpression: t => t.ScriptedInstanceId)
                .ForeignKey(principalTable: "dbo.Map", dependentKeyExpression: t => t.MapId)
                .Index(indexExpression: t => t.MapId);

            CreateTable(
                    name: "dbo.Mate",
                    columnsAction: c => new
                    {
                        MateId = c.Long(nullable: false, identity: true),
                        Attack = c.Byte(nullable: false),
                        CanPickUp = c.Boolean(nullable: false),
                        CharacterId = c.Long(nullable: false),
                        Defence = c.Byte(nullable: false),
                        Direction = c.Byte(nullable: false),
                        Experience = c.Long(nullable: false),
                        Hp = c.Double(nullable: false),
                        IsSummonable = c.Boolean(nullable: false),
                        IsTeamMember = c.Boolean(nullable: false),
                        Level = c.Byte(nullable: false),
                        Loyalty = c.Short(nullable: false),
                        MapX = c.Short(nullable: false),
                        MapY = c.Short(nullable: false),
                        MateType = c.Byte(nullable: false),
                        Mp = c.Double(nullable: false),
                        Name = c.String(maxLength: 255),
                        NpcMonsterVNum = c.Short(nullable: false),
                        Skin = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.MateId)
                .ForeignKey(principalTable: "dbo.NpcMonster", dependentKeyExpression: t => t.NpcMonsterVNum)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.NpcMonsterVNum);

            CreateTable(
                    name: "dbo.Mail",
                    columnsAction: c => new
                    {
                        MailId = c.Long(nullable: false, identity: true),
                        AttachmentAmount = c.Short(nullable: false),
                        AttachmentLevel = c.Byte(nullable: false),
                        AttachmentRarity = c.Byte(nullable: false),
                        AttachmentUpgrade = c.Byte(nullable: false),
                        AttachmentDesign = c.Short(nullable: false),
                        AttachmentVNum = c.Short(),
                        Date = c.DateTime(nullable: false),
                        EqPacket = c.String(maxLength: 255),
                        IsOpened = c.Boolean(nullable: false),
                        IsSenderCopy = c.Boolean(nullable: false),
                        Message = c.String(maxLength: 255),
                        ReceiverId = c.Long(nullable: false),
                        SenderClass = c.Byte(nullable: false),
                        SenderGender = c.Byte(nullable: false),
                        SenderHairColor = c.Byte(nullable: false),
                        SenderHairStyle = c.Byte(nullable: false),
                        SenderId = c.Long(nullable: false),
                        SenderMorphId = c.Short(nullable: false),
                        Title = c.String(maxLength: 255)
                    })
                .PrimaryKey(keyExpression: t => t.MailId)
                .ForeignKey(principalTable: "dbo.Item", dependentKeyExpression: t => t.AttachmentVNum)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.SenderId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.ReceiverId)
                .Index(indexExpression: t => t.AttachmentVNum)
                .Index(indexExpression: t => t.ReceiverId)
                .Index(indexExpression: t => t.SenderId);

            CreateTable(
                    name: "dbo.RollGeneratedItem",
                    columnsAction: c => new
                    {
                        RollGeneratedItemId = c.Short(nullable: false, identity: true),
                        IsRareRandom = c.Boolean(nullable: false),
                        ItemGeneratedAmount = c.Short(nullable: false),
                        ItemGeneratedVNum = c.Short(nullable: false),
                        ItemGeneratedDesign = c.Short(nullable: false),
                        MaximumOriginalItemRare = c.Byte(nullable: false),
                        MinimumOriginalItemRare = c.Byte(nullable: false),
                        OriginalItemDesign = c.Short(nullable: false),
                        OriginalItemVNum = c.Short(nullable: false),
                        Probability = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.RollGeneratedItemId)
                .ForeignKey(principalTable: "dbo.Item", dependentKeyExpression: t => t.ItemGeneratedVNum)
                .ForeignKey(principalTable: "dbo.Item", dependentKeyExpression: t => t.OriginalItemVNum)
                .Index(indexExpression: t => t.ItemGeneratedVNum)
                .Index(indexExpression: t => t.OriginalItemVNum);

            CreateTable(
                    name: "dbo.MinilandObject",
                    columnsAction: c => new
                    {
                        MinilandObjectId = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(nullable: false),
                        ItemInstanceId = c.Guid(),
                        Level1BoxAmount = c.Byte(nullable: false),
                        Level2BoxAmount = c.Byte(nullable: false),
                        Level3BoxAmount = c.Byte(nullable: false),
                        Level4BoxAmount = c.Byte(nullable: false),
                        Level5BoxAmount = c.Byte(nullable: false),
                        MapX = c.Short(nullable: false),
                        MapY = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.MinilandObjectId)
                .ForeignKey(principalTable: "dbo.ItemInstance", dependentKeyExpression: t => t.ItemInstanceId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.ItemInstanceId);

            CreateTable(
                    name: "dbo.CharacterRelation",
                    columnsAction: c => new
                    {
                        CharacterRelationId = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(nullable: false),
                        RelatedCharacterId = c.Long(nullable: false),
                        RelationType = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.CharacterRelationId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.RelatedCharacterId)
                .Index(indexExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.RelatedCharacterId);

            CreateTable(
                    name: "dbo.FamilyCharacter",
                    columnsAction: c => new
                    {
                        FamilyCharacterId = c.Long(nullable: false, identity: true),
                        Authority = c.Byte(nullable: false),
                        CharacterId = c.Long(nullable: false),
                        DailyMessage = c.String(maxLength: 255),
                        Experience = c.Int(nullable: false),
                        FamilyId = c.Long(nullable: false),
                        Rank = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.FamilyCharacterId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .ForeignKey(principalTable: "dbo.Family", dependentKeyExpression: t => t.FamilyId)
                .Index(indexExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.FamilyId);

            CreateTable(
                    name: "dbo.Family",
                    columnsAction: c => new
                    {
                        FamilyId = c.Long(nullable: false, identity: true),
                        FamilyExperience = c.Int(nullable: false),
                        FamilyFaction = c.Byte(nullable: false),
                        FamilyHeadGender = c.Byte(nullable: false),
                        FamilyLevel = c.Byte(nullable: false),
                        FamilyMessage = c.String(maxLength: 255),
                        LastFactionChange = c.Long(nullable: false),
                        ManagerAuthorityType = c.Byte(nullable: false),
                        ManagerCanGetHistory = c.Boolean(nullable: false),
                        ManagerCanInvite = c.Boolean(nullable: false),
                        ManagerCanNotice = c.Boolean(nullable: false),
                        ManagerCanShout = c.Boolean(nullable: false),
                        MaxSize = c.Short(nullable: false),
                        MemberAuthorityType = c.Byte(nullable: false),
                        MemberCanGetHistory = c.Boolean(nullable: false),
                        Name = c.String(maxLength: 255),
                        WarehouseSize = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.FamilyId);

            CreateTable(
                    name: "dbo.FamilyLog",
                    columnsAction: c => new
                    {
                        FamilyLogId = c.Long(nullable: false, identity: true),
                        FamilyId = c.Long(nullable: false),
                        FamilyLogData = c.String(maxLength: 255),
                        FamilyLogType = c.Byte(nullable: false),
                        Timestamp = c.DateTime(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.FamilyLogId)
                .ForeignKey(principalTable: "dbo.Family", dependentKeyExpression: t => t.FamilyId)
                .Index(indexExpression: t => t.FamilyId);

            CreateTable(
                    name: "dbo.GeneralLog",
                    columnsAction: c => new
                    {
                        LogId = c.Long(nullable: false, identity: true),
                        AccountId = c.Long(),
                        CharacterId = c.Long(),
                        IpAddress = c.String(maxLength: 255),
                        LogData = c.String(maxLength: 255),
                        LogType = c.String(),
                        Timestamp = c.DateTime(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.LogId)
                .ForeignKey(principalTable: "dbo.Account", dependentKeyExpression: t => t.AccountId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.AccountId)
                .Index(indexExpression: t => t.CharacterId);

            CreateTable(
                    name: "dbo.MinigameLog",
                    columnsAction: c => new
                    {
                        MinigameLogId = c.Long(nullable: false, identity: true),
                        StartTime = c.Long(nullable: false),
                        EndTime = c.Long(nullable: false),
                        Score = c.Int(nullable: false),
                        Minigame = c.Byte(nullable: false),
                        CharacterId = c.Long(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.MinigameLogId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.CharacterId);

            CreateTable(
                    name: "dbo.QuicklistEntry",
                    columnsAction: c => new
                    {
                        Id = c.Guid(nullable: false),
                        CharacterId = c.Long(nullable: false),
                        Morph = c.Short(nullable: false),
                        Pos = c.Short(nullable: false),
                        Q1 = c.Short(nullable: false),
                        Q2 = c.Short(nullable: false),
                        Slot = c.Short(nullable: false),
                        Type = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.Id)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.CharacterId);

            CreateTable(
                    name: "dbo.StaticBonus",
                    columnsAction: c => new
                    {
                        StaticBonusId = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(nullable: false),
                        DateEnd = c.DateTime(nullable: false),
                        StaticBonusType = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.StaticBonusId)
                .ForeignKey(principalTable: "dbo.Character", dependentKeyExpression: t => t.CharacterId)
                .Index(indexExpression: t => t.CharacterId);

            CreateTable(
                    name: "dbo.PenaltyLog",
                    columnsAction: c => new
                    {
                        PenaltyLogId = c.Int(nullable: false, identity: true),
                        AccountId = c.Long(nullable: false),
                        IP = c.String(),
                        AdminName = c.String(),
                        DateEnd = c.DateTime(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        Penalty = c.Byte(nullable: false),
                        Reason = c.String(maxLength: 255)
                    })
                .PrimaryKey(keyExpression: t => t.PenaltyLogId)
                .ForeignKey(principalTable: "dbo.Account", dependentKeyExpression: t => t.AccountId)
                .Index(indexExpression: t => t.AccountId);

            CreateTable(
                    name: "dbo.CellonOption",
                    columnsAction: c => new
                    {
                        CellonOptionId = c.Long(nullable: false, identity: true),
                        EquipmentSerialId = c.Guid(nullable: false),
                        Level = c.Byte(nullable: false),
                        Type = c.Byte(nullable: false),
                        Value = c.Int(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.CellonOptionId);

            CreateTable(
                    name: "dbo.CharacterQuest",
                    columnsAction: c => new
                    {
                        Id = c.Guid(nullable: false),
                        CharacterId = c.Long(nullable: false),
                        QuestId = c.Long(nullable: false),
                        FirstObjective = c.Int(nullable: false),
                        SecondObjective = c.Int(nullable: false),
                        ThirdObjective = c.Int(nullable: false),
                        FourthObjective = c.Int(nullable: false),
                        FifthObjective = c.Int(nullable: false),
                        IsMainQuest = c.Boolean(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.Id)
                .ForeignKey(principalTable: "dbo.Quest", dependentKeyExpression: t => t.QuestId, cascadeDelete: true)
                .Index(indexExpression: t => t.QuestId);

            CreateTable(
                    name: "dbo.Quest",
                    columnsAction: c => new
                    {
                        QuestId = c.Long(nullable: false),
                        QuestType = c.Int(nullable: false),
                        LevelMin = c.Byte(nullable: false),
                        LevelMax = c.Byte(nullable: false),
                        StartDialogId = c.Int(),
                        EndDialogId = c.Int(),
                        DialogNpcVNum = c.Int(),
                        DialogNpcId = c.Int(),
                        TargetMap = c.Short(),
                        TargetX = c.Short(),
                        TargetY = c.Short(),
                        InfoId = c.Int(nullable: false),
                        NextQuestId = c.Long(),
                        IsDaily = c.Boolean(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.QuestId);

            CreateTable(
                    name: "dbo.MaintenanceLog",
                    columnsAction: c => new
                    {
                        LogId = c.Long(nullable: false, identity: true),
                        DateEnd = c.DateTime(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        Reason = c.String(maxLength: 255)
                    })
                .PrimaryKey(keyExpression: t => t.LogId);

            CreateTable(
                    name: "dbo.PartnerSkill",
                    columnsAction: c => new
                    {
                        PartnerSkillId = c.Long(nullable: false, identity: true),
                        EquipmentSerialId = c.Guid(nullable: false),
                        SkillVNum = c.Short(nullable: false),
                        Level = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.PartnerSkillId);

            CreateTable(
                    name: "dbo.QuestLog",
                    columnsAction: c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CharacterId = c.Long(nullable: false),
                        QuestId = c.Long(nullable: false),
                        IpAddress = c.String(),
                        LastDaily = c.DateTime()
                    })
                .PrimaryKey(keyExpression: t => t.Id);

            CreateTable(
                    name: "dbo.QuestObjective",
                    columnsAction: c => new
                    {
                        QuestObjectiveId = c.Int(nullable: false, identity: true),
                        QuestId = c.Int(nullable: false),
                        Data = c.Int(),
                        Objective = c.Int(),
                        SpecialData = c.Int(),
                        DropRate = c.Int(),
                        ObjectiveIndex = c.Byte(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.QuestObjectiveId);

            CreateTable(
                    name: "dbo.QuestReward",
                    columnsAction: c => new
                    {
                        QuestRewardId = c.Long(nullable: false, identity: true),
                        RewardType = c.Byte(nullable: false),
                        Data = c.Int(nullable: false),
                        Design = c.Byte(nullable: false),
                        Rarity = c.Byte(nullable: false),
                        Upgrade = c.Byte(nullable: false),
                        Amount = c.Int(nullable: false),
                        QuestId = c.Long(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.QuestRewardId);

            CreateTable(
                    name: "dbo.ShellEffect",
                    columnsAction: c => new
                    {
                        ShellEffectId = c.Long(nullable: false, identity: true),
                        Effect = c.Byte(nullable: false),
                        EffectLevel = c.Byte(nullable: false),
                        EquipmentSerialId = c.Guid(nullable: false),
                        Value = c.Short(nullable: false)
                    })
                .PrimaryKey(keyExpression: t => t.ShellEffectId);
        }

        public override void Down()
        {
            DropForeignKey(dependentTable: "dbo.CharacterQuest", dependentColumn: "QuestId",
                principalTable: "dbo.Quest");
            DropForeignKey(dependentTable: "dbo.PenaltyLog", dependentColumn: "AccountId",
                principalTable: "dbo.Account");
            DropForeignKey(dependentTable: "dbo.Character", dependentColumn: "AccountId",
                principalTable: "dbo.Account");
            DropForeignKey(dependentTable: "dbo.StaticBuff", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.StaticBonus", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.Respawn", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.QuicklistEntry", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.MinilandObject", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.MinigameLog", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.Mate", dependentColumn: "CharacterId", principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.Mail", dependentColumn: "ReceiverId", principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.Mail", dependentColumn: "SenderId", principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.ItemInstance", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.GeneralLog", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.GeneralLog", dependentColumn: "AccountId",
                principalTable: "dbo.Account");
            DropForeignKey(dependentTable: "dbo.FamilyCharacter", dependentColumn: "FamilyId",
                principalTable: "dbo.Family");
            DropForeignKey(dependentTable: "dbo.FamilyLog", dependentColumn: "FamilyId", principalTable: "dbo.Family");
            DropForeignKey(dependentTable: "dbo.FamilyCharacter", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.CharacterSkill", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.CharacterRelation", dependentColumn: "RelatedCharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.CharacterRelation", dependentColumn: "CharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.BazaarItem", dependentColumn: "ItemInstanceId",
                principalTable: "dbo.ItemInstance");
            DropForeignKey(dependentTable: "dbo.MinilandObject", dependentColumn: "ItemInstanceId",
                principalTable: "dbo.ItemInstance");
            DropForeignKey(dependentTable: "dbo.ShopItem", dependentColumn: "ItemVNum", principalTable: "dbo.Item");
            DropForeignKey(dependentTable: "dbo.RollGeneratedItem", dependentColumn: "OriginalItemVNum",
                principalTable: "dbo.Item");
            DropForeignKey(dependentTable: "dbo.RollGeneratedItem", dependentColumn: "ItemGeneratedVNum",
                principalTable: "dbo.Item");
            DropForeignKey(dependentTable: "dbo.RecipeItem", dependentColumn: "ItemVNum", principalTable: "dbo.Item");
            DropForeignKey(dependentTable: "dbo.Recipe", dependentColumn: "ItemVNum", principalTable: "dbo.Item");
            DropForeignKey(dependentTable: "dbo.Mail", dependentColumn: "AttachmentVNum", principalTable: "dbo.Item");
            DropForeignKey(dependentTable: "dbo.ItemInstance", dependentColumn: "ItemVNum", principalTable: "dbo.Item");
            DropForeignKey(dependentTable: "dbo.Drop", dependentColumn: "ItemVNum", principalTable: "dbo.Item");
            DropForeignKey(dependentTable: "dbo.BCard", dependentColumn: "SkillVNum", principalTable: "dbo.Skill");
            DropForeignKey(dependentTable: "dbo.BCard", dependentColumn: "NpcMonsterVNum",
                principalTable: "dbo.NpcMonster");
            DropForeignKey(dependentTable: "dbo.NpcMonsterSkill", dependentColumn: "NpcMonsterVNum",
                principalTable: "dbo.NpcMonster");
            DropForeignKey(dependentTable: "dbo.Mate", dependentColumn: "NpcMonsterVNum",
                principalTable: "dbo.NpcMonster");
            DropForeignKey(dependentTable: "dbo.MapNpc", dependentColumn: "NpcVNum", principalTable: "dbo.NpcMonster");
            DropForeignKey(dependentTable: "dbo.MapMonster", dependentColumn: "MonsterVNum",
                principalTable: "dbo.NpcMonster");
            DropForeignKey(dependentTable: "dbo.Drop", dependentColumn: "MonsterVNum",
                principalTable: "dbo.NpcMonster");
            DropForeignKey(dependentTable: "dbo.MapType", dependentColumn: "ReturnMapTypeId",
                principalTable: "dbo.RespawnMapType");
            DropForeignKey(dependentTable: "dbo.MapType", dependentColumn: "RespawnMapTypeId",
                principalTable: "dbo.RespawnMapType");
            DropForeignKey(dependentTable: "dbo.MapTypeMap", dependentColumn: "MapTypeId",
                principalTable: "dbo.MapType");
            DropForeignKey(dependentTable: "dbo.MapTypeMap", dependentColumn: "MapId", principalTable: "dbo.Map");
            DropForeignKey(dependentTable: "dbo.Teleporter", dependentColumn: "MapId", principalTable: "dbo.Map");
            DropForeignKey(dependentTable: "dbo.ScriptedInstance", dependentColumn: "MapId", principalTable: "dbo.Map");
            DropForeignKey(dependentTable: "dbo.Respawn", dependentColumn: "RespawnMapTypeId",
                principalTable: "dbo.RespawnMapType");
            DropForeignKey(dependentTable: "dbo.RespawnMapType", dependentColumn: "DefaultMapId",
                principalTable: "dbo.Map");
            DropForeignKey(dependentTable: "dbo.Respawn", dependentColumn: "MapId", principalTable: "dbo.Map");
            DropForeignKey(dependentTable: "dbo.Portal", dependentColumn: "SourceMapId", principalTable: "dbo.Map");
            DropForeignKey(dependentTable: "dbo.Portal", dependentColumn: "DestinationMapId",
                principalTable: "dbo.Map");
            DropForeignKey(dependentTable: "dbo.MapNpc", dependentColumn: "MapId", principalTable: "dbo.Map");
            DropForeignKey(dependentTable: "dbo.Teleporter", dependentColumn: "MapNpcId", principalTable: "dbo.MapNpc");
            DropForeignKey(dependentTable: "dbo.Shop", dependentColumn: "MapNpcId", principalTable: "dbo.MapNpc");
            DropForeignKey(dependentTable: "dbo.ShopSkill", dependentColumn: "ShopId", principalTable: "dbo.Shop");
            DropForeignKey(dependentTable: "dbo.ShopSkill", dependentColumn: "SkillVNum", principalTable: "dbo.Skill");
            DropForeignKey(dependentTable: "dbo.NpcMonsterSkill", dependentColumn: "SkillVNum",
                principalTable: "dbo.Skill");
            DropForeignKey(dependentTable: "dbo.Combo", dependentColumn: "SkillVNum", principalTable: "dbo.Skill");
            DropForeignKey(dependentTable: "dbo.CharacterSkill", dependentColumn: "SkillVNum",
                principalTable: "dbo.Skill");
            DropForeignKey(dependentTable: "dbo.ShopItem", dependentColumn: "ShopId", principalTable: "dbo.Shop");
            DropForeignKey(dependentTable: "dbo.RecipeList", dependentColumn: "RecipeId", principalTable: "dbo.Recipe");
            DropForeignKey(dependentTable: "dbo.RecipeItem", dependentColumn: "RecipeId", principalTable: "dbo.Recipe");
            DropForeignKey(dependentTable: "dbo.RecipeList", dependentColumn: "MapNpcId", principalTable: "dbo.MapNpc");
            DropForeignKey(dependentTable: "dbo.RecipeList", dependentColumn: "ItemVNum", principalTable: "dbo.Item");
            DropForeignKey(dependentTable: "dbo.MapMonster", dependentColumn: "MapId", principalTable: "dbo.Map");
            DropForeignKey(dependentTable: "dbo.Character", dependentColumn: "MapId", principalTable: "dbo.Map");
            DropForeignKey(dependentTable: "dbo.Drop", dependentColumn: "MapTypeId", principalTable: "dbo.MapType");
            DropForeignKey(dependentTable: "dbo.BCard", dependentColumn: "ItemVNum", principalTable: "dbo.Item");
            DropForeignKey(dependentTable: "dbo.BCard", dependentColumn: "CardId", principalTable: "dbo.Card");
            DropForeignKey(dependentTable: "dbo.StaticBuff", dependentColumn: "CardId", principalTable: "dbo.Card");
            DropForeignKey(dependentTable: "dbo.ItemInstance", dependentColumn: "BoundCharacterId",
                principalTable: "dbo.Character");
            DropForeignKey(dependentTable: "dbo.BazaarItem", dependentColumn: "SellerId",
                principalTable: "dbo.Character");
            DropIndex(table: "dbo.CharacterQuest", columns: new[] { "QuestId" });
            DropIndex(table: "dbo.PenaltyLog", columns: new[] { "AccountId" });
            DropIndex(table: "dbo.StaticBonus", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.QuicklistEntry", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.MinigameLog", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.GeneralLog", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.GeneralLog", columns: new[] { "AccountId" });
            DropIndex(table: "dbo.FamilyLog", columns: new[] { "FamilyId" });
            DropIndex(table: "dbo.FamilyCharacter", columns: new[] { "FamilyId" });
            DropIndex(table: "dbo.FamilyCharacter", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.CharacterRelation", columns: new[] { "RelatedCharacterId" });
            DropIndex(table: "dbo.CharacterRelation", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.MinilandObject", columns: new[] { "ItemInstanceId" });
            DropIndex(table: "dbo.MinilandObject", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.RollGeneratedItem", columns: new[] { "OriginalItemVNum" });
            DropIndex(table: "dbo.RollGeneratedItem", columns: new[] { "ItemGeneratedVNum" });
            DropIndex(table: "dbo.Mail", columns: new[] { "SenderId" });
            DropIndex(table: "dbo.Mail", columns: new[] { "ReceiverId" });
            DropIndex(table: "dbo.Mail", columns: new[] { "AttachmentVNum" });
            DropIndex(table: "dbo.Mate", columns: new[] { "NpcMonsterVNum" });
            DropIndex(table: "dbo.Mate", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.ScriptedInstance", columns: new[] { "MapId" });
            DropIndex(table: "dbo.RespawnMapType", columns: new[] { "DefaultMapId" });
            DropIndex(table: "dbo.Respawn", columns: new[] { "RespawnMapTypeId" });
            DropIndex(table: "dbo.Respawn", columns: new[] { "MapId" });
            DropIndex(table: "dbo.Respawn", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.Portal", columns: new[] { "SourceMapId" });
            DropIndex(table: "dbo.Portal", columns: new[] { "DestinationMapId" });
            DropIndex(table: "dbo.Teleporter", columns: new[] { "MapNpcId" });
            DropIndex(table: "dbo.Teleporter", columns: new[] { "MapId" });
            DropIndex(table: "dbo.NpcMonsterSkill", columns: new[] { "SkillVNum" });
            DropIndex(table: "dbo.NpcMonsterSkill", columns: new[] { "NpcMonsterVNum" });
            DropIndex(table: "dbo.Combo", columns: new[] { "SkillVNum" });
            DropIndex(table: "dbo.CharacterSkill", columns: new[] { "SkillVNum" });
            DropIndex(table: "dbo.CharacterSkill", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.ShopSkill", columns: new[] { "SkillVNum" });
            DropIndex(table: "dbo.ShopSkill", columns: new[] { "ShopId" });
            DropIndex(table: "dbo.ShopItem", columns: new[] { "ShopId" });
            DropIndex(table: "dbo.ShopItem", columns: new[] { "ItemVNum" });
            DropIndex(table: "dbo.Shop", columns: new[] { "MapNpcId" });
            DropIndex(table: "dbo.RecipeItem", columns: new[] { "RecipeId" });
            DropIndex(table: "dbo.RecipeItem", columns: new[] { "ItemVNum" });
            DropIndex(table: "dbo.Recipe", columns: new[] { "ItemVNum" });
            DropIndex(table: "dbo.RecipeList", columns: new[] { "RecipeId" });
            DropIndex(table: "dbo.RecipeList", columns: new[] { "MapNpcId" });
            DropIndex(table: "dbo.RecipeList", columns: new[] { "ItemVNum" });
            DropIndex(table: "dbo.MapNpc", columns: new[] { "NpcVNum" });
            DropIndex(table: "dbo.MapNpc", columns: new[] { "MapId" });
            DropIndex(table: "dbo.MapMonster", columns: new[] { "MonsterVNum" });
            DropIndex(table: "dbo.MapMonster", columns: new[] { "MapId" });
            DropIndex(table: "dbo.MapTypeMap", columns: new[] { "MapTypeId" });
            DropIndex(table: "dbo.MapTypeMap", columns: new[] { "MapId" });
            DropIndex(table: "dbo.MapType", columns: new[] { "ReturnMapTypeId" });
            DropIndex(table: "dbo.MapType", columns: new[] { "RespawnMapTypeId" });
            DropIndex(table: "dbo.Drop", columns: new[] { "MonsterVNum" });
            DropIndex(table: "dbo.Drop", columns: new[] { "MapTypeId" });
            DropIndex(table: "dbo.Drop", columns: new[] { "ItemVNum" });
            DropIndex(table: "dbo.StaticBuff", columns: new[] { "CharacterId" });
            DropIndex(table: "dbo.StaticBuff", columns: new[] { "CardId" });
            DropIndex(table: "dbo.BCard", columns: new[] { "SkillVNum" });
            DropIndex(table: "dbo.BCard", columns: new[] { "NpcMonsterVNum" });
            DropIndex(table: "dbo.BCard", columns: new[] { "ItemVNum" });
            DropIndex(table: "dbo.BCard", columns: new[] { "CardId" });
            DropIndex(table: "dbo.ItemInstance", columns: new[] { "ItemVNum" });
            DropIndex(table: "dbo.ItemInstance", name: "IX_SlotAndType");
            DropIndex(table: "dbo.ItemInstance", columns: new[] { "BoundCharacterId" });
            DropIndex(table: "dbo.BazaarItem", columns: new[] { "SellerId" });
            DropIndex(table: "dbo.BazaarItem", columns: new[] { "ItemInstanceId" });
            DropIndex(table: "dbo.Character", columns: new[] { "MapId" });
            DropIndex(table: "dbo.Character", columns: new[] { "AccountId" });
            DropTable(name: "dbo.ShellEffect");
            DropTable(name: "dbo.QuestReward");
            DropTable(name: "dbo.QuestObjective");
            DropTable(name: "dbo.QuestLog");
            DropTable(name: "dbo.PartnerSkill");
            DropTable(name: "dbo.MaintenanceLog");
            DropTable(name: "dbo.Quest");
            DropTable(name: "dbo.CharacterQuest");
            DropTable(name: "dbo.CellonOption");
            DropTable(name: "dbo.PenaltyLog");
            DropTable(name: "dbo.StaticBonus");
            DropTable(name: "dbo.QuicklistEntry");
            DropTable(name: "dbo.MinigameLog");
            DropTable(name: "dbo.GeneralLog");
            DropTable(name: "dbo.FamilyLog");
            DropTable(name: "dbo.Family");
            DropTable(name: "dbo.FamilyCharacter");
            DropTable(name: "dbo.CharacterRelation");
            DropTable(name: "dbo.MinilandObject");
            DropTable(name: "dbo.RollGeneratedItem");
            DropTable(name: "dbo.Mail");
            DropTable(name: "dbo.Mate");
            DropTable(name: "dbo.ScriptedInstance");
            DropTable(name: "dbo.RespawnMapType");
            DropTable(name: "dbo.Respawn");
            DropTable(name: "dbo.Portal");
            DropTable(name: "dbo.Teleporter");
            DropTable(name: "dbo.NpcMonsterSkill");
            DropTable(name: "dbo.Combo");
            DropTable(name: "dbo.CharacterSkill");
            DropTable(name: "dbo.Skill");
            DropTable(name: "dbo.ShopSkill");
            DropTable(name: "dbo.ShopItem");
            DropTable(name: "dbo.Shop");
            DropTable(name: "dbo.RecipeItem");
            DropTable(name: "dbo.Recipe");
            DropTable(name: "dbo.RecipeList");
            DropTable(name: "dbo.MapNpc");
            DropTable(name: "dbo.MapMonster");
            DropTable(name: "dbo.Map");
            DropTable(name: "dbo.MapTypeMap");
            DropTable(name: "dbo.MapType");
            DropTable(name: "dbo.Drop");
            DropTable(name: "dbo.NpcMonster");
            DropTable(name: "dbo.StaticBuff");
            DropTable(name: "dbo.Card");
            DropTable(name: "dbo.BCard");
            DropTable(name: "dbo.Item");
            DropTable(name: "dbo.ItemInstance");
            DropTable(name: "dbo.BazaarItem");
            DropTable(name: "dbo.Character");
            DropTable(name: "dbo.Account");
        }
    }
}