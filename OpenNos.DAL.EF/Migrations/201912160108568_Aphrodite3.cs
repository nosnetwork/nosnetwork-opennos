using System.Data.Entity.Migrations;

namespace OpenNos.DAL.EF.Migrations
{
    public partial class Aphrodite3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn(table: "dbo.NpcMonster", name: "DarkResistance", columnAction: c => c.Int(nullable: false));
            AlterColumn(table: "dbo.NpcMonster", name: "FireResistance", columnAction: c => c.Int(nullable: false));
            AlterColumn(table: "dbo.NpcMonster", name: "LightResistance", columnAction: c => c.Int(nullable: false));
            AlterColumn(table: "dbo.NpcMonster", name: "WaterResistance", columnAction: c => c.Int(nullable: false));
        }

        public override void Down()
        {
            AlterColumn(table: "dbo.NpcMonster", name: "WaterResistance", columnAction: c => c.Short(nullable: false));
            AlterColumn(table: "dbo.NpcMonster", name: "LightResistance", columnAction: c => c.Short(nullable: false));
            AlterColumn(table: "dbo.NpcMonster", name: "FireResistance", columnAction: c => c.Short(nullable: false));
            AlterColumn(table: "dbo.NpcMonster", name: "DarkResistance", columnAction: c => c.Short(nullable: false));
        }
    }
}