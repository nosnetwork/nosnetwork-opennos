using System.Data.Entity.Migrations;

namespace OpenNos.DAL.EF.Migrations
{
    public partial class QuestTimeSpaceId : DbMigration
    {
        public override void Up()
        {
            AddColumn(table: "dbo.ScriptedInstance", name: "QuestTimeSpaceId",
                columnAction: c => c.Int(nullable: false));
        }

        public override void Down()
        {
            DropColumn(table: "dbo.ScriptedInstance", name: "QuestTimeSpaceId");
        }
    }
}