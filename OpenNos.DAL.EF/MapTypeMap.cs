﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OpenNos.DAL.EF
{
    public sealed class MapTypeMap
    {
        #region Properties

        public Map Map { get; set; }

        [Key] [Column(Order = 0)] public short MapId { get; set; }

        public MapType MapType { get; set; }

        [Key] [Column(Order = 1)] public short MapTypeId { get; set; }

        #endregion
    }
}