using OpenNos.DAL.EF.Base;

namespace OpenNos.DAL.EF
{
    public sealed class QuicklistEntry : SynchronizableBaseEntity
    {
        #region Properties

        public Character Character { get; set; }

        public long CharacterId { get; set; }

        public short Morph { get; set; }

        public short Pos { get; set; }

        public short Q1 { get; set; }

        public short Q2 { get; set; }

        public short Slot { get; set; }

        public short Type { get; set; }

        #endregion
    }
}