using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace OpenNos.DAL.EF.Context
{
    public class OpenNosContext : DbContext
    {
        #region Instantiation

        public OpenNosContext() : base(nameOrConnectionString: "OpenNosContext")
        {
            Configuration.LazyLoadingEnabled = true;

            // --DO NOT DISABLE, otherwise the mapping will fail only one time access to database so
            // no proxy generation needed, its just slowing down in our case
            Configuration.ProxyCreationEnabled = false;
        }

        #endregion

        #region Methods

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // remove automatic pluralization
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Account>()
                .Property(propertyExpression: e => e.Password)
                .IsUnicode(unicode: false);

            modelBuilder.Entity<Account>()
                .HasMany(navigationPropertyExpression: e => e.Character)
                .WithRequired(navigationPropertyExpression: e => e.Account)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Account>()
                .HasMany(navigationPropertyExpression: e => e.PenaltyLog)
                .WithRequired(navigationPropertyExpression: e => e.Account)
                .HasForeignKey(foreignKeyExpression: e => e.AccountId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .Property(propertyExpression: e => e.Name)
                .IsUnicode(unicode: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.Inventory)
                .WithRequired(navigationPropertyExpression: e => e.Character)
                .HasForeignKey(foreignKeyExpression: e => e.CharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.Mate)
                .WithRequired(navigationPropertyExpression: e => e.Character)
                .HasForeignKey(foreignKeyExpression: e => e.CharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.CharacterSkill)
                .WithRequired(navigationPropertyExpression: e => e.Character)
                .HasForeignKey(foreignKeyExpression: e => e.CharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.StaticBonus)
                .WithRequired(navigationPropertyExpression: e => e.Character)
                .HasForeignKey(foreignKeyExpression: e => e.CharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.CharacterRelation1)
                .WithRequired(navigationPropertyExpression: e => e.Character1)
                .HasForeignKey(foreignKeyExpression: e => e.CharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.CharacterRelation2)
                .WithRequired(navigationPropertyExpression: e => e.Character2)
                .HasForeignKey(foreignKeyExpression: e => e.RelatedCharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.StaticBuff)
                .WithRequired(navigationPropertyExpression: e => e.Character)
                .HasForeignKey(foreignKeyExpression: e => e.CharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.MinigameLog)
                .WithRequired(navigationPropertyExpression: e => e.Character)
                .HasForeignKey(foreignKeyExpression: e => e.CharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Card>()
                .HasMany(navigationPropertyExpression: e => e.StaticBuff)
                .WithRequired(navigationPropertyExpression: e => e.Card)
                .HasForeignKey(foreignKeyExpression: e => e.CardId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.QuicklistEntry)
                .WithRequired(navigationPropertyExpression: e => e.Character)
                .HasForeignKey(foreignKeyExpression: e => e.CharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.Respawn)
                .WithRequired(navigationPropertyExpression: e => e.Character)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.Mail)
                .WithRequired(navigationPropertyExpression: e => e.Sender)
                .HasForeignKey(foreignKeyExpression: e => e.SenderId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.MinilandObject)
                .WithRequired(navigationPropertyExpression: e => e.Character)
                .HasForeignKey(foreignKeyExpression: e => e.CharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Character>()
                .HasMany(navigationPropertyExpression: e => e.Mail1)
                .WithRequired(navigationPropertyExpression: e => e.Receiver)
                .HasForeignKey(foreignKeyExpression: e => e.ReceiverId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Family>()
                .HasMany(navigationPropertyExpression: e => e.FamilyLogs)
                .WithRequired(navigationPropertyExpression: e => e.Family)
                .HasForeignKey(foreignKeyExpression: e => e.FamilyId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<FamilyCharacter>()
                .HasRequired(navigationPropertyExpression: e => e.Character)
                .WithMany(navigationPropertyExpression: e => e.FamilyCharacter)
                .HasForeignKey(foreignKeyExpression: e => e.CharacterId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<BazaarItem>()
                .HasRequired(navigationPropertyExpression: e => e.Character)
                .WithMany(navigationPropertyExpression: e => e.BazaarItem)
                .HasForeignKey(foreignKeyExpression: e => e.SellerId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<BazaarItem>()
                .HasRequired(navigationPropertyExpression: e => e.ItemInstance)
                .WithMany(navigationPropertyExpression: e => e.BazaarItem)
                .HasForeignKey(foreignKeyExpression: e => e.ItemInstanceId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<MinilandObject>()
                .HasOptional(navigationPropertyExpression: e => e.ItemInstance)
                .WithMany(navigationPropertyExpression: e => e.MinilandObject)
                .HasForeignKey(foreignKeyExpression: e => e.ItemInstanceId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<FamilyCharacter>()
                .HasRequired(navigationPropertyExpression: e => e.Family)
                .WithMany(navigationPropertyExpression: e => e.FamilyCharacters)
                .HasForeignKey(foreignKeyExpression: e => e.FamilyId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Item>()
                .HasMany(navigationPropertyExpression: e => e.Drop)
                .WithRequired(navigationPropertyExpression: e => e.Item)
                .HasForeignKey(foreignKeyExpression: e => e.ItemVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Item>()
                .HasMany(navigationPropertyExpression: e => e.Recipe)
                .WithRequired(navigationPropertyExpression: e => e.Item)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Item>()
                .HasMany(navigationPropertyExpression: e => e.RecipeItem)
                .WithRequired(navigationPropertyExpression: e => e.Item)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Item>()
                .HasMany(navigationPropertyExpression: e => e.ItemInstances)
                .WithRequired(navigationPropertyExpression: e => e.Item)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Item>()
                .HasMany(navigationPropertyExpression: e => e.ShopItem)
                .WithRequired(navigationPropertyExpression: e => e.Item)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Mail>()
                .HasOptional(navigationPropertyExpression: e => e.Item)
                .WithMany(navigationPropertyExpression: e => e.Mail)
                .HasForeignKey(foreignKeyExpression: e => e.AttachmentVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<RollGeneratedItem>()
                .HasRequired(navigationPropertyExpression: e => e.OriginalItem)
                .WithMany(navigationPropertyExpression: e => e.RollGeneratedItem)
                .HasForeignKey(foreignKeyExpression: e => e.OriginalItemVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<RollGeneratedItem>()
                .HasRequired(navigationPropertyExpression: e => e.ItemGenerated)
                .WithMany(navigationPropertyExpression: e => e.RollGeneratedItem2)
                .HasForeignKey(foreignKeyExpression: e => e.ItemGeneratedVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Map>()
                .HasMany(navigationPropertyExpression: e => e.Character)
                .WithRequired(navigationPropertyExpression: e => e.Map)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Map>()
                .HasMany(navigationPropertyExpression: e => e.MapMonster)
                .WithRequired(navigationPropertyExpression: e => e.Map)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Respawn>()
                .HasRequired(navigationPropertyExpression: e => e.Map)
                .WithMany(navigationPropertyExpression: e => e.Respawn)
                .HasForeignKey(foreignKeyExpression: e => e.MapId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Respawn>()
                .HasRequired(navigationPropertyExpression: e => e.RespawnMapType)
                .WithMany(navigationPropertyExpression: e => e.Respawn)
                .HasForeignKey(foreignKeyExpression: e => e.RespawnMapTypeId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<RespawnMapType>()
                .HasRequired(navigationPropertyExpression: e => e.Map)
                .WithMany(navigationPropertyExpression: e => e.RespawnMapType)
                .HasForeignKey(foreignKeyExpression: e => e.DefaultMapId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<MapType>()
                .HasOptional(navigationPropertyExpression: e => e.RespawnMapType)
                .WithMany(navigationPropertyExpression: e => e.MapTypes)
                .HasForeignKey(foreignKeyExpression: e => e.RespawnMapTypeId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<MapType>()
                .HasOptional(navigationPropertyExpression: e => e.ReturnMapType)
                .WithMany(navigationPropertyExpression: e => e.MapTypes1)
                .HasForeignKey(foreignKeyExpression: e => e.ReturnMapTypeId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Map>()
                .HasMany(navigationPropertyExpression: e => e.MapNpc)
                .WithRequired(navigationPropertyExpression: e => e.Map)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Map>()
                .HasMany(navigationPropertyExpression: e => e.Portal)
                .WithRequired(navigationPropertyExpression: e => e.Map)
                .HasForeignKey(foreignKeyExpression: e => e.DestinationMapId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Map>()
                .HasMany(navigationPropertyExpression: e => e.Portal1)
                .WithRequired(navigationPropertyExpression: e => e.Map1)
                .HasForeignKey(foreignKeyExpression: e => e.SourceMapId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Map>()
                .HasMany(navigationPropertyExpression: e => e.ScriptedInstance)
                .WithRequired(navigationPropertyExpression: e => e.Map)
                .HasForeignKey(foreignKeyExpression: e => e.MapId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Map>()
                .HasMany(navigationPropertyExpression: e => e.Teleporter)
                .WithRequired(navigationPropertyExpression: e => e.Map)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<BCard>()
                .HasOptional(navigationPropertyExpression: e => e.Skill)
                .WithMany(navigationPropertyExpression: e => e.BCards)
                .HasForeignKey(foreignKeyExpression: e => e.SkillVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<BCard>()
                .HasOptional(navigationPropertyExpression: e => e.NpcMonster)
                .WithMany(navigationPropertyExpression: e => e.BCards)
                .HasForeignKey(foreignKeyExpression: e => e.NpcMonsterVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<BCard>()
                .HasOptional(navigationPropertyExpression: e => e.Card)
                .WithMany(navigationPropertyExpression: e => e.BCards)
                .HasForeignKey(foreignKeyExpression: e => e.CardId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<BCard>()
                .HasOptional(navigationPropertyExpression: e => e.Item)
                .WithMany(navigationPropertyExpression: e => e.BCards)
                .HasForeignKey(foreignKeyExpression: e => e.ItemVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<MapTypeMap>()
                .HasRequired(navigationPropertyExpression: e => e.Map)
                .WithMany(navigationPropertyExpression: e => e.MapTypeMap)
                .HasForeignKey(foreignKeyExpression: e => e.MapId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<MapTypeMap>()
                .HasRequired(navigationPropertyExpression: e => e.MapType)
                .WithMany(navigationPropertyExpression: e => e.MapTypeMap)
                .HasForeignKey(foreignKeyExpression: e => e.MapTypeId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<MapType>()
                .HasMany(navigationPropertyExpression: e => e.Drops)
                .WithOptional(navigationPropertyExpression: e => e.MapType)
                .HasForeignKey(foreignKeyExpression: e => e.MapTypeId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<MapNpc>()
                .HasMany(navigationPropertyExpression: e => e.Shop)
                .WithRequired(navigationPropertyExpression: e => e.MapNpc)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<MapNpc>()
                .HasMany(navigationPropertyExpression: e => e.Teleporter)
                .WithRequired(navigationPropertyExpression: e => e.MapNpc)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<RecipeList>()
                .HasOptional(navigationPropertyExpression: e => e.Item)
                .WithMany(navigationPropertyExpression: e => e.RecipeList)
                .HasForeignKey(foreignKeyExpression: e => e.ItemVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<RecipeList>()
                .HasOptional(navigationPropertyExpression: e => e.MapNpc)
                .WithMany(navigationPropertyExpression: e => e.RecipeList)
                .HasForeignKey(foreignKeyExpression: e => e.MapNpcId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<RecipeList>()
                .HasRequired(navigationPropertyExpression: e => e.Recipe)
                .WithMany(navigationPropertyExpression: e => e.RecipeList)
                .HasForeignKey(foreignKeyExpression: e => e.RecipeId)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<NpcMonster>()
                .HasMany(navigationPropertyExpression: e => e.Drop)
                .WithOptional(navigationPropertyExpression: e => e.NpcMonster)
                .HasForeignKey(foreignKeyExpression: e => e.MonsterVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<NpcMonster>()
                .HasMany(navigationPropertyExpression: e => e.Mate)
                .WithRequired(navigationPropertyExpression: e => e.NpcMonster)
                .HasForeignKey(foreignKeyExpression: e => e.NpcMonsterVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<NpcMonster>()
                .HasMany(navigationPropertyExpression: e => e.MapMonster)
                .WithRequired(navigationPropertyExpression: e => e.NpcMonster)
                .HasForeignKey(foreignKeyExpression: e => e.MonsterVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<NpcMonster>()
                .HasMany(navigationPropertyExpression: e => e.MapNpc)
                .WithRequired(navigationPropertyExpression: e => e.NpcMonster)
                .HasForeignKey(foreignKeyExpression: e => e.NpcVNum)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<NpcMonster>()
                .HasMany(navigationPropertyExpression: e => e.NpcMonsterSkill)
                .WithRequired(navigationPropertyExpression: e => e.NpcMonster)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Recipe>()
                .HasMany(navigationPropertyExpression: e => e.RecipeItem)
                .WithRequired(navigationPropertyExpression: e => e.Recipe)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Shop>()
                .HasMany(navigationPropertyExpression: e => e.ShopItem)
                .WithRequired(navigationPropertyExpression: e => e.Shop)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Shop>()
                .HasMany(navigationPropertyExpression: e => e.ShopSkill)
                .WithRequired(navigationPropertyExpression: e => e.Shop)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Skill>()
                .HasMany(navigationPropertyExpression: e => e.CharacterSkill)
                .WithRequired(navigationPropertyExpression: e => e.Skill)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Skill>()
                .HasMany(navigationPropertyExpression: e => e.Combo)
                .WithRequired(navigationPropertyExpression: e => e.Skill)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Skill>()
                .HasMany(navigationPropertyExpression: e => e.NpcMonsterSkill)
                .WithRequired(navigationPropertyExpression: e => e.Skill)
                .WillCascadeOnDelete(value: false);

            modelBuilder.Entity<Skill>()
                .HasMany(navigationPropertyExpression: e => e.ShopSkill)
                .WithRequired(navigationPropertyExpression: e => e.Skill)
                .WillCascadeOnDelete(value: false);
        }

        #endregion

        #region Properties

        public virtual DbSet<Account> Account { get; set; }

        public virtual DbSet<BazaarItem> BazaarItem { get; set; }

        public virtual DbSet<BCard> BCard { get; set; }

        public virtual DbSet<Card> Card { get; set; }

        public virtual DbSet<CellonOption> CellonOption { get; set; }

        public virtual DbSet<Character> Character { get; set; }

        public virtual DbSet<CharacterRelation> CharacterRelation { get; set; }

        public virtual DbSet<CharacterSkill> CharacterSkill { get; set; }

        public virtual DbSet<CharacterQuest> CharacterQuest { get; set; }

        public virtual DbSet<Combo> Combo { get; set; }

        public virtual DbSet<Drop> Drop { get; set; }

        public virtual DbSet<Family> Family { get; set; }

        public virtual DbSet<FamilyCharacter> FamilyCharacter { get; set; }

        public virtual DbSet<FamilyLog> FamilyLog { get; set; }

        public virtual DbSet<GeneralLog> GeneralLog { get; set; }

        public virtual DbSet<Item> Item { get; set; }

        public virtual DbSet<ItemInstance> ItemInstance { get; set; }

        public virtual DbSet<Mail> Mail { get; set; }

        public virtual DbSet<MaintenanceLog> MaintenanceLog { get; set; }

        public virtual DbSet<Map> Map { get; set; }

        public virtual DbSet<MapMonster> MapMonster { get; set; }

        public virtual DbSet<MapNpc> MapNpc { get; set; }

        public virtual DbSet<MapType> MapType { get; set; }

        public virtual DbSet<MapTypeMap> MapTypeMap { get; set; }

        public virtual DbSet<Mate> Mate { get; set; }

        public virtual DbSet<MinigameLog> MinigameLog { get; set; }

        public virtual DbSet<MinilandObject> MinilandObject { get; set; }

        public virtual DbSet<NpcMonster> NpcMonster { get; set; }

        public virtual DbSet<NpcMonsterSkill> NpcMonsterSkill { get; set; }

        public virtual DbSet<PartnerSkill> PartnerSkill { get; set; }

        public virtual DbSet<PenaltyLog> PenaltyLog { get; set; }

        public virtual DbSet<Portal> Portal { get; set; }

        public virtual DbSet<Quest> Quest { get; set; }

        public virtual DbSet<QuestLog> QuestLog { get; set; }

        public virtual DbSet<QuestObjective> QuestObjective { get; set; }

        public virtual DbSet<QuestReward> QuestReward { get; set; }

        public virtual DbSet<QuicklistEntry> QuicklistEntry { get; set; }

        public virtual DbSet<Recipe> Recipe { get; set; }

        public virtual DbSet<RecipeItem> RecipeItem { get; set; }

        public virtual DbSet<RecipeList> RecipeList { get; set; }

        public virtual DbSet<Respawn> Respawn { get; set; }

        public virtual DbSet<RespawnMapType> RespawnMapType { get; set; }

        public virtual DbSet<RollGeneratedItem> RollGeneratedItem { get; set; }

        public virtual DbSet<ScriptedInstance> ScriptedInstance { get; set; }

        public virtual DbSet<ShellEffect> ShellEffect { get; set; }

        public virtual DbSet<Shop> Shop { get; set; }

        public virtual DbSet<ShopItem> ShopItem { get; set; }

        public virtual DbSet<ShopSkill> ShopSkill { get; set; }

        public virtual DbSet<Skill> Skill { get; set; }

        public virtual DbSet<StaticBonus> StaticBonus { get; set; }

        public virtual DbSet<StaticBuff> StaticBuff { get; set; }

        public virtual DbSet<Teleporter> Teleporter { get; set; }

        #endregion
    }
}