﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OpenNos.DAL.EF
{
    public sealed class RespawnMapType
    {
        #region Instantiation

        public RespawnMapType()
        {
            Respawn = new HashSet<Respawn>();
            MapTypes = new HashSet<MapType>();
            MapTypes1 = new HashSet<MapType>();
        }

        #endregion

        #region Properties

        public short DefaultMapId { get; set; }

        public short DefaultX { get; set; }

        public short DefaultY { get; set; }

        public Map Map { get; set; }

        public ICollection<MapType> MapTypes { get; set; }

        public ICollection<MapType> MapTypes1 { get; set; }

        [MaxLength(length: 255)] public string Name { get; set; }

        public ICollection<Respawn> Respawn { get; set; }

        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.None)]
        public long RespawnMapTypeId { get; set; }

        #endregion
    }
}