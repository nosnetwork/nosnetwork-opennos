﻿using System;
using OpenNos.Domain;

namespace OpenNos.DAL.EF
{
    public sealed class StaticBonus
    {
        #region Properties

        public Character Character { get; set; }

        public long CharacterId { get; set; }

        public DateTime DateEnd { get; set; }

        public long StaticBonusId { get; set; }

        public StaticBonusType StaticBonusType { get; set; }

        #endregion
    }
}