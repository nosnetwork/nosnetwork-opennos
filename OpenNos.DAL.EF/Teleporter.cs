using OpenNos.Domain;

namespace OpenNos.DAL.EF
{
    public sealed class Teleporter
    {
        #region Properties

        public short Index { get; set; }

        public TeleporterType Type { get; set; }

        public Map Map { get; set; }

        public short MapId { get; set; }

        public MapNpc MapNpc { get; set; }

        public int MapNpcId { get; set; }

        public short MapX { get; set; }

        public short MapY { get; set; }

        public short TeleporterId { get; set; }

        #endregion
    }
}