using System.ComponentModel.DataAnnotations.Schema;

namespace OpenNos.DAL.EF
{
    public sealed class MapMonster
    {
        #region Properties

        public bool IsDisabled { get; set; }

        public bool IsMoving { get; set; }

        public Map Map { get; set; }

        public short MapId { get; set; }

        [DatabaseGenerated(databaseGeneratedOption: DatabaseGeneratedOption.None)]
        public int MapMonsterId { get; set; }

        public short MapX { get; set; }

        public short MapY { get; set; }

        public short MonsterVNum { get; set; }

        public string Name { get; set; }

        public NpcMonster NpcMonster { get; set; }

        public byte Position { get; set; }

        #endregion
    }
}