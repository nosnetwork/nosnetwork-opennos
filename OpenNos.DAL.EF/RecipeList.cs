﻿namespace OpenNos.DAL.EF
{
    public sealed class RecipeList
    {
        #region Properties

        public Item Item { get; set; }

        public short? ItemVNum { get; set; }

        public MapNpc MapNpc { get; set; }

        public int? MapNpcId { get; set; }

        public Recipe Recipe { get; set; }

        public short RecipeId { get; set; }

        public int RecipeListId { get; set; }

        #endregion
    }
}