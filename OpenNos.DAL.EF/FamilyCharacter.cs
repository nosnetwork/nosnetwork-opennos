﻿using System.ComponentModel.DataAnnotations;
using OpenNos.Domain;

namespace OpenNos.DAL.EF
{
    public sealed class FamilyCharacter
    {
        #region Properties

        public FamilyAuthority Authority { get; set; }

        public Character Character { get; set; }

        public long CharacterId { get; set; }

        [MaxLength(length: 255)] public string DailyMessage { get; set; }

        public int Experience { get; set; }

        public Family Family { get; set; }

        public long FamilyCharacterId { get; set; }

        public long FamilyId { get; set; }

        public FamilyMemberRank Rank { get; set; }

        #endregion
    }
}