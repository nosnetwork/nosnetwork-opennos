﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface ICharacterDao
    {
        #region Methods

        DeleteResult DeleteByPrimaryKey(long accountId, byte characterSlot);

        List<CharacterDto> GetTopCompliment();

        List<CharacterDto> GetTopPoints();

        List<CharacterDto> GetTopReputation();

        SaveResult InsertOrUpdate(ref CharacterDto character);

        IEnumerable<CharacterDto> LoadAll();

        IEnumerable<CharacterDto> LoadAllByAccount(long accountId);

        IEnumerable<CharacterDto> LoadByAccount(long accountId);

        CharacterDto LoadById(long characterId);

        CharacterDto LoadByName(string name);

        CharacterDto LoadBySlot(long accountId, byte slot);

        #endregion
    }
}