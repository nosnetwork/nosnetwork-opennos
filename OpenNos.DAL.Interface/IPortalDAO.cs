﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IPortalDao
    {
        #region Methods

        PortalDto Insert(PortalDto portal);

        void Insert(List<PortalDto> portals);

        IEnumerable<PortalDto> LoadByMap(short mapId);

        #endregion
    }
}