﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IComboDao
    {
        #region Methods

        ComboDto Insert(ComboDto combo);

        void Insert(List<ComboDto> combos);

        IEnumerable<ComboDto> LoadAll();

        ComboDto LoadById(short comboId);

        IEnumerable<ComboDto> LoadBySkillVnum(short skillVNum);

        IEnumerable<ComboDto> LoadByVNumHitAndEffect(short skillVNum, short hit, short effect);

        #endregion
    }
}