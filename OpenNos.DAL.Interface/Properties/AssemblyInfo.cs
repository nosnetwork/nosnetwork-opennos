﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following set of attributes.
// Change these attribute values to modify the information associated with an assembly.
[assembly: AssemblyDescription(description: "Open source nos-emulation project")]
[assembly: AssemblyCompany(company: "OpenNos Team")]
[assembly: AssemblyCopyright(copyright: "OpenNos Team Copyright © 2017")]
[assembly: AssemblyProduct(product: "OpenNos.DAL.Interface")]

// Version information for an assembly consists of the following four values:
//
// Major Version Minor Version Build Number Revision
//
// You can specify all the values or you can default the Build and Revision Numbers by using the '*'
// as shown below: [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(version: "1.0.0.0")]
[assembly: AssemblyTitle(title: "OpenNos.DAL.Interface")]
[assembly: AssemblyConfiguration(configuration: "")]
[assembly: AssemblyTrademark(trademark: "")]
[assembly: AssemblyCulture(culture: "")]

// Setting ComVisible to false makes the types in this assembly not visible to COM components. If you
// need to access a type in this assembly from COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(visibility: false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid(guid: "175c851b-8dc8-43a5-af93-af32d106cd77")]