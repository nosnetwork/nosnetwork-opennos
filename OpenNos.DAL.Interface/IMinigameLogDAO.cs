﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IMinigameLogDao
    {
        #region Methods

        SaveResult InsertOrUpdate(ref MinigameLogDto minigameLog);

        MinigameLogDto LoadById(long minigameLogId);

        IEnumerable<MinigameLogDto> LoadByCharacterId(long characterId);

        #endregion
    }
}