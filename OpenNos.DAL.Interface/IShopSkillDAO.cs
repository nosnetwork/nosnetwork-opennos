﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IShopSkillDao
    {
        #region Methods

        ShopSkillDto Insert(ShopSkillDto shopSkill);

        void Insert(List<ShopSkillDto> skills);

        IEnumerable<ShopSkillDto> LoadAll();

        IEnumerable<ShopSkillDto> LoadByShopId(int shopId);

        #endregion
    }
}