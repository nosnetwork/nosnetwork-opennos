﻿using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Domain;

namespace OpenNos.DAL.Interface
{
    public interface IItemInstanceDao
    {
        #region Methods

        DeleteResult DeleteGuidList(IEnumerable<Guid> guids);

        SaveResult InsertOrUpdateFromList(IEnumerable<ItemInstanceDto> items);

        DeleteResult DeleteFromSlotAndType(long characterId, short slot, InventoryType type);

        IEnumerable<ItemInstanceDto> LoadByCharacterId(long characterId);

        ItemInstanceDto LoadBySlotAndType(long characterId, short slot, InventoryType type);

        IEnumerable<ItemInstanceDto> LoadByType(long characterId, InventoryType type);

        IList<Guid> LoadSlotAndTypeByCharacterId(long characterId);

        DeleteResult Delete(Guid id);

        ItemInstanceDto InsertOrUpdate(ItemInstanceDto dto);

        IEnumerable<ItemInstanceDto> InsertOrUpdate(IEnumerable<ItemInstanceDto> dtos);

        ItemInstanceDto LoadById(Guid id);

        #endregion
    }
}