﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IMaintenanceLogDao
    {
        #region Methods

        MaintenanceLogDto Insert(MaintenanceLogDto maintenanceLog);

        IEnumerable<MaintenanceLogDto> LoadAll();

        MaintenanceLogDto LoadFirst();

        #endregion
    }
}