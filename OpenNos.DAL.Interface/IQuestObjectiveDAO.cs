﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IQuestObjectiveDao
    {
        #region Methods

        QuestObjectiveDto Insert(QuestObjectiveDto questObjective);

        void Insert(List<QuestObjectiveDto> questObjectives);

        List<QuestObjectiveDto> LoadAll();

        IEnumerable<QuestObjectiveDto> LoadByQuestId(long questId);

        #endregion
    }
}