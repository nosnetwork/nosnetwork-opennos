﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IGeneralLogDao
    {
        #region Methods

        bool IdAlreadySet(long id);

        GeneralLogDto Insert(GeneralLogDto generalLog);

        SaveResult InsertOrUpdate(ref GeneralLogDto generalLog);

        IEnumerable<GeneralLogDto> LoadAll();

        IEnumerable<GeneralLogDto> LoadByIp(string ip);

        IEnumerable<GeneralLogDto> LoadByAccount(long? accountId);

        IEnumerable<GeneralLogDto> LoadByLogType(string logType, long? characterId);

        IEnumerable<GeneralLogDto> LoadByLogTypeAndAccountId(string logType, long? accountId);

        void SetCharIdNull(long? characterId);

        void WriteGeneralLog(long accountId, string ipAddress, long? characterId, string logType, string logData);

        #endregion
    }
}