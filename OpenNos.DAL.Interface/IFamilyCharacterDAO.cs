﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IFamilyCharacterDao
    {
        #region Methods

        DeleteResult Delete(long characterId);

        SaveResult InsertOrUpdate(ref FamilyCharacterDto character);

        FamilyCharacterDto LoadByCharacterId(long characterId);

        IList<FamilyCharacterDto> LoadByFamilyId(long familyId);

        FamilyCharacterDto LoadById(long familyCharacterId);

        #endregion
    }
}