﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IMinilandObjectDao
    {
        #region Methods

        DeleteResult DeleteById(long id);

        SaveResult InsertOrUpdate(ref MinilandObjectDto obj);

        IEnumerable<MinilandObjectDto> LoadByCharacterId(long characterId);

        #endregion
    }
}