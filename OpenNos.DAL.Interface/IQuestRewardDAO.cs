﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IQuestRewardDao
    {
        #region Methods

        QuestRewardDto Insert(QuestRewardDto questReward);

        void Insert(List<QuestRewardDto> questRewards);

        List<QuestRewardDto> LoadAll();

        IEnumerable<QuestRewardDto> LoadByQuestId(long questId);

        #endregion
    }
}