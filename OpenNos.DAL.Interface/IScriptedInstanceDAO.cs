﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IScriptedInstanceDao
    {
        #region Methods

        ScriptedInstanceDto Insert(ScriptedInstanceDto scriptedInstance);

        void Insert(List<ScriptedInstanceDto> scriptedInstances);

        IEnumerable<ScriptedInstanceDto> LoadByMap(short mapId);

        #endregion
    }
}