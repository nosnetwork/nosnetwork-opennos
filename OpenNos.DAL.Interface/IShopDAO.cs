﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IShopDao
    {
        #region Methods

        DeleteResult DeleteByNpcId(int mapNpcId);

        ShopDto Insert(ShopDto shop);

        SaveResult Update(ref ShopDto shop);

        void Insert(List<ShopDto> shops);

        IEnumerable<ShopDto> LoadAll();

        ShopDto LoadById(int shopId);

        ShopDto LoadByNpc(int mapNpcId);

        #endregion
    }
}