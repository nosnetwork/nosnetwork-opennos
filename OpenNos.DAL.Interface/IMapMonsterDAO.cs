﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IMapMonsterDao
    {
        #region Methods

        DeleteResult DeleteById(int mapMonsterId);

        bool DoesMonsterExist(int mapMonsterId);

        MapMonsterDto Insert(MapMonsterDto mapMonster);

        void Insert(IEnumerable<MapMonsterDto> mapMonsters);

        SaveResult Update(ref MapMonsterDto mapMonster);

        MapMonsterDto LoadById(int mapMonsterId);

        IEnumerable<MapMonsterDto> LoadFromMap(short mapId);

        #endregion
    }
}