﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IMapTypeDao
    {
        #region Methods

        MapTypeDto Insert(ref MapTypeDto mapType);

        IEnumerable<MapTypeDto> LoadAll();

        MapTypeDto LoadById(short maptypeId);

        #endregion
    }
}