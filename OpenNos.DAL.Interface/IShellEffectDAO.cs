﻿using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IShellEffectDao
    {
        #region Methods

        DeleteResult DeleteByEquipmentSerialId(Guid id);

        ShellEffectDto InsertOrUpdate(ShellEffectDto shelleffect);

        void InsertOrUpdateFromList(List<ShellEffectDto> shellEffects, Guid equipmentSerialId);

        IEnumerable<ShellEffectDto> LoadByEquipmentSerialId(Guid id);

        #endregion
    }
}