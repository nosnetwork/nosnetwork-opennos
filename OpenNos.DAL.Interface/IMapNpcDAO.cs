﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IMapNpcDao
    {
        #region Methods

        DeleteResult DeleteById(int mapNpcId);

        bool DoesNpcExist(int mapNpcId);

        MapNpcDto Insert(MapNpcDto npc);

        void Insert(List<MapNpcDto> npcs);

        SaveResult Update(ref MapNpcDto mapNpc);

        IEnumerable<MapNpcDto> LoadAll();

        MapNpcDto LoadById(int mapNpcId);

        IEnumerable<MapNpcDto> LoadFromMap(short mapId);

        #endregion
    }
}