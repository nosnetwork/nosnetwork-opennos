﻿using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IPartnerSkillDao
    {
        PartnerSkillDto Insert(PartnerSkillDto partnerSkillDto);

        List<PartnerSkillDto> LoadByEquipmentSerialId(Guid equipmentSerialId);

        DeleteResult Remove(long partnerSkillId);
    }
}