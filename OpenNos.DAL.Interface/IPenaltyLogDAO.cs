﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IPenaltyLogDao
    {
        #region Methods

        DeleteResult Delete(int penaltyLogId);

        SaveResult InsertOrUpdate(ref PenaltyLogDto log);

        IEnumerable<PenaltyLogDto> LoadAll();

        IEnumerable<PenaltyLogDto> LoadByAccount(long accountId);

        PenaltyLogDto LoadById(int penaltyLogId);

        IEnumerable<PenaltyLogDto> LoadByIp(string ip);

        #endregion
    }
}