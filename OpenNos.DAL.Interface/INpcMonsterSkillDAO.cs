﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface INpcMonsterSkillDao
    {
        #region Methods

        NpcMonsterSkillDto Insert(ref NpcMonsterSkillDto npcMonsterSkill);

        void Insert(List<NpcMonsterSkillDto> skills);

        List<NpcMonsterSkillDto> LoadAll();

        IEnumerable<NpcMonsterSkillDto> LoadByNpcMonster(short npcId);

        #endregion
    }
}