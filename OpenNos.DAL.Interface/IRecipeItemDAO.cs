﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IRecipeItemDao
    {
        #region Methods

        RecipeItemDto Insert(RecipeItemDto recipeItem);

        IEnumerable<RecipeItemDto> LoadAll();

        RecipeItemDto LoadById(short recipeItemId);

        IEnumerable<RecipeItemDto> LoadByRecipe(short recipeId);

        IEnumerable<RecipeItemDto> LoadByRecipeAndItem(short recipeId, short itemVNum);

        #endregion
    }
}