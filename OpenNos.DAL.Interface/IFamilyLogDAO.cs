﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IFamilyLogDao
    {
        #region Methods

        DeleteResult Delete(long familyLogId);

        SaveResult InsertOrUpdate(ref FamilyLogDto familyLog);

        IEnumerable<FamilyLogDto> LoadByFamilyId(long familyId);

        #endregion
    }
}