﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface ITeleporterDao
    {
        #region Methods

        TeleporterDto Insert(TeleporterDto teleporter);

        IEnumerable<TeleporterDto> LoadAll();

        TeleporterDto LoadById(short teleporterId);

        IEnumerable<TeleporterDto> LoadFromNpc(int npcId);

        #endregion
    }
}