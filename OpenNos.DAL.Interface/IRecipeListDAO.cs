﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IRecipeListDao
    {
        #region Methods

        RecipeListDto Insert(RecipeListDto recipeList);

        IEnumerable<RecipeListDto> LoadAll();

        RecipeListDto LoadById(int recipeListId);

        IEnumerable<RecipeListDto> LoadByItemVNum(short itemVNum);

        IEnumerable<RecipeListDto> LoadByMapNpcId(int mapNpcId);

        IEnumerable<RecipeListDto> LoadByRecipeId(short recipeId);

        void Update(RecipeListDto recipe);

        #endregion
    }
}