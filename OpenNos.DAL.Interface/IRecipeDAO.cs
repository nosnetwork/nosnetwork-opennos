﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IRecipeDao
    {
        #region Methods

        RecipeDto Insert(RecipeDto recipe);

        IEnumerable<RecipeDto> LoadAll();

        RecipeDto LoadById(short recipeId);

        RecipeDto LoadByItemVNum(short itemVNum);

        void Update(RecipeDto recipe);

        #endregion
    }
}