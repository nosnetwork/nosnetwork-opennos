﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IShopItemDao
    {
        #region Methods

        DeleteResult DeleteById(int itemId);

        ShopItemDto Insert(ShopItemDto item);

        void Insert(List<ShopItemDto> items);

        IEnumerable<ShopItemDto> LoadAll();

        ShopItemDto LoadById(int itemId);

        IEnumerable<ShopItemDto> LoadByShopId(int shopId);

        #endregion
    }
}