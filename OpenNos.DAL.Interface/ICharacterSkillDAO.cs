﻿using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface ICharacterSkillDao
    {
        #region Methods

        DeleteResult Delete(long characterId, short skillVNum);

        IEnumerable<CharacterSkillDto> LoadByCharacterId(long characterId);

        IEnumerable<Guid> LoadKeysByCharacterId(long characterId);

        DeleteResult Delete(Guid id);

        CharacterSkillDto InsertOrUpdate(CharacterSkillDto dto);

        IEnumerable<CharacterSkillDto> InsertOrUpdate(IEnumerable<CharacterSkillDto> dtos);

        CharacterSkillDto LoadById(Guid id);

        #endregion
    }
}