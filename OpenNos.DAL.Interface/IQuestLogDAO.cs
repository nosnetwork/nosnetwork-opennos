﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IQuestLogDao
    {
        SaveResult InsertOrUpdate(ref QuestLogDto questLog);

        QuestLogDto LoadById(long id);

        IEnumerable<QuestLogDto> LoadByCharacterId(long id);
    }
}