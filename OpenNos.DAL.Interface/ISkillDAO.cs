﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface ISkillDao
    {
        #region Methods

        SkillDto Insert(SkillDto skill);

        void Insert(List<SkillDto> skills);

        IEnumerable<SkillDto> LoadAll();

        SkillDto LoadById(short skillId);

        SaveResult InsertOrUpdate(SkillDto skill);

        #endregion
    }
}