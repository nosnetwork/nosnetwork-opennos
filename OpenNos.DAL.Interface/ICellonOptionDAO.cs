﻿using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface ICellonOptionDao
    {
        #region Methods

        DeleteResult DeleteByEquipmentSerialId(Guid id);

        IEnumerable<CellonOptionDto> GetOptionsByWearableInstanceId(Guid wearableInstanceId);

        void InsertOrUpdateFromList(List<CellonOptionDto> cellonOption, Guid equipmentSerialId);

        CellonOptionDto InsertOrUpdate(CellonOptionDto cellonOption);

        #endregion
    }
}