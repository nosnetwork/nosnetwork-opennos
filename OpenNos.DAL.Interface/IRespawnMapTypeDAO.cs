﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IRespawnMapTypeDao
    {
        #region Methods

        void Insert(List<RespawnMapTypeDto> respawnMapTypes);

        SaveResult InsertOrUpdate(ref RespawnMapTypeDto respawnMapType);

        RespawnMapTypeDto LoadById(long respawnMapTypeId);

        RespawnMapTypeDto LoadByMapId(short mapId);

        #endregion
    }
}