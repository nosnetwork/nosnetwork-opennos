﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface ICardDao
    {
        #region Methods

        CardDto Insert(ref CardDto card);

        void Insert(List<CardDto> cards);

        IEnumerable<CardDto> LoadAll();

        CardDto LoadById(short cardId);

        SaveResult InsertOrUpdate(CardDto card);

        #endregion
    }
}