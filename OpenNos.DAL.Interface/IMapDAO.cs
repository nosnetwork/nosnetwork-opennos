﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IMapDao
    {
        #region Methods

        MapDto Insert(MapDto map);

        void Insert(List<MapDto> maps);

        IEnumerable<MapDto> LoadAll();

        MapDto LoadById(short mapId);

        #endregion
    }
}