﻿using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface ICharacterQuestDao
    {
        #region Methods

        DeleteResult Delete(long characterId, long questId);

        CharacterQuestDto InsertOrUpdate(CharacterQuestDto quest);

        IEnumerable<CharacterQuestDto> LoadByCharacterId(long characterId);

        IEnumerable<Guid> LoadKeysByCharacterId(long characterId);

        #endregion
    }
}