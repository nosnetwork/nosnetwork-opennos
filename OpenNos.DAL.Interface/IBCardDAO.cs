﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IBCardDao
    {
        #region Methods

        DeleteResult DeleteByItemVNum(short itemVNum);

        DeleteResult DeleteBySkillVNum(short skillVNum);

        DeleteResult DeleteByMonsterVNum(short monsterVNum);

        DeleteResult DeleteByCardId(short cardId);

        BCardDto Insert(ref BCardDto cardObject);

        void Insert(List<BCardDto> cards);

        IEnumerable<BCardDto> LoadAll();

        IEnumerable<BCardDto> LoadByCardId(short cardId);

        BCardDto LoadById(short cardId);

        IEnumerable<BCardDto> LoadByItemVNum(short vNum);

        IEnumerable<BCardDto> LoadByNpcMonsterVNum(short vNum);

        IEnumerable<BCardDto> LoadBySkillVNum(short vNum);

        #endregion
    }
}