﻿using OpenNos.Data;
using OpenNos.Data.Enums;
using OpenNos.Domain;

namespace OpenNos.DAL.Interface
{
    public interface IAccountDao
    {
        #region Methods

        DeleteResult Delete(long accountId);

        SaveResult InsertOrUpdate(ref AccountDto account);

        AccountDto LoadById(long accountId);

        AccountDto LoadByName(string name);

        void WriteGeneralLog(long accountId, string ipAddress, long? characterId, GeneralLogType logType,
            string logData);

        #endregion
    }
}