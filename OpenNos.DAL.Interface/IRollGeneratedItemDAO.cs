﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IRollGeneratedItemDao
    {
        #region Methods

        RollGeneratedItemDto Insert(RollGeneratedItemDto item);

        IEnumerable<RollGeneratedItemDto> LoadAll();

        RollGeneratedItemDto LoadById(short id);

        IEnumerable<RollGeneratedItemDto> LoadByItemVNum(short vnum);

        #endregion
    }
}