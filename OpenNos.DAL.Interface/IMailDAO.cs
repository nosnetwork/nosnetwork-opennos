﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IMailDao
    {
        #region Methods

        DeleteResult DeleteById(long mailId);

        SaveResult InsertOrUpdate(ref MailDto mail);

        IEnumerable<MailDto> LoadAll();

        MailDto LoadById(long mailId);

        IEnumerable<MailDto> LoadSentToCharacter(long characterId);

        IEnumerable<MailDto> LoadSentByCharacter(long characterId);
        //

        #endregion
    }
}