﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IDropDao
    {
        #region Methods

        DropDto Insert(DropDto drop);

        void Insert(List<DropDto> drops);

        List<DropDto> LoadAll();

        IEnumerable<DropDto> LoadByMonster(short monsterVNum);

        #endregion
    }
}