﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface ICharacterRelationDao
    {
        #region Methods

        DeleteResult Delete(long characterRelationId);

        SaveResult InsertOrUpdate(ref CharacterRelationDto characterRelation);

        IEnumerable<CharacterRelationDto> LoadAll();

        CharacterRelationDto LoadById(long characterId);

        #endregion
    }
}