﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IBazaarItemDao
    {
        #region Methods

        DeleteResult Delete(long bazaarItemId);

        SaveResult InsertOrUpdate(ref BazaarItemDto bazaarItem);

        IEnumerable<BazaarItemDto> LoadAll();

        BazaarItemDto LoadById(long bazaarItemId);

        void RemoveOutDated();

        #endregion
    }
}