﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IQuestDao
    {
        #region Methods

        DeleteResult DeleteById(long id);

        QuestDto InsertOrUpdate(QuestDto quest);

        void Insert(List<QuestDto> questList);

        QuestDto LoadById(long id);

        IEnumerable<QuestDto> LoadAll();

        #endregion
    }
}