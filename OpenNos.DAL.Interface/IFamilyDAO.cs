﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IFamilyDao
    {
        #region Methods

        DeleteResult Delete(long familyId);

        SaveResult InsertOrUpdate(ref FamilyDto family);

        IEnumerable<FamilyDto> LoadAll();

        FamilyDto LoadByCharacterId(long characterId);

        FamilyDto LoadById(long familyId);

        FamilyDto LoadByName(string name);

        #endregion
    }
}