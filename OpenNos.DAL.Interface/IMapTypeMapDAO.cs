﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IMapTypeMapDao
    {
        #region Methods

        void Insert(List<MapTypeMapDto> mapTypeMaps);

        IEnumerable<MapTypeMapDto> LoadAll();

        MapTypeMapDto LoadByMapAndMapType(short mapId, short maptypeId);

        IEnumerable<MapTypeMapDto> LoadByMapId(short mapId);

        IEnumerable<MapTypeMapDto> LoadByMapTypeId(short maptypeId);

        #endregion
    }
}