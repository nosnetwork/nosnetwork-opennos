﻿using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IRespawnDao
    {
        #region Methods

        SaveResult InsertOrUpdate(ref RespawnDto respawn);

        IEnumerable<RespawnDto> LoadByCharacter(long characterId);

        RespawnDto LoadById(long respawnId);

        #endregion
    }
}