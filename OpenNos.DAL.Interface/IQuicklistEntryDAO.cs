﻿using System;
using System.Collections.Generic;
using OpenNos.Data;
using OpenNos.Data.Enums;

namespace OpenNos.DAL.Interface
{
    public interface IQuicklistEntryDao
    {
        #region Methods

        IEnumerable<QuicklistEntryDto> LoadByCharacterId(long characterId);

        IEnumerable<Guid> LoadKeysByCharacterId(long characterId);

        DeleteResult Delete(Guid id);

        QuicklistEntryDto InsertOrUpdate(QuicklistEntryDto dto);

        IEnumerable<QuicklistEntryDto> InsertOrUpdate(IEnumerable<QuicklistEntryDto> dtos);

        QuicklistEntryDto LoadById(Guid id);

        #endregion
    }
}