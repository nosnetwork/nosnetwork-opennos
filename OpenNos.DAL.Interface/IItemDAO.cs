﻿using System.Collections.Generic;
using OpenNos.Data;

namespace OpenNos.DAL.Interface
{
    public interface IItemDao
    {
        #region Methods

        IEnumerable<ItemDto> FindByName(string name);

        ItemDto Insert(ItemDto item);

        void Insert(IEnumerable<ItemDto> items);

        IEnumerable<ItemDto> LoadAll();

        ItemDto LoadById(short vNum);

        #endregion
    }
}