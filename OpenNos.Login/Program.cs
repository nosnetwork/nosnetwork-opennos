using System;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using log4net;
using OpenNos.Core;
using OpenNos.Core.Cryptography;
using OpenNos.Core.Serializing;
using OpenNos.DAL.EF.Helpers;
using OpenNos.GameObject;
using OpenNos.Handler;
using OpenNos.Master.Library.Client;

namespace OpenNos.Login
{
    public static class Program
    {
        #region Members

        public static bool _isDebug;

        static int _port;

        #endregion

        #region Methods

        public static void Main(string[] args)
        {
            checked
            {
                try
                {
#if DEBUG
                    _isDebug = true;
#endif
                    CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo(name: "en-US");
                    // ReSharper disable once LocalizableElement
                    Console.Title = $"Erased Login Server{(_isDebug ? " Development Environment" : "")}";

                    var ignoreStartupMessages = false;
                    foreach (var arg in args) ignoreStartupMessages |= arg == "--nomsg";

                    // initialize Logger
                    Logger.InitializeLogger(log: LogManager.GetLogger(type: typeof(Program)));

                    var port = Convert.ToInt32(value: ConfigurationManager.AppSettings[name: "LoginPort"]);
                    var portArgIndex = Array.FindIndex(array: args, match: s => s == "--port");
                    if (portArgIndex != -1
                        && args.Length >= portArgIndex + 1
                        && int.TryParse(s: args[portArgIndex + 1], result: out port))
                        // ReSharper disable once LocalizableElement
                        Console.WriteLine(value: "Port override: " + port);
                    _port = port;
                    if (!ignoreStartupMessages)
                    {
                        var assembly = Assembly.GetExecutingAssembly();
                        FileVersionInfo.GetVersionInfo(fileName: assembly.Location);
                        var text = "LOGIN SERVER by NN Team";
                        var offset = Console.WindowWidth / 2 + text.Length / 2;
                        var separator = new string(c: '=', count: Console.WindowWidth);
                        Console.WriteLine(value: separator + string.Format(format: "{0," + offset + "}\n", arg0: text) +
                                                 separator);
                    }

                    // initialize api
                    if (CommunicationServiceClient.Instance.Authenticate(
                        authKey: ConfigurationManager.AppSettings[name: "MasterAuthKey"]))
                        Logger.Info(message: Language.Instance.GetMessageFromKey(key: "API_INITIALIZED"));

                    // initialize DB
                    if (!DataAccessHelper.Initialize())
                    {
                        Console.ReadKey();
                        return;
                    }

                    Logger.Info(message: Language.Instance.GetMessageFromKey(key: "CONFIG_LOADED"));

                    try
                    {
                        AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(data: "General Error", ex: ex);
                    }

                    try
                    {
                        // initialize PacketSerialization
                        PacketFactory.Initialize<WalkPacket>();

                        // ReSharper disable once UnusedVariable
                        var networkManager = new NetworkManager<LoginCryptography>(
                            ipAddress: ConfigurationManager.AppSettings[name: "IPAddress"], port: port,
                            packetHandler: typeof(LoginPacketHandler),
                            fallbackEncryptor: typeof(LoginCryptography), isWorldServer: false);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogEventError(logEvent: "INITIALIZATION_EXCEPTION", data: "General Error Server",
                            ex: ex);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogEventError(logEvent: "INITIALIZATION_EXCEPTION", data: "General Error", ex: ex);
                    Console.ReadKey();
                }
            }
        }

        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.Error(ex: (Exception)e.ExceptionObject);
            try
            {
            }
            catch (Exception ex)
            {
                Logger.Error(ex: ex);
            }

            Logger.Debug(data: "Login Server crashed! Rebooting gracefully...");
            Process.Start(fileName: "OpenNos.Login.exe", arguments: $"--nomsg --port {_port}");
            Environment.Exit(exitCode: 1);
        }

        #endregion
    }
}