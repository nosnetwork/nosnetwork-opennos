using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using log4net.Config;

// General Information about an assembly is controlled through the following set of attributes.
// Change these attribute values to modify the information associated with an assembly.
[assembly: AssemblyDescription(description: "Open source nos-emulation project")]
[assembly: AssemblyCompany(company: "OpenNos Team")]
[assembly: AssemblyCopyright(copyright: "OpenNos Team Copyright � 2017")]
[assembly: AssemblyProduct(product: "OpenNos.Login")]

// Version information for an assembly consists of the following four values:
//
// Major Version Minor Version Build Number Revision
//
// You can specify all the values or you can default the Build and Revision Numbers by using the '*'
// as shown below: [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(version: "1.1.*")]
[assembly: AssemblyTitle(title: "OpenNos.Login")]
[assembly: AssemblyConfiguration(configuration: "")]
[assembly: AssemblyTrademark(trademark: "")]
[assembly: AssemblyCulture(culture: "")]

// Setting ComVisible to false makes the types in this assembly not visible to COM components. If you
// need to access a type in this assembly from COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(visibility: false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid(guid: "8267017b-5871-46a7-b4c9-5f50d801a0eb")]
[assembly:
    Debuggable(modes: DebuggableAttribute.DebuggingModes.Default |
                      DebuggableAttribute.DebuggingModes.DisableOptimizations |
                      DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints |
                      DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: CompilationRelaxations(relaxations: 8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: XmlConfigurator(Watch = true)]